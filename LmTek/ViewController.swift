//
//  ViewController.swift
//  LmTek
//
//  Created by Sai Sankar on 06/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

//        switch AVCaptureDevice.authorizationStatus(for: .video) {
//        case .notDetermined:
//            AVCaptureDevice.requestAccess(for: .video) { granted in
//                if granted {
//                    self._setupCaptureSession()
//                }
//            }
//        case .restricted:
//            break
//        case .denied:
//            break
//        case .authorized:
//            _setupCaptureSession()
//        }
    }
    private enum EffectType {
        case invert, none
    }
    private var _effectType = EffectType.none
    private var _captureSession: AVCaptureSession?
    private var _videoOutput: AVCaptureVideoDataOutput?
    private var _assetWriter: AVAssetWriter?
    private var _assetWriterInput: AVAssetWriterInput?
    private var _adpater: AVAssetWriterInputPixelBufferAdaptor?
    private var _currentSampleTime: CMTime?
    private var _currentVideoDimensions: CMVideoDimensions?
    private var _filename = ""
    private var _time: Double = 0
    private func _setupCaptureSession() {
        let session = AVCaptureSession()
        session.sessionPreset = .hd1920x1080

        guard
            let device = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .unspecified),
            let input = try? AVCaptureDeviceInput(device: device),
            session.canAddInput(input) else { return }

        session.beginConfiguration()
        session.addInput(input)
        session.commitConfiguration()

        let output = AVCaptureVideoDataOutput()
        guard session.canAddOutput(output) else { return }
        output.setSampleBufferDelegate(self, queue: DispatchQueue(label: "com.yusuke024.video"))
        session.beginConfiguration()
        session.addOutput(output)
        session.commitConfiguration()

        DispatchQueue.main.async {
            let previewView = _PreviewView()
            previewView.videoPreviewLayer.session = session
            previewView.frame = self.view.bounds
            previewView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.view.insertSubview(previewView, at: 0)
        }

        session.startRunning()
        _videoOutput = output
        _captureSession = session
    }

    private enum _CaptureState {
        case idle, start, capturing, end
    }
    private var _captureState = _CaptureState.idle
    @IBAction func capture(_ sender: Any) {
        switch _captureState {
        case .idle:
            _captureState = .start
        case .capturing:
            _captureState = .end
        default:
            break
        }
    }
    
    @IBAction func changeEffect() {
        switch _effectType {
        case .invert:
            self._effectType = .none
        case .none:
            self._effectType = .invert
        }
    }
}

extension ViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        print("didOutput sampleBuffer:",sampleBuffer)
        let timestamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer).seconds
        if self._effectType == .invert {
            connection.videoOrientation = .portrait
            guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
                return
            }
            let cameraImage = CIImage(cvPixelBuffer: pixelBuffer)
            let filter = CIFilter(name: "CIColorInvert")
            filter?.setValue(cameraImage, forKey: kCIInputImageKey)
            
            let formatDescription = CMSampleBufferGetFormatDescription(sampleBuffer)!
            self._currentVideoDimensions = CMVideoFormatDescriptionGetDimensions(formatDescription)
            self._currentSampleTime = CMSampleBufferGetOutputPresentationTimeStamp(sampleBuffer)
            DispatchQueue.main.async {
                if let outputValue = filter!.value(forKey: kCIOutputImageKey) as? CIImage {
                    let filteredImage = UIImage(ciImage: outputValue)
                    self.imageView.image = filteredImage
                }
            }
        }
        else {
            DispatchQueue.main.async {
                self.imageView.image = nil
            }
        }
        
        switch _captureState {
        case .start:
            // Set up recorder
            _filename = UUID().uuidString
            let videoPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("\(_filename).mov")
            let writer = try! AVAssetWriter(outputURL: videoPath, fileType: .mov)
            let settings = _videoOutput!.recommendedVideoSettingsForAssetWriter(writingTo: .mov)
            let input = AVAssetWriterInput(mediaType: .video, outputSettings: settings) // [AVVideoCodecKey: AVVideoCodecType.h264, AVVideoWidthKey: 1920, AVVideoHeightKey: 1080])
            input.mediaTimeScale = CMTimeScale(bitPattern: 600)
            input.expectsMediaDataInRealTime = true
            input.transform = CGAffineTransform(rotationAngle: .pi/2)
            let adapter = AVAssetWriterInputPixelBufferAdaptor(assetWriterInput: input, sourcePixelBufferAttributes: nil)
            if writer.canAdd(input) {
                writer.add(input)
            }
            writer.startWriting()
            writer.startSession(atSourceTime: .zero)
            _assetWriter = writer
            _assetWriterInput = input
            _adpater = adapter
            _captureState = .capturing
            _time = timestamp
        case .capturing:
            if _assetWriterInput?.isReadyForMoreMediaData == true {
                if self._effectType == .invert {
                    var newPixelBuffer : CVPixelBuffer? = nil
                    CVPixelBufferPoolCreatePixelBuffer(nil, (_adpater?.pixelBufferPool)!, &newPixelBuffer)
                    
                    let success = self._adpater?.append(newPixelBuffer!, withPresentationTime: self._currentSampleTime!)
                    
                    if success == false {
                        print("Pixel Buffer failed")
                    }
                }
                let time = CMTime(seconds: timestamp - _time, preferredTimescale: CMTimeScale(600))
                _adpater?.append(CMSampleBufferGetImageBuffer(sampleBuffer)!, withPresentationTime: time)
            }
            break
        case .end:
            guard _assetWriterInput?.isReadyForMoreMediaData == true, _assetWriter!.status != .failed else { break }
            let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("\(_filename).mov")
            _assetWriterInput?.markAsFinished()
            _assetWriter?.finishWriting { [weak self] in
                self?._captureState = .idle
                self?._assetWriter = nil
                self?._assetWriterInput = nil
                DispatchQueue.main.async {
                    let videoPlayerVC = self?.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                    videoPlayerVC.videoURL = url
                    DispatchQueue.main.async {
                        self?.present(videoPlayerVC, animated: true, completion: nil)
                    }
                    /*let player = AVPlayer(url: url)
                    let playerLayer = AVPlayerLayer(player: player)
                    playerLayer.frame = CGRect(x: 0, y: 0, width: 200, height: 300)
                    self?.view.layer.addSublayer(playerLayer)
                    player.play()*/
                    //let activity = UIActivityViewController(activityItems: [url], applicationActivities: nil)
                    //self?.present(activity, animated: true, completion: nil)
                }
            }
        default:
            break
        }
        
    }
}

private class _PreviewView: UIView {
    override class var layerClass: AnyClass {
        return AVCaptureVideoPreviewLayer.self
    }

    var videoPreviewLayer: AVCaptureVideoPreviewLayer {
        return layer as! AVCaptureVideoPreviewLayer
    }
}
