//
//  HasTagCollectionCell.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 27/07/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class HasTagCollectionCell: UICollectionViewCell {
    var viewBG:UIView = UIView()
    let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var imgDraw:UIImageView = UIImageView()
    var lblLike:UILabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
           
        self.design()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func design(){
        
        viewBG = UIView()
        viewBG.frame = CGRect(x: 0, y: 1, width: (screenWidth/3), height: (screenWidth/3))
        viewBG.backgroundColor = UIColor.black
        viewBG.layer.cornerRadius = 1//ShaplayerViewTicket(calayer1, corner: 12)
        viewBG.layer.borderWidth = 1.5
        viewBG.layer.borderColor = UIColor.black.cgColor
        viewBG.clipsToBounds = true
        self.addSubview(viewBG)
        
        let sLayer:CAShapeLayer = CAShapeLayer()
        imgDraw = UIImageView()
        imgDraw.frame = CGRect(x: 0, y: 0, width: viewBG.frame.size.width, height: viewBG.frame.size.height)
        imgDraw.ShaplayerUp(sLayer, corner: 1)
        imgDraw.image = UIImage.init(named: "Rectangle 15")
        imgDraw.isUserInteractionEnabled = true
        imgDraw.contentMode = .scaleAspectFill
        imgDraw.clipsToBounds = true
        viewBG.addSubview(imgDraw)
        
        let imgVideoIcon:UIImageView = UIImageView()
        imgVideoIcon.frame = CGRect(x: viewBG.frame.size.height-65, y: viewBG.frame.size.height-30, width: 15, height: 15)
        imgVideoIcon.image = UIImage.init(named: "play")
        imgDraw.addSubview(imgVideoIcon)
        
        lblLike = UILabel()
        lblLike.frame = CGRect(x: viewBG.frame.size.height-45, y: viewBG.frame.size.height-35, width: 40, height: 30)
        lblLike.text = "96.3 K"
        lblLike.textAlignment = .center
        lblLike.textColor = .white
        lblLike.font = fontSizeSemiBoldSmall
        imgDraw.addSubview(lblLike)

    }

}
