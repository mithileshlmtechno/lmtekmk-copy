//
//  HastagProfileVC.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 26/07/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit
import Alamofire

@available(iOS 13.0, *)
class HastagProfileVC: BaseViewController,UISearchBarDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout  {
    
    var yRef: CGFloat = 100.0

    var hastagCollectionview:UICollectionView!
    var cellId = "Cell"
    var viewScroll: UIScrollView = UIScrollView()
    var arrAllHasData: NSMutableArray = NSMutableArray()
    var btnUseVideo: UIButton = UIButton()
    var videoIndex : Int?
    var userID: String  = ""
    var videos_list = [GetVideosData]()
    var strSearchkey: String = ""
    var strHastagName: String = ""
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        self.headerView()
        self.screenDesigning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getHastagVideoAPI(strSearchkey)
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func headerView() {
        let btnBack = UIButton()
        btnBack.frame = CGRect(x: 10, y: 44, width: 30, height: 30)
        btnBack.backgroundColor = UIColor.clear
        btnBack.setImage(UIImage.init(named: "back_btn"), for: .normal)
        btnBack.addTarget(self, action: #selector(self.btnBackClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnBack)
    }
    
    func screenDesigning() {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -5)
        layout.itemSize = CGSize(width: (screenWidth/3), height: (screenWidth/3))

        hastagCollectionview = UICollectionView(frame: CGRect(x: 0, y: yRef, width: screenWidth, height: screenHeight-(yRef+40)), collectionViewLayout: layout)
        hastagCollectionview.dataSource = self
        hastagCollectionview.delegate = self
        hastagCollectionview.register(HasTagCollectionCell.self, forCellWithReuseIdentifier: cellId)
        hastagCollectionview.showsVerticalScrollIndicator = false
        hastagCollectionview.backgroundColor = UIColor.clear
        self.view.addSubview(hastagCollectionview)
    
        btnUseVideo = UIButton()
        btnUseVideo.frame = CGRect(x: (screenWidth-70)/2, y: screenHeight-130, width: 70, height: 70)
        btnUseVideo.backgroundColor = colorYellow
        btnUseVideo.layer.cornerRadius = 35
        btnUseVideo.setImage(UIImage.init(named: "Videocamera"), for: .normal)
        btnUseVideo.clipsToBounds = true
        btnUseVideo.addTarget(self, action: #selector(self.btnUseVideoClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnUseVideo)

    }
    
    @objc func btnCancelClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func btnUseVideoClicked(_ sender: UIButton) {
        let cameraRecordingVC = self.storyboard?.instantiateViewController(withIdentifier: "CameraRecordingVC") as! CameraRecordingVC
        self.navigationController?.pushViewController(cameraRecordingVC, animated: false)
    }
    
    //MARK: CollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videos_list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)  as! HasTagCollectionCell
        let objdata: GetVideosData = videos_list[indexPath.row]
        
        let strurl:String = objdata.thumb_url as! String
        let url = URL(string: strurl);
        if (url != nil)
        {
            Cell.imgDraw.clipsToBounds = true
            Cell.imgDraw.sd_setImage(with: url, placeholderImage: UIImage.init(named: ""))
        }
        Cell.lblLike.text = String(describing: objdata.likes ?? "")
        return Cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        let viewHeader: UIView = UIView()
        viewHeader.frame = CGRect(x: 0, y: 0, width: screenWidth, height: 130)
        viewHeader.backgroundColor = UIColor.clear
        collectionView.addSubview(viewHeader)

        let lblHastag = UILabel()
        lblHastag.frame = CGRect(x: 20, y: 10, width: 80, height: 80)
        lblHastag.backgroundColor = colorGrayBG
        lblHastag.layer.cornerRadius = 5
        lblHastag.text = "#"
        lblHastag.textColor = .white
        lblHastag.textAlignment = .center
        lblHastag.font = UIFont.systemFont(ofSize: 35)
        lblHastag.clipsToBounds = true
        viewHeader.addSubview(lblHastag)

        let lblHastagTitle = UILabel()
        lblHastagTitle.frame = CGRect(x: 110, y: 10, width: screenWidth-120, height: 30)
        lblHastagTitle.backgroundColor = .clear
        lblHastagTitle.layer.cornerRadius = 5
        lblHastagTitle.text = strSearchkey
        lblHastagTitle.textColor = .white
        lblHastagTitle.textAlignment = .left
        lblHastagTitle.font = fontSizeBoldTitle
        lblHastagTitle.clipsToBounds = true
        viewHeader.addSubview(lblHastagTitle)

        let lblHastagViews = UILabel()
        lblHastagViews.frame = CGRect(x: 110, y: 60, width: screenWidth-120, height: 30)
        lblHastagViews.backgroundColor = UIColor.clear
        lblHastagViews.layer.cornerRadius = 5
        lblHastagViews.text = "73M Plays"
        lblHastagViews.textColor = .white
        lblHastagViews.textAlignment = .left
        lblHastagViews.font = fontSizeRegulor
        lblHastagViews.clipsToBounds = true
        viewHeader.addSubview(lblHastagViews)

        return CGSize(width: screenWidth, height: 130)

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        let playVideoVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        playVideoVC.checkSearchVideo = true
        playVideoVC.videosArray = videos_list
        playVideoVC.checkVideoTag = indexPath.row
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(playVideoVC, animated: true)
        }

    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemSize = ((collectionView.frame.width/3.0))
            return CGSize(width: itemSize, height: itemSize)
    }
     
    func updateHastagPage() {
        self.hastagCollectionview.reloadData()
    }
    
    func getHastagVideoAPI(_ searchkey: String) {
        let url = URL(string: Constants.BASE_URL+Constants.GET_ALL_HASHTAGS_VIDEOS)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        print(headers)
        let param: Parameters = ["search_key": searchkey]
        print(param)
        DialougeUtils.addActivityView(view: self.view)
        self.getHastagVideoAPICall(param, url: url, header: headers,searchKey: searchkey)
    }

    func getHastagVideoAPICall(_ param: Parameters, url: URL, header: HTTPHeaders,searchKey: String){
        AllUserApis.allPostDataWithoutImageAPI(vc: self, url: url, parameter: param, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status: Bool = result!["status"] as! Bool
                        if status == true{
                            let allData: [[String : Any]] = result!["user_data"] as! [[String : Any]]
                            for i in 0..<allData.count {
                                let dictdata: [String: Any] = allData[i]
                                let objdata: GetVideosData = GetVideosData.init(VideoInfo: dictdata)!
                                self.videos_list.append(objdata)
                            }
                            self.hastagCollectionview.reloadData()
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                }
            }
        }
    }
}
