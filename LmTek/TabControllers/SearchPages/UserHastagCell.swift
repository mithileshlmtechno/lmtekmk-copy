//
//  UserHastagCell.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 20/07/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class UserHastagCell: UITableViewCell {
    
    var viewCell:UIView = UIView()
    var lblUserName:UILabel = UILabel()
    var lblMessage:UILabel = UILabel()
    var btnCross:UIButton = UIButton()
    var lblHastag = UILabel()
    var btnFollow: UIButton = UIButton()

    
    let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        viewCell.frame = CGRect(x: 0, y: 0, width: screenWidth, height: 80)
        viewCell.backgroundColor = .black
        viewCell.clipsToBounds = true
        self.addSubview(viewCell)
        
        lblHastag = UILabel()
        lblHastag.frame = CGRect(x: 5, y: 10, width: 60, height: 60)
        lblHastag.backgroundColor = .white
        lblHastag.layer.cornerRadius = 30
        lblHastag.text = "#"
        lblHastag.font = UIFont.init(name: fontsemoBold, size: 20)
        lblHastag.clipsToBounds = true
        viewCell.addSubview(lblHastag)

        lblUserName = UILabel()
        lblUserName.frame = CGRect(x: 80, y: 15, width: viewCell.frame.size.width-100, height: 20)
        lblUserName.text = "Aman"
        lblUserName.textColor = UIColor.white
        lblUserName.textAlignment = .left
        lblUserName.font = fontSizeSemiBoldTitle
        lblUserName.clipsToBounds = true
        viewCell.addSubview(lblUserName)

        lblMessage = UILabel()
        lblMessage.frame = CGRect(x: 80, y: 40, width: viewCell.frame.size.width-100, height: 20)
        lblMessage.text = "(Aamir) 😊 😊"
        lblMessage.textColor = UIColor.white
        lblMessage.textAlignment = .left
        lblMessage.font = fontSizeRegulor
        lblMessage.clipsToBounds = true
        viewCell.addSubview(lblMessage)

        btnCross = UIButton()
        btnCross.frame = CGRect(x: viewCell.frame.size.width-30, y: 30, width: 20, height: 20)
        btnCross.clipsToBounds = true
        btnCross.setImage(UIImage.init(named: "Cross"), for: .normal)
        viewCell.addSubview(btnCross)
        
        btnFollow = UIButton()
        btnFollow.frame = CGRect(x: viewCell.frame.size.width-90, y: (viewCell.frame.size.height/2)-15, width: 80, height: 30)
        btnFollow.backgroundColor = colorYellow
        btnFollow.setTitle("FOLLOW", for: .normal)
        btnFollow.setTitleColor(.black, for: .normal)
        btnFollow.layer.cornerRadius = 3
        btnFollow.titleLabel?.font = fontSizeSmall
        btnFollow.clipsToBounds = true
        viewCell.addSubview(btnFollow)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
