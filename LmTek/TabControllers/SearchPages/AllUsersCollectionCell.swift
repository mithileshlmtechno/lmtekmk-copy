//
//  AllUsersCollectionCell.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 12/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class AllUsersCollectionCell: UICollectionViewCell {
    var viewBG:UIView = UIView()
    let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var imgDraw:UIImageView = UIImageView()
    var lblDescription:UILabel = UILabel()
    var btnViewGift:UIButton = UIButton()
    var lblType:UILabel = UILabel()
    var btnFavorate:UIButton = UIButton()
    var lblRating:UILabel = UILabel()
    var btnViewDetail:UIButton = UIButton()

    override init(frame: CGRect) {
        super.init(frame: frame)
           
        self.design()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func design() {
        
        viewBG = UIView()
        viewBG.frame = CGRect(x: 10, y: 5, width: (screenWidth/2)-13, height: 190)
        viewBG.backgroundColor = UIColor.black
        viewBG.layer.cornerRadius = 4//ShaplayerViewTicket(calayer1, corner: 12)
        viewBG.clipsToBounds = true
        viewBG.layer.shadowOffset = CGSize.zero
        viewBG.layer.shadowRadius = 2
        viewBG.layer.shadowOpacity = 2
        viewBG.layer.shadowColor = UIColor.lightGray.withAlphaComponent(0.4).cgColor
        viewBG.layer.masksToBounds = false
        self.addSubview(viewBG)
        
        let sLayer:CAShapeLayer = CAShapeLayer()
        imgDraw = UIImageView()
        imgDraw.frame = CGRect(x: 0, y: 0, width: viewBG.frame.size.width, height: viewBG.frame.size.height-45)
        imgDraw.ShaplayerUp(sLayer, corner: 4)
        imgDraw.image = UIImage.init(named: "Rectangle 15")
        imgDraw.isUserInteractionEnabled = true
        imgDraw.clipsToBounds = true
        viewBG.addSubview(imgDraw)
        
        lblRating = UILabel()
        lblRating.frame = CGRect(x: viewBG.frame.size.width-50, y: viewBG.frame.size.height-72, width: 40, height: 20)
        lblRating.backgroundColor = UIColor.clear
        lblRating.text = "8.2K"
        lblRating.textAlignment = .center
        lblRating.font = fontSizeSmall
        lblRating.clipsToBounds = true
        lblRating.textColor = .white
        imgDraw.addSubview(lblRating)

        let imgAstrick:UIImageView = UIImageView()
        imgAstrick.frame = CGRect(x: viewBG.frame.size.width-70, y: viewBG.frame.size.height-69, width: 12, height: 12)
        imgAstrick.image = UIImage.init(named: "miniPlay")
        imgDraw.addSubview(imgAstrick)
        
        lblDescription = UILabel()
        lblDescription.frame = CGRect(x: 10, y: viewBG.frame.size.height-40, width: viewBG.frame.size.width-20, height: 38)
        lblDescription.backgroundColor = UIColor.clear
        lblDescription.text = "Lorem ipsum dolor sit amet, consectetur adipiscing 😊 "
        lblDescription.font = fontSizeSmall
        lblDescription.clipsToBounds = true
        lblDescription.textColor = colorGray
        lblDescription.textAlignment = .left
        lblDescription.numberOfLines = 2
        lblDescription.lineBreakMode = .byWordWrapping
        lblDescription.sizeToFit()
        viewBG.addSubview(lblDescription)
    }

}
