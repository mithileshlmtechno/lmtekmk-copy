//
//  SearchAllVC.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 13/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit
import Alamofire

@available(iOS 13.0, *)
class SearchAllVC: BaseViewController,UISearchBarDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout  {

    var yRef: CGFloat = 42.0
    var searchUsers = UISearchBar()
    var searchActive:Bool = false

    var ProductCollectionview:UICollectionView!
    var cellId = "Cell"
    var viewScroll: UIScrollView = UIScrollView()
    var arrAllSearchData: NSMutableArray = NSMutableArray()
    var arrAllHasData: NSMutableArray = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        self.screenDesigning()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    func screenDesigning() {
        
        searchUsers = UISearchBar()
        searchUsers.frame = CGRect(x: 10, y: yRef, width: screenWidth-100, height: 40)
        searchUsers.backgroundColor = ColorTextFieldBG
        searchUsers.placeholder = "Search Users, Hastags"
        searchUsers.barTintColor = ColorTextFieldBG
        searchUsers.searchTextField.backgroundColor = ColorTextFieldBG
        searchUsers.searchTextField.font = fontSizeRegulor
        //searchUsers.delegate = self
        searchUsers.isUserInteractionEnabled = true
        searchUsers.layer.cornerRadius = 4
        searchUsers.layer.borderWidth = 1
        searchUsers.layer.borderColor = colorGray.cgColor
        searchUsers.showsCancelButton = false
        searchUsers.clipsToBounds = true
        self.view.addSubview(searchUsers)
        
        let btnCancel = UIButton()
        btnCancel.frame = CGRect(x: screenWidth-80, y: yRef, width: 70, height: 40)
        btnCancel.backgroundColor = UIColor.clear
        btnCancel.setTitle("Cancel", for: .normal)
        btnCancel.titleLabel?.font = fontSizeSemiBoldTitle
        btnCancel.setTitleColor(colorYellow, for: .normal)
        btnCancel.addTarget(self, action: #selector(self.btnCancelClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnCancel)
        
        yRef = yRef+searchUsers.frame.size.height + 15
        
        viewScroll = UIScrollView()
        viewScroll.frame = CGRect(x: 0, y: yRef, width: screenWidth, height: screenHeight-yRef)
        viewScroll.backgroundColor = UIColor.clear
        viewScroll.isPagingEnabled = true
        viewScroll.clipsToBounds = true
        self.view.addSubview(viewScroll)
                
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -5)
        layout.itemSize = CGSize(width: (screenWidth/3), height: (screenWidth/3))

        ProductCollectionview = UICollectionView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight-(yRef+40)), collectionViewLayout: layout)
        ProductCollectionview.dataSource = self
        ProductCollectionview.delegate = self
        ProductCollectionview.register(SearchAllCollectionCell.self, forCellWithReuseIdentifier: cellId)
        ProductCollectionview.showsVerticalScrollIndicator = false
        ProductCollectionview.backgroundColor = UIColor.clear
        viewScroll.addSubview(ProductCollectionview)
    
    }
    @objc func btnCancelClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: SearchviewDelegate
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
        let ViewForDoneButtonOnKeyboard = UIToolbar()
        ViewForDoneButtonOnKeyboard.sizeToFit()
        let btnDoneOnKeyboard = UIBarButtonItem(title: "Done", style: .bordered, target: self, action: #selector(self.doneBtnFromKeyboardClicked))
        ViewForDoneButtonOnKeyboard.items = [btnDoneOnKeyboard]
        searchUsers.inputAccessoryView = ViewForDoneButtonOnKeyboard
//        tblUsers.isHidden = true

    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
        //tblSearch.isHidden = true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.text = nil
        searchBar.resignFirstResponder()
//        tblUsers.resignFirstResponder()
//        tblUsers.reloadData()

    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.resignFirstResponder()
    }
    func filterContent(forSearchText searchText: String?, scope: String?) {
        var recipes:NSArray = NSArray()
        var searchResults:NSArray = NSArray()

        let resultPredicate = NSPredicate(format: "name contains[c] %@", searchText ?? "")
        searchResults = recipes.filtered(using: resultPredicate) as NSArray
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        allFilterData = arrMyDocumentData.filter({($0.name as AnyObject).contains(searchText)
//        })
//        print("filter = ",allFilterData)
//
//        tblUsers.isHidden = false
//        if(allFilterData.count == 0){
//            searchActive = false;
//             print("UISearchBar.text cleared!")
//        } else {
//            searchActive = true;
//        }
        //self.getAllSearchAPI(searchText)
    }
    @objc func doneBtnFromKeyboardClicked()
    {
        self.becomeFirstResponder()
        self.view.endEditing(true)
    }

    //MARK: CollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 17
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)  as! SearchAllCollectionCell
        return Cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchFirstVC") as! SearchFirstVC
        self.navigationController?.pushViewController(vc, animated: true)

    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemSize = ((collectionView.frame.width/3.0)-5)
            return CGSize(width: itemSize, height: itemSize)
    }
        
}
