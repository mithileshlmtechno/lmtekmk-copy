//
//  SearchFirstVC.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 12/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SearchFirstVC: BaseViewController,UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {

    var yRef: CGFloat = 42.0
    var searchUsers = UISearchBar()
    var tblUsers: UITableView = UITableView()
    var searchActive:Bool = false
//    var allFilterData:[getMyDocumentData] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        self.screenDesigning()
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    func screenDesigning() {
        searchUsers = UISearchBar()
        searchUsers.frame = CGRect(x: 10, y: yRef, width: screenWidth-100, height: 40)
        searchUsers.backgroundColor = ColorTextFieldBG
        searchUsers.placeholder = "Search Users, Hastags"
        searchUsers.barTintColor = ColorTextFieldBG
        searchUsers.searchTextField.backgroundColor = ColorTextFieldBG
        searchUsers.searchTextField.font = fontSizeRegulor
        //searchUsers.delegate = self
        searchUsers.isUserInteractionEnabled = true
        searchUsers.layer.cornerRadius = 4
        searchUsers.layer.borderWidth = 1
        searchUsers.layer.borderColor = colorGray.cgColor
        searchUsers.showsCancelButton = false
        searchUsers.clipsToBounds = true
        self.view.addSubview(searchUsers)
        
        let btnCancel = UIButton()
        btnCancel.frame = CGRect(x: screenWidth-80, y: yRef, width: 70, height: 40)
        btnCancel.backgroundColor = UIColor.clear
        btnCancel.setTitle("Cancel", for: .normal)
        btnCancel.titleLabel?.font = fontSizeSemiBoldTitle
        btnCancel.setTitleColor(colorYellow, for: .normal)
        btnCancel.addTarget(self, action: #selector(self.btnCancelClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnCancel)
        
        yRef = yRef+searchUsers.frame.size.height + 5
        
        tblUsers = UITableView()
        tblUsers.frame = CGRect(x: 0, y: yRef, width: screenWidth, height: screenHeight-yRef)
        tblUsers.backgroundColor = UIColor.clear
        tblUsers.delegate = self
        tblUsers.dataSource = self
        tblUsers.separatorStyle = .none
        self.view.addSubview(tblUsers)

    }
    //MARK: Action
    
    @objc func btnCancelClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: SearchviewDelegate
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
        let ViewForDoneButtonOnKeyboard = UIToolbar()
        ViewForDoneButtonOnKeyboard.sizeToFit()
        let btnDoneOnKeyboard = UIBarButtonItem(title: "Done", style: .bordered, target: self, action: #selector(self.doneBtnFromKeyboardClicked))
        ViewForDoneButtonOnKeyboard.items = [btnDoneOnKeyboard]
        searchUsers.inputAccessoryView = ViewForDoneButtonOnKeyboard
        tblUsers.isHidden = true

    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
        //tblSearch.isHidden = true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.text = nil
        searchBar.resignFirstResponder()
        tblUsers.resignFirstResponder()
        tblUsers.reloadData()

    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.resignFirstResponder()
    }
    func filterContent(forSearchText searchText: String?, scope: String?) {
        var recipes:NSArray = NSArray()
        var searchResults:NSArray = NSArray()

        let resultPredicate = NSPredicate(format: "name contains[c] %@", searchText ?? "")
        searchResults = recipes.filtered(using: resultPredicate) as NSArray
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        allFilterData = arrMyDocumentData.filter({($0.name as AnyObject).contains(searchText)
//        })
//        print("filter = ",allFilterData)
//
//        tblUsers.isHidden = false
//        if(allFilterData.count == 0){
//            searchActive = false;
//             print("UISearchBar.text cleared!")
//        } else {
//            searchActive = true;
//        }
         self.tblUsers.reloadData()
    }
    @objc func doneBtnFromKeyboardClicked()
    {
        self.becomeFirstResponder()
        self.view.endEditing(true)
        tblUsers.isHidden = false
    }

    //MARK: TableviewDelegate
       
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive == true) {
//            return allFilterData.count
        }else{
//            return arrMyDocumentData.count
        }
        return 2
    }
          
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let CellIdentifier:String = "mycell";
        var cell: SearchUserFirstCell? = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? SearchUserFirstCell
            if cell == nil {
                autoreleasepool {
                    cell = SearchUserFirstCell(style: .default, reuseIdentifier: CellIdentifier)
                }
            }
         if searchActive == true{
//             let objdata:getMyDocumentData = allFilterData[indexPath.row]
//
//             cell?.lblTitle.text = String(describing: objdata.name!)
//              let checkWaiting:String = String(describing: objdata.is_signed!)
//              if checkWaiting == "0"{
//                  cell?.lblWaitingText.isHidden = false
//              }else{
//                  cell?.lblWaitingText.isHidden = true
//              }
//             cell?.addSubview((cell?.viewCell)!)

         }else{
//             let objdata:getMyDocumentData = arrMyDocumentData[indexPath.row]
//
//             cell?.lblTitle.text = String(describing: objdata.name!)
//              let checkWaiting:String = String(describing: objdata.is_signed!)
//              if checkWaiting == "0"{
//                  cell?.lblWaitingText.isHidden = false
//              }else{
//                  cell?.lblWaitingText.isHidden = true
//              }
//             cell?.addSubview((cell?.viewCell)!)

         }
         cell?.btnCross.isHidden = false
         cell?.btnFollow.isHidden = true
        cell?.selectionStyle = .none
        cell?.backgroundColor = UIColor.clear
        return cell!
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if searchActive == true{
//            let objdata:getMyDocumentData = allFilterData[indexPath.row]
//            strDocID = String(describing: objdata.document_id!)
//            strImageURl = String(describing: objdata.document!)
//            self.popUpDocument()
        }else{
//            let objdata:getMyDocumentData = arrMyDocumentData[indexPath.row]
//            strDocID = String(describing: objdata.document_id!)
//            strImageURl = String(describing: objdata.document!)
//            self.popUpDocument()
        }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchAllTabsVC") as! SearchAllTabsVC
        self.navigationController?.pushViewController(vc, animated: true)

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let viewHeaderCell = UIView()
        viewHeaderCell.frame = CGRect(x: 10, y: 0, width: screenWidth, height: 40)
        viewHeaderCell.backgroundColor = .black
        view.addSubview(viewHeaderCell)

        let lblRecent = UILabel()
        lblRecent.frame = CGRect(x: 5, y: 0, width: 80, height: 40)
        lblRecent.backgroundColor = UIColor.clear
        lblRecent.text = "Recent"
        lblRecent.textColor = .white
        lblRecent.textAlignment = .left
        lblRecent.clipsToBounds = true
        viewHeaderCell.addSubview(lblRecent)

        let btnCancel = UIButton()
        btnCancel.frame = CGRect(x: viewHeaderCell.frame.size.width-80, y: 0, width: 80, height: 40)
        btnCancel.backgroundColor = UIColor.clear
        btnCancel.setTitle("See All", for: .normal)
        btnCancel.titleLabel?.font = fontSizeRegulor
        btnCancel.setTitleColor(colorGray, for: .normal)
        viewHeaderCell.addSubview(btnCancel)

        return viewHeaderCell
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return view
    }

}
