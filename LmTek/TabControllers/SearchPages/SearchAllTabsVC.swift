//
//  SearchAllTabsVC.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 12/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit
import Alamofire

@available(iOS 13.0, *)
class SearchAllTabsVC: BaseViewController,UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    var yRef: CGFloat = 42.0
    var searchUsers = UISearchBar()
    var tblUsers: UITableView = UITableView()
    var tblHastag: UITableView = UITableView()
    var searchActive:Bool = false
    
    var btnAll:UIButton = UIButton()
    var btnUsers:UIButton = UIButton()
    var btnVideos:UIButton = UIButton()
    var btnHashtag:UIButton = UIButton()
    var lblYellowLine:UILabel = UILabel()
    var checkTab:String = "all"
    
    var ProductCollectionview:UICollectionView!
    var cellId = "Cell"
    
    var videosCollectionView:UICollectionView!

    var viewScroll: UIScrollView = UIScrollView()
    
    var arrAllUsersHastags: NSMutableArray = NSMutableArray()
    var dictAllSearchData: [String: Any]?
    
    var videosArray = [GetVideosData]()
    var strSearchKey: String = ""
    var lblBGLine:UILabel = UILabel()
    var viewAllVideosUH:AllVideosUserHastagView = AllVideosUserHastagView()
    var checkFirst: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        self.screenDesigning()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    func screenDesigning() {
        
        searchUsers = UISearchBar()
        searchUsers.frame = CGRect(x: 10, y: yRef, width: screenWidth-100, height: 40)
        searchUsers.backgroundColor = .clear
        searchUsers.placeholder = " Search Users, Hastags"
        searchUsers.barTintColor = .clear
        searchUsers.searchTextField.font = fontSizeRegulor
        searchUsers.searchTextField.textColor = .white
        searchUsers.isUserInteractionEnabled = true
        searchUsers.delegate = self
        searchUsers.layer.cornerRadius = 4
        searchUsers.showsCancelButton = false
        searchUsers.clipsToBounds = true
        self.view.addSubview(searchUsers)

        searchUsers.becomeFirstResponder()
        
        let btnCancel = UIButton()
        btnCancel.frame = CGRect(x: screenWidth-80, y: yRef, width: 70, height: 40)
        btnCancel.backgroundColor = UIColor.clear
        btnCancel.setTitle("Cancel", for: .normal)
        btnCancel.titleLabel?.font = fontSizeSemiBoldTitle
        btnCancel.setTitleColor(colorYellow, for: .normal)
        btnCancel.addTarget(self, action: #selector(self.btnCancelClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnCancel)
        
        yRef = yRef+searchUsers.frame.size.height + 15
        
        btnAll = UIButton()
        btnAll.frame = CGRect(x: 0, y: yRef, width: screenWidth/4, height: 30)
        btnAll.setTitle("All", for: .normal)
        btnAll.setTitleColor(colorYellow, for: .normal)
        btnAll.titleLabel?.font = fontSizeSemiBoldTitle
        btnAll.addTarget(self, action: #selector(self.btnAllClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnAll)
        
        btnUsers = UIButton()
        btnUsers.frame = CGRect(x: screenWidth/4, y: yRef, width: screenWidth/4, height: 30)
        btnUsers.setTitle("USERS", for: .normal)
        btnUsers.setTitleColor(.white, for: .normal)
        btnUsers.titleLabel?.font = fontSizeSemiBoldTitle
        btnUsers.addTarget(self, action: #selector(self.btnUsersClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnUsers)

        btnVideos = UIButton()
        btnVideos.frame = CGRect(x: (screenWidth/4)*2, y: yRef, width: screenWidth/4, height: 30)
        btnVideos.setTitle("VIDEOS", for: .normal)
        btnVideos.setTitleColor(.white, for: .normal)
        btnVideos.titleLabel?.font = fontSizeSemiBoldTitle
        btnVideos.addTarget(self, action: #selector(self.btnVideosClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnVideos)

        btnHashtag = UIButton()
        btnHashtag.frame = CGRect(x: (screenWidth/4)*3, y: yRef, width: screenWidth/4, height: 30)
        btnHashtag.setTitle("HASHTAG", for: .normal)
        btnHashtag.setTitleColor(.white, for: .normal)
        btnHashtag.titleLabel?.font = fontSizeSemiBoldTitle
        btnHashtag.addTarget(self, action: #selector(self.btnHashtagClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnHashtag)

        yRef = yRef+btnHashtag.frame.size.height + 10
                
        lblBGLine = UILabel()
        lblBGLine.frame = CGRect(x: 10, y: yRef, width: screenWidth, height: 1)
        lblBGLine.backgroundColor = colorGrayBG
        lblBGLine.clipsToBounds = true
        self.view.addSubview(lblBGLine)

        lblYellowLine = UILabel()
        lblYellowLine.frame = CGRect(x: 10, y: yRef-0.5, width: screenWidth/4, height: 2)
        lblYellowLine.backgroundColor = colorYellow
        lblYellowLine.clipsToBounds = true
        self.view.addSubview(lblYellowLine)

        
        yRef = yRef+lblYellowLine.frame.size.height + 2
        self.designingTab()
        
    }
    
    func designingTab() {
        viewScroll.removeFromSuperview()
        viewScroll = UIScrollView()
        viewScroll.frame = CGRect(x: 0, y: yRef, width: screenWidth, height: screenHeight-yRef)
        viewScroll.backgroundColor = UIColor.clear
        viewScroll.isPagingEnabled = true
        viewScroll.delegate = self
        viewScroll.clipsToBounds = true
        self.view.addSubview(viewScroll)
        
        var xRef:CGFloat = 0
        
        for i in 0..<4 {

            if i == 0 {
                viewAllVideosUH = AllVideosUserHastagView()
                viewAllVideosUH.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight-(yRef+40))
                viewAllVideosUH.dictAllSearchData = dictAllSearchData
                viewAllVideosUH.delegate = self
                viewAllVideosUH.viewDesigning()
                viewScroll.addSubview(viewAllVideosUH)
                
                if checkFirst == false {
                    btnAll.isHidden = true
                    btnUsers.isHidden = true
                    btnVideos.isHidden = true
                    btnHashtag.isHidden = true
                    lblYellowLine.isHidden = true
                    lblBGLine.isHidden = true
                    viewScroll.isHidden = true
                    checkFirst = true
                } else {
                    btnAll.isHidden = false
                    btnUsers.isHidden = false
                    btnVideos.isHidden = false
                    btnHashtag.isHidden = false
                    lblYellowLine.isHidden = false
                    lblBGLine.isHidden = false
                    viewScroll.isHidden = false

                }
            }
            else if i == 1 {
                tblUsers = UITableView()
                tblUsers.frame = CGRect(x: xRef, y: 0, width: screenWidth, height: screenHeight-yRef)
                tblUsers.backgroundColor = UIColor.black
                tblUsers.delegate = self
                tblUsers.dataSource = self
                tblUsers.separatorStyle = .none
                viewScroll.addSubview(tblUsers)
            } else if i == 2 {
                let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
                layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
                layout.itemSize = CGSize(width: (screenWidth/2)-10, height: 90)

                videosCollectionView = UICollectionView(frame: CGRect(x: xRef, y: 0, width: screenWidth, height: screenHeight-(yRef+40)), collectionViewLayout: layout)
                videosCollectionView.dataSource = self
                videosCollectionView.delegate = self
                videosCollectionView.register(AllUsersCollectionCell.self, forCellWithReuseIdentifier: cellId)
                videosCollectionView.showsVerticalScrollIndicator = false
                videosCollectionView.backgroundColor = UIColor.clear
                viewScroll.addSubview(videosCollectionView)

            } else if i == 3 {
                tblHastag = UITableView()
                tblHastag.frame = CGRect(x: xRef, y: 0, width: screenWidth, height: screenHeight-yRef)
                tblHastag.backgroundColor = UIColor.black
                tblHastag.delegate = self
                tblHastag.dataSource = self
                tblHastag.separatorStyle = .none
                viewScroll.addSubview(tblHastag)
            }
            
            xRef = xRef+screenWidth
        }
        viewScroll.contentSize = CGSize(width: xRef, height: screenHeight-yRef)
    }
    
    //MARK: Action
    
    @objc func btnCancelClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func btnAllClicked(_ sender: UIButton) {
        checkTab = "all"
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations: {
            self.lblYellowLine.frame = CGRect(x: 10, y: 137-0.5, width: screenWidth/4, height: 2)
            self.btnAll.setTitleColor(colorYellow, for: .normal)
            self.btnUsers.setTitleColor(.white, for: .normal)
            self.btnVideos.setTitleColor(.white, for: .normal)
            self.btnHashtag.setTitleColor(.white, for: .normal)
            self.viewScroll.contentOffset = CGPoint(x: 0, y: 0)
        }, completion: nil)
    }
    
    @objc func btnUsersClicked(_ sender: UIButton) {
        checkTab = "users"
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations: {
            self.lblYellowLine.frame = CGRect(x: screenWidth/4, y: 137-0.5, width: screenWidth/4, height: 2)
            self.btnUsers.setTitleColor(colorYellow, for: .normal)
            self.btnAll.setTitleColor(.white, for: .normal)
            self.btnVideos.setTitleColor(.white, for: .normal)
            self.btnHashtag.setTitleColor(.white, for: .normal)
            self.viewScroll.contentOffset = CGPoint(x: screenWidth, y: 0)
        }, completion: nil)

        self.getAllSearchAPI(strSearchKey)
    }
    
    @objc func btnVideosClicked(_ sender: UIButton) {
        checkTab = "videos"
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations: {
            self.lblYellowLine.frame = CGRect(x: (screenWidth/4)*2, y: 137-0.5, width: screenWidth/4, height: 2)
            self.btnVideos.setTitleColor(colorYellow, for: .normal)
            self.btnAll.setTitleColor(.white, for: .normal)
            self.btnUsers.setTitleColor(.white, for: .normal)
            self.btnHashtag.setTitleColor(.white, for: .normal)
            self.viewScroll.contentOffset = CGPoint(x: screenWidth*2, y: 0)
        }, completion: nil)
        
        self.getAllSearchAPI(strSearchKey)
    }
    
    @objc func btnHashtagClicked(_ sender: UIButton) {
        checkTab = "hashtag"
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations: {
            self.lblYellowLine.frame = CGRect(x: (screenWidth/4)*3, y: 137-0.5, width: screenWidth/4, height: 2)
            self.btnHashtag.setTitleColor(colorYellow, for: .normal)
            self.btnAll.setTitleColor(.white, for: .normal)
            self.btnUsers.setTitleColor(.white, for: .normal)
            self.btnVideos.setTitleColor(.white, for: .normal)
            self.viewScroll.contentOffset = CGPoint(x: screenWidth*3, y: 0)

        }, completion: nil)
        self.getAllSearchAPI(strSearchKey)
        
    }
    //MARK: SearchviewDelegate
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
        let ViewForDoneButtonOnKeyboard = UIToolbar()
        ViewForDoneButtonOnKeyboard.sizeToFit()
        let btnDoneOnKeyboard = UIBarButtonItem(title: "Done", style: .bordered, target: self, action: #selector(self.doneBtnFromKeyboardClicked))
        ViewForDoneButtonOnKeyboard.items = [btnDoneOnKeyboard]
        searchUsers.inputAccessoryView = ViewForDoneButtonOnKeyboard
        tblUsers.isHidden = true
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        //searchActive = false
        //tblSearch.isHidden = true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        //searchActive = false
        searchBar.text = nil
        searchBar.resignFirstResponder()
        tblUsers.resignFirstResponder()
        tblUsers.reloadData()

    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //searchActive = false
        searchBar.resignFirstResponder()
    }
    func filterContent(forSearchText searchText: String?, scope: String?) {
        var recipes:NSArray = NSArray()
        var searchResults:NSArray = NSArray()

        let resultPredicate = NSPredicate(format: "name contains[c] %@", searchText ?? "")
        searchResults = recipes.filtered(using: resultPredicate) as NSArray
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        var searchkey: String = searchText
        searchkey = searchkey.removeWhitespace()
        if searchkey == "" || searchkey.isEmpty == true {
            btnAll.isHidden = true
            btnUsers.isHidden = true
            btnVideos.isHidden = true
            btnHashtag.isHidden = true
            lblYellowLine.isHidden = true
            lblBGLine.isHidden = true
            viewScroll.isHidden = true
            searchActive = false
        } else {
            btnAll.isHidden = false
            btnUsers.isHidden = false
            btnVideos.isHidden = false
            btnHashtag.isHidden = false
            lblYellowLine.isHidden = false
            lblBGLine.isHidden = false
            viewScroll.isHidden = false
            strSearchKey = searchText
            searchActive = true
            self.getAllSearchAPI(searchText)
        }
    }
    @objc func doneBtnFromKeyboardClicked()
    {
        self.becomeFirstResponder()
        self.view.endEditing(true)
        tblUsers.isHidden = false
    }
    //MARK: Action
    func reload(tableView: UITableView) {

        let contentOffset = tableView.contentOffset
        tableView.reloadData()
        tableView.layoutIfNeeded()
        tableView.setContentOffset(contentOffset, animated: false)
        
    }
    @objc func btnFollowClicked(_ sender: UIButton) {
        let objdata: GetHastagData = arrAllUsersHastags.object(at: sender.tag) as! GetHastagData
        if checkTab == "users" {
            if String(describing: objdata.is_following ?? "") == "0" {
                self.doFollowUserAPI(String(describing: objdata.id ?? ""), is_following: "1")
            } else {
                self.doFollowUserAPI(String(describing: objdata.id ?? ""), is_following: "0")
            }
        }
    }
    //MARK: TableviewDelegate
       
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAllUsersHastags.count
    }
          
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let objdata: GetHastagData = arrAllUsersHastags.object(at: indexPath.row) as! GetHastagData

        let CellIdentifier:String = "mycell"+String(indexPath.row);
        var cell: SearchUserFirstCell? = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? SearchUserFirstCell
        if cell == nil {
            autoreleasepool {
                cell = SearchUserFirstCell(style: .default, reuseIdentifier: CellIdentifier)
            }
        }
        
        cell?.btnCross.isHidden = true
        cell?.lblUserName.text = String(describing: objdata.username ?? "")
        if checkTab == "users" {
            if String(describing: objdata.is_following ?? "") == "0" {
                cell?.btnFollow.setTitle("Follow", for: .normal)
            } else {
                cell?.btnFollow.setTitle("Following", for: .normal)
            }
            cell?.lblMessage.text = String(describing: objdata.name ?? "")
            cell?.btnFollow.tag = indexPath.row
            cell?.btnFollow.addTarget(self, action: #selector(self.btnFollowClicked(_:)), for: .touchUpInside)
            cell?.lblHastag.isHidden = true
            let strurl:String = objdata.profile_picture as! String
            let url = URL(string: strurl);
            if (url != nil)
            {
                cell?.imgUser.sd_setImage(with: url, placeholderImage: UIImage.init(named: ""))
            }
        }
        
        if checkTab == "hashtag" {
            cell?.btnFollow.isHidden = true
            cell?.imgUser.isHidden = true
            cell?.lblMessage.text = String(describing: objdata.name ?? "")
        }
        
        cell?.selectionStyle = .none
        cell?.backgroundColor = UIColor.clear
        cell?.addSubview(cell!.viewCell)
        return cell!
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("index")
        
        if checkTab == "users"{
            let objdata: GetHastagData = arrAllUsersHastags.object(at: indexPath.row) as! GetHastagData

            let userProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "UserProfileVC") as! UserProfileVC
            userProfileVC.isFollowing = (objdata.is_following as? String == "1") ? true : false
            userProfileVC.videoIndex = indexPath.item
            userProfileVC.user_id = String(describing: objdata.user_id ?? "")
            self.navigationController?.pushViewController(userProfileVC, animated: true)
        }
        
        if checkTab == "hashtag" {
            let objdata: GetHastagData = arrAllUsersHastags.object(at: indexPath.row) as! GetHastagData

            let userProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "HastagProfileVC") as! HastagProfileVC
            userProfileVC.videoIndex = indexPath.item
            userProfileVC.strSearchkey = String(describing: objdata.name ?? "")
            userProfileVC.userID = String(describing: objdata.id ?? "")
            self.navigationController?.pushViewController(userProfileVC, animated: true)

        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return view
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return view
    }
    //MARK: CollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videosArray.count
    }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)  as! AllUsersCollectionCell
        
        let objdata: GetVideosData = videosArray[indexPath.row]
        
        let strurl:String = objdata.thumb_url as! String
        let url = URL(string: strurl);
        if (url != nil)
        {
            Cell.imgDraw.clipsToBounds = true
            Cell.imgDraw.sd_setImage(with: url, placeholderImage: UIImage.init(named: ""))
        }
        Cell.lblDescription.text = String(describing: objdata.caption ?? "")
        Cell.lblRating.text = String(describing: objdata.likes ?? "")
        return Cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        let playVideoVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        playVideoVC.checkSearchVideo = true
        playVideoVC.videosArray = videosArray
        playVideoVC.checkVideoTag = indexPath.row
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(playVideoVC, animated: true)
        }

    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemSize = ((collectionView.frame.width/2.0)-10)
            return CGSize(width: itemSize, height: 195)
    }
    //MARK: scrollview Delegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == viewScroll && scrollView != ProductCollectionview {
            if viewScroll.contentOffset == CGPoint(x: 0, y: 0) {
                checkTab = "all"
                UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations: {
                    self.lblYellowLine.frame = CGRect(x: 10, y: 137-0.5, width: screenWidth/4, height: 2)
                    self.btnAll.setTitleColor(colorYellow, for: .normal)
                    self.btnUsers.setTitleColor(.white, for: .normal)
                    self.btnVideos.setTitleColor(.white, for: .normal)
                    self.btnHashtag.setTitleColor(.white, for: .normal)
                }, completion: nil)
                
                self.getAllSearchAPI(strSearchKey)

            } else if viewScroll.contentOffset == CGPoint(x: screenWidth, y: 0) {
                checkTab = "users"
                UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations: {
                    self.lblYellowLine.frame = CGRect(x: screenWidth/4, y: 137-0.5, width: screenWidth/4, height: 2)
                    self.btnUsers.setTitleColor(colorYellow, for: .normal)
                    self.btnAll.setTitleColor(.white, for: .normal)
                    self.btnVideos.setTitleColor(.white, for: .normal)
                    self.btnHashtag.setTitleColor(.white, for: .normal)
                }, completion: nil)
                self.getAllSearchAPI(strSearchKey)
                
            } else if viewScroll.contentOffset == CGPoint(x: screenWidth*2, y: 0) {
                checkTab = "videos"
                UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations: {
                    self.lblYellowLine.frame = CGRect(x: (screenWidth/4)*2, y: 137-0.5, width: screenWidth/4, height: 2)
                    self.btnVideos.setTitleColor(colorYellow, for: .normal)
                    self.btnAll.setTitleColor(.white, for: .normal)
                    self.btnUsers.setTitleColor(.white, for: .normal)
                    self.btnHashtag.setTitleColor(.white, for: .normal)
                }, completion: nil)
                
                self.getAllSearchAPI(strSearchKey)

            } else if viewScroll.contentOffset == CGPoint(x: screenWidth*3, y: 0) {
                checkTab = "hashtag"
                UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations: {
                    self.lblYellowLine.frame = CGRect(x: (screenWidth/4)*3, y: 137-0.5, width: screenWidth/4, height: 2)
                    self.btnHashtag.setTitleColor(colorYellow, for: .normal)
                    self.btnAll.setTitleColor(.white, for: .normal)
                    self.btnUsers.setTitleColor(.white, for: .normal)
                    self.btnVideos.setTitleColor(.white, for: .normal)

                }, completion: nil)
                self.getAllSearchAPI(strSearchKey)
            }
        }
    }
    //MARK: Function
    
    func updateUI() {
        
    }
    //MARK: APIS
    
    func getAllSearchAPI(_ searchKey: String) {
        let url = URL(string: Constants.BASE_URL+Constants.SEARCH_DISCOVERY)!
        let headers: HTTPHeaders = ["Authentication": User.sharedInstance.auth_key ?? ""]
        let params = ["search_key" : searchKey]
        print(String(User.sharedInstance.auth_key ?? ""))
        DialougeUtils.addActivityView(view: self.view)
        self.getAllSearchAPICall(url, header: headers, param: params)
    }
    
    func getAllSearchAPICall(_ url: URL, header: HTTPHeaders, param: Parameters){
        AllUserApis.allPostDataWithoutImageAPI(vc: self, url: url, parameter: param, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            if self.videosArray.count > 0 {
                                self.videosArray.removeAll()
                            }
                            let allData: [String : Any] = result!["user_data"] as! [String : Any]
                            self.dictAllSearchData = allData
                            
                            let hastagData: [[String : Any]] = allData["hashtags"] as! [[String : Any]]
                            let videosData: [[String : Any]] = allData["videos"] as! [[String : Any]]
                            let userData: [[String : Any]] = allData["users"] as! [[String : Any]]
                            
                            if self.checkTab == "hashtag" {
                                if self.arrAllUsersHastags.count > 0 {
                                    self.arrAllUsersHastags.removeAllObjects()
                                }
                                for i in 0..<hastagData.count {
                                    let dictdata: [String : Any] = hastagData[i]
                                    let objHasdata: GetHastagData = GetHastagData.init(VideoInfo: dictdata)!
                                    self.arrAllUsersHastags.add(objHasdata)
                                }
                            }
                            if self.checkTab == "users" {
                                if self.arrAllUsersHastags.count > 0 {
                                    self.arrAllUsersHastags.removeAllObjects()
                                }
                                
                                for i in 0..<userData.count {
                                    let dictdata: [String : Any] = userData[i]
                                    let objHasdata: GetHastagData = GetHastagData.init(VideoInfo: dictdata)!
                                    self.arrAllUsersHastags.add(objHasdata)
                                }
                            }
                            for i in 0..<videosData.count {
                                let dictdata: [String : Any] = videosData[i]
                                let objdata: GetVideosData = GetVideosData.init(VideoInfo: dictdata)!
                                self.videosArray.append(objdata)
                            }
                            
                            if self.checkTab == "all" {
                                self.designingTab()
                            } else if self.checkTab == "hashtag" {
                                self.tblHastag.reloadData()
                            } else if self.checkTab == "users" {
                                self.reload(tableView: self.tblUsers)
                            } else if self.checkTab == "videos" {
                                self.videosCollectionView.reloadData()
                            } else {
                                self.designingTab()
                            }
                            
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                    DialougeUtils.removeActivityView(view: self.view)
                }
            }
        }
    }
    
    func doFollowUserAPI(_ follower_user_id: String,is_following: String ) {
        let url = URL(string: Constants.BASE_URL+Constants.FOLLOW_USER)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        print(headers)
        let param: Parameters = ["follower_user_id": follower_user_id]
        print(param)
        DialougeUtils.addActivityView(view: self.view)
        self.doFollowUserAPICall(param, url: url, header: headers,follower_user_id: follower_user_id, is_following: is_following)
    }
    
    func doFollowUserAPICall(_ param: Parameters, url: URL, header: HTTPHeaders,follower_user_id: String, is_following: String) {
        AllUserApis.allPostDataWithoutImageAPI(vc: self, url: url, parameter: param, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status: Bool = result!["status"] as! Bool
                        if status == true {
//                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.PROFILE_UPDATE_NOTIFICATION), object: nil)
                            self.getAllSearchAPI(self.strSearchKey)
                        }
                    } else {
                    }
                }
            }
        }
    }
}

extension SearchAllTabsVC: funcAllSearchData {
    func videoClick(_ videodata: [GetVideosData], index: Int) {
        print("videodata")
        let playVideoVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        playVideoVC.checkSearchVideo = true
        playVideoVC.videosArray = videodata
        playVideoVC.checkVideoTag = index
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(playVideoVC, animated: true)
        }
    }
    func allVideoClick(_ videodata: [GetVideosData]) {
        print("all video")
        checkTab = "videos"
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations: {
            self.lblYellowLine.frame = CGRect(x: (screenWidth/4)*2, y: 137-0.5, width: screenWidth/4, height: 2)
            self.btnVideos.setTitleColor(colorYellow, for: .normal)
            self.btnAll.setTitleColor(.white, for: .normal)
            self.btnUsers.setTitleColor(.white, for: .normal)
            self.btnHashtag.setTitleColor(.white, for: .normal)
            self.viewScroll.contentOffset = CGPoint(x: screenWidth*2, y: 0)
        }, completion: nil)
        
        self.getAllSearchAPI(strSearchKey)
    }
        
    func allUserdata() {
        checkTab = "users"
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations: {
            self.lblYellowLine.frame = CGRect(x: screenWidth/4, y: 137-0.5, width: screenWidth/4, height: 2)
            self.btnUsers.setTitleColor(colorYellow, for: .normal)
            self.btnAll.setTitleColor(.white, for: .normal)
            self.btnVideos.setTitleColor(.white, for: .normal)
            self.btnHashtag.setTitleColor(.white, for: .normal)
            self.viewScroll.contentOffset = CGPoint(x: screenWidth, y: 0)
        }, completion: nil)

        self.getAllSearchAPI(strSearchKey)
    }
    
    func userData(_ userdata: GetHastagData, index: Int) {
        let userProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "UserProfileVC") as! UserProfileVC
        userProfileVC.isFollowing = (userdata.is_following as? String == "1") ? true : false
        userProfileVC.videoIndex = index
        userProfileVC.user_id = String(describing: userdata.user_id ?? "")
        self.navigationController?.pushViewController(userProfileVC, animated: true)
    }
    
    func allhastagdata() {
        checkTab = "hashtag"
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations: {
            self.lblYellowLine.frame = CGRect(x: (screenWidth/4)*3, y: 137-0.5, width: screenWidth/4, height: 2)
            self.btnHashtag.setTitleColor(colorYellow, for: .normal)
            self.btnAll.setTitleColor(.white, for: .normal)
            self.btnUsers.setTitleColor(.white, for: .normal)
            self.btnVideos.setTitleColor(.white, for: .normal)
            self.viewScroll.contentOffset = CGPoint(x: screenWidth*3, y: 0)

        }, completion: nil)
        self.getAllSearchAPI(strSearchKey)
    }
        
    func hastagClicked(_ hastagdata: GetHastagData, index: Int) {
        let userProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "HastagProfileVC") as! HastagProfileVC
        userProfileVC.videoIndex = index
        userProfileVC.strSearchkey = String(describing: hastagdata.name ?? "")
        userProfileVC.userID = String(describing: hastagdata.id ?? "")
        self.navigationController?.pushViewController(userProfileVC, animated: true)

    }
    
}
