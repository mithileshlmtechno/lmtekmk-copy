//
//  AllVideosUserHastagView.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 08/08/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit

protocol funcAllSearchData {
    func allVideoClick(_ videodata: [GetVideosData])
    func videoClick(_ videodata: [GetVideosData], index: Int)
    func allUserdata()
    func userData(_ userdata: GetHastagData, index: Int)
    func allhastagdata()
    func hastagClicked(_ hastagdata: GetHastagData, index: Int)
}
class AllVideosUserHastagView: UIView, UITableViewDelegate, UITableViewDataSource {
    
    var delegate: funcAllSearchData?
    
    var tblAllSearch:UITableView = UITableView()
    var dictAllSearchData: [String: Any]?
    var widthV: CGFloat = (screenWidth/2)-15
    var numberRow: Int = 0
    var rowNumber: Int = 0
    var extraRowHeight: Int = 0
    
    var arrAllHastags: NSMutableArray = NSMutableArray()
    var arrAllUsers: NSMutableArray = NSMutableArray()
    var videosArray = [GetVideosData]()

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Action
    
    func viewDesigning() {
        let hastagData: [[String : Any]] = dictAllSearchData?["hashtags"] as? [[String : Any]] ?? []
        let videosData: [[String : Any]] = dictAllSearchData?["videos"] as? [[String : Any]] ?? []
        let userData: [[String : Any]] = dictAllSearchData?["users"] as? [[String : Any]] ?? []
        
        for i in 0..<hastagData.count {
            let dictdata: [String : Any] = hastagData[i]
            let objHasdata: GetHastagData = GetHastagData.init(VideoInfo: dictdata)!
            self.arrAllHastags.add(objHasdata)
        }
        for i in 0..<userData.count {
            let dictdata: [String : Any] = userData[i]
            let objHasdata: GetHastagData = GetHastagData.init(VideoInfo: dictdata)!
            self.arrAllUsers.add(objHasdata)
        }
        if videosArray.count > 0 {
            videosArray.removeAll()
        }
        for i in 0..<videosData.count {
            let dictdata: [String : Any] = videosData[i]
            let objdata: GetVideosData = GetVideosData.init(VideoInfo: dictdata)!
            self.videosArray.append(objdata)
        }

        numberRow = videosArray.count
        
        rowNumber = numberRow/2
        let numberM = numberRow%2
        rowNumber = rowNumber+numberM
        if rowNumber > 8 {
            extraRowHeight = 150
        } else if rowNumber < 8 {
            extraRowHeight = 140
        }
        tblAllSearch.removeFromSuperview()
        tblAllSearch = UITableView(frame: CGRect(x: 0, y:0, width:screenWidth, height:screenHeight-140), style: .grouped)
        tblAllSearch.delegate = self
        tblAllSearch.dataSource = self
        tblAllSearch.separatorColor = UIColor(red: 46/255, green: 69/255, blue: 134/255, alpha: 0.5)
        tblAllSearch.separatorStyle = .none
        tblAllSearch.backgroundColor = UIColor.clear
        tblAllSearch.showsVerticalScrollIndicator = false
        self.addSubview(tblAllSearch)
    }
    
    // MARK: - UITableView delegates
        
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return (widthV*CGFloat(rowNumber))+CGFloat(extraRowHeight)
        } else {
            return 50
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        if section == 0 {
            let viewHeader: UIView = UIView()
            viewHeader.frame = CGRect(x: 0, y: 0, width: screenWidth, height: (widthV*CGFloat(rowNumber))+CGFloat(extraRowHeight))
            viewHeader.backgroundColor = UIColor.clear
            self.addSubview(viewHeader)
            var xRef:CGFloat = 10
            var yRef:CGFloat = 50
            
            for i in 0..<numberRow {
                let objdata: GetVideosData = videosArray[i]

                let viewVideo:UIView = UIView()
                viewVideo.frame = CGRect(x: xRef, y: yRef, width: widthV, height: widthV)
                viewVideo.backgroundColor = .clear
                viewHeader.addSubview(viewVideo)
                
                let sLayer:CAShapeLayer = CAShapeLayer()
                let imgDraw = UIImageView()
                imgDraw.frame = CGRect(x: 0, y: 0, width: viewVideo.frame.size.width, height: viewVideo.frame.size.height)
                imgDraw.ShaplayerUp(sLayer, corner: 1)
                let strurl:String = objdata.thumb_url as! String
                let url = URL(string: strurl);
                if (url != nil)
                {
                    imgDraw.clipsToBounds = true
                    imgDraw.sd_setImage(with: url, placeholderImage: UIImage.init(named: ""))
                }
                imgDraw.isUserInteractionEnabled = true
                imgDraw.contentMode = .scaleAspectFill
                imgDraw.clipsToBounds = true
                viewVideo.addSubview(imgDraw)
                
                let imgVideoIcon:UIImageView = UIImageView()
                imgVideoIcon.frame = CGRect(x: viewVideo.frame.size.width-25, y: 10, width: 15, height: 15)
                imgVideoIcon.image = UIImage.init(named: "video-Icon")
                viewVideo.addSubview(imgVideoIcon)

                let btnView = UIButton()
                btnView.frame = CGRect(x: 0, y: 0, width: viewVideo.frame.size.width, height: viewVideo.frame.size.height)
                btnView.clipsToBounds = true
                btnView.tag = i
                btnView.addTarget(self, action: #selector(self.btnvideoClicked(_:)), for: .touchUpInside)
                viewVideo.addSubview(btnView)

                xRef = xRef+viewVideo.frame.size.width + 10
                if (i+1) % 2 == 0 {
                    yRef = yRef+viewVideo.frame.size.height + 10
                    xRef = 10
                }
            }
            let lblVideos:UILabel = UILabel()
            lblVideos.frame = CGRect(x: 10, y: 0, width: 100, height: 30)
            lblVideos.text = "Video"
            lblVideos.font = fontSizeRegulor
            lblVideos.textColor = .white
            lblVideos.clipsToBounds = true
            viewHeader.addSubview(lblVideos)
            
            let btnMore:UIButton = UIButton()
            btnMore.frame = CGRect(x: screenWidth-70, y: 0, width: 50, height: 30)
            btnMore.setTitle("More", for: .normal)
            btnMore.titleLabel?.font = fontSizeRegulor
            btnMore.setTitleColor(.white, for: .normal)
            btnMore.addTarget(self, action: #selector(self.btnMorevideoClicked(_:)), for: .touchUpInside)
            btnMore.clipsToBounds = true
            
            viewHeader.addSubview(btnMore)

            let lblUser:UILabel = UILabel()
            lblUser.frame = CGRect(x: 10, y: viewHeader.frame.size.height-30, width: 100, height: 30)
            lblUser.text = "Users"
            lblUser.font = fontSizeRegulor
            lblUser.textColor = .white
            lblUser.clipsToBounds = true
            viewHeader.addSubview(lblUser)
            
            let btnUserMore:UIButton = UIButton()
            btnUserMore.frame = CGRect(x: screenWidth-70, y: viewHeader.frame.size.height-30, width: 50, height: 30)
            btnUserMore.setTitle("More", for: .normal)
            btnUserMore.titleLabel?.font = fontSizeRegulor
            btnUserMore.setTitleColor(.white, for: .normal)
            btnUserMore.clipsToBounds = true
            btnUserMore.addTarget(self, action: #selector(self.btnMoreUserClicked(_:)), for: .touchUpInside)
            viewHeader.addSubview(btnUserMore)
            return viewHeader
        } else {
            let viewHeader: UIView = UIView()
            viewHeader.frame = CGRect(x: 0, y: 0, width: screenWidth, height: 50)
            viewHeader.backgroundColor = UIColor.clear
            self.addSubview(viewHeader)
            
            let lblVideos:UILabel = UILabel()
            lblVideos.frame = CGRect(x: 10, y: 10, width: 100, height: 30)
            lblVideos.text = "Hastags"
            lblVideos.font = fontSizeRegulor
            lblVideos.textColor = .white
            lblVideos.clipsToBounds = true
            viewHeader.addSubview(lblVideos)
            
            let btnMore:UIButton = UIButton()
            btnMore.frame = CGRect(x: screenWidth-70, y: 10, width: 50, height: 30)
            btnMore.setTitle("More", for: .normal)
            btnMore.titleLabel?.font = fontSizeRegulor
            btnMore.setTitleColor(.white, for: .normal)
            btnMore.clipsToBounds = true
            btnMore.addTarget(self, action: #selector(self.btnHastagMoreClicked(_:)), for: .touchUpInside)
            viewHeader.addSubview(btnMore)

            return viewHeader
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return arrAllUsers.count
        } else {
            return arrAllHastags.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80 //UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let CellIdentifier:String = "mycell"+String(indexPath.row);
        var cell: SearchUserFirstCell? = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? SearchUserFirstCell
            if cell == nil {
                autoreleasepool {
                    cell = SearchUserFirstCell(style: .default, reuseIdentifier: CellIdentifier)
                }
            }
        if indexPath.section == 0 {
            let objdata: GetHastagData = arrAllUsers.object(at: indexPath.row) as! GetHastagData
            cell?.lblMessage.text = String(describing: objdata.name ?? "")
            let strurl:String = objdata.profile_picture as! String
            let url = URL(string: strurl);
            if (url != nil)
            {
                cell?.imgUser.sd_setImage(with: url, placeholderImage: UIImage.init(named: ""))
            }
            cell?.lblUserName.text = String(describing: objdata.username ?? "")
            cell?.lblHastag.isHidden = true
        } else {
            let objdata: GetHastagData = arrAllHastags.object(at: indexPath.row) as! GetHastagData
            cell?.btnFollow.isHidden = true
            cell?.imgUser.isHidden = true
            cell?.lblMessage.text = String(describing: objdata.name ?? "")
            cell?.lblUserName.text = String(describing: objdata.username ?? "")
        }
        cell?.clipsToBounds = true
        cell?.selectionStyle = .none
        cell?.backgroundColor = UIColor.clear
        cell?.addSubview((cell?.viewCell)!)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let objdata: GetHastagData = arrAllUsers.object(at: indexPath.row) as! GetHastagData
            delegate?.userData(objdata, index: indexPath.row)
        } else {
            let objdata: GetHastagData = arrAllHastags.object(at: indexPath.row) as! GetHastagData
            delegate?.hastagClicked(objdata, index: indexPath.row)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let viewHeader:UIView = UIView()
        return viewHeader
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
//MARK: Action
    
    @objc func btnvideoClicked(_ sender: UIButton) {
        delegate?.videoClick(videosArray, index: sender.tag)
    }
    
    @objc func btnMorevideoClicked(_ sender: UIButton) {
        delegate?.allVideoClick(videosArray)
    }
    @objc func btnHastagMoreClicked(_ sender: UIButton) {
        delegate?.allhastagdata()
    }
        
    @objc func btnMoreUserClicked(_ sender: UIButton) {
        delegate?.allUserdata()
    }

}
