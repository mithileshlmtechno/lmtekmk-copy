//
//  OnboardingModel.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 08/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import Foundation

class AuthUserLoginData: NSObject{
    
    var password:Any?
    var device_id:Any?
    var unique_id:Any?
    var email:Any?
    var phone:Any?
    var cover_photo:Any?
    var is_email:Any?
    var is_phone:Any?
    var first_name:Any?
    var last_name:Any?
    var dob:Any?
    var is_celebrity:Any?
    var insta_id:Any?
    var apple_id:Any?
    var facebook_id:Any?
    var google_id:Any?
    var user_type:Any?
    var login_type:Any?
    var role:Any?
    var added_on:Any?
    var updated_on:Any?
    var gender:Any?
    var is_private:Any?
    var country_code:Any?
    var referral_code:Any?
    var username:Any?
    var country_access:Any?
    var status:Any?
    var id:Any?
    var expire_time:Any?
    var refresh_token:Any?
    var profile_picture:Any?
    var auth_key:Any?

    init?(loginInfo: [String: Any]) {

        self.password = loginInfo["password"]
        self.device_id = loginInfo["device_id"]
        self.unique_id = loginInfo["unique_id"]
        self.email = loginInfo["email"]
        self.phone = loginInfo["phone"]
        self.cover_photo = loginInfo["cover_photo"]
        self.is_email = loginInfo["is_email"]
        self.is_phone = loginInfo["is_phone"]
        self.first_name = loginInfo["first_name"]
        self.last_name = loginInfo["last_name"]
        self.dob = loginInfo["dob"]
        self.is_celebrity = loginInfo["is_celebrity"]
        self.insta_id = loginInfo["insta_id"]
        self.apple_id = loginInfo["apple_id"]
        self.facebook_id = loginInfo["facebook_id"]
        self.google_id = loginInfo["google_id"]
        self.user_type = loginInfo["user_type"]
        self.login_type = loginInfo["login_type"]
        self.role = loginInfo["role"]
        self.added_on = loginInfo["added_on"]
        self.updated_on = loginInfo["updated_on"]
        self.gender = loginInfo["gender"]
        self.is_private = loginInfo["is_private"]
        self.country_code = loginInfo["country_code"]
        self.referral_code = loginInfo["referral_code"]
        self.username = loginInfo["username"]
        self.country_access = loginInfo["country_access"]
        self.status = loginInfo["status"]
        self.id = loginInfo["id"]
        self.expire_time = loginInfo["expire_time"]
        self.refresh_token = loginInfo["refresh_token"]
        self.profile_picture = loginInfo["profile_picture"]
        self.auth_key = loginInfo["auth_key"]

    }
}


class AuthUserGetOTPData: NSObject{
    
    var email_phone:Any?
    var country_code:Any?
    var otp:Any?

    init?(loginInfo: [String: Any]) {
        self.email_phone = loginInfo["email_phone"]
        self.country_code = loginInfo["country_code"]
        self.otp = loginInfo["otp"]
    }
}


class GetVideosData: NSObject{
    
    var allow_comment:Any?
    var allow_download:Any?
    var allow_duet:Any?
    var audio_caption:Any?
    var audio_id:Any?
    var audio_name:Any?
    var audio_url:Any?
    var caption:Any?
    var comments:Any?
    var created_at:Any?
    var downloads:Any?
    var first_name:Any?
    var id:Any?
    var isPlaying:Any?
    var is_commented:Any?
    var is_following:Any?
    var is_liked:Any?
    var is_user_verified:Any?
    var last_name:Any?
    var likes:Any?
    var playDuration:Any?
    var profile_picture:Any?
    var shares:Any?
    var status:Any?
    var thumb_url:Any?
    var total_following:Any?
    var updated_at:Any?
    var user_id:Any?
    var username:Any?
    var video_id: Any?
    var video_url:Any?
    var total_videos: Any?

    init?(VideoInfo: [String: Any]) {
        self.allow_comment = VideoInfo["allow_comment"]
        self.allow_download = VideoInfo["allow_download"]
        self.allow_duet = VideoInfo["allow_duet"]
        self.audio_caption = VideoInfo["audio_caption"]
        self.audio_id = VideoInfo["audio_id"]
        self.audio_name = VideoInfo["audio_name"]
        self.audio_url = VideoInfo["audio_url"]
        self.caption = VideoInfo["caption"]
        self.comments = VideoInfo["comments"]
        self.created_at = VideoInfo["created_at"]
        self.downloads = VideoInfo["downloads"]
        self.first_name = VideoInfo["first_name"]
        self.id = VideoInfo["id"]
        self.isPlaying = VideoInfo["isPlaying"]
        self.is_commented = VideoInfo["is_commented"]
        self.is_following = VideoInfo["is_following"]
        self.is_liked = VideoInfo["is_liked"]
        self.is_user_verified = VideoInfo["is_user_verified"]
        self.last_name = VideoInfo["last_name"]
        self.likes = VideoInfo["likes"]
        self.playDuration = VideoInfo["playDuration"]
        self.profile_picture = VideoInfo["profile_picture"]
        self.shares = VideoInfo["shares"]
        self.status = VideoInfo["status"]
        self.thumb_url = VideoInfo["thumb_url"]
        self.total_following = VideoInfo["total_following"]
        self.updated_at = VideoInfo["updated_at"]
        self.user_id = VideoInfo["user_id"]
        self.username = VideoInfo["username"]
        self.video_id = VideoInfo["video_id"]
        self.video_url = VideoInfo["video_url"]
        self.total_videos = VideoInfo["total_videos"]
    }
}


class GetHastagData: NSObject{
    
    var id:Any?
    var is_celebrity:Any?
    var is_following:Any?
    var name:Any?
    var profile_picture:Any?
    var type:Any?
    var user_id:Any?
    var username:Any?

    init?(VideoInfo: [String: Any]) {
        self.id = VideoInfo["id"]
        self.is_celebrity = VideoInfo["is_celebrity"]
        self.is_following = VideoInfo["is_following"]
        self.name = VideoInfo["name"]
        self.profile_picture = VideoInfo["profile_picture"]
        self.type = VideoInfo["type"]
        self.user_id = VideoInfo["user_id"]
        self.username = VideoInfo["username"]

    }
}


class GetSearchVideosData: NSObject{
    
    var type:Any?
    var video_url:Any?
    var thumb_url:Any?
    var id:Any?

    init?(VideoInfo: [String: Any]) {
        self.type = VideoInfo["type"]
        self.video_url = VideoInfo["video_url"]
        self.thumb_url = VideoInfo["thumb_url"]
        self.id = VideoInfo["id"]
    }
}
