//
//  HelperUser.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 09/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import Foundation


struct User {
    static var sharedInstance = User()
    let defaults = UserDefaults.standard
    var emial: String? {
        get {
            return defaults.string(forKey: "email")
        }
        set {
            defaults.setValue(newValue, forKey: "email")
        }
    }
    
    var unique_id: String? {
        get {
            return defaults.string(forKey: "unique_id")
        }
        set {
            defaults.setValue(newValue, forKey: "unique_id")
        }
    }

    var id: String? {
        get {
            return defaults.string(forKey: "id")
        }
        set {
            defaults.setValue(newValue, forKey: "id")
        }
    }
    
    var auth_key: String? {
        get {
            return defaults.string(forKey: "auth_key")
        }
        set {
            defaults.setValue(newValue, forKey: "auth_key")
        }
    }

    var username: String? {
        get {
            return defaults.string(forKey: "username")
        }
        set {
            return defaults.setValue(newValue, forKey: "username")
        }
    }
    
    var refresh_token: String? {
        get {
            return defaults.string(forKey: "refresh_token")
        }
        set {
            return defaults.setValue(newValue, forKey: "refresh_token")
        }
    }
    
    var login: Bool {
        get {
            return defaults.bool(forKey: "LOGIN")
        }
        set {
            return defaults.set(true, forKey: "LOGIN")
        }
    }
}

/*
 "password": "b1b3773a05c0ed0176787a4f1574ff0075f7521e",
 "device_id": "cghjkhjgvhh",
 "unique_id": "1653510232234",
 "email": "joy@gmail.com",
 "phone": "7003447713",
 "cover_photo": "http://lmtek.s3.amazonaws.com/uploads/cover_photo/lmtek-124-1653998054.png",
 "is_email": "",
 "is_phone": "1",
 "first_name": "Joy",
 "last_name": "Sinha",
 "dob": "2022-12-12",
 "is_celebrity": "0",
 "insta_id": "",
 "apple_id": "",
 "facebook_id": "",
 "google_id": "",
 "user_type": "2",
 "login_type": "",
 "role": "0",
 "added_on": "2022-05-26 01:53:52",
 "updated_on": "2022-05-26 01:53:52",
 "gender": "1",
 "is_private": "0",
 "country_code": "",
 "referral_code": "",
 "username": "joi7713",
 "country_access": "ALL",
 "status": 1,
 "id": "124",
 "expire_time": "2023-06-08 20:08:18",
 "refresh_token": "0962444b415bbf9f4493b7fe87b98da205f03e68",
 "profile_picture": "",
 "auth_key": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMy4yMzQuODEuMjRcLyIsIm5iZiI6MTY1NDc3MDM5NywiaWF0IjoxNjU0NzcwMzk3LCJwYXNzd29yZCI6ImIxYjM3NzNhMDVjMGVkMDE3Njc4N2E0ZjE1NzRmZjAwNzVmNzUyMWUiLCJkZXZpY2VfaWQiOiJjZ2hqa2hqZ3ZoaCIsInVuaXF1ZV9pZCI6IjE2NTM1MTAyMzIyMzQiLCJlbWFpbCI6ImpveUBnbWFpbC5jb20iLCJwaG9uZSI6IjcwMDM0NDc3MTMiLCJjb3Zlcl9waG90byI6Imh0dHA6XC9cL2xtdGVrLnMzLmFtYXpvbmF3cy5jb21cL3VwbG9hZHNcL2NvdmVyX3Bob3RvXC9sbXRlay0xMjQtMTY1Mzk5ODA1NC5wbmciLCJpc19lbWFpbCI6IiIsImlzX3Bob25lIjoiMSIsImZpcnN0X25hbWUiOiJKb3kiLCJsYXN0X25hbWUiOiJTaW5oYSIsImRvYiI6IjIwMjItMTItMTIiLCJpc19jZWxlYnJpdHkiOiIwIiwiaW5zdGFfaWQiOiIiLCJhcHBsZV9pZCI6IiIsImZhY2Vib29rX2lkIjoiIiwiZ29vZ2xlX2lkIjoiIiwidXNlcl90eXBlIjoiMiIsImxvZ2luX3R5cGUiOiIiLCJyb2xlIjoiMCIsImFkZGVkX29uIjoiMjAyMi0wNS0yNiAwMTo1Mzo1MiIsInVwZGF0ZWRfb24iOiIyMDIyLTA1LTI2IDAxOjUzOjUyIiwiZ2VuZGVyIjoiMSIsImlzX3ByaXZhdGUiOiIwIiwiY291bnRyeV9jb2RlIjoiIiwicmVmZXJyYWxfY29kZSI6IiIsInVzZXJuYW1lIjoiam9pNzcxMyIsImNvdW50cnlfYWNjZXNzIjoiQUxMIiwic3RhdHVzIjoxLCJpZCI6IjEyNCIsImV4cGlyZV90aW1lIjoiMjAyMy0wNi0wOCAyMDowODoxOCIsInJlZnJlc2hfdG9rZW4iOiIwOTYyNDQ0YjQxNWJiZjlmNDQ5M2I3ZmU4N2I5OGRhMjA1ZjAzZTY4IiwicHJvZmlsZV9waWN0dXJlIjoiIn0.ClWykJgIHQgIbo_Y8Kam6k9ONoGSN9aOYCPsBGolPQk"
*/
