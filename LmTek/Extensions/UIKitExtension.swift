//
//  UIKitExtension.swift
//  DocKare Doctor
//
//  Created by PTBLR-1128 on 18/11/19.
//  Copyright © 2019 Provab Technosoft Pvt Ltd. All rights reserved.
//

import Foundation
import UIKit

extension  UIColor {
    
    open class var brandBlueColor: UIColor {
        get {
            return UIColor(red:0.00, green:0.44, blue:0.73, alpha:1.0)
        }
    }
    
}

extension UIApplication {
    var stausBarVIew : UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}

extension UIView {
    
    enum Direction {
        case Bottom
        case Top
        case Left
        case Right
    }
    
    func dropShadoww(scale: Bool = true) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize(width: 1, height: 2)
        self.layer.shadowRadius = 1
        self.layer.cornerRadius = self.frame.height / 2
    }
    
    func dropShadowOnly() {
        let shadowPath = UIBezierPath()
        shadowPath.move(to: CGPoint(x: self.bounds.origin.x, y: self.frame.size.height))
        shadowPath.addLine(to: CGPoint(x: self.bounds.width / 2, y: self.bounds.height + 7.0))
        shadowPath.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height))
        shadowPath.close()
        
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOpacity = 1
        self.layer.masksToBounds = false
        self.layer.shadowPath = shadowPath.cgPath
    }
    
    func dropShadowWithRadius(x: CGFloat, scale: Bool = true, color: UIColor = UIColor.lightGray, radiusDirection: CACornerMask? = nil) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor//UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize(width: 1, height: 0)
        let shadowRadius : CGFloat = 3
        self.layer.shadowRadius = shadowRadius
        self.layer.cornerRadius = x
        if let cornerRadiusDirection = radiusDirection {
            self.layer.maskedCorners = cornerRadiusDirection
        }
    }
    
    func dropShadowWithRadius(_ radius: CGFloat, inDirection direction: Direction, color: UIColor = UIColor.lightGray.withAlphaComponent(0.5)) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = 1
        let shadowRadius : CGFloat = 3
        self.layer.shadowRadius = shadowRadius
        self.layer.cornerRadius = radius
        if direction == .Bottom {
            self.layer.shadowOffset = CGSize(width: 0, height: 5)
            self.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        }
        else if direction == .Top {
            self.layer.shadowOffset = CGSize(width: 0, height: -5)
            self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }
        else if direction == .Left {
            self.layer.shadowOffset = CGSize(width: -5, height: 0)
            self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        }
        else {
            self.layer.shadowOffset = CGSize(width: 5, height: 0)
            self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        }
    }
    
    func makeCornorRadius(x: CGFloat) {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = x
    }
    
    func cornorRadiusWithColor(x: CGFloat, color: UIColor) {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = x
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = 1
    }
    
    func cornerRadiusInDirection(radius: CGFloat, direction: CACornerMask) {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = radius
        self.layer.maskedCorners = direction
    }
    
    func setBorder(_ border: UIColor?, andBackground color: UIColor?) {
        self.borderColor = border
        self.backgroundColor = color
    }
    
    func addDashedBorder() {
        let color = UIColor.black.cgColor
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 0.8
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
    
    func slantView() {
        let path = UIBezierPath()
        let shapeLayer = CAShapeLayer()
        shapeLayer.lineWidth = 0
        shapeLayer.fillColor = UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 1).cgColor
        path.move(to: CGPoint(x: bounds.maxX, y: bounds.minY))
        path.addLine(to: CGPoint(x: bounds.maxX, y: bounds.maxY))
        path.addLine(to: CGPoint(x: bounds.minX, y: bounds.maxY))
        path.addLine(to: CGPoint(x: bounds.minX, y: bounds.maxY - 5))
        path.close()
        shapeLayer.path = path.cgPath
        layer.mask = shapeLayer
    }
    
    func setGradient(colors: [CGColor], startPoint: CGPoint, endPoint: CGPoint) {
        let tgl = CAGradientLayer()
        tgl.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.bounds.height)
        tgl.colors = colors
        tgl.startPoint = startPoint //CGPoint.init(x:0, y:0)
        tgl.endPoint = endPoint //CGPoint.init(x:0, y:1)
        
        self.layer.insertSublayer(tgl, at: 0)
    }
}

@IBDesignable extension UIView {
    @IBInspectable var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            }
            else {
                return nil
            }
        }
    }
    @IBInspectable var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    @IBInspectable var cornerRadius:CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
    
    func addShadow(shadowColor: CGColor = UIColor.black.cgColor,
                   shadowOffset: CGSize = CGSize(width: 1.0, height: 2.0),
                   shadowOpacity: Float = 0.4,
                   shadowRadius: CGFloat = 3.0) {
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
    
    
    
}

extension Data{
    func toString() -> String{
        return String(data: self, encoding: .utf8)!
    }
}


extension UIColor {
    
    
    convenience init?(hexString: String) {
        var chars = Array(hexString.hasPrefix("#") ? hexString.dropFirst() : hexString[...])
        let red, green, blue, alpha: CGFloat
        switch chars.count {
        case 3:
            chars = chars.flatMap { [$0, $0] }
            fallthrough
        case 6:
            chars = ["F","F"] + chars
            fallthrough
        case 8:
            alpha = CGFloat(strtoul(String(chars[0...1]), nil, 16)) / 255
            red   = CGFloat(strtoul(String(chars[2...3]), nil, 16)) / 255
            green = CGFloat(strtoul(String(chars[4...5]), nil, 16)) / 255
            blue  = CGFloat(strtoul(String(chars[6...7]), nil, 16)) / 255
        default:
            return nil
        }
        self.init(red: red, green: green, blue:  blue, alpha: alpha)
    }
    
    
}

extension Date {
    func dateFormatWithSuffix() -> String {
        return "dd'\(self.daySuffix())' MMM yyyy"
    }
    
    func daySuffix() -> String {
        let calendar = Calendar.current
        let components = (calendar as NSCalendar).components(.day, from: self)
        let dayOfMonth = components.day
        switch dayOfMonth {
        case 1, 21, 31:
            return "st"
        case 2, 22:
            return "nd"
        case 3, 23:
            return "rd"
        default:
            return "th"
        }
    }
    
    var currentUTCTimeZoneDate: String {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.s"

        return formatter.string(from: self)
    }
}

extension String {
    
    var htmlToAttributedString: NSAttributedString? {
        
        // getting attributed string...
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType:  NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }

func attributedString(withRegularFont regularFont: UIFont, andBoldFont boldFont: UIFont) -> NSMutableAttributedString {
    var attributedString = NSMutableAttributedString()
        guard let data = self.data(using: .utf8) else { return NSMutableAttributedString() }
        do {
            attributedString = try NSMutableAttributedString(data: data,
                                                             options: [.documentType: NSAttributedString.DocumentType.html,
                                                                       .characterEncoding:String.Encoding.utf8.rawValue],
                                                             documentAttributes: nil)
            let range = NSRange(location: 0, length: attributedString.length)
            attributedString.enumerateAttribute(NSAttributedString.Key.font, in: range, options: .longestEffectiveRangeNotRequired) { value, range, _ in
                let currentFont: UIFont         = value as! UIFont
                var replacementFont: UIFont?    = nil

                if currentFont.fontName.contains("bold") || currentFont.fontName.contains("Bold") {
                    replacementFont             =   boldFont
                } else {
                    replacementFont             =   regularFont
                }

                let replacementAttribute        =   [NSAttributedString.Key.font:replacementFont!]
                attributedString.addAttributes(replacementAttribute, range: range)
            }
        } catch let e {
            print(e.localizedDescription)
        }
       return attributedString
}
}

extension UISlider {
    func setSliderThumb(image: UIImage) {
        DispatchQueue.main.async {
            self.setThumbImage(image, for: .normal)
            self.setThumbImage(image, for: .highlighted)
        }
    }
}

extension UIViewController {
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }
}

extension UIImage {
    convenience init?(color: UIColor, size: CGSize = CGSize(width: 1.0, height: 1.0)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else {
            return nil
        }
        
        self.init(cgImage: cgImage)
    }
   
    class func createTransparentImageFrom(label: UILabel, imageSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(imageSize, false, 0.0)//2.0)
        /*let currentView = UIView.init(frame: CGRect(x: 0, y: 0, width: imageSize.width, height: imageSize.height))
        currentView.backgroundColor = UIColor.clear
        currentView.addSubview(label)
        
        currentView.layer.render(in: UIGraphicsGetCurrentContext()!)*/
        label.layer.render(in: UIGraphicsGetCurrentContext()!)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    class func imageWithLabel(label: UILabel) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(label.bounds.size, false, 1.0)//(label.bounds.size, false, 0.0)
        label.layer.render(in: UIGraphicsGetCurrentContext()!)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}

extension UILabel {
    func textWidth() -> CGFloat {
        return UILabel.textWidth(label: self)
    }
    
    class func textWidth(label: UILabel) -> CGFloat {
        return textWidth(label: label, text: label.text!)
    }
    
    class func textWidth(label: UILabel, text: String) -> CGFloat {
        return textWidth(font: label.font, text: text)
    }
    
    class func textWidth(font: UIFont, text: String) -> CGFloat {
        let myText = text as NSString
        
        let rect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude)
        let labelSize = myText.boundingRect(with: rect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return ceil(labelSize.width)
    }
}

/*extension UIViewController {
    func setGradient(view: UIView, colors: [CGColor]) {
        let tgl = CAGradientLayer()
        tgl.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: view.bounds.height)
        tgl.colors = colors
        tgl.startPoint = CGPoint.init(x:0, y:0)
        tgl.endPoint = CGPoint.init(x:0, y:1)
        
        view.layer.insertSublayer(tgl, at: 0)
    }
}*/

extension Int {
    func timeFormatted() -> String {
        let seconds: Int = self % 60
        let minutes: Int = (self / 60) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }
}
