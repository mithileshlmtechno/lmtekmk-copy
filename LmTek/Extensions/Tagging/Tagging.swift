import UIKit
import Foundation

open class Tagging: UIView {
    
    // MARK: - Apperance
        
    // MARK: - Properties
    var descriptionPlaceholder = "Describe your video"
    
    open var symbol: String = "@"
    open var tagableList: [String]?
    open var defaultAttributes: [NSAttributedString.Key: Any] = {
        return [NSAttributedString.Key.foregroundColor: UIColor.white,
                NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15),
                NSAttributedString.Key.underlineStyle: NSNumber(value: 0)]
    }()
    open var symbolAttributes: [NSAttributedString.Key: Any] = {
        return [NSAttributedString.Key.foregroundColor: UIColor.white,
                NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15),
                NSAttributedString.Key.underlineStyle: NSNumber(value: 0)]
    }()
    open var taggedAttributes: [NSAttributedString.Key: Any] = {return [NSAttributedString.Key.underlineStyle: NSNumber(value: 1)]}()
    
    public private(set) var taggedList: [TaggingModel] = []
    public weak var dataSource: TaggingDataSource?
    public weak var delegate: TaggingDelegate?
    
    private var currentTaggingText: String? {
        didSet {
            guard let currentTaggingText = currentTaggingText, let tagableList = tagableList else {return}
            let matchedTagableList = tagableList.filter {
                $0.contains(currentTaggingText.lowercased()) || $0.contains(currentTaggingText.uppercased())
            }
            dataSource?.tagging(self, didChangedTagableList: matchedTagableList)
        }
    }
    private var currentTaggingRange: NSRange?
    private var tagRegex: NSRegularExpression! {return try! NSRegularExpression(pattern: "\(symbol)([^\\s\\K]+)")}
    
    // MARK: - UI Components
    
    public let textView: UITextView = {
        let textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.backgroundColor = .black
//        textView.textColor = .white
        return textView
    }()
    
    // MARK: - Con(De)structor
    
    public init() {
        super.init(frame: .zero)
        commonSetup()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonSetup()
    }
    
    // MARK: - Public methods
    
    public func updateTaggedList(allText: String, tagText: String) {
        guard let range = currentTaggingRange else {return}
        
        let origin = (allText as NSString).substring(with: range)
        let tag = tagFormat(tagText)
        let replace = tag.appending(" ")
        let changed = (allText as NSString).replacingCharacters(in: range, with: replace)
        let tagRange = NSMakeRange(range.location, tag.utf16.count)
        
        taggedList.append(TaggingModel(text: tagText, range: tagRange))
        for i in 0..<taggedList.count-1 {
            var location = taggedList[i].range.location
            let length = taggedList[i].range.length
            if location > tagRange.location {
                location += replace.count - origin.count
                taggedList[i].range = NSMakeRange(location, length)
            }
        }
        
        textView.text = changed
        updateAttributeText(selectedLocation: range.location+replace.count)
        dataSource?.tagging(self, didChangedTaggedList: taggedList)
    }
    
    // MARK: - Private methods
    
    private func commonSetup() {
        setProperties()
        addSubview(textView)
        layout()
    }
    
    private func setProperties() {
        backgroundColor = .clear
        textView.delegate = self
    }
    
    private func tagFormat(_ text: String) -> String {
        return symbol.appending(text)
    }
    
}

// MARK: - Layout

extension Tagging {
    
    private func layout() {
        addConstraints(
            [NSLayoutConstraint(item: textView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 0),
             NSLayoutConstraint(item: textView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0),
             NSLayoutConstraint(item: textView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1.0, constant: 0),
             NSLayoutConstraint(item: textView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0)])
    }
    
}

// MARK: - UITextViewDelegate

extension Tagging: UITextViewDelegate {
    
    public func textViewDidChange(_ textView: UITextView) {
        tagging(textView: textView)
        updateAttributeText(selectedLocation: textView.selectedRange.location)
        if let lastSpaceIndex = textView.text.lastIndex(of: " "), let lastHashIndex = textView.text.lastIndex(of: "#"), lastSpaceIndex > lastHashIndex {
            self.delegate?.taggingHideTagsList()
        }
        if textView.text.last == "@" {
            self.delegate?.tagUser()
        }
    }
    
    public func textViewDidChangeSelection(_ textView: UITextView) {
        tagging(textView: textView)
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        updateTaggedList(range: range, textCount: text.utf16.count)
        return true
    }
    
    public func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            textView.text = self.descriptionPlaceholder
            textView.textColor = UIColor.lightGray //UIColor(hexString: "#1D1B1B")?.withAlphaComponent(0.19)
        }
        /*if !self.view_tags.isHidden {
            self.view_tags.isHidden = true
        }*/
        self.delegate?.taggingHideTagsList()
    }
    
}

// MARK: - Tagging Algorithm

extension Tagging {
    
    private func matchedData(taggingCharacters: [Character], selectedLocation: Int, taggingText: String) -> (NSRange?, String?) {
        var matchedRange: NSRange?
        var matchedString: String?
        let tag = String(taggingCharacters.reversed())
        let textRange = NSMakeRange(selectedLocation-tag.count, tag.count)
        
        guard tag == symbol else {
            let matched = tagRegex.matches(in: taggingText, options: .reportCompletion, range: textRange)
            if matched.count > 0, let range = matched.last?.range {
                matchedRange = range
                matchedString = (taggingText as NSString).substring(with: range).replacingOccurrences(of: symbol, with: "")
            }
            return (matchedRange, matchedString)
        }
        
        matchedRange = textRange
        matchedString = symbol
        return (matchedRange, matchedString)
    }
    
    private func tagging(textView: UITextView) {
        let selectedLocation = textView.selectedRange.location
        let taggingText = (textView.text as NSString).substring(with: NSMakeRange(0, selectedLocation))
        let space: Character = " "
        let lineBrak: Character = "\n"
        var tagable: Bool = false
        var characters: [Character] = []
        
        for char in Array(taggingText).reversed() {
            if char == symbol.first {
                characters.append(char)
                tagable = true
                break
            } else if char == space || char == lineBrak {
                tagable = false
                break
            }
            characters.append(char)
        }
        
        guard tagable else {
            currentTaggingRange = nil
            currentTaggingText = nil
            return
        }
        
        let data = matchedData(taggingCharacters: characters, selectedLocation: selectedLocation, taggingText: taggingText)
        currentTaggingRange = data.0
        currentTaggingText = data.1
    }
    
   func updateAttributeText(selectedLocation: Int) {
       
        var error: Error? = nil
        var regex: NSRegularExpression? = nil
        var atRegex: NSRegularExpression? = nil
        do {
            regex = try NSRegularExpression(pattern: "#(\\w+)", options: [])
            atRegex = try NSRegularExpression(pattern: "@(\\w+)", options: [])
        } catch {
            print("error:",error.localizedDescription)
        }
        let stringWithTags = textView.text
        let matches = regex?.matches(in: stringWithTags ?? "", options: [], range: NSRange(location: 0, length: stringWithTags?.count ?? 0))
        let atMatches = atRegex?.matches(in: stringWithTags ?? "", options: [], range: NSRange(location: 0, length: stringWithTags?.count ?? 0))
        let attString = NSMutableAttributedString(string: stringWithTags ?? "")
        let font = UIFont(name: "OpenSans-Regular", size: 16.0)
        attString.addAttribute(.font, value: font, range: NSRange(location: 0, length: stringWithTags!.count))
        attString.addAttribute(.foregroundColor, value: UIColor.white, range: NSRange(location: 0, length: stringWithTags!.count))
        let stringLength = stringWithTags?.count ?? 0
        for match in matches ?? [] {
            let wordRange = match.range(at: 1)
            let word = (stringWithTags as NSString?)?.substring(with: wordRange)
            attString.addAttribute(.font, value: font, range: NSRange(location: 0, length: stringLength))
            let foregroundColor = UIColor.white
            attString.addAttribute(.foregroundColor, value: foregroundColor, range: wordRange)
            print("Found tag \(word ?? "")")
        }
        for match in atMatches ?? [] {
            let wordRange = match.range(at: 1)
            let word = (stringWithTags as NSString?)?.substring(with: wordRange)
            attString.addAttribute(.font, value: font, range: NSRange(location: 0, length: stringLength))
            let foregroundColor = UIColor.white
            attString.addAttribute(.foregroundColor, value: foregroundColor, range: wordRange)
            print("Found tag \(word ?? "")")
        }
        textView.attributedText = attString//attributedString
        textView.selectedRange = NSMakeRange(selectedLocation, 0)
    }
    
    private func updateTaggedList(range: NSRange, textCount: Int) {
        taggedList = taggedList.filter({ (model) -> Bool in
            if model.range.location < range.location && range.location < model.range.location+model.range.length {
                return false
            }
            if range.length > 0 {
                if range.location <= model.range.location && model.range.location < range.location+range.length {
                    return false
                }
            }
            return true
        })
        
        for i in 0..<taggedList.count {
            var location = taggedList[i].range.location
            let length = taggedList[i].range.length
            if location >= range.location {
                if range.length > 0 {
                    if textCount > 1 {
                        location += textCount - range.length
                    } else {
                        location -= range.length
                    }
                } else {
                    location += textCount
                }
                taggedList[i].range = NSMakeRange(location, length)
            }
        }
        
        currentTaggingText = nil
        dataSource?.tagging(self, didChangedTaggedList: taggedList)
    }
    
}
