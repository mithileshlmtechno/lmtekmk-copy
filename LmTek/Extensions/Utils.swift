//
//  Utils.swift
//  DocKare Doctor
//
//  Created by PTBLR-1128 on 18/11/19.
//  Copyright © 2019 Provab Technosoft Pvt Ltd. All rights reserved.
//

import Foundation
import UIKit
import DGActivityIndicatorView
import ObjectMapper
import SwiftyJSON

class Utils {
    
    static func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView)
    {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.lightGray.cgColor
        shapeLayer.lineWidth = 1
        shapeLayer.lineDashPattern = [7, 3] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    
    static func delay(delay: Double, closure: @escaping () -> ())
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            closure()
        }
    }
    
    static func isKeyPresentInUserDefaults(key: String) -> Bool
    {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    static func convertToDictionary(text: String) -> [String: Any]?
    {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    class func getTheRootViewController() -> UIViewController
    {
        let vc = UIViewController()
        if let app = UIApplication.shared.delegate as? AppDelegate {
            if let rootController = app.window?.rootViewController {
                return rootController
            }
        }
        return vc
    }
    
    static func isValidAlphaNumeric(pass: String) -> Bool
    {
        let passFormat = "[A-Za-z0-9]*"
        let prediction = NSPredicate(format:"SELF MATCHES %@", passFormat)
        return prediction.evaluate(with: pass)
    }
    
    static func isValidName(pass: String) -> Bool
    {
        let passFormat = "[A-Za-z ]*"
        let prediction = NSPredicate(format:"SELF MATCHES %@", passFormat)
        return prediction.evaluate(with: pass)
    }
    
    static func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
}



class DialougeUtils {
    
    static let activityView = UIActivityIndicatorView(style: .whiteLarge)
    
    static func removeActivityView(view : UIView)
    {
        //activityView.stopAnimating()
        //view.isUserInteractionEnabled = true
        DispatchQueue.main.async {
            let window = UIApplication.shared.keyWindow!
            window.viewWithTag(111)?.removeFromSuperview()
        }
    }
    
    
    static func addActivityView(view : UIView)
    {
        
        let window = UIApplication.shared.keyWindow!
        
        window.viewWithTag(111)?.removeFromSuperview()
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRect(x: 0, y: 0, width: window.frame.size.width , height: window.frame.size.height + 80 )
        loadingView.backgroundColor =  UIColor.clear //UIColor(displayP3Red: 0.99, green: 0.99, blue: 0.99, alpha: 0.5)
        //loadingView.backgroundColor =  UIColor.lightGray.withAlphaComponent(1.0)
        loadingView.clipsToBounds = true
        loadingView.tag = 111
        
        let holdingView: UIView = UIView()
        holdingView.frame = CGRect(x: 0, y: 0, width: 80 , height: 80 )
        holdingView.backgroundColor =  UIColor.clear
        holdingView.layer.cornerRadius = 10
        holdingView.clipsToBounds = true
        loadingView.addSubview(holdingView)
        
        
//        let activityIndicator = DGActivityIndicatorView(type: .ballSpinFadeLoader, tintColor: UIColor.brandBlueColor, size: 50.0)
        let activityIndicator = DGActivityIndicatorView(type: .ballSpinFadeLoader, tintColor: colorYellow, size: 50.0)

        activityIndicator?.frame = CGRect(x: loadingView.frame.size.width/2 , y: loadingView.frame.size.height / 2, width: 50, height: 30)
        activityIndicator?.center = (window.rootViewController?.view.center)!
        holdingView.center = (window.rootViewController?.view.center)!
        
        //view.isUserInteractionEnabled = false
        activityIndicator?.startAnimating()
        loadingView.addSubview(activityIndicator!)
        window.addSubview(loadingView)
        window.bringSubviewToFront(loadingView)
    }
    
}
