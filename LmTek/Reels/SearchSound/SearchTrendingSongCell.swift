//
//  SearchTrendingSongCell.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 13/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SearchTrendingSongCell: UITableViewCell {
    
    var viewCell:UIView = UIView()
    var lblUserName:UILabel = UILabel()
    var lblMessage:UILabel = UILabel()
    var btnCross:UIButton = UIButton()
    var imgUser = UIImageView()

    
    let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        viewCell.frame = CGRect(x: 10, y: 5, width: screenWidth-20, height: 70)
        viewCell.backgroundColor = colorGrayBG
        viewCell.layer.cornerRadius = 4
        viewCell.clipsToBounds = true
        self.addSubview(viewCell)
        
        imgUser = UIImageView()
        imgUser.frame = CGRect(x: 0, y: 0, width: 70, height: 70)
        imgUser.backgroundColor = .gray
        imgUser.layer.cornerRadius = 4
        imgUser.clipsToBounds = true
        viewCell.addSubview(imgUser)

        lblUserName = UILabel()
        lblUserName.frame = CGRect(x: 80, y: 5, width: viewCell.frame.size.width-140, height: 20)
        lblUserName.text = "Song Title"
        lblUserName.textColor = UIColor.white
        lblUserName.textAlignment = .left
        lblUserName.font = fontSizeSemiBoldTitle
        lblUserName.clipsToBounds = true
        viewCell.addSubview(lblUserName)

        lblMessage = UILabel()
        lblMessage.frame = CGRect(x: 80, y: 25, width: viewCell.frame.size.width-100, height: 20)
        lblMessage.text = "Lorem ipsum dolor sit amet"
        lblMessage.textColor = UIColor.white
        lblMessage.textAlignment = .left
        lblMessage.font = fontSizeSmall
        lblMessage.clipsToBounds = true
        viewCell.addSubview(lblMessage)

        let lblTotalTime = UILabel()
        lblTotalTime.frame = CGRect(x: 80, y: 45, width: viewCell.frame.size.width-100, height: 18)
        lblTotalTime.text = "00:11"
        lblTotalTime.textColor = UIColor.white
        lblTotalTime.textAlignment = .left
        lblTotalTime.font = fontSizeSmall
        lblTotalTime.clipsToBounds = true
        viewCell.addSubview(lblTotalTime)

        let imgHeart: UIImageView = UIImageView()
        imgHeart.frame = CGRect(x: viewCell.frame.size.width-40, y: 25, width: 20, height: 20)
        imgHeart.image = UIImage.init(named: "heartWhiteIcon")
        viewCell.addSubview(imgHeart)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
