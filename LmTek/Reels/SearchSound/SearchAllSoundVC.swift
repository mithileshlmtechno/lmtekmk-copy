//
//  SearchAllSoundVC.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 13/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SearchAllSoundVC: BaseViewController,UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    var yRef: CGFloat = 42.0
    var searchUsers = UISearchBar()
    var tblUsers: UITableView = UITableView()
    var searchActive:Bool = false
//    var allFilterData:[getMyDocumentData] = []
    
    var btnLibrery:UIButton = UIButton()
    var btnLocal:UIButton = UIButton()
    var btnFavorite:UIButton = UIButton()
    var lblYellowLine:UILabel = UILabel()
    var checkTab:String = "All"
    
    var ProductCollectionview:UICollectionView!
    var cellId = "Cell"
    var viewScroll: UIScrollView = UIScrollView()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        self.screenDesigning()
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    func screenDesigning() {
        
        searchUsers = UISearchBar()
        searchUsers.frame = CGRect(x: 50, y: yRef, width: screenWidth-60, height: 40)
        searchUsers.backgroundColor = ColorTextFieldBG
        searchUsers.placeholder = "Search Audio"
        searchUsers.barTintColor = ColorTextFieldBG
        searchUsers.searchTextField.backgroundColor = ColorTextFieldBG
        searchUsers.searchTextField.font = fontSizeRegulor
        //searchUsers.delegate = self
        searchUsers.isUserInteractionEnabled = true
        searchUsers.layer.cornerRadius = 4
        searchUsers.layer.borderWidth = 1
        searchUsers.layer.borderColor = colorGray.cgColor
        searchUsers.showsCancelButton = false
        searchUsers.clipsToBounds = true
        self.view.addSubview(searchUsers)
        
        let btnCancel = UIButton()
        btnCancel.frame = CGRect(x: 10, y: yRef+5, width: 30, height: 30)
        btnCancel.backgroundColor = UIColor.clear
        btnCancel.setImage(UIImage.init(named: "Cross"), for: .normal)
        btnCancel.addTarget(self, action: #selector(self.btnCancelClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnCancel)
        
        yRef = yRef+searchUsers.frame.size.height + 15
        
        btnLibrery = UIButton()
        btnLibrery.frame = CGRect(x: 0, y: yRef, width: screenWidth/3, height: 30)
        btnLibrery.setTitle("LIBRERY", for: .normal)
        btnLibrery.setTitleColor(colorYellow, for: .normal)
        btnLibrery.titleLabel?.font = fontSizeSemiBoldTitle
        btnLibrery.addTarget(self, action: #selector(self.btnLibreryClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnLibrery)
        
        btnLocal = UIButton()
        btnLocal.frame = CGRect(x: screenWidth/3, y: yRef, width: screenWidth/3, height: 30)
        btnLocal.setTitle("LOCAL", for: .normal)
        btnLocal.setTitleColor(.white, for: .normal)
        btnLocal.titleLabel?.font = fontSizeSemiBoldTitle
        btnLocal.addTarget(self, action: #selector(self.btnLocalClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnLocal)

        btnFavorite = UIButton()
        btnFavorite.frame = CGRect(x: (screenWidth/3)*2, y: yRef, width: screenWidth/3, height: 30)
        btnFavorite.setTitle("FAVORITE", for: .normal)
        btnFavorite.setTitleColor(.white, for: .normal)
        btnFavorite.titleLabel?.font = fontSizeSemiBoldTitle
        btnFavorite.addTarget(self, action: #selector(self.btnFavoriteClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnFavorite)

        yRef = yRef+btnFavorite.frame.size.height + 10
        
        print(yRef)
        
        let lblBGLine:UILabel = UILabel()
        lblBGLine.frame = CGRect(x: 10, y: yRef, width: screenWidth, height: 1)
        lblBGLine.backgroundColor = colorGrayBG
        lblBGLine.clipsToBounds = true
        self.view.addSubview(lblBGLine)

        lblYellowLine = UILabel()
        lblYellowLine.frame = CGRect(x: 10, y: yRef-0.5, width: screenWidth/3, height: 2)
        lblYellowLine.backgroundColor = colorYellow
        lblYellowLine.clipsToBounds = true
        self.view.addSubview(lblYellowLine)

        yRef = yRef+lblYellowLine.frame.size.height + 2

        viewScroll = UIScrollView()
        viewScroll.frame = CGRect(x: 0, y: yRef, width: screenWidth, height: screenHeight-yRef)
        viewScroll.backgroundColor = UIColor.clear
        viewScroll.isPagingEnabled = true
        viewScroll.clipsToBounds = true
        self.view.addSubview(viewScroll)
        
        var xRef:CGFloat = 0
        
//        for i in 0..<4 {
//
//            if i == 0 {
//
//            } else if i == 1 {
                tblUsers = UITableView()
                tblUsers.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight-yRef)
                tblUsers.backgroundColor = UIColor.black
                tblUsers.delegate = self
                tblUsers.dataSource = self
                tblUsers.separatorStyle = .none
                viewScroll.addSubview(tblUsers)
//            } else if i == 2 {
//
//            } else if i == 3 {
//
//            }
            
//            xRef = xRef+screenWidth
     //   }
    }
    func showCollection(){
        
    }
    //MARK: Action
    
    @objc func btnCancelClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnLibreryClicked(_ sender: UIButton) {
        checkTab = "all"
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations: {
            self.lblYellowLine.frame = CGRect(x: 10, y: 137-0.5, width: screenWidth/3, height: 2)
            self.btnLibrery.setTitleColor(colorYellow, for: .normal)
            self.btnLocal.setTitleColor(.white, for: .normal)
            self.btnFavorite.setTitleColor(.white, for: .normal)
            self.viewScroll.contentOffset = CGPoint(x: 0, y: 0)
        }, completion: nil)
    }
    
    @objc func btnLocalClicked(_ sender: UIButton) {
        checkTab = "users"
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations: {
            self.lblYellowLine.frame = CGRect(x: screenWidth/3, y: 137-0.5, width: screenWidth/3, height: 2)
            self.btnLocal.setTitleColor(colorYellow, for: .normal)
            self.btnLibrery.setTitleColor(.white, for: .normal)
            self.btnFavorite.setTitleColor(.white, for: .normal)
            self.viewScroll.contentOffset = CGPoint(x: screenWidth, y: 0)
        }, completion: nil)

    }
    
    @objc func btnFavoriteClicked(_ sender: UIButton) {
        checkTab = "videos"
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations: {
            self.lblYellowLine.frame = CGRect(x: (screenWidth/3)*2, y: 137-0.5, width: screenWidth/3, height: 2)
            self.btnFavorite.setTitleColor(colorYellow, for: .normal)
            self.btnLibrery.setTitleColor(.white, for: .normal)
            self.btnLocal.setTitleColor(.white, for: .normal)
        }, completion: nil)

    }
    
    //MARK: SearchviewDelegate
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
        let ViewForDoneButtonOnKeyboard = UIToolbar()
        ViewForDoneButtonOnKeyboard.sizeToFit()
        let btnDoneOnKeyboard = UIBarButtonItem(title: "Done", style: .bordered, target: self, action: #selector(self.doneBtnFromKeyboardClicked))
        ViewForDoneButtonOnKeyboard.items = [btnDoneOnKeyboard]
        searchUsers.inputAccessoryView = ViewForDoneButtonOnKeyboard
        tblUsers.isHidden = true

    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
        //tblSearch.isHidden = true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.text = nil
        searchBar.resignFirstResponder()
        tblUsers.resignFirstResponder()
        tblUsers.reloadData()

    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.resignFirstResponder()
    }
    func filterContent(forSearchText searchText: String?, scope: String?) {
        var recipes:NSArray = NSArray()
        var searchResults:NSArray = NSArray()

        let resultPredicate = NSPredicate(format: "name contains[c] %@", searchText ?? "")
        searchResults = recipes.filtered(using: resultPredicate) as NSArray
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        allFilterData = arrMyDocumentData.filter({($0.name as AnyObject).contains(searchText)
//        })
//        print("filter = ",allFilterData)
//
//        tblUsers.isHidden = false
//        if(allFilterData.count == 0){
//            searchActive = false;
//             print("UISearchBar.text cleared!")
//        } else {
//            searchActive = true;
//        }
         self.tblUsers.reloadData()
    }
    @objc func doneBtnFromKeyboardClicked()
    {
        self.becomeFirstResponder()
        self.view.endEditing(true)
        tblUsers.isHidden = false
    }

    //MARK: TableviewDelegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
       
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 2
        } else if section == 1{
            return 1
        } else {
            return 2
        }
    }
          
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         if indexPath.section == 1 {
             let CellIdentifier:String = "cell";
             var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: CellIdentifier)
                 if cell == nil {
                     autoreleasepool {
                         cell = UITableViewCell(style: .default, reuseIdentifier: CellIdentifier)
                     }
                 }
             let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
             layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
             layout.itemSize = CGSize(width: (screenWidth-65)/4, height: (screenWidth-65)/4)

             ProductCollectionview = UICollectionView(frame: CGRect(x: 0, y: 10, width: screenWidth, height: (screenWidth/2)-5), collectionViewLayout: layout)
             ProductCollectionview.dataSource = self
             ProductCollectionview.delegate = self
             ProductCollectionview.register(SearchAllSoundCollectionCell.self, forCellWithReuseIdentifier: cellId)
             ProductCollectionview.showsVerticalScrollIndicator = false
             ProductCollectionview.backgroundColor = UIColor.clear
             cell?.addSubview(ProductCollectionview)

             cell?.backgroundColor = UIColor.clear
             cell?.selectionStyle = .none
             return cell!

         }else {
             let CellIdentifier:String = "mycell";
             var cell: SearchTrendingSongCell? = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? SearchTrendingSongCell
                 if cell == nil {
                     autoreleasepool {
                         cell = SearchTrendingSongCell(style: .default, reuseIdentifier: CellIdentifier)
                     }
                 }
              
              cell?.btnCross.isHidden = true
              cell?.selectionStyle = .none
              cell?.backgroundColor = UIColor.clear
              return cell!
         }

    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            
        } else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TrendingSearchVC") as! TrendingSearchVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            return (screenWidth/2)-15
        }else {
            return 90
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
            let viewHeaderCell = UIView()
            viewHeaderCell.frame = CGRect(x: 10, y: 0, width: screenWidth, height: 40)
            viewHeaderCell.backgroundColor = .black
            view.addSubview(viewHeaderCell)

            let lblRecent = UILabel()
            lblRecent.frame = CGRect(x:10, y: 0, width: 80, height: 40)
            lblRecent.backgroundColor = UIColor.clear
            lblRecent.text = "Trending"
            lblRecent.textColor = .white
            lblRecent.textAlignment = .left
            lblRecent.clipsToBounds = true
            viewHeaderCell.addSubview(lblRecent)

            let btnCancel = UIButton()
            btnCancel.frame = CGRect(x: viewHeaderCell.frame.size.width-80, y: 0, width: 80, height: 40)
            btnCancel.backgroundColor = UIColor.clear
            btnCancel.setTitle("See All", for: .normal)
            btnCancel.titleLabel?.font = fontSizeRegulor
            btnCancel.setTitleColor(colorGray, for: .normal)
            viewHeaderCell.addSubview(btnCancel)

            return viewHeaderCell

        } else if section == 1 {
            let viewHeaderCell = UIView()
            viewHeaderCell.frame = CGRect(x: 10, y: 0, width: screenWidth, height: 40)
            viewHeaderCell.backgroundColor = .black
            view.addSubview(viewHeaderCell)

            let lblRecent = UILabel()
            lblRecent.frame = CGRect(x:10, y: 0, width: 80, height: 40)
            lblRecent.backgroundColor = UIColor.clear
            lblRecent.text = "Collection"
            lblRecent.textColor = .white
            lblRecent.textAlignment = .left
            lblRecent.clipsToBounds = true
            viewHeaderCell.addSubview(lblRecent)

            let btnCancel = UIButton()
            btnCancel.frame = CGRect(x: viewHeaderCell.frame.size.width-80, y: 0, width: 80, height: 40)
            btnCancel.backgroundColor = UIColor.clear
            btnCancel.setTitle("See All", for: .normal)
            btnCancel.titleLabel?.font = fontSizeRegulor
            btnCancel.setTitleColor(colorGray, for: .normal)
            viewHeaderCell.addSubview(btnCancel)

            return viewHeaderCell

        } else {
            let viewHeaderCell = UIView()
            viewHeaderCell.frame = CGRect(x: 10, y: 0, width: screenWidth, height: 40)
            viewHeaderCell.backgroundColor = .black
            view.addSubview(viewHeaderCell)

            let lblRecent = UILabel()
            lblRecent.frame = CGRect(x:10, y: 0, width: 180, height: 40)
            lblRecent.backgroundColor = UIColor.clear
            lblRecent.text = "Best of Love"
            lblRecent.textColor = .white
            lblRecent.textAlignment = .left
            lblRecent.clipsToBounds = true
            viewHeaderCell.addSubview(lblRecent)

            let btnCancel = UIButton()
            btnCancel.frame = CGRect(x: viewHeaderCell.frame.size.width-80, y: 0, width: 80, height: 40)
            btnCancel.backgroundColor = UIColor.clear
            btnCancel.setTitle("See All", for: .normal)
            btnCancel.titleLabel?.font = fontSizeRegulor
            btnCancel.setTitleColor(colorGray, for: .normal)
            viewHeaderCell.addSubview(btnCancel)

            return viewHeaderCell

        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return view
    }
    //MARK: CollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)  as! SearchAllSoundCollectionCell
        return Cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemSize = ((collectionView.frame.width-65)/4)
            return CGSize(width: itemSize, height: itemSize)
    }

}
