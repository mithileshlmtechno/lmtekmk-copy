//
//  SearchAllSoundCollectionCell.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 13/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SearchAllSoundCollectionCell: UICollectionViewCell {
    var viewBG:UIView = UIView()
    let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var imgDraw:UIImageView = UIImageView()

    override init(frame: CGRect) {
        super.init(frame: frame)
           
        self.design()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func design(){
        
        viewBG = UIView()
        viewBG.frame = CGRect(x: 10, y: 1, width: (screenWidth-65)/4, height: (screenWidth-65)/4)
        viewBG.backgroundColor = .gray
        viewBG.layer.cornerRadius = 4//ShaplayerViewTicket(calayer1, corner: 12)
        viewBG.clipsToBounds = true
        self.addSubview(viewBG)
        
//        let sLayer:CAShapeLayer = CAShapeLayer()
//        imgDraw = UIImageView()
//        imgDraw.frame = CGRect(x: 0, y: 0, width: viewBG.frame.size.width, height: viewBG.frame.size.height)
//        imgDraw.ShaplayerUp(sLayer, corner: 1)
//        imgDraw.image = UIImage.init(named: "Rectangle 15")
//        imgDraw.isUserInteractionEnabled = true
//        imgDraw.clipsToBounds = true
//        viewBG.addSubview(imgDraw)
                
    }

}
