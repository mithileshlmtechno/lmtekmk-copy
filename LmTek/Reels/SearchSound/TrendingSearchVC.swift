//
//  TrendingSearchVC.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 13/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit
import RETrimControl

@available(iOS 13.0, *)
class TrendingSearchVC: BaseViewController,UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, RETrimControlDelegate {
    func trimControl(_ trimControl: RETrimControl!, didChangeLeftValue leftValue: CGFloat, rightValue: CGFloat) {
        print(leftValue)
    }
    
    
    var yRef: CGFloat = 42.0
    var searchUsers = UISearchBar()
    var tblUsers: UITableView = UITableView()
    var searchActive:Bool = false
    var TableNumber:Int!
    var viewBG:UIView = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        self.screenDesigning()
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func screenDesigning() {
        
        searchUsers = UISearchBar()
        searchUsers.frame = CGRect(x: 50, y: yRef, width: screenWidth-60, height: 40)
        searchUsers.backgroundColor = ColorTextFieldBG
        searchUsers.placeholder = "Search Audio"
        searchUsers.barTintColor = ColorTextFieldBG
        searchUsers.searchTextField.backgroundColor = ColorTextFieldBG
        searchUsers.searchTextField.font = fontSizeRegulor
        //searchUsers.delegate = self
        searchUsers.isUserInteractionEnabled = true
        searchUsers.layer.cornerRadius = 4
        searchUsers.layer.borderWidth = 1
        searchUsers.layer.borderColor = colorGray.cgColor
        searchUsers.showsCancelButton = false
        searchUsers.clipsToBounds = true
        self.view.addSubview(searchUsers)
        
        let btnCancel = UIButton()
        btnCancel.frame = CGRect(x: 5, y: yRef+5, width: 30, height: 30)
        btnCancel.backgroundColor = UIColor.clear
        btnCancel.setImage(UIImage.init(named: "BackLeft"), for: .normal)
        btnCancel.addTarget(self, action: #selector(self.btnCancelClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnCancel)
        
        yRef = yRef+searchUsers.frame.size.height + 5
        
        tblUsers = UITableView()
        tblUsers.frame = CGRect(x: 0, y: yRef, width: screenWidth, height: screenHeight-yRef)
        tblUsers.backgroundColor = UIColor.black
        tblUsers.delegate = self
        tblUsers.dataSource = self
        tblUsers.separatorStyle = .none
        self.view.addSubview(tblUsers)
        
        self.ShowBootomTrime()
    }
    func showCollection(){
        
    }
    //MARK: Action
    
    @objc func btnCancelClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: SearchviewDelegate
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
        let ViewForDoneButtonOnKeyboard = UIToolbar()
        ViewForDoneButtonOnKeyboard.sizeToFit()
        let btnDoneOnKeyboard = UIBarButtonItem(title: "Done", style: .bordered, target: self, action: #selector(self.doneBtnFromKeyboardClicked))
        ViewForDoneButtonOnKeyboard.items = [btnDoneOnKeyboard]
        searchUsers.inputAccessoryView = ViewForDoneButtonOnKeyboard
        tblUsers.isHidden = true
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
        //tblSearch.isHidden = true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.text = nil
        searchBar.resignFirstResponder()
        tblUsers.resignFirstResponder()
        tblUsers.reloadData()
        
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.resignFirstResponder()
    }
    func filterContent(forSearchText searchText: String?, scope: String?) {
        var recipes:NSArray = NSArray()
        var searchResults:NSArray = NSArray()
        
        let resultPredicate = NSPredicate(format: "name contains[c] %@", searchText ?? "")
        searchResults = recipes.filtered(using: resultPredicate) as NSArray
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //        allFilterData = arrMyDocumentData.filter({($0.name as AnyObject).contains(searchText)
        //        })
        //        print("filter = ",allFilterData)
        //
        //        tblUsers.isHidden = false
        //        if(allFilterData.count == 0){
        //            searchActive = false;
        //             print("UISearchBar.text cleared!")
        //        } else {
        //            searchActive = true;
        //        }
        self.tblUsers.reloadData()
    }
    @objc func doneBtnFromKeyboardClicked()
    {
        self.becomeFirstResponder()
        self.view.endEditing(true)
        tblUsers.isHidden = false
    }
    
    //MARK: TableviewDelegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let CellIdentifier:String = "mycell";
        var cell: SearchTrendingSongCell? = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? SearchTrendingSongCell
        if cell == nil {
            autoreleasepool {
                cell = SearchTrendingSongCell(style: .default, reuseIdentifier: CellIdentifier)
            }
        }
        
        cell?.btnCross.isHidden = true
        cell?.selectionStyle = .none
        cell?.backgroundColor = UIColor.clear
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return view
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return view
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let MoreAction = UIContextualAction(style: .normal, title:  "Use", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            self.openViewBottom()
            success(true)
        })
        MoreAction.backgroundColor = .gray
        
        return UISwipeActionsConfiguration(actions: [MoreAction])
    }
    func addTriming(_ indexNumber: Int) {
        let IndexR: IndexPath = IndexPath(row: indexNumber, section: 0)
        if let cell: SearchTrendingSongCell = tblUsers.cellForRow(at: IndexR) as? SearchTrendingSongCell {
            
            let trimAudio: RETrimControl = RETrimControl()
            trimAudio.frame = CGRect(x: 10, y: 100, width: screenWidth-20, height: 60)
            trimAudio.backgroundColor = colorGrayBG
            trimAudio.textColor = .white
            trimAudio.length = 100
            trimAudio.delegate = self
            cell.addSubview(trimAudio)
            
            let btnTrim: UIButton = UIButton()
            btnTrim.frame = CGRect(x: 10, y: 170, width: screenWidth-20, height: 40)
            btnTrim.backgroundColor = colorYellow
            btnTrim.setTitle("Trim and Use", for: .normal)
            btnTrim.setTitleColor(.black, for: .normal)
            btnTrim.titleLabel?.font = fontSizeSemiBoldTitle
            btnTrim.layer.cornerRadius = 4
            btnTrim.clipsToBounds = true
            cell.addSubview(btnTrim)
        }
    }
    
    func openViewBottom(){
        UIView.animate(withDuration: 0.5, delay: 0.2, options: [.curveEaseOut], animations: {
            self.viewBG.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        }, completion: nil)

    }
    //MARK: Trim Audio
    
    func ShowBootomTrime(){
        
        viewBG = UIView()
        viewBG.frame = CGRect(x: 0, y: screenHeight, width: screenWidth, height: screenHeight)
        viewBG.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        viewBG.clipsToBounds = true
        self.view.addSubview(viewBG)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleUserTap(_:)))
        viewBG.addGestureRecognizer(tap)
        viewBG.isUserInteractionEnabled = true

        let viewBottom = UIView()
        viewBottom.frame = CGRect(x: 0, y: screenHeight-300, width: screenWidth, height: 300)
        viewBottom.backgroundColor = UIColor.black.withAlphaComponent(0.9)
        viewBG.addSubview(viewBottom)
        
        let imgSong = UIImageView()
        imgSong.frame = CGRect(x: 10, y: 10, width: 70, height: 70)
        imgSong.backgroundColor = .gray
        imgSong.layer.cornerRadius = 4
        imgSong.clipsToBounds = true
        viewBottom.addSubview(imgSong)

        let lblSongName = UILabel()
        lblSongName.frame = CGRect(x: 90, y: 15, width: viewBottom.frame.size.width-140, height: 20)
        lblSongName.text = "Song Title"
        lblSongName.textColor = UIColor.white
        lblSongName.textAlignment = .left
        lblSongName.font = fontSizeSemiBoldTitle
        lblSongName.clipsToBounds = true
        viewBottom.addSubview(lblSongName)

        let lblSongText = UILabel()
        lblSongText.frame = CGRect(x: 90, y: 35, width: viewBottom.frame.size.width-100, height: 20)
        lblSongText.text = "Lorem ipsum dolor sit amet"
        lblSongText.textColor = UIColor.white
        lblSongText.textAlignment = .left
        lblSongText.font = fontSizeSmall
        lblSongText.clipsToBounds = true
        viewBottom.addSubview(lblSongText)

        let lblTotalTime = UILabel()
        lblTotalTime.frame = CGRect(x: 90, y: 55, width: viewBottom.frame.size.width-100, height: 18)
        lblTotalTime.text = "00:11"
        lblTotalTime.textColor = UIColor.white
        lblTotalTime.textAlignment = .left
        lblTotalTime.font = fontSizeSmall
        lblTotalTime.clipsToBounds = true
        viewBottom.addSubview(lblTotalTime)

        let imgHeart: UIImageView = UIImageView()
        imgHeart.frame = CGRect(x: viewBottom.frame.size.width-40, y: 35, width: 20, height: 20)
        imgHeart.image = UIImage.init(named: "BinIcon")
        viewBottom.addSubview(imgHeart)

        let trimAudio: RETrimControl = RETrimControl()
        trimAudio.frame = CGRect(x: 10, y: 100, width: screenWidth-20, height: 60)
        trimAudio.backgroundColor = colorGrayBG
        trimAudio.textColor = .white
        trimAudio.length = 100
        trimAudio.delegate = self
        viewBottom.addSubview(trimAudio)
                
        let btnTrim: UIButton = UIButton()
        btnTrim.frame = CGRect(x: 10, y: 200, width: screenWidth-20, height: 40)
        btnTrim.backgroundColor = colorYellow
        btnTrim.setTitle("Trim and Use", for: .normal)
        btnTrim.setTitleColor(.black, for: .normal)
        btnTrim.titleLabel?.font = fontSizeSemiBoldTitle
        btnTrim.layer.cornerRadius = 4
        btnTrim.clipsToBounds = true
        viewBottom.addSubview(btnTrim)
    }
    
    @objc func handleUserTap(_ sender: UITapGestureRecognizer? = nil) {
        UIView.animate(withDuration: 0.5, delay: 0.2, options: [.curveEaseOut], animations: {
            self.viewBG.frame = CGRect(x: 0, y: screenHeight, width: screenWidth, height: screenHeight)
        }, completion: nil)

    }

}
