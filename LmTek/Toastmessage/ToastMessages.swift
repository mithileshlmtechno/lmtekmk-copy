//
//  ToastMessages.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 05/08/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import Foundation

import Foundation
import UIKit

struct ToastMessage {
    
    let bgColor: UIColor = .red
    
    static func showToast(in vc: UIViewController, message: String, bgColor: UIColor) {
        let toastLabel = UIButton(frame: CGRect(x: vc.view.frame.size.width / 2 - 150,
                                               y: vc.view.frame.size.height - 100,
                                               width: 300,
                                               height: 40))
        toastLabel.backgroundColor = bgColor
        toastLabel.setTitleColor(UIColor.black, for: .normal)
        toastLabel.titleLabel?.font = fontSizeRegulor
        toastLabel.setTitle("  "+message, for: .normal)
        //toastLabel.setImage(image.withRenderingMode(.alwaysTemplate), for: .normal)
        toastLabel.tintColor = .white
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 20;
        toastLabel.clipsToBounds = true
        vc.view.addSubview(toastLabel)
        UIView.animate(withDuration: 1, delay: 1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}
