//
//  LoginUserVC.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 04/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit
import Alamofire
import FBSDKCoreKit
import FBSDKLoginKit
import AuthenticationServices

@available(iOS 13.0, *)
class LoginUserVC: BaseViewController,ASAuthorizationControllerPresentationContextProviding, ASAuthorizationControllerDelegate {
    
    var yRef:CGFloat = 50.0
    
    var btnTopLanguage: UIButton = UIButton()
    var txtUserName: UITextField = UITextField()
    var txtPassword: UITextField = UITextField()
    var btnForgetPassword: UIButton = UIButton()
    var arrLoginData: NSMutableArray = NSMutableArray()
    
    var btnEye: UIButton = UIButton()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        //self.updateUI()
        self.screenDesigning()
    }
    func screenDesigning() {
        
        let viewscrollView: UIScrollView = UIScrollView()
        viewscrollView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight-80)
        viewscrollView.backgroundColor = UIColor.black
        self.view.addSubview(viewscrollView)
        
        btnTopLanguage = UIButton()
        btnTopLanguage.frame = CGRect(x: (screenWidth-250)/2, y: yRef, width: 250, height: 30)
        btnTopLanguage.backgroundColor = UIColor.clear
        btnTopLanguage.setTitle("English (india)  ", for: .normal)
        btnTopLanguage.setImage(UIImage.init(named: "DownArrow"), for: .normal)
        btnTopLanguage.setTitleColor(colorGray, for: .normal)
        btnTopLanguage.titleLabel?.font = fontSizeRegulor
        btnTopLanguage.clipsToBounds = true
        btnTopLanguage.addTarget(self, action: #selector(self.btnTopLanguageClicked(_:)), for: .touchUpInside)
        viewscrollView.addSubview(btnTopLanguage)
        
        btnTopLanguage.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        btnTopLanguage.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        btnTopLanguage.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)

        yRef = yRef+btnTopLanguage.frame.size.height+55
        
        let imgUser:UIImageView = UIImageView()
        imgUser.frame = CGRect(x: (screenWidth-90)/2, y: yRef, width: 90, height: 90)
        imgUser.backgroundColor = UIColor.clear
        imgUser.image = UIImage.init(named: "AppLogo")
        imgUser.contentMode = .scaleAspectFill
        viewscrollView.addSubview(imgUser)
        
        yRef = yRef+imgUser.frame.size.height+55
        
        txtUserName = UITextField()
        txtUserName.frame = CGRect(x: 15, y: yRef, width: screenWidth-30, height: 40)
        txtUserName.backgroundColor = colorGray.withAlphaComponent(0.25)
        txtUserName.attributedPlaceholder = NSAttributedString(string: "Email/Phone number/Username", attributes: [NSAttributedString.Key.foregroundColor: colorGray])
        txtUserName.setLeftPaddingPoints(10)
        txtUserName.setRightPaddingPoints(10)
        txtUserName.textColor = UIColor.white
        txtUserName.font = fontSizeRegulor
        txtUserName.layer.cornerRadius = 4
        txtUserName.clipsToBounds = true
        viewscrollView.addSubview(txtUserName)
        
        yRef = yRef+txtUserName.frame.size.height+15
        
        txtPassword = UITextField()
        txtPassword.frame = CGRect(x: 15, y: yRef, width: screenWidth-30, height: 40)
        txtPassword.backgroundColor = colorGray.withAlphaComponent(0.25)
        txtPassword.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: colorGray])
        txtPassword.textColor = UIColor.white
        txtPassword.layer.cornerRadius = 4
        txtPassword.setLeftPaddingPoints(10)
        txtPassword.setRightPaddingPoints(50)
        txtPassword.font = fontSizeRegulor
        txtPassword.isSecureTextEntry = true
        txtPassword.clipsToBounds = true
        viewscrollView.addSubview(txtPassword)
        
        btnEye = UIButton()
        btnEye.frame = CGRect(x: txtPassword.frame.size.width-20, y: yRef+10, width: 20, height: 20)
        btnEye.setImage(UIImage.init(named: "Passwordeye"), for: .normal)
        btnEye.addTarget(self, action: #selector(self.btnEyeClicked(_:)), for: .touchUpInside)
        viewscrollView.addSubview(btnEye)
        
        yRef = yRef+txtPassword.frame.size.height+8
        
        let btnForgetPassword:UIButton = UIButton()
        btnForgetPassword.frame = CGRect(x: screenWidth-150, y: yRef, width: 135, height: 20)
        btnForgetPassword.backgroundColor = UIColor.clear
        btnForgetPassword.setTitle("Forget Password?", for: .normal)
        btnForgetPassword.setTitleColor(colorYellow, for: .normal)
        btnForgetPassword.titleLabel?.font = fontSizeRegulor
        btnForgetPassword.clipsToBounds = true
        btnForgetPassword.addTarget(self, action: #selector(self.btnForgetPasswordClicked(_:)), for: .touchUpInside)
        viewscrollView.addSubview(btnForgetPassword)
        
        yRef = yRef+btnForgetPassword.frame.size.height+25
        
        let btnLogin: UIButton = UIButton()
        btnLogin.frame = CGRect(x: 15, y: yRef, width: screenWidth-30, height: 40)
        btnLogin.backgroundColor = colorYellow
        btnLogin.setTitle("Login", for: .normal)
        btnLogin.setTitleColor(.black, for: .normal)
        btnLogin.titleLabel?.font = fontSizeSemiBoldTitle
        btnLogin.layer.cornerRadius = 4
        btnLogin.clipsToBounds = true
        btnLogin.addTarget(self, action: #selector(self.btnLoginClicked(_:)), for: .touchUpInside)
        viewscrollView.addSubview(btnLogin)
        
        yRef = yRef+btnLogin.frame.size.height+35
        
        let lblLine = UILabel()
        lblLine.frame = CGRect(x: 15, y: yRef, width: (screenWidth/2)-35, height: 1)
        lblLine.backgroundColor = colorYellow.withAlphaComponent(0.25)
        lblLine.clipsToBounds = true
        viewscrollView.addSubview(lblLine)
        
        let lblOR = UILabel()
        lblOR.frame = CGRect(x: (screenWidth/2)-15, y: yRef-15, width: 30, height: 30)
        lblOR.backgroundColor = UIColor.clear
        lblOR.text = "OR"
        lblOR.textColor = .white
        lblOR.textAlignment = .center
        lblOR.font = fontSizeRegulor
        lblOR.clipsToBounds = true
        viewscrollView.addSubview(lblOR)
        
        let lblLine1 = UILabel()
        lblLine1.frame = CGRect(x: (screenWidth/2)+20, y: yRef, width: (screenWidth/2)-35, height: 1)
        lblLine1.backgroundColor = colorYellow.withAlphaComponent(0.25)
        lblLine1.clipsToBounds = true
        viewscrollView.addSubview(lblLine1)
        
        yRef = yRef+lblLine.frame.size.height+45
        
        let btnSignFacebook:UIButton = UIButton()
        btnSignFacebook.frame = CGRect(x: (screenWidth-230)/2, y: yRef, width: 230, height: 25)
        btnSignFacebook.setTitle("  Log in with Facebook", for: .normal)
        btnSignFacebook.setTitleColor(ColorFaceBook, for: .normal)
        btnSignFacebook.titleLabel?.font = fontSizeRegulor
        btnSignFacebook.setImage(UIImage.init(named: "facebookSmall"), for: .normal)
        btnSignFacebook.addTarget(self, action: #selector(self.btnFacebookClicked(_:)), for: .touchUpInside)
        viewscrollView.addSubview(btnSignFacebook)
        
        yRef = yRef+btnSignFacebook.frame.size.height+10
        
        let btnAppleSignIn:UIButton = UIButton()
        btnAppleSignIn.frame = CGRect(x: (screenWidth-230)/2, y: yRef, width: 230, height: 25)
        btnAppleSignIn.setTitle("  Log in with Apple", for: .normal)
        btnAppleSignIn.setTitleColor(colorGray, for: .normal)
        btnAppleSignIn.titleLabel?.font = fontSizeRegulor
        btnAppleSignIn.setImage(UIImage.init(named: "AppleLogo"), for: .normal)
        btnAppleSignIn.addTarget(self, action: #selector(self.btnAppleLoginClicked(_:)), for: .touchUpInside)
        viewscrollView.addSubview(btnAppleSignIn)
        
        yRef = yRef+btnAppleSignIn.frame.size.height+15
        
        viewscrollView.contentSize = CGSize(width: screenWidth, height: yRef)
        
        let lblLineBottom:UILabel = UILabel()
        lblLineBottom.frame = CGRect(x: 0, y: screenHeight-70, width: screenWidth, height: 1)
        lblLineBottom.backgroundColor = colorGray.withAlphaComponent(0.25)
        lblLineBottom.clipsToBounds = true
        self.view.addSubview(lblLineBottom)
        
        let lblDontHave:UILabel = UILabel()
        lblDontHave.frame = CGRect(x: (screenWidth/2)-150, y: screenHeight-55, width: 200, height: 25)
        lblDontHave.backgroundColor = UIColor.clear
        lblDontHave.text = "Don’t have an account"
        lblDontHave.textColor = UIColor.lightGray
        lblDontHave.textAlignment = .right
        lblDontHave.font = fontSizeRegulor
        self.view.addSubview(lblDontHave)
        
        let btnSignIn:UIButton = UIButton()
        btnSignIn.frame = CGRect(x: (screenWidth/2.0)+55, y: screenHeight-55, width: 75, height: 25)
        btnSignIn.setTitle("Sign Up", for: .normal)
        btnSignIn.setTitleColor(colorYellow, for: .normal)
        btnSignIn.titleLabel?.font = fontSizeRegulor
        btnSignIn.addTarget(self, action: #selector(self.btnSignUpClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnSignIn)
        
    }
    //MARK: Action
    func updateData() {
        let objdata: AuthUserLoginData = arrLoginData.object(at: 0) as! AuthUserLoginData
        let id = String(describing: objdata.id!)
        print(id)
        let chooseLanguageVC = self.storyboard?.instantiateViewController(withIdentifier: "ChooseLanguageVC") as! ChooseLanguageVC
        chooseLanguageVC.isAfterLogin = true
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(chooseLanguageVC, animated: true)
        }
    }
    @objc func btnTopLanguageClicked(_ sender: UIButton) {
        
    }
    @objc func btnLoginClicked(_ sender: UIButton) {
        if txtUserName.text?.isEmpty == true {
            ToastMessage.showToast(in: self, message: "Please Enter User Name", bgColor: .white)
        }else if txtPassword.text?.isEmpty == true {
            ToastMessage.showToast(in: self, message: "Please Enter Password", bgColor: .white)
        }else {
            self.apiLogin()
        }
//        let chooseLanguageVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpAddYourNameVC") as! SignUpAddYourNameVC
//        self.navigationController?.pushViewController(chooseLanguageVC, animated: true)
    }
    
    @objc func btnFacebookClicked(_ sender: UIButton) {
        let login = LoginManager()
        login.logIn(permissions: ["public_profile"], viewController: self) { (result) in
            switch result {
            case .cancelled:
                ToastMessage.showToast(in: self, message: "login cancelled", bgColor: .white)
            case .success(granted: let granted, declined: let declined, token: let token):
                print("granted:",granted," , declined:",declined," , token",token)
                for permission in granted {
                    if permission == .publicProfile {
                        self.getFBUserData()
                        break
                    }
                }
                break
            case .failed(let error):
                ToastMessage.showToast(in: self, message: error.localizedDescription, bgColor: .white)
            }
        }
    }
    //MARK: action Apple login
    @objc func btnAppleLoginClicked(_ sender: UIButton) {
        self.setupProviderLoginView()
    }
    @objc func btnEyeClicked(_ sender: UIButton) {
        if btnEye.image(for: .normal) == UIImage.init(named: "Passwordeye") {
            txtPassword.isSecureTextEntry = false
            btnEye.setImage(UIImage.init(named: "EyeShow"), for: .normal)
        }else {
            btnEye.setImage(UIImage.init(named: "Passwordeye"), for: .normal)
            txtPassword.isSecureTextEntry = true
        }
    }
    
    @objc func btnSignInWithApple(_ sender: UIButton) {

    }
    
    @objc  func btnSignUpClicked(_ sender: Any) {
        let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpEmailPhoneVC") as! SignUpEmailPhoneVC
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    
    @objc func btnForgetPasswordClicked(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgetPasswordEmailPhoneVC") as! ForgetPasswordEmailPhoneVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
   
    //MARK: Api
    
    func apiLogin() {
        let url = URL(string: Constants.BASE_URL+Constants.LOGIN)!
        DialougeUtils.addActivityView(view: self.view)
        let parameters = ["email_phone_username": txtUserName.text!, "password": txtPassword.text!, "device_id": "cghjkhjgvhh"]

        self.loginAPI(parameters, url: url)
    }
    
    func loginAPI(_ param: Parameters, url: URL){
        LoginSignUpAPI.loginCustomer(vc: self, parameter: param, url: url) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            let dictData:[String:Any] = result!["user_data"] as! [String: Any]
                            let objdata: AuthUserLoginData = AuthUserLoginData.init(loginInfo: dictData)!
                            self.arrLoginData.add(objdata)
                            User.sharedInstance.auth_key = String(describing: objdata.auth_key!)
                            User.sharedInstance.id = String(describing: objdata.id!)
                            User.sharedInstance.username = String(describing: objdata.username!)
                            User.sharedInstance.refresh_token = String(describing: objdata.refresh_token!)
                            User.sharedInstance.login = true
                            
                            var user_data = dictData as? [String : Any] ?? [:]
                            user_data["country_code"] = nil
                            UserDefaults.standard.set(user_data, forKey: Constants.KEYS.LoginResponse)
                            UserDefaults.standard.set(true, forKey: Constants.KEYS.isLoggedIn)
                            UserDefaults.standard.synchronize()
                            
                            if (UserDefaults.standard.value(forKey: Constants.KEYS.LoginResponse) != nil)
                            {
                                let dict = UserDefaults.standard.value(forKey: Constants.KEYS.LoginResponse) as! [String : AnyObject]
                                print(dict)
                                APIManager.shared.headers["token"] = (dict["refresh_token"] as! String)
                                print("headers\(APIManager.shared.headers)")
                                self.updateData()
                            }
                           
                        }else{
                            let message:String = result!["message"] as! String
                            ToastMessage.showToast(in: self, message: message, bgColor: .white)
                        }
                    }
                } else {
                    self.showAlertMessage("server error")
                }
            }
        }
    }
    
    func getFBUserData() {
        if let token = AccessToken.current {
            let parameters = ["fields": "id, name, picture.type(large), first_name, last_name"]
            GraphRequest(graphPath: "me", parameters: parameters).start { (connection, result, err) in
                if let error = err {
                    print("getFBUserData error:",error.localizedDescription)
                    ToastMessage.showToast(in: self, message: error.localizedDescription, bgColor: .white)
                }
                else {
                    print("getFBUserData result:",result)
                    if let resultObj = result as? [String : Any] {
                        var params = [String : String]()
                        params["facebook_id"] = resultObj["id"] as? String ?? ""
                        params["first_name"] = resultObj["first_name"] as? String ?? ""
                        params["last_name"] = resultObj["last_name"] as? String ?? ""
                        params["type"] = "1"
                        params["device_id"] = UserDefaults.standard.string(forKey: Constants.KEYS.FCMID) ?? ""
                        params["imei_number"] = ""
                        params["lat"] = ""
                        params["lng"] = ""
                        params["country_name"] = ""
                        params["ip_address"] = ""
                        print("login params:",params)
                        self.loginWithFacebook(params)
                    }
                }
            }
        }
    }
    
    func loginWithApple(_ params: [String : String]) {
        let url = URL(string: Constants.BASE_URL+Constants.LOGIN_APPLE)!
        DialougeUtils.addActivityView(view: self.view)
        self.facebookAPI(params, url: url)
    }

    func loginWithFacebook(_ params: [String : String]) {
        let url = URL(string: Constants.BASE_URL+Constants.LOGIN_FACEBOOK)!
        DialougeUtils.addActivityView(view: self.view)
        self.facebookAPI(params, url: url)
    }
    
    func facebookAPI(_ param: Parameters, url: URL) {
        LoginSignUpAPI.loginCustomer(vc: self, parameter: param, url: url) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            let dictData:[String:Any] = result!["user_data"] as! [String: Any]
                            let objdata: AuthUserLoginData = AuthUserLoginData.init(loginInfo: dictData)!
                            self.arrLoginData.add(objdata)
                            User.sharedInstance.auth_key = String(describing: objdata.auth_key ?? "")
                            User.sharedInstance.id = String(describing: objdata.id ?? "")
                            User.sharedInstance.username = String(describing: objdata.username ?? "")
                            User.sharedInstance.refresh_token = String(describing: objdata.refresh_token ?? "")
                            User.sharedInstance.login = true

                            var user_data = dictData as? [String : Any] ?? [:]
                            user_data["country_code"] = nil
                            UserDefaults.standard.set(user_data, forKey: Constants.KEYS.LoginResponse)
                            UserDefaults.standard.set(true, forKey: Constants.KEYS.isLoggedIn)
                            UserDefaults.standard.synchronize()
                            
                            if (UserDefaults.standard.value(forKey: Constants.KEYS.LoginResponse) != nil)
                            {
                                let dict = UserDefaults.standard.value(forKey: Constants.KEYS.LoginResponse) as! [String : AnyObject]
                                print(dict)
                                APIManager.shared.headers["token"] = (dict["refresh_token"] as! String)
                                print("headers\(APIManager.shared.headers)")
                                self.updateData()
                            }
                        }else{
                            let message:String = result!["message"] as! String
                            ToastMessage.showToast(in: self, message: message, bgColor: .white)
                        }
                    }
                } else {
                    self.showAlertMessage("server error")
                }
            }
        }
    }
    
    func setupProviderLoginView() {
        DialougeUtils.addActivityView(view: self.view)

        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()

    }
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            
            // Create an account in your system.
            let userIdentifier = appleIDCredential.user
            let fullName = appleIDCredential.fullName?.givenName ?? ""
            let lastName = appleIDCredential.fullName?.middleName ?? ""
            let email = appleIDCredential.email
            
            let socialID = (appleIDCredential.user)
            let name : String = fullName
            //self.SocielLogin()
            var params = [String : String]()
            params["apple_id"] = socialID
            params["first_name"] = name
            params["last_name"] = lastName
            params["type"] = "1"
            params["device_id"] = UserDefaults.standard.string(forKey: Constants.KEYS.FCMID) ?? ""
            params["imei_number"] = ""
            params["lat"] = ""
            params["lng"] = ""
            params["country_name"] = ""
            params["ip_address"] = ""
            print(params)
            self.loginWithApple(params)
        default:
            break
        }
    }
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
    }
    func performExistingAccountSetupFlows() {
        // Prepare requests for both Apple ID and password providers.
        let requests = [ASAuthorizationAppleIDProvider().createRequest(),
                        ASAuthorizationPasswordProvider().createRequest()]
        
        // Create an authorization controller with the given requests.
        let authorizationController = ASAuthorizationController(authorizationRequests: requests)
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }

    

}

