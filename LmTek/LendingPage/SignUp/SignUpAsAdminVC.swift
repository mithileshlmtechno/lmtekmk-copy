//
//  SignUpAsAdminVC.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 05/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD

@available(iOS 13.0, *)
class SignUpAsAdminVC: BaseViewController {

    @IBOutlet weak var btnSignUp: UIButton!
    var strSearchName: String = ""
    @IBOutlet weak var lblUserName: UILabel!
    var arrLoginData:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        lblUserName.text = "Sign up as " + strSearchName
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSignUpClicked(_ sender: UIButton) {
        self.getUserDetails()
    }
    @IBAction func btnLoginClicked(_ sender: UIButton) {
        self.GotoLogin()
    }
    
    @IBAction func btnTermsConditionClicked(_ sender: UIButton) {
    }
    @objc func gotoViewContyroller() {
        if #available(iOS 13.0, *) {
            if let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate {
                //sceneDelegate.window?.rootViewController = navController
                //sceneDelegate.window?.makeKeyAndVisible()
                sceneDelegate.showHomeScreen()
            }
        } else {
            if let appDelegate =  UIApplication.shared.delegate as? AppDelegate {
                //appdelegate.window?.rootViewController = navController
                //appdelegate.window?.makeKeyAndVisible()
                appDelegate.showHomeScreen()
            }
        }
    }
    func getUserDetails(){
        
        let url = URL(string: Constants.BASE_URL+Constants.GET_USER_DETAILS)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        DialougeUtils.addActivityView(view: self.view)
        self.getUserDataAPI(url, header: headers)

    }
    
    func getUserDataAPI(_ url: URL, header: HTTPHeaders){
        LoginSignUpAPI.getUserDataAPI(vc: self, url: url, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            let message:String = result!["message"] as! String
                            let dictData:[String:Any] = result!["data"] as! [String: Any]
                            let objdata: AuthUserLoginData = AuthUserLoginData.init(loginInfo: dictData)!
                            self.arrLoginData.add(objdata)
                            User.sharedInstance.id = String(describing: objdata.id!)
                            User.sharedInstance.username = String(describing: objdata.username!)
                            
                            var user_data = dictData as? [String : Any] ?? [:]
                            user_data["country_code"] = nil
                            UserDefaults.standard.set(user_data, forKey: Constants.KEYS.LoginResponse)
                            UserDefaults.standard.set(true, forKey: Constants.KEYS.isLoggedIn)
                            UserDefaults.standard.synchronize()
                            
                            if (UserDefaults.standard.value(forKey: Constants.KEYS.LoginResponse) != nil)
                            {
                                let dict = UserDefaults.standard.value(forKey: Constants.KEYS.LoginResponse) as! [String : AnyObject]
                                print(dict)
                                APIManager.shared.headers["token"] = (dict["refresh_token"] as! String)
                                print("headers\(APIManager.shared.headers)")
                                ToastMessage.showToast(in: self, message: message, bgColor: .white)
                                Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.gotoViewContyroller), userInfo: nil, repeats: false)

                            }
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                            ToastMessage.showToast(in: self, message: message, bgColor: .white)
                        }
                    }
                } else {
                    self.showAlertMessage("server error")
                }
            }
        }
    }
    //MARK: Go to Login
    func GotoLogin() {
        var vcFound:Bool = false
        if let viewControllers = self.navigationController?.viewControllers {
              for vc in viewControllers {
                   if vc.isKind(of: LoginUserVC.classForCoder()) {
                    vcFound = true
                    self.navigationController?.popToViewController(vc, animated: true)
                    break;
                   }
              }
        }
        if(vcFound == false) {
            let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginUserVC") as! LoginUserVC
            self.navigationController?.pushViewController(signUpVC, animated: true)
        }

    }

}
@available(iOS 13.0, *)
extension SignUpAsAdminVC {
    func showHud(_ message: String) {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = message
        hud.isUserInteractionEnabled = false
        _ = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.hideHUD), userInfo: nil, repeats: false)
    }
    
    @objc func hideHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
        if #available(iOS 13.0, *) {
            if let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate {
                //sceneDelegate.window?.rootViewController = navController
                //sceneDelegate.window?.makeKeyAndVisible()
                sceneDelegate.showHomeScreen()
            }
        } else {
            if let appDelegate =  UIApplication.shared.delegate as? AppDelegate {
                //appdelegate.window?.rootViewController = navController
                //appdelegate.window?.makeKeyAndVisible()
                appDelegate.showHomeScreen()
            }
        }
    }
}
