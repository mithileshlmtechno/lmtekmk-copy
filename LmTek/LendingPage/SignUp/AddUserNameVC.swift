//
//  AddUserNameVC.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 05/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD

@available(iOS 13.0, *)
class AddUserNameVC: BaseViewController, UITextFieldDelegate {

    @IBOutlet weak var txtAdminName: UITextField!
    @IBOutlet weak var btnNext: UIButton!
    var checkUserName: Bool = false
    var strSearchName: String = ""
    var strSuggestName: String = ""
    var checkUpdateUserName: Bool = false
    var arrLoginData:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        txtAdminName.delegate = self
        strSearchName = User.sharedInstance.username ?? ""
        checkUserName = true
        self.updateUI()
    }
    
    func updateUI() {
        txtAdminName.setLeftPaddingPoints(10)
        txtAdminName.tintColor = .white
        txtAdminName.attributedPlaceholder = NSAttributedString(string: "Admin Name", attributes: [NSAttributedString.Key.foregroundColor: colorGray])
        txtAdminName.text = strSearchName
        strSuggestName = strSearchName
    }
    
    @IBAction func btnNextClicked(_ sender: UIButton) {
        if checkUserName == false {
            ToastMessage.showToast(in: self, message: "Please enter other user name", bgColor: .white)
        }else {
            if strSearchName.isEmpty == true {
                ToastMessage.showToast(in: self, message: "Please enter user name", bgColor: .white)
            }else {
                if checkUpdateUserName == false {
                    self.getUserDetails()
                }else{
                    self.updateUserNameAPI()
                }
            }
        }
    }
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnLoginClicked(_ sender: UIButton) {
        self.GotoLogin()
    }
    
    @IBAction func btnReloadClicked(_ sender: UIButton) {
        txtAdminName.text = strSuggestName
        checkUpdateUserName = false
    }
    
    //MARK: Delegate Textfield
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        checkUpdateUserName = true
        if (range.length == 0)
        {
            strSearchName = strSearchName+string
        }
        if range.length == 1{
            _ = strSearchName.remove(at: strSearchName.index(before: strSearchName.endIndex))
        }
        if strSearchName.isEmpty != true {
            self.searchUserNameFromAPI()
        }
        return true
    }
    
    @objc func gotoViewContyroller() {
//        if #available(iOS 13.0, *) {
//            if let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate {
//                //sceneDelegate.window?.rootViewController = navController
//                //sceneDelegate.window?.makeKeyAndVisible()
//                sceneDelegate.showHomeScreen()
//            }
//        } else {
//            if let appDelegate =  UIApplication.shared.delegate as? AppDelegate {
//                //appdelegate.window?.rootViewController = navController
//                //appdelegate.window?.makeKeyAndVisible()
//                appDelegate.showHomeScreen()
//            }
//        }
        let chooseLanguageVC = self.storyboard?.instantiateViewController(withIdentifier: "ChooseLanguageVC") as! ChooseLanguageVC
        chooseLanguageVC.isAfterLogin = true
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(chooseLanguageVC, animated: true)
        }

    }
    
    func updateUserNameAPI() {
        let url = URL(string: Constants.BASE_URL+Constants.UPDATE_USERNAME)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        let parameters: Parameters = ["username": strSearchName]
        DialougeUtils.addActivityView(view: self.view)
        print(parameters)
        self.ApiupdateUserName(parameters, url: url, header: headers)
    }
    
    func searchUserNameFromAPI() {
        let url = URL(string: Constants.BASE_URL+Constants.SEARCH_USERNAME)!
        let parameters: Parameters = ["username": strSearchName]
        self.searchUserNameAPI(parameters, url: url)
    }
    
    func searchUserNameAPI(_ param: Parameters, url: URL) {
        LoginSignUpAPI.allPostAPIS(vc: self, parameter: param, url: url) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            let _:String = result!["message"] as! String
                            self.checkUserName = true
                        }else{
                            self.checkUserName = false
                            let message:String = result!["message"] as! String
                            print(message)
                            ToastMessage.showToast(in: self, message: message, bgColor: .white)
                        }
                    }
                } else {
                    self.showAlertMessage("server error")
                }
            }
        }
    }
    
    func ApiupdateUserName(_ param: Parameters, url: URL, header: HTTPHeaders) {
        LoginSignUpAPI.SendOTPAPI(vc: self, parameter: param, url: url, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            User.sharedInstance.username = self.strSearchName
                            self.getUserDetails()
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                            ToastMessage.showToast(in: self, message: message, bgColor: .white)
                        }
                    }
                } else {
                    self.showAlertMessage("server error")
                }
            }
        }
    }
    
    func getUserDetails() {
        let url = URL(string: Constants.BASE_URL+Constants.GET_USER_DETAILS)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        self.getUserDataAPI(url, header: headers)
    }
    
    func getUserDataAPI(_ url: URL, header: HTTPHeaders){
        LoginSignUpAPI.getUserDataAPI(vc: self, url: url, header: header) { result, error in
            DispatchQueue.main.async {
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            let message:String = result!["message"] as! String
                            let dictData:[String:Any] = result!["data"] as! [String: Any]
                            let objdata: AuthUserLoginData = AuthUserLoginData.init(loginInfo: dictData)!
                            self.arrLoginData.add(objdata)
                            User.sharedInstance.id = String(describing: objdata.id!)
                            User.sharedInstance.username = String(describing: objdata.username!)
                            
                            var user_data = dictData as? [String : Any] ?? [:]
                            user_data["country_code"] = nil
                            UserDefaults.standard.set(user_data, forKey: Constants.KEYS.LoginResponse)
                            UserDefaults.standard.set(true, forKey: Constants.KEYS.isLoggedIn)
                            UserDefaults.standard.synchronize()
                            
                            if (UserDefaults.standard.value(forKey: Constants.KEYS.LoginResponse) != nil)
                            {
                                let dict = UserDefaults.standard.value(forKey: Constants.KEYS.LoginResponse) as! [String : AnyObject]
                                print(dict)
                                APIManager.shared.headers["token"] = (dict["refresh_token"] as! String)
                                print("headers\(APIManager.shared.headers)")
                                ToastMessage.showToast(in: self, message: message, bgColor: .white)
                                Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.gotoViewContyroller), userInfo: nil, repeats: false)

                            }
                        }else{
                            let message:String = result!["message"] as! String
                            ToastMessage.showToast(in: self, message: message, bgColor: .white)
                        }
                    }
                } else {
                    self.showAlertMessage("server error")
                }
            }
        }
    }
    //MARK: Go to Login
    func GotoLogin() {
        var vcFound:Bool = false
        if let viewControllers = self.navigationController?.viewControllers {
              for vc in viewControllers {
                   if vc.isKind(of: LoginUserVC.classForCoder()) {
                    vcFound = true
                    self.navigationController?.popToViewController(vc, animated: true)
                    break;
                   }
              }
        }
        if(vcFound == false) {
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "LoginUserVC") as! LoginUserVC
            self.navigationController?.pushViewController(VC, animated: true)
        }

    }
}

@available(iOS 13.0, *)
extension AddUserNameVC {
    func showHud(_ message: String) {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = message
        hud.isUserInteractionEnabled = false
        _ = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.hideHUD), userInfo: nil, repeats: false)
    }
    
    @objc func hideHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
//        let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpAsAdminVC") as! SignUpAsAdminVC
//        self.navigationController?.pushViewController(signUpVC, animated: true)
        if #available(iOS 13.0, *) {
            if let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate {
                //sceneDelegate.window?.rootViewController = navController
                //sceneDelegate.window?.makeKeyAndVisible()
                sceneDelegate.showHomeScreen()
            }
        } else {
            if let appDelegate =  UIApplication.shared.delegate as? AppDelegate {
                //appdelegate.window?.rootViewController = navController
                //appdelegate.window?.makeKeyAndVisible()
                appDelegate.showHomeScreen()
            }
        }

    }
}
