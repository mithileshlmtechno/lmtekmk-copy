//
//  SignUpAddYourNameVC.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 05/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SignUpAddYourNameVC: BaseViewController,UITextFieldDelegate {

    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet var LblLogin: UILabel!
    var checkEmailPhone: String = ""
    var emailPhone: String = ""
    var strUserName: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        txtName.delegate = self
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.tapLabel(_:)))
        tap3.numberOfTapsRequired = 1
        LblLogin.addGestureRecognizer(tap3)
        LblLogin.isUserInteractionEnabled = true

        self.updateUI()
    }
    
    func updateUI() {
        txtName.setLeftPaddingPoints(10)
        txtName.tintColor = .white
        txtName.attributedPlaceholder = NSAttributedString(string: "Name", attributes: [NSAttributedString.Key.foregroundColor: colorGray])

    }
    @objc func tapLabel(_ gesture: UITapGestureRecognizer) {
    let termsRange = ("Already have an account  Login" as NSString).range(of: "Login")
    // comment for now
    //let privacyRange = (text as NSString).range(of: "Privacy Policy")

    if gesture.didTapAttributedTextInLabel(label: LblLogin, inRange: termsRange) {
        print(" tapped Login")
        self.GotoLogin()
    } else {
        print("Tapped none")
    }
    }

    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNextClicked(_ sender: Any) {
        if txtName.text?.isEmpty == true {
            ToastMessage.showToast(in: self, message: "Please Enter Name", bgColor: .white)
        }else {
            strUserName = txtName.text!
            let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpPasswordVC") as! SignUpPasswordVC
            signUpVC.strName =  strUserName
            signUpVC.checkEmailPhone =  checkEmailPhone
            signUpVC.emailPhone =  emailPhone
            self.navigationController?.pushViewController(signUpVC, animated: true)
        }
    }
    //MARK: Delegate Textfield
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtName {
            btnNext.backgroundColor = colorYellow
        }
    }
    //MARK: Go to Login
    func GotoLogin() {
        var vcFound:Bool = false
        if let viewControllers = self.navigationController?.viewControllers {
              for vc in viewControllers {
                   if vc.isKind(of: LoginUserVC.classForCoder()) {
                    vcFound = true
                    self.navigationController?.popToViewController(vc, animated: true)
                    break;
                   }
              }
        }
        if(vcFound == false) {
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "LoginUserVC") as! LoginUserVC
            self.navigationController?.pushViewController(VC, animated: true)
        }

    }

}
