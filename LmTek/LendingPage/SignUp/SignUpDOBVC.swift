//
//  SignUpDOBVC.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 05/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD

@available(iOS 13.0, *)
class SignUpDOBVC: BaseViewController {
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    var viewCalander = UIButton()
    var datepicker: UIDatePicker = UIDatePicker()
    var viewBG:UIView = UIView()

    @IBOutlet weak var viewDateBG: UIView!
    var year:Int = 0
    var checkDOB: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnBack.isHidden = true
        self.view.backgroundColor = .black
        self.updateUI()
    }
    
    func updateUI() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleUserTap(_:)))
        viewDateBG.addGestureRecognizer(tap)
        viewDateBG.isUserInteractionEnabled = true
    }
    
    //MARK: Action
    
    @objc func handleUserTap(_ sender: UITapGestureRecognizer? = nil) {
        self.OpenCalander()
    }
    
    @IBAction func btnNextClciked(_ sender: UIButton) {
        if checkDOB == true {
            self.apiAddDOB()
        }else {
            ToastMessage.showToast(in: self, message: "Please select date of birth", bgColor: .white)
        }
    }
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func openBackgroundview() {
        viewBG = UIView()
        viewBG.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        viewBG.backgroundColor = .clear
        self.view.addSubview(viewBG)
    }
    
    func OpenCalander(){
        
        viewCalander = UIButton()
        viewCalander.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        viewCalander.backgroundColor = .black.withAlphaComponent(0.4)
        viewCalander.addTarget(self, action: #selector(self.hidePickerClicked(_:)), for: .touchUpInside)
        self.view.addSubview(viewCalander)
        
        let sLayer:CAShapeLayer = CAShapeLayer()
        let viewUpTime:UIView = UIView()
        viewUpTime.frame = CGRect(x: 0, y: screenHeight-230, width: screenWidth, height: 50)
        viewUpTime.backgroundColor = UIColor.init(red: 64/255.0, green: 64/255.0, blue: 64/255.0, alpha: 1)
        viewUpTime.ShaplayerUp(sLayer, corner: 8)
        viewCalander.addSubview(viewUpTime)
        
        let btnDone:UIButton = UIButton()
        btnDone.frame = CGRect(x: viewUpTime.frame.size.width-100, y: 0, width: 100, height: 50)
        btnDone.setTitle("Done", for: .normal)
        btnDone.titleLabel?.font = fontSizeSemiBoldTitle
        btnDone.setTitleColor(colorYellow, for: .normal)
        btnDone.addTarget(self, action: #selector(self.btnDoneClicked(_:)), for: .touchUpInside)
        viewUpTime.addSubview(btnDone)
        
        
        let viewpickerBG:UIView = UIView()
        viewpickerBG.frame = CGRect(x: 0, y: screenHeight-180, width: screenWidth, height: 180)
        viewpickerBG.backgroundColor = UIColor.init(red: 64/255.0, green: 64/255.0, blue: 64/255.0, alpha: 1)
        viewCalander.addSubview(viewpickerBG)
        
        let lblLine:UIView = UIView()
        lblLine.frame = CGRect(x: 0, y: screenHeight-181, width: screenWidth, height: 1)
        lblLine.backgroundColor = UIColor.lightGray
        viewCalander.addSubview(lblLine)

        datepicker.frame = CGRect(x: 0, y: screenHeight-180, width: screenWidth, height: 180)
        datepicker.clipsToBounds = true
        datepicker.backgroundColor = .clear
        datepicker.setValue(UIColor.white, forKeyPath: "textColor")
        datepicker.datePickerMode = .date
        datepicker.preferredDatePickerStyle = .wheels
        datepicker.maximumDate = Date()
        datepicker.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
        viewCalander.addSubview(datepicker)
    }
    
    func currentYear() {
        let calendar = NSCalendar.current
        let date = NSDate()
        year = calendar.component(.year, from: date as Date)
        lblYear.text = String(year)
    }
    
    @objc func hidePickerClicked(_ sender: UIButton){
        viewCalander.removeFromSuperview()
        datepicker.removeFromSuperview()
    }
    
    @objc func btnDoneClicked(_ sender: UIButton){
        viewCalander.removeFromSuperview()
        datepicker.removeFromSuperview()
    }
    
    @objc func datePickerValueChanged(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        let strDate:String = dateFormatter.string(from: sender.date)
        let selectedDate: Date = dateFormatter.date(from: strDate)!
        
        lblDate.text = strDate
        
        let diffInDays = Calendar.current.dateComponents([.year], from: selectedDate, to: Date()).year
        lblYear.text = String(diffInDays!) + " Year"
        checkDOB = true
    }
    @objc func gotoViewContyroller() {
        self.viewBG.removeFromSuperview()
        let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "AddUserNameVC") as! AddUserNameVC
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    //MARK: Api
    
    func apiAddDOB() {
        self.openBackgroundview()
        let url = URL(string: Constants.BASE_URL+Constants.ADD_DOB)!
        DialougeUtils.addActivityView(view: self.view)
        let parameters = ["dob": lblDate.text!]
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        self.DOBAPI(parameters, url: url, header: headers)
    }
    
    func DOBAPI(_ param: Parameters, url: URL, header: HTTPHeaders){
        LoginSignUpAPI.SendOTPAPI(vc: self, parameter: param, url: url, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            let message:String = result!["message"] as! String
                            ToastMessage.showToast(in: self, message: message, bgColor: .white)
                            Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.gotoViewContyroller), userInfo: nil, repeats: false)

                        }else{
                            self.viewBG.removeFromSuperview()
                            let message:String = result!["message"] as! String
                            print(message)
                            ToastMessage.showToast(in: self, message: message, bgColor: .white)
                        }
                    }
                } else {
                    self.viewBG.removeFromSuperview()
                    self.showAlertMessage("server error")
                }
            }
        }
    }
}

@available(iOS 13.0, *)
extension SignUpDOBVC {
    func showHud(_ message: String) {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = message
        hud.isUserInteractionEnabled = false
        _ = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.hideHUD), userInfo: nil, repeats: false)
    }
    
    @objc func hideHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
        let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "AddUserNameVC") as! AddUserNameVC
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
}
