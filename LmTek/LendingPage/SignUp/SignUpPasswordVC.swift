//
//  SignUpPasswordVC.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 05/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD

@available(iOS 13.0, *)
class SignUpPasswordVC: BaseViewController, UITextFieldDelegate {

    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var btnSavePassword: UIButton!
    
    var strName: String = ""
    var checkEmailPhone: String = ""
    var emailPhone: String = ""
    var arrLoginData:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnCheckBox.isHidden = true
        btnSavePassword.isHidden = true
        self.view.backgroundColor = .black
        btnNext.setTitle("Register", for: .normal)
        self.updateUI()
    }
    
    func updateUI() {
        txtPassword.setLeftPaddingPoints(10)
        txtPassword.tintColor = .white
        txtPassword.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: colorGray])
        txtPassword.isSecureTextEntry = true
        txtPassword.delegate = self

    }
    @IBAction func btnNextClicked(_ sender: UIButton) {
        if txtPassword.text?.isEmpty == true {
            ToastMessage.showToast(in: self, message: "Please Enter Password", bgColor: .white)
        }else {
            self.apiRegisterUser()
        }
    }
    
    @IBAction func btnLoginClicked(_ sender: UIButton) {
        self.GotoLogin()
    }
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: delegate uitextfield delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        btnNext.alpha = 1
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if txtPassword.text == "" || txtPassword.text?.isEmpty == true {
            btnNext.alpha = 0.5
        } else {
            btnNext.alpha = 1
        }
    }
    @objc func gotoViewContyroller() {
        let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpDOBVC") as! SignUpDOBVC
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    //MARK: Api
    
    func apiRegisterUser(){
        let url = URL(string: Constants.BASE_URL+Constants.REGISTER_USER)!
        DialougeUtils.addActivityView(view: self.view)
        let parameters = ["name": strName, "email_phone": emailPhone, "password": txtPassword.text!, "device_id": "hasgcjhasc"]
        self.registerUserAPI(parameters, url: url)
    }
    
    func registerUserAPI(_ param: Parameters, url: URL){
        LoginSignUpAPI.allPostAPIS(vc: self, parameter: param, url: url) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            let message:String = result!["message"] as! String
                            let dictData:[String:Any] = result!["user_data"] as! [String: Any]
                            let objdata: AuthUserLoginData = AuthUserLoginData.init(loginInfo: dictData)!
                            self.arrLoginData.add(objdata)
                            User.sharedInstance.auth_key = String(describing: objdata.auth_key!)
                            User.sharedInstance.id = String(describing: objdata.id!)
                            User.sharedInstance.username = String(describing: objdata.username!)
                            User.sharedInstance.refresh_token = String(describing: objdata.refresh_token!)
                            User.sharedInstance.login = true

                            var user_data = dictData as? [String : Any] ?? [:]
                            user_data["country_code"] = nil
                            UserDefaults.standard.set(user_data, forKey: Constants.KEYS.LoginResponse)
                            UserDefaults.standard.set(true, forKey: Constants.KEYS.isLoggedIn)
                            UserDefaults.standard.synchronize()
                            
                            if (UserDefaults.standard.value(forKey: Constants.KEYS.LoginResponse) != nil)
                            {
                                let dict = UserDefaults.standard.value(forKey: Constants.KEYS.LoginResponse) as! [String : AnyObject]
                                print(dict)
                                APIManager.shared.headers["token"] = (dict["refresh_token"] as! String)
                                print("headers\(APIManager.shared.headers)")
                                ToastMessage.showToast(in: self, message: message, bgColor: .white)
                                Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.gotoViewContyroller), userInfo: nil, repeats: false)
                            }

                        }else{
                            let message:String = result!["message"] as! String
                            ToastMessage.showToast(in: self, message: message, bgColor: .white)
                        }
                    }
                } else {
                    self.showAlertMessage("server error")
                }
            }
        }
    }


    //MARK: Go to Login
    func GotoLogin() {
        var vcFound:Bool = false
        if let viewControllers = self.navigationController?.viewControllers {
              for vc in viewControllers {
                   if vc.isKind(of: LoginUserVC.classForCoder()) {
                    vcFound = true
                    self.navigationController?.popToViewController(vc, animated: true)
                    break;
                   }
              }
        }
        if(vcFound == false) {
            let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginUserVC") as! LoginUserVC
            self.navigationController?.pushViewController(signUpVC, animated: true)
        }

    }
}

@available(iOS 13.0, *)
extension SignUpPasswordVC {
    func showHud(_ message: String) {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = message
        hud.isUserInteractionEnabled = false
        _ = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.hideHUD), userInfo: nil, repeats: false)
    }
    
    @objc func hideHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
        let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpDOBVC") as! SignUpDOBVC
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
}
