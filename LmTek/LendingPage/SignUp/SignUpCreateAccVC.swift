//
//  SignUpCreateAccVC.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 05/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SignUpCreateAccVC: UIViewController {

    @IBOutlet weak var btnToLanguageClicked: UIButton!
    @IBOutlet weak var btnCreateNewClicked: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
    }
    

    @IBAction func btnCreateNextClicked(_ sender: UIButton) {
    }
    
    
    @IBAction func btnLoginClicked(_ sender: UIButton) {
        self.GotoLogin()
    }
    //MARK: Go to Login
    func GotoLogin() {
        var vcFound:Bool = false
        if let viewControllers = self.navigationController?.viewControllers {
              for vc in viewControllers {
                   if vc.isKind(of: LoginUserVC.classForCoder()) {
                    vcFound = true
                    self.navigationController?.popToViewController(vc, animated: true)
                    break;
                   }
              }
        }
        if(vcFound == false) {
            let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginUserVC") as! LoginUserVC
            self.navigationController?.pushViewController(signUpVC, animated: true)
        }

    }

}
