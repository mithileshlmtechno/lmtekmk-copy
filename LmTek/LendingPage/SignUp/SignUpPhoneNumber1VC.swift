//
//  SignUpPhoneNumber1VC.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 05/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SignUpPhoneNumber1VC: UIViewController {
    var yRef:CGFloat = 50.0
    var  btnTopLanguage: UIButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        yRef = (screenHeight/2)-120
        self.screenDesigning()
    }
    
    func screenDesigning() {
                
        btnTopLanguage = UIButton()
        btnTopLanguage.frame = CGRect(x: (screenWidth-250)/2, y: 50.0, width: 250, height: 30)
        btnTopLanguage.backgroundColor = UIColor.clear
        btnTopLanguage.setTitle("English (india)  ", for: .normal)
        btnTopLanguage.setImage(UIImage.init(named: "DownArrow"), for: .normal)
        btnTopLanguage.setTitleColor(colorGray, for: .normal)
        btnTopLanguage.titleLabel?.font = fontSizeRegulor
        btnTopLanguage.clipsToBounds = true
        self.view.addSubview(btnTopLanguage)
        
        btnTopLanguage.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        btnTopLanguage.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        btnTopLanguage.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)

        let imgUser:UIImageView = UIImageView()
        imgUser.frame = CGRect(x: (screenWidth-90)/2, y: yRef, width: 90, height: 90)
        imgUser.backgroundColor = UIColor.clear
        imgUser.image = UIImage.init(named: "AppLogo")
        imgUser.contentMode = .scaleAspectFill
        self.view.addSubview(imgUser)
        
        yRef = yRef+imgUser.frame.size.height+10

        let lblBloopers = UILabel()
        lblBloopers.frame = CGRect(x: (screenWidth-200)/2, y: yRef, width: 200, height: 30)
        lblBloopers.backgroundColor = UIColor.clear
        lblBloopers.text = "Bloopers"
        lblBloopers.textColor = colorYellow
        lblBloopers.textAlignment = .center
        lblBloopers.font = fontSizeSemiBoldTitle
        lblBloopers.clipsToBounds = true
        self.view.addSubview(lblBloopers)
        
        yRef = yRef+lblBloopers.frame.size.height+5

        let lblMovemts = UILabel()
        lblMovemts.frame = CGRect(x: (screenWidth-200)/2, y: yRef, width: 200, height: 25)
        lblMovemts.backgroundColor = UIColor.clear
        lblMovemts.text = "Live The Moments"
        lblMovemts.textColor = colorGray
        lblMovemts.textAlignment = .center
        lblMovemts.font = fontSizeRegulor
        lblMovemts.clipsToBounds = true
        self.view.addSubview(lblMovemts)
        
        yRef = yRef+lblMovemts.frame.size.height+45
        
        let btnCreateAccount: UIButton = UIButton()
        btnCreateAccount.frame = CGRect(x: 15, y: yRef, width: screenWidth-30, height: 40)
        btnCreateAccount.backgroundColor = colorYellow
        btnCreateAccount.setTitle("Create New Account", for: .normal)
        btnCreateAccount.setTitleColor(.black, for: .normal)
        btnCreateAccount.titleLabel?.font = fontSizeSemiBoldTitle
        btnCreateAccount.layer.cornerRadius = 4
        btnCreateAccount.clipsToBounds = true
        btnCreateAccount.addTarget(self, action: #selector(self.btnCreateAccountClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnCreateAccount)

        yRef = yRef+btnCreateAccount.frame.size.height+10

        let btnLogin: UIButton = UIButton()
        btnLogin.frame = CGRect(x: 15, y: yRef, width: screenWidth-30, height: 40)
        btnLogin.backgroundColor = .clear
        btnLogin.setTitle("Login", for: .normal)
        btnLogin.setTitleColor(colorYellow, for: .normal)
        btnLogin.titleLabel?.font = fontSizeSemiBoldTitle
        btnLogin.layer.cornerRadius = 4
        btnLogin.clipsToBounds = true
        btnLogin.addTarget(self, action: #selector(self.btnLoginClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnLogin)

    }
    
    @objc func btnCreateAccountClicked(_ sender : UIButton){
        let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpEmailPhoneVC") as! SignUpEmailPhoneVC
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    @objc func btnLoginClicked(_ sender: UIButton) {
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "LoginUserVC") as! LoginUserVC
        self.navigationController?.pushViewController(VC, animated: true)

    }
}
