//
//  OTPVC.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 05/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD

@available(iOS 13.0, *)
class AllOTPVC: BaseViewController,UITextFieldDelegate {

    @IBOutlet weak var lblOTPText: UILabel!
    @IBOutlet weak var txtFirst: UITextField!
    @IBOutlet weak var txtSecond: UITextField!
    @IBOutlet weak var txtThird: UITextField!
    @IBOutlet weak var txtForth: UITextField!
    var forgetPass:Bool = false
    var checkEmailPhone: String = ""
    var emailPhone: String = ""
    var strOTP: String = ""
    var countyPhoneCode:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        UIBarButtonItem.appearance().tintColor = UIColor.black
        self.updateUI()
    }
    func updateUI() {
        if checkEmailPhone == "phone" {
            lblOTPText.text = "Enter OTP Sent to your mobile number " + emailPhone
        }else if checkEmailPhone == "email" {
            lblOTPText.text = "Enter OTP Sent to your email address " + emailPhone
        }
        txtFirst.delegate = self
        txtSecond.delegate = self
        txtThird.delegate = self
        txtForth.delegate = self

        txtFirst.keyboardType = .numberPad
        txtSecond.keyboardType = .numberPad
        txtThird.keyboardType = .numberPad
        txtForth.keyboardType = .numberPad
        
        if strOTP.isEmpty == true{
        }else{
            for i in 0..<strOTP.count{
                if i == 0{
                    txtFirst.text = String(strOTP.character(i))
                } else if i == 1 {
                    txtSecond.text = String(strOTP.character(i))
                } else if i == 2 {
                    txtThird.text = String(strOTP.character(i))
                }else {
                    txtForth.text = String(strOTP.character(i))
                }
            }
        }
    }
    @IBAction func btnNextClicked(_ sender: UIButton) {
        if forgetPass == true {
            if txtFirst.text?.isEmpty == true || txtSecond.text?.isEmpty == true || txtThird.text?.isEmpty == true || txtForth.text?.isEmpty == true {
                ToastMessage.showToast(in: self, message: "Please Enter OTP", bgColor: .white)
            }else {
                self.forgetPasswordAPI()
            }
        }else {
            if txtFirst.text?.isEmpty == true || txtSecond.text?.isEmpty == true || txtThird.text?.isEmpty == true || txtForth.text?.isEmpty == true {
                ToastMessage.showToast(in: self, message: "Please Enter OTP", bgColor: .white)
            }else {
                self.apiOTOVerify()
            }
        }
    }
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnResentOTPClicked(_ sender: UIButton) {
        self.apiSendOTP()
    }
    
    @IBAction func btnLoginClicked(_ sender: UIButton) {
        self.GotoLogin()
    }
    //MARK: TextField Delegate Method
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if (range.length == 0)
        {
            if (textField == txtFirst)
            {
                txtFirst.text = string
                txtSecond.becomeFirstResponder()
            }
            if (textField == txtSecond)
            {
                txtSecond.text = string
                txtThird.becomeFirstResponder()
            }
            if (textField == txtThird)
            {
                txtThird.text = string
                txtForth.becomeFirstResponder()
            }
            if (textField == txtForth)
            {
                txtForth.text = string
                txtForth.resignFirstResponder()
            }
            print("0")
        }
        return true

    }
    
    @objc func gotoViewContyroller() {
        if forgetPass == true{
            let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgetConfirmPasswordVC") as! ForgetConfirmPasswordVC
            self.navigationController?.pushViewController(signUpVC, animated: true)

        }else {
            let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpAddYourNameVC") as! SignUpAddYourNameVC
            signUpVC.checkEmailPhone = checkEmailPhone
            signUpVC.emailPhone = emailPhone
            self.navigationController?.pushViewController(signUpVC, animated: true)
        }
    }
    //MARK: Api
    
    func apiOTOVerify(){
        let url = URL(string: Constants.BASE_URL+Constants.VERIFY_OTP)!
        DialougeUtils.addActivityView(view: self.view)
        let parameters = ["email_phone": emailPhone, "otp": strOTP]

        self.verifyOTPAPI(parameters, url: url)
    }
    
    func gotoSetPassword(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgetConfirmPasswordVC") as! ForgetConfirmPasswordVC
        vc.emailPhone = emailPhone
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func forgetPasswordAPI(){
        let url = URL(string: Constants.BASE_URL+Constants.FORGET_PASSWORD)!
        DialougeUtils.addActivityView(view: self.view)
        let parameters = ["email_phone": emailPhone, "otp": strOTP, "device_id": "553e768"]
        print(parameters)
        self.forgetPasswordAPICall(parameters, url: url)
    }
    
    func forgetPasswordAPICall(_ param: Parameters, url: URL){
        LoginSignUpAPI.allPostAPIS(vc: self, parameter: param, url: url) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status: Bool = result!["status"] as! Bool
                        if status == true{
                            let dictData:[String:Any] = result!["user_data"] as! [String: Any]
                            let objdata: AuthUserLoginData = AuthUserLoginData.init(loginInfo: dictData)!
                            User.sharedInstance.auth_key = String(describing: objdata.auth_key!)
                            User.sharedInstance.id = String(describing: objdata.id!)
                            User.sharedInstance.username = String(describing: objdata.username!)
                            User.sharedInstance.refresh_token = String(describing: objdata.refresh_token!)
                            self.gotoSetPassword()
                        }else{
                            let message:String = result!["message"] as! String
                            ToastMessage.showToast(in: self, message: message, bgColor: .white)
                        }
                    }
                } else {
                    self.showAlertMessage("server error")
                }
            }
        }
    }
    func verifyOTPAPI(_ param: Parameters, url: URL){
        LoginSignUpAPI.allPostAPIS(vc: self, parameter: param, url: url) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            let message:String = result!["message"] as! String
                            ToastMessage.showToast(in: self, message: message, bgColor: .white)
                            Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.gotoViewContyroller), userInfo: nil, repeats: false)

                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                            ToastMessage.showToast(in: self, message: message, bgColor: .white)
                        }
                    }
                } else {
                    self.showAlertMessage("server error")
                }
            }
        }
    }
    func apiSendOTP() {
        
        let url = URL(string: Constants.BASE_URL+Constants.GET_OTP)!
        let parameters = ["email_phone": emailPhone, "country_code": countyPhoneCode]
        DialougeUtils.addActivityView(view: self.view)
        print(parameters)
        self.loginAPI(parameters, url: url)
    }
    
    func loginAPI(_ param: Parameters, url: URL){
        LoginSignUpAPI.loginCustomer(vc: self, parameter: param, url: url) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status: Bool = result!["status"] as! Bool
                        if status == true{
                            let dictData:[String:Any] = result!["user_data"] as! [String: Any]
                            let objdata: AuthUserGetOTPData = AuthUserGetOTPData.init(loginInfo: dictData)!
                            self.strOTP = String(describing: objdata.otp!)
                            ToastMessage.showToast(in: self, message: result!["message"] as! String, bgColor: .white)
                            Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.gotoOTP), userInfo: nil, repeats: false)
                        }else{
                            let message:String = result!["message"] as! String
                            ToastMessage.showToast(in: self, message: message, bgColor: .white)
                        }
                    }
                } else {
                    self.showAlertMessage("server error")
                }
            }
        }
    }

    //MARK: Go to Login
    func GotoLogin() {
        var vcFound:Bool = false
        if let viewControllers = self.navigationController?.viewControllers {
              for vc in viewControllers {
                   if vc.isKind(of: LoginUserVC.classForCoder()) {
                    vcFound = true
                    self.navigationController?.popToViewController(vc, animated: true)
                    break;
                   }
              }
        }
        if(vcFound == false) {
            let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginUserVC") as! LoginUserVC
            self.navigationController?.pushViewController(signUpVC, animated: true)
        }

    }
    
    @objc func gotoOTP() {
        self.updateUI()
    }
}

@available(iOS 13.0, *)
extension AllOTPVC {
    func showHud(_ message: String) {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = message
        hud.isUserInteractionEnabled = false
        _ = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.hideHUD), userInfo: nil, repeats: false)
    }
    
    @objc func hideHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
        if forgetPass == true{
            let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgetConfirmPasswordVC") as! ForgetConfirmPasswordVC
            self.navigationController?.pushViewController(signUpVC, animated: true)

        }else {
            let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpAddYourNameVC") as! SignUpAddYourNameVC
            signUpVC.checkEmailPhone = checkEmailPhone
            signUpVC.emailPhone = emailPhone
            self.navigationController?.pushViewController(signUpVC, animated: true)
        }
    }
}
