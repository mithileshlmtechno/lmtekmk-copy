//
//  ApisMethod.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 09/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import Foundation
import Alamofire



class LoginSignUpAPI {

    //MARK: Email phone Username
        
    class func loginCustomer(vc: UIViewController, parameter: Parameters,url: URL, completetionBlock: @escaping ([String : Any]?, _ error:NSError?) -> Void){
                
        let urlstring:String = String(describing: url)
        print(urlstring)
        
        Alamofire.upload(multipartFormData: { multipartFormData in
                        
            for (key, value) in parameter {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to: url){ (result) in
            switch result {
            case .success(let upload, _, _):

                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })

                upload.responseJSON { response in
                    print(response.result.value ?? "")
                    if let err = response.error{
                        print(err)
                        return
                    }
                    let json:NSDictionary = response.value as! NSDictionary
                    print(json)
                    let status:Bool = json["status"] as! Bool
                    if status != true{
                        if response.response?.statusCode == 200 {
                            completetionBlock((json as! [String: Any]),nil)
                        } else {
                            completetionBlock((json as! [String: Any]),nil)
                        }
                    } else{
                        completetionBlock((json as! [String: Any]),nil)
                    }
                }

            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    
class func allPostAPIS(vc: UIViewController, parameter: Parameters,url: URL, completetionBlock: @escaping ([String : Any]?, _ error:NSError?) -> Void){
            
    let urlstring:String = String(describing: url)
    print(urlstring)
    
    Alamofire.upload(multipartFormData: { multipartFormData in
                    
        for (key, value) in parameter {
            multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
        }
    },
                     to: url){ (result) in
        switch result {
        case .success(let upload, _, _):

            upload.uploadProgress(closure: { (progress) in
                print("Upload Progress: \(progress.fractionCompleted)")
            })

            upload.responseJSON { response in
                print(response.result.value ?? "")
                if let err = response.error{
                    print(err)
                    return
                }
                let json:NSDictionary = response.value as! NSDictionary
                print(json)
                let status:Bool = json["status"] as! Bool
                if status != true{
                    if response.response?.statusCode == 200 {
                        completetionBlock((json as! [String: Any]),nil)
                    } else {
                        completetionBlock((json as! [String: Any]),nil)
                    }
                } else{
                    completetionBlock((json as! [String: Any]),nil)
                }
            }

        case .failure(let encodingError):
            print(encodingError)
        }
    }
}
    
    class func SendOTPAPI(vc: UIViewController, parameter: Parameters, url: URL, header: HTTPHeaders, completetionBlock: @escaping ([String : Any]?, _ error:NSError?) -> Void){
            
    let urlstring:String = String(describing: url)
    print(urlstring)
    
    Alamofire.upload(multipartFormData: { multipartFormData in
                    
        for (key, value) in parameter {
            multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
        }
    },
                     to: url, headers: header){ (result) in
        switch result {
        case .success(let upload, _, _):

            upload.uploadProgress(closure: { (progress) in
                print("Upload Progress: \(progress.fractionCompleted)")
            })

            upload.responseJSON { response in
                print(response.result.value ?? "")
                if let err = response.error{
                    print(err)
                    return
                }
                let json:NSDictionary = response.value as! NSDictionary
                print(json)
                let status:Bool = json["status"] as! Bool
                if status != true{
                    if response.response?.statusCode == 200 {
                        completetionBlock((json as! [String: Any]),nil)
                    } else {
                        completetionBlock((json as! [String: Any]),nil)
                    }
                } else{
                    completetionBlock((json as! [String: Any]),nil)
                }
            }

        case .failure(let encodingError):
            print(encodingError)
        }
    }
}
    
    class func getUserDataAPI(vc: UIViewController, url : URL, header: HTTPHeaders, completetionBlock: @escaping ([String : Any]?, _ error:NSError?) -> Void){
        
        Handler.handlerGetMethodAPI(url, header: header) { status, result,error  in
            if status == true{
                completetionBlock((result ),nil)
            }else{
                completetionBlock((nil ), nil)
            }
        }

    }
    class func getViewUserDataAPI(vc: UIView, url : URL, header: HTTPHeaders, completetionBlock: @escaping ([String : Any]?, _ error:NSError?) -> Void){
        
        Handler.handlerGetMethodAPI(url, header: header) { status, result,error  in
            if status == true{
                completetionBlock((result ),nil)
            }else{
                completetionBlock((nil ), nil)
            }
        }

    }

    class func getAllUseDataAPI(vc: UIViewController, url : URL, header: HTTPHeaders, completetionBlock: @escaping ([String : Any]?, _ error:NSError?) -> Void){
        
        Handler.handlerGetMethodAPI(url, header: header) { status, result,error  in
            if status == true{
                completetionBlock((result ),nil)
            }else{
                completetionBlock((nil ), nil)
            }
        }

    }

}

class AllUserApis {
    
    class func uploadUserCoverPhotoAPI(vc: UIViewController, url: URL,imgData: NSData, header: HTTPHeaders, completetionBlock: @escaping ([String : Any]?, _ error:NSError?) -> Void){
            
    let urlstring:String = String(describing: url)
    print(urlstring)
    
    Alamofire.upload(multipartFormData: { multipartFormData in
        
        multipartFormData.append(imgData as Data, withName: "cover_photo", fileName: "\(Date().timeIntervalSince1970).png", mimeType: "image/png")

    },
                     to: url, headers: header){ (result) in
        switch result {
        case .success(let upload, _, _):

            upload.uploadProgress(closure: { (progress) in
                print("Upload Progress: \(progress.fractionCompleted)")
            })

            upload.responseJSON { response in
                print(response.result.value ?? "")
                if let err = response.error{
                    print(err)
                    return
                }
                let json:NSDictionary = response.value as! NSDictionary
                print(json)
                let status:Bool = json["status"] as! Bool
                if status != true{
                    if response.response?.statusCode == 200 {
                        completetionBlock((json as! [String: Any]),nil)
                    } else {
                        completetionBlock((json as! [String: Any]),nil)
                    }
                } else{
                    completetionBlock((json as! [String: Any]),nil)
                }
            }

        case .failure(let encodingError):
            print(encodingError)
        }
    }
}
    
    class func uploadUserProfilePhotoAPI(vc: UIViewController, url: URL,imgData: NSData, header: HTTPHeaders, completetionBlock: @escaping ([String : Any]?, _ error:NSError?) -> Void){
            
    let urlstring:String = String(describing: url)
    print(urlstring)
    
    Alamofire.upload(multipartFormData: { multipartFormData in
        
        multipartFormData.append(imgData as Data, withName: "profile_picture", fileName: "\(Date().timeIntervalSince1970).png", mimeType: "image/png")

    },
                     to: url, headers: header){ (result) in
        switch result {
        case .success(let upload, _, _):

            upload.uploadProgress(closure: { (progress) in
                print("Upload Progress: \(progress.fractionCompleted)")
            })

            upload.responseJSON { response in
                print(response.result.value ?? "")
                if let err = response.error{
                    print(err)
                    return
                }
                let json:NSDictionary = response.value as! NSDictionary
                print(json)
                let status:Bool = json["status"] as! Bool
                if status != true{
                    if response.response?.statusCode == 200 {
                        completetionBlock((json as! [String: Any]),nil)
                    } else {
                        completetionBlock((json as! [String: Any]),nil)
                    }
                } else{
                    completetionBlock((json as! [String: Any]),nil)
                }
            }

        case .failure(let encodingError):
            print(encodingError)
        }
    }
}
    
    class func uploadVideoAPI(vc: UIViewController, url: URL,imgData: NSData, header: HTTPHeaders,videoUrl: URL, params: Parameters, completetionBlock: @escaping ([String : Any]?, _ error:NSError?) -> Void){
            
    let urlstring:String = String(describing: url)
    print(urlstring)
    
    Alamofire.upload(multipartFormData: { multipartFormData in
        multipartFormData.append(videoUrl, withName: "video_url", fileName: "\(Date().timeIntervalSince1970).mp4", mimeType: "video/mp4")

        multipartFormData.append(imgData as Data, withName: "thumb_url", fileName: "\(Date().timeIntervalSince1970).png", mimeType: "image/png")
        
        for (key, value) in params {
            multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
        }

    },to: url, headers: header){ (result) in
        switch result {
        case .success(let upload, _, _):

            upload.uploadProgress(closure: { (progress) in
                print("Upload Progress: \(progress.fractionCompleted)")
            })

            upload.responseJSON { response in
                print(response.result.value ?? "")
                if let err = response.error{
                    print(err)
                    return
                }
                let json:NSDictionary = response.value as! NSDictionary
                print(json)
                let status:Bool = json["status"] as! Bool
                if status != true{
                    if response.response?.statusCode == 200 {
                        completetionBlock((json as! [String: Any]),nil)
                    } else {
                        completetionBlock((json as! [String: Any]),nil)
                    }
                } else{
                    completetionBlock((json as! [String: Any]),nil)
                }
            }

        case .failure(let encodingError):
            print(encodingError)
        }
    }
}
    class func allPostDataWithoutImageAPI(vc: UIViewController, url: URL,parameter: Parameters, header: HTTPHeaders, completetionBlock: @escaping ([String : Any]?, _ error:NSError?) -> Void){
            
    let urlstring:String = String(describing: url)
    print(urlstring)
    
    Alamofire.upload(multipartFormData: { multipartFormData in
        
        for (key, value) in parameter {
            multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
        }
    },
                     to: url, headers: header){ (result) in
        switch result {
        case .success(let upload, _, _):

            upload.uploadProgress(closure: { (progress) in
                print("Upload Progress: \(progress.fractionCompleted)")
            })

            upload.responseJSON { response in
                print(response.result.value ?? "")
                if let err = response.error{
                    print(err)
                    return
                }
                let json:NSDictionary = response.value as! NSDictionary
                print(json)
                let status:Bool = json["status"] as! Bool
                if status != true{
                    if response.response?.statusCode == 200 {
                        completetionBlock((json as! [String: Any]),nil)
                    } else {
                        completetionBlock((json as! [String: Any]),nil)
                    }
                } else{
                    completetionBlock((json as! [String: Any]),nil)
                }
            }

        case .failure(let encodingError):
            print(encodingError)
        }
    }
}
    
    class func allPostDatalViewWithoutImageAPI(vc: UIView, url: URL,parameter: Parameters, header: HTTPHeaders, completetionBlock: @escaping ([String : Any]?, _ error:NSError?) -> Void){
            
    let urlstring:String = String(describing: url)
    print(urlstring)
    
    Alamofire.upload(multipartFormData: { multipartFormData in
        
        for (key, value) in parameter {
            multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
        }
    },
                     to: url, headers: header){ (result) in
        switch result {
        case .success(let upload, _, _):

            upload.uploadProgress(closure: { (progress) in
                print("Upload Progress: \(progress.fractionCompleted)")
            })

            upload.responseJSON { response in
                print(response.result.value ?? "")
                if let err = response.error{
                    print(err)
                    return
                }
                let json:NSDictionary = response.value as! NSDictionary
                print(json)
                let status:Bool = json["status"] as! Bool
                if status != true{
                    if response.response?.statusCode == 200 {
                        completetionBlock((json as! [String: Any]),nil)
                    } else {
                        completetionBlock((json as! [String: Any]),nil)
                    }
                } else{
                    completetionBlock((json as! [String: Any]),nil)
                }
            }

        case .failure(let encodingError):
            print(encodingError)
        }
    }
}
}
class Handler {
    
    class func handlerGetMethodAPI(_ url: URL, header: HTTPHeaders, handler: @escaping (Bool, [String : Any]?, AFError?) -> Void) {
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            
            switch response.result
            {
            case .success(let json):
                if response.response?.statusCode == 200 {
                    handler(true, json as? [String : Any], nil)
                } else {
                    handler(true, json as? [String : Any], nil)
                }
                
                break;
            case .failure(let error):
                handler(false, nil, error as? AFError)
                break
            }
        }
    }
    
    class func handlerPostMethodAPI(_ url: URL, parameter: Parameters, handler: @escaping (Bool, [String : Any]?, AFError?) -> Void) {
         Alamofire.request(url, method: .post, parameters: parameter, encoding: JSONEncoding.default).responseJSON { response in
             
             switch response.result
             {
             case .success(let json):
                 if response.response?.statusCode == 200 {
                     handler(true, json as? [String : Any], nil)
                 } else {
                     handler(true, json as? [String : Any], nil)
                 }
                 break;
             case .failure(let error):
                 handler(false, nil, error as? AFError)
                 break
             }
         }
         
     }

}
