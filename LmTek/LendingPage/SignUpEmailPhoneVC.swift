//
//  SignUpEmailPhoneVC.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 04/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit
import Alamofire
import CountryPickerView
import MBProgressHUD

@available(iOS 13.0, *)
class SignUpEmailPhoneVC: BaseViewController,CountryPickerViewDelegate, CountryPickerViewDataSource {

    var yRef:CGFloat = 90.0
    var lblLine:UILabel = UILabel()
    var txtPhoneNo:UITextField = UITextField()
    var txtEmail:UITextField = UITextField()
    var pickerCountry: CountryPickerView!
    var countyPhoneCode:String = ""
    var checkTab: String = "email"
    var emailPhone: String = ""
    var strOTP: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        self.screenDesigning()
    }
    
    func screenDesigning() {
        
        let imgUser:UIImageView = UIImageView()
        imgUser.frame = CGRect(x: (screenWidth-90)/2, y: yRef, width: 90, height: 90)
        imgUser.backgroundColor = UIColor.clear
        imgUser.image = UIImage.init(named: "UserWhite")
        imgUser.contentMode = .scaleAspectFill
        self.view.addSubview(imgUser)

        yRef = yRef+imgUser.frame.size.height+5
        
        let lblName:UILabel = UILabel()
        lblName.frame = CGRect(x: 20, y: yRef, width: screenWidth-40, height: 25)
        lblName.text = "Sign Up"
        lblName.font = fontSizeBoldTitle
        lblName.textAlignment = .center
        lblName.textColor = colorGray
        lblName.clipsToBounds = true
        self.view.addSubview(lblName)

        yRef = yRef+lblName.frame.size.height+25

        let btnEmail:UIButton = UIButton()
        btnEmail.frame = CGRect(x: 15, y: yRef, width: (screenWidth/2)-30, height: 30)
        btnEmail.backgroundColor = UIColor.clear
        btnEmail.setTitle("Email", for: .normal)
        btnEmail.setTitleColor(colorGray, for: .normal)
        btnEmail.titleLabel?.font = fontSizeRegulor
        btnEmail.clipsToBounds = true
        btnEmail.addTarget(self, action: #selector(self.btnEmailClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnEmail)

        let btnPhone:UIButton = UIButton()
        btnPhone.frame = CGRect(x: (screenWidth/2)+15, y: yRef, width: (screenWidth/2)-30, height: 30)
        btnPhone.backgroundColor = UIColor.clear
        btnPhone.setTitle("Phone", for: .normal)
        btnPhone.setTitleColor(colorGray, for: .normal)
        btnPhone.titleLabel?.font = fontSizeRegulor
        btnPhone.clipsToBounds = true
        btnPhone.addTarget(self, action: #selector(self.btnPhoneClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnPhone)

        yRef = yRef+btnPhone.frame.size.height+8

        let lblBGLine:UILabel = UILabel()
        lblBGLine.frame = CGRect(x: 15, y: yRef, width: screenWidth-30, height: 1)
        lblBGLine.backgroundColor = colorGray.withAlphaComponent(0.25)
        lblBGLine.clipsToBounds = true
        self.view.addSubview(lblBGLine)
        
        print(yRef)
        
        lblLine = UILabel()
        lblLine.frame = CGRect(x: 15, y: yRef-0.5, width: (screenWidth/2)-30, height: 2)
        lblLine.backgroundColor = colorYellow
        lblLine.clipsToBounds = true
        self.view.addSubview(lblLine)

        yRef = yRef+lblLine.frame.size.height+25

        txtPhoneNo = UITextField()
        txtPhoneNo.frame = CGRect(x: 15, y: yRef, width: screenWidth-30, height: 40)
        txtPhoneNo.backgroundColor = colorGray.withAlphaComponent(0.25)
        txtPhoneNo.attributedPlaceholder = NSAttributedString(string: "Phone", attributes: [NSAttributedString.Key.foregroundColor: colorGray])
        txtPhoneNo.textColor = UIColor.white
        txtPhoneNo.layer.cornerRadius = 4
        txtPhoneNo.keyboardType = .numberPad
        txtPhoneNo.setLeftPaddingPoints(130)
        txtPhoneNo.setRightPaddingPoints(10)
        txtPhoneNo.font = fontSizeRegulor
        txtPhoneNo.clipsToBounds = true
        self.view.addSubview(txtPhoneNo)
        
        pickerCountry = CountryPickerView()
        pickerCountry.frame = CGRect(x: 20, y: yRef+5, width: 120, height: 30)
        pickerCountry.backgroundColor = UIColor.clear
        pickerCountry.font = fontSizeSemiBoldTitle
        pickerCountry.tintColor = .white
        pickerCountry.countryDetailsLabel.textColor = .white
        pickerCountry.delegate = self
        pickerCountry.dataSource = self
        self.view.addSubview(pickerCountry)

        countyPhoneCode = pickerCountry.selectedCountry.phoneCode
        print(pickerCountry.selectedCountry.phoneCode)

        let lblLinePass: UIButton = UIButton()
        lblLinePass.frame = CGRect(x: 125, y: 5, width: 1, height: 30)
        lblLinePass.backgroundColor = colorGray.withAlphaComponent(0.5)
        txtPhoneNo.addSubview(lblLinePass)

        txtEmail = UITextField()
        txtEmail.frame = CGRect(x: 15, y: yRef, width: screenWidth-30, height: 40)
        txtEmail.backgroundColor = colorGray.withAlphaComponent(0.25)
        txtEmail.attributedPlaceholder = NSAttributedString(string: "Email Address", attributes: [NSAttributedString.Key.foregroundColor: colorGray])
        txtEmail.setLeftPaddingPoints(10)
        txtEmail.setRightPaddingPoints(10)
        txtEmail.keyboardType = .emailAddress
        txtEmail.textColor = UIColor.white
        txtEmail.font = fontSizeRegulor
        txtEmail.layer.cornerRadius = 4
        txtEmail.clipsToBounds = true
        self.view.addSubview(txtEmail)
        
        txtPhoneNo.isHidden = true
        pickerCountry.isHidden = true
        
        yRef = yRef+txtEmail.frame.size.height+15

        let lblRecieveOtp:UILabel = UILabel()
        lblRecieveOtp.frame = CGRect(x: 15, y: yRef, width: screenWidth-30, height: 60)
        lblRecieveOtp.backgroundColor = UIColor.clear
        lblRecieveOtp.text = "You will recieve an OTP code for security purpose"
        lblRecieveOtp.textColor = colorGray
        lblRecieveOtp.font = fontSizeRegulor
        lblRecieveOtp.textAlignment = .center
        lblRecieveOtp.clipsToBounds = true
        lblRecieveOtp.numberOfLines = 0
        lblRecieveOtp.lineBreakMode = .byWordWrapping
        self.view.addSubview(lblRecieveOtp)

        yRef = yRef+lblRecieveOtp.frame.size.height+40
        
        let btnNext: UIButton = UIButton()
        btnNext.frame = CGRect(x: 15, y: yRef, width: screenWidth-30, height: 40)
        btnNext.backgroundColor = colorYellow
        btnNext.setTitle("Next", for: .normal)
        btnNext.setTitleColor(.black, for: .normal)
        btnNext.titleLabel?.font = fontSizeRegulor
        btnNext.layer.cornerRadius = 4
        btnNext.clipsToBounds = true
        btnNext.addTarget(self, action: #selector(self.btnNextClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnNext)

        let lblLineBottom:UILabel = UILabel()
        lblLineBottom.frame = CGRect(x: 0, y: screenHeight-70, width: screenWidth, height: 1)
        lblLineBottom.backgroundColor = colorGray.withAlphaComponent(0.25)
        lblLineBottom.clipsToBounds = true
        self.view.addSubview(lblLineBottom)

        let lblDontHave:UILabel = UILabel()
        lblDontHave.frame = CGRect(x: 35, y: screenHeight-55, width: (screenWidth/2.0)+20, height: 25)
        lblDontHave.backgroundColor = UIColor.clear
        lblDontHave.text = "Already have an account?"
        lblDontHave.textColor = UIColor.lightGray
        lblDontHave.textAlignment = .right
        lblDontHave.font = fontSizeRegulor
        self.view.addSubview(lblDontHave)

        let btnSignIn:UIButton = UIButton()
        btnSignIn.frame = CGRect(x: (screenWidth/2.0)+40, y: screenHeight-55, width: 90, height: 25)
        btnSignIn.setTitle("Login", for: .normal)
        btnSignIn.setTitleColor(colorYellow, for: .normal)
        btnSignIn.titleLabel?.font = fontSizeSemiBoldTitle
        btnSignIn.addTarget(self, action: #selector(self.btnLoginClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnSignIn)

    }
    //MARK: Action
    @objc func btnLoginClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func btnNextClicked(_ sender: UIButton) {
        if checkTab == "email" {
            if txtEmail.text?.isEmpty == true {
                ToastMessage.showToast(in: self, message: "Please Enter Email Address", bgColor: .white)
            } else {
                if isValidEmail(txtEmail.text ?? "") == true {
                    countyPhoneCode = ""
                    emailPhone = txtEmail.text!
                    self.apiCheckEmailorPhone()
                } else {
                    ToastMessage.showToast(in: self, message: "Please Enter Valid Email", bgColor: .white)
                }
            }
        }else if checkTab == "phone" {
            if txtPhoneNo.text?.isEmpty == true {
                ToastMessage.showToast(in: self, message: "Please Enter Phone Number", bgColor: .white)
            }else {
                let strPhNo: String = txtPhoneNo.text ?? ""
                if strPhNo.length >= 10 {
                    emailPhone = txtPhoneNo.text!
                    self.apiCheckEmailorPhone()
                } else {
                    ToastMessage.showToast(in: self, message: "Please Enter Valid Phone Number", bgColor: .white)
                }
            }
        } else {
        }
//        self.gotoNextPage()
    }
    @objc func btnPhoneClicked(_ sender: UIButton) {
        checkTab = "phone"
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations: {
            self.lblLine.frame = CGRect(x: screenWidth/2, y: 273, width: (screenWidth/2)-15, height: 2)
            self.txtEmail.isHidden = true
            self.txtPhoneNo.isHidden = false
            self.pickerCountry.isHidden = false
        }, completion: nil)

    }
    
    @objc func btnEmailClicked(_ sender: UIButton) {
        checkTab = "email"
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations: {
            self.lblLine.frame = CGRect(x: 15, y: 273, width: (screenWidth/2)-15, height: 2)
            self.txtEmail.isHidden = false
            self.txtPhoneNo.isHidden = true
            self.pickerCountry.isHidden = true
        }, completion: nil)

    }
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        print("Country = ",country)
        countyPhoneCode = country.phoneCode
        pickerCountry.textColor = .white
        pickerCountry.countryDetailsLabel.textColor = .white
    }
    //MARK: Goto Next
    func gotoNextPage() {
    }
    @objc  func gotoOTP() {
        let OTPVC = self.storyboard?.instantiateViewController(withIdentifier: "AllOTPVC") as! AllOTPVC
        OTPVC.checkEmailPhone = checkTab
        OTPVC.emailPhone = emailPhone
        OTPVC.strOTP = strOTP
        OTPVC.forgetPass = false
        self.navigationController?.pushViewController(OTPVC, animated: true)

    }
    //MARK: Api
    
    func apiCheckEmailorPhone() {
        let url = URL(string: Constants.BASE_URL+Constants.CHECK_EMAIL_PHONE)!
        DialougeUtils.addActivityView(view: self.view)
        let parameters = ["email_phone": emailPhone]
        print(parameters)
        self.checkEmailPhoneAPI(parameters, url: url)
    }
    func apiSendOTP() {
        
        let url = URL(string: Constants.BASE_URL+Constants.GET_OTP)!
        let parameters = ["email_phone": emailPhone, "country_code": countyPhoneCode]
        DialougeUtils.addActivityView(view: self.view)
        print(parameters)
        self.loginAPI(parameters, url: url)
    }
    
    func loginAPI(_ param: Parameters, url: URL){
        LoginSignUpAPI.loginCustomer(vc: self, parameter: param, url: url) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status: Bool = result!["status"] as! Bool
                        if status == true{
                            let dictData:[String:Any] = result!["user_data"] as! [String: Any]
                            let objdata: AuthUserGetOTPData = AuthUserGetOTPData.init(loginInfo: dictData)!
                            self.strOTP = String(describing: objdata.otp!)
                            ToastMessage.showToast(in: self, message: result!["message"] as! String, bgColor: .white)
                            Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.gotoOTP), userInfo: nil, repeats: false)
                        }else{
                            let message:String = result!["message"] as! String
                            ToastMessage.showToast(in: self, message: message, bgColor: .white)
                        }
                    }
                } else {
                    self.showAlertMessage("server error")
                }
            }
        }
    }
    
    
    func checkEmailPhoneAPI(_ param: Parameters, url: URL){
        LoginSignUpAPI.allPostAPIS(vc: self, parameter: param, url: url) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status: Bool = result!["status"] as! Bool
                        if status == true{
                            self.apiSendOTP()
                        }else{
                            let message:String = result!["message"] as! String
                            ToastMessage.showToast(in: self, message: message, bgColor: .white)
                        }
                    }
                } else {
                    self.showAlertMessage("server error")
                }
            }
        }
    }
}
@available(iOS 13.0, *)
extension SignUpEmailPhoneVC {
    func showHud(_ message: String) {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = message
        hud.isUserInteractionEnabled = false
        _ = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.hideHUD), userInfo: nil, repeats: false)
    }
    
    @objc func hideHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
        let OTPVC = self.storyboard?.instantiateViewController(withIdentifier: "AllOTPVC") as! AllOTPVC
        OTPVC.checkEmailPhone = checkTab
        OTPVC.emailPhone = emailPhone
        OTPVC.strOTP = strOTP
        OTPVC.forgetPass = false
        self.navigationController?.pushViewController(OTPVC, animated: true)

    }
}
