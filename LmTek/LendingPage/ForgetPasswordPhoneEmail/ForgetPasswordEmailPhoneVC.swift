//
//  ForgetPasswordEmailPhoneVC.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 04/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit
import Alamofire
import CountryPickerView
import MBProgressHUD

@available(iOS 13.0, *)
class ForgetPasswordEmailPhoneVC: BaseViewController,CountryPickerViewDelegate, CountryPickerViewDataSource{
    
    var yRef:CGFloat = 50.0
    var lblLine:UILabel = UILabel()
    var txtPhoneNo:UITextField = UITextField()
    var txtEmail:UITextField = UITextField()
    var checkEmailPhone: String = ""
    var emailPhone: String = ""
    var pickerCountry: CountryPickerView!
    var countyPhoneCode:String = ""
    var strOTP: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        checkEmailPhone = "email"
        self.screenDesigning()
    }
    
    func screenDesigning() {
        
        let viewscrollView: UIScrollView = UIScrollView()
        viewscrollView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight-80)
        viewscrollView.backgroundColor = UIColor.black
        self.view.addSubview(viewscrollView)

        let imgUser:UIImageView = UIImageView()
        imgUser.frame = CGRect(x: (screenWidth-80)/2, y: yRef, width: 80, height: 80)
        imgUser.backgroundColor = UIColor.clear
        imgUser.image = UIImage.init(named: "User_8")
        imgUser.contentMode = .scaleAspectFill
        viewscrollView.addSubview(imgUser)

        yRef = yRef+imgUser.frame.size.height+30
        
        let lblName:UILabel = UILabel()
        lblName.frame = CGRect(x: 20, y: yRef, width: screenWidth-40, height: 25)
        lblName.text = "Trouble with logging in?"
        lblName.font = fontSizeBoldTitle
        lblName.textAlignment = .center
        lblName.textColor = colorGray
        lblName.font = fontSizeBoldTitle
        lblName.clipsToBounds = true
        viewscrollView.addSubview(lblName)

        yRef = yRef+lblName.frame.size.height+5

        let lblOtpText:UILabel = UILabel()
        lblOtpText.frame = CGRect(x: 20, y: yRef, width: screenWidth-40, height: 80)
        lblOtpText.text = "Enter your phone number and we’ll send you a login code to get back into your account."
        lblOtpText.font = fontSizeRegulor
        lblOtpText.textAlignment = .center
        lblOtpText.textColor = colorGray
        lblOtpText.clipsToBounds = true
        lblOtpText.numberOfLines = 3
        lblOtpText.lineBreakMode = .byWordWrapping
        viewscrollView.addSubview(lblOtpText)

        yRef = yRef+lblOtpText.frame.size.height+25

        let btnEmail:UIButton = UIButton()
        btnEmail.frame = CGRect(x: 15, y: yRef, width: (screenWidth/2)-30, height: 25)
        btnEmail.backgroundColor = UIColor.clear
        btnEmail.setTitle("Email", for: .normal)
        btnEmail.setTitleColor(colorGray, for: .normal)
        btnEmail.titleLabel?.font = fontSizeRegulor
        btnEmail.clipsToBounds = true
        btnEmail.addTarget(self, action: #selector(self.btnEmailClicked(_:)), for: .touchUpInside)
        viewscrollView.addSubview(btnEmail)

        let btnPhone:UIButton = UIButton()
        btnPhone.frame = CGRect(x: (screenWidth/2)+15, y: yRef, width: (screenWidth/2)-30, height: 25)
        btnPhone.backgroundColor = UIColor.clear
        btnPhone.setTitle("Phone", for: .normal)
        btnPhone.setTitleColor(colorGray, for: .normal)
        btnPhone.titleLabel?.font = fontSizeRegulor
        btnPhone.clipsToBounds = true
        btnPhone.addTarget(self, action: #selector(self.btnPhoneClicked(_:)), for: .touchUpInside)
        viewscrollView.addSubview(btnPhone)

        yRef = yRef+btnPhone.frame.size.height+8

        let lblBGLine:UILabel = UILabel()
        lblBGLine.frame = CGRect(x: 15, y: yRef, width: screenWidth-30, height: 1)
        lblBGLine.backgroundColor = colorGray.withAlphaComponent(0.25)
        lblBGLine.clipsToBounds = true
        viewscrollView.addSubview(lblBGLine)
        
        print(yRef)
        lblLine = UILabel()
        lblLine.frame = CGRect(x: 15, y: yRef-0.5, width: (screenWidth/2)-30, height: 2)
        lblLine.backgroundColor = colorYellow
        lblLine.clipsToBounds = true
        viewscrollView.addSubview(lblLine)

        yRef = yRef+lblLine.frame.size.height+20

        txtEmail = UITextField()
        txtEmail.frame = CGRect(x: 15, y: yRef, width: screenWidth-30, height: 40)
        txtEmail.backgroundColor = colorGray.withAlphaComponent(0.25)
        txtEmail.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: colorGray])
        txtEmail.setLeftPaddingPoints(10)
        txtEmail.setRightPaddingPoints(10)
        txtEmail.textColor = UIColor.white
        txtEmail.layer.cornerRadius = 4
        txtEmail.clipsToBounds = true
        viewscrollView.addSubview(txtEmail)

        txtPhoneNo = UITextField()
        txtPhoneNo.frame = CGRect(x: 15, y: yRef, width: screenWidth-30, height: 40)
        txtPhoneNo.backgroundColor = colorGray.withAlphaComponent(0.25)
        txtPhoneNo.attributedPlaceholder = NSAttributedString(string: "Phone", attributes: [NSAttributedString.Key.foregroundColor: colorGray])
        txtPhoneNo.textColor = UIColor.white
        txtPhoneNo.layer.cornerRadius = 4
        txtPhoneNo.setLeftPaddingPoints(130)
        txtPhoneNo.setRightPaddingPoints(10)
        txtPhoneNo.clipsToBounds = true
        viewscrollView.addSubview(txtPhoneNo)

        pickerCountry = CountryPickerView()
        pickerCountry.frame = CGRect(x: 20, y: yRef+5, width: 120, height: 30)
        pickerCountry.backgroundColor = UIColor.clear
        pickerCountry.font = fontSizeSemiBoldTitle
        pickerCountry.tintColor = .white
        pickerCountry.countryDetailsLabel.textColor = .white
        pickerCountry.delegate = self
        pickerCountry.dataSource = self
        viewscrollView.addSubview(pickerCountry)

        countyPhoneCode = pickerCountry.selectedCountry.phoneCode
        print(pickerCountry.selectedCountry.phoneCode)

        pickerCountry.isHidden = true
        txtPhoneNo.isHidden = true
        
        yRef = yRef+txtEmail.frame.size.height+15
        
        let btnNext: UIButton = UIButton()
        btnNext.frame = CGRect(x: 15, y: yRef, width: screenWidth-30, height: 40)
        btnNext.backgroundColor = colorYellow
        btnNext.setTitle("Next", for: .normal)
        btnNext.setTitleColor(.black, for: .normal)
        btnNext.titleLabel?.font = fontSizeRegulor
        btnNext.layer.cornerRadius = 4
        btnNext.clipsToBounds = true
        btnNext.addTarget(self, action: #selector(self.btnNextClicked(_:)), for: .touchUpInside)
        viewscrollView.addSubview(btnNext)

        yRef = yRef+txtEmail.frame.size.height+15

        let lblRecieveOtp:UILabel = UILabel()
        lblRecieveOtp.frame = CGRect(x: 15, y: yRef, width: screenWidth-30, height: 30)
        lblRecieveOtp.backgroundColor = UIColor.clear
        lblRecieveOtp.text = "Can’t reset password"
        lblRecieveOtp.textColor = colorYellow
        lblRecieveOtp.font = fontSizeRegulor
        lblRecieveOtp.textAlignment = .center
        lblRecieveOtp.clipsToBounds = true
        viewscrollView.addSubview(lblRecieveOtp)

        yRef = yRef+lblRecieveOtp.frame.size.height+30

        let lblLineOr1:UILabel = UILabel()
        lblLineOr1.frame = CGRect(x: 15, y: yRef, width: (screenWidth/2)-35, height: 1)
        lblLineOr1.backgroundColor = colorGray.withAlphaComponent(0.25)
        lblLineOr1.clipsToBounds = true
        viewscrollView.addSubview(lblLineOr1)
        
        let lblOr:UILabel = UILabel()
        lblOr.frame = CGRect(x: (screenWidth/2)-15, y: yRef-10, width: 30, height: 20)
        lblOr.backgroundColor = UIColor.clear
        lblOr.text = "OR"
        lblOr.font = fontSizeRegulor
        lblOr.textColor = .white
        lblOr.textAlignment = .center
        lblOr.clipsToBounds = true
        viewscrollView.addSubview(lblOr)
        
        let lblLineOr2:UILabel = UILabel()
        lblLineOr2.frame = CGRect(x: (screenWidth/2)+20, y: yRef, width: (screenWidth/2)-35, height: 1)
        lblLineOr2.backgroundColor = colorGray.withAlphaComponent(0.25)
        lblLineOr2.clipsToBounds = true
        viewscrollView.addSubview(lblLineOr2)

        yRef = yRef+lblOr.frame.size.height+15

        let btnSignFacebook:UIButton = UIButton()
        btnSignFacebook.frame = CGRect(x: (screenWidth-230)/2, y: yRef, width: 230, height: 25)
        btnSignFacebook.setTitle("  Log in with Facebook", for: .normal)
        btnSignFacebook.setTitleColor(ColorFaceBook, for: .normal)
        btnSignFacebook.titleLabel?.font = fontSizeRegulor
        btnSignFacebook.setImage(UIImage.init(named: "facebookSmall"), for: .normal)
        btnSignFacebook.addTarget(self, action: #selector(self.btnfaceLoginClicked(_:)), for: .touchUpInside)
        viewscrollView.addSubview(btnSignFacebook)
        
        yRef = yRef+btnSignFacebook.frame.size.height+10
        
        let btnAppleSignIn:UIButton = UIButton()
        btnAppleSignIn.frame = CGRect(x: (screenWidth-230)/2, y: yRef, width: 230, height: 25)
        btnAppleSignIn.setTitle("  Log in with Apple", for: .normal)
        btnAppleSignIn.setTitleColor(colorGray, for: .normal)
        btnAppleSignIn.titleLabel?.font = fontSizeRegulor
        btnAppleSignIn.setImage(UIImage.init(named: "AppleLogo"), for: .normal)
        btnAppleSignIn.addTarget(self, action: #selector(self.btnAppleLoginClicked(_:)), for: .touchUpInside)
        viewscrollView.addSubview(btnAppleSignIn)
        
        yRef = yRef+btnAppleSignIn.frame.size.height+15

        viewscrollView.contentSize = CGSize(width: screenWidth, height: yRef)

        let lblLineBottom:UILabel = UILabel()
        lblLineBottom.frame = CGRect(x: 15, y: screenHeight-80, width: screenWidth-30, height: 1)
        lblLineBottom.backgroundColor = colorGray.withAlphaComponent(0.25)
        lblLineBottom.clipsToBounds = true
        self.view.addSubview(lblLineBottom)

        let lblDontHave:UILabel = UILabel()
        lblDontHave.frame = CGRect(x: (screenWidth/2)-80, y: screenHeight-65, width: 80, height: 25)
        lblDontHave.backgroundColor = UIColor.clear
        lblDontHave.text = "Back to "
        lblDontHave.textColor = UIColor.lightGray
        lblDontHave.textAlignment = .right
        lblDontHave.font = fontSizeRegulor
        self.view.addSubview(lblDontHave)

        let btnSignIn:UIButton = UIButton()
        btnSignIn.frame = CGRect(x: (screenWidth/2.0)+5, y: screenHeight-65, width: 50, height: 25)
        btnSignIn.setTitle("Login", for: .normal)
        btnSignIn.setTitleColor(colorYellow, for: .normal)
        btnSignIn.titleLabel?.font = fontSizeRegulor
        btnSignIn.titleLabel?.font = UIFont.init(name: fontBold, size: 17)
        btnSignIn.addTarget(self, action: #selector(self.btnBackLoginClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnSignIn)

    }
    //MARK: Action
    @objc func btnNextClicked(_ sender: UIButton) {
        if checkEmailPhone == "phone" {
            if txtPhoneNo.text?.isEmpty == true || txtPhoneNo.text == "" {
                ToastMessage.showToast(in: self, message: "Please Enter Phone Number", bgColor: .white)
            } else {
                emailPhone = txtPhoneNo.text!
                //self.apiSendOTP()
                self.findUserOTP(emailPhone)
            }
        } else {
            if txtEmail.text?.isEmpty == true || txtEmail.text == "" {
                ToastMessage.showToast(in: self, message: "Please Enter Email", bgColor: .white)
            } else {
                emailPhone = txtPhoneNo.text!
                self.findUserOTP(emailPhone)
            }
        }
    }
    
    @objc func btnPhoneClicked(_ sender: UIButton) {
        checkEmailPhone = "phone"
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations: {
            self.lblLine.frame = CGRect(x: screenWidth/2, y: 328, width: (screenWidth/2)-15, height: 2)
            self.pickerCountry.isHidden = false
            self.txtPhoneNo.isHidden = false
            self.txtEmail.isHidden = true
        }, completion: nil)

    }
    
    @objc func btnEmailClicked(_ sender: UIButton) {
        checkEmailPhone = "email"
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations: {
            self.lblLine.frame = CGRect(x: 15, y: 328, width: (screenWidth/2)-30, height: 2)
            self.pickerCountry.isHidden = true
            self.txtPhoneNo.isHidden = true
            self.txtEmail.isHidden = false
        }, completion: nil)

    }

    @objc func btnfaceLoginClicked(_ sender: UIButton) {
        
    }
    
    @objc func btnAppleLoginClicked(_ sender: UIButton){
        
    }
    @objc func btnBackLoginClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func gotoViewContyroller() {
        let OTPVC = self.storyboard?.instantiateViewController(withIdentifier: "AllOTPVC") as! AllOTPVC
        OTPVC.forgetPass = true
        OTPVC.emailPhone = emailPhone
        OTPVC.checkEmailPhone = checkEmailPhone
        OTPVC.strOTP = strOTP
        self.navigationController?.pushViewController(OTPVC, animated: true)
    }
    //MARK: Picker delegate
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        print("Country = ",country)
        countyPhoneCode = country.phoneCode
        pickerCountry.textColor = .white
        pickerCountry.countryDetailsLabel.textColor = .white
    }

    func findUserOTP(_ emailPhone: String){
        let url = URL(string: Constants.BASE_URL+Constants.FIND_USER)!
        let parameters = ["email_phone_username": emailPhone]
        print(parameters)
        DialougeUtils.addActivityView(view: self.view)
        self.findUserOTPCall(parameters, url: url)
    }
    
    func findUserOTPCall(_ param: Parameters, url: URL){
        LoginSignUpAPI.loginCustomer(vc: self, parameter: param, url: url) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status: Bool = result!["status"] as! Bool
                        if status == true{
                            self.apiSendOTP()
                        }else{
                            let message:String = result!["message"] as! String
                            ToastMessage.showToast(in: self, message: message, bgColor: .white)
                        }
                    }
                } else {
                    ToastMessage.showToast(in: self, message: "server error", bgColor: .white)
                }
            }
        }
    }
    func apiSendOTP(){
        let url = URL(string: Constants.BASE_URL+Constants.GET_OTP)!
        let parameters = ["email_phone": emailPhone, "country_code": countyPhoneCode]
        print(parameters)
        DialougeUtils.addActivityView(view: self.view)
        self.loginAPI(parameters, url: url)
    }
    
    func loginAPI(_ param: Parameters, url: URL){
        LoginSignUpAPI.loginCustomer(vc: self, parameter: param, url: url) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status: Bool = result!["status"] as! Bool
                        if status == true{
                            let dictData:[String:Any] = result!["user_data"] as! [String: Any]
                            let objdata: AuthUserGetOTPData = AuthUserGetOTPData.init(loginInfo: dictData)!
                            self.strOTP = String(describing: objdata.otp!)
                            ToastMessage.showToast(in: self, message: result!["message"] as! String, bgColor: .white)
                            Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.gotoViewContyroller), userInfo: nil, repeats: false)

                        }else{
                            let message:String = result!["message"] as! String
                            ToastMessage.showToast(in: self, message: message, bgColor: .white)
                        }
                    }
                } else {
                    self.showAlertMessage("server error")
                }
            }
        }
    }
    

}
@available(iOS 13.0, *)
extension ForgetPasswordEmailPhoneVC {
    func showHud(_ message: String) {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = message
        hud.isUserInteractionEnabled = false
        _ = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.hideHUD), userInfo: nil, repeats: false)
    }
    
    @objc func hideHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
        let OTPVC = self.storyboard?.instantiateViewController(withIdentifier: "AllOTPVC") as! AllOTPVC
        OTPVC.forgetPass = true
        OTPVC.emailPhone = emailPhone
        OTPVC.checkEmailPhone = checkEmailPhone
        OTPVC.strOTP = strOTP
        self.navigationController?.pushViewController(OTPVC, animated: true)
    }
}
