//
//  ForgetConfirmPasswordVC.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 05/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit
import Alamofire

@available(iOS 13.0, *)
class ForgetConfirmPasswordVC: BaseViewController {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var btnOK: UIButton!
    
    var emailPhone: String = ""
    var strOTP: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        self.updateUI()
    }
    
    func updateUI() {
        txtPassword.setLeftPaddingPoints(10)
        txtConfirmPassword.setLeftPaddingPoints(10)

        txtPassword.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: colorGray])
        txtPassword.isSecureTextEntry = true
        txtConfirmPassword.attributedPlaceholder = NSAttributedString(string: "Confirm Password", attributes: [NSAttributedString.Key.foregroundColor: colorGray])
        txtConfirmPassword.isSecureTextEntry = true

    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnOkClicked(_ sender: UIButton) {
        if txtPassword.text?.isEmpty == true {
            ToastMessage.showToast(in: self, message: "Please Enter Password", bgColor: .white)
        } else if txtConfirmPassword.text?.isEmpty == true {
            ToastMessage.showToast(in: self, message: "Please Enter Confirm Password", bgColor: .white)
        }else {
            self.setPasswordAPI()
        }
    }
    func gotoForgetwithLogin(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgetPassWithLoginVC") as! ForgetPassWithLoginVC
        vc.strConfirmPassword = txtConfirmPassword.text!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func setPasswordAPI(){
        
        let url = URL(string: Constants.BASE_URL+Constants.SET_PASSWORD)!
        DialougeUtils.addActivityView(view: self.view)
        let parameters: Parameters = ["user_id": User.sharedInstance.id ?? "", "password": txtPassword.text!, "confirm_password": txtConfirmPassword.text!]
        print(parameters)
        self.setPasswordAPICall(parameters, url: url)
    }
    
    func setPasswordAPICall(_ param: Parameters, url: URL){
        LoginSignUpAPI.allPostAPIS(vc: self, parameter: param, url: url) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status: Bool = result!["status"] as! Bool
                        if status == true{
//                            let dictData:[String:Any] = result!["user_data"] as! [String: Any]
                            //self.showHud(result!["message"] as! String)
                            self.gotoForgetwithLogin()
                        }else{
                            let message:String = result!["message"] as! String
                            ToastMessage.showToast(in: self, message: message, bgColor: .white)
                        }
                    }
                } else {
                    self.showAlertMessage("server error")
                }
            }
        }
    }
}
