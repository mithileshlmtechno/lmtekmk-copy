//
//  PaswordResetLinkVC.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 05/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class PaswordResetLinkVC: UIViewController {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var btnOK: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
    }
    
    func updateUI() {

    }

    @IBAction func btnOKClicked(_ sender: UIButton) {
    }
    
    @IBAction func btnBackClikced(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
