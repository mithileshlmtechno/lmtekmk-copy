//
//  ForgetPassWithLoginVC.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 05/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class ForgetPassWithLoginVC: BaseViewController {

    @IBOutlet weak var btnTopLanguage: UIButton!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    
    var strConfirmPassword: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        self.updateUI()
    }
    
    func updateUI() {
        btnTopLanguage.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        btnTopLanguage.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        btnTopLanguage.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)

        txtConfirmPassword.setLeftPaddingPoints(10)
        txtConfirmPassword.isSecureTextEntry = true
        txtConfirmPassword.attributedPlaceholder = NSAttributedString(string: "Confirm Password", attributes: [NSAttributedString.Key.foregroundColor: colorGray])

    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnForgetPasswordClicked(_ sender: UIButton) {
        var vcFound:Bool = false
        if let viewControllers = self.navigationController?.viewControllers {
              for vc in viewControllers {
                   if vc.isKind(of: ForgetPassUserPhoneVC.classForCoder()) {
                    vcFound = true
                    self.navigationController?.popToViewController(vc, animated: true)
                    break;
                   }
              }
        }
        if(vcFound == false) {
            let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgetPassUserPhoneVC") as! ForgetPassUserPhoneVC
            self.navigationController?.pushViewController(signUpVC, animated: true)
        }
    }
    
    @IBAction func btnLoginClicked(_ sender: UIButton) {
        if txtConfirmPassword.text?.isEmpty == true {
            ToastMessage.showToast(in: self, message: "Please Enter Confirm Password", bgColor: .white)
        } else {
            if strConfirmPassword == txtConfirmPassword.text!{
                self.loginScreen()
            } else {
                ToastMessage.showToast(in: self, message: "Don't match Confirm Password", bgColor: .white)
            }
        }
    }
    func loginScreen() {
        var vcFound:Bool = false
        if let viewControllers = self.navigationController?.viewControllers {
              for vc in viewControllers {
                   if vc.isKind(of: LoginUserVC.classForCoder()) {
                    vcFound = true
                    self.navigationController?.popToViewController(vc, animated: true)
                    break;
                   }
              }
        }
        if(vcFound == false) {
            let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginUserVC") as! LoginUserVC
            self.navigationController?.pushViewController(signUpVC, animated: true)
        }
    }
    @IBAction func loginWithAnotherAccountClicked(_ sender: UIButton) {
        
    }
}
