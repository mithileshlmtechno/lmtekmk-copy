//
//  ForgetPassUserPhoneVC.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 05/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class ForgetPassUserPhoneVC: UIViewController {

    @IBOutlet weak var lblLine: UILabel!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var btnPhoneCode: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        self.updateUI()
    }
    func screenDesigning() {
        
    }
    func updateUI() {
        txtUserName.setLeftPaddingPoints(10)
        txtPhone.setLeftPaddingPoints(10)
        txtPhone.isHidden = true
        btnPhoneCode.isHidden = true

        txtUserName.attributedPlaceholder = NSAttributedString(string: "User name", attributes: [NSAttributedString.Key.foregroundColor: colorGray])
        txtPhone.attributedPlaceholder = NSAttributedString(string: "Phone", attributes: [NSAttributedString.Key.foregroundColor: colorGray])
        
    }
    @IBAction func btnUserNameClicked(_ sender: UIButton) {
//        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations: {
//            self.lblLine.frame = CGRect(x: 15, y: 302.5, width: (screenWidth/2)-15, height: 2)
//            self.txtPhone.isHidden = true
//            self.txtUserName.isHidden = false
//        }, completion: nil)

    }
    @IBAction func btnPhoneNumberClicked(_ sender: UIButton) {
//        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations: {
//            self.lblLine.frame = CGRect(x: 15, y: 302.5, width: (screenWidth/2)-15, height: 2)
//            self.txtPhone.isHidden = false
//            self.txtUserName.isHidden = true
//        }, completion: nil)

    }
    
    @IBAction func btnBackToLogin(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNextClicked(_ sender: UIButton) {
        let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "AllOTPVC") as! AllOTPVC
        signUpVC.forgetPass = true
        self.navigationController?.pushViewController(signUpVC, animated: true)

    }
    
}
