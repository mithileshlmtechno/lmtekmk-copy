//
//  BaseViewController.swift
//  Monadie
//
//  Created by Dilip Technology on 26/02/18.
//  Copyright © 2018 Dilip Technology. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class BaseViewController: UIViewController {

    var loader:UIAlertController!

    var bannerHeight:CGFloat = 50
    
    var menuHeight:CGFloat = 50
    var fontDiff:CGFloat = -2.0
    
    let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var isFromViewDidLoad: Bool = true
    
    let strAppName:String = ""
    
    var keyboardHeight:CGFloat = 333.0

    let imgViewBG:UIImageView = UIImageView()

    override func viewDidLoad() {
        super.viewDidLoad()

        if UserDefaults.standard.bool(forKey: "LOG_IN") == true{
            
        }

        imgViewBG.frame = CGRect(x:0, y:0, width:screenWidth, height:screenHeight)
        imgViewBG.image = UIImage.init(named: "")
        self.view.addSubview(imgViewBG)

//        if(self.appDelegate.isiPad == true){
//            bannerHeight = 90
//        }
        self.view.backgroundColor = UIColor.clear
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
    }
    
    static func showAlert(viewCtrl: UIViewController, title: String, message: String, completion: (()->Swift.Void)?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            (_: UIAlertAction) -> Void in
            if completion != nil {
                completion!()
            }
        }
        ;        alertController.addAction(okAction)
        DispatchQueue.main.async {
            viewCtrl.present(alertController, animated: true, completion: nil)
        }
    }

    func addLoader()
    {
        if(loader == nil) {
            loader = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
            
            loader.view.tintColor = UIColor.black
            let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x:10, y:5, width:50, height:50)) as UIActivityIndicatorView
            loadingIndicator.hidesWhenStopped = true
            if #available(iOS 13.0, *) {
                loadingIndicator.style = UIActivityIndicatorView.Style.large
                loadingIndicator.color = .black
            } else {
                loadingIndicator.style = .gray
                loadingIndicator.color = .black
            }
            loadingIndicator.startAnimating();
            
            loader.view.addSubview(loadingIndicator)
            present(loader, animated: true, completion: nil)
        }
    }
    
    func addLoader(_ strMessage:String)
    {
        if(loader == nil)
        {
            loader = UIAlertController(title: nil, message: strMessage, preferredStyle: .alert)
            
            loader.view.tintColor = UIColor.black
            let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x:10, y:5, width:50, height:50)) as UIActivityIndicatorView
            loadingIndicator.hidesWhenStopped = true
            if #available(iOS 13.0, *) {
                loadingIndicator.style = UIActivityIndicatorView.Style.large
            } else {
                loadingIndicator.style = .gray
            }
            loadingIndicator.startAnimating();
            
            loader.view.addSubview(loadingIndicator)
            present(loader, animated: true, completion: nil)
        }
    }
    
    func removeLoader()
    {
        if(loader != nil) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                if(self.loader != nil) {
                    self.loader.dismiss(animated: true, completion: nil)
                    self.loader = nil
                }
            }
        }
    }


    func checkLowercase(_ text : String) -> Bool{
        let capitalLetterRegEx  = ".*[a-z]+.*"
        let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
        let capitalresult = texttest.evaluate(with: text)
        print("\(capitalresult)")
        return capitalresult
    }
    
    func checkUpercase(_ text : String) -> Bool{
        let capitalLetterRegEx  = ".*[A-Z]+.*"
        let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
        let capitalresult = texttest.evaluate(with: text)
        print("\(capitalresult)")
        return capitalresult
    }
    func checkNumber(_ text : String) -> Bool{
        let numberRegEx  = ".*[0-9]+.*"
        let texttest1 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let numberresult = texttest1.evaluate(with: text)
        print("\(numberresult)")
        
        return numberresult
    }
    func checkSpecialCharecter(_ text : String) -> Bool{
        let specialCharacterRegEx  = ".*[!&^%$#@()/]+.*"
        let texttest2 = NSPredicate(format:"SELF MATCHES %@", specialCharacterRegEx)
        let specialresult = texttest2.evaluate(with: text)
        print("\(specialresult)")
        return specialresult
    }

    // MARK: - Comomn Methods
    func showAlertMessage(_ strMessage: String) {
        let alert = UIAlertController(title: "", message: strMessage, preferredStyle: .alert)
        let yesButton = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            //Handle your yes please button action here
        })
        alert.addAction(yesButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertMessageDispatchQueue(_ strMessage: String) {
     
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
        let alert = UIAlertController(title: "", message: strMessage, preferredStyle: .alert)
        let yesButton = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            //Handle your yes please button action here
        })
        alert.addAction(yesButton)
        self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showUpgradeMessage(_ strMessage: String) {
        let alert = UIAlertController(title: "", message: "Please upgrade to premium", preferredStyle: .alert)
        let yesButton = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.gotoSubscribe()
        })
        alert.addAction(yesButton)
        let noButton = UIAlertAction(title: "Cancel", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            
//            if(strMessage == ""){
//                self.navigationController?.popToViewController(self.appDelegate.objHomeVC, animated: true)
//            }
        })
        alert.addAction(noButton)
        self.present(alert, animated: true, completion: nil)
    }

    func gotoSubscribe()
    {
//        let objSubscribe:SubscribeViewController = SubscribeViewController()
//        objSubscribe.view.backgroundColor = UIColor.init(white: 0.9, alpha: 1.0)
//        objSubscribe.view.frame = CGRect(x:0, y:72, width:screenWidth, height: screenHeight-72)
//        if(self.navigationController != nil) {
//            self.navigationController?.pushViewController(objSubscribe, animated: false)
//        }
//        else {
//            self.appDelegate.navController?.pushViewController(objSubscribe, animated: false)
//        }
    }

    func gotoBack(){
        self.navigationController?.popViewController(animated: true)
    }
    func showAlertMessage(_ strMessage: String, withTitle strTitle: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
        let alert = UIAlertController(title: strTitle, message: strMessage, preferredStyle: .alert)
        let yesButton = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            //Handle your yes please button action here
            self.gotoBack()
        })
        alert.addAction(yesButton)
        self.present(alert, animated: true, completion: nil)
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-  UITextField Methods
//    func animateTextField(_ textField:UITextField, up:Bool)
//    {
//        print("Animate ")
//        let movementDistance:CGFloat = -130
//        let movementDuration: Double = 0.3
//
//        var movement:CGFloat = 0
//        if up
//        {
//            movement = movementDistance
//        }
//        else
//        {
//            movement = -movementDistance
//        }
//        UIView.beginAnimations("animateTextField", context: nil)
//        UIView.setAnimationBeginsFromCurrentState(true)
//        UIView.setAnimationDuration(movementDuration)
//        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
//        //CGRectOffset(self.view.frame, 0, movement)
//        UIView.commitAnimations()
//    }

    func isValidEmail(_ testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    func phoneValidate(value: String) -> Bool {
            let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
            let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
            let result = phoneTest.evaluate(with: value)
            return result
        }

  func isValidPassword(password: String) -> Bool {
    if(password.count >= 6) {
        return true
    }
        let passwordRegEx = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()\\-_=+{}|?>.<,:;~`’]{8,}$"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        let result = passwordTest.evaluate(with: password)
        return result
    }

    @objc func keyboardWillShow(_ notification: Notification) {

        //let yOffset:CGFloat = self.view.frame.origin.y
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            self.keyboardHeight = keyboardRectangle.height
            print("keyboardHeight = ", self.keyboardHeight)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
