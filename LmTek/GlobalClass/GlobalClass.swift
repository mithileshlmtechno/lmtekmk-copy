//
//  GlobalClass.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 04/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import Foundation
import UIKit

var screenHeight: CGFloat = UIScreen.main.bounds.size.height
var screenWidth: CGFloat = UIScreen.main.bounds.size.width

var fontRegular: String = "proximanova-regular"
var fontBold: String = "proximanova-bold"
var fontsemoBold: String = "proximanova-semibold"

var fontSizeRegulor: UIFont = UIFont.init(name: fontRegular, size: 17)!
var fontSizeBoldTitle: UIFont = UIFont.init(name: fontBold, size: 20)!
var fontSizeSemiBoldTitle: UIFont = UIFont.init(name: fontsemoBold, size: 17)!
var fontSizeSemiBoldSmall: UIFont = UIFont.init(name: fontsemoBold, size: 14)!
var fontSizeSmall: UIFont = UIFont.init(name: fontRegular, size: 13)!

var colorGray: UIColor = UIColor.init(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
var colorYellow: UIColor = UIColor.init(red: 254/255.0, green: 250/255.0, blue: 55/255.0, alpha: 1)
var colorGrayBG: UIColor = UIColor.init(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 0.25)
var ColorFaceBook: UIColor = UIColor.init(red: 2/255.0, green: 150/255.0, blue: 248/255.0, alpha: 1)

var ColorTextFieldBG: UIColor = UIColor.init(red: 47/255.0, green: 47/255.0, blue: 47/255.0, alpha: 1)

