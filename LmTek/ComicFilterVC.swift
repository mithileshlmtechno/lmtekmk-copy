//
//  ComicFilterVC.swift
//  LmTek
//
//  Created by Sai Sankar on 06/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import AVFoundation

class ComicFilterVC: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {
    
    var captureSession = AVCaptureSession()
    var backCamera: AVCaptureDevice?
    var frontCamera: AVCaptureDevice?
    var currentCamera: AVCaptureDevice?
    
    var photoOutput: AVCapturePhotoOutput?
    var orientation: AVCaptureVideoOrientation = .portrait
    
    let videoOutput = AVCaptureVideoDataOutput()
    
    let context = CIContext()
    
    var effectsArray = ["CILineOverlay", "CIComicEffect", "CIGaussianBlur","CIFalseColor", "CIColorInvert", "CIColorMonochrome", ""] //["CIComicEffect", "CIBloom", "CICrystalize", "CIGloom", "CISpotColor"]
    var selectedEffect = 0
    
    var imagePicker = UIImagePickerController()
    
    @IBOutlet weak var filteredImage: UIImageView!
    @IBOutlet weak var view_video: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupDevice()
        setupInputOutput()
    }
    
    override func viewDidLayoutSubviews() {
        orientation = AVCaptureVideoOrientation(rawValue: UIApplication.shared.statusBarOrientation.rawValue)!
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) != .authorized
        {
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler:
            { (authorized) in
                DispatchQueue.main.async
                {
                    if authorized
                    {
                        self.setupInputOutput()
                    }
                }
            })
        }
    }
    
    @IBAction func changeCamera() {
        if self.currentCamera == self.backCamera {
            self.currentCamera = self.frontCamera
        }
        else {
            self.currentCamera = self.backCamera
        }
        if let input = self.captureSession.inputs.first, let output = self.captureSession.outputs.first {
            //DispatchQueue.main.async {
                self.captureSession.removeInput(input)
                self.captureSession.removeOutput(output)
                self.captureSession.stopRunning()
            //}
        }
        self.setupInputOutput()
    }
    
    @IBAction func chageEffect() {
        self.selectedEffect += 1
        if self.selectedEffect >= self.effectsArray.count {
            self.selectedEffect = 0
        }
        if let input = self.captureSession.inputs.first, let output = self.captureSession.outputs.first {
            //DispatchQueue.main.async {
                self.captureSession.stopRunning()
                self.captureSession.removeInput(input)
                self.captureSession.removeOutput(output)
            //}
        }
        self.setupInputOutput()
    }
    
    @IBAction func captureButtonClicked() {
    }
    
    func setupDevice() {
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
        let devices = deviceDiscoverySession.devices
        
        for device in devices {
            if device.position == AVCaptureDevice.Position.back {
                backCamera = device
            }
            else if device.position == AVCaptureDevice.Position.front {
                frontCamera = device
            }
        }
        
        currentCamera = backCamera
    }
    
    func setupInputOutput() {
        do {
            setupCorrectFramerate(currentCamera: currentCamera!)
            let captureDeviceInput = try AVCaptureDeviceInput(device: currentCamera!)
            captureSession.sessionPreset = AVCaptureSession.Preset.hd1280x720
            if captureSession.canAddInput(captureDeviceInput) {
                captureSession.addInput(captureDeviceInput)
            }
            
            videoOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "sample buffer delegate", attributes: []))
            if captureSession.canAddOutput(videoOutput) {
                captureSession.addOutput(videoOutput)
            }
            captureSession.startRunning()
        } catch {
            print(error)
        }
    }
    
    func setupCorrectFramerate(currentCamera: AVCaptureDevice) {
        for vFormat in currentCamera.formats {
            //see available types
            //print("\(vFormat) \n")
            
            var ranges = vFormat.videoSupportedFrameRateRanges as [AVFrameRateRange]
            let frameRates = ranges[0]
            
            do {
                //set to 240fps - available types are: 30, 60, 120 and 240 and custom
                // lower framerates cause major stuttering
                if frameRates.maxFrameRate == 240 {
                    try currentCamera.lockForConfiguration()
                    currentCamera.activeFormat = vFormat as AVCaptureDevice.Format
                    //for custom framerate set min max activeVideoFrameDuration to whatever you like, e.g. 1 and 180
                    currentCamera.activeVideoMinFrameDuration = frameRates.minFrameDuration
                    currentCamera.activeVideoMaxFrameDuration = frameRates.maxFrameDuration
                }
            }
            catch {
                print("Could not set active format")
                print(error)
            }
        }
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        autoreleasepool(invoking: { () -> Void in
            connection.videoOrientation = orientation
            let videoOutput = AVCaptureVideoDataOutput()
            videoOutput.setSampleBufferDelegate(self, queue: DispatchQueue.main)
            
            let comicEffect = CIFilter(name: self.effectsArray[self.selectedEffect]) //"CIComicEffect")
            
            self.currentSampleTime = CMSampleBufferGetOutputPresentationTimeStamp(sampleBuffer)
            
            if let effect = comicEffect, let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) {
                
                if self.isRecording {
                    if self.assetWriterPixelBufferInput?.assetWriterInput.isReadyForMoreMediaData == true {
                        var newPixelBuffer : CVPixelBuffer? = nil
                        
                        CVPixelBufferPoolCreatePixelBuffer(nil, (self.assetWriterPixelBufferInput?.pixelBufferPool)!, &newPixelBuffer)
                        
                        let success = self.assetWriterPixelBufferInput?.append(newPixelBuffer!, withPresentationTime: self.currentSampleTime!)
                        
                        if success == false {
                            print("Pixel Buffer failed")
                        }
                    }
                }
                
                let cameraImage = CIImage(cvImageBuffer: pixelBuffer)
                
                effect.setValue(cameraImage, forKey: kCIInputImageKey)
                
                if let outputImage = effect.outputImage, let cgImage = self.context.createCGImage(outputImage, from: cameraImage.extent) {
                    //let cgImage = self.context.createCGImage(comicEffect!.outputImage!, from: cameraImage.extent)!
                    
                    DispatchQueue.main.async {
                        let filteredImage = UIImage(cgImage: cgImage)
                        self.filteredImage.image = filteredImage
                    }
                }
            } else {
                if let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) {
                    
                    if self.isRecording {
                        if self.assetWriterPixelBufferInput?.assetWriterInput.isReadyForMoreMediaData == true {
                            var newPixelBuffer : CVPixelBuffer? = nil
                            
                            CVPixelBufferPoolCreatePixelBuffer(nil, (self.assetWriterPixelBufferInput?.pixelBufferPool)!, &newPixelBuffer)
                            
                            let success = self.assetWriterPixelBufferInput?.append(newPixelBuffer!, withPresentationTime: self.currentSampleTime!)
                            
                            if success == false {
                                print("Pixel Buffer failed")
                            }
                        }
                    }
                    
                    let ciImage = CIImage(cvImageBuffer: imageBuffer)
                    let context = CIContext()
                    guard let image = context.createCGImage(ciImage, from: CGRect(x: 0, y: 0, width: CVPixelBufferGetWidth(imageBuffer), height: CVPixelBufferGetHeight(imageBuffer))) else {
                        print("cgimage error")
                        return
                    }
                    DispatchQueue.main.async {
                        self.filteredImage.image = UIImage(cgImage: image)
                    }
                }
                else {
                    print("pixel buffer else")
                }
            }
        })
        
        
    }
    
    var outputFileLocation: URL?
    var videoWriter: AVAssetWriter!
    var assetWriterPixelBufferInput: AVAssetWriterInputPixelBufferAdaptor?
    var videoWriterInput: AVAssetWriterInput!
    var audioWriterInput: AVAssetWriterInput!
    var currentSampleTime : CMTime?
    var isRecording = Bool()
    
    var sessionAtSourceTime: Timer?
    var path: URL?
    
    func setUpWriter() {

        do {
            outputFileLocation = videoFileLocation()
            videoWriter = try AVAssetWriter(outputURL: outputFileLocation!, fileType: AVFileType.mov)

            // add video input
            let outputSettings = [
            AVVideoCodecKey : AVVideoCodecType.h264,
            AVVideoWidthKey : 720,
            AVVideoHeightKey : 1280,
            AVVideoCompressionPropertiesKey : [
                AVVideoAverageBitRateKey : 2300000,
                ],
                ] as [String : Any]
            videoWriterInput = AVAssetWriterInput(mediaType: AVMediaType.video, outputSettings: outputSettings)

            assetWriterPixelBufferInput = AVAssetWriterInputPixelBufferAdaptor(assetWriterInput: videoWriterInput, sourcePixelBufferAttributes: outputSettings)
            videoWriterInput.expectsMediaDataInRealTime = true

            if videoWriter.canAdd(videoWriterInput) {
                videoWriter.add(videoWriterInput)
                print("video input added")
            } else {
                print("no input added")
            }

            // add audio input
            audioWriterInput = AVAssetWriterInput(mediaType: AVMediaType.audio, outputSettings: nil)

            audioWriterInput.expectsMediaDataInRealTime = true

            if videoWriter.canAdd(audioWriterInput!) {
                videoWriter.add(audioWriterInput!)
                print("audio input added")
            }


            videoWriter.startWriting()
            videoWriter.startSession(atSourceTime: self.currentSampleTime!)
        } catch let error {
            debugPrint(error.localizedDescription)
        }


    }

    func canWrite() -> Bool {
        return isRecording && videoWriter != nil && videoWriter?.status == .writing
    }


     //video file location method
    func videoFileLocation() -> URL {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        let videoOutputUrl = URL(fileURLWithPath: documentsPath.appendingPathComponent("videoFile")).appendingPathExtension("mov")
        do {
        if FileManager.default.fileExists(atPath: videoOutputUrl.path) {
            try FileManager.default.removeItem(at: videoOutputUrl)
            print("file removed")
        }
        } catch {
            print(error)
        }

        return videoOutputUrl
    }

    // MARK: AVCaptureVideoDataOutputSampleBufferDelegate
    /*func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {

        let writable = canWrite()

        if writable,
            sessionAtSourceTime == nil {
            // start writing
            sessionAtSourceTime = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
            videoWriter.startSession(atSourceTime: sessionAtSourceTime!)
            //print("Writing")
        }

        if output == videoDataOutput {
            connection.videoOrientation = .portrait

            if connection.isVideoMirroringSupported {
                connection.isVideoMirrored = true
            }
        }

        if writable,
            output == videoDataOutput,
            (videoWriterInput.isReadyForMoreMediaData) {
            // write video buffer
            videoWriterInput.append(sampleBuffer)
            //print("video buffering")
        } else if writable,
            output == audioDataOutput,
            (audioWriterInput.isReadyForMoreMediaData) {
            // write audio buffer
            audioWriterInput?.append(sampleBuffer)
            //print("audio buffering")
        }

    }*/

    // MARK: Start recording
   @IBAction func start() {
        guard !isRecording else { return }
        isRecording = true
        sessionAtSourceTime = nil
        setUpWriter()
        print(isRecording)
        print(videoWriter)
      path = videoWriter.outputURL
        if videoWriter.status == .writing {
            print("status writing")
        } else if videoWriter.status == .failed {
            print("status failed")
        } else if videoWriter.status == .cancelled {
            print("status cancelled")
        } else if videoWriter.status == .unknown {
            print("status unknown")
        } else {
            print("status completed")
        }

    }

    // MARK: Stop recording
    @IBAction func stop() {
        guard isRecording else { return }
        isRecording = false
        videoWriterInput.markAsFinished()
        print("marked as finished")
        videoWriter.finishWriting { [weak self] in
            if let file = self?.path {
                /*let player = AVPlayer(url: file)
                let playerLayer = AVPlayerLayer(player: player)
                playerLayer.frame = (self?.view_video.bounds)!
                self?.view_video.isHidden = false
                self?.view_video.layer.addSublayer(playerLayer)
                player.play()*/
                let videoPlayerVC = self?.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                videoPlayerVC.videoURL = file
                DispatchQueue.main.async {
                    self?.present(videoPlayerVC, animated: true, completion: nil)
                }
            }
            self?.sessionAtSourceTime = nil
        }
        //print("finished writing \(self.outputFileLocation)")
        captureSession.stopRunning()
        //performSegue(withIdentifier: "videoPreview", sender: nil)
    }
    
    @IBAction func selectVideoClicked() {
        self.imagePicker.sourceType = .photoLibrary
        self.imagePicker.delegate = self
        self.imagePicker.mediaTypes = ["public.movie"]
        
        self.present(self.imagePicker, animated: true, completion: nil)
    }
}

extension ComicFilterVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print("didFinishPickingMediaWithInfo:",info)
        let url = info[UIImagePickerController.InfoKey.mediaURL] as? URL
        if let file = url {
            let player = AVPlayer(url: file)
            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.frame = self.view_video.bounds
            self.view_video.isHidden = false
            self.view_video.layer.addSublayer(playerLayer)
            let interval = CMTime(seconds: 10, preferredTimescale: 1)
            let times = [interval.value as NSValue]
            player.addBoundaryTimeObserver(forTimes: times, queue: nil) {
                player.setRate(0.5, time: CMTime(seconds: 10, preferredTimescale: 1), atHostTime: CMTime(seconds: 10, preferredTimescale: 1))
            }
            player.play()
            
            /*let videoAsset : AVURLAsset = AVURLAsset(url: file)
            
            let mixComposition = AVMutableComposition(url: file)
            
            let compositionVideoTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
                        
            do {
                let videoInsertResult = try compositionVideoTrack?.insertTimeRange(CMTimeRange(start: .zero, duration: videoAsset.duration), of: videoAsset.tracks(withMediaType: .video)[0], at: .zero)
                
                let videoScalarFactor = 2.0
                let videoDuration = videoAsset.duration
                compositionVideoTrack?.scaleTimeRange(CMTimeRange(start: .zero, duration: videoDuration), toDuration: CMTime(value: videoDuration.value * Int64(videoScalarFactor), timescale: videoDuration.timescale))
                
                let assetExport = AVAssetExportSession.init(asset: mixComposition, presetName: AVAssetExportPresetLowQuality)
                
                if let video_url = assetExport?.outputURL {
                    let player = AVPlayer(url: file)
                    let playerLayer = AVPlayerLayer(player: player)
                    playerLayer.frame = self.view_video.bounds
                    self.view_video.isHidden = false
                    self.view_video.layer.addSublayer(playerLayer)
                    player.play()
                }
            }
            catch {
                print("error:",error.localizedDescription)
            }*/
        }
        picker.dismiss(animated: true, completion: nil)
    }
}
