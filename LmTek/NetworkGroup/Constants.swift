//
//  Constants.swift
//  LmTek
//
//  Created by PTBLR-1128 on 24/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import Foundation
import UIKit

//internal let banubaClientToken = "sTciF/2Pzw0+kWKPw0xZ4DRKfEC8pkAjGbiOTzWJ7kaJrkbM4EYxciaCArKSGeZysOANaATaeZ0ARCv5QgO+FfIirL2XX6rUAd0pb52GjzwekfE1OmayjDjfxkON3+/3VBazdM8d9Icc2WHOXWZIDA7ggkwHT/5DvMn0ysSOsUgjNS05UXnoABrJKAaFRONxWPkudqRggICCL8nj3WbBMVYzFhimRKjgRUjK02qLvEDrFb9jzp3vPo4Q+cEDnmDpz2s+6KsMzf6fjOB248jZtLd38xZGj0QDqcTD7YBFlBAGYW7w7uMxUONq6ju69KgoFRfYeqL8/HkV1eaQY1rHIuF/kZTXw1FEfnNWq37z2V9CMRRQ8n+zm3pCnqM2BVApG8Epiin5xS7VnOHUP0EzF4sYzYOXO/6EJjdKiZ9sRY92zHFw/Tuj7pThTWZDNu64TTfOo/pjFdDnMZY2gHNhk5Xxp1Rwx8OwxEgqO/0ujhDlt21hRHXRfD8/h7F96hNBOBYJWddT70W2E+zo2irKKLXvfh5sQhAWYN1gHDKwDFLsTIaFhAnvsDHBz1H4+ldyD65+t3GGtEeVS9zhKy5P0UtVIt59RN6peSZMTOJuy4jvXntyspRwSeRS/WDvYE4dPzLM8KARR3qVw7Bcwns2/4WNhJh612tWa60HyKxOrMY8OnwgaUsmXhBIfEv1JkXZQeLkNur8ovZACrgyP+tqObxxMLSKEN0UlyM2Lr52JFMG/t7qU5OTKztekG1qwV1lTK5qUZ8tuUf8eSiL08mDbAE2j9pPoAeNpYSSNfZYlK9Pz4+EnjeWg9qUDyWeickAHsW9BuUrc5WT"

let EffectsStoryboard = UIStoryboard(name: "Effects", bundle: nil)

let MainStoryboard = UIStoryboard(name: "Main", bundle: nil)

class Constants {
    static var previousTab = 0
    
    //MARK:- URL'S...
    static let BASE_URL = "http://13.234.81.24/rest/"
    //"http://13.234.81.24/rest"//"http://23.20.5.8/lmtek/rest"
    
    //MARK:- Onboard:
    static let GET_COUNTRY_CODES = "general/countryCodeList"
    static let LOGIN = "auth/login"
    static let VERIFY_OTP = "auth/verify_otp"
    static let GET_OTP = "auth/get_otp"
    static let CHECK_EMAIL_PHONE  = "auth/checkEmailPhone"
    static let REGISTER_USER = "auth/registerUser"
    static let ADD_DOB = "auth/addDob"
    static let SEARCH_USERNAME = "auth/searchUsername"
    static let UPDATE_USERNAME = "auth/updateUsername"
    static let GET_USER_DETAILS = "auth/getUser"
    static let LOGIN_FACEBOOK = "auth/facebook"
    static let LOGIN_APPLE = "auth/apple"
    static let LOGIN_GOOGLE = "auth/google"
    static let GET_LANGUAGES = "auth/getLanguages"
    static let GET_SPLASHSCREEN = "general/splashScreen"
    static let GET_CATEGORIES = "auth/getCategories"
    static let UPDATE_LANGUAGE = "auth/updateLanguage"
    static let UPDATE_CATEGORY = "auth/updateCategory"
    
    //MARK: Forget Password
    static let FORGET_PASSWORD  = "auth/forgetPassword"
    static let SET_PASSWORD  = "auth/setPassword"
    static let FIND_USER = "auth/findUser"
    
    //MARK: HOME:
    static let GET_GENERAL_VIDEOS = "general/videos"
    static let GET_GENERAL_VIDEOS_PAGINATION = "general/videos2?limit=20&skip="
    static let GET_COMMETNS = "auth/comments/"
    static let ADD_COMMENT = "auth/doComment/"
    static let LIKE_VIDEO = "auth/doLike/"
    static let GET_VIDEOS_LIST = "video/videos/"
    static let GET_VIDEOS_LIST_PAGINATION = "auth/videos" //"video/videos2/?limit=20&skip="
    static let GET_REPORT_REASONS = "general/getReportReasons"
    static let REPORT_VIDEO = "video/reportVideo"
    static let DO_SHARE = "auth/doShare/"
    static let DO_DOWNLOAD = "auth/doDownload/"
    static let FOLLOWING_VIDEOS = "auth/followingUserVideos/"
    static let FOLLOW_USER = "auth/doFollow"
    static let MARK_VIDEO_AS_WATCHED = "auth/markVideoAsWatched"
    
    //MARK: Camera:
    static let GET_AUDIO_LIST = "auth/sounds"
    static let UPLOAD_VIDEO = "auth/uploadVideo/"
    static let GET_FAVOURITE_AUDIO_LIST = "auth/favouriteSoundsList/"
    static let MAKE_AUDIO_FAVOURITE = "auth/markFavouriteSound/"
    static let SEARCH_AUDIO = "auth/searchSound/"
    static let VIDEO_HASHTAGS = "auth/hashtags/"
    
    //MARK: Profile
    static let GET_OTHER_PROFILE = "auth/othersProfile"
    static let GET_USER_PROFILE = "user/profile"
    static let UPDATE_USER_PROFILE = "auth/updateProfile"
    static let UPDATE_PROFILE_PICTURE = "auth/uploadProfilePicture"
    static let UPDATE_COVER_PICTURE = "auth/uploadCoverPhoto"
    static let GET_PRIVACY_SETTINGS = "auth/privacy_setting"
    static let UPDATE_PRIVACY_SETTINGS = "auth/update_privacy"
    static let GET_DUET_VIDEOS = "auth/duetVideos"
    
    //MARK: Local
    static let GET_FOLLOWING_LIST = "auth/getFollowingList"
    static let GET_FOLLOWERS_LIST = "auth/getFollowersList"
    static let GET_NOTIFICATIONS_LIST = "auth/notificationList"
    
    //MARK:- Discover
    static let GET_TOP_DISCOVERY_DATA = "auth/topDiscoveryData"
    static let SEARCH_DISCOVERY = "auth/discovery"
    static let GET_ALL_HASHTAGS_VIDEOS = "auth/viewAllHashtagVideos"
    
    //MARK:- Colors...
    static let YELLOW_COLOR = "#FEFA37"
    static let ORANGE_COLOR = "#EA9043"
    static let RED_COLOR = "#DA3F52"
    static let DARK_BLUE_COLOR = "#415478"
    static let LIGHT_BLUE_COLOR = "#768DAB"
    static let CHOOSE_LANGUAGE_BLUE_COLOR = "#2000C2"
    
    static let NOTIFICATIONS_BLACK_COLOR = "#312F2F"
    static let OFFICIAL_ACCOUNT_BLUE_COLOR = "#4E5BF4"
    static let ACCOUNT_BLACK_COLOR = "#29292C"
    static let FOLLOW_BLUE_COLOR = "#1D0DCE"
    static let FOLLOWING_RED_COLOR = "#DD514E"
    
    static let PRIVACY_SETTING_SWITCH_BORDER_ORANGE_COLOR = "#E05E4B"
    static let PRIVACY_SETTING_SWITCH_THUMB_ORANGE_COLOR = "#DB4450"
    static let PRIVACY_SETTING_SWITCH_BORDER_BLACK_COLOR = "#E0E0E0"
    static let PRIVACY_SETTING_SWITCH_THUMB_BLACK_COLOR = "#332B2B"
    
    //MARK: NotificationNames...
    static let PROFILE_UPDATE_NOTIFICATION = "PROFILE_UPDATE_NOTIFICATION"
    static let VIDEO_ADDED_NOTIFICATION = "VIDEO_ADDED_NOTIFICATION"
    
    //MARK:- Profile...
    static var profileModel : ProfileItem?
    
    //MARK:- CountryCodes...
    static var COUNTRY_CODES = [CountryCodeItem]()
    
    //MARK:- Keys...
    struct KEYS {
        static let isLoggedIn = "isLoggedIn"
        static let LoginResponse = "LoginResponse"
        static let TOKEN = "Token"
        static let FCMID = "FCMID"
        static let IS_CLOSE_CASE_DO_NOT_SHOW = "IS_CLOSE_CASE_DO_NOT_SHOW"
    }
    
    //MARK:- Queue Labels...
    struct QueueLabels {
        static let VideoSessionQueue = "video_capture_session_queue"
        static let AudioSessionQueue = "audio_capture_session_queue"
    }
    
    struct CIEffectKeys {
        static let InputAmountKey = "inputAmount"
        static let InputHighlightAmount = "inputHighlightAmount"
        static let InputShadowAmount = "inputShadowAmount"
    }
}
