//
//  TransportManager.swift
//  DocKare Doctor
//
//  Created by PTBLR-1128 on 20/11/19.
//  Copyright © 2019 Provab Technosoft Pvt Ltd. All rights reserved.
//

import Foundation
import Alamofire
//import ObjectMapper
//import AlamofireObjectMapper
import SwiftyJSON

typealias CallBackHandler = (_ data: Any?, _ err: Error?)-> ()


class TransportManager {
    
    static var sharedInstance = TransportManager()
    let user_Defauls:UserDefaults = UserDefaults.standard
    
    fileprivate let reachabilityManger:NetworkReachabilityManager! = NetworkReachabilityManager()
    
    var isReachable: Bool!  {
        get {
            return reachabilityManger.isReachable
        }
    }
    
    func sendRequest(parameters:[String : String], url: String,authkey: String, method: VKMethodType, isActivity: Bool = true, callback: @escaping CallBackHandler){
        print(url)
        let window = UIApplication.shared.keyWindow!
        if isActivity {
            //DialougeUtils.addActivityView(view:window)
        }
        APIManager.shared.getRequestRaw(params: parameters, file: url, httpMethod: method, authkey: authkey) { (response, success, erro) in
            if isActivity {
                //DialougeUtils.removeActivityView(view: window)
            }
            if success == true {
                let json = JSON(response as Any)
                guard let message = json["message"].rawString() else {return}
                guard let err = json["error"].int else {return}
                if let status = json["status"].bool {
                    if status == true {
                        callback(json, nil)
                    }else{
                        /* let error = NSError(domain: "\(err)",
                         code: err,
                         userInfo: [NSLocalizedDescriptionKey: message])
                         callback(message,error)*/
                        if err >= 4100 {
                            /*if let app = UIApplication.shared.delegate as? AppDelegate {
                                
                                UserDefaults.standard.set(nil, forKey: Constants.KEYS.LoginResponse)
                                UserDefaults.standard.synchronize()
                                app.showLoginScreen()
                                
                            }*/
                            self.sessionExpired()
                            window.rootViewController?.view.makeToast(message: "Session Expired! Please login again to continue.")
                        }
                        else if err == 0 {
                            callback(json, nil)
                        }
                        else {
                            let error = NSError(domain: "\(err)",
                                code: err,
                                userInfo: [NSLocalizedDescriptionKey: message])
                            callback(message,error)
                        }
                    }
                }
            }else {
                callback(nil,erro)
            }
        }
        /*APIManager.shared.getRequestFormdata(params: parameters, file: url, httpMethod: method){ (response, success, erro) in
            if isActivity {
                //DialougeUtils.removeActivityView(view: window)
            }
            if success == true {
                let json = JSON(response as Any)
                guard let message = json["message"].rawString() else {return}
                guard let err = json["error"].int else {return}
                if let status = json["status"].bool {
                    if status == true {
                        callback(json, nil)
                    }else{
                        /* let error = NSError(domain: "\(err)",
                         code: err,
                         userInfo: [NSLocalizedDescriptionKey: message])
                         callback(message,error)*/
                        if err >= 4100 {
                            if let app = UIApplication.shared.delegate as? AppDelegate {
                                
                                UserDefaults.standard.set(nil, forKey: Constants.KEYS.LoginResponse)
                                UserDefaults.standard.synchronize()
                                //app.showLoginScreen()
                            }
                            
                            window.rootViewController?.view.makeToast(message: "Session Expired! Please login again to continue.")
                            
                        }
                        else if err == 0 {
                            callback(json, nil)
                        }
                        else {
                            let error = NSError(domain: "\(err)",
                                code: err,
                                userInfo: [NSLocalizedDescriptionKey: message])
                            callback(message,error)
                        }
                    }
                }
            }else {
                callback(nil,erro)
            }
        }*/
    }
    
    /*func sendGetRequest(url: String, callback: @escaping CallBackHandler){
        let window = UIApplication.shared.keyWindow!
        DialougeUtils.addActivityView(view:window)
        print("api \(url)")
        let dict = UserDefaults.standard.value(forKey: Constants.KEYS.LoginResponse) as! [String : AnyObject]
        APIManager.shared.headers["Authentication"] = (dict["auth_key"] as! String)
        APIManager.shared.getRequest(file: url, httpMethod: .GET) { (response, success, err) in
            DialougeUtils.removeActivityView(view: window)
            if success == true {
                let json = JSON(response as Any)
                print("json\(json)")
                guard let message = json["message"].rawString() else {return}
                guard let err = json["error"].int else {return}
                if let status = json["status"].bool {
                    if status == true {
                        callback(json, nil)
                    }else{
                        if err >= 4100 {
                            if let app = UIApplication.shared.delegate as? AppDelegate {
                                
                                UserDefaults.standard.set(nil, forKey: Constants.KEYS.LoginResponse)
                                UserDefaults.standard.synchronize()
                                app.showLoginScreen()
                            }
                            
                            window.rootViewController?.view.makeToast(message: "Session Expired! Please login again to continue.")
                            
                        }
                        else if err == 0 {
                            callback(json, nil)
                        }
                        else {
                            let error = NSError(domain: "\(err)",
                                code: err,
                                userInfo: [NSLocalizedDescriptionKey: message])
                            callback(message,error)
                        }
                    }
                }
            }else
            {
                callback(nil,err)
            }
        }
    }*/
    
    func sendRequestWithoutAuth(parameters:[String : Any], url: String,authkey: String, method: VKMethodType, isActivity: Bool = true, callback: @escaping (_ data: Any?, _ err: Error?)-> ()){
        
        let window = UIApplication.shared.keyWindow!
        /*if isActivity {
            DialougeUtils.addActivityView(view:window)
        }*/
        APIManager.shared.getRequestRaw(params: parameters, file: url, httpMethod: method, authkey: authkey) { (response, success, erro) in
            /*if isActivity {
                DialougeUtils.removeActivityView(view: window)
            }*/
            if success == true {
                let json = JSON(response as Any)
                guard let message = json["message"].rawString() else {return}
                guard let err = json["error"].int else {return}
                if let status = json["status"].bool {
                    if status == true {
                        callback(json, nil)
                    }else{
                       /* let error = NSError(domain: "\(err)",
                                            code: err,
                                            userInfo: [NSLocalizedDescriptionKey: message])
                        callback(message,error)*/
                        if err >= 4100 {
                            self.sessionExpired()
                            window.rootViewController?.view.makeToast(message: "Session Expired! Please login again to continue.")
                            
                        }
                        else if err == 0 {
                            callback(json, nil)
                        }
                        else {
                            let error = NSError(domain: "\(err)",
                                code: err,
                                userInfo: [NSLocalizedDescriptionKey: message])
                            callback(message,error)
                        }
                    }
                }
            }else {
                callback(nil,erro)
            }
        }
        /*APIManager.shared.getRequestFormdataWithoutAuth(params: parameters, file: url, httpMethod: method){ (response, success, erro) in
            /*if isActivity {
                DialougeUtils.removeActivityView(view: window)
            }*/
            if success == true {
                let json = JSON(response as Any)
                guard let message = json["message"].rawString() else {return}
                guard let err = json["error"].int else {return}
                if let status = json["status"].bool {
                    if status == true {
                        callback(json, nil)
                    }else{
                       /* let error = NSError(domain: "\(err)",
                                            code: err,
                                            userInfo: [NSLocalizedDescriptionKey: message])
                        callback(message,error)*/
                        if err >= 4100 {
                            if let app = UIApplication.shared.delegate as? AppDelegate {
                                
                                /*UserDefaults.standard.set(nil, forKey: Constants.KEYS.LoginResponse)
                                UserDefaults.standard.synchronize()
                                app.showLoginScreen()*/
                            }
                            
                            //window.rootViewController?.view.makeToast(message: "Session Expired! Please login again to continue.")
                            
                        }
                        else if err == 0 {
                            callback(json, nil)
                        }
                        else {
                            let error = NSError(domain: "\(err)",
                                code: err,
                                userInfo: [NSLocalizedDescriptionKey: message])
                            callback(message,error)
                        }
                    }
                }
            }else {
                callback(nil,erro)
            }
        }*/
    }
    
    func sendGetRequestWithoutAuth(url: String, callback: @escaping CallBackHandler){
        let window = UIApplication.shared.keyWindow!
        //DialougeUtils.addActivityView(view:window)
        print("api \(url)")
        APIManager.shared.getRequest(file: url, httpMethod: .GET) { (response, success, err) in
            //DialougeUtils.removeActivityView(view: window)
            if success == true {
                let json = JSON(response as Any)
                print("json\(json)")
                guard let message = json["message"].rawString() else {return}
                guard let err = json["error"].int else {return}
                if let status = json["status"].bool {
                    if status == true {
                        callback(json, nil)
                    }else{
                        if err >= 4100 {
                            /*if let app = UIApplication.shared.delegate as? AppDelegate {
                                
                                UserDefaults.standard.set(nil, forKey: Constants.KEYS.LoginResponse)
                                UserDefaults.standard.synchronize()
                                app.showLoginScreen()
                            }*/
                            self.sessionExpired()
                            window.rootViewController?.view.makeToast(message: "Session Expired! Please login again to continue.")
                            
                        }
                        else if err == 0 {
                            callback(json, nil)
                        }
                        else {
                            let error = NSError(domain: "\(err)",
                                code: err,
                                userInfo: [NSLocalizedDescriptionKey: message])
                            callback(message,error)
                        }
                    }
                }
            }else
            {
                callback(nil,err)
            }
        }
    }
    
    /*func sendImageRequest(parameters:[String : String], images: [String : UIImage]?, url: String, method: VKMethodType, callback: @escaping CallBackHandler){
        
        let window = UIApplication.shared.keyWindow!
        DialougeUtils.addActivityView(view:window)
        APIManager.shared.getRequestFormdata(params: parameters, images: images, file: url, httpMethod: method) { (response, success, erro) in
            DialougeUtils.removeActivityView(view: window)
            if success == true {
                let json = JSON(response as Any)
                guard let message = json["message"].rawString() else {return}
                guard let err = json["error"].int else {return}
                if let status = json["status"].bool {
                    if status == true {
                        callback(json, nil)
                    }else{
                        /* let error = NSError(domain: "\(err)",
                         code: err,
                         userInfo: [NSLocalizedDescriptionKey: message])
                         callback(message,error)*/
                        if err >= 4100 {
                            if let app = UIApplication.shared.delegate as? AppDelegate {
                                
                                UserDefaults.standard.set(nil, forKey: Constants.KEYS.LoginResponse)
                                UserDefaults.standard.synchronize()
                                app.showLoginScreen()
                            }
                            
                            window.rootViewController?.view.makeToast(message: "Session Expired! Please login again to continue.")
                            
                        }else {
                            let error = NSError(domain: "\(err)",
                                code: err,
                                userInfo: [NSLocalizedDescriptionKey: message])
                            callback(message,error)
                        }
                    }
                }
            }else {
                callback(nil,erro)
            }
        }
    }
    
    func sendPDFRequest(pdfFileParams:[String : URL], url: String, method: VKMethodType, callback: @escaping CallBackHandler){
        
        let window = UIApplication.shared.keyWindow!
        DialougeUtils.addActivityView(view:window)
        APIManager.shared.getRequestFormdata(pdfFileParams: pdfFileParams, file: url, httpMethod: method) { (response, success, erro) in
            DialougeUtils.removeActivityView(view: window)
            if success == true {
                let json = JSON(response as Any)
                guard let message = json["message"].rawString() else {return}
                guard let err = json["error"].int else {return}
                if let status = json["status"].bool {
                    if status == true {
                        callback(json, nil)
                    }else{
                        /* let error = NSError(domain: "\(err)",
                         code: err,
                         userInfo: [NSLocalizedDescriptionKey: message])
                         callback(message,error)*/
                        if err >= 4100 {
                            if let app = UIApplication.shared.delegate as? AppDelegate {
                                
                                UserDefaults.standard.set(nil, forKey: Constants.KEYS.LoginResponse)
                                UserDefaults.standard.synchronize()
                                app.showLoginScreen()
                            }
                            
                            window.rootViewController?.view.makeToast(message: "Session Expired! Please login again to continue.")
                            
                        }else {
                            let error = NSError(domain: "\(err)",
                                code: err,
                                userInfo: [NSLocalizedDescriptionKey: message])
                            callback(message,error)
                        }
                    }
                }
            }else {
                callback(nil,erro)
            }
        }

    }
    
    func sendVideoCallRequest(parameters:[String : String], url: String, method: VKMethodType, callback: @escaping CallBackHandler) {
        
        let window = UIApplication.shared.keyWindow!
        DialougeUtils.addActivityView(view:window)
        APIManager.shared.getRequestVideoCallFormData(params: parameters, file: url, httpMethod: method) { (response, success, erro) in
            DialougeUtils.removeActivityView(view: window)
            if success == true {
                let json = JSON(response as Any)
                guard let message = json["message"].rawString() else {return}
                /*guard*/ let err = json["error"].int ?? 0//else {return}
                if let status = json["status"].bool {
                    if status == true {
                        callback(json, nil)
                    }else{
                        /* let error = NSError(domain: "\(err)",
                         code: err,
                         userInfo: [NSLocalizedDescriptionKey: message])
                         callback(message,error)*/
                        if err >= 4100 {
                            if let app = UIApplication.shared.delegate as? AppDelegate {
                                
                                UserDefaults.standard.set(nil, forKey: Constants.KEYS.LoginResponse)
                                UserDefaults.standard.synchronize()
                                app.showLoginScreen()
                            }
                            
                            window.rootViewController?.view.makeToast(message: "Session Expired! Please login again to continue.")
                            
                        }else {
                            let error = NSError(domain: "\(err)",
                                code: err,
                                userInfo: [NSLocalizedDescriptionKey: message])
                            callback(message,error)
                        }
                    }
                }
            }else {
                callback(nil,erro)
            }
        }
    }*/
    
    func sendAttachmentRequest(params: [String : String], videoParams: [String : URL]?, imageParams: [String : UIImage]?, url: String, method: VKMethodType, callback: @escaping CallBackHandler) {
        
        let window = UIApplication.shared.keyWindow!
        DialougeUtils.addActivityView(view:window)
        APIManager.shared.getRequestFormDataAttachments(params: params, videoParams: videoParams, imageParams: imageParams, file: url, httpMethod: method) { (response, success, erro) in
            DialougeUtils.removeActivityView(view: window)
            if success == true {
                let json = JSON(response as Any)
                guard let message = json["message"].rawString() else {return}
                guard let err = json["error"].int else {return}
                if let status = json["status"].bool {
                    if status == true {
                        callback(json, nil)
                    }else{
                        /* let error = NSError(domain: "\(err)",
                         code: err,
                         userInfo: [NSLocalizedDescriptionKey: message])
                         callback(message,error)*/
                        if err >= 4100 {
                            /*if let app = UIApplication.shared.delegate as? AppDelegate {
                                
                                UserDefaults.standard.set(nil, forKey: Constants.KEYS.LoginResponse)
                                UserDefaults.standard.synchronize()
                                app.showLoginScreen()
                            }*/
                            self.sessionExpired()
                            window.rootViewController?.view.makeToast(message: "Session Expired! Please login again to continue.")
                            
                        }else {
                            let error = NSError(domain: "\(err)",
                                code: err,
                                userInfo: [NSLocalizedDescriptionKey: message])
                            callback(message,error)
                        }
                    }
                }
            }else {
                callback(nil,erro)
            }
        }

    }
    
    //MARK:- session expired
    func sessionExpired() {
        if #available(iOS 13.0, *) {
            if let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate {
                sceneDelegate.showLoginScreen()
            }
        } else {
            if let appdelegate =  UIApplication.shared.delegate as? AppDelegate {
                appdelegate.showLoginScreen()
            }
        }
    }
    
    //MARK:- api calls...
    
    //MARK:- Home
    func getGeneralVideos(callback: @escaping CallBackHandler) {
        self.sendGetRequestWithoutAuth(url: Constants.GET_GENERAL_VIDEOS) { (data, error) in
            callback(data, error)
        }
    }
    
    func getComments(video_id : String, callback: @escaping CallBackHandler) {
        self.sendRequest(parameters: ["video_id" : video_id], url: Constants.GET_COMMETNS, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func addComment(video_id: String, comment: String, callback: @escaping CallBackHandler) {
        let params = ["video_id" : video_id, "comment" : comment]
        self.sendRequest(parameters: params, url: Constants.ADD_COMMENT, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func likeVideo(video_id: String, user_id: String, callback: @escaping CallBackHandler) {
        let params = ["video_id" : video_id, "user_id": user_id]
        self.sendRequest(parameters: params, url: Constants.LIKE_VIDEO, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func getHomeVideos(callback: @escaping CallBackHandler) {
        self.sendRequest(parameters: [:], url: Constants.GET_VIDEOS_LIST, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func getVideo(video_id: String, callback: @escaping CallBackHandler) {
        self.sendRequest(parameters: ["video_id" : video_id], url: Constants.GET_VIDEOS_LIST, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func getReportReasons(callback: @escaping CallBackHandler) {
        self.sendGetRequestWithoutAuth(url: Constants.GET_REPORT_REASONS) { (data, error) in
            callback(data, error)
        }
    }
    
    func reportVideo(video_id: String, reason_id: String, callback: @escaping CallBackHandler) {
        let params = ["video_id" : video_id, "reason_id" : reason_id]
        self.sendRequest(parameters: params, url: Constants.REPORT_VIDEO, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func doDownload(video_id: String, callback: @escaping CallBackHandler) {
        self.sendRequest(parameters: ["video_id" : video_id], url: Constants.DO_DOWNLOAD, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func doShare(video_id: String, callback: @escaping CallBackHandler) {
        self.sendRequest(parameters: ["video_id" : video_id], url: Constants.DO_SHARE, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func getFollowingVideos(callback: @escaping CallBackHandler) {
        self.sendRequest(parameters: [ : ], url: Constants.FOLLOWING_VIDEOS, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func followUser(follower_user_id: String, callback: @escaping CallBackHandler) {
        self.sendRequest(parameters: ["follower_user_id" : follower_user_id], url: Constants.FOLLOW_USER, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func markVideoAsWatched(video_id: String, callback: @escaping CallBackHandler) {
        self.sendRequest(parameters: ["video_id" : video_id], url: Constants.MARK_VIDEO_AS_WATCHED, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func getGeneralVideosPagination(skip: String, callback: @escaping CallBackHandler) {
        self.sendGetRequestWithoutAuth(url: Constants.GET_GENERAL_VIDEOS_PAGINATION.appending(skip)) { (data, error) in
            callback(data, error)
        }
    }
    
    func getHomeVideoPagination(skip: String, callback: @escaping CallBackHandler) {
        self.sendRequest(parameters: [:], url: Constants.GET_VIDEOS_LIST_PAGINATION.appending(skip), authkey: "", method: .GET) { (data, error) in
            callback(data, error)
        }
    }
    func getHomePageVideoPagination(callback: @escaping CallBackHandler) {
        self.sendRequest(parameters: [:], url: Constants.GET_VIDEOS_LIST_PAGINATION, authkey: "", method: .GET) { (data, error) in
            callback(data, error)
        }
    }

    //MARK:- Onboard
    func getCountryCodes(callback: @escaping CallBackHandler) {
        self.sendGetRequestWithoutAuth(url: Constants.GET_COUNTRY_CODES) { (data, error) in
            callback(data, error)
        }
    }
    
    func login(loginDict: [String : String], callback: @escaping CallBackHandler) {
        sendRequestWithoutAuth(parameters: loginDict, url: Constants.LOGIN, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func verifyOtp(otp: Int, user_id: String, callback: @escaping CallBackHandler) {
        let params = ["user_id" : user_id, "otp" : otp] as [String : Any]
        sendRequestWithoutAuth(parameters: params, url: Constants.VERIFY_OTP, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func loginFacebook(params: [String : String], callback: @escaping CallBackHandler) {
        sendRequestWithoutAuth(parameters: params, url: Constants.LOGIN_FACEBOOK, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func loginGoogle(params: [String : String], callback: @escaping CallBackHandler) {
        sendRequestWithoutAuth(parameters: params, url: Constants.LOGIN_GOOGLE, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func getContentLanguages(callback: @escaping CallBackHandler) {
        sendRequestWithoutAuth(parameters: [ : ], url: Constants.GET_LANGUAGES, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func getSplashScreen(callback: @escaping CallBackHandler) {
        sendRequestWithoutAuth(parameters: [ : ], url: Constants.GET_SPLASHSCREEN, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func getContentCategories(callback: @escaping CallBackHandler) {
        sendRequestWithoutAuth(parameters: [ : ], url: Constants.GET_CATEGORIES, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func updateLanguage(language_id: String, callback: @escaping CallBackHandler) {
        sendRequest(parameters: ["language_id" : language_id], url: Constants.UPDATE_LANGUAGE, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func updateCategory(category_id: String, callback: @escaping CallBackHandler) {
        sendRequest(parameters: ["category_id" : category_id], url: Constants.UPDATE_CATEGORY, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    //MARK:- Camera
    func getAudioList(callback: @escaping CallBackHandler) {
        self.sendGetRequestWithoutAuth(url: Constants.GET_AUDIO_LIST) { (data, error) in
            callback(data, error)
        }
    }
    
    func uploadVideo(params: [String : String], video: URL, image: UIImage, callback: @escaping CallBackHandler) {
        self.sendAttachmentRequest(params: params, videoParams: ["video_url" : video], imageParams: ["thumb_url" : image], url: Constants.UPLOAD_VIDEO, method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func getFavouriteAudioList(callback: @escaping CallBackHandler) {
        self.sendRequest(parameters: [ : ], url: Constants.GET_FAVOURITE_AUDIO_LIST, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func makeAudioFavourite(sound_id : String, callback: @escaping CallBackHandler) {
        self.sendRequest(parameters: ["sound_id" : sound_id], url: Constants.MAKE_AUDIO_FAVOURITE, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func searchAudio(search_key: String, callback: @escaping CallBackHandler) {
        self.sendRequest(parameters: ["search_key" : search_key], url: Constants.SEARCH_AUDIO, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func getHashtags(callback: @escaping CallBackHandler) {
        self.sendRequest(parameters: [:], url: Constants.VIDEO_HASHTAGS, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    //MARK:- Profile

    func getOtherProfile(user_id: String, callback: @escaping CallBackHandler) {
        let params = ["profile_id" : user_id]
        sendRequestWithoutAuth(parameters: params, url: Constants.GET_OTHER_PROFILE, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func getProfile(callback: @escaping CallBackHandler) {
        sendRequest(parameters: [:], url: Constants.GET_USER_PROFILE, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func updateProfile(params: [String : String], callback: @escaping CallBackHandler) {
        sendRequest(parameters: params, url: Constants.UPDATE_USER_PROFILE, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func uploadProfilePicture(image: UIImage, callback: @escaping CallBackHandler) {
        sendAttachmentRequest(params: [:], videoParams: nil, imageParams: ["image" : image], url: Constants.UPDATE_PROFILE_PICTURE, method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func uploadCoverPhoto(image: UIImage, callback: @escaping CallBackHandler) {
        sendAttachmentRequest(params: [:], videoParams: nil, imageParams: ["image" : image], url: Constants.UPDATE_COVER_PICTURE, method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func getPrivacySettings(callback: @escaping CallBackHandler) {
        sendRequest(parameters: [:], url: Constants.GET_PRIVACY_SETTINGS, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func updatePrivacySettings(security_type: String, security_status: String, callback: @escaping CallBackHandler) {
        sendRequest(parameters: ["security_type" : security_type, "security_status" : security_status], url: Constants.UPDATE_PRIVACY_SETTINGS, authkey: User.sharedInstance.auth_key!, method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func getDuetVideos(callback: @escaping CallBackHandler) {
        sendRequest(parameters: [:], url: Constants.GET_DUET_VIDEOS, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    //MARK:- Local
    func getFollowingList(callback: @escaping CallBackHandler) {
        sendRequest(parameters: [:], url: Constants.GET_FOLLOWING_LIST, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func getFollowersList(callback: @escaping CallBackHandler) {
        sendRequest(parameters: [:], url: Constants.GET_FOLLOWERS_LIST, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func getNotificationsList(callback: @escaping CallBackHandler) {
        sendRequest(parameters: [:], url: Constants.GET_NOTIFICATIONS_LIST, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    //MARK:- Discover
    func getTopDiscoveryData(callback: @escaping CallBackHandler) {
        sendRequest(parameters: [:], url: Constants.GET_TOP_DISCOVERY_DATA, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func searchDiscovery(search_key: String, callback: @escaping CallBackHandler) {
        sendRequest(parameters: ["search_key" : search_key], url: Constants.SEARCH_DISCOVERY, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
    
    func getAllHashtagVideos(search_key: String, callback: @escaping CallBackHandler) {
        sendRequest(parameters: ["search_key" : search_key], url: Constants.GET_ALL_HASHTAGS_VIDEOS, authkey: "", method: .POST) { (data, error) in
            callback(data, error)
        }
    }
}
