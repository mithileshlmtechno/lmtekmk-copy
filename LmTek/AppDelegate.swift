//
//  AppDelegate.swift
//  LmTek
//
//  Created by Sai Sankar on 06/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FBSDKCoreKit
//import GoogleSignIn
import Firebase
import UserNotifications


@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    static let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        FirebaseApp.configure()

        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
//        GIDSignIn.sharedInstance().clientID = "725088471536-gqcft60cp6v6rk1lfmdjto4js3l6auim.apps.googleusercontent.com"
        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().delegate = self
        
        let authOptions : UNAuthorizationOptions = [.alert, .sound, .badge]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (status, error) in
            print("requestAuthorization status:",status,"error:",error)
        }
        
        application.registerForRemoteNotifications()
                
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        if #available(iOS 13.0, *) {
            IQKeyboardManager.shared.disabledTouchResignedClasses = [UploadVideoVC.self, VideoPlayerVC.self, DiscoveryVC.self]
        } else {
            // Fallback on earlier versions
        }
        
        APIManager.shared.basicURL = Constants.BASE_URL
        
        print("font families:",UIFont.familyNames)
        
        
        self.window = UIWindow()
        
        if UserDefaults.standard.bool(forKey: Constants.KEYS.isLoggedIn) == true {
            if let loginDict = UserDefaults.standard.value(forKey: Constants.KEYS.LoginResponse) as? [String : Any], let token = loginDict["refresh_token"] as? String {
                print("login token:",token,loginDict)
                self.showHomeScreen()
            }
        }
        else {
            self.showLoginScreen()
        }

        return true
    }

    // MARK: UISceneSession Lifecycle
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        guard let url = URLContexts.first?.url else {
            return
        }
        let _ = ApplicationDelegate.shared.application(
            UIApplication.shared,
            open: url,
            sourceApplication: nil,
            annotation: [UIApplication.OpenURLOptionsKey.annotation])
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//        let googleHandle = GIDSignIn.sharedInstance()?.handle(url) ?? false
        let facebookHandle = ApplicationDelegate.shared.application(app, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
//        return googleHandle || facebookHandle
        return facebookHandle

    }

    func showLoginScreen() {
        UserDefaults.standard.setValue(nil, forKey: Constants.KEYS.LoginResponse)
        UserDefaults.standard.setValue(false, forKey: Constants.KEYS.isLoggedIn)
        UserDefaults.standard.synchronize()
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let navController = storyBoard.instantiateViewController(withIdentifier: "LoginNavigation")
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
    }
    
    func showHomeScreen() {
        let dict = UserDefaults.standard.value(forKey: Constants.KEYS.LoginResponse) as! [String : AnyObject]
        APIManager.shared.headers["token"] = (dict["refresh_token"] as! String)
        
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let navController = /*UINavigationController(rootViewController: exVC)*/storyBoard.instantiateViewController(withIdentifier: "HomeNavigation")
        //navController.isNavigationBarHidden = true
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
    }
    func showWithouttoken() {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let navController = storyBoard.instantiateViewController(withIdentifier: "HomeNavigation")
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
    }

}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
         print("didReceiveRemoteNotification:",userInfo)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("didRegisterForRemoteNotificationsWithDeviceToken deviceToken",deviceToken.map { String(format: "%02.2hhx", $0)}.joined())
        Messaging.messaging().apnsToken = deviceToken
    }
}

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("didReceiveRegistrationToken fcmtoken:",fcmToken)
        UserDefaults.standard.set(fcmToken, forKey: Constants.KEYS.FCMID)
    }
    
    
}
