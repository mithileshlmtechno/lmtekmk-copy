//
//  HomeTabBarController.swift
//  LmTek
//
//  Created by Sai Sankar on 14/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class HomeTabBarController: UITabBarController {
    //MARK:- Variables...
    var unselectedImagesArray = [#imageLiteral(resourceName: "home_unselected"), #imageLiteral(resourceName: "lmtek_unselected"), #imageLiteral(resourceName: "live_selected"), #imageLiteral(resourceName: "local_unselected"), #imageLiteral(resourceName: "profile_unselected")]
    
    var unselectedArray = [UIImage.init(named: "HomeIcon"),UIImage.init(named: "SearchWhite"),UIImage.init(named: "CenterPluss"),UIImage.init(named: "InboxWhite"),UIImage.init(named: "AccountWhite")]
    
    var selectedImagesArray = [#imageLiteral(resourceName: "home_selected"), #imageLiteral(resourceName: "lmtek_selected"), #imageLiteral(resourceName: "live_selected"), #imageLiteral(resourceName: "local_selected"), #imageLiteral(resourceName: "profile_selected")]

    var selectedImageArray = [UIImage.init(named: "HomeYellow"),UIImage.init(named: "SearchWhite"),UIImage.init(named: "CenterPluss"),UIImage.init(named: "InboxWhite"),UIImage.init(named: "AccountWhite")]
    
    var Allitem = ["Home", "Discover", "", "Search", "Profile"]
    
    
    var controller:HomeVC = HomeVC()
    var controller1:DiscoveryVC = DiscoveryVC()
    var controller2:CameraVC = CameraVC()
    var controller3:LocalVC = LocalVC()
    var controller4:ProfileVC = ProfileVC()

    var nav:UINavigationController!
    var nav1:UINavigationController!
    var nav2:UINavigationController!
    var nav3:UINavigationController!
    var nav4:UINavigationController!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBar.backgroundImage = UIImage(color: UIColor.black)//UIColor.black.withAlphaComponent(0.3))
        //self.tabBar.backgroundColor = .black
        
        self.delegate = self

        if let count = self.tabBar.items?.count {
            for i in 0 ..< count {
                self.tabBarController?.title = Allitem[i]
                self.tabBar.items?[i].selectedImage = selectedImageArray[i]!.withRenderingMode(.alwaysOriginal)
                self.tabBar.tintColor = colorYellow
                self.tabBar.items?[i].image = unselectedArray[i]!.withRenderingMode(.alwaysOriginal)
            }
        }
        
//        self.tabBar.barTintColor = colorYellow
//        self.tabBar.tintColor = colorYellow
//        self.tabBar.backgroundColor = .black
//
//        controller = HomeVC()
//        controller.tabBarItem = UITabBarItem()
//        nav = UINavigationController(rootViewController: controller)
//        nav.tabBarItem.image = UIImage.init(named: "HomeIcon")
//
//        controller1 = DiscoveryVC()
//        controller1.tabBarItem = UITabBarItem()
//        nav1 = UINavigationController(rootViewController: controller1)
//        nav1.tabBarItem.image = UIImage.init(named: "SearchWhite")
//
//        controller2 = CameraVC()
//        controller2.tabBarItem = UITabBarItem()
//        nav2 = UINavigationController(rootViewController: controller2)
//        nav2.tabBarItem.image = UIImage.init(named: "CenterPluss")
//
//        controller3 = LocalVC()
//        controller3.tabBarItem = UITabBarItem()
//        nav3 = UINavigationController(rootViewController: controller3)
//        nav3.tabBarItem.image = UIImage.init(named: "InboxWhite")
//
//        controller4 = ProfileVC()
//        controller4.tabBarItem = UITabBarItem()
//        nav4 = UINavigationController(rootViewController: controller4)
//        nav4.tabBarItem.image = UIImage.init(named: "AccountWhite")
//
//        viewControllers = [nav, nav1, nav2, nav3, nav4]


    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        /*if self.tabBar.items?[2] == item {
            self.tabBar.isHidden = true
        }
        else {
            self.tabBar.isHidden = false
        }
        if self.tabBar.items?[0] == item {
            self.tabBar.backgroundImage = UIImage(color: UIColor.black.withAlphaComponent(0.3))
        }
        else {
            self.tabBar.backgroundImage = UIImage(color: UIColor.white)
        }*/
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

@available(iOS 13.0, *)
extension HomeTabBarController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        /*Constants.previousTab = tabBarController.selectedIndex
        return true*/
        if viewController is CameraVC {
            if UserDefaults.standard.bool(forKey: Constants.KEYS.isLoggedIn) {
                /*let cameraPreviewVC = self.storyboard?.instantiateViewController(withIdentifier: "CameraPreviewVC") as! CameraPreviewVC
                self.navigationController?.pushViewController(cameraPreviewVC, animated: false)*/
                let cameraRecordingVC = self.storyboard?.instantiateViewController(withIdentifier: "CameraRecordingVC") as! CameraRecordingVC
                self.navigationController?.pushViewController(cameraRecordingVC, animated: false)
            }
            else {
                let alertController = UIAlertController(title: nil, message: "You need to login first to access this section", preferredStyle: .actionSheet)
                alertController.addAction(UIAlertAction(title: "Login", style: .default, handler: { (action) in
                    if #available(iOS 13.0, *) {
                        if let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate {
                            sceneDelegate.showLoginScreen()
                        }
                    } else {
                        if let appdelegate =  UIApplication.shared.delegate as? AppDelegate {
                            appdelegate.showLoginScreen()
                        }
                    }
                }))
                alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                DispatchQueue.main.async {
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            return false
        }
        return true
    }
}
