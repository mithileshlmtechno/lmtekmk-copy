//
//  RecordingFilterVC.swift
//  LmTek
//
//  Created by PTBLR-1128 on 24/02/21.
//  Copyright © 2021 LmTek. All rights reserved.
//

import UIKit
import AVFoundation

protocol RecordingFilterVCDelegate {
    func addRecordedAudio(isOriginalAudioActive: Bool, audioURL: URL)
}

class RecordingFilterVC: UIViewController {
    //MARK:- Outlets...
    @IBOutlet weak var collection_videoFrames : UICollectionView!
    @IBOutlet weak var view_video : UIView!
    @IBOutlet weak var view_removeOriginalSound: UIView!
    @IBOutlet weak var img_keepOriginalSound: UIImageView!

    //MARK:- Variables...
    var videoFramesArray = [UIImage]()
    var videoURL : URL?
    var audioRecorder : AVAudioRecorder?
    var audioArray = [AudioRecordedItem]()
    var videoPlayer: AVPlayer?
    var isVideoPlaying = false
    var isInitial = true
    var isOriginalSoundActive = true
    var delegate : RecordingFilterVCDelegate? = nil
    var audioPlayer: AVAudioPlayer?
    
    //MARK:- View Life Cycle...
    override func viewDidLoad() {
        super.viewDidLoad()

        self.generateVideoFrameImages()
        self.cellRegistration()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if self.isInitial {
            self.isInitial = false
            if let url = self.videoURL {
                let asset = AVAsset(url: url)
                let item = AVPlayerItem(asset: asset)
                
                self.videoPlayer = AVPlayer(playerItem: item)
                self.videoPlayer?.isMuted = true
                
                let videoLayer = AVPlayerLayer(player: self.videoPlayer)
                
                videoLayer.frame = self.view_video.bounds
                videoLayer.videoGravity = .resizeAspectFill
                self.view_video.layer.insertSublayer(videoLayer, at: 0)
                //self.videoPlayer?.actionAtItemEnd = .none
                //self.videoPlayer?.play()
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- IBActions...
    @IBAction func cancelClicked() {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func saveClicked() {
        /*let url = getAudioFileUrl()
        
        do {
            // AVAudioPlayer setting up with the saved file URL
            let sound = try AVAudioPlayer(contentsOf: url)
            self.audioPlayer = sound
            
            // Here conforming to AVAudioPlayerDelegate
            sound.delegate = self
            sound.prepareToPlay()
            sound.play()
        } catch {
            print("error loading file")
            // couldn't load file :(
        }*/
        if isRecording {
            self.isRecording = false
            self.audioRecorder?.stop()
            self.videoPlayer?.pause()
        }
        if let firstAudio = self.audioArray.first {
            print("saveClicked self.isOriginalSoundActive:",self.isOriginalSoundActive," firstAudio.audioURL:",firstAudio.audioURL)
            /*do {
                // AVAudioPlayer setting up with the saved file URL
                let sound = try AVAudioPlayer(contentsOf: firstAudio.audioURL)
                self.audioPlayer = sound
                sound.prepareToPlay()
                sound.play()
            } catch {
                print("error loading file")
                // couldn't load file :(
            }*/
            self.delegate?.addRecordedAudio(isOriginalAudioActive: self.isOriginalSoundActive, audioURL: firstAudio.audioURL)
            self.navigationController?.popViewController(animated: false)
        }
        
    }
    var isRecording = false
    @IBAction func recordClicked() {
        print("recordClicked")
        /*if let recorder = self.audioRecorder {
            recorder.stop()
            //self.videoPlayer?.pause()
            //self.audioRecorder = nil
        }*/
        if self.isRecording {
            self.isRecording = false
            self.audioRecorder?.stop()
            self.videoPlayer?.pause()
        }
        else {
            let session = AVAudioSession.sharedInstance()
            do
            {
                //try session.setCategory(.playback)
                try session.setCategory(.playAndRecord, mode: .default, options: .defaultToSpeaker)//(.playAndRecord, options: .defaultToSpeaker)
                try session.setActive(true)
                let settings = [
                    AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                    AVSampleRateKey: 44100,
                    AVNumberOfChannelsKey: 2,
                    AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue
                ]
                let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                let outputPath = documentsURL?.appendingPathComponent("recording.m4a")//caf")\(self.audioArray.count)
                if FileManager.default.fileExists(atPath: (outputPath?.path)!) {
                    do {
                        try FileManager.default.removeItem(atPath: (outputPath?.path)!)
                    }
                    catch {
                        print ("applyFilterToVideo Error deleting file:",error.localizedDescription)
                    }
                }
                self.audioRecorder = nil
                self.audioRecorder = try AVAudioRecorder(url: outputPath!, settings: settings)
                /*audioRecorder?.delegate = self
                audioRecorder?.isMeteringEnabled = true
                audioRecorder?.prepareToRecord()
                
                try AVAudioSession.sharedInstance().setCategory(.playAndRecord)*/
                //self.videoPlayer?.play()
                audioRecorder?.delegate = self
                audioRecorder?.record()
                self.videoPlayer?.volume = 0
                self.videoPlayer?.play()
                
                //5. Changing record icon to stop icon
                isRecording = true
            }
            catch let error {
                print("audio recorder exception:",error.localizedDescription)
            }
        }
    
        /*if isRecording {
            finishRecording()
        }else {
            startRecording()
        }*/
    }
    
    /*func startRecording() {
        //1. create the session
        let session = AVAudioSession.sharedInstance()
        
        do {
            // 2. configure the session for recording and playback
            try session.setCategory(.playAndRecord, mode: .default, options: .defaultToSpeaker)//(AVAudioSessionCategoryPlayAndRecord, with: .defaultToSpeaker)
            try session.setActive(true)
            // 3. set up a high-quality recording session
            let settings = [
                AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                AVSampleRateKey: 44100,
                AVNumberOfChannelsKey: 2,
                AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
            ]
            // 4. create the audio recording, and assign ourselves as the delegate
            audioRecorder = try AVAudioRecorder(url: getAudioFileUrl(), settings: settings)
            audioRecorder?.delegate = self
            audioRecorder?.record()
            
            //5. Changing record icon to stop icon
            isRecording = true
        }
        catch let error {
            // failed to record!
        }
    }
    
    // Stop recording
    func finishRecording() {
        audioRecorder?.stop()
        isRecording = false
    }
    
    // Path for saving/retreiving the audio file
    func getAudioFileUrl() -> URL{
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let docsDirect = paths[0]
        let audioUrl = docsDirect.appendingPathComponent("recording.m4a")
        return audioUrl
    }*/
    
    @IBAction func playClicked() {
        /*if self.isVideoPlaying {
            self.videoPlayer?.pause()
            self.isVideoPlaying = false
        }
        else {
            self.videoPlayer?.play()
            self.isVideoPlaying = true
        }*/
    }
    
    @IBAction func keepOriginalSoundClicked() {
        self.isOriginalSoundActive = !self.isOriginalSoundActive
        self.img_keepOriginalSound.isHidden = !self.isOriginalSoundActive
        self.view_removeOriginalSound.isHidden = self.isOriginalSoundActive
    }
    
    //MARK:- Functions...
    func cellRegistration() {
        self.collection_videoFrames.register(UINib(nibName: "VideoFrameCVCell", bundle: nil), forCellWithReuseIdentifier: "VideoFrameCVCell")
    }
    
    func generateVideoFrameImages() {
        if let url = self.videoURL {
            let asset = AVURLAsset(url: url, options: nil)
            let imageGenerator = AVAssetImageGenerator(asset: asset)
            let seconds = Int64(asset.duration.seconds)
            print("generateVideoFrameImages seconds:",seconds)
            do {
                for i in 1 ... seconds {
                    print("loop i:",i)
                    let time = CMTimeMakeWithSeconds(Float64(i), preferredTimescale: 600)
                    let cgImage = try imageGenerator.copyCGImage(at: time/*CMTime(value: 0, timescale: CMTimeScale(i))*/, actualTime: nil)
                    let image = UIImage(cgImage: cgImage)
                    self.videoFramesArray.append(image)
                }
                
                //self.img_thumbnail.image = UIImage(cgImage: cgImage)
            }
            catch {
                print("RecordingFilterVC generateVideoFrameImages error:",error.localizedDescription)
            }
        }
    }

}

extension RecordingFilterVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    //MARK:- CollectionView Dlegates...
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.videoFramesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoFrameCVCell", for: indexPath) as! VideoFrameCVCell
        
        cell.img_frame.image = self.videoFramesArray[indexPath.item]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //let aspect = self.view.frame.size.width / self.view.frame.size.height
        return CGSize(width: 30, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension RecordingFilterVC: AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    //MARK:- AVAudioRecorder Delegates...
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        /*if flag {
            finishRecording()
        }else {
            // Recording interrupted by other reasons like call coming, reached time limit.
        }*/
        print("audioRecorderDidFinishRecording:",recorder,flag)
        let url = recorder.url
        let asset = AVAsset(url: url)
        let audioItem = AudioRecordedItem(audioURL: url, startTime: .zero, videoTime: asset.duration)
        self.audioArray.append(audioItem)
        print("audioArray:",self.audioArray)
        /*do {
            let audioPlayer = try AVAudioPlayer(contentsOf: url)
            print("audio player:",audioPlayer)
            audioPlayer.play()
            /*if let player = audioPlayer {
                print("audio player:",player)
                player.play()
            }
            else {
                print("no player")
            }*/
            /*let playerItem = AVPlayerItem(asset: asset)
        self.audioPlayer = AVPlayer(playerItem: playerItem)
            print("audio player:",audioPlayer)
        self.audioPlayer?.play()*/
        }
        catch {
            print("audioRecorderDidFinishRecording player exception:",error.localizedDescription)
        }*/
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if flag {
            
        }else {
            // Playing interrupted by other reasons like call coming, the sound has not finished playing.
        }
    }
}

//MARK:- RecordedAudioModel...
struct AudioRecordedItem {
    var audioURL: URL
    var startTime: CMTime
    var videoTime: CMTime
}
