//
//  VideoPlayerVC.swift
//  LmTek
//
//  Created by PTBLR-1128 on 31/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import AVFoundation
import IQKeyboardManagerSwift
import SpriteKit
import StickerView

class VideoPlayerVC: UIViewController {
    //MARK:- Outlets...
    @IBOutlet weak var view_player : UIView!
    @IBOutlet weak var btn_upload : UIButton!
    @IBOutlet weak var btn_back : UIButton!
    @IBOutlet weak var collection_filters : UICollectionView!
    @IBOutlet weak var collection_effects : UICollectionView!
    @IBOutlet weak var constraint_filtersCollectionsHeight : NSLayoutConstraint!
    @IBOutlet weak var btn_expandCollapse : UIButton!
//    var imgSticker: stickerImageView!
    var tiledLayer = CATiledLayer()
    let canvasSize = CGSize(width: 160000, height: 160000)

    var editeHandeler: Bool = false
    var testLable:VideoTextLabel = VideoTextLabel()

    //MARK: Variables...
    
    var videoURL : URL?
    var videoPlayer : AVPlayer?
    var isInitial = true
    var filtersArray = [(title: "voice effects", image: #imageLiteral(resourceName: "ic_voice")), (title: "filters", image: #imageLiteral(resourceName: "ic_filterWhite")), (title: "Trim", image: #imageLiteral(resourceName: "ic_trim")), (title: "Volume", image: #imageLiteral(resourceName: "ic_volume")), (title: "Recording", image: #imageLiteral(resourceName: "ic_recordWhite"))]

    var effectsArray = [(title: "Sounds", image: #imageLiteral(resourceName: "ic_audioWhite")), (title: "Effects", image: #imageLiteral(resourceName: "ic_effects")), (title: "Text", image: #imageLiteral(resourceName: "ic_text")), (title: "Stickers", image: #imageLiteral(resourceName: "ic_sticker"))]
    var duetVideoID : String?
    var duetUserID : String?
    var textLabelsArray = [StickerView]()
    var stickerImagesArray = [StickerView]()
    var textLabelCurrentTag = 0
    var isAudioSelected = false
    var filter : CIFilter? = nil
    var selectedFilter : (title: String, type: FilterItem) = (title: "None", type: .NONE)
    var selectedVoiceEffect : (name: String, type: VoiceEffectItem) = (name: "None", type: .None)
    var videoEffect : (name: String, type: VideoEffect) = (name: "None", type: .None)
    var videoVolume: Float = 1.0
    var isVolumeChanged = false
    
    var window : UIWindow!
    
    var startTime: CGFloat = 0
    var stopTime: CGFloat = 0
    
    var isFiltersExpanded = false
    var btnCross: UIButton = UIButton()
    var stickerView3: StickerView!
    var stickerViewtext: StickerView!
    var strCheckSticker: String = ""
    var fontSize: Int = 25
    var emojiTag: Int = 0
    var TextTag: Int = 0
    
    var textSizeH: Int = 50
    var aftertextSizeH: Int = 50
    
    private var _selectedStickerView:StickerView?
        var selectedStickerView:StickerView? {
            get {
                return _selectedStickerView
            }
            set {
                // if other sticker choosed then resign the handler
                if _selectedStickerView != newValue {
                    if let selectedStickerView = _selectedStickerView {
                        selectedStickerView.showEditingHandlers = false
                    }
                    _selectedStickerView = newValue
                }
                // assign handler to new sticker added
                if let selectedStickerView = _selectedStickerView {
                    selectedStickerView.showEditingHandlers = true
                    selectedStickerView.superview?.bringSubviewToFront(selectedStickerView)
                }
            }
        }

    var isDuet: String = "0"
    //MARK: View Life Cycle...
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async {
            self.window = UIApplication.shared.keyWindow
        }
        self.btn_back.setImage(#imageLiteral(resourceName: "ic_backArrow").withRenderingMode(.alwaysTemplate), for: .normal)
        self.btn_back.tintColor = UIColor.white
        self.cellRegistration()
        self.setUpFiltersCollection()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        
        if !self.isMovingToParent {
            if !self.isAudioSelected {
                self.videoPlayer?.play()
                self.isAudioSelected = false
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if self.isInitial {
            self.isInitial = false
            if let url = videoURL {
                let asset = AVAsset(url: url)
                self.stopTime = CGFloat(asset.duration.seconds)
                self.playVideo(url: url)
                
                /*let videoAsset = AVAsset(url: url)
                //let ciFilters = CIFilter.filterNames(inCategory: nil)
                //print("ciFilters:",ciFilters)
                /*let filter = CIFilter(name: "CIGaussianBlur")!
                let composition = AVVideoComposition(asset: videoAsset, applyingCIFiltersWithHandler: { request in
                    
                    // Clamp to avoid blurring transparent pixels at the image edges
                    let source = request.sourceImage.clampedToExtent()
                    filter.setValue(source, forKey: kCIInputImageKey)
                    
                    // Vary filter parameters based on video timing
                    let seconds = CMTimeGetSeconds(request.compositionTime)
                    filter.setValue(seconds * 10.0, forKey: kCIInputRadiusKey)
                    
                    // Crop the blurred output to the bounds of the original image
                    let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
                    
                    // Provide the filter output to the composition
                    request.finish(with: output, context: nil)
                })*/
                
                let playerItem = AVPlayerItem(asset: videoAsset)
                //playerItem.videoComposition = composition
                self.videoPlayer = AVPlayer(playerItem: playerItem)//AVPlayer(url: url)
                
                let videoLayer = AVPlayerLayer(player: self.videoPlayer)
                videoLayer.frame = self.view_player.bounds //CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)//self.view.bounds
                videoLayer.videoGravity = .resizeAspectFill
                self.view_player.layer.insertSublayer(videoLayer, at: 0)
                self.videoPlayer?.actionAtItemEnd = .none
                NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(notification:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
                
                self.videoPlayer?.play()*/
            }
        }
    }
    
    func setUpFiltersCollection(){
        
        UIView.animate(withDuration: 0.50) {
            if self.isFiltersExpanded {
                self.btn_expandCollapse.transform = CGAffineTransform(rotationAngle: .pi)
                self.constraint_filtersCollectionsHeight.constant = CGFloat(self.filtersArray.count * 65)
            } else {
                self.btn_expandCollapse.transform = CGAffineTransform.identity
                self.constraint_filtersCollectionsHeight.constant = CGFloat(2 * 65)
            }
        }
    }

    //MARK:- IBActions...
    @IBAction func crossClicked() {
        guard let url = self.videoURL else {
            self.navigationController?.popViewController(animated: true)
            return
        }
        //try? FileManager.default.removeItem(at: url)
        self.videoPlayer?.pause()
        self.videoPlayer = nil
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func expandCollapseBtnClicked() {
        isFiltersExpanded = !isFiltersExpanded
        self.setUpFiltersCollection()
    }
    
    @IBAction func uploadClicked() {
        self.videoPlayer?.pause()
        print("condition values",self.textLabelsArray,self.filter,self.stickerImagesArray,isVolumeChanged,selectedVoiceEffect.type)
        if self.textLabelsArray.isEmpty && self.filter == nil && self.stickerImagesArray.isEmpty && !isVolumeChanged && self.selectedVoiceEffect.type == .None {
            print("uploadClicked ")
            let uploadVideoVC = self.storyboard?.instantiateViewController(withIdentifier: "UploadVideoVC") as! UploadVideoVC
            uploadVideoVC.video_url = self.videoURL
            uploadVideoVC.duetVideoID = self.duetVideoID
            uploadVideoVC.duetUserID = self.duetUserID
            uploadVideoVC.isDuet = self.isDuet
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(uploadVideoVC, animated: true)
            }
        }
        else {
            print("uploadClicked videoOutput")
            if let url = self.videoURL {
                DispatchQueue.main.async {
                    DialougeUtils.addActivityView(view: self.window)
                }

                if let filter = self.filter {
                    self.applyFilterToVideo(url: url, filter: filter)
                }
                else {
                    self.videoOutput(videoAsset: AVAsset(url: url))//, label: label)
                }
            }
        }
    }
    
    //MARK: Functions...
    
    func cellRegistration() {
        self.collection_filters.register(UINib(nibName: "CameraOptionsCVCell", bundle: nil), forCellWithReuseIdentifier: "CameraOptionsCVCell")
        self.collection_effects.register(UINib(nibName: "CameraOptionsCVCell", bundle: nil), forCellWithReuseIdentifier: "CameraOptionsCVCell")
    }
    
    func playVideo(url: URL) {
        print("VideoPlayerVC playVideo:",url)
        let asset = AVAsset(url: url)
        let item = AVPlayerItem(asset: asset)
        if let filter = self.filter {
            item.videoComposition = AVVideoComposition(asset: asset,  applyingCIFiltersWithHandler: { request in
                let source = request.sourceImage.clampedToExtent()
                filter.setValue(source, forKey: kCIInputImageKey)
                let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
                request.finish(with: output, context: nil)
            })
        }
        if self.selectedVoiceEffect.type != .None {
            item.audioTimePitchAlgorithm = AVAudioTimePitchAlgorithm.varispeed
        }
        
        self.videoPlayer = AVPlayer(playerItem: item)
        let videoLayer = AVPlayerLayer(player: self.videoPlayer)
        
        DispatchQueue.main.async {
            videoLayer.frame = self.view_player.bounds
            videoLayer.videoGravity = .resizeAspectFill
            self.view_player.layer.sublayers?.removeAll()
            self.view_player.layer.insertSublayer(videoLayer, at: 0)
            self.videoPlayer?.actionAtItemEnd = .none
            self.videoPlayer?.play()

            /*let blurEffect = UIBlurEffect(style: .light)
            let blurredEffectView = UIVisualEffectView(effect: blurEffect)
            blurredEffectView.frame = self.view_player.bounds
            self.view_player.addSubview(blurredEffectView) */

//            let pixelateNode: SKEffectNode = SKEffectNode()
//            let pixelFilter = CIFilter(name: "CIPixellate")!
//            pixelFilter.setDefaults()
//            pixelFilter.setValue(110.0, forKey: "inputScale")
//            pixelateNode.filter = pixelFilter
//            pixelateNode.shouldEnableEffects = true
            //self.addChild(pixelateNode)
            //self.addChild(pixelateNode)
        //addChild(pixelateNode)
            
//            let testSprite = SKSpriteNode(imageNamed: "testSprite")
//            pixelateNode.addChild(testSprite)

            if self.selectedVoiceEffect.type != .None {
                if self.selectedVoiceEffect.type == .Chipmunk {
                    self.videoPlayer?.rate = 1.5
                }
                else {
                    self.videoPlayer?.rate = 0.75
                }
            }
            self.videoVolume = self.videoPlayer?.volume ?? 0.0
            NotificationCenter.default.removeObserver(self)
            NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(notification:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: item)
        }
    }
    
    @objc func playerDidFinishPlaying(notification: Notification) {
        if let playerItem = notification.object as? AVPlayerItem {
            playerItem.seek(to: CMTime.zero, completionHandler: nil)
        }
    }
    
    
    func applyVideoEffects(to composition: AVMutableVideoComposition, size: CGSize) {//, currentLabel: UILabel) {

        let parentLayer = CALayer()
        let videoLayer = CALayer()
        parentLayer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        videoLayer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        parentLayer.addSublayer(videoLayer)
        
        for i in 0..<self.textLabelsArray.count {
//            let textLayer = CATextLayer()
//            textLayer.backgroundColor = UIColor.clear.cgColor
//            let attributedText = currentLabel.attributedText
//            textLayer.string = attributedText
//
//            //let scaleWidth = self.view_player.frame.size.width / size.width
//            //let scaleHeight = self.view_player.frame.size.height / size.height
//            let x = currentLabel.frame.origin.x // scaleWidth
//            let y = self.view_player.frame.size.height - (currentLabel.frame.origin.y + currentLabel.frame.size.height)//(self.view_player.frame.size.height - (currentLabel.frame.origin.y + currentLabel.frame.size.height)) / scaleHeight
//            let width = currentLabel.frame.size.width // scaleWidth
//            let height = currentLabel.frame.size.height // scaleHeight
//            textLayer.frame = CGRect(x: x, y: y, width: width, height: height)
//
//            textLayer.transform = CATransform3DInvert(currentLabel.layer.transform)
//
//            textLayer.masksToBounds = true
//            parentLayer.addSublayer(textLayer)
            
            let overlayLayer = CALayer()
            let sticker = textLabelsArray[i]
            overlayLayer.contents = sticker.contentView
            let x = sticker.frame.origin.x
            let y = self.view_player.frame.size.height - (sticker.frame.origin.y + sticker.frame.size.height)
            let width = sticker.frame.size.width
            let height = sticker.frame.size.height
            sticker.layer.frame = CGRect(x: x, y: y, width: width, height: height)
            sticker.layer.transform = CATransform3DInvert(sticker.layer.transform)
            parentLayer.addSublayer(sticker.layer)

        }
        
          for i in 0..<self.stickerImagesArray.count {
            
              let overlayLayer = CALayer()
              let sticker = stickerImagesArray[i]
              overlayLayer.contents = sticker.contentView
              let x = sticker.frame.origin.x
              let y = self.view_player.frame.size.height - (sticker.frame.origin.y + sticker.frame.size.height)
              let width = sticker.frame.size.width
              let height = sticker.frame.size.height
              sticker.layer.frame = CGRect(x: x, y: y, width: width, height: height)
              sticker.layer.transform = CATransform3DInvert(sticker.layer.transform)
              parentLayer.addSublayer(sticker.layer)
        }
        
        // 3 - apply magic
        composition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: parentLayer)
    }
    
    func applyFilterToVideo(url: URL, filter: CIFilter) {
        print("VideoPlayerVC applyFilterToVideo:",url)
        let asset = AVAsset(url: url)
        let videoComposition = AVVideoComposition(asset: asset,  applyingCIFiltersWithHandler: { request in
            
            let source = request.sourceImage.clampedToExtent()
            filter.setValue(source, forKey: kCIInputImageKey)
            
            let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
            
            request.finish(with: output, context: nil)
        })
        
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        let outputPath = documentsURL?.appendingPathComponent("newVideoWithFilter.mp4")
        if FileManager.default.fileExists(atPath: (outputPath?.path)!) {
            do {
                try FileManager.default.removeItem(atPath: (outputPath?.path)!)
            }
            catch {
                print ("applyFilterToVideo Error deleting file:",error.localizedDescription)
            }
        }
        let exporter = AVAssetExportSession.init(asset: asset, presetName: AVAssetExportPresetHighestQuality)
        exporter?.outputURL = outputPath
        exporter?.outputFileType = AVFileType.mp4
        exporter?.shouldOptimizeForNetworkUse = true
        exporter?.videoComposition = videoComposition
        exporter?.exportAsynchronously(completionHandler: {
            switch exporter?.status {
            case .completed:
                print("applyFilterToVideo successfully exported",exporter?.status.rawValue)
                if self.textLabelsArray.isEmpty {
                    DispatchQueue.main.async {
                        DialougeUtils.removeActivityView(view: self.window)
                    }
                    
                    let uploadVideoVC = self.storyboard?.instantiateViewController(withIdentifier: "UploadVideoVC") as! UploadVideoVC
                    uploadVideoVC.video_url = outputPath!
                    uploadVideoVC.duetVideoID = self.duetVideoID
                    uploadVideoVC.duetUserID = self.duetUserID
                    uploadVideoVC.isDuet = self.isDuet
                    DispatchQueue.main.async {
                        self.navigationController?.pushViewController(uploadVideoVC, animated: true)
                    }
                }
                else {
                    DispatchQueue.main.async {
                        self.videoOutput(videoAsset: AVAsset(url: outputPath!))
                    }
                }
            default:
                print("applyFilterToVideo failed export:",exporter?.status.rawValue)
                DialougeUtils.removeActivityView(view: self.window)
            }
        })
        
    }

    func videoOutput(videoAsset: AVAsset) {
        print("videoOutput videoAsset:",videoAsset)
        // Create AVMutableComposition object. This object will hold your AVMutableCompositionTrack instances.
        let mixComposition = AVMutableComposition()
        
        let duration = videoAsset.duration
        var finalTimeScale : Int64 = 0
        
        if self.selectedVoiceEffect.type == .Chipmunk {
            finalTimeScale = duration.value * 2
            finalTimeScale = finalTimeScale / 3
        }
        else if self.selectedVoiceEffect.type == .Baritone {
            finalTimeScale = duration.value * 4
            finalTimeScale = finalTimeScale / 3
        }
        
        let speedDuration = CMTime(value: finalTimeScale, timescale: duration.timescale)

        // Video track
        
        let videoTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: kCMPersistentTrackID_Invalid)!
        do {
            try videoTrack.insertTimeRange(CMTimeRange(start: CMTime.zero, duration: videoAsset.duration), of: videoAsset.tracks(withMediaType: AVMediaType.video)[0], at: CMTime.zero)
            
        } catch {
            print("Error selecting video track !!")
        }

        // Create AVMutableVideoCompositionInstruction

        let mainInstruction = AVMutableVideoCompositionInstruction()
        if self.selectedVoiceEffect.type == .None {
            mainInstruction.timeRange = CMTimeRange(start: CMTime.zero, duration: videoAsset.duration)
        }
        else {
            mainInstruction.timeRange = CMTimeRange(start: CMTime.zero, duration: speedDuration)
        }
        // Create an AvmutableVideoCompositionLayerInstruction for the video track and fix orientation

        let videoLayerInstruction = AVMutableVideoCompositionLayerInstruction.init(assetTrack: videoTrack)
        let videoAssetTrack = videoAsset.tracks(withMediaType: AVMediaType.video)[0]
        
        
        let dates = [Date(),Date()]
        if dates.contains(where: ({ Calendar.current.compare($0, to: Date(), toGranularity: .day) == .orderedSame})) {
            
        }
            videoLayerInstruction.setOpacity(0.0, at: videoAsset.duration)

        let audioMix: AVMutableAudioMix? = AVMutableAudioMix()
        if let audioTrackFirst = videoAsset.tracks(withMediaType: .audio).first {
            let audioTrack = mixComposition.addMutableTrack(
                withMediaType: .audio,
                preferredTrackID: kCMPersistentTrackID_Invalid)
            
            do {
                try audioTrack?.insertTimeRange(
                    CMTimeRangeMake(
                        start: CMTime.zero,
                        duration: videoAsset.duration),
                    of: audioTrackFirst,
                    at: .zero)
                
            } catch {
                print("Failed to load Audio track")
            }
            
            if let track = audioTrack {
                var audioMixParams = [AVMutableAudioMixInputParameters]()
                let audioMixParam = AVMutableAudioMixInputParameters(track: audioTrackFirst)
                audioMixParam.trackID = track.trackID
                audioMixParam.setVolume(self.videoPlayer?.volume ?? 0.0, at: .zero)
                if self.selectedVoiceEffect.type != .None {
                    audioMixParam.audioTimePitchAlgorithm = AVAudioTimePitchAlgorithm.varispeed
                    videoTrack.scaleTimeRange(CMTimeRange(start: .zero, duration: videoAsset.duration), toDuration: speedDuration)
                    audioTrack?.scaleTimeRange(CMTimeRange(start: .zero, duration: videoAsset.duration), toDuration: speedDuration)
                }
                audioMixParams.append(audioMixParam)
                audioMix?.inputParameters = audioMixParams
            }
            
        }
        //Add instructions

        mainInstruction.layerInstructions = [videoLayerInstruction]
        let mainCompositionInst = AVMutableVideoComposition()
        
        let naturalSize : CGSize! = videoAssetTrack.naturalSize
       
        let renderWidth = naturalSize.width
        let renderHeight = naturalSize.height
        
        
        let height = self.view_player.frame.size.height
        let width = self.view_player.frame.size.width
        
        print("video dimesnions",height,width,renderWidth,renderHeight)
        videoLayerInstruction.setTransform(CGAffineTransform(scaleX: 1, y: 1), at: .zero)
        
//        videoLayerInstruction.setTransform(CGAffineTransform(scaleX: width / renderWidth, y: height / renderHeight), at: .zero)
        
        if width > renderWidth{
            mainCompositionInst.renderSize = CGSize(width: width, height: height)
            
        }
        else{
        mainCompositionInst.renderSize = /*CGSize(width: width, height: height)*/CGSize(width: renderWidth, height: renderHeight)
        }
        mainCompositionInst.renderScale = 1.0
        mainCompositionInst.instructions = [mainInstruction]
        mainCompositionInst.frameDuration = CMTime(value: 1, timescale: 30)

        self.applyVideoEffects(to: mainCompositionInst, size: naturalSize)//, currentLabel: label)

        // Get Path
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        let outputPath = documentsURL?.appendingPathComponent("newVideoWithLabel.mp4")
        if FileManager.default.fileExists(atPath: (outputPath?.path)!) {
            do {
                try FileManager.default.removeItem(atPath: (outputPath?.path)!)
            }
            catch {
                print ("Error deleting file")
            }
        }
        //MARK: Create exporter

        let exporter = AVAssetExportSession.init(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)
        exporter?.outputURL = outputPath
        exporter?.outputFileType = AVFileType.mp4
        if let mix = audioMix {
            exporter?.audioMix = mix
        }
        exporter?.shouldOptimizeForNetworkUse = true
        exporter?.videoComposition = mainCompositionInst
        print("videoOutput exporter:",exporter!)
        exporter?.exportAsynchronously(completionHandler: {
            print("videoOutput exportAsynchronously:")
            self.exportDidFinish(session: exporter!)
        })
        
    }

    func exportDidFinish(session: AVAssetExportSession) {
        print("exportDidFinish session:",session.status == .completed)
        DispatchQueue.main.async {
            DialougeUtils.removeActivityView(view: self.window)
        }
        if session.status == .completed {
            print("exportDidFinish completed")
            if let outputURL: URL = session.outputURL {
                DispatchQueue.main.async {
                    let uploadVideoVC = self.storyboard?.instantiateViewController(withIdentifier: "UploadVideoVC") as! UploadVideoVC
                    uploadVideoVC.video_url = outputURL
                    uploadVideoVC.duetVideoID = self.duetVideoID
                    uploadVideoVC.duetUserID = self.duetUserID
                    uploadVideoVC.isDuet = self.isDuet
                    self.navigationController?.pushViewController(uploadVideoVC, animated: true)
                }
                /*print("present activity sheet")
                let linkToShare = [outputURL]
                let activityViewController = UIActivityViewController(activityItems: linkToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view // so that ipad won't crash
                
                activityViewController.completionWithItemsHandler = {(activityType: UIActivity.ActivityType?, completed: Bool, returnedItems:[Any]?, error: Error?) in
                    
                    print("activityType:",activityType, ", completed:",completed)
                }
                DispatchQueue.main.async {
                    self.present(activityViewController, animated: true, completion: nil)
                }*/
            }
            /*PHPhotoLibrary.shared().performChanges({
                PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: outputURL!)
            }) { saved, error in
                if saved {
                    let fetchOptions = PHFetchOptions()
                    fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
                    let fetchResult = PHAsset.fetchAssets(with: .video, options: fetchOptions).lastObject
                    PHImageManager().requestAVAsset(forVideo: fetchResult!, options: nil, resultHandler: { (avurlAsset, audioMix, dict) in
                        let newObj = avurlAsset as! AVURLAsset
                        print(newObj.url)
                        DispatchQueue.main.async(execute: {
                            print(newObj.url.absoluteString)
                        })
                    })
                    print (fetchResult!)
                }
            }*/
        }
    }
    
    //MARK: Get Video Efects
    
    func videoEfects() {
//
//        self.videoPlayer = AVPlayer(playerItem: playerItem)
//        let videoLayer = AVPlayerLayer(player: self.videoPlayer)

//        DispatchQueue.main.async {
//            videoLayer.frame = self.view_player.bounds
//            videoLayer.videoGravity = .resizeAspectFill
//            self.view_player.layer.sublayers?.removeAll()
//            self.view_player.layer.insertSublayer(videoLayer, at: 0)
//            self.videoPlayer?.actionAtItemEnd = .none
//            self.videoPlayer?.play()
//            if self.selectedVoiceEffect.type != .None {
//                if self.selectedVoiceEffect.type == .Chipmunk {
//                    self.videoPlayer?.rate = 1.5
//                }
//                else {
//                    self.videoPlayer?.rate = 0.75
//                }
//            }
//            self.videoVolume = self.videoPlayer?.volume ?? 0.0
//            NotificationCenter.default.removeObserver(self)
//            NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(notification:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
//        }

        let viewEfectBG:UIView = UIView()
        viewEfectBG.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        viewEfectBG.backgroundColor = .black.withAlphaComponent(0.3)
        self.view.addSubview(viewEfectBG)
        
        
    }
    func showediteSticker(_ stickerView: StickerView){
        if editeHandeler == false {
            stickerView.showEditingHandlers = false
            editeHandeler = true
        } else {
            stickerView.showEditingHandlers = true
            editeHandeler = false
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let position = touch.location(in: view)
            print(position)

        }
    }
}

extension VideoPlayerVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    //MARK:- CollectionView Delegates...
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collection_filters {
            return self.filtersArray.count
        }
        else {
            return self.effectsArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CameraOptionsCVCell", for: indexPath) as! CameraOptionsCVCell
        
        if collectionView == self.collection_filters {
            let model = self.filtersArray[indexPath.item]
            cell.lbl_title.text = model.title
            if indexPath.row == 5 {
                cell.img_option.image = model.image.withRenderingMode(.alwaysTemplate)
                cell.img_option.tintColor = UIColor.white
            }
            else {
                cell.img_option.image = model.image.withRenderingMode(.alwaysOriginal)
            }
        }
        
        else {
            let model = self.effectsArray[indexPath.item]
            cell.lbl_title.text = model.title
            cell.img_option.image = model.image.withRenderingMode(.alwaysOriginal)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.collection_filters {
            if indexPath.item == 0 {
                return CGSize(width: 50, height: 80)
            }
            return CGSize(width: 50, height: 65)
        }
        return CGSize(width: 50, height: 65)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == self.collection_filters {
            return 5
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == self.collection_effects {
            return 5
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.collection_effects {
            if indexPath.item == 0 {
                let audiosListVC = self.storyboard?.instantiateViewController(withIdentifier: "AudiosListVC") as! AudiosListVC
                audiosListVC.delegate = self
                self.videoPlayer?.pause()
                self.navigationController?.pushViewController(audiosListVC, animated: true)
            } else if indexPath.row == 1 {
               // self.videoEfects()
//                let EfectsView = Bundle.main.loadNibNamed("EfectsView", owner: nil, options: nil)![0] as! EfectsView
//                EfectsView.delegate = self
//                EfectsView.selectedvideoEffect = self.videoEffect
//                self.view.addSubview(EfectsView)

            }
            else if indexPath.item == 2 {
                let addTextToVideoView = Bundle.main.loadNibNamed("AddTextToVideoView", owner: nil, options: nil)![0] as! AddTextToVideoView
                addTextToVideoView.delegate = self
                self.view.addSubview(addTextToVideoView)
            }
            else if indexPath.item == 3 {
                let stickerView = Bundle.main.loadNibNamed("ViewSticker", owner: nil, options: nil)![0] as! ViewSticker
                stickerView.delegate = self
                self.view.addSubview(stickerView)
            }
        }
        else if collectionView == self.collection_filters {
            if indexPath.item == 0 {
                print("voice effects view")
                let voiceEffectSelectionView = Bundle.main.loadNibNamed("VoiceEffectSelectionView", owner: nil, options: nil)![0] as! VoiceEffectSelectionView
                voiceEffectSelectionView.delegate = self
                voiceEffectSelectionView.selectedVoiceEffect = self.selectedVoiceEffect
                self.view.addSubview(voiceEffectSelectionView)
            }
            else if indexPath.item == 1 {
                print("filter view")
                let filterSelectionView = Bundle.main.loadNibNamed("FilterSelectionView", owner: nil, options: nil)![0] as! FilterSelectionView
                filterSelectionView.delegate = self
                filterSelectionView.selectedFilter = self.selectedFilter
                self.view.addSubview(filterSelectionView)
            }
            else if indexPath.item == 2 {
                if let url = self.videoURL {
                    self.videoPlayer?.pause()
                    let videoTrimmerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoTrimmerVC") as! VideoTrimmerVC
                    videoTrimmerVC.videoAsset = AVAsset(url: url)
                    videoTrimmerVC.delegate = self
                    self.navigationController?.pushViewController(videoTrimmerVC, animated: false)
                }
            }
            else if indexPath.item == 3 {
                let volumeFilterView = Bundle.main.loadNibNamed("VolumeFilterView", owner: nil, options: nil)![0] as! VolumeFilterView
                volumeFilterView.delegate = self
                volumeFilterView.previousValue = self.videoPlayer?.volume ?? 0//self.videoVolume
                self.view.addSubview(volumeFilterView)
            }
            else if indexPath.item == 4 {
                print("RecordingFilterVC")
                self.videoPlayer?.pause()
                let recordingFilterVC = self.storyboard?.instantiateViewController(withIdentifier: "RecordingFilterVC") as! RecordingFilterVC
                recordingFilterVC.videoURL = self.videoURL
                recordingFilterVC.delegate = self
                self.navigationController?.pushViewController(recordingFilterVC, animated: false)
            }
        }
    }
    
}

extension VideoPlayerVC: AddTextToVideoViewDelegate {
    //MARK: AddTextToVideoView Delegates...
    func addLabel(text: String, font: (title: String, font: String), alignment: NSTextAlignment, textStyle: Int, color: UIColor, selectedFont: Int) {
        guard !text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            return
        }
        
        strCheckSticker = "text"
        TextTag = TextTag+1

//        let textLabel = VideoTextLabel()
//        textLabel.delegate = self
//        textLabel.numberOfLines = 0
//
//        textLabel.textFont = font
//        textLabel.alignment = alignment
//        textLabel.textStyle = textStyle
//        textLabel.color = color
//        textLabel.selectedFont = selectedFont
//
//        var backgroundColor : UIColor
//        var foregroundColor : UIColor
//        switch textStyle {
//        case 0:
//            backgroundColor = .clear
//            foregroundColor = color
//        case 1:
//            backgroundColor = color
//            if color == .white {
//                foregroundColor = .black
//            }
//            else {
//                foregroundColor = .white
//            }
//        case 2:
//            backgroundColor = color.withAlphaComponent(0.5)
//            foregroundColor = .white
//        default:
//            backgroundColor = .clear
//            foregroundColor = color
//        }
//        let paragraphStyle = NSMutableParagraphStyle()
//        paragraphStyle.alignment = alignment
//        let fontName = UIFont(name: font.font, size: 25)!
//        let attributedString = NSAttributedString(string: text, attributes: [NSAttributedString.Key.foregroundColor : foregroundColor, NSAttributedString.Key.backgroundColor : backgroundColor, NSAttributedString.Key.font : fontName/*self.txt_description.font ?? UIFont.boldSystemFont(ofSize: 25)*/, NSAttributedString.Key.paragraphStyle : paragraphStyle])
//        textLabel.attributedText = attributedString
//
//        let contentSize = textLabel.intrinsicContentSize
//        textLabel.frame = CGRect(x: (self.view.frame.size.width - contentSize.width) / 2, y: (self.view.frame.size.height - contentSize.height) / 2, width: contentSize.width, height: contentSize.height)
//
//        textLabel.isUserInteractionEnabled = true
//        let doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(doubleTapped(_:)))
//        doubleTapGesture.delegate = self
//        doubleTapGesture.numberOfTapsRequired = 2
//        textLabel.addGestureRecognizer(doubleTapGesture)
//
//        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panAction))
//        panGesture.delegate = self
//        textLabel.addGestureRecognizer(panGesture)
//
//        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(pinchAction))
//        pinchGesture.delegate = self
//        textLabel.addGestureRecognizer(pinchGesture)
//
//        let rotateGesture = UIRotationGestureRecognizer(target: self, action: #selector(rotateAction))
//        rotateGesture.delegate = self
//        textLabel.addGestureRecognizer(rotateGesture)
//
//        print("textLabel frame:",textLabel.frame)
//        self.view.addSubview(textLabel)
//        self.view.bringSubviewToFront(textLabel)
        
        //textLabel.tag = textLabelCurrentTag
        

        testLable = VideoTextLabel()
        testLable.frame = CGRect(x: (screenWidth-50)/2, y: (screenHeight-50)/2, width: 90, height: 30)
        
        var backgroundColor : UIColor
        var foregroundColor : UIColor
        switch textStyle {
        case 0:
            backgroundColor = .clear
            foregroundColor = color
        case 1:
            backgroundColor = color
            if color == .white {
                foregroundColor = .black
            }
            else {
                foregroundColor = .white
            }
        case 2:
            backgroundColor = color.withAlphaComponent(0.5)
            foregroundColor = .white
        default:
            backgroundColor = .clear
            foregroundColor = color
        }
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = alignment

        let fontName = UIFont(name: font.font, size: 25)!
        let attributedString = NSAttributedString(string: text, attributes: [NSAttributedString.Key.foregroundColor : foregroundColor, NSAttributedString.Key.backgroundColor : backgroundColor, NSAttributedString.Key.font : fontName, NSAttributedString.Key.paragraphStyle : paragraphStyle])
        testLable.attributedText = attributedString

        stickerViewtext = StickerView.init(contentView: testLable)
        stickerViewtext.center = CGPoint.init(x: 150, y: 150)
        stickerViewtext.delegate = self
        stickerViewtext.setImage(UIImage.init(named: "Cross")!, forHandler: StickerViewHandler.close)
        stickerViewtext.setImage(UIImage.init(named: "increase-size")!, forHandler: StickerViewHandler.rotate)
        stickerViewtext.setImage(UIImage.init(named: "ic_crop")!.withRenderingMode(.alwaysTemplate), forHandler: StickerViewHandler.flip)
        stickerViewtext.tintColor = .white
        stickerViewtext.showEditingHandlers = false
        stickerViewtext.tag = TextTag
        self.view.addSubview(stickerViewtext)
        self.view.bringSubviewToFront(stickerViewtext)
        self.selectedStickerView = stickerViewtext
        
        textLabelCurrentTag += 1
        textLabelsArray.append(stickerViewtext)
        
    }
    
    func editLabel(text: String, font: (title: String, font: String), alignment: NSTextAlignment, textStyle: Int, color: UIColor, tag: Int, selectedFont: Int) {
        if let index = self.textLabelsArray.firstIndex(where: ({ $0.tag == tag})) {
            let textLabel = self.textLabelsArray[index]
            
            guard !text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
                textLabel.removeFromSuperview()
                self.textLabelsArray.remove(at: index)
                return
            }
            
//            textLabel.textFont = font
//            textLabel.alignment = alignment
//            textLabel.textStyle = textStyle
//            textLabel.color = color
//            textLabel.selectedFont = selectedFont
            
            var backgroundColor : UIColor
            var foregroundColor : UIColor
            switch textStyle {
            case 0:
                backgroundColor = .clear
                foregroundColor = color
            case 1:
                backgroundColor = color
                if color == .white {
                    foregroundColor = .black
                }
                else {
                    foregroundColor = .white
                }
            case 2:
                backgroundColor = color.withAlphaComponent(0.5)
                foregroundColor = .white
            default:
                backgroundColor = .clear
                foregroundColor = color
            }
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = alignment
            let fontName = UIFont(name: font.font, size: 25)!
            let attributedString = NSAttributedString(string: text, attributes: [NSAttributedString.Key.foregroundColor : foregroundColor, NSAttributedString.Key.backgroundColor : backgroundColor, NSAttributedString.Key.font : fontName, NSAttributedString.Key.paragraphStyle : paragraphStyle])
//            textLabel.attributedText = attributedString
            
            let contentSize = textLabel.intrinsicContentSize
            textLabel.frame.size = CGSize(width: contentSize.width, height: contentSize.height)
            
            textLabel.isHidden = false
        }
    }
    
    @objc func doubleTapped(_ gesture: UITapGestureRecognizer) {
        print("doubleTapped:")
        self.editLabel(label: gesture.view as! VideoTextLabel)
    }
    
    @objc func panAction(_ gesture: UIPanGestureRecognizer) {
        print("pan Action:",gesture.state)
        if gesture.state == UIGestureRecognizer.State.began {
            // add something you want to happen when the Label Panning has started
        }

        if gesture.state == UIGestureRecognizer.State.ended {
            // add something you want to happen when the Label Panning has ended
        }

        if gesture.state == UIGestureRecognizer.State.changed {
            // add something you want to happen when the Label Panning has been change ( during the moving/panning )
            let point = gesture.location(in: self.view)
            let gestureView = gesture.view
            gestureView?.center = point
            print("point:",point)
        }
    }
    
    @objc func pinchAction(_ gesture: UIPinchGestureRecognizer) {
        print("pinchAction")
        guard gesture.view != nil else {return}
        
        let textLabel = gesture.view as! VideoTextLabel
        
        if gesture.state == .began {
            let font = textLabel.font
            textLabel.pointSize = font!.pointSize
            
            gesture.scale = textLabel.font!.pointSize * 0.1
        }
        if 1 <= gesture.scale && gesture.scale <= 10  {
            textLabel.font = UIFont(name: textLabel.font!.fontName, size: gesture.scale * 10)
            
            resizeLabelToText(textLabel: textLabel)
        }
    }
    
    @objc func rotateAction(_ gesture: UIRotationGestureRecognizer) {
        if let view = gesture.view as? VideoTextLabel {
            view.transform = view.transform.rotated(by: gesture.rotation)
            view.rotationAngle = gesture.rotation
            gesture.rotation = 0
        }
    }
    
    func resizeLabelToText(textLabel : UILabel) {
        let labelSize = textLabel.intrinsicContentSize
        textLabel.bounds.size = labelSize
    }
}

extension VideoPlayerVC: UIGestureRecognizerDelegate {
    //MARK:- GestureRecognizer Delegates...
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension VideoPlayerVC: VideoTextLabelDelegate {
    //MARK:- VideoTextLabel Delegates...
    func editLabel(label: VideoTextLabel) {
        print("editLabel")
        let addTextToVideoView = Bundle.main.loadNibNamed("AddTextToVideoView", owner: nil, options: nil)![0] as! AddTextToVideoView
        addTextToVideoView.delegate = self
        
        addTextToVideoView.addTextType = .EDIT
        addTextToVideoView.labelTag = label.tag
        addTextToVideoView.labelText = label.text
        addTextToVideoView.selectedAlignment = label.alignment ?? .center
        addTextToVideoView.selectedColor = label.color ?? .white
        addTextToVideoView.selectedTextStyle = label.textStyle
        addTextToVideoView.selectedFont = label.selectedFont
        
        label.isHidden = true
        self.view.addSubview(addTextToVideoView)
    }
}

extension VideoPlayerVC: AudioListVCDelegate {
    //MARK:- AudioListVC Delegates...
    func selectedAudio(url: String?, id: String?, name: String?) {
        if let url = URL(string: url ?? ""), let video_url = self.videoURL {
            self.isAudioSelected = true
            let audioAsset = AVAsset(url: url)
            let videoAsset = AVAsset(url: video_url)
            
            let mixComposition = AVMutableComposition()
            
            let videoTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
            let audioTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)
            
            let videoDuration = videoAsset.duration
            
            do {
                try videoTrack?.insertTimeRange(CMTimeRange(start: .zero, duration: videoDuration), of: videoAsset.tracks(withMediaType: .video)[0], at: .zero)
                try audioTrack?.insertTimeRange(CMTimeRange(start: .zero, duration: videoDuration), of: audioAsset.tracks(withMediaType: .audio)[0], at: .zero)
            }
            catch {
                print("VideoPlayerVC error to insert time:")
            }
            
            let assetExport = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)!
            assetExport.outputFileType = .mp4
            try? FileManager().removeItem(at: video_url)
            assetExport.outputURL = video_url
            assetExport.shouldOptimizeForNetworkUse = true
            
            assetExport.exportAsynchronously {
                print("VideoPlayerVC exporter")
                switch assetExport.status {
                case .completed:
                    print("success")
                    self.videoPlayer?.pause()
                    self.playVideo(url: video_url)
                    self.isAudioSelected = false
                    /*let playerItem = AVPlayerItem(asset: AVAsset(url: video_url))
                    self.videoPlayer = AVPlayer(playerItem: playerItem)
                    
                    let videoLayer = AVPlayerLayer(player: self.videoPlayer)
                    videoLayer.frame = self.view_player.bounds
                    videoLayer.videoGravity = .resizeAspectFill
                    DispatchQueue.main.async {
                        self.view_player.layer.sublayers?.removeAll()
                        self.view_player.layer.insertSublayer(videoLayer, at: 0)
                        self.videoPlayer?.actionAtItemEnd = .none
                        self.videoPlayer?.play()
                        NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(notification:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
                    }*/
                case .failed:
                    print("failed",assetExport.error)
                case .cancelled:
                    print("cancelled",assetExport.error)
                default:
                    print("default switch")
                }
            }
            
            print("VideoPlayerVC selectedAudio audioAsset:",audioAsset)
        }
    }
}

extension VideoPlayerVC: FilterSelectionViewDelegate {
    //MARK:- FilterSelectionView Delegates...
    func didSelectFilter(filter: (title: String, type: FilterItem)) {
        self.videoPlayer?.pause()
        print("VideoPlayerVC selected filter:",filter)
        self.selectedFilter = filter
        var ciFilter : CIFilter?
        switch filter.type {
        case .NONE:
            ciFilter = nil
        /*case .MONOCHROME_FILTER:
            self.filter = CIFilter(name: "CIColorMonochrome")
        case .INVERT_COLORS_FILTER:
            self.filter = CIFilter(name: "CIColorInvert")
        case .FALSE_COLOR_FILTER:
            self.filter = CIFilter(name: "CIFalseColor")*/
        case .COLOR_MATRIX_FILTER:
            ciFilter = CIFilter(name: filter.type.rawValue)
            ciFilter?.setValue(CIVector(x: 0, y: 0, z: 0, w: 0), forKey: "inputGVector")
        case .TEMPERATURE_TINT_FILTER:
            ciFilter = CIFilter(name: filter.type.rawValue)
            if filter.title == "F10" {
                ciFilter?.setValue(CIVector(x: 16000, y: 1000), forKey: "inputNeutral")
                ciFilter?.setValue(CIVector(x: 1000, y: 500), forKey: "inputTargetNeutral")
            }
            else if filter.title == "F11" {
                ciFilter?.setValue(CIVector(x: 6500, y: 500), forKey: "inputNeutral")
                ciFilter?.setValue(CIVector(x: 1000, y: 630), forKey: "inputTargetNeutral")
            }
        default:
            ciFilter = CIFilter(name: filter.type.rawValue)
        }
        self.filter = ciFilter
        if let url = self.videoURL {
            self.playVideo(url: url)
        }
        /*if let filt = ciFilter, let url = self.videoURL {
            print("VideoPlayerVC filt:",filt,url)
            let asset = AVAsset(url: url)
            let item = AVPlayerItem(asset: asset)
            item.videoComposition = AVVideoComposition(asset: asset,  applyingCIFiltersWithHandler: { request in

                print("item.videoComposition")
                // Clamp to avoid blurring transparent pixels at the image edges
                let source = request.sourceImage.clampedToExtent()
                filt.setValue(source, forKey: kCIInputImageKey)

                // Vary filter parameters based on video timing
                /*let seconds = CMTimeGetSeconds(request.compositionTime)
                filt.setValue(seconds * 10.0, forKey: kCIInputRadiusKey)*/

                // Crop the blurred output to the bounds of the original image
                let output = filt.outputImage!.cropped(to: request.sourceImage.extent)

                // Provide the filter output to the composition
                request.finish(with: output, context: nil)
            })
            
            self.videoPlayer = AVPlayer(playerItem: item)
            
            let videoLayer = AVPlayerLayer(player: self.videoPlayer)
            
            DispatchQueue.main.async {
                print("filter video")
                videoLayer.frame = self.view_player.bounds
                videoLayer.videoGravity = .resizeAspectFill
                self.view_player.layer.sublayers?.removeAll()
                self.view_player.layer.insertSublayer(videoLayer, at: 0)
                self.videoPlayer?.actionAtItemEnd = .none
                self.videoPlayer?.play()
                NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(notification:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: item)
            }
            
        }
        else {
            print("VideoPlayerVC filt else:")
            if let url = self.videoURL {
                print("VideoPlayerVC url:",url)
                self.videoPlayer?.pause()
                let playerItem = AVPlayerItem(asset: AVAsset(url: url))
                self.videoPlayer = AVPlayer(playerItem: playerItem)
                
                let videoLayer = AVPlayerLayer(player: self.videoPlayer)
                videoLayer.frame = self.view_player.bounds
                videoLayer.videoGravity = .resizeAspectFill
                DispatchQueue.main.async {
                    self.view_player.layer.sublayers?.removeAll()
                    self.view_player.layer.insertSublayer(videoLayer, at: 0)
                    self.videoPlayer?.actionAtItemEnd = .none
                    self.videoPlayer?.play()
                    NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(notification:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
                }
            }
        }*/
    }
}

extension VideoPlayerVC: VoiceEffectSelectionViewDelegate {
    //MARK: VoiceEffectSelectionView Delegates...
    func didSelectVoiceEffect(voiceEffect: (name: String, type: VoiceEffectItem)) {
        print("VideoPlayerVC didSelectVoiceEffect voiceEffect:",voiceEffect)
        self.selectedVoiceEffect = voiceEffect
        if let url = self.videoURL {
            self.playVideo(url: url)
        }
        /*if self.selectedVoiceEffect.type != .None {
            if self.selectedVoiceEffect.type == .Chipmunk {
                self.videoPlayer?.rate = 2.0
            }
            else {
                self.videoPlayer?.rate = 0.75
            }
        }
        else {
            self.videoPlayer?.rate = 1.0
        }*/
    }
}

extension VideoPlayerVC: RecordingFilterVCDelegate {
    //MARK:- RecordingFilterVC Delegates...
    func addRecordedAudio(isOriginalAudioActive: Bool, audioURL: URL) {
        print("addRecordedAudio isOriginalAudioActive:",isOriginalAudioActive," audioURL:",audioURL)
        if let video_url = self.videoURL {
            self.isAudioSelected = true
            let audioAsset = AVAsset(url: audioURL)
            let videoAsset = AVAsset(url: video_url)
            
            let mixComposition = AVMutableComposition()
            
            let videoTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
            let audioTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)
            let videoAudioTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)
            
            let videoDuration = videoAsset.duration
            
            do {
                try videoTrack?.insertTimeRange(CMTimeRange(start: .zero, duration: videoDuration), of: videoAsset.tracks(withMediaType: .video)[0], at: .zero)
                if isOriginalAudioActive, let originalTrack = videoAsset.tracks(withMediaType: .audio).first {
                    try videoAudioTrack?.insertTimeRange(CMTimeRange(start: .zero, duration: videoDuration), of: originalTrack, at: .zero)
                }
                try audioTrack?.insertTimeRange(CMTimeRange(start: .zero, duration: videoDuration), of: audioAsset.tracks(withMediaType: .audio)[0], at: .zero)
            }
            catch {
                print("VideoPlayerVC addRecordedAudio error to insert time:")
            }
            
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
            let outputPath = documentsURL?.appendingPathComponent("newVideoWithRecordedAudio.mp4")
            if FileManager.default.fileExists(atPath: (outputPath?.path)!) {
                do {
                    try FileManager.default.removeItem(atPath: (outputPath?.path)!)
                }
                catch {
                    print ("addRecordedAudio Error deleting file:",error.localizedDescription)
                }
            }
            let assetExport = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)!
            assetExport.outputFileType = .mp4
            assetExport.outputURL = outputPath
            assetExport.shouldOptimizeForNetworkUse = true
            
            assetExport.exportAsynchronously {
                print("VideoPlayerVC addRecordedAudio exporter")
                switch assetExport.status {
                case .completed:
                    print("success")
                    self.videoPlayer?.pause()
                    if let url = outputPath {
                        self.playVideo(url: url)
                    }
                    self.isAudioSelected = false
                case .failed:
                    print("failed",assetExport.error)
                case .cancelled:
                    print("cancelled",assetExport.error)
                default:
                    print("default switch")
                }
            }
            
            print("VideoPlayerVC addRecordedAudio audioAsset:",audioAsset)
        }
    }
}

@available(iOS 13.0, *)
extension VideoPlayerVC: StickerDelegate {
    //MARK:- StickerView Delegates...
    func didSelect(sticker: UIImage) {
        
        emojiTag = emojiTag+1
        
        strCheckSticker = "emoji"
        
        let testImage = UIImageView.init(frame: CGRect.init(x: (screenWidth-50)/2, y: (screenHeight-50)/2, width: 50, height: 50))
        testImage.image = sticker
        testImage.contentMode = .scaleAspectFit
        stickerView3 = StickerView.init(contentView: testImage)
        stickerView3.center = CGPoint.init(x: 150, y: 150)
        stickerView3.delegate = self
        stickerView3.setImage(UIImage.init(named: "Cross")!, forHandler: StickerViewHandler.close)
        stickerView3.setImage(UIImage.init(named: "increase-size")!, forHandler: StickerViewHandler.rotate)
        stickerView3.setImage(UIImage.init(named: "ic_crop")!.withRenderingMode(.alwaysTemplate), forHandler: StickerViewHandler.flip)
        stickerView3.tintColor = .white
        stickerView3.showEditingHandlers = false
        stickerView3.tag = emojiTag
        self.view.addSubview(stickerView3)
        self.selectedStickerView = stickerView3
                
        self.stickerImagesArray.append(stickerView3)

    }
}

extension VideoPlayerVC: VolumeFilterViewDelegate {
    //MARK:- VolumeFilterView Delegates...
    func adjustVolumeOfVideo(volume: Float) {
        if self.videoVolume != volume {
            self.isVolumeChanged = true
        }
        self.videoPlayer?.volume = volume
        self.videoVolume = volume
    }
    
    func sliderAdjusted(volume: Float) {
        self.videoPlayer?.volume = volume
    }
}

extension VideoPlayerVC: VideoTrimmerVCDelegate {
    //MARK:- VideoTrimmerVC Delegates...
    func trimmedVideo(url: URL, startTime: CGFloat, stopTime: CGFloat) {
        self.videoURL = url
        self.playVideo(url: url)
        //self.startTime = startTime
        //self.stopTime = stopTime
    }
}

extension VideoPlayerVC: EfectsViewDelegate {
    func didSelectEffect(voiceEffect: (name: String, type: VideoEffect)) {
        
    }
    
}
extension UIImage {
    func imageWithInsets(insetDimen: CGFloat) -> UIImage {
        return imageWithInset(insets: UIEdgeInsets(top: insetDimen, left: insetDimen, bottom: insetDimen, right: insetDimen))
    }
    
    func imageWithInset(insets: UIEdgeInsets) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(
            CGSize(width: self.size.width + insets.left + insets.right,
                   height: self.size.height + insets.top + insets.bottom), false, self.scale)
        let origin = CGPoint(x: insets.left, y: insets.top)
        self.draw(at: origin)
        let imageWithInsets = UIGraphicsGetImageFromCurrentImageContext()?.withRenderingMode(self.renderingMode)
        UIGraphicsEndImageContext()
        return imageWithInsets!
    }
    
}

extension VideoPlayerVC : StickerViewDelegate {

    func stickerViewDidTap(_ stickerView: StickerView) {
        //self.selectedStickerView = stickerView
        print("tap view")
        self.showediteSticker(stickerView)
    }
    
    func stickerViewDidBeginMoving(_ stickerView: StickerView) {
        self.selectedStickerView = stickerView
    }
    
    /// Other delegate methods which we not used currently but choose method according to your event and requirements.
    func stickerViewDidChangeMoving(_ stickerView: StickerView) {
    
    }
    
    func stickerViewDidEndMoving(_ stickerView: StickerView) {
        print("end move")
        print(stickerView.layer)
        
    }
    
    func stickerViewDidBeginRotating(_ stickerView: StickerView) {
    }
    
    func stickerViewDidChangeRotating(_ stickerView: StickerView) {
        
        print("text move", stickerView.frame.size)
        if strCheckSticker == "text" {
            textSizeH = Int(stickerView.frame.size.height)
            if textSizeH > 50 {
                if aftertextSizeH < textSizeH {
                    fontSize = fontSize+1
                    testLable.font = UIFont.systemFont(ofSize: CGFloat(fontSize))
                } else {
                    fontSize = fontSize-1
                    testLable.font = UIFont.systemFont(ofSize: CGFloat(fontSize))
                }
                aftertextSizeH = Int(stickerView.frame.size.height)
            }
        }
    }
    
    func stickerViewDidEndRotating(_ stickerView: StickerView) {
        print(stickerView)
        print(stickerView.frame)
    }
    
    func stickerViewDidClose(_ stickerView: StickerView) {
        print("closs view", stickerView.tag)
        if strCheckSticker == "text" {
            for i in 0..<textLabelsArray.count {
                let sticker: StickerView = textLabelsArray[i]
                if sticker.tag == stickerView.tag {
                    textLabelsArray.remove(at: i)
                    return
                }
            }
        }
        
        if strCheckSticker == "emoji" {
            for i in 0..<stickerImagesArray.count {
                let sticker: StickerView = stickerImagesArray[i]
                if sticker.tag == stickerView.tag {
                    stickerImagesArray.remove(at: i)
                    return
                }
            }
        }
    }
}
