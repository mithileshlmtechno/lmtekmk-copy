//
//  VideoTrimmerVC.swift
//  LmTek
//
//  Created by PTBLR-1128 on 30/03/21.
//  Copyright © 2021 LmTek. All rights reserved.
//

import UIKit
import AVFoundation
import ICGVideoTrimmer

protocol VideoTrimmerVCDelegate {
    func trimmedVideo(url: URL, startTime: CGFloat, stopTime: CGFloat)
}

protocol ThumbnailImage {
    func selectThumbNailImage(_ startTime: CGFloat)
}

class VideoTrimmerVC: UIViewController {
    var delegate1 : ThumbnailImage?
    var checkThumbImage: Bool = false
    
    //MARK:- Outlets...
    @IBOutlet weak var view_player: UIView!
    @IBOutlet weak var trimmerView: ICGVideoTrimmerView!//TrimmerView!
    @IBOutlet weak var btn_back : UIButton!
    @IBOutlet weak var img_play : UIImageView!
    var video_url : URL?
        
    //MARK:- Variables...
    var videoAsset: AVAsset?
    var player: AVPlayer?
    var isInitial = true
    var playbackTimeCheckerTimer: Timer?
    
    var videoPlaybackPosition: CGFloat = 0
    var stopTime: CGFloat = 0
    var startTime: CGFloat = 0
    
    var restartOnPlay = true
    
    var delegate : VideoTrimmerVCDelegate?
    
    var tempVideoPath = String()

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*trimmerView.handleColor = UIColor.white
        trimmerView.mainColor = UIColor.darkGray*/
        trimmerView.delegate = self
        
        self.btn_back.setImage(#imageLiteral(resourceName: "ic_backArrow").withRenderingMode(.alwaysTemplate), for: .normal)
        self.btn_back.tintColor = UIColor.white
        
        self.tempVideoPath = NSTemporaryDirectory().appending("tmpTrim.mp4")//[NSTemporaryDirectory() stringByAppendingPathComponent:@"tmpMov.mov"];
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if self.isInitial {
            self.isInitial = false
            
            if let asset = self.videoAsset {
                //trimmerView.asset = asset
                let playerItem = AVPlayerItem(asset: asset)
                self.player = AVPlayer(playerItem: playerItem)
                /*NotificationCenter.default.addObserver(self, selector: #selector(self.itemDidFinishPlaying(_:)),
                name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)*/
                let playerLayer = AVPlayerLayer(player: player)
                playerLayer.frame = self.view_player.bounds
                playerLayer.videoGravity = .resizeAspectFill
                self.view_player.layer.insertSublayer(playerLayer, at: 0)
                self.player?.play()
                self.player?.seek(to: CMTime(seconds: Double(self.startTime), preferredTimescale: asset.duration.timescale))
                
                /* set properties for trimmer view
                [self.trimmerView setThemeColor:[UIColor lightGrayColor]];
                [self.trimmerView setAsset:self.asset];
                [self.trimmerView setShowsRulerView:YES];
                [self.trimmerView setRulerLabelInterval:10];
                [self.trimmerView setTrackerColor:[UIColor cyanColor]];
                [self.trimmerView setDelegate:self];
                
                // important: reset subviews
                [self.trimmerView resetSubviews];*/
                self.seekVideoToPos(self.startTime)
                self.trimmerView.themeColor = UIColor.lightGray
                self.trimmerView.asset = asset
                self.trimmerView.showsRulerView = false
                self.trimmerView.trackerColor = UIColor.cyan
                self.trimmerView.resetSubviews()
                //self.trimmerView(self.trimmerView, didChangeLeftPosition: self.startTime, rightPosition: self.stopTime)
                //self.trimmerView.resetSubviews()
                //self.trimmerView.delegate = self
                //self.trimmerView.seek(toTime: self.startTime)
            }
        }
    }
    
    //MARK:- IBActions...
    @IBAction func crossClicked() {
        self.player?.pause()
        self.player = nil
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func playClicked() {

        guard let player = player else { return }
        
        if player.isPlaying {
            self.player?.pause()
            self.img_play.isHidden = false
            self.stopPlaybackTimeChecker()
        }else {
            if self.restartOnPlay {
                self.seekVideoToPos(self.startTime)
                self.trimmerView.seek(toTime: self.startTime)
                self.restartOnPlay = false
            }
            self.player?.play()
            self.img_play.isHidden = true
            self.startPlaybackTimeChecker()
        }
    }
        
    @IBAction func trimClicked() {
        guard let asset = self.videoAsset else { return }
        self.deleteTempFile()
        let compatiblePresets = AVAssetExportSession.exportPresets(compatibleWith: asset)
        if compatiblePresets.contains(AVAssetExportPresetMediumQuality) {
            let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetPassthrough)
            
            let url = URL(fileURLWithPath: self.tempVideoPath)
            
            exportSession?.outputURL = url
            exportSession?.outputFileType = .mp4
            
            let start = CMTime(seconds: Double(self.startTime), preferredTimescale: asset.duration.timescale)
            let duration = CMTime(seconds: Double(self.stopTime), preferredTimescale: asset.duration.timescale)
            
            let range = CMTimeRange(start: start, end: duration)//(start: start, duration: duration)
            print("range:",range)
            exportSession?.timeRange = range
             
            exportSession?.exportAsynchronously {
                switch exportSession?.status {
                case .failed:
                    print("export failed",exportSession?.error?.localizedDescription)
                case .cancelled:
                    print("Export cancelled")
                default:
                    DispatchQueue.main.async {
                        self.delegate?.trimmedVideo(url: url, startTime: self.startTime, stopTime: self.stopTime)
                        self.player?.pause()
                        if self.checkThumbImage == true {
                            self.delegate1?.selectThumbNailImage(self.startTime)
                        }
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }

    }

    //MARK:- Functions...
    @objc func itemDidFinishPlaying(_ notification: Notification) {
        /*if let startTime = trimmerView.startTime {
            player?.seek(to: startTime)
        }*/
    }
    
    func startPlaybackTimeChecker() {

        self.stopPlaybackTimeChecker()
        self.playbackTimeCheckerTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self,
                                                        selector:
            #selector(self.onPlaybackTimeChecker), userInfo: nil, repeats: true)
    }

    func stopPlaybackTimeChecker() {

        playbackTimeCheckerTimer?.invalidate()
        playbackTimeCheckerTimer = nil
    }
    
    @objc func onPlaybackTimeChecker() {

        /*guard let startTime = trimmerView.startTime, let endTime = trimmerView.endTime, let player = player else {
            return
        }

        let playBackTime = player.currentTime()
        trimmerView.seek(to: playBackTime)

        if playBackTime >= endTime {
            player.seek(to: startTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
            trimmerView.seek(to: startTime)
        }*/
        
        guard let player = self.player else { return }
        let currentTime = player.currentTime()
        var seconds : Float64 = CMTimeGetSeconds(currentTime)
        if seconds < 0 {
            seconds = 0
        }
        
        self.videoPlaybackPosition = CGFloat(seconds)
        self.trimmerView.seek(toTime: CGFloat(seconds))
        
        if self.videoPlaybackPosition >= self.stopTime {
            self.videoPlaybackPosition = self.startTime;
            self.seekVideoToPos(self.startTime)
            self.trimmerView.seek(toTime: self.startTime)
        }
    }
    
    func seekVideoToPos(_ pos: CGFloat) {
        guard let player = self.player else { return }
        self.videoPlaybackPosition = pos;
        let time = CMTime(seconds: Double(self.videoPlaybackPosition), preferredTimescale: player.currentTime().timescale)
        self.player?.seek(to: time, toleranceBefore: .zero, toleranceAfter: .zero)
    }

    func deleteTempFile() {
        let url = URL(fileURLWithPath: self.tempVideoPath)
        let fileManager = FileManager.default
        let isExist = fileManager.fileExists(atPath: url.path)
        if isExist {
            try? fileManager.removeItem(at: url)
        }
    }

}

/*extension VideoTrimmerVC: TrimmerViewDelegate {
    //MARK:- TrimmerView Delegates...
    func didChangePositionBar(_ playerTime: CMTime) {
        print("didChangePositionBar playerTime:",playerTime)
        player?.seek(to: playerTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
        player?.play()
        startPlaybackTimeChecker()
    }
    
    func positionBarStoppedMoving(_ playerTime: CMTime) {
        print("positionBarStoppedMoving playerTime:",playerTime)
        stopPlaybackTimeChecker()
        player?.pause()
        player?.seek(to: playerTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
        let duration = (trimmerView.endTime! - trimmerView.startTime!).seconds
        print(duration)
    }
    
}*/

extension VideoTrimmerVC: ICGVideoTrimmerDelegate {
    //MARK:- ICGVideoTrimmer Delegates...
    func trimmerView(_ trimmerView: ICGVideoTrimmerView!, didChangeLeftPosition startTime: CGFloat, rightPosition endTime: CGFloat) {
        print("didChangeLeftPosition startTime:",startTime)
        print("rightPosition endTime:",endTime)
        self.restartOnPlay = true
        self.player?.pause()
        self.stopPlaybackTimeChecker()
        //self.trimmerView.hideTracker(true)

        if (startTime != self.startTime) {
            //then it moved the left position, we should rearrange the bar
            self.seekVideoToPos(startTime)
        }
        else{ // right has changed
            self.seekVideoToPos(endTime)
        }
        self.startTime = startTime;
        self.stopTime = endTime;
    }
}

extension AVPlayer {

    var isPlaying: Bool {
        return self.rate != 0 && self.error == nil
    }
}
