//
//  InviteFriendsVC.swift
//  LmTek
//
//  Created by PTBLR-1128 on 15/12/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

protocol InviteFriendsVCDelegate {
    func didSelectFriend(user: FollowUserItem)
}

class InviteFriendsVC: UIViewController {
    //MARK:- IBOutlets...
    @IBOutlet weak var tbl_friends : UITableView!
    @IBOutlet weak var txt_search : UITextField!

    //MARK:- Variables...
    var followingUsersArray = [FollowUserItem]()
    var filteredUsersArray = [FollowUserItem]()
    var delegate : InviteFriendsVCDelegate? = nil
    
    //MARK:- View Life Cycle...
    override func viewDidLoad() {
        super.viewDidLoad()

        self.cellRegistration()
        self.getFollowingsAPI()
    }
    

    //MARK:- IBActions...
    @IBAction func backClicked() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func textDidChange(_ sender: UITextField) {
        let searchText = sender.text?.lowercased() ?? ""
        self.filteredUsersArray = self.followingUsersArray.filter({ (userModel) -> Bool in
            let first_name = userModel.first_name ?? ""
            let last_name = userModel.last_name ?? ""
            let username = userModel.username ?? ""
            return first_name.lowercased().contains(searchText) || last_name.lowercased().contains(searchText) || username.lowercased().contains(searchText)
        })
        self.tbl_friends.reloadData()
    }
    
    //MARK:- Functions...
    func cellRegistration() {
        self.tbl_friends.register(UINib(nibName: "FollowerTVCell", bundle: nil), forCellReuseIdentifier: "FollowerTVCell")
        self.tbl_friends.rowHeight = UITableView.automaticDimension
        self.tbl_friends.estimatedRowHeight = 40
    }

}

extension InviteFriendsVC: UITableViewDelegate, UITableViewDataSource {
    //MARK: TableView Delegates...
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredUsersArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FollowerTVCell") as! FollowerTVCell
        
        let followUser = self.filteredUsersArray[indexPath.row]
        cell.lbl_status.text = followUser.username ?? ""
        cell.lbl_accountName.text = "\(followUser.first_name ?? "") \(followUser.last_name ?? "")"
        
        cell.img_account.sd_setImage(with: URL(string: followUser.profile_picture ?? ""), placeholderImage: #imageLiteral(resourceName: "profile_unselected"))
        
        cell.lbl_follow.isHidden = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.didSelectFriend(user: self.followingUsersArray[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }
}

extension InviteFriendsVC {
    //MARK: API's...
    func getFollowingList() {
        TransportManager.sharedInstance.getFollowingList { (data, err) in
            if let error = err {
                ToastMessage.showToast(in: self, message: error.localizedDescription, bgColor: .white)
            }
            else {
                if let json = data as? JSON, let resultObj = json.dictionaryObject {
                    print("getFollowingList resultObj:",resultObj)
                    self.followingUsersArray = FollowUserModel().decodingFollowUserItems(array: resultObj["data"] as? [[String : Any]] ?? [])
                    self.filteredUsersArray = self.followingUsersArray
                    self.tbl_friends.reloadData()
                }
            }
        }
    }
    func getFollowingsAPI(){
        let url = URL(string: Constants.BASE_URL+Constants.GET_FOLLOWING_LIST)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        DialougeUtils.addActivityView(view: self.view)
        self.getFollowingsAPICall(url, header: headers)
    }
    
    func getFollowingsAPICall(_ url: URL, header: HTTPHeaders){
        LoginSignUpAPI.getAllUseDataAPI(vc: self, url: url, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            self.followingUsersArray = FollowUserModel().decodingFollowUserItems(array: result!["user_data"] as? [[String : Any]] ?? [])
                            self.filteredUsersArray = self.followingUsersArray
                            self.tbl_friends.reloadData()
                        }
                        }else{
                        }
                } else {
                }
            }
        }
    }

}
