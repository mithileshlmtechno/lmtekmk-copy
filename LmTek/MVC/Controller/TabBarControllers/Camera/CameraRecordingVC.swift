//
//  CameraRecordingVC.swift
//  LmTek
//
//  Created by PTBLR-1128 on 18/12/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import AVFoundation
import GLKit
import Photos

import AVKit
import VideoToolbox
import MobileCoreServices


class EmojiTextField: UITextField {

       // required for iOS 13
       override var textInputContextIdentifier: String? { "" }

        override var textInputMode: UITextInputMode? {
            for mode in UITextInputMode.activeInputModes {
                if mode.primaryLanguage == "emoji" {
                    return mode
                }
            }
            return nil
        }

    override init(frame: CGRect) {
            super.init(frame: frame)

            commonInit()
        }

        required init?(coder: NSCoder) {
            super.init(coder: coder)

             commonInit()
        }

        func commonInit() {
            NotificationCenter.default.addObserver(self, selector: #selector(inputModeDidChange), name: UITextInputMode.currentInputModeDidChangeNotification, object: nil)
        }

        @objc func inputModeDidChange(_ notification: Notification) {
            guard isFirstResponder else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                self?.reloadInputViews()
            }
        }
        
    }
class CameraRecordingVC: BaseViewController, UITextFieldDelegate {
    //MARK: IBOutlets
    @IBOutlet weak var recordButton: UIButton?
    @IBOutlet weak var cameraSwitchButton: UIButton?
    @IBOutlet weak var torchButton: UIButton?
    @IBOutlet weak var view_camera: UIView!
    
    //@IBOutlet weak var view_camera : UIView!
    @IBOutlet weak var collection_options : UICollectionView!
    @IBOutlet weak var img_audio : UIImageView!
    @IBOutlet weak var view_upload : UIView!
    @IBOutlet weak var collection_cameraTypes : UICollectionView!
    @IBOutlet weak var lbl_timer : UILabel!
    @IBOutlet weak var lbl_audio : UILabel!
    @IBOutlet weak var progress_audio : UIProgressView!
    @IBOutlet weak var progress_record : UIProgressView!
    @IBOutlet weak var view_effects : UIView!
    //@IBOutlet weak var view_uploadBackground : UIView!
    @IBOutlet weak var lbl_shutterTime : UILabel!
    //@IBOutlet weak var img_camera : UIImageView!
    @IBOutlet weak var view_saveVideo : UIView!
    @IBOutlet weak var btn_saveVideo : UIButton!
    @IBOutlet weak var btn_deleteClip : UIButton!
    @IBOutlet weak var txf_emoji: UITextField!
    @IBOutlet weak var img_emoji:UIImageView!
    
    @IBOutlet var viewYellowCircle: CircularProgressView!
    var Emojiwidth: CGFloat = 10.0
    var EmojiHeight: CGFloat = 10.0

    //MARK:- Properties...
    // MARK: Video Preview Objects
    private var videoPreviewView: GLKView?
    private var ciContext: CIContext?
    private var glContext: EAGLContext?
    private var videoPreviewViewBounds: CGRect?
    
    // MARK: Camera Input and Outputs Objects
    private var frontCameraDeviceInput: AVCaptureDeviceInput?
    private var backCameraDeviceInput: AVCaptureDeviceInput?
    private var audioDeviceInput: AVCaptureDeviceInput?
    private var videoDataOutput: AVCaptureVideoDataOutput?
    
    // MARK: Session Objects
    private var captureSession: AVCaptureSession?
    private var videoWriter: VideoWriter?
    
    //MARK: helper recording
    private var cvPixelBuffer: CVPixelBuffer?
    private var isCapturing = false
    private var isBackCameraActive = false//true
    private var orientation = UIInterfaceOrientation.portrait
    private var deviceZoomFactor: CGFloat = 1.0
    
    //MARK: helper zoom
    private var previousLocation = CGPoint.zero
    
    enum FilterType {
        case NONE
        case INVERT
        case VIGNETTE
        case PHOTOINSTANT
        case CRYSTALIZE
        case COMIC
        case BLOOM
        case EDGES
        case EDGEWORK
        case GLOOM
        case HEXAGONAL
        case HIGHLIGHT_SHADOW
        case PIXELLATE
        case POINTILLIZE
    }
    
    var selectedFilter : (title: String, type: FilterItem) = (title: "None", type: .NONE) //: FilterType = .NONE
    var filter: CIFilter?
    
    var audioAsset : AVAsset?
    var audioPlayer : AVPlayer?
    
    var cameraOptionsArray = ["Flip", "Speed", "Beauty", "Filters", "Timer"]//, "Flash"]
    var cameraOptionsImagesArray = [#imageLiteral(resourceName: "ic_flipWhite"), #imageLiteral(resourceName: "ic_speedOn"), #imageLiteral(resourceName: "ic_beautyWhite"), #imageLiteral(resourceName: "ic_filterWhite"), #imageLiteral(resourceName: "ic_timerWhite")]
    var cameraTypesArray = ["Live", "60s", "15s", "Templates"]
    var selectedCameraType = 2
    
    var isFlashOn = false
    var recordEndCorrectly = true
    
    var recordTimer : Timer?
    var recordingTime = 0.0
    var captureTime = 15.0
    var audioDuration : Double?
    var displayLink : CADisplayLink?
    
    var currentSpeed : SpeedItem = .NORMAL
    
    var shutterTimer : Timer?
    var shutterDuration = 0
    var timerDuration : Double?
    
    var videosListArray = [RecordedVideoModel]()
    var videoDistributionViewArray = [UIView]()
    
    var isBeautyOn = false
    
    //Latest
    //private let sdkManager = BanubaSdkManager()
    private var effects: [String] = []
    @IBOutlet weak var effectsList: UICollectionView!
    var textFieldOrigin : CGPoint = CGPoint(x: 0, y: 0)
    var isEffectsClicked = false
    
    // MARK: - Properties -
    
    var circularViewDuration: TimeInterval = 35
    
    // MARK: Object Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        txf_emoji.tintColor = UIColor.clear
        txf_emoji.isUserInteractionEnabled = true
        self.createPanGestureRecognizer(targetView: txf_emoji)

        self.cellRegistration()
        viewYellowCircle.progressColor = .blue
        viewYellowCircle.trackColor = colorYellow

        let doubleTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleDoubleTap))
        doubleTap.numberOfTapsRequired = 2
        view_camera.addGestureRecognizer(doubleTap)

    }
    @objc func handleDoubleTap() {
     print("Double Tap!")
        self.rotateCamera()
    }

    func setUpCircularProgressBarView() {
        viewYellowCircle.setProgress(duration: 10, to: 1)
    }

//MARK: Circle Progress view

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
       
        if isEffectsClicked{
            isEffectsClicked = false
            return true
        }
        return false
    }
    
    @objc func emojiImageDrag(pan: UIPanGestureRecognizer) {
        
        if(txf_emoji.isEditing){return;}
        
       if pan.state == .began {
           print("panIF")
           textFieldOrigin = pan.location(in: txf_emoji)
       }else {
           print("panELSE")
           let location = pan.location(in: view) // get pan location
           txf_emoji.frame.origin = CGPoint(x: location.x - textFieldOrigin.x, y: location.y - textFieldOrigin.y)
       }
        
       }
    
    //LATEST
   /* override func viewWillAppear(_ animated: Bool){
        //sdkVersion.text = UIApplication.banubaVersion
        sdkManager.input.startCamera()
        sdkManager.startEffectPlayer()
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        sdkManager.input.stopCamera()
        sdkManager.stopEffectPlayer()
    }*/
    
    
    //MARK: Gesture Method
    func createPanGestureRecognizer(targetView: UITextField) {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.handlePanGesture(panGesture:)))
        targetView.addGestureRecognizer(panGesture)
        targetView.isUserInteractionEnabled = true
    }

   @objc func handlePanGesture(panGesture: UIPanGestureRecognizer) {

       let translation = panGesture.translation(in: view)
       panGesture.setTranslation(.zero, in: view)
       let label = panGesture.view as! UITextField
       label.center = CGPoint(x: label.center.x+translation.x, y: label.center.y+translation.y)
       label.isMultipleTouchEnabled = true
       label.isUserInteractionEnabled = true

       let direction = panGesture.direction(in: panGesture.view!)
       
       if direction.contains(.Left){
           print("left")
           Emojiwidth = Emojiwidth-0.5
           EmojiHeight = EmojiHeight-0.5
           txf_emoji.frame.size = CGSize(width: 40+Emojiwidth, height: 40+EmojiHeight)
           txf_emoji.font = UIFont.systemFont(ofSize: Emojiwidth+20)

       } else if direction.contains(.Right) {
           print("right")
           Emojiwidth = Emojiwidth+0.5
           EmojiHeight = EmojiHeight+0.5
           txf_emoji.frame.size = CGSize(width: 40+Emojiwidth, height: 40+EmojiHeight)
           txf_emoji.font = UIFont.systemFont(ofSize: Emojiwidth+20)
       } else if direction.contains(.Up) {
           print("Up")
           //UpRotation = UpRotation+0.001
       } else if direction.contains(.Down) {
           print("Down")
           //DownRotation = DownRotation-0.001
       }

    }
    
    override var shouldAutorotate: Bool {
        return false
        //return isCapturing == false
    }
    
    /*override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }*/
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.requestAccess(for: .video, completion: { (granted) in
            if granted {
                if self.captureSession == nil && self.videoPreviewView == nil {
                    self.setupVideoPreviewView()
                    self.setupCaptureSession()
                }
            } else {
                self.showDeviceAccessAlert(for: .video)
            }
        })
        
        requestAccess(for: .audio) { (granted) in
            if granted == false {
                self.showDeviceAccessAlert(for: .audio)
            }
        }
        if let session = self.captureSession, !session.isRunning {
            session.startRunning()
        }
        //self.captureSession?.startRunning()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        orientation = UIApplication.shared.statusBarOrientation
        resizePreviewView()
    }
    // MARK: Setup
    func setupCaptureSession() {
        // fetch camera device inputs
        guard let frontCameraDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front) else {
            print("Can't fetch front camera device")
            return
        }
        
        guard let backCameraDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back) else {
            print("Can't fetch back camera device")
            return
        }
        
        guard let audioDevice = AVCaptureDevice.default(for: .audio) else {
            print("Can't getch audio device")
            return
        }
        
        do {
            frontCameraDeviceInput = try AVCaptureDeviceInput(device: frontCameraDevice)
            backCameraDeviceInput = try AVCaptureDeviceInput(device: backCameraDevice)
        } catch {
            print("Unable to obtain video device input")
            return
        }
        
        audioDeviceInput = try? AVCaptureDeviceInput(device: audioDevice)
        
        // create the capture session
        captureSession = AVCaptureSession()
        captureSession?.sessionPreset = .high
        
        // CoreImage wants BGRA pixel format
        let outputSettings: [String: Any] = [kCVPixelBufferPixelFormatTypeKey as String: kCVPixelFormatType_32BGRA]
        
        // create and configure video data output
        videoDataOutput = AVCaptureVideoDataOutput()
        videoDataOutput?.videoSettings = outputSettings
        let videoSessionQueue = DispatchQueue(label: Constants.QueueLabels.VideoSessionQueue)
        videoDataOutput?.setSampleBufferDelegate(self, queue: videoSessionQueue)
        videoDataOutput?.alwaysDiscardsLateVideoFrames = true
        
        // create and configure audio data output
        let audioDataOutput = AVCaptureAudioDataOutput()
        let audioSessionQueue = DispatchQueue(label: Constants.QueueLabels.AudioSessionQueue)
        audioDataOutput.setSampleBufferDelegate(self, queue: audioSessionQueue)
        
        // begin configure capture session
        captureSession?.beginConfiguration()
        
        // connect the video device input and video data output
        //captureSession?.addInput(backCameraDeviceInput!)
        captureSession?.addInput(frontCameraDeviceInput!)
        captureSession?.addOutput(videoDataOutput!)
        if let audioDeviceInput = audioDeviceInput {
            captureSession?.addInput(audioDeviceInput)
            captureSession?.addOutput(audioDataOutput)
        }
        captureSession?.commitConfiguration()
        //videoPreviewView?.transform = (videoPreviewView?.transform.scaledBy(x: -1, y: 1))!
        //self.mirrorFrontCamera()
        // then start everything
        captureSession?.startRunning()
    }
    
    func setupVideoPreviewView() {
        glContext = EAGLContext(api: .openGLES2)
        if let eaglContext = glContext {
            videoPreviewView = GLKView(frame: /*self.view.bounds*/self.view_camera.bounds, context: eaglContext)
            ciContext = CIContext(eaglContext: eaglContext)
        }
        if let videoPreviewView = videoPreviewView {
            videoPreviewView.enableSetNeedsDisplay = false
            videoPreviewView.frame = self.view.bounds
            videoPreviewView.isUserInteractionEnabled = false
            
            //self.view.addSubview(videoPreviewView)
            //self.view.sendSubviewToBack(videoPreviewView)
            self.view_camera.addSubview(videoPreviewView)
            self.view_camera.sendSubviewToBack(videoPreviewView)
        }
        
        resizePreviewView()
    }
    
    func mirrorFrontCamera() {
        if let connection = self.videoDataOutput?.connection(with: .video) {
            connection.isVideoMirrored = true
        }
    }
    
    // MARK: Update
    func resizePreviewView() {
        guard let videoPreviewView =  videoPreviewView else {
            print("can't resize preview vide")
            return
        }
        DispatchQueue.main.async {
            videoPreviewView.frame = self.view.bounds
            videoPreviewView.bindDrawable()
            self.videoPreviewViewBounds = CGRect.zero
            self.videoPreviewViewBounds?.size.width = self.view.bounds.width * videoPreviewView.contentScaleFactor
            self.videoPreviewViewBounds?.size.height = self.view.bounds.height * videoPreviewView.contentScaleFactor
        }
    }
    
    //MARK:- IBActions...
    @IBAction func backClicked() {
        if self.isCapturing {
            let alertController = UIAlertController(title: "Alert", message: "Going back removes the saved video.\nDo you want to delete?", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
                self.recordEndCorrectly = false
                self.invalidateTimer()
                self.videoWriter?.fileWriter.cancelWriting()//assetWriter?.cancelWriting()
                self.audioAsset = nil
                self.lbl_audio.text = "Sounds"
                self.audioPlayer?.pause()
                self.audioPlayer = nil
                self.currentSpeed = .NORMAL
                self.tabBarController?.selectedIndex = Constants.previousTab
                //if let tabBar = self.tabBarController?.tabBar, let items = tabBar.items {
                self.navigationController?.popViewController(animated: false)
                //}
            }))
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
        else {
            self.lbl_timer.isHidden = true
            self.progress_audio.isHidden = true
            self.progress_audio.progress = 0
            self.audioAsset = nil
            self.lbl_audio.text = "Sounds"
            self.currentSpeed = .NORMAL
            self.tabBarController?.selectedIndex = Constants.previousTab
            //if let tabBar = self.tabBarController?.tabBar, let items = tabBar.items {
            self.navigationController?.popViewController(animated: false)
            //}
        }
    }
    
    @IBAction func captureVideo() {
        
        if isCapturing {
            print("capturing")
            stopRecording()
        } else {
            print("not capturing")
            startRecording()
        }
        
    }
    
    @IBAction func musicClicked() {
        if self.videosListArray.isEmpty {
            let audiosListVC = self.storyboard?.instantiateViewController(withIdentifier: "AudiosListVC") as! AudiosListVC
            audiosListVC.delegate = self
            self.navigationController?.pushViewController(audiosListVC, animated: true)
        }
    }
    
    @IBAction func galleryClicked() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = ["public.movie"]
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func pinchToZoom(_ sender: UIPinchGestureRecognizer) {
        guard let backDevice = backCameraDeviceInput?.device else {
            print("Can't find back device")
            return;
        }
        
        guard let frontDevice = frontCameraDeviceInput?.device else {
            print("Can't find front device")
            return;
        }
        
        let device = isBackCameraActive ? backDevice : frontDevice
        /*guard let device = self.currentCamera else {
            return
        }*/
        if sender.state == .changed {
            let maxZoomFactor = device.activeFormat.videoMaxZoomFactor
            let pinchVelocityDividerFactor : CGFloat = 2.0
            
            do {
                try device.lockForConfiguration()
                defer {
                    device.unlockForConfiguration()
                }
                
                let desiredZoomFactor = device.videoZoomFactor + atan2(sender.velocity, pinchVelocityDividerFactor)
                device.videoZoomFactor = max(1.0, min(desiredZoomFactor, maxZoomFactor))
                
            }
            catch {
                print("device lockconfiguration exception:",error.localizedDescription)
            }
        }
    }
    
    @IBAction func singleTapGesture(tap: UITapGestureRecognizer) {
        /*guard tapToFocus == true else {
            // Ignore taps
            return
        }*/

        let screenSize = self.view_camera.bounds.size
        let tapPoint = tap.location(in: self.view_camera)
        let x = tapPoint.y / screenSize.height
        let y = 1.0 - tapPoint.x / screenSize.width
        let focusPoint = CGPoint(x: x, y: y)

        //if let device = currentCamera {
        guard let backDevice = backCameraDeviceInput?.device else {
            print("Can't find back device")
            return;
        }
        
        guard let frontDevice = frontCameraDeviceInput?.device else {
            print("Can't find front device")
            return;
        }
        
        let device = isBackCameraActive ? backDevice : frontDevice
        do {
            try device.lockForConfiguration()
            
            if device.isFocusPointOfInterestSupported == true {
                device.focusPointOfInterest = focusPoint
                device.focusMode = .autoFocus
            }
            device.exposurePointOfInterest = focusPoint
            device.exposureMode = AVCaptureDevice.ExposureMode.continuousAutoExposure
            device.unlockForConfiguration()
            //Call delegate function and pass in the location of the touch
            
            DispatchQueue.main.async {
                self.focusAnimationAt(tapPoint)
            }
        }
        catch {
            // just ignore
        }
        //}
    }
    
    @IBAction func mergeVideos() {
        /*if self.videosListArray.count == 1 {
            self.captureSession?.stopRunning()
            //try? FileManager.default.removeItem(at: url)
            let videoItem = self.videosListArray[0]
            if videoItem.videoSpeed != .NORMAL || self.audioAsset != nil {
                self.mergeAudioAndVideo(videoURL: self.videosListArray[0].video_url)
            }
            /*if self.currentSpeed != .NORMAL || self.audioAsset != nil {
                self.mergeAudioAndVideo(videoURL: self.videosListArray[0].video_url)
            }*/
            else {
                let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                videoPlayerVC.videoURL = videoItem.video_url//self.videosListArray[0].video_url
                
                self.videoWriter = nil
                self.cvPixelBuffer = nil
                
                if self.isFlashOn {
                    self.collectionView(self.collection_options, didSelectItemAt: IndexPath(item: 5, section: 0))
                }
                
                
                self.navigationController?.pushViewController(videoPlayerVC, animated: true)
            }
        }
        else {
            self.mergingVideos()
            return
            /*guard let videoDataOutput = videoDataOutput else {
                print("videoDataOutput is nil, can't create VideoWriter")
                return
            }
            
            guard let videoWidth = videoDataOutput.videoSettings["Width"] as? Int else  {
                print("Can't recognize video resolution, can't create VideoWriter")
                return
            }
            
            guard let videoHeight = videoDataOutput.videoSettings["Height"] as? Int else  {
                print("Can't recognize video resolution, can't create VideoWriter")
                return
            }
            
            let savePathURL = URL(fileURLWithPath: NSHomeDirectory() + "/Documents/mergedVideo.mp4")
            do {
                try FileManager.default.removeItem(at: savePathURL)
            }
            catch {
                print("file remove error:",error.localizedDescription)
            }
            
            let mainComposition = AVMutableVideoComposition()
            let mixComposition = AVMutableComposition()
            let mainInstruction = AVMutableVideoCompositionInstruction()
            
            var finalDuration : CMTime = .zero
            var i = 0
            for videoItem in self.videosListArray {
                print("i:",i)
                let videoAsset = AVAsset(url: videoItem.video_url)
                let videoTrack = videoAsset.tracks(withMediaType: .video)[0]
                let mutableCompositionVideoTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
                do {
                    try? mutableCompositionVideoTrack?.insertTimeRange(CMTimeRange(start: .zero, end: videoAsset.duration), of: videoTrack, at: finalDuration)
                    
                    
                    /*if !videoItem.isBackCameraActive {
                        let transform: CGAffineTransform = CGAffineTransform(scaleX: -1.0, y: 1.0)
                        mutableCompositionVideoTrack?.preferredTransform = transform
                    }*/
                    let cameraType = videoItem.isBackCameraActive ? "back" : "front"
                    let videoLayerInstruction = /*self.videoCompositionInstruction(mutableCompositionVideoTrack!, asset: videoAsset, cameraType: cameraType) */AVMutableVideoCompositionLayerInstruction(assetTrack: mutableCompositionVideoTrack!)
                    //videoLayerInstruction.setTransform(CGAffineTransform(scaleX: -1.0, y: 1.0).concatenating(CGAffineTransform(translationX: 0, y: 0)), at: finalDuration)
                    var newScale : CGAffineTransform
                    var newMove : CGAffineTransform
                    let naturalsize = mutableCompositionVideoTrack!.naturalSize
                    let scaleRatio = naturalsize.width / naturalsize.height
                    print("naturalsize:",naturalsize,"videowidth:",videoWidth,"videoheight:",videoHeight)
                    //if videoItem.isBackCameraActive {
                        newScale = CGAffineTransform(scaleX: scaleRatio, y: scaleRatio)
                        newMove = CGAffineTransform(translationX: view_camera.frame.size.width, y: 0)//UIScreen.main.bounds.height / 8)
                    /*}
                    else {
                        newScale = CGAffineTransform(scaleX: 1, y: 1)
                        let y = UIScreen.main.bounds.height / 4
                        let statusBarHeight = UIApplication.shared.statusBarFrame.height
                        let transformY = y - (statusBarHeight * 1.4)
                        newMove = CGAffineTransform(translationX: 0, y: 0)
                    }*/
                    
                    
                    videoLayerInstruction.setTransform(newScale.concatenating(newMove), at: finalDuration)
                    
                    mainInstruction.layerInstructions.append(videoLayerInstruction)
                    
                    if let audioTrackFirst = videoAsset.tracks(withMediaType: .audio).first {
                        let audioTrack = mixComposition.addMutableTrack(
                            withMediaType: .audio,
                            preferredTrackID: kCMPersistentTrackID_Invalid)
                        
                            
                        try? audioTrack?.insertTimeRange(
                            CMTimeRangeMake(
                                start: .zero,
                                duration: videoAsset.duration),
                            of: audioTrackFirst,
                            at: finalDuration)
                    }
                                        
                    finalDuration = CMTimeAdd(finalDuration, videoAsset.duration)
                    
                    videoLayerInstruction.setOpacity(0.0, at: finalDuration)
                }
                catch {
                    return
                }
                print("loop end:",i)
                i += 1
            }
            mainInstruction.timeRange = CMTimeRange(start: .zero, end: finalDuration)
            mainComposition.instructions = [mainInstruction]
            mainComposition.frameDuration = CMTime(value: 1, timescale: 30)
            mainComposition.renderSize = CGSize(width: videoWidth, height: videoHeight)
            
            //let finalPath = savePathURL.absoluteString
            let assetExport = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)!
            assetExport.outputFileType = .mp4
            assetExport.videoComposition = mainComposition
            assetExport.outputURL = savePathURL
            assetExport.shouldOptimizeForNetworkUse = true
            
            assetExport.exportAsynchronously {
                print("duet merge exporter")
                switch assetExport.status {
                case .completed:
                    print("success")
                    let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                    videoPlayerVC.videoURL = assetExport.outputURL
                    if self.isFlashOn {
                        self.collectionView(self.collection_options, didSelectItemAt: IndexPath(item: 5, section: 0))
                    }
                    DispatchQueue.main.async {
                        self.navigationController?.pushViewController(videoPlayerVC, animated: true)
                    }
                case .failed:
                    print("failed",assetExport.error)
                case .cancelled:
                    print("cancelled",assetExport.error)
                default:
                    print("default switch")
                }
            }*/
        }*/
        self.mergingVideos()
    }
    
    @IBAction func deleteLastClipClicked() {
        if !self.videosListArray.isEmpty {
            let alertController = UIAlertController(title: "Delete last clip?", message: nil, preferredStyle: .alert)
            let deleteAction = UIAlertAction(title: "DELETE", style: .destructive) { (action) in
                self.videosListArray.removeLast()
                if let model = self.videosListArray.last {
                    self.progress_record.progress = model.progress
                    
                    self.recordingTime = model.recordedTime
                    if let audioProgress = model.audioProgress, let audioTime = model.audioTime {
                        self.progress_audio.progress = audioProgress
                        self.audioPlayer?.seek(to: audioTime)
                    }
                }
                else {
                    self.progress_record.progress = 0
                    self.recordingTime = 0
                    self.view_upload.isHidden = false
                    self.view_saveVideo.isHidden = true
                    self.collection_cameraTypes.isHidden = false
                    self.progress_audio.progress = 0
                    self.audioPlayer?.seek(to: .zero)
                }
                let view = self.videoDistributionViewArray.removeLast()
                view.removeFromSuperview()
            }
            alertController.addAction(deleteAction)
            alertController.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
            DispatchQueue.main.async {
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: User Interface
    @IBAction func switchCamera(_ sender: Any) {
        guard let captureSession = captureSession else {
            print("Can't switch camera, session is nil")
            return
        }
        
        guard let frontCameraDeviceInput = frontCameraDeviceInput else {
            print("Can't switch camera, frontCameraDeviceInput is nil")
            return
        }
        
        guard let backCameraDeviceInput = backCameraDeviceInput else {
            print("Can't switch camera, backCameraDeviceInput is nil")
            return
        }
        
        captureSession.beginConfiguration()
        //Change camera device inputs from back to front or opposite
        if captureSession.inputs.contains(frontCameraDeviceInput) == true {
            captureSession.removeInput(frontCameraDeviceInput)
            captureSession.addInput(backCameraDeviceInput)
            backCameraDeviceInput.device.videoZoomFactor = deviceZoomFactor
            isBackCameraActive = true
            self.torchButton?.isHidden = false
        } else if captureSession.inputs.contains(backCameraDeviceInput) == true {
            captureSession.removeInput(backCameraDeviceInput)
            captureSession.addInput(frontCameraDeviceInput)
            frontCameraDeviceInput.device.videoZoomFactor = deviceZoomFactor
            isBackCameraActive = false
            self.torchButton?.isHidden = true
            /*if let conn = self.videoDataOutput?.connection(with: .video) {
                conn.isVideoMirrored = true
            }*/
        }
        
        //Commit all the configuration changes at once
        captureSession.commitConfiguration();
        
        // fix mirrored preview
        videoPreviewView?.transform = (videoPreviewView?.transform.scaledBy(x: -1, y: 1))!
        cvPixelBuffer = nil
    }
    
    @IBAction func torch(_ sender: Any) {
        guard let backDevice = backCameraDeviceInput?.device else {
            print("Can't find back device")
            return;
        }
        
        if backDevice.hasTorch == false || backDevice.isTorchAvailable == false {
            print("Can't turn on/off tourch")
            return;
        }
        
        do {
            try backDevice.lockForConfiguration()
            backDevice.torchMode = backDevice.torchMode == .on ? .off : .on;
            backDevice.unlockForConfiguration()
        } catch {
            print("Something went wrong")
        }
    }
    
    @IBAction func record(_ sender: Any) {
        //var recordButtonImage: UIImage?
        if isCapturing {
            stopRecording()
            //recordButtonImage = UIImage(named: "record")
        } else {
            startRecording()
            //recordButtonImage = UIImage(named: "recording")
        }
        
        /*if let recordButtonImage = recordButtonImage {
            self.recordButton?.setImage(recordButtonImage, for: .normal)
        }*/
    }
    
    
    //Latest
    @IBAction func onEffectButton(_ sender: Any?) {
        print("entered into effect button clicked")
        /*let viewController = EffectsStoryboard.instantiateViewController(withIdentifier: "EffectsHomeVC") as! EffectsHomeVC
        
        self.navigationController?.pushViewController(viewController, animated: true)*/
        isEffectsClicked = true
        txf_emoji.becomeFirstResponder()
        
        
    }
    
    
    
    
    /*@IBAction func handlePanGestureRecognizer(_ panGestureRecognizer: UIPanGestureRecognizer) {
        /*let locationInView = panGestureRecognizer.location(in: self.view)
        
        switch panGestureRecognizer.state {
        case .began:
            previousLocation = locationInView
        case .changed:
            let zoomPointMaxAltitude = zoomBg.center.y + zoomBg.frame.size.height / 2 - zoomPoint.frame.size.height/2 + zoomPoint.imageEdgeInsets.top
            let zoomPointMinAltitude = zoomBg.center.y - zoomBg.frame.size.height / 2 + zoomPoint.frame.size.height/2 - zoomPoint.imageEdgeInsets.bottom
            let locationOffsetY = locationInView.y - previousLocation.y
            zoomPoint.center.y += locationOffsetY
            previousLocation = locationInView
            
            zoomPoint.center.y = min(zoomPointMaxAltitude, zoomPoint.center.y)
            zoomPoint.center.y = max(zoomPointMinAltitude, zoomPoint.center.y)
            
            let percent = 1 - (zoomPoint.center.y - zoomPointMinAltitude) / (zoomPointMaxAltitude - zoomPointMinAltitude)
            changeDeviceZoom(percent: percent)
        case .ended, .cancelled: break
        default: break
        }*/
    }*/
    
    @IBAction func filterClicked() {
        /*let alertController = UIAlertController(title: "Select Filter", message: nil, preferredStyle: .alert)
        let noneAction = UIAlertAction(title: "NONE", style: .cancel) { (action) in
            self.selectedFilter = .NONE
        }
        let invertAction = UIAlertAction(title: "INVERT", style: .default) { (action) in
            self.selectedFilter = .INVERT
        }
        let vignetteAction = UIAlertAction(title: "VIGNETTE", style: .default) { (action) in
            self.selectedFilter = .VIGNETTE
        }
        let photoInstantAction = UIAlertAction(title: "PHOTOINSTANT", style: .default) { (action) in
            self.selectedFilter = .PHOTOINSTANT
        }
        let crystalizeAction = UIAlertAction(title: "CRYSTALIZE", style: .default) { (action) in
            self.selectedFilter = .CRYSTALIZE
        }
        let comicAction = UIAlertAction(title: "COMIC", style: .default) { (action) in
            self.selectedFilter = .COMIC
        }
        let bloomAction = UIAlertAction(title: "BLOOM", style: .default) { (action) in
            self.selectedFilter = .BLOOM
        }
        let edgesAction = UIAlertAction(title: "EDGES", style: .default) { (action) in
            self.selectedFilter = .EDGES
        }
        let edgeworkAction = UIAlertAction(title: "EDGEWORK", style: .default) { (action) in
            self.selectedFilter = .EDGEWORK
        }
        let gloomAction = UIAlertAction(title: "GLOOM", style: .default) { (action) in
            self.selectedFilter = .GLOOM
        }
        let hexagonalAction = UIAlertAction(title: "HEXAGONAL", style: .default) { (action) in
            self.selectedFilter = .HEXAGONAL
        }
        let highlightAction = UIAlertAction(title: "HIGHLIGHT_SHADOW", style: .default) { (action) in
            self.selectedFilter = .HIGHLIGHT_SHADOW
        }
        let pixellateAction = UIAlertAction(title: "PIXELLATE", style: .default) { (action) in
            self.selectedFilter = .PIXELLATE
        }
        let pointillizeAction = UIAlertAction(title: "POINTILLIZE", style: .default) { (action) in
            self.selectedFilter = .POINTILLIZE
        }
        
        alertController.addAction(noneAction)
        alertController.addAction(invertAction)
        alertController.addAction(vignetteAction)
        alertController.addAction(photoInstantAction)
        alertController.addAction(crystalizeAction)
        alertController.addAction(comicAction)
        alertController.addAction(bloomAction)
        alertController.addAction(edgesAction)
        alertController.addAction(edgeworkAction)
        alertController.addAction(gloomAction)
        alertController.addAction(hexagonalAction)
        alertController.addAction(highlightAction)
        alertController.addAction(pixellateAction)
        alertController.addAction(pointillizeAction)
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }*/
        let filterSelectionView = Bundle.main.loadNibNamed("FilterSelectionView", owner: nil, options: nil)![0] as! FilterSelectionView
        filterSelectionView.delegate = self
        self.view.addSubview(filterSelectionView)
    }
    
    func changeDeviceZoom(percent: CGFloat) {
        guard let backDevice = backCameraDeviceInput?.device else {
            print("Can't find back device")
            return;
        }
        
        guard let frontDevice = frontCameraDeviceInput?.device else {
            print("Can't find front device")
            return;
        }
        
        let device = isBackCameraActive ? backDevice : frontDevice
        
        let maxZoomFactor = min(frontDevice.activeFormat.videoMaxZoomFactor, backDevice.activeFormat.videoMaxZoomFactor)
        let minZoomFactor: CGFloat = 1
        deviceZoomFactor = maxZoomFactor * percent
        
        deviceZoomFactor = min(maxZoomFactor, deviceZoomFactor)
        deviceZoomFactor = max(minZoomFactor, deviceZoomFactor)
        
        do {
            try device.lockForConfiguration()
            device.videoZoomFactor = deviceZoomFactor
            device.unlockForConfiguration()
        } catch {
            print("Can't change zoom factor")
        }
    }
    
    //MARK: private methods
    private func startRecording() {
        if isCapturing == true {
            print("Can't start recording, already recording!")
            return
        }
        
        guard let videoDataOutput = videoDataOutput else {
            print("videoDataOutput is nil, can't create VideoWriter")
            return
        }
        
        guard let videoWidth = videoDataOutput.videoSettings["Width"] as? Int else  {
            print("Can't recognize video resolution, can't create VideoWriter")
            return
        }
        
        guard let videoHeight = videoDataOutput.videoSettings["Height"] as? Int else  {
            print("Can't recognize video resolution, can't create VideoWriter")
            return
        }
        
        let path = videofilePath()
        let videoURL = URL(fileURLWithPath: path)
        let fileManager = FileManager()
        
        if fileManager.fileExists(atPath: path) {
            do {
                try fileManager.removeItem(atPath: path)
            } catch {
                print("Can't remove file at path \(path) so can't create VideoWriter")
                return
            }
        }
        
        var size = CGSize(width: videoWidth, height: videoHeight)
        if UIApplication.shared.statusBarOrientation.isPortrait {
            size = CGSize(width: videoHeight, height: videoWidth)
        }
        
        //self.cameraSwitchButton?.isHidden = true
        //self.torchButton?.isHidden = true
        self.collection_options.isHidden = true
        self.collection_cameraTypes.isHidden = true
        self.view_effects.isHidden = true
        self.view_upload.isHidden = true
        self.view_saveVideo.isHidden = true
        self.startTimer()
        
        self.videoWriter = VideoWriter(fileUrl: videoURL, size: size)
        
        /*if let cameraInput = self.frontCameraDeviceInput, self.captureSession?.inputs.contains(cameraInput) == true {
            var transform: CGAffineTransform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            //transform = transform.rotated(by: CGFloat(Double.pi/2))
            self.videoWriter?.videoInput.transform = transform
        }*/
        self.videoWriter?.fileWriter.startWriting()
        if let asset = self.audioAsset {
            if self.videosListArray.isEmpty {
                self.audioPlayer = nil
                let playerItem = AVPlayerItem(asset: asset)
                self.audioPlayer = AVPlayer(playerItem: playerItem)
            }
            self.audioPlayer?.play()
            self.displayLink = CADisplayLink(target: self, selector: #selector(self.updateSlider))
            self.displayLink?.preferredFramesPerSecond = 10
            self.progress_audio.progress = 0
            self.progress_audio.isHidden = false
            self.displayLink?.add(to: .current, forMode: .default)
        }
        self.isCapturing = true
    }
    
    private func stopRecording() {
        if isCapturing == false {
            print("Can't stop recording")
            return
        }
        
        guard let videoWriter = videoWriter else {
            print("Video writer is nil")
            return
        }
        
        self.isCapturing = false
        self.videoWriter?.markAsFinished()

        self.invalidateTimer()
        var newRecordTime: Float =  (Float(self.recordingTime) / Float(self.captureTime)) * 100
        newRecordTime = newRecordTime/100
        
        self.viewYellowCircle.stopProgress(duration: TimeInterval(Float(self.captureTime)), to: newRecordTime)
        self.audioPlayer?.pause()
        self.collection_options.isHidden = false
        self.view_effects.isHidden = false
        self.view_saveVideo.isHidden = false
        
        DispatchQueue.main.async {
            videoWriter.finish {
                //self.videoWriter = nil
                //self.cvPixelBuffer = nil
                //let videoFilePath = self.videofilePath()
                let url = videoWriter.fileWriter.outputURL
                var model = RecordedVideoModel(video_url: url, isBackCameraActive: self.isBackCameraActive, progress: self.progress_record.progress, recordedTime: self.recordingTime, videoSpeed: self.currentSpeed)
                if !self.progress_audio.isHidden {
                    model.audioProgress = self.progress_audio.progress
                    model.audioTime = self.audioPlayer?.currentTime()
                }
                self.videosListArray.append(model)
                let view = UIView()
                view.backgroundColor = UIColor.white
                //view.center.y = self.progress_record.center.y
                view.center.x = (CGFloat(self.progress_record.progress) * self.progress_record.frame.size.width) + self.progress_record.frame.origin.x
                view.frame.size = CGSize(width: 1, height: 5)
                view.frame.origin.y = self.progress_record.frame.origin.y
                self.view_camera.addSubview(view)
                self.videoDistributionViewArray.append(view)
                
                if let duration = self.timerDuration, self.recordingTime >= duration {
                    self.mergeVideos()
                    return
                }
                else if self.recordingTime >= self.captureTime {
                    self.mergeVideos()
                    return
                }
                
                /*if self.audioAsset == nil && self.currentSpeed == .NORMAL {
                 DispatchQueue.main.async {
                 self.captureSession?.stopRunning()
                 //try? FileManager.default.removeItem(at: url)
                 let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                 videoPlayerVC.videoURL = url
                 
                 self.videoWriter = nil
                 self.cvPixelBuffer = nil
                 
                 self.navigationController?.pushViewController(videoPlayerVC, animated: true)
                 }
                 }
                 /*else if let asset = self.audioAsset {
                 self.mergeAudioAndVideo(audioAsset: asset, videoURL: url)
                 }*/
                 else {
                 //self.speedVideo(url: url)
                 self.mergeAudioAndVideo(videoURL: url)
                 }*/
                /*self.requestPhotoLibraryAccess(completion: { (granted) in
                 PHPhotoLibrary.shared().performChanges({
                 PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: videoFilePath))
                 }) { saved, error in
                 if saved == true {
                 print("Video saved to photo library")
                 } else {
                 print("Video did not save to photo library, reason- \(String(describing: error?.localizedDescription))")
                 }
                 }
                 })*/
            }
        }
    }
    
    // request device access
    private func requestAccess(for mediaType: AVMediaType, completion: @escaping (Bool) -> Void ) {
        let authorizationStatus = AVCaptureDevice.authorizationStatus(for: mediaType)
        switch authorizationStatus {
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: mediaType, completionHandler: { (granted) in
                DispatchQueue.main.async {
                    completion(granted)
                }
            })
        case .authorized:
            completion(true)
        case .denied, .restricted:
            completion(false)
        }
    }
    
    private func requestPhotoLibraryAccess(completion: @escaping (Bool) -> Void ) {
        let authorizationStatus = PHPhotoLibrary.authorizationStatus()
        
        switch authorizationStatus {
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ (status) in
                let granted = (status == .authorized)
                DispatchQueue.main.async {
                    completion(granted)
                }
            })
        case .authorized:
            completion(true)
        case .denied, .restricted:
            completion(false)
        case .limited:
            completion(false)
        }
    }
    
    // show device access denied alert, and redirect to settings if there is no access
    private func showDeviceAccessAlert(for mediaType: AVMediaType) {
        var alertTitle: String?
        var alertMessage: String?
        
        if mediaType == .video {
            alertTitle = "Camera Access"//Constants.AlertTitles.CameraAccess
            alertMessage = "This app does not have access to your Camera. To enable press OK and switch Camera on."//Constants.AlertMessages.CameraAccess
        } else if mediaType == .audio {
            alertTitle = "Microphone Access"//Constants.AlertTitles.MicrophoneAccess
            alertMessage = "This app does not have access to your Microphone. To enable press OK and switch Microphone on."//Constants.AlertMessages.MicrophoneAccess
        } else {
            print("Unknown Media Type to show alert")
            return
        }
        
        let alertController = UIAlertController(title: alertTitle!, message: alertMessage!, preferredStyle: .alert)
        let okAction = UIAlertAction(title: /*Constants.AlertActionTitles.Ok*/"OK", style: .default) { (action) in
            let settingsURL = URL(string: UIApplication.openSettingsURLString)
            if let url = settingsURL {
                UIApplication.shared.open(url, options: [:], completionHandler: nil);
            } else {
                print("Can't open application settings")
            }
        }
        let cancelAction = UIAlertAction(title: /*Constants.AlertActionTitles.Cancel*/"Cancel", style: .cancel, handler: nil);
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    // get video file path
    private func videofilePath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths.first!
        let videoPath = "\(documentsDirectory)/video\(self.videosListArray.count).mp4"
        
        return videoPath
    }
    
    //MARK:- Functions...
    
    func cellRegistration() {
        self.collection_options.register(UINib(nibName: "CameraOptionsCVCell", bundle: nil), forCellWithReuseIdentifier: "CameraOptionsCVCell")
        self.collection_cameraTypes.register(UINib(nibName: "CameraTypeCVCell", bundle: nil), forCellWithReuseIdentifier: "CameraTypeCVCell")
    }
        
    func mergeVideoAudio(videoURL: URL, cameraType: String) {
       /* print("mergeVideoAudio")
        let videoAsset = AVAsset(url: videoURL)
        let mixComposition = AVMutableComposition()

        // 2 - Create two video tracks
        guard
          let videoTrack = mixComposition.addMutableTrack(
            withMediaType: .video,
            preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
          else { return }
        print("guard videoTrack")

        do {
            print("videoAsset.tracks(withMediaType: .video):",videoAsset.tracks(withMediaType: .video))
          try videoTrack.insertTimeRange(
            CMTimeRangeMake(start: .zero, duration: videoAsset.duration),
            of: videoAsset.tracks(withMediaType: .video)[0],
            at: .zero)
        } catch {
          print("Failed to load first track")
          return
        }

        if cameraType == "back" {
            
        }
        else {
            
        }

        // 3 - Composition Instructions
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRangeMake(
          start: .zero,
          duration: videoAsset.duration)
        print("mainInstruction")

        // 4 - Set up the instructions — one for each asset
        let videoInstruction = self.videoCompositionInstruction(
          videoTrack,
          asset: videoAsset, cameraType: cameraType)
        videoInstruction.setOpacity(0.0, at: videoAsset.duration)
        print("videoInstruction")

        // 5 - Add all instructions together and create a mutable video composition
        mainInstruction.layerInstructions = [videoInstruction]
        let mainComposition = AVMutableVideoComposition()
        mainComposition.instructions = [mainInstruction]
        mainComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        let width = UIScreen.main.bounds.width
        let compositionWidth = (Int(width) % 2 == 0) ? width : width - 1
        mainComposition.renderSize = CGSize(
          width: /*UIScreen.main.bounds.width*/compositionWidth,
          height: UIScreen.main.bounds.height)
        print("mainComposition")
        // 6 - Audio track
        if let loadedAudioAsset = self.audioAsset {
          let audioTrack = mixComposition.addMutableTrack(
            withMediaType: .audio,
            preferredTrackID: 0)
          do {
            try audioTrack?.insertTimeRange(
              CMTimeRangeMake(
                start: CMTime.zero,
                duration: videoAsset.duration),
              of: loadedAudioAsset.tracks(withMediaType: .audio)[0],
              at: .zero)
          } catch {
            print("Failed to load Audio track")
          }
        }
        else if let audioTrackFirst = videoAsset.tracks(withMediaType: .audio).first {
            print("natural audio")
            let audioTrack = mixComposition.addMutableTrack(
                withMediaType: .audio,
                preferredTrackID: kCMPersistentTrackID_Invalid)
            
            do {
                
                try audioTrack?.insertTimeRange(
                    CMTimeRangeMake(
                        start: CMTime.zero,
                        duration: videoAsset.duration),
                    of: audioTrackFirst,
                    at: .zero)
            } catch {
                print("Failed to load Audio track")
            }
        }

        // 7 - Get path
        guard
          let documentDirectory = FileManager.default.urls(
            for: .documentDirectory,
            in: .userDomainMask).first
          else { return }
        print("guard documentDirectory")
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .short
        let date = dateFormatter.string(from: Date())
        let url = documentDirectory.appendingPathComponent("mergeVideo-\(date).mp4")

        // 8 - Create Exporter
        guard let exporter = AVAssetExportSession(
          asset: mixComposition,
          presetName: AVAssetExportPresetHighestQuality)
          else { return }
        print("guard exporter")
        exporter.outputURL = url
        exporter.outputFileType = AVFileType.mp4
        exporter.shouldOptimizeForNetworkUse = true
        exporter.videoComposition = mainComposition

        // 9 - Perform the Export
        exporter.exportAsynchronously {
            print("exporter.exportAsynchronously")
            DispatchQueue.main.async {
                self.exportDidFinish(exporter)
            }
        }*/
    }
    
    func orientationFromTransform(
      _ transform: CGAffineTransform
    ) -> (orientation: UIImage.Orientation, isPortrait: Bool) {
        var assetOrientation = UIImage.Orientation.up
        var isPortrait = false
        let tfA = transform.a
        let tfB = transform.b
        let tfC = transform.c
        let tfD = transform.d
        
        if tfA == 0 && tfB == 1.0 && tfC == -1.0 && tfD == 0 {
            print("orientationFromTransform 1")
            assetOrientation = .right
            isPortrait = true
        } else if tfA == 0 && tfB == -1.0 && tfC == 1.0 && tfD == 0 {
            print("orientationFromTransform 2")
            assetOrientation = .left
            isPortrait = true
        } else if tfA == 1.0 && tfB == 0 && tfC == 0 && tfD == 1.0 {
            print("orientationFromTransform 3")
            assetOrientation = .up
            isPortrait = true
        } else if tfA == -1.0 && tfB == 0 && tfC == 0 && tfD == -1.0 {
            print("orientationFromTransform 4")
            assetOrientation = .down
        }
        else if tfA == 0.0 && tfB == 1.0 && tfC == 1.0 && tfD == 0.0 {
            print("orientationFromTransform 5")
            assetOrientation = .rightMirrored
            isPortrait = true
        }
        else {
            print("orientationFromTransform else:",tfA, tfB, tfC, tfD)
        }
        return (assetOrientation, isPortrait)
    }
    
    func videoCompositionInstruction(
      _ track: AVCompositionTrack,
      asset: AVAsset, cameraType: String
    ) -> AVMutableVideoCompositionLayerInstruction {
        print("videoCompositionInstruction-----")
        let instruction = AVMutableVideoCompositionLayerInstruction(assetTrack: track)
        let assetTrack = asset.tracks(withMediaType: AVMediaType.video)[0]
        
        let transform = assetTrack.preferredTransform
        let assetInfo = orientationFromTransform(transform)
                
        var scaleToFitRatio = UIScreen.main.bounds.width / assetTrack.naturalSize.width
        if assetInfo.isPortrait {
            
            print("assetInfo.isPortrait")
            
            
            if cameraType == "front" {
                print("cameraType == front")
                //scaleToFitRatio = UIScreen.main.bounds.width / UIScreen.main.bounds.height
                let scaleFactor = CGAffineTransform(
                    scaleX: 0.55,
                    y: 0.7)
                /*let scaleFactor = CGAffineTransform(
                    scaleX: (UIScreen.main.bounds.width / assetTrack.naturalSize.width) * 2,
                    y: (UIScreen.main.bounds.height / assetTrack.naturalSize.height) * 0.5)*/
                
                /*let statusBarHeight = UIApplication.shared.statusBarFrame.height
                let transformY = 0 - (statusBarHeight * 1.4)*/
                let finalTransform = assetTrack.preferredTransform.concatenating(scaleFactor)//.translatedBy(x: self.view_camera.frame.size.height * 0.8, y: 0)
                instruction.setTransform(finalTransform.concatenating(CGAffineTransform(translationX: 0, y: UIScreen.main.bounds.height / 4)) , at: .zero)
            }
            else {
                scaleToFitRatio = UIScreen.main.bounds.width / assetTrack.naturalSize.height
                let scaleFactor = CGAffineTransform(
                    scaleX: 0.5,
                    y: 0.65)
                let finalTransform = assetTrack.preferredTransform.concatenating(scaleFactor)
                instruction.setTransform(finalTransform.concatenating(CGAffineTransform(translationX: 0, y: 0)) , at: .zero)
            }
                        
        } else {
            print("assetInfo.isPortrait else")
            let scaleFactor = CGAffineTransform(
                scaleX: scaleToFitRatio,
                y: scaleToFitRatio)
            var concat = assetTrack.preferredTransform.concatenating(scaleFactor)
                .concatenating(CGAffineTransform(
                    translationX: 0,
                    y: UIScreen.main.bounds.width / 2))
            if assetInfo.orientation == .down {
                print("assetInfo.orientation == .down")
                let fixUpsideDown = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                let windowBounds = UIScreen.main.bounds
                let yFix = assetTrack.naturalSize.height + windowBounds.height
                let centerFix = CGAffineTransform(
                    translationX: assetTrack.naturalSize.width,
                    y: yFix)
                concat = fixUpsideDown.concatenating(centerFix).concatenating(scaleFactor)
            }
            instruction.setTransform(concat, at: .zero)
        }
        print("----videoCompositionInstruction")
        return instruction
    }
    
    func exportDidFinish(_ session: AVAssetExportSession) {
        /*print("exportDidFinish")
        // Cleanup assets
        audioAsset = nil
        
        self.lbl_audio.text = "Sounds"
        
        guard
            session.status == AVAssetExportSession.Status.completed,
            let outputURL = session.outputURL
            else {
                print("exportDidFinish session.status:",session.status,session.status.rawValue)
                return
        }
        if self.currentSpeed != .NORMAL {
            self.speedVideo(url: outputURL)
        }
        else {
            let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
            videoPlayerVC.videoURL = outputURL
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(videoPlayerVC, animated: true)
            }
        }*/
    }
    
    func startTimer() {
        self.viewYellowCircle.setProgress(duration: TimeInterval(Float(self.captureTime)), to: 1)
        if self.videosListArray.isEmpty {
            self.recordingTime = 0
        }
        self.recordTimer?.invalidate()
        self.recordTimer = nil
        
        //self.recordingTime = 0
        self.lbl_timer.text = Int(floor(self.recordingTime)).timeFormatted()
        //self.lbl_timer.isHidden = false
        self.lbl_timer.isHidden = true /// changed this because For 15sec video, when we choose 3x then timer will display till 45 seconds. It may confuse to the user so just progress bar is enough(like in tik tok)

        switch self.currentSpeed {
        case .SLOWER:
            self.captureTime = 0.3 * self.captureTime
        case .SLOW:
            self.captureTime = 0.5 * self.captureTime
        case .NORMAL:
            self.captureTime = 1.0 * self.captureTime
        case .FAST:
            self.captureTime = 2.0 * self.captureTime
        case .FASTER:
            self.captureTime = 3.0 * self.captureTime
        default:
            self.captureTime = 1.0 * self.captureTime
        }
        self.recordTimer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(self.countTimer), userInfo: nil, repeats: true)
    }
    
    @objc func countTimer(_ timer: Timer) {
        self.recordingTime += 0.05
        self.lbl_timer.text = Int(floor(self.recordingTime)).timeFormatted()
        self.progress_record.progress = Float(self.recordingTime / self.captureTime)
        if let duration = self.timerDuration, self.recordingTime >= duration {
            //self.invalidateTimer()
            self.captureVideo()
        }
        else if self.recordingTime >= self.captureTime {
            //self.invalidateTimer()
            self.captureVideo()
        }
    }
    
    func invalidateTimer() {
        self.recordTimer?.invalidate()
        self.recordTimer = nil
        self.lbl_timer.isHidden = true
        //self.progress_audio.isHidden = true
        self.displayLink?.invalidate()
        self.displayLink = nil
        //self.progress_audio.progress = 0
        
        //self.recordingTime = 0
        self.lbl_timer.text = Int(floor(self.recordingTime)).timeFormatted()
        self.timerDuration = nil
    }
        
    @objc func updateSlider() {
        self.progress_audio.progress = Float((audioPlayer?.currentTime().seconds ?? 0) / (self.audioAsset?.duration.seconds ?? 1))
    }
    
    func mergeAudioAndVideo(/*audioAsset: AVAsset, */videoURL: URL) {
        let videoAsset = AVURLAsset(url: videoURL, options: nil)
        let mixComposition = AVMutableComposition()
        let compositionVideoTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        guard let videoAssetTrack = videoAsset.tracks(withMediaType: .video).first else { return }
        do {
            try compositionVideoTrack?.insertTimeRange(CMTimeRange(start: .zero, duration: videoAsset.duration), of: /*videoAsset.tracks(withMediaType: .video).first!*/videoAssetTrack, at: .zero)
        }
        catch {
            print("videoasset insert error:",error.localizedDescription)
        }
        
        let videoDuration = videoAsset.duration
        
        //slow down the video half speed (change multiplier to desired value"
        var finalTimeScale : Int64
        switch self.currentSpeed {
        case .SLOWER:
            finalTimeScale = videoDuration.value * 4
        case .SLOW:
            finalTimeScale = videoDuration.value * 2
        case .FAST:
            finalTimeScale = videoDuration.value / 2
        case .FASTER:
            finalTimeScale = videoDuration.value / 4
        default:
            finalTimeScale = videoDuration.value
        }
        
        compositionVideoTrack?.scaleTimeRange(CMTimeRange(start: .zero, duration: videoDuration), toDuration: CMTime(value: finalTimeScale, timescale: videoDuration.timescale))
        
        /*if let cameraInput = self.frontCameraDeviceInput, self.captureSession?.inputs.contains(cameraInput) == true {
            let transform: CGAffineTransform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            compositionVideoTrack?.preferredTransform = transform
        }*/
                                
        let documentURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let finalURL = documentURL.appendingPathComponent("mergeVideo.mp4")
        
        do {
            try FileManager.default.removeItem(atPath: finalURL.path)
        }
        catch {
            print("file doesn't exist error:",error.localizedDescription)
        }
        
        if let audioAsset = self.audioAsset {
            let audioTrack = mixComposition.addMutableTrack(
                withMediaType: .audio,
                preferredTrackID: 0)
            do {
                try audioTrack?.insertTimeRange(
                    CMTimeRangeMake(
                        start: CMTime.zero,
                        duration: videoAsset.duration),
                    of: audioAsset.tracks(withMediaType: .audio)[0],
                    at: .zero)
            } catch {
                print("Failed to load Audio track")
            }
            audioTrack?.scaleTimeRange(CMTimeRange(start: .zero, duration: videoDuration), toDuration: CMTime(value: finalTimeScale, timescale: videoDuration.timescale))
        }
        else if let audioTrackFirst = videoAsset.tracks(withMediaType: .audio).first {
            let audioTrack = mixComposition.addMutableTrack(
                withMediaType: .audio,
                preferredTrackID: kCMPersistentTrackID_Invalid)
            
            do {
                
                try audioTrack?.insertTimeRange(
                    CMTimeRangeMake(
                        start: CMTime.zero,
                        duration: videoAsset.duration),
                    of: audioTrackFirst,
                    at: .zero)
            } catch {
                print("Failed to load Audio track speed exception:",error.localizedDescription)
            }
            audioTrack?.scaleTimeRange(CMTimeRange(start: .zero, duration: videoDuration), toDuration: CMTime(value: finalTimeScale, timescale: videoDuration.timescale))
            print("audioTrack timerange:",audioTrack?.timeRange)
        }
        
        let exporter = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)
        exporter?.outputURL = finalURL
        exporter?.outputFileType = .mp4
        //exporter?.videoComposition = AVVideoComposition(propertiesOf: videoAsset)
        exporter?.exportAsynchronously(completionHandler: {
            switch exporter?.status {
            case .failed:
                print("exporter failed:",exporter?.status.rawValue)
                break
            case .cancelled:
                print("exporter cancelled:",exporter?.status.rawValue)
                break
            case .completed:
                print("exporter completed:",exporter?.status.rawValue)
                let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                videoPlayerVC.videoURL = finalURL
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(videoPlayerVC, animated: true)
                }
                break
            default:
                print("exporter default:",exporter?.status.rawValue)
            }
        })
    }
    
    func focusAnimationAt(_ point: CGPoint) {
        let focusView = UIImageView(image: #imageLiteral(resourceName: "ic_focus"))
        focusView.center = point
        focusView.alpha = 0.0
        view.addSubview(focusView)
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseInOut, animations: {
            focusView.alpha = 1.0
            focusView.transform = CGAffineTransform(scaleX: 1.25, y: 1.25)
        }) { (success) in
            UIView.animate(withDuration: 0.15, delay: 0.5, options: .curveEaseInOut, animations: {
                focusView.alpha = 0.0
                focusView.transform = CGAffineTransform(translationX: 0.6, y: 0.6)
            }) { (success) in
                focusView.removeFromSuperview()
            }
        }
    }
    
    /*func orientationFromTransform(
      _ transform: CGAffineTransform
    ) -> (orientation: UIImage.Orientation, isPortrait: Bool) {
        var assetOrientation = UIImage.Orientation.up
        var isPortrait = false
        let tfA = transform.a
        let tfB = transform.b
        let tfC = transform.c
        let tfD = transform.d
        
        if tfA == 0 && tfB == 1.0 && tfC == -1.0 && tfD == 0 {
            print("orientationFromTransform 1")
            assetOrientation = .right
            isPortrait = true
        } else if tfA == 0 && tfB == -1.0 && tfC == 1.0 && tfD == 0 {
            print("orientationFromTransform 2")
            assetOrientation = .left
            isPortrait = true
        } else if tfA == 1.0 && tfB == 0 && tfC == 0 && tfD == 1.0 {
            print("orientationFromTransform 3")
            assetOrientation = .up
        } else if tfA == -1.0 && tfB == 0 && tfC == 0 && tfD == -1.0 {
            print("orientationFromTransform 4")
            assetOrientation = .down
        }
        else if tfA == -1.0 && tfB == 0 && tfC == 0 && tfD == 1.0 {
            print("orientationFromTransform 5")
            assetOrientation = .upMirrored
            //isPortrait = true
        }
        return (assetOrientation, isPortrait)
    }*/

    
    func videoCompositionInstruction(
      _ track: AVCompositionTrack,
      asset: AVAsset
    ) -> AVMutableVideoCompositionLayerInstruction {
      // 1
      let instruction = AVMutableVideoCompositionLayerInstruction(assetTrack: track)

      // 2
      let assetTrack = asset.tracks(withMediaType: AVMediaType.video)[0]

      // 3
      let transform = assetTrack.preferredTransform
      let assetInfo = orientationFromTransform(transform)

      var scaleToFitRatio = UIScreen.main.bounds.width / assetTrack.naturalSize.width
      if assetInfo.isPortrait {
        // 4
        scaleToFitRatio = UIScreen.main.bounds.width / assetTrack.naturalSize.height
        let scaleFactor = CGAffineTransform(
          scaleX: scaleToFitRatio,
          y: scaleToFitRatio)
        instruction.setTransform(
          assetTrack.preferredTransform.concatenating(scaleFactor),
          at: .zero)
      } else {
        // 5
        let scaleFactor = CGAffineTransform(
          scaleX: scaleToFitRatio,
          y: scaleToFitRatio)
        var concat = assetTrack.preferredTransform.concatenating(scaleFactor)
          .concatenating(CGAffineTransform(
            translationX: 1.0,//UIScreen.main.bounds.width / 2,
            y: 1.0))//UIScreen.main.bounds.width / 2))
        /*if assetInfo.orientation == .down {
          let fixUpsideDown = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
          let windowBounds = UIScreen.main.bounds
          let yFix = assetTrack.naturalSize.height + windowBounds.height
          let centerFix = CGAffineTransform(
            translationX: assetTrack.naturalSize.width,
            y: yFix)
          concat = fixUpsideDown.concatenating(centerFix).concatenating(scaleFactor)
        }*/
        instruction.setTransform(concat, at: .zero)
      }

      return instruction
    }

    func mergingVideos() {
        
        let mixComposition = AVMutableComposition()
        var nextClipStartTime : CMTime = CMTime.zero
        
        
        let videoComposition = AVMutableVideoComposition()
        let videoCompositionInstructions = AVMutableVideoCompositionInstruction()
        videoComposition.frameDuration = CMTime(value: 1, timescale: 30)
        var height = Int(self.view_camera.bounds.size.height)
        var width = Int(self.view_camera.bounds.size.width)
        if height % 2 != 0 {
            height += 1
        }
        if width % 2 != 0 {
            width += 1
        }
        videoComposition.renderSize = CGSize(width: CGFloat(width), height: CGFloat(height))//self.view_camera.bounds.size
        videoComposition.renderScale = 1.0
        
        let compositionAudioTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)
        let compositionVideoTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        for videoModel in self.videosListArray {
            let videoAsset = AVURLAsset(url: videoModel.video_url)

            let timeRangeInAsset = CMTimeRangeMake(start: .zero, duration: videoAsset.duration)
            let videoTrack = videoAsset.tracks(withMediaType: .video)[0]
            
            //let compositionVideoTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
            
            var finalTimeScale : Int64
            let videoDuration = videoAsset.duration
            switch videoModel.videoSpeed {
            case .SLOWER:
                finalTimeScale = videoDuration.value * 4
            case .SLOW:
                finalTimeScale = videoDuration.value * 2
            case .FAST:
                finalTimeScale = videoDuration.value / 2
            case .FASTER:
                finalTimeScale = videoDuration.value / 4
            default:
                finalTimeScale = videoDuration.value
            }
            
            let speedDuration = CMTime(value: finalTimeScale, timescale: videoDuration.timescale)
            
            do {
                try compositionVideoTrack?.insertTimeRange(CMTimeRange(start: .zero, duration: videoAsset.duration), of: videoTrack, at: nextClipStartTime)
                
                if videoModel.videoSpeed != .NORMAL {
                    compositionVideoTrack?.scaleTimeRange(CMTimeRange(start: /*.zero*/nextClipStartTime, duration: videoDuration), toDuration: speedDuration)
                }
                
                if self.audioAsset == nil {
                    try compositionAudioTrack?.insertTimeRange(CMTimeRange(start: .zero, duration: videoAsset.duration), of: videoAsset.tracks(withMediaType: .audio)[0], at: nextClipStartTime)
                    if videoModel.videoSpeed != .NORMAL {
                        compositionAudioTrack?.scaleTimeRange(CMTimeRange(start: /*.zero*/nextClipStartTime, duration: videoDuration), toDuration: speedDuration)
                    }
                }
            }
            catch {
                print("catch insert timerange:",error.localizedDescription)
            }
            let layerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: compositionVideoTrack!)
            
            var width = Int(self.view_camera.frame.size.width)
            width = (width % 2 == 0) ? width : width + 1
            
            let scale = CGFloat(width) / videoTrack.naturalSize.width
            var transform : CGAffineTransform = videoTrack.preferredTransform
            transform = transform.scaledBy(x: scale, y: scale)
            
            layerInstruction.setTransform(transform, at: nextClipStartTime)
            videoCompositionInstructions.layerInstructions.append(layerInstruction)
            
            if videoModel.videoSpeed == .NORMAL {
                nextClipStartTime = CMTimeAdd(nextClipStartTime, timeRangeInAsset.duration)
            }
            else {
                nextClipStartTime = CMTimeAdd(nextClipStartTime, speedDuration)
            }
            layerInstruction.setOpacity(0.0, at: nextClipStartTime)
        }
        
        if let audioAsset = self.audioAsset {
            do {
                try compositionAudioTrack?.insertTimeRange(CMTimeRange(start: .zero, duration: nextClipStartTime), of: audioAsset.tracks(withMediaType: .audio)[0], at: .zero)
            }
            catch {
                print("mergingVideos unable to add audio asset:",error.localizedDescription)
            }
        }
        
        videoCompositionInstructions.timeRange = CMTimeRange(start: .zero, duration: nextClipStartTime)
        
        videoComposition.instructions = [videoCompositionInstructions]
        
        self.applyVideoEffects(to: videoComposition, size: CGSize(width: CGFloat(width), height: CGFloat(height)))
        
        let savePathURL = URL(fileURLWithPath: NSHomeDirectory() + "/Documents/mergedVideo.mp4")
        do {
            try FileManager.default.removeItem(at: savePathURL)
        }
        catch {
            print("mergingVideos file remove error:",error.localizedDescription)
        }
        
        let assetExport = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)!
        assetExport.outputFileType = .mp4
        assetExport.videoComposition = videoComposition
        assetExport.outputURL = savePathURL
        assetExport.shouldOptimizeForNetworkUse = true
        
        assetExport.exportAsynchronously {
            print("mergingVideos exporter")
            switch assetExport.status {
            case .completed:
                print("success")
                if self.isFlashOn {
                    self.collectionView(self.collection_options, didSelectItemAt: IndexPath(item: 5, section: 0))
                }
                DispatchQueue.main.async {
                    let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                    videoPlayerVC.videoURL = assetExport.outputURL
                    self.navigationController?.pushViewController(videoPlayerVC, animated: true)
                }
            case .failed:
                print("failed",assetExport.error)
            case .cancelled:
                print("cancelled",assetExport.error)
            default:
                print("default switch")
            }
        }
        
        
    }
    
    
    func applyVideoEffects(to composition: AVMutableVideoComposition, size: CGSize) {//, currentLabel: UILabel) {

        let parentLayer = CALayer()
        let videoLayer = CALayer()
        parentLayer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        videoLayer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        parentLayer.addSublayer(videoLayer)
        
        //for currentLabel in self.textLabelsArray {
        if txf_emoji.text != ""{
            let textLayer = CATextLayer()
            textLayer.backgroundColor = UIColor.clear.cgColor
            let attributedText = txf_emoji.attributedText
            textLayer.string = attributedText
            
            
            //let scaleHeight = self.view_player.frame.size.height / size.height
            let x = txf_emoji.frame.origin.x // scaleWidth
            let y = self.view_camera.frame.size.height - (txf_emoji.frame.origin.y + txf_emoji.frame.size.height)//(self.view_player.frame.size.height - (currentLabel.frame.origin.y + currentLabel.frame.size.height)) / scaleHeight
            let width = txf_emoji.frame.size.width // scaleWidth
            let height = txf_emoji.frame.size.height // scaleHeight
            textLayer.frame = CGRect(x: x, y: y, width: width, height: height)
            
            textLayer.transform = CATransform3DInvert(txf_emoji.layer.transform)
            
            textLayer.masksToBounds = true
            parentLayer.addSublayer(textLayer)
        }
        //}
        
       
        
        // 3 - apply magic
        composition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: parentLayer)
    }
    
    
    
    
    
    func videoSelectedFromLibrary(url: URL) {
        let mixComposition = AVMutableComposition()
        var nextClipStartTime : CMTime = CMTime.zero
        
        let videoComposition = AVMutableVideoComposition()
        let videoCompositionInstructions = AVMutableVideoCompositionInstruction()
        videoComposition.frameDuration = CMTime(value: 1, timescale: 30)
        var height = Int(self.view_camera.bounds.size.height)
        var width = Int(self.view_camera.bounds.size.width)
        if height % 2 != 0 {
            height += 1
        }
        if width % 2 != 0 {
            width += 1
        }
        videoComposition.renderSize = CGSize(width: CGFloat(width), height: CGFloat(height))//self.view_camera.bounds.size
        videoComposition.renderScale = 1.0
        
        let compositionAudioTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)
        let compositionVideoTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        let videoAsset = AVURLAsset(url: url)

        let timeRangeInAsset = CMTimeRangeMake(start: .zero, duration: videoAsset.duration)
        let videoTrack = videoAsset.tracks(withMediaType: .video)[0]
           
        do {
            try compositionVideoTrack?.insertTimeRange(CMTimeRange(start: .zero, duration: videoAsset.duration), of: videoTrack, at: nextClipStartTime)
            
            
            
            if let audioTrack = videoAsset.tracks(withMediaType: .audio).first {
                try compositionAudioTrack?.insertTimeRange(CMTimeRange(start: .zero, duration: videoAsset.duration), of: audioTrack, at: nextClipStartTime)
            }
        }
        catch {
            print("catch insert timerange:",error.localizedDescription)
        }
        let layerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: compositionVideoTrack!)
        let scale = CGFloat(width) / videoTrack.naturalSize.width //self.view_camera.frame.size.width / videoTrack.naturalSize.width
        var transform : CGAffineTransform = videoTrack.preferredTransform
        transform = transform.scaledBy(x: scale, y: scale)
            
        layerInstruction.setTransform(transform, at: nextClipStartTime)
        videoCompositionInstructions.layerInstructions.append(layerInstruction)
            
            
        nextClipStartTime = CMTimeAdd(nextClipStartTime, timeRangeInAsset.duration)
            
        layerInstruction.setOpacity(0.0, at: nextClipStartTime)
        
        videoCompositionInstructions.timeRange = CMTimeRange(start: .zero, duration: nextClipStartTime)
        videoComposition.instructions = [videoCompositionInstructions]
        let savePathURL = URL(fileURLWithPath: NSHomeDirectory() + "/Documents/mergedVideo.mp4")
        do {
            try FileManager.default.removeItem(at: savePathURL)
        }
        catch {
            print("mergingVideos file remove error:",error.localizedDescription)
        }
        
        let assetExport = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)!
        assetExport.outputFileType = .mp4
        assetExport.videoComposition = videoComposition
        assetExport.outputURL = savePathURL
        assetExport.shouldOptimizeForNetworkUse = true
        
        assetExport.exportAsynchronously {
            print("mergingVideos exporter")
            switch assetExport.status {
            case .completed:
                print("success")
                DispatchQueue.main.async {
                    let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                    videoPlayerVC.videoURL = assetExport.outputURL
                    self.navigationController?.pushViewController(videoPlayerVC, animated: true)
                }
                if self.isFlashOn {
                    self.collectionView(self.collection_options, didSelectItemAt: IndexPath(item: 5, section: 0))
                }
            case .failed:
                print("failed",assetExport.error)
            case .cancelled:
                print("cancelled",assetExport.error)
            default:
                print("default switch")
            }
        }
    }
    
    func rotateCamera() {
        
        let indexP: IndexPath = IndexPath(row: 0, section: 0)
        guard let captureSession = captureSession else {
            print("Can't switch camera, session is nil")
            return
        }
        
        guard let frontCameraDeviceInput = frontCameraDeviceInput else {
            print("Can't switch camera, frontCameraDeviceInput is nil")
            return
        }
        
        guard let backCameraDeviceInput = backCameraDeviceInput else {
            print("Can't switch camera, backCameraDeviceInput is nil")
            return
        }
        
        captureSession.beginConfiguration()
        //Change camera device inputs from back to front or opposite
        if captureSession.inputs.contains(frontCameraDeviceInput) == true {
            captureSession.removeInput(frontCameraDeviceInput)
            captureSession.addInput(backCameraDeviceInput)
            backCameraDeviceInput.device.videoZoomFactor = deviceZoomFactor
            isBackCameraActive = true
            self.isBeautyOn = false
            //self.torchButton?.isHidden = false
        } else if captureSession.inputs.contains(backCameraDeviceInput) == true {
            captureSession.removeInput(backCameraDeviceInput)
            captureSession.addInput(frontCameraDeviceInput)
            frontCameraDeviceInput.device.videoZoomFactor = deviceZoomFactor
            isBackCameraActive = false
            self.isBeautyOn = false
            self.isFlashOn = false
            //self.torchButton?.isHidden = true
        }
        
        //Commit all the configuration changes at once
        captureSession.commitConfiguration();
        
        // fix mirrored preview
        //videoPreviewView?.transform = (videoPreviewView?.transform.scaledBy(x: -1, y: 1))!
        /*if !self.isBackCameraActive {
            self.mirrorFrontCamera()
        }*/
        cvPixelBuffer = nil
        if isBackCameraActive {
            self.cameraOptionsArray.append("Flash")
            collection_options.insertItems(at: [IndexPath(row: 5, section: 0)])
        }
        else {
            self.cameraOptionsArray.remove(at: 5)
            collection_options.deleteItems(at: [IndexPath(row: 5, section: 0)])
        }
        //collectionView.reloadItems(at: [IndexPath(item: 5, section: 0)])
        guard let cell = collection_options.cellForItem(at: IndexPath(row: 0, section: 0)) as? CameraOptionsCVCell else { return }
        let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: Double.pi)
        rotation.duration = 0.5//1
        rotation.isCumulative = true
        rotation.repeatCount = 1
        cell.img_option.layer.removeAnimation(forKey: "rotationAnimation")
        cell.img_option.layer.add(rotation, forKey: "rotationAnimation")

    }
    func mergeAllVideos() {
        print("mergeAllVideos")
        let mixComposition = AVMutableComposition()
        
        let compositionVideoTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        let compositionAudioTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        var nextClipStartTime : CMTime = CMTime.zero
                
        for videoModel in self.videosListArray {
            let videoAsset = AVURLAsset(url: videoModel.video_url)
            let timeRangeInAsset = CMTimeRangeMake(start: .zero, duration: videoAsset.duration)
            let videoTrack = videoAsset.tracks(withMediaType: .video)[0]
                        
            do {
                try compositionVideoTrack?.insertTimeRange(CMTimeRange(start: .zero, duration: videoAsset.duration), of: videoTrack, at: nextClipStartTime)
                
                try compositionAudioTrack?.insertTimeRange(CMTimeRange(start: .zero, duration: videoAsset.duration), of: videoAsset.tracks(withMediaType: .audio)[0], at: nextClipStartTime)
            }
            catch {
                print("catch insert timerange:",error.localizedDescription)
            }
            
            nextClipStartTime = CMTimeAdd(nextClipStartTime, timeRangeInAsset.duration)
        }
        
        let savePathURL = URL(fileURLWithPath: NSHomeDirectory() + "/Documents/mergedVideo.mp4")
        do {
            try FileManager.default.removeItem(at: savePathURL)
        }
        catch {
            print("file remove error:",error.localizedDescription)
        }
        
        let assetExport = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)!
        assetExport.outputFileType = .mp4
        assetExport.outputURL = savePathURL
        assetExport.shouldOptimizeForNetworkUse = true
        
        assetExport.exportAsynchronously {
            print("mergingVideos exporter")
            switch assetExport.status {
            case .completed:
                print("success")
                let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                videoPlayerVC.videoURL = assetExport.outputURL
                if self.isFlashOn {
                    self.collectionView(self.collection_options, didSelectItemAt: IndexPath(item: 5, section: 0))
                }
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(videoPlayerVC, animated: true)
                }
            case .failed:
                print("failed",assetExport.error)
            case .cancelled:
                print("cancelled",assetExport.error)
            default:
                print("default switch")
            }
        }
        /*print("mergeAllVideos")
        let savePathURL = URL(fileURLWithPath: NSHomeDirectory() + "/Documents/camRecordedVideo.mp4")
        do {
            try FileManager.default.removeItem(at: savePathURL)
        }
        catch {
            print("file remove error:",error.localizedDescription)
        }
        
        var mutableVideoComposition = AVMutableVideoComposition()
        var mixComposition = AVMutableComposition()
        var finalDuration = CMTime.zero
        let mainInstruction = AVMutableVideoCompositionInstruction()
        for videoModel in self.videosListArray {
            if let videoURL = videoModel.mergingURL {
                let videoAsset = AVAsset(url: videoURL)
                                
                let videoTrack = videoAsset.tracks(withMediaType: .video)[0]
                
                let mutableCompositionNewVideoTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)!
                do {
                    try mutableCompositionNewVideoTrack.insertTimeRange(CMTimeRange(start: .zero, duration: videoAsset.duration), of: videoTrack, at: finalDuration)
                }
                catch {
                    print("newmutabletrack error:",error.localizedDescription)
                }
                
                let videoLayerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: mutableCompositionNewVideoTrack)
                var newScale : CGAffineTransform
                var newMove : CGAffineTransform
                if videoModel.isBackCameraActive {
                    newScale = CGAffineTransform(scaleX: 0.51, y: 0.665)
                    newMove = CGAffineTransform(translationX: 0, y: UIScreen.main.bounds.height / 4)
                }
                else {
                    newScale = CGAffineTransform(scaleX: 0.51, y: 0.59)
                    let y = UIScreen.main.bounds.height / 4
                    let statusBarHeight = UIApplication.shared.statusBarFrame.height
                    let transformY = y - (statusBarHeight * 1.4)
                    newMove = CGAffineTransform(translationX: 0, y: transformY)
                }
                
                
                videoLayerInstruction.setTransform(newScale.concatenating(newMove), at: .zero)
                mainInstruction.layerInstructions.append(videoLayerInstruction)
                
                if let audioTrackFirst = videoAsset.tracks(withMediaType: .audio).first {
                    let audioTrack = mixComposition.addMutableTrack(
                        withMediaType: .audio,
                        preferredTrackID: kCMPersistentTrackID_Invalid)
                    
                    do {
                        
                        try audioTrack?.insertTimeRange(
                            CMTimeRangeMake(
                                start: CMTime.zero,
                                duration: videoAsset.duration),
                            of: /*savedVideoAsset.tracks(withMediaType: .audio)[0]*/audioTrackFirst,
                            at: finalDuration)
                    } catch {
                        print("Failed to load Audio track")
                    }
                }
                finalDuration = CMTimeAdd(finalDuration, videoAsset.duration)
            }
        }
        mainInstruction.timeRange = CMTimeRange(start: .zero, duration: finalDuration)
        //mainInstruction.layerInstructions = [newVideoLayerInstruction, savedVideoLayerInstruction]
        
        mutableVideoComposition.instructions = [mainInstruction]
        mutableVideoComposition.frameDuration = CMTime(value: 1, timescale: 30)
        let width = UIScreen.main.bounds.width
        let compositionWidth = (Int(width) % 2 == 0) ? width : width - 1
        mutableVideoComposition.renderSize = CGSize(
        width: /*UIScreen.main.bounds.width*/compositionWidth,
        height: UIScreen.main.bounds.height)
        
        
        let finalPath = savePathURL.absoluteString
        let assetExport = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)!
        assetExport.videoComposition = mutableVideoComposition
        assetExport.outputFileType = .mp4
        
        assetExport.outputURL = savePathURL
        assetExport.shouldOptimizeForNetworkUse = true
        
        assetExport.exportAsynchronously {
            print("duet merge exporter")
            switch assetExport.status {
            case .completed:
                print("success")
                let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                videoPlayerVC.videoURL = assetExport.outputURL
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(videoPlayerVC, animated: true)
                }
            case .failed:
                print("failed",assetExport.error)
            case .cancelled:
                print("cancelled",assetExport.error)
            default:
                print("default switch")
            }
        }*/
    }
    
}

extension CameraRecordingVC: AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate {
    // MARK: AVCaptureVideoDataOutputSampleBufferDelegate
    public func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        if CMSampleBufferDataIsReady(sampleBuffer) == false {
            print("Sample buffer is not ready")
            return
        }
        
        if output is AVCaptureVideoDataOutput {
            videoOutput(output, didOutput: sampleBuffer, from: connection)
        }
        
        if output is AVCaptureAudioDataOutput {
            audioOutput(output, didOutput: sampleBuffer, from: connection)
        }
    }
    
    private func videoOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            print("Can't create pixel buffer")
            return
        }
        
        guard let videoPreviewViewBounds = videoPreviewViewBounds else {
            print("Unrecognized preview bounds")
            return
        }
        
        var sourceImage = CIImage(cvImageBuffer: pixelBuffer)
        // fix video orientation issue
        if isBackCameraActive == true && orientation == .landscapeLeft {
            sourceImage = sourceImage.oriented(forExifOrientation: 3)
        } else if isBackCameraActive == false && orientation == .landscapeRight {
            sourceImage = sourceImage.oriented(forExifOrientation: 3)
        } else if orientation == .portrait {
            if isBackCameraActive {
                sourceImage = sourceImage.oriented(forExifOrientation: 6)
            }
            else {
                sourceImage = sourceImage.oriented(forExifOrientation: 5)
            }
        }
        let sourceExtent = sourceImage.extent
        
        // add filter to image
        //var filteredImage : CIImage
        var filterImage : CIImage?
        if let ciFilter = self.filter {
            ciFilter.setValue(sourceImage, forKey: kCIInputImageKey)
            filterImage = ciFilter.outputImage
        }
        else {
            filterImage = sourceImage
        }
        guard let filteredImage = filterImage else {
             print("Can't add filter ro image")
            return
        }
        /*guard let filteredImage = sourceImage.invertColorEffect() else {
            print("Can't add filter ro image")
            return
        }*/
        /*switch self.selectedFilter {
        case .NONE:
            guard let filteredImage1 = sourceImage.noneEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .INVERT:
            guard let filteredImage1 = sourceImage.invertColorEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .VIGNETTE:
            guard let filteredImage1 = sourceImage.vignetteEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .PHOTOINSTANT:
            guard let filteredImage1 = sourceImage.photoInstantEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .CRYSTALIZE:
            guard let filteredImage1 = sourceImage.crystallizeEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .COMIC:
            guard let filteredImage1 = sourceImage.comicEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .BLOOM:
            guard let filteredImage1 = sourceImage.bloomEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .EDGES:
            guard let filteredImage1 = sourceImage.edgesEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .EDGEWORK:
            guard let filteredImage1 = sourceImage.edgeWorkEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .GLOOM:
            guard let filteredImage1 = sourceImage.gloomEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .HEXAGONAL:
            guard let filteredImage1 = sourceImage.hexagonalPixellateEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .HIGHLIGHT_SHADOW:
            guard let filteredImage1 = sourceImage.highlightShadowAdjust() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .PIXELLATE:
            guard let filteredImage1 = sourceImage.pixellateEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .POINTILLIZE:
            guard let filteredImage1 = sourceImage.pointillizeEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        default:
            guard let filteredImage1 = sourceImage.noneEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        }*/
        
        let sourceAspect = sourceExtent.size.width / sourceExtent.size.height
        let previewAspect = videoPreviewViewBounds.size.width / videoPreviewViewBounds.size.height
        
        // we want to maintain the aspect radio of the screen size, so we clip the video image
        var drawRect = sourceExtent
        if sourceAspect > previewAspect {
            // use full height of the video image, and center crop the width
            drawRect.origin.x += (drawRect.size.width - drawRect.size.height * previewAspect) / 2.0
            drawRect.size.width = drawRect.size.height * previewAspect
        } else {
            // use full width of the video image, and center crop the height
            drawRect.origin.y += (drawRect.size.height - drawRect.size.width / previewAspect) / 2.0
            drawRect.size.height = drawRect.size.width / previewAspect
        }
        
        videoPreviewView?.bindDrawable()
        if glContext != EAGLContext.current() {
            EAGLContext.setCurrent(glContext)
        }
        
        // clear eagl view to grey
        glClearColor(0.5, 0.5, 0.5, 1.0);
        glClear(GLbitfield(GL_COLOR_BUFFER_BIT));
        
        // set the blend mode to "source over" so that CI will use that
        glEnable(GLenum(GL_BLEND));
        glBlendFunc(GLenum(GL_ONE), GLenum(GL_ONE_MINUS_SRC_ALPHA));
        
        ciContext?.draw(filteredImage, in: videoPreviewViewBounds, from: drawRect)
        videoPreviewView?.display()
        
        //recording
        if isCapturing {
            // convert CIImage to CVPixelBuffer
            if cvPixelBuffer == nil {
                let attributesDictionary = [kCVPixelBufferIOSurfacePropertiesKey: [:]]
                CVPixelBufferCreate(kCFAllocatorDefault,
                                    Int(sourceImage.extent.size.width),
                                    Int(sourceImage.extent.size.height),
                                    kCVPixelFormatType_32BGRA,
                                    attributesDictionary as CFDictionary,
                                    &cvPixelBuffer);
            }
            ciContext?.render(filteredImage, to: cvPixelBuffer!)
            
            // convert new CVPixelBuffer to new CMSampleBuffer
            var sampleTime = CMSampleTimingInfo()
            sampleTime.duration = CMSampleBufferGetDuration(sampleBuffer)
            sampleTime.presentationTimeStamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
            sampleTime.decodeTimeStamp = CMSampleBufferGetDecodeTimeStamp(sampleBuffer)
            
            var videoInfo: CMVideoFormatDescription? = nil
            CMVideoFormatDescriptionCreateForImageBuffer(allocator: kCFAllocatorDefault, imageBuffer: cvPixelBuffer!, formatDescriptionOut: &videoInfo)
            var newSampleBuffer: CMSampleBuffer?
            CMSampleBufferCreateReadyWithImageBuffer(allocator: kCFAllocatorDefault, imageBuffer: cvPixelBuffer!, formatDescription: videoInfo!, sampleTiming: &sampleTime, sampleBufferOut: &newSampleBuffer)
            
            // writer new CMSampleBuffer to asser writer
            self.videoWriter?.write(sample: newSampleBuffer!, isVideoBuffer: true)
        }
    }
    
    private func audioOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        //recording
        if isCapturing {
            self.videoWriter?.write(sample: sampleBuffer, isVideoBuffer: false)
        }
    }
}

extension CameraRecordingVC: FilterSelectionViewDelegate {
    //MARK:- FilterSelectionView Delegates...
    func didSelectFilter(filter: (title: String, type: FilterItem)) {
        print("selected filter:",filter)
        self.selectedFilter = filter
        switch filter.type {
        case .NONE:
            self.filter = nil
        /*case .MONOCHROME_FILTER:
            self.filter = CIFilter(name: "CIColorMonochrome")
        case .INVERT_COLORS_FILTER:
            self.filter = CIFilter(name: "CIColorInvert")
        case .FALSE_COLOR_FILTER:
            self.filter = CIFilter(name: "CIFalseColor")*/
        case .COLOR_MATRIX_FILTER:
            self.filter = CIFilter(name: filter.type.rawValue)
            self.filter?.setValue(CIVector(x: 0, y: 0, z: 0, w: 0), forKey: "inputGVector")
        case .TEMPERATURE_TINT_FILTER:
            self.filter = CIFilter(name: filter.type.rawValue)
            if filter.title == "F10" {
                self.filter?.setValue(CIVector(x: 16000, y: 1000), forKey: "inputNeutral")
                self.filter?.setValue(CIVector(x: 1000, y: 500), forKey: "inputTargetNeutral")
            }
            else if filter.title == "F11" {
                self.filter?.setValue(CIVector(x: 6500, y: 500), forKey: "inputNeutral")
                self.filter?.setValue(CIVector(x: 1000, y: 630), forKey: "inputTargetNeutral")
            }
        default:
            self.filter = CIFilter(name: filter.type.rawValue)
        }
    }
}

extension CameraRecordingVC: UIGestureRecognizerDelegate {
    //MARK:- UIGestureRecognizer Delegates...
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        print("shouldReceive touch:",touch.view)
        print("is touch view image_camera:",touch.view == self.view_camera)
        if touch.view?.isDescendant(of: self.view_camera) == true && touch.view != self.view_camera {
            return false
        }
        return true
    }
}

extension CameraRecordingVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    //MARK:- ImagePicker Delegates...
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print("video captured info:",info)
        guard let url = info[UIImagePickerController.InfoKey.mediaURL] as? URL else {
            picker.dismiss(animated: true, completion: nil)
            return
        }
        picker.dismiss(animated: true) {
            self.videoSelectedFromLibrary(url: url)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("imagePickerControllerDidCancel")
        picker.dismiss(animated: true, completion: nil)
    }
    //MARK: m code
    func videoEditorController(_ editor: UIVideoEditorController, didSaveEditedVideoToPath path: String) {
        print("edite save")
       self.dismiss(animated:true)
    }
    func videoEditorControllerDidCancel(_ editor: UIVideoEditorController) {
        print("edite cancel")
    }

}

extension CameraRecordingVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    //MARK:- CollectionView Delegates...
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collection_options {
            return self.cameraOptionsArray.count
        }
        else if collectionView == self.collection_cameraTypes {
            return self.cameraTypesArray.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.collection_options {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CameraOptionsCVCell", for: indexPath) as! CameraOptionsCVCell
            
            cell.lbl_title.text = self.cameraOptionsArray[indexPath.item]
            
            if indexPath.item == 5 {
                if self.isFlashOn {
                    cell.img_option.image = #imageLiteral(resourceName: "ic_flashOn").withRenderingMode(.alwaysTemplate)
                }
                else {
                    cell.img_option.image = #imageLiteral(resourceName: "ic_flash").withRenderingMode(.alwaysOriginal)
                }
            }
            else if indexPath.item == 1 {
                if self.currentSpeed == .NORMAL {
                    cell.img_option.image = #imageLiteral(resourceName: "ic_speedOff")
                }
                else {
                    cell.img_option.image = #imageLiteral(resourceName: "ic_speedOn")
                }
            }
            else {
                cell.img_option.image = self.cameraOptionsImagesArray[indexPath.item].withRenderingMode(.alwaysOriginal)
            }
            
            cell.img_option.tintColor = UIColor.white
            return cell
        }
        else if collectionView == self.collection_cameraTypes {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CameraTypeCVCell", for: indexPath) as! CameraTypeCVCell
            
            cell.lbl_title.text = self.cameraTypesArray[indexPath.item]
            
            if self.selectedCameraType == indexPath.item {
                cell.lbl_dot.isHidden = false
            }
            else {
                cell.lbl_dot.isHidden = true
            }
            
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.collection_options {
            return CGSize(width: 50, height: 65)
        }
        else if collectionView == self.collection_cameraTypes {
            let text =  self.cameraTypesArray[indexPath.item]
            let width = UILabel.textWidth(font: UIFont(name:"OpenSans-Regular",size:13)!, text: text)
            
            return CGSize(width: width + 60, height: 40)
        }
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == self.collection_options {
            return 0
        }
        else if collectionView == self.collection_cameraTypes {
            return 5
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == self.collection_options {
            return 0
        }
        else if collectionView == self.collection_cameraTypes {
            return 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == self.collection_options {
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        else if collectionView == self.collection_cameraTypes {
            return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        }
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.collection_options {
            switch indexPath.item {
            case 0:
                guard let captureSession = captureSession else {
                    print("Can't switch camera, session is nil")
                    return
                }
                
                guard let frontCameraDeviceInput = frontCameraDeviceInput else {
                    print("Can't switch camera, frontCameraDeviceInput is nil")
                    return
                }
                
                guard let backCameraDeviceInput = backCameraDeviceInput else {
                    print("Can't switch camera, backCameraDeviceInput is nil")
                    return
                }
                
                captureSession.beginConfiguration()
                //Change camera device inputs from back to front or opposite
                if captureSession.inputs.contains(frontCameraDeviceInput) == true {
                    captureSession.removeInput(frontCameraDeviceInput)
                    captureSession.addInput(backCameraDeviceInput)
                    backCameraDeviceInput.device.videoZoomFactor = deviceZoomFactor
                    isBackCameraActive = true
                    self.isBeautyOn = false
                    //self.torchButton?.isHidden = false
                } else if captureSession.inputs.contains(backCameraDeviceInput) == true {
                    captureSession.removeInput(backCameraDeviceInput)
                    captureSession.addInput(frontCameraDeviceInput)
                    frontCameraDeviceInput.device.videoZoomFactor = deviceZoomFactor
                    isBackCameraActive = false
                    self.isBeautyOn = false
                    self.isFlashOn = false
                    //self.torchButton?.isHidden = true
                }
                
                //Commit all the configuration changes at once
                captureSession.commitConfiguration();
                
                // fix mirrored preview
                //videoPreviewView?.transform = (videoPreviewView?.transform.scaledBy(x: -1, y: 1))!
                /*if !self.isBackCameraActive {
                    self.mirrorFrontCamera()
                }*/
                cvPixelBuffer = nil
                if isBackCameraActive {
                    self.cameraOptionsArray.append("Flash")
                    collectionView.insertItems(at: [IndexPath(item: 5, section: 0)])
                }
                else {
                    self.cameraOptionsArray.remove(at: 5)
                    collectionView.deleteItems(at: [IndexPath(item: 5, section: 0)])
                }
                //collectionView.reloadItems(at: [IndexPath(item: 5, section: 0)])
                guard let cell = collectionView.cellForItem(at: indexPath) as? CameraOptionsCVCell else { return }
                let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
                rotation.toValue = NSNumber(value: Double.pi)
                rotation.duration = 0.5//1
                rotation.isCumulative = true
                rotation.repeatCount = 1
                cell.img_option.layer.removeAnimation(forKey: "rotationAnimation")
                cell.img_option.layer.add(rotation, forKey: "rotationAnimation")
                /*guard let input = self.videoInput else {
                    return
                }
                self.captureSession.beginConfiguration()
                if self.currentCamera == self.backCamera {
                    if self.isFlashOn {
                        /*guard let device = AVCaptureDevice.default(for: .video) else { return }
                        guard device.hasTorch else { return }
                        do {
                            try device.lockForConfiguration()
                            
                            device.torchMode = .off
                            self.isFlashOn = false
                            device.unlockForConfiguration()
                        }
                        catch {
                            print("lockForConfiguration error : ",error.localizedDescription)
                        }*/
                        self.isFlashOn = false
                    }
                    self.captureSession.removeInput(input)
                    guard let camera = self.frontCamera else {
                        return
                    }
                    do {
                        self.videoInput = try AVCaptureDeviceInput(device: camera)
                        self.currentCamera = camera
                        if let video = self.videoInput,  self.captureSession.canAddInput(video) {
                            self.captureSession.addInput(video)
                        }
                        self.mirrorFrontCamera()
                    }
                    catch {
                        print("camera error:",error.localizedDescription)
                    }
                }
                else {
                    self.captureSession.removeInput(input)
                    guard let camera = self.backCamera else {
                        return
                    }
                    do {
                        self.videoInput = try AVCaptureDeviceInput(device: camera)
                        self.currentCamera = camera
                        if let video = self.videoInput,  self.captureSession.canAddInput(video) {
                            self.captureSession.addInput(video)
                        }
                    }
                    catch {
                        print("camera error:",error.localizedDescription)
                    }
                }
                self.captureSession.commitConfiguration()
                videoPreviewView?.transform = (videoPreviewView?.transform.scaledBy(x: -1, y: 1))!
                cvPixelBuffer = nil
                collectionView.reloadItems(at: [IndexPath(item: 5, section: 0)])*/
            case 5:
                /*if self.imagePicker.cameraDevice == .rear {
                 if self.imagePicker.cameraFlashMode == .off {
                 self.imagePicker.cameraFlashMode = .on
                 }
                 else {
                 self.imagePicker.cameraFlashMode = .off
                 }
                 collectionView.reloadItems(at: [indexPath])
                 }*/
                /*if self.currentCamera == self.backCamera {
                    guard let device = AVCaptureDevice.default(for: .video) else { return }
                    guard device.hasTorch else { return }
                    do {
                        try device.lockForConfiguration()
                        
                        if device.torchMode == .on {
                            device.torchMode = .off
                            self.isFlashOn = false
                            //sender.setImage(#imageLiteral(resourceName: "flash_off"), for: .normal)
                        }
                        else {
                            do {
                                try device.setTorchModeOn(level: 1.0)
                                self.isFlashOn = true
                                //sender.setImage(#imageLiteral(resourceName: "flash_on"), for: .normal)
                            }
                            catch {
                                print("setTorchModeOn error : ",error.localizedDescription)
                            }
                        }
                        device.unlockForConfiguration()
                        collectionView.reloadItems(at: [indexPath])
                    }
                    catch {
                        print("lockForConfiguration error : ",error.localizedDescription)
                    }
                }*/
                guard let backDevice = backCameraDeviceInput?.device else {
                    print("Can't find back device")
                    return;
                }
                
                if backDevice.hasTorch == false || backDevice.isTorchAvailable == false {
                    print("Can't turn on/off tourch")
                    return;
                }
                
                do {
                    try backDevice.lockForConfiguration()
                    backDevice.torchMode = backDevice.torchMode == .on ? .off : .on;
                    backDevice.unlockForConfiguration()
                    self.isFlashOn = backDevice.torchMode == .on ? true : false
                } catch {
                    print("Something went wrong")
                }
                collectionView.reloadItems(at: [IndexPath(item: 5, section: 0)])
            case 1:
                print("speed view")
                let speedVariationView = Bundle.main.loadNibNamed("SpeedVariationView", owner: nil, options: nil)![0] as! SpeedVariationView
                speedVariationView.delegate = self
                speedVariationView.selectedSpeed = self.currentSpeed
                self.view.addSubview(speedVariationView)
            case 2:
                let indexPath = IndexPath(row: 2, section: 0)
                let currentCell = collection_options.cellForItem(at: indexPath) as? CameraOptionsCVCell
                if isBackCameraActive == true{
                    guard let currentISO = backCameraDeviceInput?.device.iso else{return}
                    var newIso: Float = 0.0
                    try? backCameraDeviceInput?.device.lockForConfiguration()
                    if !isBeautyOn {
                        newIso = currentISO + 100.0
                        newIso = newIso > (backCameraDeviceInput?.device.activeFormat.maxISO)! ? (backCameraDeviceInput?.device.activeFormat.maxISO)! : newIso
                        newIso = newIso < (backCameraDeviceInput?.device.activeFormat.minISO)! ? (backCameraDeviceInput?.device.activeFormat.minISO)! : newIso
                        
                        backCameraDeviceInput?.device.setExposureModeCustom(duration: AVCaptureDevice.currentExposureDuration, iso: newIso, completionHandler: nil)
                        
                        //                    UIScreen.main.brightness = 1.0
                        self.isBeautyOn = true
                        ToastMessage.showToast(in: self, message: "Beauty-On", bgColor: .white)
                        currentCell?.lbl_title.text = "Beauty-On"
                        
                    }else{
                        newIso = currentISO - 100.0
                        newIso = newIso > (backCameraDeviceInput?.device.activeFormat.maxISO)! ? (backCameraDeviceInput?.device.activeFormat.maxISO)! : newIso
                        newIso = newIso < (backCameraDeviceInput?.device.activeFormat.minISO)! ? (backCameraDeviceInput?.device.activeFormat.minISO)! : newIso
                        
                        backCameraDeviceInput?.device.setExposureModeCustom(duration: AVCaptureDevice.currentExposureDuration, iso: newIso, completionHandler: nil)
                        
                        //                    UIScreen.main.brightness = 0.5
                        self.isBeautyOn = false
                        ToastMessage.showToast(in: self, message: "Beauty-Off", bgColor: .white)
                        currentCell?.lbl_title.text = "Beauty-Off"
                    }
                    backCameraDeviceInput?.device.unlockForConfiguration()
                }else{
                    guard let currentISO = frontCameraDeviceInput?.device.iso else{return}
                    var newIso: Float = 0.0
                    try? frontCameraDeviceInput?.device.lockForConfiguration()
                    if !isBeautyOn {
                        newIso = currentISO + 300.0
                        newIso = newIso > (frontCameraDeviceInput?.device.activeFormat.maxISO)! ? (frontCameraDeviceInput?.device.activeFormat.maxISO)! : newIso
                        newIso = newIso < (frontCameraDeviceInput?.device.activeFormat.minISO)! ? (frontCameraDeviceInput?.device.activeFormat.minISO)! : newIso
                        
                        frontCameraDeviceInput?.device.setExposureModeCustom(duration: AVCaptureDevice.currentExposureDuration, iso: newIso, completionHandler: nil)
                        
                        //                    UIScreen.main.brightness = 1.0
                        self.isBeautyOn = true
                        ToastMessage.showToast(in: self, message: "Beauty-On", bgColor: .white)
                        currentCell?.lbl_title.text = "Beauty-On"
                        
                    }else{
                        newIso = currentISO - 300.0
                        newIso = newIso > (frontCameraDeviceInput?.device.activeFormat.maxISO)! ? (frontCameraDeviceInput?.device.activeFormat.maxISO)! : newIso
                        newIso = newIso < (frontCameraDeviceInput?.device.activeFormat.minISO)! ? (frontCameraDeviceInput?.device.activeFormat.minISO)! : newIso
                        
                        frontCameraDeviceInput?.device.setExposureModeCustom(duration: AVCaptureDevice.currentExposureDuration, iso: newIso, completionHandler: nil)
                        
                        //                    UIScreen.main.brightness = 0.5
                        self.isBeautyOn = false
                        ToastMessage.showToast(in: self, message: "Beauty-Off", bgColor: .white)
                        currentCell?.lbl_title.text = "Beauty-Off"
                    }
                    frontCameraDeviceInput?.device.unlockForConfiguration()
                }
            case 3:
                print("filter view")
                let filterSelectionView = Bundle.main.loadNibNamed("FilterSelectionView", owner: nil, options: nil)![0] as! FilterSelectionView
                filterSelectionView.delegate = self
                filterSelectionView.selectedFilter = self.selectedFilter
                self.view.addSubview(filterSelectionView)
            case 4:
                print("timer view")
                let timerSelectionView = Bundle.main.loadNibNamed("TimerSelectionView", owner: nil, options: nil)![0] as! TimerSelectionView
                timerSelectionView.slider_timer.maximumValue = Float(self.captureTime)
                timerSelectionView.slider_timer.value = Float(self.captureTime)
                timerSelectionView.lbl_timer.text = "\(Int(floor(self.captureTime)))s"
                timerSelectionView.delegate = self
                self.view.addSubview(timerSelectionView)
            default:
                break
            }
        }
        else if collectionView == self.collection_cameraTypes {
            //if !self.movieFileOutput.isRecording {
                self.selectedCameraType = indexPath.item
                switch indexPath.item {
                case 1:
                    if let duration = self.audioDuration {
                        self.captureTime = self.captureTime < 60 ? self.captureTime : 60
                    }
                    else {
                        self.captureTime = 60
                    }
                default:
                    if let duration = self.audioDuration {
                        self.captureTime = self.captureTime < 15 ? self.captureTime : 15
                    }
                    else {
                        self.captureTime = 15
                    }
                }
                collectionView.reloadData()
            //}
        }
    }
}

extension CameraRecordingVC: SpeedVariationViewDelegate {
    //MARK:- SpeedVariationView Delegates...
    func selectedSpeed(speedItem: SpeedItem) {
        self.currentSpeed = speedItem
        self.collection_options.reloadItems(at: [IndexPath(item: 1, section: 0)])
    }
}

extension CameraRecordingVC: TimerSelectionViewDelegate {
    //MARK:- TimerSelectionView Delegates...
    func startShutter(timeSpot: Int, timer: Float) {
        self.timerDuration = Double(timer)
        self.shutterDuration = timeSpot
        self.lbl_shutterTime.text = "\(self.shutterDuration)"
        self.lbl_shutterTime.isHidden = false
        self.shutterTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.countShutter), userInfo: nil, repeats: true)
        guard let timerAudioPath = Bundle.main.url(forResource: "Timer_Sound", withExtension: "mp3") else {
            print("timer sound not found")
            return
        }
        self.audioPlayer = AVPlayer(url: timerAudioPath)
        self.audioPlayer?.play()
        
    }
    
    @objc func countShutter() {
        self.shutterDuration -= 1
        self.lbl_shutterTime.text = "\(self.shutterDuration)"
        if shutterDuration <= 0 {
            self.audioPlayer?.pause()
            self.audioPlayer = nil
            self.captureVideo()
            self.lbl_shutterTime.isHidden = true
            self.shutterTimer?.invalidate()
            self.shutterTimer = nil
        }
    }
}

extension CameraRecordingVC: AudioListVCDelegate {
    //MARK:- AudioListVC Delegates...
    func selectedAudio(url: String?, id: String?, name: String?) {
        if let url = URL(string: url ?? "") {
            self.audioAsset = AVAsset(url: url)
            self.lbl_audio.text = name ?? "Sounds"
            self.lbl_audio.isHidden = false
            self.progress_audio.progress = 0
            self.progress_audio.isHidden = false
            if let asset = self.audioAsset {
                self.audioDuration = asset.duration.seconds//Int(asset.duration.seconds)
            }
            if let duration = audioDuration {
                captureTime = captureTime < duration ? captureTime : duration
            }
            print("selectedAudio audioAsset:",audioAsset)
        }
    }
}

extension String {
    func image() -> UIImage? {
            let size = CGSize(width: 40, height: 40)
            UIGraphicsBeginImageContextWithOptions(size, false, 0)
            UIColor.white.set()
            let rect = CGRect(origin: .zero, size: size)
            UIRectFill(CGRect(origin: .zero, size: size))
            (self as AnyObject).draw(in: rect, withAttributes: [.font: UIFont.systemFont(ofSize: 40)])
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return image
        }
}
