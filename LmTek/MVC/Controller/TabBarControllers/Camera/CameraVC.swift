//
//  CameraVC.swift
//  LmTek
//
//  Created by PTBLR-1128 on 24/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

class CameraVC: UIViewController {
    //MARK: Outlets...
    @IBOutlet weak var view_camera : UIView!
    @IBOutlet weak var collection_options : UICollectionView!
    @IBOutlet weak var view_gradient : UIView!
    @IBOutlet weak var view_capture : UIView!
    @IBOutlet weak var btn_audio : UIButton!
    @IBOutlet weak var view_effects : UIView!
    @IBOutlet weak var view_image : UIView!
    @IBOutlet weak var collection_cameraTypes : UICollectionView!
    @IBOutlet weak var lbl_timer : UILabel!
    @IBOutlet weak var lbl_audio : UILabel!
    @IBOutlet weak var progress_audio : UIProgressView!

    //MARK: Variables...
    //var imagePicker = UIImagePickerController()
    var cameraOptionsArray = ["Flip", "Speed", "Beauty", "Filters", "Timer", "Flash"]
    var cameraOptionsImagesArray = [#imageLiteral(resourceName: "ic_changeCamera"), #imageLiteral(resourceName: "ic_speed"), #imageLiteral(resourceName: "ic_beauty"), #imageLiteral(resourceName: "ic_filters"), #imageLiteral(resourceName: "ic_timer")]
    var cameraTypesArray = ["Live", "60s", "15s", "Templates"]
    var selectedCameraType = 2
    
    var isCapturing = false
    
    var captureSession = AVCaptureSession()
    var backCamera: AVCaptureDevice?
    var frontCamera: AVCaptureDevice?
    var currentCamera: AVCaptureDevice?
    var videoInput : AVCaptureInput?
    let videoOutput = AVCaptureVideoDataOutput()
    
    var previewLayer = AVCaptureVideoPreviewLayer()
    
    var isInitial = true
    var isFlashOn = false
    
    var movieFileOutput = AVCaptureMovieFileOutput()
    
    var recordEndCorrectly = true
    
    var mediaPicker : MPMediaPickerController!
    var audioAsset : AVAsset?
    var audioPlayer : AVPlayer?
    
    var recordTimer : Timer?
    var recordingTime = 0
    var captureTime = 15
    var audioDuration : Int?
    var displayLink : CADisplayLink?
    
    private var _assetWriter: AVAssetWriter?
    private var _assetWriterInput: AVAssetWriterInput?
    private var _adpater: AVAssetWriterInputPixelBufferAdaptor?
    private var _currentSampleTime: CMTime?
    private var _currentVideoDimensions: CMVideoDimensions?
    private var _filename = ""
    private var _time: Double = 0
    
    private enum _CaptureState {
        case idle, start, capturing, end
    }
    private var _captureState = _CaptureState.idle
    
    var currentSpeed : SpeedItem = .NORMAL
    
    //MARK:- View Life Cycle...
    override func viewDidLoad() {
        super.viewDidLoad()

        self.cellRegistration()
        self.view_gradient.setGradient(colors: [UIColor.black.withAlphaComponent(0).cgColor, UIColor.black.withAlphaComponent(0.7).cgColor], startPoint: CGPoint.init(x:0, y:0), endPoint: CGPoint.init(x:0, y:1))
//        self.view_capture.setGradient(colors: [UIColor(hexString: Constants.ORANGE_COLOR)?.cgColor ?? UIColor.orange.cgColor, UIColor(hexString: Constants.YELLOW_COLOR)?.cgColor ?? UIColor.yellow.cgColor], startPoint: CGPoint.init(x:0, y:0), endPoint: CGPoint.init(x:0, y:1))
        //self.showCamera()
        setupDevice()
        setupInputOutput()
        setUpMediaPicker()
                
        let audioImage = #imageLiteral(resourceName: "ic_audio").withRenderingMode(.alwaysTemplate)
        self.btn_audio.setImage(audioImage, for: .normal)
        self.btn_audio.tintColor = UIColor.white
        
        self.view_effects.setGradient(colors: [UIColor(hexString: Constants.ORANGE_COLOR)?.cgColor ?? UIColor.orange.cgColor, UIColor(hexString: Constants.RED_COLOR)?.cgColor ?? UIColor.red.cgColor], startPoint: CGPoint(x: 0, y: 1), endPoint: CGPoint(x: 1, y: 0))
        self.view_image.setGradient(colors: [UIColor(hexString: Constants.DARK_BLUE_COLOR)?.cgColor ?? UIColor.blue.cgColor, UIColor(hexString: Constants.LIGHT_BLUE_COLOR)?.cgColor ?? UIColor.blue.cgColor], startPoint: CGPoint(x: 0, y: 1), endPoint: CGPoint(x: 1, y: 0))
    }
        
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !self.captureSession.isRunning {
            self.captureSession.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.captureSession.isRunning {
            self.captureSession.stopRunning()
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        print("viewDidLayoutSubviews")
        if isInitial {
            print("isInitial")
            self.isInitial = false
            let preview = AVCaptureVideoPreviewLayer(session: self.captureSession)
            preview.frame = self.view_camera.bounds
            preview.videoGravity = .resizeAspectFill
            self.view_camera.layer.insertSublayer(preview, at: 0)
        }
    }
    
    //MARK:- IBActions...
    @IBAction func crossClicked() {
        if self.movieFileOutput.isRecording {
            let alertController = UIAlertController(title: "Alert", message: "Going back removes the saved video.\nDo you want to delete?", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
                self.recordEndCorrectly = false
                self.invalidateTimer()
                self.movieFileOutput.stopRecording()
                self.audioAsset = nil
                self.lbl_audio.text = ""
                self.audioPlayer?.pause()
                self.audioPlayer = nil
                self.currentSpeed = .NORMAL
                self.tabBarController?.selectedIndex = Constants.previousTab
                if let tabBar = self.tabBarController?.tabBar, let items = tabBar.items {
                    self.tabBarController?.tabBar(tabBar, didSelect: items[Constants.previousTab])
                }
            }))
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
        else {
            self.lbl_timer.isHidden = true
            self.progress_audio.isHidden = true
            self.progress_audio.progress = 0
            self.audioAsset = nil
            self.lbl_audio.text = ""
            self.currentSpeed = .NORMAL
            self.tabBarController?.selectedIndex = Constants.previousTab
            if let tabBar = self.tabBarController?.tabBar, let items = tabBar.items {
                self.tabBarController?.tabBar(tabBar, didSelect: items[Constants.previousTab])
            }
        }
    }
    
    @IBAction func cameraFlipClicked() {
        
    }
    
    @IBAction func captureVideo() {
        if self.movieFileOutput.isRecording {
            print("recording")
            self.recordEndCorrectly = true
            self.invalidateTimer()
            self.audioPlayer?.pause()
            self.audioPlayer = nil
            self.movieFileOutput.stopRecording()
        }
        else {
            print("not recording")
            let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            let fileURL = paths[0].appendingPathComponent("output\(Int64(Date().timeIntervalSince1970)).mp4")//mov")
            //do {
            try? FileManager.default.removeItem(at: fileURL)
            if let asset = self.audioAsset {
                self.audioPlayer = nil
                let playerItem = AVPlayerItem(asset: asset)
                self.audioPlayer = AVPlayer(playerItem: playerItem)
                self.audioPlayer?.play()
                self.displayLink = CADisplayLink(target: self, selector: #selector(self.updateSlider))
                self.displayLink?.preferredFramesPerSecond = 10
                self.progress_audio.progress = 0
                self.progress_audio.isHidden = false
                self.displayLink?.add(to: .current, forMode: .default)
                
            }
            self.movieFileOutput.startRecording(to: fileURL, recordingDelegate: self)
            self.startTimer()
            
        }
    }
    
    @IBAction func musicClicked() {
        //self.present(self.mediaPicker, animated: true, completion: nil)
        let audiosListVC = self.storyboard?.instantiateViewController(withIdentifier: "AudiosListVC") as! AudiosListVC
        audiosListVC.delegate = self
        self.navigationController?.pushViewController(audiosListVC, animated: true)
    }
    
    @IBAction func galleryClicked() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = ["public.movie"]
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func pinchToZoom(_ sender: UIPinchGestureRecognizer) {
        guard let device = self.currentCamera else {
            return
        }
        if sender.state == .changed {
            let maxZoomFactor = device.activeFormat.videoMaxZoomFactor
            let pinchVelocityDividerFactor : CGFloat = 2.0
            
            do {
                try device.lockForConfiguration()
                defer {
                    device.unlockForConfiguration()
                }
                
                let desiredZoomFactor = device.videoZoomFactor + atan2(sender.velocity, pinchVelocityDividerFactor)
                device.videoZoomFactor = max(1.0, min(desiredZoomFactor, maxZoomFactor))
                
            }
            catch {
                print("device lockconfiguration exception:",error.localizedDescription)
            }
        }
    }
    
    //MARK:- Functions...
    /*func showCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            self.imagePicker.sourceType = .camera
            self.imagePicker.cameraDevice = .front
            self.imagePicker.showsCameraControls = false
            
            self.imagePicker.delegate = self
                        
            let size = self.view_camera.bounds
            let aspectRatio = CGFloat(4.0/3.0)
            let height = size.width * aspectRatio
            let scale = size.height / height
            self.imagePicker.cameraViewTransform = CGAffineTransform(translationX: 0, y: (size.height - height) / 2)
            self.imagePicker.cameraViewTransform = self.imagePicker.cameraViewTransform.scaledBy(x: scale, y: scale)
            
            self.imagePicker.view.frame = self.view_camera.bounds
            self.view_camera.addSubview(self.imagePicker.view)
            self.view_camera.sendSubviewToBack(self.imagePicker.view)
        }
    }*/
    
    func cellRegistration() {
        self.collection_options.register(UINib(nibName: "CameraOptionsCVCell", bundle: nil), forCellWithReuseIdentifier: "CameraOptionsCVCell")
        self.collection_cameraTypes.register(UINib(nibName: "CameraTypeCVCell", bundle: nil), forCellWithReuseIdentifier: "CameraTypeCVCell")
    }
    
    func setupDevice() {
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
        let devices = deviceDiscoverySession.devices
        
        for device in devices {
            if device.position == AVCaptureDevice.Position.back {
                backCamera = device
            }
            else if device.position == AVCaptureDevice.Position.front {
                frontCamera = device
            }
        }
        
        currentCamera = frontCamera
    }
    
    func setupInputOutput() {
        do {
            guard let camera = currentCamera else {
                return
            }
            self.videoInput = try AVCaptureDeviceInput(device: camera)
            self.captureSession.sessionPreset = .photo//.high
            if let input = self.videoInput, captureSession.canAddInput(input) {
                captureSession.addInput(input)
            }
            
            guard let audioDevice = AVCaptureDevice.default(for: .audio) else { return }
            
            let audioInput = try AVCaptureDeviceInput(device: audioDevice)
            if captureSession.canAddInput(audioInput) {
                captureSession.addInput(audioInput)
            }
            
            if self.captureSession.canAddOutput(self.movieFileOutput) {
                self.captureSession.addOutput(self.movieFileOutput)
            }
            /*if self.captureSession.canAddOutput(videoOutput) {
                videoOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "com.lmtek.LmTek"))
                self.captureSession.addOutput(videoOutput)
            }*/
            
            
            self.mirrorFrontCamera()
            
            self.captureSession.startRunning()
        } catch {
            print(error)
        }
    }
    
    func mirrorFrontCamera() {
        /*for connection in self.movieFileOutput.connections {
            for port in connection.inputPorts {
                if port.mediaType == .video {
                    if connection.isVideoMirroringSupported {
                        connection.isVideoMirrored = true
                        break
                    }
                }
            }
        }*/
        print("mirrorFrontCamera")
        guard let currentCameraInput = captureSession.inputs.first as? AVCaptureDeviceInput else {
            return
        }
        if let conn = self.movieFileOutput.connection(with: .video) {
            conn.isVideoMirrored = true//currentCameraInput.device.position == .front
        }
    }
    
    func setUpMediaPicker() {
        self.mediaPicker = MPMediaPickerController(mediaTypes: .any)
        self.mediaPicker.delegate = self
        self.mediaPicker.prompt = "Select audio"
    }
    
    func mergeVideoAudio(videoURL: URL, cameraType: String) {
        print("mergeVideoAudio")
        let videoAsset = AVAsset(url: videoURL)
        let mixComposition = AVMutableComposition()

        // 2 - Create two video tracks
        guard
          let videoTrack = mixComposition.addMutableTrack(
            withMediaType: .video,
            preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
          else { return }
        print("guard videoTrack")

        do {
            print("videoAsset.tracks(withMediaType: .video):",videoAsset.tracks(withMediaType: .video))
          try videoTrack.insertTimeRange(
            CMTimeRangeMake(start: .zero, duration: videoAsset.duration),
            of: videoAsset.tracks(withMediaType: .video)[0],
            at: .zero)
        } catch {
          print("Failed to load first track")
          return
        }

        if cameraType == "back" {
            
        }
        else {
            
        }

        // 3 - Composition Instructions
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRangeMake(
          start: .zero,
          duration: videoAsset.duration)
        print("mainInstruction")

        // 4 - Set up the instructions — one for each asset
        let videoInstruction = self.videoCompositionInstruction(
          videoTrack,
          asset: videoAsset, cameraType: cameraType)
        videoInstruction.setOpacity(0.0, at: videoAsset.duration)
        print("videoInstruction")

        // 5 - Add all instructions together and create a mutable video composition
        mainInstruction.layerInstructions = [videoInstruction]
        let mainComposition = AVMutableVideoComposition()
        mainComposition.instructions = [mainInstruction]
        mainComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        let width = UIScreen.main.bounds.width
        let compositionWidth = (Int(width) % 2 == 0) ? width : width - 1
        mainComposition.renderSize = CGSize(
          width: /*UIScreen.main.bounds.width*/compositionWidth,
          height: UIScreen.main.bounds.height)
        print("mainComposition")
        // 6 - Audio track
        if let loadedAudioAsset = self.audioAsset {
          let audioTrack = mixComposition.addMutableTrack(
            withMediaType: .audio,
            preferredTrackID: 0)
          do {
            try audioTrack?.insertTimeRange(
              CMTimeRangeMake(
                start: CMTime.zero,
                duration: videoAsset.duration),
              of: loadedAudioAsset.tracks(withMediaType: .audio)[0],
              at: .zero)
          } catch {
            print("Failed to load Audio track")
          }
            //audioTrack?.scaleTimeRange(CMTimeRangeMake(start: .zero, duration: videoAsset.duration), toDuration: CMTimeMake(value: finalTimeScale, timescale: videoAsset.duration.timescale))
        }
        else if let audioTrackFirst = videoAsset.tracks(withMediaType: .audio).first {
            print("natural audio")
            let audioTrack = mixComposition.addMutableTrack(
                withMediaType: .audio,
                preferredTrackID: kCMPersistentTrackID_Invalid)
            
            do {
                
                try audioTrack?.insertTimeRange(
                    CMTimeRangeMake(
                        start: CMTime.zero,
                        duration: videoAsset.duration),
                    of: /*savedVideoAsset.tracks(withMediaType: .audio)[0]*/audioTrackFirst,
                    at: .zero)
            } catch {
                print("Failed to load Audio track")
            }
            //audioTrack?.scaleTimeRange(CMTimeRangeMake(start: .zero, duration: videoAsset.duration), toDuration: CMTimeMake(value: finalTimeScale, timescale: videoAsset.duration.timescale))
        }

        // 7 - Get path
        guard
          let documentDirectory = FileManager.default.urls(
            for: .documentDirectory,
            in: .userDomainMask).first
          else { return }
        print("guard documentDirectory")
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .short
        let date = dateFormatter.string(from: Date())
        let url = documentDirectory.appendingPathComponent("mergeVideo-\(date).mp4")

        // 8 - Create Exporter
        guard let exporter = AVAssetExportSession(
          asset: mixComposition,
          presetName: AVAssetExportPresetHighestQuality)
          else { return }
        print("guard exporter")
        exporter.outputURL = url
        exporter.outputFileType = AVFileType.mp4
        exporter.shouldOptimizeForNetworkUse = true
        exporter.videoComposition = mainComposition

        // 9 - Perform the Export
        exporter.exportAsynchronously {
            print("exporter.exportAsynchronously")
            DispatchQueue.main.async {
                self.exportDidFinish(exporter)
            }
        }
    }
    
    func orientationFromTransform(
      _ transform: CGAffineTransform
    ) -> (orientation: UIImage.Orientation, isPortrait: Bool) {
        var assetOrientation = UIImage.Orientation.up
        var isPortrait = false
        let tfA = transform.a
        let tfB = transform.b
        let tfC = transform.c
        let tfD = transform.d
        
        if tfA == 0 && tfB == 1.0 && tfC == -1.0 && tfD == 0 {
            print("orientationFromTransform 1")
            assetOrientation = .right
            isPortrait = true
        } else if tfA == 0 && tfB == -1.0 && tfC == 1.0 && tfD == 0 {
            print("orientationFromTransform 2")
            assetOrientation = .left
            isPortrait = true
        } else if tfA == 1.0 && tfB == 0 && tfC == 0 && tfD == 1.0 {
            print("orientationFromTransform 3")
            assetOrientation = .up
        } else if tfA == -1.0 && tfB == 0 && tfC == 0 && tfD == -1.0 {
            print("orientationFromTransform 4")
            assetOrientation = .down
        }
        else if tfA == 0.0 && tfB == 1.0 && tfC == 1.0 && tfD == 0.0 {
            print("orientationFromTransform 5")
            assetOrientation = .rightMirrored
            isPortrait = true
        }
        else {
            print("orientationFromTransform else:",tfA, tfB, tfC, tfD)
        }
        return (assetOrientation, isPortrait)
    }
    
    func videoCompositionInstruction(
      _ track: AVCompositionTrack,
      asset: AVAsset, cameraType: String
    ) -> AVMutableVideoCompositionLayerInstruction {
        print("videoCompositionInstruction-----")
        let instruction = AVMutableVideoCompositionLayerInstruction(assetTrack: track)
        let assetTrack = asset.tracks(withMediaType: AVMediaType.video)[0]
        
        let transform = assetTrack.preferredTransform
        let assetInfo = orientationFromTransform(transform)
                
        var scaleToFitRatio = UIScreen.main.bounds.width / assetTrack.naturalSize.width
        if assetInfo.isPortrait {
            
            print("assetInfo.isPortrait")
            
            
            if cameraType == "front" {
                print("cameraType == front")
                //scaleToFitRatio = UIScreen.main.bounds.width / UIScreen.main.bounds.height
                let scaleFactor = CGAffineTransform(
                    scaleX: 0.5,
                    y: 0.65)
                
                /*let statusBarHeight = UIApplication.shared.statusBarFrame.height
                let transformY = 0 - (statusBarHeight * 1.4)*/
                let finalTransform = assetTrack.preferredTransform.concatenating(scaleFactor)//.translatedBy(x: self.view_camera.frame.size.height * 0.8, y: 0)
                instruction.setTransform(finalTransform.concatenating(CGAffineTransform(translationX: 0, y: UIScreen.main.bounds.height / 4)) , at: .zero)
            }
            else {
                scaleToFitRatio = UIScreen.main.bounds.width / assetTrack.naturalSize.height
                let scaleFactor = CGAffineTransform(
                    scaleX: 0.5,
                    y: 0.65)
                let finalTransform = assetTrack.preferredTransform.concatenating(scaleFactor)
                instruction.setTransform(finalTransform.concatenating(CGAffineTransform(translationX: 0, y: 0)) , at: .zero)
            }
            
            
            
        } else {
            print("assetInfo.isPortrait else")
            let scaleFactor = CGAffineTransform(
                scaleX: scaleToFitRatio,
                y: scaleToFitRatio)
            var concat = assetTrack.preferredTransform.concatenating(scaleFactor)
                .concatenating(CGAffineTransform(
                    translationX: 0,
                    y: UIScreen.main.bounds.width / 2))
            if assetInfo.orientation == .down {
                print("assetInfo.orientation == .down")
                let fixUpsideDown = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                let windowBounds = UIScreen.main.bounds
                let yFix = assetTrack.naturalSize.height + windowBounds.height
                let centerFix = CGAffineTransform(
                    translationX: assetTrack.naturalSize.width,
                    y: yFix)
                concat = fixUpsideDown.concatenating(centerFix).concatenating(scaleFactor)
            }
            instruction.setTransform(concat, at: .zero)
        }
        print("----videoCompositionInstruction")
        return instruction
    }
    
    func exportDidFinish(_ session: AVAssetExportSession) {
        print("exportDidFinish")
        // Cleanup assets
        audioAsset = nil
        
        self.lbl_audio.text = ""
        
        guard
            session.status == AVAssetExportSession.Status.completed,
            let outputURL = session.outputURL
            else {
                print("exportDidFinish session.status:",session.status,session.status.rawValue)
                return
        }
        if self.currentSpeed != .NORMAL {
            self.speedVideo(url: outputURL)
        }
        else {
            let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
            videoPlayerVC.videoURL = outputURL
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(videoPlayerVC, animated: true)
            }
        }
    }
    
    func startTimer() {
        self.recordTimer?.invalidate()
        self.recordTimer = nil
        
        self.recordingTime = 0
        self.lbl_timer.text = self.recordingTime.timeFormatted()
        self.lbl_timer.isHidden = false
        self.recordTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.countTimer), userInfo: nil, repeats: true)
    }
    
    @objc func countTimer(_ timer: Timer) {
        self.recordingTime += 1
        self.lbl_timer.text = self.recordingTime.timeFormatted()
        if self.recordingTime == self.captureTime {
            self.captureVideo()
        }
    }
    
    func invalidateTimer() {
        self.recordTimer?.invalidate()
        self.recordTimer = nil
        self.lbl_timer.isHidden = true
        self.progress_audio.isHidden = true
        self.displayLink?.invalidate()
        self.displayLink = nil
        self.progress_audio.progress = 0
        self.recordingTime = 0
        self.lbl_timer.text = self.recordingTime.timeFormatted()
    }
    
    func addCameraEffects() {
    }
    
    @objc func updateSlider() {
        self.progress_audio.progress = Float((audioPlayer?.currentTime().seconds ?? 0) / (self.audioAsset?.duration.seconds ?? 1))
    }
    
    func speedVideo(url: URL) {
        let videoAsset = AVURLAsset(url: url, options: nil)
        let mixComposition = AVMutableComposition()
        let compositionVideoTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        do {
            try compositionVideoTrack?.insertTimeRange(CMTimeRange(start: .zero, duration: videoAsset.duration), of: videoAsset.tracks(withMediaType: .video).first!, at: .zero)
        }
        catch {
            print("videoasset insert error:",error.localizedDescription)
        }
        
        let videoDuration = videoAsset.duration
        
        //slow down the video half speed (change multiplier to desired value"
        var finalTimeScale : Int64
        switch self.currentSpeed {
        case .SLOWER:
            finalTimeScale = videoDuration.value * 4
        case .SLOW:
            finalTimeScale = videoDuration.value * 2
        case .FAST:
            finalTimeScale = videoDuration.value / 2
        case .FASTER:
            finalTimeScale = videoDuration.value / 4
        default:
            finalTimeScale = videoDuration.value
        }
        
        compositionVideoTrack?.scaleTimeRange(CMTimeRange(start: .zero, duration: videoDuration), toDuration: CMTime(value: finalTimeScale, timescale: videoDuration.timescale))
        
        let documentURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let finalURL = documentURL.appendingPathComponent("speedVideo.mp4")
        
        do {
            try FileManager.default.removeItem(atPath: finalURL.path)
        }
        catch {
            print("file doesn't exist error:",error.localizedDescription)
        }
        
        if let audioTrackFirst = videoAsset.tracks(withMediaType: .audio).first {
            let audioTrack = mixComposition.addMutableTrack(
                withMediaType: .audio,
                preferredTrackID: kCMPersistentTrackID_Invalid)
            
            do {
                
                try audioTrack?.insertTimeRange(
                    CMTimeRangeMake(
                        start: CMTime.zero,
                        duration: videoAsset.duration),
                    of: /*savedVideoAsset.tracks(withMediaType: .audio)[0]*/audioTrackFirst,
                    at: .zero)
            } catch {
                print("Failed to load Audio track speed exception:",error.localizedDescription)
            }
            audioTrack?.scaleTimeRange(CMTimeRange(start: .zero, duration: videoDuration), toDuration: CMTime(value: finalTimeScale, timescale: videoDuration.timescale))
        }
        
        let exporter = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)
        exporter?.outputURL = finalURL
        exporter?.outputFileType = .mp4
        exporter?.exportAsynchronously(completionHandler: {
            switch exporter?.status {
            case .failed:
                print("exporter failed:",exporter?.status.rawValue)
                break
            case .cancelled:
                print("exporter cancelled:",exporter?.status.rawValue)
                break
            case .completed:
                print("exporter completed:",exporter?.status.rawValue)
                let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                videoPlayerVC.videoURL = finalURL
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(videoPlayerVC, animated: true)
                }
                break
            default:
                print("exporter default:",exporter?.status.rawValue)
            }
        })
    }

}

extension CameraVC: AudioListVCDelegate {
    //MARK:- AudioListVC Delegates...
    /*func selectedAudio(_ audio: AudioItem) {
        if let url = URL(string: audio.url ?? "") {
            self.audioAsset = AVAsset(url: url)
            self.lbl_audio.text = audio.name ?? ""
            self.lbl_audio.isHidden = false
            self.progress_audio.progress = 0
            self.progress_audio.isHidden = false
            if let asset = self.audioAsset {
                self.audioDuration = Int(asset.duration.seconds)
            }
            if let duration = audioDuration {
                captureTime = captureTime < duration ? captureTime : duration
            }
            print("selectedAudio audioAsset:",audioAsset)
        }
    }*/
    
    func selectedAudio(url: String?, id: String?, name: String?) {
        if let url = URL(string: url ?? "") {
            self.audioAsset = AVAsset(url: url)
            self.lbl_audio.text = name ?? ""
            self.lbl_audio.isHidden = false
            self.progress_audio.progress = 0
            self.progress_audio.isHidden = false
            if let asset = self.audioAsset {
                self.audioDuration = Int(asset.duration.seconds)
            }
            if let duration = audioDuration {
                captureTime = captureTime < duration ? captureTime : duration
            }
            print("selectedAudio audioAsset:",audioAsset)
        }
    }
}

extension CameraVC: AVCaptureVideoDataOutputSampleBufferDelegate {
    //MARK:- AVCaptureVideoDataOutputSampleBuffer Delegates...
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        /*guard let buffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            return
        }
        
        let ciImage = CIImage(cvImageBuffer: buffer)
        
        let uiImage = UIImage(ciImage: ciImage)
        
        DispatchQueue.main.async {
            
        }*/
        let timestamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer).seconds
        switch _captureState {
        case .start:
            // Set up recorder
            _filename = "output\(Int64(Date().timeIntervalSince1970))"//UUID().uuidString
            let videoPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("\(_filename).mp4")
            let writer = try! AVAssetWriter(outputURL: videoPath, fileType: .mp4)
            let settings = videoOutput.recommendedVideoSettingsForAssetWriter(writingTo: .mp4)
            let input = AVAssetWriterInput(mediaType: .video, outputSettings: settings) // [AVVideoCodecKey: AVVideoCodecType.h264, AVVideoWidthKey: 1920, AVVideoHeightKey: 1080])
            input.mediaTimeScale = CMTimeScale(bitPattern: 600)
            input.expectsMediaDataInRealTime = true
            input.transform = CGAffineTransform(rotationAngle: .pi/2)
            let adapter = AVAssetWriterInputPixelBufferAdaptor(assetWriterInput: input, sourcePixelBufferAttributes: nil)
            if writer.canAdd(input) {
                writer.add(input)
            }
            let audioInput = AVAssetWriterInput(mediaType: .audio, outputSettings: nil)
            if writer.canAdd(audioInput) {
                writer.add(audioInput)
            }
            writer.startWriting()
            writer.startSession(atSourceTime: .zero)
            _assetWriter = writer
            _assetWriterInput = input
            _adpater = adapter
            _captureState = .capturing
            _time = timestamp
        case .capturing:
            if _assetWriterInput?.isReadyForMoreMediaData == true {
                /*if self._effectType == .invert {
                    var newPixelBuffer : CVPixelBuffer? = nil
                    CVPixelBufferPoolCreatePixelBuffer(nil, (_adpater?.pixelBufferPool)!, &newPixelBuffer)
                    
                    let success = self._adpater?.append(newPixelBuffer!, withPresentationTime: self._currentSampleTime!)
                    
                    if success == false {
                        print("Pixel Buffer failed")
                    }
                }*/
                let time = CMTime(seconds: timestamp - _time, preferredTimescale: CMTimeScale(600))
                _adpater?.append(CMSampleBufferGetImageBuffer(sampleBuffer)!, withPresentationTime: time)
            }
            break
        case .end:
            print("end capture")
            self._captureState = .idle
            guard _assetWriterInput?.isReadyForMoreMediaData == true, _assetWriter!.status != .failed else { break }
            let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("\(_filename).mp4")
            _assetWriterInput?.markAsFinished()
            _assetWriter?.finishWriting { [weak self] in
                print("finished recording")
                self?._captureState = .idle
                self?._assetWriter = nil
                self?._assetWriterInput = nil
                var cameraType = "back"
                if self?.currentCamera == self?.backCamera {
                    cameraType = "back"
                }
                else {
                    cameraType = "front"
                }
                DispatchQueue.main.async {

                    self?.mergeVideoAudio(videoURL: url, cameraType: cameraType)
                    /*let player = AVPlayer(url: url)
                     let playerLayer = AVPlayerLayer(player: player)
                     playerLayer.frame = CGRect(x: 0, y: 0, width: 200, height: 300)
                     self?.view.layer.addSublayer(playerLayer)
                     player.play()*/
                }
            }
        default:
            break
        }
    }
}

extension CameraVC: AVCaptureFileOutputRecordingDelegate {
    //MARK:- AVCaptureFileOutputRecording Delegates...
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        if let err = error {
            print("didFinishRecordingTo error:",err.localizedDescription)
        }
        else {
            print("didFinishRecordingTo outputFileURL:",outputFileURL)
            if self.recordEndCorrectly {
                print("recordEndCorrectly:",recordEndCorrectly)
                //let videoAsset = AVAsset(url: outputFileURL)
                /*if let audio = self.audioAsset {
                    self.mergeVideoAudio(audioAsset: audio, videoURL: outputFileURL)
                }
                else {
                    let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                    videoPlayerVC.videoURL = outputFileURL
                    DispatchQueue.main.async {
                        self.navigationController?.pushViewController(videoPlayerVC, animated: true)
                    }
                }*/
                var cameraType = "back"
                if self.currentCamera == self.backCamera {
                    cameraType = "back"
                }
                else {
                    cameraType = "front"
                }
                self.mergeVideoAudio(videoURL: outputFileURL, cameraType: cameraType)
            }
        }
    }
}

extension CameraVC: MPMediaPickerControllerDelegate {
    //MARK:- MPMediaPickerController Delegates...
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        dismiss(animated: true) {
            let selectedSongs = mediaItemCollection.items
            guard let song = selectedSongs.first, let audioURL = song.value(forProperty: MPMediaItemPropertyAssetURL) as? URL else { return }
            print("song:",song,"audioURL:",audioURL)
            self.audioAsset = AVAsset(url: audioURL)
        }
    }
}

extension CameraVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIVideoEditorControllerDelegate {
    //MARK: ImagePicker Delegates...
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print("video captured info:",info)
        let url = info[UIImagePickerController.InfoKey.mediaURL] as? URL
        picker.dismiss(animated: true) {
            let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
            videoPlayerVC.videoURL = url
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(videoPlayerVC, animated: true)
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("imagePickerControllerDidCancel")
        picker.dismiss(animated: true, completion: nil)
    }
}

extension CameraVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    //MARK:- CollectionView Delegates...
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collection_options {
            return self.cameraOptionsArray.count
        }
        else if collectionView == self.collection_cameraTypes {
            return self.cameraTypesArray.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.collection_options {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CameraOptionsCVCell", for: indexPath) as! CameraOptionsCVCell
            
            cell.lbl_title.text = self.cameraOptionsArray[indexPath.item]
            
            if indexPath.item == 5 {
                if self.isFlashOn {
                    cell.img_option.image = #imageLiteral(resourceName: "ic_flashOff")
                }
                else {
                    cell.img_option.image = #imageLiteral(resourceName: "ic_flashOn")
                }
            }
            else {
                cell.img_option.image = self.cameraOptionsImagesArray[indexPath.item]
            }
            return cell
        }
        else if collectionView == self.collection_cameraTypes {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CameraTypeCVCell", for: indexPath) as! CameraTypeCVCell
            
            cell.lbl_title.text = self.cameraTypesArray[indexPath.item]
            
            if self.selectedCameraType == indexPath.item {
                cell.lbl_dot.isHidden = false
            }
            else {
                cell.lbl_dot.isHidden = true
            }
            
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.collection_options {
            return CGSize(width: 50, height: 65)
        }
        else if collectionView == self.collection_cameraTypes {
            let text =  self.cameraTypesArray[indexPath.item]
            let width = UILabel.textWidth(font: UIFont(name:"OpenSans-Regular",size:13)!, text: text)
            
            return CGSize(width: width + 60, height: 40)
        }
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == self.collection_options {
            return 0
        }
        else if collectionView == self.collection_cameraTypes {
            return 5
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == self.collection_options {
            return 0
        }
        else if collectionView == self.collection_cameraTypes {
            return 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == self.collection_options {
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        else if collectionView == self.collection_cameraTypes {
            return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        }
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.collection_options {
            switch indexPath.item {
            case 0:
                guard let input = self.videoInput else {
                    return
                }
                if self.currentCamera == self.backCamera {
                    if self.isFlashOn {
                        guard let device = AVCaptureDevice.default(for: .video) else { return }
                        guard device.hasTorch else { return }
                        do {
                            try device.lockForConfiguration()
                            
                            device.torchMode = .off
                            self.isFlashOn = false
                            device.unlockForConfiguration()
                        }
                        catch {
                            print("lockForConfiguration error : ",error.localizedDescription)
                        }
                    }
                    self.captureSession.removeInput(input)
                    guard let camera = self.frontCamera else {
                        return
                    }
                    do {
                        self.videoInput = try AVCaptureDeviceInput(device: camera)
                        self.currentCamera = camera
                        if let video = self.videoInput,  self.captureSession.canAddInput(video) {
                            self.captureSession.addInput(video)
                        }
                        self.mirrorFrontCamera()
                    }
                    catch {
                        print("camera error:",error.localizedDescription)
                    }
                }
                else {
                    self.captureSession.removeInput(input)
                    guard let camera = self.backCamera else {
                        return
                    }
                    do {
                        self.videoInput = try AVCaptureDeviceInput(device: camera)
                        self.currentCamera = camera
                        if let video = self.videoInput,  self.captureSession.canAddInput(video) {
                            self.captureSession.addInput(video)
                        }
                    }
                    catch {
                        print("camera error:",error.localizedDescription)
                    }
                }
                collectionView.reloadItems(at: [IndexPath(item: 5, section: 0)])
            case 5:
                /*if self.imagePicker.cameraDevice == .rear {
                 if self.imagePicker.cameraFlashMode == .off {
                 self.imagePicker.cameraFlashMode = .on
                 }
                 else {
                 self.imagePicker.cameraFlashMode = .off
                 }
                 collectionView.reloadItems(at: [indexPath])
                 }*/
                if self.currentCamera == self.backCamera {
                    guard let device = AVCaptureDevice.default(for: .video) else { return }
                    guard device.hasTorch else { return }
                    do {
                        try device.lockForConfiguration()
                        
                        if device.torchMode == .on {
                            device.torchMode = .off
                            self.isFlashOn = false
                            //sender.setImage(#imageLiteral(resourceName: "flash_off"), for: .normal)
                        }
                        else {
                            do {
                                try device.setTorchModeOn(level: 1.0)
                                self.isFlashOn = true
                                //sender.setImage(#imageLiteral(resourceName: "flash_on"), for: .normal)
                            }
                            catch {
                                print("setTorchModeOn error : ",error.localizedDescription)
                            }
                        }
                        device.unlockForConfiguration()
                        collectionView.reloadItems(at: [indexPath])
                    }
                    catch {
                        print("lockForConfiguration error : ",error.localizedDescription)
                    }
                }
            case 1:
                print("speed view")
                let speedVariationView = Bundle.main.loadNibNamed("SpeedVariationView", owner: nil, options: nil)![0] as! SpeedVariationView
                speedVariationView.delegate = self
                speedVariationView.selectedSpeed = self.currentSpeed
                self.view.addSubview(speedVariationView)
            default:
                break
            }
        }
        else if collectionView == self.collection_cameraTypes {
            if !self.movieFileOutput.isRecording {
                self.selectedCameraType = indexPath.item
                switch indexPath.item {
                case 1:
                    if let duration = self.audioDuration {
                        self.captureTime = self.captureTime < 60 ? self.captureTime : 60
                    }
                    else {
                        self.captureTime = 60
                    }
                default:
                    if let duration = self.audioDuration {
                        self.captureTime = self.captureTime < 15 ? self.captureTime : 15
                    }
                    else {
                        self.captureTime = 15
                    }
                }
                collectionView.reloadData()
            }
        }
    }
}

extension CameraVC: SpeedVariationViewDelegate {
    //MARK:- SpeedVariationView Delegates...
    func selectedSpeed(speedItem: SpeedItem) {
        self.currentSpeed = speedItem
    }
}

//MARK:- SPEEDITEM ENUM
public enum SpeedItem : Int {
    case SLOWER = 0
    case SLOW = 1
    case NORMAL = 2
    case FAST = 3
    case FASTER = 4
}
