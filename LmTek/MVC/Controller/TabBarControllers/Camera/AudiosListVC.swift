//
//  AudiosListVC.swift
//  LmTek
//
//  Created by PTBLR-1128 on 10/09/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import SwiftyJSON
import AVFoundation
import MediaPlayer
import Alamofire

protocol AudioListVCDelegate {
    //func selectedAudio(_ audio: AudioItem)
    func selectedAudio(url: String?, id: String?, name: String?)
}

class AudiosListVC: UIViewController {
    //MARK:- Outlets...
    @IBOutlet weak var tbl_audios : UITableView!
    @IBOutlet weak var txt_search : UITextField!
    @IBOutlet weak var btn_discover : UIButton!
    @IBOutlet weak var btn_myFavourites : UIButton!
    @IBOutlet var btns_audioList : [UIButton]!
    @IBOutlet weak var btn_localAudio : UIButton!
    
    //MARK:- Variables...
    var audiosArray = [AudioItem]()
    var favouriteAudiosArray = [FavouriteAudioItem]()
    //var filteredArray = [AudioItem]()
    var audioPlayer : AVPlayer?
    var playing_id = ""
    var isPlaying = false
    //var isSearching = false
    var delegate : AudioListVCDelegate? = nil
    var selectedAudioList = -1
    var isInitial = true
    
    var mediaPicker : MPMediaPickerController!
    var audioDuration: Double = 0.0
    
    
    //MARK:- View Life Cycle...
    override func viewDidLoad() {
        super.viewDidLoad()

        txt_search.attributedPlaceholder = NSAttributedString(
            string: "   Search Sound",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
        )
        txt_search.setLeftPaddingPoints(10)
        txt_search.setRightPaddingPoints(10)
        //self.getAudioList()
        //self.audioListSelected(self.btn_discover)
        self.cellRegistration()
        self.setUpMediaPicker()
        self.btn_localAudio.dropShadowWithRadius(x: 20)
        
    }
    
    override func viewDidLayoutSubviews() {
        if self.isInitial {
            self.isInitial = false
            self.audioListSelected(self.btn_discover)
        }
    }
    
    //MARK:- IBActions...
    @IBAction func backClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func textDidChange(_ textField : UITextField) {
        if let _ = self.audioPlayer {
            self.audioPlayer?.pause()
            self.audioPlayer = nil
            self.playing_id = ""
            self.isPlaying = false
        }
        if textField.text?.isEmpty == true {
            print("text is empty")
            //self.isSearching = false
            if self.selectedAudioList == 1 {
                self.audioListSelected(self.btn_discover)
            }
            else {
                //self.getAudioList()
                self.getAudioListAPI()
            }
        }
        else {
            //self.isSearching = true
            let audioName = (textField.text ?? "").lowercased()
            /*self.filteredArray = self.audiosArray.filter({ (audioItem) -> Bool in
                let name = (audioItem.name ?? "").lowercased()
                let caption = (audioItem.caption ?? "").lowercased()
                return name.contains(audioName) || caption.contains(audioName)
            })
            self.tbl_audios.reloadData()*/
            //self.searchAudio(audioName)
            self.searchAudioSongAPI(audioName)
        }
    }
    
    @IBAction func audioListSelected(_ sender: UIButton) {
        if sender.tag != self.selectedAudioList {
            if let _ = audioPlayer {
                self.audioPlayer?.pause()
                self.audioPlayer = nil
                self.playing_id = ""
            }
            self.changeButtonsColor(sender)
            //self.getAudioList()
            self.getAudioListAPI()
        }
    }
    
    @IBAction func localAudioButtonClicked() {
        self.present(self.mediaPicker, animated: true, completion: nil)
    }
    

    //MARK: Functions...
    func cellRegistration() {
        self.tbl_audios.register(UINib(nibName: "AudioTVCell", bundle: nil), forCellReuseIdentifier: "AudioTVCell")
        self.tbl_audios.rowHeight = UITableView.automaticDimension
        self.tbl_audios.estimatedRowHeight = 40
    }
    
    func changeButtonsColor(_ sender: UIButton) {
            for btn in self.btns_audioList {
                print("audioListSelected:",btn.tag)
                let layers = btn.layer.sublayers ?? []
                for i in 0 ..< layers.count {
                    let layer = layers[i]
                    if layer.isKind(of: CAGradientLayer.self) {
                        print("gradient layer",btn.tag)
                        btn.layer.sublayers?.remove(at: i)
                    }
                }
                if sender.tag == btn.tag {
                    print("if gradient")
                    btn.backgroundColor = colorYellow
                    btn.setTitleColor(UIColor.black, for: .normal)
                }
                else {
                    print("else gradient")
                    btn.setTitleColor(UIColor.lightGray, for: .normal)
                    btn.backgroundColor = .white
                }
            }
            self.selectedAudioList = sender.tag
            print("self.selectedAudioList:",self.selectedAudioList)
    }
    
    func setUpMediaPicker() {
        self.mediaPicker = MPMediaPickerController(mediaTypes: .any)
        self.mediaPicker.delegate = self
        self.mediaPicker.prompt = "Select audio"
    }

}

extension AudiosListVC : UITableViewDelegate, UITableViewDataSource {
    //MARK:- TableView Delegates...
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /*if self.isSearching {
            return self.filteredArray.count
        }*/
        if self.selectedAudioList == 0 {
            return self.audiosArray.count
        }
        else {
            return self.favouriteAudiosArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AudioTVCell") as! AudioTVCell

        if self.selectedAudioList == 0 {
            let model = self.audiosArray[indexPath.row]
            if self.playing_id == model.id && self.isPlaying {
                cell.btn_useAudio.isHidden = false
                cell.view_useAudio.isHidden = false
                cell.constraint_useAudioView.constant = 40
                cell.img_playPause.image = #imageLiteral(resourceName: "ic_pause_audio")
                if self.audioPlayer?.isPlaying == true {
                    UIView.animate(withDuration: audioDuration, delay: 2, options: [.curveEaseOut], animations: {
                        cell.lblBG.isHidden = false
                        cell.lblBG.frame.size.width = screenWidth
                    }, completion: nil)
                }

            } else {
                cell.lblBG.isHidden = true
                cell.btn_useAudio.isHidden = true
                cell.view_useAudio.isHidden = true
                cell.constraint_useAudioView.constant = 0
                cell.img_playPause.image = #imageLiteral(resourceName: "ic_play_audio")
            }
            cell.lbl_title.text = model.name ?? ""
            cell.lbl_subTitle.text = model.caption ?? ""
            
            if model.is_favourite == "1" {
                cell.btn_favourite.setImage(#imageLiteral(resourceName: "heartWhiteIcon").withRenderingMode(.alwaysTemplate), for: .normal)
                cell.btn_favourite.tintColor = colorYellow
            }
            else {
                cell.btn_favourite.setImage(#imageLiteral(resourceName: "heartWhiteIcon"), for: .normal)
            }
            
            cell.btn_useAudio.tag = indexPath.row
            cell.btn_useAudio.addTarget(self, action: #selector(useAudioClicked), for: .touchUpInside)
            
            cell.btn_favourite.tag = indexPath.row
            cell.btn_favourite.addTarget(self, action: #selector(makeFavouriteClicked), for: .touchUpInside)
        }
        else {
            let model = self.favouriteAudiosArray[indexPath.row]
            if self.playing_id == model.id && self.isPlaying {
                cell.btn_useAudio.isHidden = false
                cell.view_useAudio.isHidden = false
                cell.constraint_useAudioView.constant = 40
                cell.img_playPause.image = #imageLiteral(resourceName: "ic_pause_audio")
            }
            else {
                cell.btn_useAudio.isHidden = true
                cell.view_useAudio.isHidden = true
                cell.constraint_useAudioView.constant = 0
                cell.img_playPause.image = #imageLiteral(resourceName: "ic_play_audio")
            }
            
            cell.lbl_title.text = model.name ?? ""
            cell.lbl_subTitle.text = model.caption ?? ""
            
            cell.btn_favourite.setImage(#imageLiteral(resourceName: "sticker16"), for: .normal)
            
            cell.btn_useAudio.tag = indexPath.row
            cell.btn_useAudio.addTarget(self, action: #selector(useAudioClicked), for: .touchUpInside)
            
            cell.btn_favourite.tag = indexPath.row
            cell.btn_favourite.addTarget(self, action: #selector(makeFavouriteClicked), for: .touchUpInside)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell:AudioTVCell = tbl_audios.cellForRow(at: indexPath) as! AudioTVCell
        
        if self.selectedAudioList == 0 {
            let model = self.audiosArray[indexPath.row]
            if self.playing_id == model.id {
                if self.isPlaying {
                    self.isPlaying = false
                    self.audioPlayer?.pause()
                }
                else {
                    self.isPlaying = true
                    self.audioPlayer?.play()
                }
                tableView.reloadRows(at: [IndexPath(row: indexPath.row, section: 0)], with: .automatic)
            }
            else {
                if self.isPlaying {
                    self.audioPlayer?.pause()
                    self.audioPlayer = nil
                    self.isPlaying = false
                }
                if let url = URL(string: model.url ?? "") {
                    let audioAsset = AVURLAsset.init(url: url, options: nil)
                    let duration = audioAsset.duration
                    let durationInSeconds = CMTimeGetSeconds(duration)
                    audioDuration = durationInSeconds
                    print("audioDuration = ",audioDuration)
                    var indexPaths = [IndexPath]()
                    if let index = self.audiosArray.firstIndex(where: { $0.id == self.playing_id }) {
                        indexPaths.append(IndexPath(row: index, section: 0))
                    }
                    self.playing_id = model.id ?? ""
                    self.audioPlayer = AVPlayer(url: url)
                    self.audioPlayer?.play()
                    self.isPlaying = true
                    indexPaths.append(indexPath)
                    tableView.reloadRows(at: indexPaths, with: .automatic)
                    
                }
            }
        }
        else {
            let model = self.favouriteAudiosArray[indexPath.row]
            if self.playing_id == model.id {
                if self.isPlaying {
                    self.isPlaying = false
                    self.audioPlayer?.pause()
                }
                else {
                    self.isPlaying = true
                    self.audioPlayer?.play()
                }
                tableView.reloadRows(at: [IndexPath(row: indexPath.row, section: 0)], with: .automatic)
            }
            else {
                if self.isPlaying {
                    self.audioPlayer?.pause()
                    self.audioPlayer = nil
                    self.isPlaying = false
                }
                if let url = URL(string: model.url ?? "") {
                    var indexPaths = [IndexPath]()
                    if let index = self.audiosArray.firstIndex(where: { $0.id == self.playing_id }) {
                        indexPaths.append(IndexPath(row: index, section: 0))
                    }
                    self.playing_id = model.id ?? ""
                    self.audioPlayer = AVPlayer(url: url)
                    self.audioPlayer?.play()
                    self.isPlaying = true
                    indexPaths.append(indexPath)
                    tableView.reloadRows(at: indexPaths, with: .automatic)
                }
            }
        }
    }
    func durationOfAudio(_ fileURL : URL) -> CGFloat {
        do {
            let audioAsset = AVURLAsset.init(url: fileURL, options: nil)
            let duration = audioAsset.duration
            let durationInSeconds = CMTimeGetSeconds(duration)
            let floatDuartion: CGFloat = CGFloat(durationInSeconds)
            return floatDuartion
        } catch {
//            assertionFailure("Failed crating audio player: \(error).")
            return 0.0
        }
    }
    
    @objc func useAudioClicked(_ sender: UIButton) {
        if self.selectedAudioList == 0 {
            let model = self.audiosArray[sender.tag]
            self.delegate?.selectedAudio(url: model.url, id: model.id, name: model.name)
            self.navigationController?.popViewController(animated: true)
        }
        else {
            let model = self.favouriteAudiosArray[sender.tag]
            self.delegate?.selectedAudio(url: model.url, id: model.id, name: model.name)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func makeFavouriteClicked(_ sender: UIButton) {
        if self.selectedAudioList == 0 {
            let id = audiosArray[sender.tag].id
            self.makeAudioFavouriteAPI(id ?? "")
        }
        else {
            self.makeAudioFavouriteAPI(self.audiosArray[sender.tag].id ?? "")
        }
    }
}

extension AudiosListVC: MPMediaPickerControllerDelegate {
    //MARK:- MPMediaPickerController Delegates...
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        dismiss(animated: true) {
            let selectedSongs = mediaItemCollection.items
            guard let song = selectedSongs.first, let audioURL = song.value(forProperty: MPMediaItemPropertyAssetURL) as? URL, let title = song.title else { return }
            print("song:",song,"audioURL:",audioURL)
            //self.audioAsset = AVAsset(url: audioURL)
            self.delegate?.selectedAudio(url: audioURL.absoluteString, id: nil, name: title)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension AudiosListVC {
    //MARK:- API's...
    func getAudioListAPI(){
        if self.selectedAudioList == 0 {
            let url = URL(string: Constants.BASE_URL+Constants.GET_AUDIO_LIST)!
            let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
            DialougeUtils.addActivityView(view: self.view)
            self.getAudioListAPICall(url, header: headers)
        }else {
            let url = URL(string: Constants.BASE_URL+Constants.GET_FAVOURITE_AUDIO_LIST)!
            print(url)
            let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
            DialougeUtils.addActivityView(view: self.view)
            self.getFavoriteListAPICall(url, header: headers)
            
        }
    }
    
    func getAudioListAPICall(_ url: URL, header: HTTPHeaders){
        LoginSignUpAPI.getUserDataAPI(vc: self, url: url, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            if self.audiosArray.count > 0 {
                                self.audiosArray.removeAll()
                                self.favouriteAudiosArray.removeAll()
                            }
                            self.audiosArray = AudioModel().decodingAudios(result!["user_data"] as? [[String :Any]] ?? [])
                            print("getAudioList audiosArray:",self.audiosArray)
                            DispatchQueue.main.async {
                                self.tbl_audios.reloadData()
                            }
                        }else{
                            if self.audiosArray.count > 0 {
                                self.audiosArray.removeAll()
                                self.favouriteAudiosArray.removeAll()
                            }
                            DispatchQueue.main.async {
                                self.tbl_audios.reloadData()
                            }

                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                }
            }
        }
    }
    
    func getFavoriteListAPICall(_ url: URL, header: HTTPHeaders){
        LoginSignUpAPI.getUserDataAPI(vc: self, url: url, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            if self.audiosArray.count > 0 {
                                self.audiosArray.removeAll()
                                self.favouriteAudiosArray.removeAll()
                            }
                            self.favouriteAudiosArray = AudioModel().decodingFavouriteAudioItems(result!["user_data"] as? [[String : Any]] ?? [])
                            DispatchQueue.main.async {
                                self.tbl_audios.reloadData()
                            }
                        }else{
                            if self.audiosArray.count > 0 {
                                self.audiosArray.removeAll()
                                self.favouriteAudiosArray.removeAll()
                            }
                            DispatchQueue.main.async {
                                self.tbl_audios.reloadData()
                            }

                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                }
            }
        }
    }
    func makeAudioFavouriteAPI(_ sound_id: String){
        let param: Parameters = ["sound_id": sound_id]
        let url = URL(string: Constants.BASE_URL+Constants.MAKE_AUDIO_FAVOURITE)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        DialougeUtils.addActivityView(view: self.view)
        self.makeAudioFavouriteAPICall(param, url: url, header: headers,audio_id: sound_id)
    }
    
    func makeAudioFavouriteAPICall(_ param: Parameters, url: URL, header: HTTPHeaders,audio_id: String){
        AllUserApis.allPostDataWithoutImageAPI(vc: self, url: url, parameter: param, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status: Bool = result!["status"] as! Bool
                        if status == true{
                            if self.playing_id == audio_id {
                                self.audioPlayer?.pause()
                                self.audioPlayer = nil
                                self.playing_id = ""
                            }
                        self.getAudioListAPI()
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                }
            }
        }
    }

    func searchAudioSongAPI(_ search: String){
        let param: Parameters = ["search_key": search]
        let url = URL(string: Constants.BASE_URL+Constants.SEARCH_AUDIO)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        DialougeUtils.addActivityView(view: self.view)
        self.searchSongAPICall(param, url: url, header: headers)
    }
    
    func searchSongAPICall(_ param: Parameters, url: URL, header: HTTPHeaders){
        AllUserApis.allPostDataWithoutImageAPI(vc: self, url: url, parameter: param, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status: Bool = result!["status"] as! Bool
                        if status == true{
                            self.changeButtonsColor(self.btn_discover)
                            self.audiosArray = AudioModel().decodingAudios(result!["data"] as? [[String : Any]] ?? [])
                            DispatchQueue.main.async {
                                self.tbl_audios.reloadData()
                            }
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                }
            }
        }
    }

}
