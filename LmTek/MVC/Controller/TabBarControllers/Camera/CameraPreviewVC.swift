//
//  CameraPreviewVC.swift
//  LmTek
//
//  Created by PTBLR-1128 on 17/11/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer
import GLKit

class CameraPreviewVC: UIViewController {
    //MARK:- Outlets...
    @IBOutlet weak var view_camera : UIView!
    @IBOutlet weak var collection_options : UICollectionView!
    @IBOutlet weak var img_audio : UIImageView!
    @IBOutlet weak var view_upload : UIView!
    @IBOutlet weak var collection_cameraTypes : UICollectionView!
    @IBOutlet weak var lbl_timer : UILabel!
    @IBOutlet weak var lbl_audio : UILabel!
    @IBOutlet weak var progress_audio : UIProgressView!
    @IBOutlet weak var progress_record : UIProgressView!
    @IBOutlet weak var view_effects : UIView!
    @IBOutlet weak var view_uploadBackground : UIView!
    @IBOutlet weak var lbl_shutterTime : UILabel!
    @IBOutlet weak var img_camera : UIImageView!
    
    //MARK:- Variables...
    var cameraOptionsArray = ["Flip", "Speed", "Beauty", "Filters", "Timer", "Flash"]
    var cameraOptionsImagesArray = [#imageLiteral(resourceName: "ic_flipWhite"), #imageLiteral(resourceName: "ic_speedOn"), #imageLiteral(resourceName: "ic_beautyWhite"), #imageLiteral(resourceName: "ic_filterWhite"), #imageLiteral(resourceName: "ic_timerWhite")]
    var cameraTypesArray = ["Live", "60s", "15s", "Templates"]
    var selectedCameraType = 2
    
    var isCapturing = false
    
    var captureSession = AVCaptureSession()
    var backCamera: AVCaptureDevice?
    var frontCamera: AVCaptureDevice?
    var currentCamera: AVCaptureDevice?
    var videoInput : AVCaptureDeviceInput?//AVCaptureInput?
    var audioInput : AVCaptureDeviceInput?
    
    var previewLayer = AVCaptureVideoPreviewLayer()
    
    var isInitial = true
    var isFlashOn = false
    
    //var movieFileOutput = AVCaptureMovieFileOutput()
    
    var recordEndCorrectly = true
    
    var mediaPicker : MPMediaPickerController!
    var audioAsset : AVAsset?
    var audioPlayer : AVPlayer?
    
    var recordTimer : Timer?
    var recordingTime = 0.0
    var captureTime = 15.0
    var audioDuration : Double?//Int?
    var displayLink : CADisplayLink?
        
    var currentSpeed : SpeedItem = .NORMAL
    
    var shutterTimer : Timer?
    var shutterDuration = 0
    var timerDuration : Double?
    
    var assetWriter: AVAssetWriter?
    var assetWriterPixelBufferInput: AVAssetWriterInputPixelBufferAdaptor?
    let videoOutput = AVCaptureVideoDataOutput()
    var isWriting = false
    var currentSampleTime: CMTime?
    var currentVideoDimensions: CMVideoDimensions?
    var filter: CIFilter?// = CIFilter(name: "CIColorMonochrome")!
    var assetWriterAudioInput : AVAssetWriterInput?
    
    var videoWriter: VideoWriter?
    //MARK: helper recording
    private var cvPixelBuffer: CVPixelBuffer?
    private var orientation = UIInterfaceOrientation.portrait
    private var deviceZoomFactor: CGFloat = 1.0
    
    // MARK: Video Preview Objects
    private var videoPreviewView: GLKView?
    private var ciContext: CIContext?
    private var glContext: EAGLContext?
    private var videoPreviewViewBounds: CGRect?
    
    //MARK: View Life Cycle...

    override func viewDidLoad() {
        super.viewDidLoad()

        self.cellRegistration()
        /*setupDevice()
        setupInputOutput()*/
        //setUpMediaPicker()
                
        /*let audioImage = #imageLiteral(resourceName: "ic_audio").withRenderingMode(.alwaysTemplate)
        self.img_audio.image = audioImage
        self.img_audio.tintColor = UIColor.white*/
        
        //self.view_upload.setGradient(colors: [UIColor(hexString: Constants.DARK_BLUE_COLOR)?.cgColor ?? UIColor.blue.cgColor, UIColor(hexString: Constants.LIGHT_BLUE_COLOR)?.cgColor ?? UIColor.blue.cgColor], startPoint: CGPoint(x: 0, y: 1), endPoint: CGPoint(x: 1, y: 0))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !self.captureSession.isRunning {
            self.captureSession.startRunning()
        }
        self.progress_record.progress = 0
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.captureSession.isRunning {
            self.captureSession.stopRunning()
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        print("viewDidLayoutSubviews")
        if isInitial {
            print("isInitial")
            self.isInitial = false
            glContext = EAGLContext(api: .openGLES2)
            if let eaglContext = glContext {
                videoPreviewView = GLKView(frame: self.view_camera.bounds, context: eaglContext)
                ciContext = CIContext(eaglContext: eaglContext)
            }
            if let videoPreviewView = videoPreviewView {
                videoPreviewView.enableSetNeedsDisplay = false
                videoPreviewView.frame = self.view.bounds
                videoPreviewView.isUserInteractionEnabled = false
                
                self.view_camera.addSubview(videoPreviewView)
                self.view_camera.sendSubviewToBack(videoPreviewView)
            }
            orientation = UIApplication.shared.statusBarOrientation
            print("orientation:",orientation)
            resizePreviewView()
            setupDevice()
            setupInputOutput()
        }
    }
    
    func resizePreviewView() {
        guard let videoPreviewView =  videoPreviewView else {
            print("can't resize preview vide")
            return
        }
        
        videoPreviewView.frame = self.view.bounds
        videoPreviewView.bindDrawable()
        videoPreviewViewBounds = CGRect.zero
        videoPreviewViewBounds?.size.width = self.view.bounds.width * videoPreviewView.contentScaleFactor
        videoPreviewViewBounds?.size.height = self.view.bounds.height * videoPreviewView.contentScaleFactor
    }
    

    //MARK:- IBActions...
    @IBAction func backClicked() {
        /*if self.movieFileOutput.isRecording {
            let alertController = UIAlertController(title: "Alert", message: "Going back removes the saved video.\nDo you want to delete?", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
                self.recordEndCorrectly = false
                self.invalidateTimer()
                self.movieFileOutput.stopRecording()
                self.audioAsset = nil
                self.lbl_audio.text = "Sounds"
                self.audioPlayer?.pause()
                self.audioPlayer = nil
                self.currentSpeed = .NORMAL
                self.tabBarController?.selectedIndex = Constants.previousTab
                //if let tabBar = self.tabBarController?.tabBar, let items = tabBar.items {
                self.navigationController?.popViewController(animated: false)
                //}
            }))
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
        else {
            self.lbl_timer.isHidden = true
            self.progress_audio.isHidden = true
            self.progress_audio.progress = 0
            self.audioAsset = nil
            self.lbl_audio.text = "Sounds"
            self.currentSpeed = .NORMAL
            self.tabBarController?.selectedIndex = Constants.previousTab
            //if let tabBar = self.tabBarController?.tabBar, let items = tabBar.items {
            self.navigationController?.popViewController(animated: false)
            //}
        }*/
        if self.isWriting {
            let alertController = UIAlertController(title: "Alert", message: "Going back removes the saved video.\nDo you want to delete?", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
                self.recordEndCorrectly = false
                self.invalidateTimer()
                //self.movieFileOutput.stopRecording()
                self.assetWriter?.cancelWriting()
                self.audioAsset = nil
                self.lbl_audio.text = "Sounds"
                self.audioPlayer?.pause()
                self.audioPlayer = nil
                self.currentSpeed = .NORMAL
                self.tabBarController?.selectedIndex = Constants.previousTab
                //if let tabBar = self.tabBarController?.tabBar, let items = tabBar.items {
                self.navigationController?.popViewController(animated: false)
                //}
            }))
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
        else {
            self.lbl_timer.isHidden = true
            self.progress_audio.isHidden = true
            self.progress_audio.progress = 0
            self.audioAsset = nil
            self.lbl_audio.text = "Sounds"
            self.currentSpeed = .NORMAL
            self.tabBarController?.selectedIndex = Constants.previousTab
            //if let tabBar = self.tabBarController?.tabBar, let items = tabBar.items {
            self.navigationController?.popViewController(animated: false)
            //}
        }
    }
    
    @IBAction func captureVideo() {
        /*if self.movieFileOutput.isRecording {
            print("recording")
            self.recordEndCorrectly = true
            self.invalidateTimer()
            self.audioPlayer?.pause()
            self.audioPlayer = nil
            self.movieFileOutput.stopRecording()
            self.collection_options.isHidden = false
            self.collection_cameraTypes.isHidden = false
            self.view_effects.isHidden = false
            self.view_upload.isHidden = false
        }
        else {
            print("not recording")
            //let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            //let fileURL = paths[0].appendingPathComponent("output\(Int64(Date().timeIntervalSince1970)).mp4")//mov")
            //do {
            try? FileManager.default.removeItem(at: movieURL() as URL)//fileURL)
            if let asset = self.audioAsset {
                self.audioPlayer = nil
                let playerItem = AVPlayerItem(asset: asset)
                self.audioPlayer = AVPlayer(playerItem: playerItem)
                self.audioPlayer?.play()
                self.displayLink = CADisplayLink(target: self, selector: #selector(self.updateSlider))
                self.displayLink?.preferredFramesPerSecond = 10
                self.progress_audio.progress = 0
                self.progress_audio.isHidden = false
                self.displayLink?.add(to: .current, forMode: .default)
                
            }
            self.collection_options.isHidden = true
            self.collection_cameraTypes.isHidden = true
            self.view_effects.isHidden = true
            self.view_upload.isHidden = true
            self.movieFileOutput.startRecording(to: /*fileURL*/movieURL() as URL, recordingDelegate: self)
            self.startTimer()
            
        }*/
         
        /*if isWriting {
            print("stop record")
            self.isWriting = false
            assetWriterPixelBufferInput = nil
            
            self.invalidateTimer()
            self.audioPlayer?.pause()
            self.audioPlayer = nil
            self.collection_options.isHidden = false
            self.collection_cameraTypes.isHidden = false
            self.view_effects.isHidden = false
            self.view_upload.isHidden = false
            
            assetWriter?.finishWriting(completionHandler: {[unowned self] () -> Void in
                //self.saveMovieToCameraRoll()
                /*let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                 videoPlayerVC.videoURL = self.assetWriter?.outputURL
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(videoPlayerVC, animated: true)
                }*/
                
                guard let url = self.assetWriter?.outputURL else { print("output url is nil"); return }
                let asset = AVAsset(url: url)
                let errors = Error.self
                
                let item = AVPlayerItem(asset: asset)
                var videoComposition : AVVideoComposition
                //item.videoComposition = AVVideoComposition(asset: asset,  applyingCIFiltersWithHandler: { [weak self] request in
                if let ciFilter = self.filter {
                    videoComposition = AVVideoComposition(asset: asset,  applyingCIFiltersWithHandler: { [weak self] request in
                        guard let self = self else {
                            //request.finish(with: errors.self)
                            return
                        }
                        let source = request.sourceImage.clampedToExtent()
                        ciFilter.setValue(source, forKey: kCIInputImageKey)
                        
                        let output = ciFilter.outputImage?.cropped(to: request.sourceImage.extent)
                        
                        request.finish(with: output!, context: nil)
                    })
                }
                else {
                    videoComposition = AVVideoComposition(propertiesOf: asset)
                }
                
                item.videoComposition = videoComposition
                let mutableComposition = AVMutableComposition()
                if let videoTrack = mutableComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid) {
                    do {
                        try videoTrack.insertTimeRange(CMTimeRange(start: .zero, duration: asset.duration), of: asset.tracks(withMediaType: .video)[0], at: .zero)
                    }
                    catch {
                        print("Failed to load video track in capture video")
                    }
                }
                if let loadedAudioAsset = self.audioAsset {
                    let audioTrack = mutableComposition.addMutableTrack(
                    withMediaType: .audio,
                    preferredTrackID: 0)
                  do {
                    try audioTrack?.insertTimeRange(
                      CMTimeRangeMake(
                        start: CMTime.zero,
                        duration: asset.duration),
                      of: loadedAudioAsset.tracks(withMediaType: .audio)[0],
                      at: .zero)
                  } catch {
                    print("Failed to load Audio track in capture video")
                  }
                }
                else if let audioTrackFirst = asset.tracks(withMediaType: .audio).first {
                    print("natural audio")
                    let audioTrack = mutableComposition.addMutableTrack(
                        withMediaType: .audio,
                        preferredTrackID: kCMPersistentTrackID_Invalid)
                    
                    do {
                        
                        try audioTrack?.insertTimeRange(
                            CMTimeRangeMake(
                                start: CMTime.zero,
                                duration: asset.duration),
                            of: audioTrackFirst,
                            at: .zero)
                    } catch {
                        print("Failed to load Audio track")
                    }
                }
                else {
                    print("audio track is not there in the video")
                }
                
                // self.player.replaceCurrentItem(with: item)
                
                let exporter = AVAssetExportSession(asset: /*item.asset*/mutableComposition, presetName: AVAssetExportPresetMediumQuality)//AVAssetExportPresetHighestQuality)
                exporter?.videoComposition = videoComposition
                //exporter?.timeRange = CMTimeRange(start: CMTime(seconds: 1, preferredTimescale: 1), duration: asset.duration)
                
                exporter?.outputFileType = .mp4
                let filename = "filename.mp4"
                
                let documentsDirectory = FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).last!
                let outputURL = documentsDirectory.appendingPathComponent(filename)
                try? FileManager.default.removeItem(at: outputURL)
                exporter?.outputURL = outputURL
                exporter?.exportAsynchronously(completionHandler: {
                    guard exporter?.status == .completed else {
                        print("export failed: \(exporter?.error)")
                        return
                    }
                    print("done: ",outputURL)
                    
                    DispatchQueue.main.async {
                        self.captureSession.stopRunning()
                        //try? FileManager.default.removeItem(at: url)
                        let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                        videoPlayerVC.videoURL = outputURL//self.assetWriter?.outputURL
                        
                        self.navigationController?.pushViewController(videoPlayerVC, animated: true)
                    }
                })
                
            })
        }
        else {
            print("start record")
            DispatchQueue.main.async {
                
                //let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
                //let fileURL = paths[0].appendingPathComponent("output\(Int64(Date().timeIntervalSince1970)).mp4")//mov")
                //do {
                //try? FileManager.default.removeItem(at: fileURL)
                if let asset = self.audioAsset {
                    self.audioPlayer = nil
                    let playerItem = AVPlayerItem(asset: asset)
                    self.audioPlayer = AVPlayer(playerItem: playerItem)
                    self.audioPlayer?.play()
                    self.displayLink = CADisplayLink(target: self, selector: #selector(self.updateSlider))
                    self.displayLink?.preferredFramesPerSecond = 10
                    self.progress_audio.progress = 0
                    self.progress_audio.isHidden = false
                    self.displayLink?.add(to: .current, forMode: .default)
                    
                }
                self.collection_options.isHidden = true
                self.collection_cameraTypes.isHidden = true
                self.view_effects.isHidden = true
                self.view_upload.isHidden = true
                self.startTimer()
                
                self.createWriter()
                self.assetWriter?.startWriting()
                self.assetWriter?.startSession(atSourceTime: self.currentSampleTime!)
                self.isWriting = true
            }
        }*/
        
        if isCapturing {
            print("capturing")
            stopRecording()
        } else {
            print("not capturing")
            startRecording()
        }
        
    }
    
    @IBAction func musicClicked() {
        let audiosListVC = self.storyboard?.instantiateViewController(withIdentifier: "AudiosListVC") as! AudiosListVC
        audiosListVC.delegate = self
        self.navigationController?.pushViewController(audiosListVC, animated: true)
    }
    
    @IBAction func galleryClicked() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = ["public.movie"]
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func pinchToZoom(_ sender: UIPinchGestureRecognizer) {
        guard let device = self.currentCamera else {
            return
        }
        if sender.state == .changed {
            let maxZoomFactor = device.activeFormat.videoMaxZoomFactor
            let pinchVelocityDividerFactor : CGFloat = 2.0
            
            do {
                try device.lockForConfiguration()
                defer {
                    device.unlockForConfiguration()
                }
                
                let desiredZoomFactor = device.videoZoomFactor + atan2(sender.velocity, pinchVelocityDividerFactor)
                device.videoZoomFactor = max(1.0, min(desiredZoomFactor, maxZoomFactor))
                
            }
            catch {
                print("device lockconfiguration exception:",error.localizedDescription)
            }
        }
    }
    
    @IBAction func singleTapGesture(tap: UITapGestureRecognizer) {
        /*guard tapToFocus == true else {
            // Ignore taps
            return
        }*/

        let screenSize = self.view_camera.bounds.size
        let tapPoint = tap.location(in: self.view_camera)
        let x = tapPoint.y / screenSize.height
        let y = 1.0 - tapPoint.x / screenSize.width
        let focusPoint = CGPoint(x: x, y: y)

        if let device = currentCamera {
            do {
                try device.lockForConfiguration()

                if device.isFocusPointOfInterestSupported == true {
                    device.focusPointOfInterest = focusPoint
                    device.focusMode = .autoFocus
                }
                device.exposurePointOfInterest = focusPoint
                device.exposureMode = AVCaptureDevice.ExposureMode.continuousAutoExposure
                device.unlockForConfiguration()
                //Call delegate function and pass in the location of the touch

                DispatchQueue.main.async {
                    self.focusAnimationAt(tapPoint)
                }
            }
            catch {
                // just ignore
            }
        }
    }
    
    //MARK:- Functions...
    
    func cellRegistration() {
        self.collection_options.register(UINib(nibName: "CameraOptionsCVCell", bundle: nil), forCellWithReuseIdentifier: "CameraOptionsCVCell")
        self.collection_cameraTypes.register(UINib(nibName: "CameraTypeCVCell", bundle: nil), forCellWithReuseIdentifier: "CameraTypeCVCell")
    }
    
    func setupDevice() {
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
        let devices = deviceDiscoverySession.devices
        
        for device in devices {
            if device.position == AVCaptureDevice.Position.back {
                do {
                    try device.lockForConfiguration()
                    //device.automaticallyEnablesLowLightBoostWhenAvailable = true
                    //device.whiteBalanceMode = .continuousAutoWhiteBalance
                    device.exposureMode = .continuousAutoExposure
                    device.unlockForConfiguration()
                }
                catch {
                    print("setupDevice lockForConfigurationException:",error.localizedDescription)
                }
                backCamera = device
            }
            else if device.position == AVCaptureDevice.Position.front {
                do {
                    try device.lockForConfiguration()
                    //device.automaticallyEnablesLowLightBoostWhenAvailable = true
                    //device.whiteBalanceMode = .continuousAutoWhiteBalance
                    device.exposureMode = .continuousAutoExposure
                    device.unlockForConfiguration()
                }
                catch {
                    print("setupDevice lockForConfigurationException:",error.localizedDescription)
                }
                frontCamera = device
            }
        }
        
        currentCamera = frontCamera
    }
    
    func setupInputOutput() {
        do {
            guard let camera = currentCamera else {
                return
            }
            self.videoInput = try AVCaptureDeviceInput(device: camera)
            self.captureSession.sessionPreset = .high//.photo//.high
            self.captureSession.beginConfiguration()
            if let input = self.videoInput, captureSession.canAddInput(input) {
                captureSession.addInput(input)
            }
            
            guard let audioDevice = AVCaptureDevice.default(for: .audio) else { return }
            
            let audioInput = try AVCaptureDeviceInput(device: audioDevice)
            if captureSession.canAddInput(audioInput) {
                captureSession.addInput(audioInput)
            }
            
            let outputSettings: [String: Any] = [kCVPixelBufferPixelFormatTypeKey as String: kCVPixelFormatType_32BGRA]
            videoOutput.videoSettings = outputSettings
            let videoSessionQueue = DispatchQueue(label: Constants.QueueLabels.VideoSessionQueue)
            videoOutput.setSampleBufferDelegate(self, queue: videoSessionQueue)//DispatchQueue.main)
            videoOutput.alwaysDiscardsLateVideoFrames = true
            if captureSession.canAddOutput(videoOutput) {
                captureSession.addOutput(videoOutput)
            }
            else {
                print("unable to add videoOutput")
            }
            
            let audioOutput = AVCaptureAudioDataOutput()
            let audioSessionQueue = DispatchQueue(label: Constants.QueueLabels.AudioSessionQueue)
            audioOutput.setSampleBufferDelegate(self, queue: audioSessionQueue)//DispatchQueue.main)
            if captureSession.canAddOutput(audioOutput) {
                captureSession.addOutput(audioOutput)
            }
            
            /*if self.captureSession.canAddOutput(self.movieFileOutput) {
                self.captureSession.addOutput(self.movieFileOutput)
            }
            else {
                print("unable to add movieFileOutput")
            }*/
            
            //self.mirrorFrontCamera()
            self.captureSession.commitConfiguration()
            //videoPreviewView?.transform = (videoPreviewView?.transform.scaledBy(x: -1, y: 1))!
            self.captureSession.startRunning()
        } catch {
            print(error)
        }
    }
    
    func mirrorFrontCamera() {
        print("mirrorFrontCamera")
        guard let currentCameraInput = captureSession.inputs.first as? AVCaptureDeviceInput else {
            return
        }
        /*if let conn = self.videoOutput.connection(with: .video) {
            conn.isVideoMirrored = true
        }*/
        /*if let conn = self.movieFileOutput.connection(with: .video) {
            conn.isVideoMirrored = true
        }*/
    }
    
    func setUpMediaPicker() {
        self.mediaPicker = MPMediaPickerController(mediaTypes: .any)
        self.mediaPicker.delegate = self
        self.mediaPicker.prompt = "Select audio"
    }
    
    func mergeVideoAudio(videoURL: URL, cameraType: String) {
        print("mergeVideoAudio")
        let videoAsset = AVAsset(url: videoURL)
        let mixComposition = AVMutableComposition()

        // 2 - Create two video tracks
        guard
          let videoTrack = mixComposition.addMutableTrack(
            withMediaType: .video,
            preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
          else { return }
        print("guard videoTrack")

        do {
            print("videoAsset.tracks(withMediaType: .video):",videoAsset.tracks(withMediaType: .video))
          try videoTrack.insertTimeRange(
            CMTimeRangeMake(start: .zero, duration: videoAsset.duration),
            of: videoAsset.tracks(withMediaType: .video)[0],
            at: .zero)
        } catch {
          print("Failed to load first track")
          return
        }

        if cameraType == "back" {
            
        }
        else {
            
        }

        // 3 - Composition Instructions
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRangeMake(
          start: .zero,
          duration: videoAsset.duration)
        print("mainInstruction")

        // 4 - Set up the instructions — one for each asset
        let videoInstruction = self.videoCompositionInstruction(
          videoTrack,
          asset: videoAsset, cameraType: cameraType)
        videoInstruction.setOpacity(0.0, at: videoAsset.duration)
        print("videoInstruction")

        // 5 - Add all instructions together and create a mutable video composition
        mainInstruction.layerInstructions = [videoInstruction]
        let mainComposition = AVMutableVideoComposition()
        mainComposition.instructions = [mainInstruction]
        mainComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        let width = UIScreen.main.bounds.width
        let compositionWidth = (Int(width) % 2 == 0) ? width : width - 1
        mainComposition.renderSize = CGSize(
          width: /*UIScreen.main.bounds.width*/compositionWidth,
          height: UIScreen.main.bounds.height)
        print("mainComposition")
        // 6 - Audio track
        if let loadedAudioAsset = self.audioAsset {
          let audioTrack = mixComposition.addMutableTrack(
            withMediaType: .audio,
            preferredTrackID: 0)
          do {
            try audioTrack?.insertTimeRange(
              CMTimeRangeMake(
                start: CMTime.zero,
                duration: videoAsset.duration),
              of: loadedAudioAsset.tracks(withMediaType: .audio)[0],
              at: .zero)
          } catch {
            print("Failed to load Audio track")
          }
        }
        else if let audioTrackFirst = videoAsset.tracks(withMediaType: .audio).first {
            print("natural audio")
            let audioTrack = mixComposition.addMutableTrack(
                withMediaType: .audio,
                preferredTrackID: kCMPersistentTrackID_Invalid)
            
            do {
                
                try audioTrack?.insertTimeRange(
                    CMTimeRangeMake(
                        start: CMTime.zero,
                        duration: videoAsset.duration),
                    of: audioTrackFirst,
                    at: .zero)
            } catch {
                print("Failed to load Audio track")
            }
        }

        // 7 - Get path
        guard
          let documentDirectory = FileManager.default.urls(
            for: .documentDirectory,
            in: .userDomainMask).first
          else { return }
        print("guard documentDirectory")
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .short
        let date = dateFormatter.string(from: Date())
        let url = documentDirectory.appendingPathComponent("mergeVideo-\(date).mp4")

        // 8 - Create Exporter
        guard let exporter = AVAssetExportSession(
          asset: mixComposition,
          presetName: AVAssetExportPresetHighestQuality)
          else { return }
        print("guard exporter")
        exporter.outputURL = url
        exporter.outputFileType = AVFileType.mp4
        exporter.shouldOptimizeForNetworkUse = true
        exporter.videoComposition = mainComposition

        // 9 - Perform the Export
        exporter.exportAsynchronously {
            print("exporter.exportAsynchronously")
            DispatchQueue.main.async {
                self.exportDidFinish(exporter)
            }
        }
    }
    
    func orientationFromTransform(
      _ transform: CGAffineTransform
    ) -> (orientation: UIImage.Orientation, isPortrait: Bool) {
        var assetOrientation = UIImage.Orientation.up
        var isPortrait = false
        let tfA = transform.a
        let tfB = transform.b
        let tfC = transform.c
        let tfD = transform.d
        
        if tfA == 0 && tfB == 1.0 && tfC == -1.0 && tfD == 0 {
            print("orientationFromTransform 1")
            assetOrientation = .right
            isPortrait = true
        } else if tfA == 0 && tfB == -1.0 && tfC == 1.0 && tfD == 0 {
            print("orientationFromTransform 2")
            assetOrientation = .left
            isPortrait = true
        } else if tfA == 1.0 && tfB == 0 && tfC == 0 && tfD == 1.0 {
            print("orientationFromTransform 3")
            assetOrientation = .up
        } else if tfA == -1.0 && tfB == 0 && tfC == 0 && tfD == -1.0 {
            print("orientationFromTransform 4")
            assetOrientation = .down
        }
        else if tfA == 0.0 && tfB == 1.0 && tfC == 1.0 && tfD == 0.0 {
            print("orientationFromTransform 5")
            assetOrientation = .rightMirrored
            isPortrait = true
        }
        else {
            print("orientationFromTransform else:",tfA, tfB, tfC, tfD)
        }
        return (assetOrientation, isPortrait)
    }
    
    func videoCompositionInstruction(
      _ track: AVCompositionTrack,
      asset: AVAsset, cameraType: String
    ) -> AVMutableVideoCompositionLayerInstruction {
        print("videoCompositionInstruction-----")
        let instruction = AVMutableVideoCompositionLayerInstruction(assetTrack: track)
        let assetTrack = asset.tracks(withMediaType: AVMediaType.video)[0]
        
        let transform = assetTrack.preferredTransform
        let assetInfo = orientationFromTransform(transform)
                
        var scaleToFitRatio = UIScreen.main.bounds.width / assetTrack.naturalSize.width
        if assetInfo.isPortrait {
            
            print("assetInfo.isPortrait")
            
            if cameraType == "front" {
                print("cameraType == front")
                let scaleFactor = CGAffineTransform(
                    scaleX: 0.55,
                    y: 0.7)
                let finalTransform = assetTrack.preferredTransform.concatenating(scaleFactor)//.translatedBy(x: self.view_camera.frame.size.height * 0.8, y: 0)
                instruction.setTransform(finalTransform.concatenating(CGAffineTransform(translationX: 0, y: UIScreen.main.bounds.height / 4)) , at: .zero)
            }
            else {
                scaleToFitRatio = UIScreen.main.bounds.width / assetTrack.naturalSize.height
                let scaleFactor = CGAffineTransform(
                    scaleX: 0.5,
                    y: 0.65)
                let finalTransform = assetTrack.preferredTransform.concatenating(scaleFactor)
                instruction.setTransform(finalTransform.concatenating(CGAffineTransform(translationX: 0, y: 0)) , at: .zero)
            }
            
            
            
        } else {
            print("assetInfo.isPortrait else")
            let scaleFactor = CGAffineTransform(
                scaleX: scaleToFitRatio,
                y: scaleToFitRatio)
            var concat = assetTrack.preferredTransform.concatenating(scaleFactor)
                .concatenating(CGAffineTransform(
                    translationX: 0,
                    y: UIScreen.main.bounds.width / 2))
            if assetInfo.orientation == .down {
                print("assetInfo.orientation == .down")
                let fixUpsideDown = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                let windowBounds = UIScreen.main.bounds
                let yFix = assetTrack.naturalSize.height + windowBounds.height
                let centerFix = CGAffineTransform(
                    translationX: assetTrack.naturalSize.width,
                    y: yFix)
                concat = fixUpsideDown.concatenating(centerFix).concatenating(scaleFactor)
            }
            instruction.setTransform(concat, at: .zero)
        }
        print("----videoCompositionInstruction")
        return instruction
    }
    
    func exportDidFinish(_ session: AVAssetExportSession) {
        print("exportDidFinish")
        // Cleanup assets
        audioAsset = nil
        
        self.lbl_audio.text = "Sounds"
        
        guard
            session.status == AVAssetExportSession.Status.completed,
            let outputURL = session.outputURL
            else {
                print("exportDidFinish session.status:",session.status,session.status.rawValue)
                return
        }
        if self.currentSpeed != .NORMAL {
            self.speedVideo(url: outputURL)
        }
        else {
            let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
            videoPlayerVC.videoURL = outputURL
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(videoPlayerVC, animated: true)
            }
        }
    }
    
    func startTimer() {
        self.recordTimer?.invalidate()
        self.recordTimer = nil
        
        self.recordingTime = 0
        self.lbl_timer.text = Int(floor(self.recordingTime)).timeFormatted()
        self.lbl_timer.isHidden = false
        self.recordTimer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(self.countTimer), userInfo: nil, repeats: true)
    }
    
    @objc func countTimer(_ timer: Timer) {
        self.recordingTime += 0.05
        self.lbl_timer.text = Int(floor(self.recordingTime)).timeFormatted()
        self.progress_record.progress = Float(self.recordingTime / self.captureTime)
        if let duration = self.timerDuration, self.recordingTime >= duration {
            self.captureVideo()
        }
        else if self.recordingTime >= self.captureTime {
            self.captureVideo()
        }
    }
    
    func invalidateTimer() {
        self.recordTimer?.invalidate()
        self.recordTimer = nil
        self.lbl_timer.isHidden = true
        self.progress_audio.isHidden = true
        self.displayLink?.invalidate()
        self.displayLink = nil
        self.progress_audio.progress = 0
        self.recordingTime = 0
        self.lbl_timer.text = Int(floor(self.recordingTime)).timeFormatted()
        self.timerDuration = nil
    }
    
    func addCameraEffects() {
    }
    
    @objc func updateSlider() {
        self.progress_audio.progress = Float((audioPlayer?.currentTime().seconds ?? 0) / (self.audioAsset?.duration.seconds ?? 1))
    }
    
    func speedVideo(url: URL) {
        let videoAsset = AVURLAsset(url: url, options: nil)
        let mixComposition = AVMutableComposition()
        let compositionVideoTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        do {
            try compositionVideoTrack?.insertTimeRange(CMTimeRange(start: .zero, duration: videoAsset.duration), of: videoAsset.tracks(withMediaType: .video).first!, at: .zero)
        }
        catch {
            print("videoasset insert error:",error.localizedDescription)
        }
        
        let videoDuration = videoAsset.duration
        
        //slow down the video half speed (change multiplier to desired value"
        var finalTimeScale : Int64
        switch self.currentSpeed {
        case .SLOWER:
            finalTimeScale = videoDuration.value * 4
        case .SLOW:
            finalTimeScale = videoDuration.value * 2
        case .FAST:
            finalTimeScale = videoDuration.value / 2
        case .FASTER:
            finalTimeScale = videoDuration.value / 4
        default:
            finalTimeScale = videoDuration.value
        }
        
        compositionVideoTrack?.scaleTimeRange(CMTimeRange(start: .zero, duration: videoDuration), toDuration: CMTime(value: finalTimeScale, timescale: videoDuration.timescale))
        
        let documentURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let finalURL = documentURL.appendingPathComponent("speedVideo.mp4")
        
        do {
            try FileManager.default.removeItem(atPath: finalURL.path)
        }
        catch {
            print("file doesn't exist error:",error.localizedDescription)
        }
        
        if let audioTrackFirst = videoAsset.tracks(withMediaType: .audio).first {
            let audioTrack = mixComposition.addMutableTrack(
                withMediaType: .audio,
                preferredTrackID: kCMPersistentTrackID_Invalid)
            
            do {
                
                try audioTrack?.insertTimeRange(
                    CMTimeRangeMake(
                        start: CMTime.zero,
                        duration: videoAsset.duration),
                    of: audioTrackFirst,
                    at: .zero)
            } catch {
                print("Failed to load Audio track speed exception:",error.localizedDescription)
            }
            audioTrack?.scaleTimeRange(CMTimeRange(start: .zero, duration: videoDuration), toDuration: CMTime(value: finalTimeScale, timescale: videoDuration.timescale))
        }
        
        let exporter = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)
        exporter?.outputURL = finalURL
        exporter?.outputFileType = .mp4
        exporter?.exportAsynchronously(completionHandler: {
            switch exporter?.status {
            case .failed:
                print("exporter failed:",exporter?.status.rawValue)
                break
            case .cancelled:
                print("exporter cancelled:",exporter?.status.rawValue)
                break
            case .completed:
                print("exporter completed:",exporter?.status.rawValue)
                let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                videoPlayerVC.videoURL = finalURL
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(videoPlayerVC, animated: true)
                }
                break
            default:
                print("exporter default:",exporter?.status.rawValue)
            }
        })
    }
    
    func focusAnimationAt(_ point: CGPoint) {
        let focusView = UIImageView(image: #imageLiteral(resourceName: "ic_focus"))
        focusView.center = point
        focusView.alpha = 0.0
        view.addSubview(focusView)
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseInOut, animations: {
            focusView.alpha = 1.0
            focusView.transform = CGAffineTransform(scaleX: 1.25, y: 1.25)
        }) { (success) in
            UIView.animate(withDuration: 0.15, delay: 0.5, options: .curveEaseInOut, animations: {
                focusView.alpha = 0.0
                focusView.transform = CGAffineTransform(translationX: 0.6, y: 0.6)
            }) { (success) in
                focusView.removeFromSuperview()
            }
        }
    }
    
    func createWriter() {
        self.checkForAndDeleteFile()

        do {
            assetWriter = try AVAssetWriter(outputURL: movieURL() as URL, fileType: AVFileType.mp4)//mov)
        } catch let error as NSError {
            print(error.localizedDescription)
            return
        }

        let outputSettings = [
            AVVideoCodecKey : AVVideoCodecType.h264,//proRes422
            AVVideoWidthKey : Int(currentVideoDimensions!.width),
            AVVideoHeightKey : Int(currentVideoDimensions!.height)
        ] as [String : Any]

        let assetWriterVideoInput = AVAssetWriterInput(mediaType: AVMediaType.video, outputSettings: outputSettings as? [String : AnyObject])
        assetWriterVideoInput.expectsMediaDataInRealTime = true
        assetWriterVideoInput.transform = CGAffineTransform(rotationAngle: CGFloat(0.0))//M_PI / 2.0))

        let sourcePixelBufferAttributesDictionary = [
            String(kCVPixelBufferPixelFormatTypeKey) : Int(kCVPixelFormatType_420YpCbCr8BiPlanarFullRange), //Int(kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange),//Int(kCVPixelFormatType_32BGRA),
            String(kCVPixelBufferWidthKey) : Int(currentVideoDimensions!.width),
            String(kCVPixelBufferHeightKey) : Int(currentVideoDimensions!.height),
            String(kCVPixelFormatOpenGLESCompatibility) : kCFBooleanTrue
        ] as [String : Any]

        assetWriterPixelBufferInput = AVAssetWriterInputPixelBufferAdaptor(assetWriterInput: assetWriterVideoInput,
                                                                           sourcePixelBufferAttributes: sourcePixelBufferAttributesDictionary)

        if assetWriter!.canAdd(assetWriterVideoInput) {
            assetWriter!.add(assetWriterVideoInput)
        } else {
            print("no way\(assetWriterVideoInput)")
        }
        
        var acl = AudioChannelLayout()
        bzero(&acl, MemoryLayout.size(ofValue: acl))
        acl.mChannelLayoutTag = kAudioChannelLayoutTag_Mono
        
        let audioOutputSettings = [AVFormatIDKey : Int(kAudioFormatMPEG4AAC), AVNumberOfChannelsKey : 1, AVSampleRateKey : Float(44100.0), AVEncoderBitRateKey : 64000, AVChannelLayoutKey : Data(bytes: &acl, count: MemoryLayout.size(ofValue: acl))] as? [String : Any]//[AVFormatIDKey : kAudioFormatLinearPCM, AVSampleRateKey : 48000, AVLinearPCMBitDepthKey : 16, AVLinearPCMIsNonInterleaved : false, AVLinearPCMIsFloatKey : false, AVLinearPCMIsBigEndianKey : false, AVNumberOfChannelsKey : 1] as? [String : Any]
        /*let*/ assetWriterAudioInput = AVAssetWriterInput(mediaType: .audio, outputSettings: audioOutputSettings)
        assetWriterAudioInput?.expectsMediaDataInRealTime = true
        guard let audioInput = assetWriterAudioInput else { return }
        print("assetWriterAudioInput:",assetWriter?.canAdd(audioInput))
        if assetWriter?.canAdd(audioInput) == true {
            assetWriter?.add(audioInput)
        }
        else {
            print("unable to add audio input")
        }
    }
    
    func checkForAndDeleteFile() {
        let fm = FileManager.default
        let url = movieURL()
        let exist = fm.fileExists(atPath: url.path!)

        if exist {
            do {
                try fm.removeItem(at: url as URL)
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
    }
    
    func movieURL() -> NSURL {
        let tempDir = NSTemporaryDirectory()
        let url = NSURL(fileURLWithPath: tempDir).appendingPathComponent("tmpMov.mp4")
        return url! as NSURL
    }
    
    func convertCIImageToCGImage(inputImage: CIImage) -> CGImage? {
        let context = CIContext(options: nil)
        if let cgImage = context.createCGImage(inputImage, from: inputImage.extent) {
            return cgImage
        }
        return nil
    }
    
    func exportingWithFilter(url: URL) {
        //guard let url = self.assetWriter?.outputURL else { print("output url is nil"); return }
        let asset = AVAsset(url: url)
        let errors = Error.self
        
        let item = AVPlayerItem(asset: asset)
        var videoComposition : AVVideoComposition
        //item.videoComposition = AVVideoComposition(asset: asset,  applyingCIFiltersWithHandler: { [weak self] request in
        if let ciFilter = self.filter {
            videoComposition = AVVideoComposition(asset: asset,  applyingCIFiltersWithHandler: { [weak self] request in
                guard let self = self else {
                    //request.finish(with: errors.self)
                    return
                }
                let source = request.sourceImage.clampedToExtent()
                ciFilter.setValue(source, forKey: kCIInputImageKey)
                
                let output = ciFilter.outputImage?.cropped(to: request.sourceImage.extent)
                
                request.finish(with: output!, context: nil)
            })
        }
        else {
            videoComposition = AVVideoComposition(propertiesOf: asset)
        }
        
        item.videoComposition = videoComposition
        let mutableComposition = AVMutableComposition()
        if let videoTrack = mutableComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid) {
            do {
                try videoTrack.insertTimeRange(CMTimeRange(start: .zero, duration: asset.duration), of: asset.tracks(withMediaType: .video)[0], at: .zero)
            }
            catch {
                print("Failed to load video track in capture video")
            }
        }
        if let loadedAudioAsset = self.audioAsset {
            let audioTrack = mutableComposition.addMutableTrack(
            withMediaType: .audio,
            preferredTrackID: 0)
          do {
            try audioTrack?.insertTimeRange(
              CMTimeRangeMake(
                start: CMTime.zero,
                duration: asset.duration),
              of: loadedAudioAsset.tracks(withMediaType: .audio)[0],
              at: .zero)
          } catch {
            print("Failed to load Audio track in capture video")
          }
        }
        else if let audioTrackFirst = asset.tracks(withMediaType: .audio).first {
            print("natural audio")
            let audioTrack = mutableComposition.addMutableTrack(
                withMediaType: .audio,
                preferredTrackID: kCMPersistentTrackID_Invalid)
            
            do {
                
                try audioTrack?.insertTimeRange(
                    CMTimeRangeMake(
                        start: CMTime.zero,
                        duration: asset.duration),
                    of: audioTrackFirst,
                    at: .zero)
            } catch {
                print("Failed to load Audio track")
            }
        }
        else {
            print("audio track is not there in the video")
        }
        
        // self.player.replaceCurrentItem(with: item)
        
        let exporter = AVAssetExportSession(asset: /*item.asset*/mutableComposition, presetName: AVAssetExportPresetMediumQuality)//AVAssetExportPresetHighestQuality)
        exporter?.videoComposition = videoComposition
        //exporter?.timeRange = CMTimeRange(start: CMTime(seconds: 1, preferredTimescale: 1), duration: asset.duration)
        
        exporter?.outputFileType = .mp4
        let filename = "filename.mp4"
        
        let documentsDirectory = FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).last!
        let outputURL = documentsDirectory.appendingPathComponent(filename)
        try? FileManager.default.removeItem(at: outputURL)
        exporter?.outputURL = outputURL
        exporter?.exportAsynchronously(completionHandler: {
            guard exporter?.status == .completed else {
                print("export failed: \(exporter?.error)")
                return
            }
            print("done: ",outputURL)
            
            DispatchQueue.main.async {
                self.captureSession.stopRunning()
                //try? FileManager.default.removeItem(at: url)
                let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                videoPlayerVC.videoURL = outputURL//self.assetWriter?.outputURL
                
                self.navigationController?.pushViewController(videoPlayerVC, animated: true)
            }
        })
    }
    
    func startRecording() {
        if isCapturing == true {
            print("Can't start recording, already recording!")
            return
        }
        
        /*guard let videoDataOutput = videoDataOutput else {
            print("videoDataOutput is nil, can't create VideoWriter")
            return
        }*/
        
        guard let videoWidth = videoOutput.videoSettings["Width"] as? Int else  {
            print("Can't recognize video resolution, can't create VideoWriter")
            return
        }
        
        guard let videoHeight = videoOutput.videoSettings["Height"] as? Int else  {
            print("Can't recognize video resolution, can't create VideoWriter")
            return
        }
        
        let path = videofilePath()
        let videoURL = URL(fileURLWithPath: path)
        let fileManager = FileManager()
        
        if fileManager.fileExists(atPath: path) {
            do {
                try fileManager.removeItem(atPath: path)
            } catch {
                print("Can't remove file at path \(path) so can't create VideoWriter")
                return
            }
        }
        
        var size = CGSize(width: videoWidth, height: videoHeight)
        if UIApplication.shared.statusBarOrientation.isPortrait {
            size = CGSize(width: videoHeight, height: videoWidth)
        }
        
        //self.cameraSwitchButton?.isHidden = true
        //self.torchButton?.isHidden = true
        self.collection_options.isHidden = true
        self.collection_cameraTypes.isHidden = true
        self.view_effects.isHidden = true
        self.view_upload.isHidden = true
        self.startTimer()
        
        self.videoWriter = VideoWriter(fileUrl: videoURL, size: size)
        
        if self.currentCamera == self.frontCamera {
            let transform: CGAffineTransform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            self.videoWriter?.videoInput.transform = transform
        }
        self.videoWriter?.fileWriter.startWriting()
        if let asset = self.audioAsset {
            self.audioPlayer = nil
            let playerItem = AVPlayerItem(asset: asset)
            self.audioPlayer = AVPlayer(playerItem: playerItem)
            self.audioPlayer?.play()
            self.displayLink = CADisplayLink(target: self, selector: #selector(self.updateSlider))
            self.displayLink?.preferredFramesPerSecond = 10
            self.progress_audio.progress = 0
            self.progress_audio.isHidden = false
            self.displayLink?.add(to: .current, forMode: .default)
        }
        
        self.isCapturing = true
    }
    
    private func stopRecording() {
        if isCapturing == false {
            print("Can't stop recording")
            return
        }
        
        guard let videoWriter = videoWriter else {
            print("Video writer is nil")
            return
        }
        
        self.isCapturing = false
        self.videoWriter?.markAsFinished()
        /*self.cameraSwitchButton?.isHidden = false
        if isBackCameraActive {
            self.torchButton?.isHidden = false
        }*/
        self.invalidateTimer()
        self.audioPlayer?.pause()
        self.audioPlayer = nil
        self.collection_options.isHidden = false
        self.collection_cameraTypes.isHidden = false
        self.view_effects.isHidden = false
        self.view_upload.isHidden = false
        
        videoWriter.finish {
            self.videoWriter = nil
            self.cvPixelBuffer = nil
            
            DispatchQueue.main.async {
                self.captureSession.stopRunning()
                //try? FileManager.default.removeItem(at: url)
                let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                videoPlayerVC.videoURL = URL(string: self.videofilePath())
                
                self.navigationController?.pushViewController(videoPlayerVC, animated: true)
            }
            
            /*let videoFilePath = self.videofilePath()

            self.requestPhotoLibraryAccess(completion: { (granted) in
                PHPhotoLibrary.shared().performChanges({
                    PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: videoFilePath))
                }) { saved, error in
                    if saved == true {
                        print("Video saved to photo library")
                    } else {
                        print("Video did not save to photo library, reason- \(String(describing: error?.localizedDescription))")
                    }
                }
            })*/
            
        }
    }
    
    func videofilePath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths.first!
        let videoPath = "\(documentsDirectory)/video.mp4"
        
        return videoPath
    }
}

extension CameraPreviewVC: AudioListVCDelegate {
    
    func selectedAudio(url: String?, id: String?, name: String?) {
        if let url = URL(string: url ?? "") {
            self.audioAsset = AVAsset(url: url)
            self.lbl_audio.text = name ?? "Sounds"
            self.lbl_audio.isHidden = false
            self.progress_audio.progress = 0
            self.progress_audio.isHidden = false
            if let asset = self.audioAsset {
                self.audioDuration = asset.duration.seconds//Int(asset.duration.seconds)
            }
            if let duration = audioDuration {
                captureTime = captureTime < duration ? captureTime : duration
            }
            print("selectedAudio audioAsset:",audioAsset)
        }
    }
}

extension CameraPreviewVC: AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate {
    //MARK:- AVCaptureVideoDataOutputSampleBuffer Delegates...
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        /*autoreleasepool {
            print("didOutput sampleBuffer")
            if connection.isVideoOrientationSupported {
                connection.videoOrientation = .portrait//AVCaptureVideoOrientation.landscapeLeft;
            }

            guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }

            let cameraImage = CIImage(cvPixelBuffer: pixelBuffer)
            if let ciFilter = self.filter {
                //let filter = CIFilter(name: "CIColorMonochrome")!
                
                ciFilter.setValue(cameraImage, forKey: kCIInputImageKey)
           }

            let formatDescription = CMSampleBufferGetFormatDescription(sampleBuffer)!
            self.currentVideoDimensions = CMVideoFormatDescriptionGetDimensions(formatDescription)
            self.currentSampleTime = CMSampleBufferGetOutputPresentationTimeStamp(sampleBuffer)

            if self.isWriting {
                if self.assetWriterPixelBufferInput?.assetWriterInput.isReadyForMoreMediaData == true {
                    let context = CIContext()
                    context.render(cameraImage, to: pixelBuffer)
                    let success = self.assetWriterPixelBufferInput?.append(/*newPixelBuffer!*/pixelBuffer, withPresentationTime: self.currentSampleTime!)

                    if success == false {
                        print("Pixel Buffer failed")
                    }

                }
            }

            // COMMENT: And now you're sending the filtered image back to the screen.
            DispatchQueue.main.async {
                if let ciFilter = self.filter {
                    if let outputValue = ciFilter.outputImage {// ciFilter.value(forKey: kCIOutputImageKey) as? CIImage {
                        //let filteredImage = UIImage(ciImage: outputValue)
                        guard let cgImage = self.convertCIImageToCGImage(inputImage: outputValue) else { return }
                        self.img_camera.image = UIImage(cgImage: cgImage)//filteredImage
                    }
                }
                else {
                    self.img_camera.image = UIImage(ciImage: cameraImage)
                }
            }
        }*/
        if CMSampleBufferDataIsReady(sampleBuffer) == false {
            print("Sample buffer is not ready")
            return
        }
        
        if output is AVCaptureVideoDataOutput {
            videoOutput(output, didOutput: sampleBuffer, from: connection)
        }
        
        if output is AVCaptureAudioDataOutput {
            audioOutput(output, didOutput: sampleBuffer, from: connection)
        }
    }
    
    func videoOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            print("Can't create pixel buffer")
            return
        }
        
        guard let videoPreviewViewBounds = videoPreviewViewBounds else {
            print("Unrecognized preview bounds")
            return
        }
        
        var sourceImage = CIImage(cvImageBuffer: pixelBuffer)
        // fix video orientation issue
        /*if isBackCameraActive == true && orientation == .landscapeLeft {
            sourceImage = sourceImage.oriented(forExifOrientation: 3)
        } else if isBackCameraActive == false && orientation == .landscapeRight {
            sourceImage = sourceImage.oriented(forExifOrientation: 3)
        } else if orientation == .portrait {
            sourceImage = sourceImage.oriented(forExifOrientation: 6)
        }*/
        let sourceExtent = sourceImage.extent
        
        // add filter to image
        var filterImage : CIImage?
        if let ciFilter = self.filter {
            ciFilter.setValue(sourceImage, forKey: kCIInputImageKey)
            filterImage = ciFilter.outputImage
        }
        else {
            filterImage = sourceImage
        }
        guard let filteredImage = filterImage else {
             print("Can't add filter ro image")
            return
        }
        /*guard let filteredImage = sourceImage.invertColorEffect() else {
            print("Can't add filter ro image")
            return
        }*/
        /*switch self.selectedFilter {
        case .NONE:
            guard let filteredImage1 = sourceImage.noneEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .INVERT:
            guard let filteredImage1 = sourceImage.invertColorEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .VIGNETTE:
            guard let filteredImage1 = sourceImage.vignetteEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .PHOTOINSTANT:
            guard let filteredImage1 = sourceImage.photoInstantEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .CRYSTALIZE:
            guard let filteredImage1 = sourceImage.crystallizeEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .COMIC:
            guard let filteredImage1 = sourceImage.comicEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .BLOOM:
            guard let filteredImage1 = sourceImage.bloomEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .EDGES:
            guard let filteredImage1 = sourceImage.edgesEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .EDGEWORK:
            guard let filteredImage1 = sourceImage.edgeWorkEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .GLOOM:
            guard let filteredImage1 = sourceImage.gloomEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .HEXAGONAL:
            guard let filteredImage1 = sourceImage.hexagonalPixellateEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .HIGHLIGHT_SHADOW:
            guard let filteredImage1 = sourceImage.highlightShadowAdjust() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .PIXELLATE:
            guard let filteredImage1 = sourceImage.pixellateEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        case .POINTILLIZE:
            guard let filteredImage1 = sourceImage.pointillizeEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        default:
            guard let filteredImage1 = sourceImage.noneEffect() else {
                print("Can't add filter ro image")
                return
            }
            filteredImage = filteredImage1
        }*/
        
        let sourceAspect = sourceExtent.size.width / sourceExtent.size.height
        let previewAspect = videoPreviewViewBounds.size.width / videoPreviewViewBounds.size.height
        
        // we want to maintain the aspect radio of the screen size, so we clip the video image
        var drawRect = sourceExtent
        if sourceAspect > previewAspect {
            // use full height of the video image, and center crop the width
            drawRect.origin.x += (drawRect.size.width - drawRect.size.height * previewAspect) / 2.0
            drawRect.size.width = drawRect.size.height * previewAspect
        } else {
            // use full width of the video image, and center crop the height
            drawRect.origin.y += (drawRect.size.height - drawRect.size.width / previewAspect) / 2.0
            drawRect.size.height = drawRect.size.width / previewAspect
        }
        
        videoPreviewView?.bindDrawable()
        if glContext != EAGLContext.current() {
            EAGLContext.setCurrent(glContext)
        }
        
        // clear eagl view to grey
        glClearColor(0.5, 0.5, 0.5, 1.0);
        glClear(GLbitfield(GL_COLOR_BUFFER_BIT));
        
        // set the blend mode to "source over" so that CI will use that
        glEnable(GLenum(GL_BLEND));
        glBlendFunc(GLenum(GL_ONE), GLenum(GL_ONE_MINUS_SRC_ALPHA));
        
        ciContext?.draw(filteredImage, in: videoPreviewViewBounds, from: drawRect)
        videoPreviewView?.display()
        
        //recording
        if isCapturing {
            // convert CIImage to CVPixelBuffer
            if cvPixelBuffer == nil {
                let attributesDictionary = [kCVPixelBufferIOSurfacePropertiesKey: [:]]
                CVPixelBufferCreate(kCFAllocatorDefault,
                                    Int(sourceImage.extent.size.width),
                                    Int(sourceImage.extent.size.height),
                                    kCVPixelFormatType_32BGRA,
                                    attributesDictionary as CFDictionary,
                                    &cvPixelBuffer);
            }
            ciContext?.render(filteredImage, to: cvPixelBuffer!)
            
            // convert new CVPixelBuffer to new CMSampleBuffer
            var sampleTime = CMSampleTimingInfo()
            sampleTime.duration = CMSampleBufferGetDuration(sampleBuffer)
            sampleTime.presentationTimeStamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
            sampleTime.decodeTimeStamp = CMSampleBufferGetDecodeTimeStamp(sampleBuffer)
            
            var videoInfo: CMVideoFormatDescription? = nil
            CMVideoFormatDescriptionCreateForImageBuffer(allocator: kCFAllocatorDefault, imageBuffer: cvPixelBuffer!, formatDescriptionOut: &videoInfo)
            var newSampleBuffer: CMSampleBuffer?
            CMSampleBufferCreateReadyWithImageBuffer(allocator: kCFAllocatorDefault, imageBuffer: cvPixelBuffer!, formatDescription: videoInfo!, sampleTiming: &sampleTime, sampleBufferOut: &newSampleBuffer)
            
            // writer new CMSampleBuffer to asser writer
            self.videoWriter?.write(sample: newSampleBuffer!, isVideoBuffer: true)
        }
    }
    
    func audioOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        //recording
        if isCapturing {
            self.videoWriter?.write(sample: sampleBuffer, isVideoBuffer: false)
        }
    }
}

extension CameraPreviewVC: UIGestureRecognizerDelegate {
    //MARK:- UIGestureRecognizer Delegates...
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        print("shouldReceive touch:",touch.view)
        print("is touch view image_camera:",touch.view == self.view_camera)
        if touch.view?.isDescendant(of: self.view_camera) == true && touch.view != self.view_camera {
            return false
        }
        return true
    }
}

extension CameraPreviewVC: AVCaptureFileOutputRecordingDelegate {
    //MARK:- AVCaptureFileOutputRecording Delegates...
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        if let err = error {
            print("didFinishRecordingTo error:",err.localizedDescription)
        }
        else {
            print("didFinishRecordingTo outputFileURL:",outputFileURL)
            if self.recordEndCorrectly {
                print("recordEndCorrectly:",recordEndCorrectly)
                var cameraType = "back"
                if self.currentCamera == self.backCamera {
                    cameraType = "back"
                }
                else {
                    cameraType = "front"
                }
                //self.mergeVideoAudio(videoURL: outputFileURL, cameraType: cameraType)
                self.exportingWithFilter(url: outputFileURL)
            }
        }
    }
}

extension CameraPreviewVC: MPMediaPickerControllerDelegate {
    //MARK:- MPMediaPickerController Delegates...
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        dismiss(animated: true) {
            let selectedSongs = mediaItemCollection.items
            guard let song = selectedSongs.first, let audioURL = song.value(forProperty: MPMediaItemPropertyAssetURL) as? URL else { return }
            print("song:",song,"audioURL:",audioURL)
            self.audioAsset = AVAsset(url: audioURL)
        }
    }
}

extension CameraPreviewVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIVideoEditorControllerDelegate {
    //MARK:- ImagePicker Delegates...
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print("video captured info:",info)
        let url = info[UIImagePickerController.InfoKey.mediaURL] as? URL
        picker.dismiss(animated: true) {
            let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
            videoPlayerVC.videoURL = url
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(videoPlayerVC, animated: true)
            }
        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("imagePickerControllerDidCancel")
        picker.dismiss(animated: true, completion: nil)
    }
}

extension CameraPreviewVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    //MARK:- CollectionView Delegates...
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collection_options {
            return self.cameraOptionsArray.count
        }
        else if collectionView == self.collection_cameraTypes {
            return self.cameraTypesArray.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.collection_options {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CameraOptionsCVCell", for: indexPath) as! CameraOptionsCVCell
            
            cell.lbl_title.text = self.cameraOptionsArray[indexPath.item]
            
            if indexPath.item == 5 {
                if self.isFlashOn {
                    cell.img_option.image = #imageLiteral(resourceName: "ic_flash").withRenderingMode(.alwaysOriginal)
                }
                else {
                    cell.img_option.image = #imageLiteral(resourceName: "ic_flashOn").withRenderingMode(.alwaysTemplate)
                }
            }
            else if indexPath.item == 1 {
                if self.currentSpeed == .NORMAL {
                    cell.img_option.image = #imageLiteral(resourceName: "ic_speedOff")
                }
                else {
                    cell.img_option.image = #imageLiteral(resourceName: "ic_speedOn")
                }
            }
            else {
                cell.img_option.image = self.cameraOptionsImagesArray[indexPath.item].withRenderingMode(.alwaysOriginal)
            }
            
            cell.img_option.tintColor = UIColor.white
            return cell
        }
        else if collectionView == self.collection_cameraTypes {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CameraTypeCVCell", for: indexPath) as! CameraTypeCVCell
            
            cell.lbl_title.text = self.cameraTypesArray[indexPath.item]
            
            if self.selectedCameraType == indexPath.item {
                cell.lbl_dot.isHidden = false
            }
            else {
                cell.lbl_dot.isHidden = true
            }
            
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.collection_options {
            return CGSize(width: 50, height: 65)
        }
        else if collectionView == self.collection_cameraTypes {
            let text =  self.cameraTypesArray[indexPath.item]
            let width = UILabel.textWidth(font: UIFont(name:"OpenSans-Regular",size:13)!, text: text)
            
            return CGSize(width: width + 60, height: 40)
        }
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == self.collection_options {
            return 0
        }
        else if collectionView == self.collection_cameraTypes {
            return 5
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == self.collection_options {
            return 0
        }
        else if collectionView == self.collection_cameraTypes {
            return 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == self.collection_options {
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        else if collectionView == self.collection_cameraTypes {
            return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        }
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.collection_options {
            switch indexPath.item {
            case 0:
                guard let input = self.videoInput else {
                    return
                }
                self.captureSession.beginConfiguration()
                if self.currentCamera == self.backCamera {
                    if self.isFlashOn {
                        /*guard let device = AVCaptureDevice.default(for: .video) else { return }
                        guard device.hasTorch else { return }
                        do {
                            try device.lockForConfiguration()
                            
                            device.torchMode = .off
                            self.isFlashOn = false
                            device.unlockForConfiguration()
                        }
                        catch {
                            print("lockForConfiguration error : ",error.localizedDescription)
                        }*/
                        self.isFlashOn = false
                    }
                    self.captureSession.removeInput(input)
                    guard let camera = self.frontCamera else {
                        return
                    }
                    do {
                        self.videoInput = try AVCaptureDeviceInput(device: camera)
                        self.currentCamera = camera
                        if let video = self.videoInput,  self.captureSession.canAddInput(video) {
                            self.captureSession.addInput(video)
                        }
                        self.mirrorFrontCamera()
                    }
                    catch {
                        print("camera error:",error.localizedDescription)
                    }
                }
                else {
                    self.captureSession.removeInput(input)
                    guard let camera = self.backCamera else {
                        return
                    }
                    do {
                        self.videoInput = try AVCaptureDeviceInput(device: camera)
                        self.currentCamera = camera
                        if let video = self.videoInput,  self.captureSession.canAddInput(video) {
                            self.captureSession.addInput(video)
                        }
                    }
                    catch {
                        print("camera error:",error.localizedDescription)
                    }
                }
                self.captureSession.commitConfiguration()
                videoPreviewView?.transform = (videoPreviewView?.transform.scaledBy(x: -1, y: 1))!
                cvPixelBuffer = nil
                collectionView.reloadItems(at: [IndexPath(item: 5, section: 0)])
            case 5:
                /*if self.imagePicker.cameraDevice == .rear {
                 if self.imagePicker.cameraFlashMode == .off {
                 self.imagePicker.cameraFlashMode = .on
                 }
                 else {
                 self.imagePicker.cameraFlashMode = .off
                 }
                 collectionView.reloadItems(at: [indexPath])
                 }*/
                if self.currentCamera == self.backCamera {
                    guard let device = AVCaptureDevice.default(for: .video) else { return }
                    guard device.hasTorch else { return }
                    do {
                        try device.lockForConfiguration()
                        
                        if device.torchMode == .on {
                            device.torchMode = .off
                            self.isFlashOn = false
                            //sender.setImage(#imageLiteral(resourceName: "flash_off"), for: .normal)
                        }
                        else {
                            do {
                                try device.setTorchModeOn(level: 1.0)
                                self.isFlashOn = true
                                //sender.setImage(#imageLiteral(resourceName: "flash_on"), for: .normal)
                            }
                            catch {
                                print("setTorchModeOn error : ",error.localizedDescription)
                            }
                        }
                        device.unlockForConfiguration()
                        collectionView.reloadItems(at: [indexPath])
                    }
                    catch {
                        print("lockForConfiguration error : ",error.localizedDescription)
                    }
                }
            case 1:
                print("speed view")
                let speedVariationView = Bundle.main.loadNibNamed("SpeedVariationView", owner: nil, options: nil)![0] as! SpeedVariationView
                speedVariationView.delegate = self
                speedVariationView.selectedSpeed = self.currentSpeed
                self.view.addSubview(speedVariationView)
            case 3:
                print("filter view")
                let filterSelectionView = Bundle.main.loadNibNamed("FilterSelectionView", owner: nil, options: nil)![0] as! FilterSelectionView
                filterSelectionView.delegate = self
                self.view.addSubview(filterSelectionView)
            case 4:
                print("timer view")
                let timerSelectionView = Bundle.main.loadNibNamed("TimerSelectionView", owner: nil, options: nil)![0] as! TimerSelectionView
                timerSelectionView.slider_timer.maximumValue = Float(self.captureTime)
                timerSelectionView.slider_timer.value = Float(self.captureTime)
                timerSelectionView.lbl_timer.text = "\(Int(floor(self.captureTime)))s"
                timerSelectionView.delegate = self
                self.view.addSubview(timerSelectionView)
            default:
                break
            }
        }
        else if collectionView == self.collection_cameraTypes {
            //if !self.movieFileOutput.isRecording {
                self.selectedCameraType = indexPath.item
                switch indexPath.item {
                case 1:
                    if let duration = self.audioDuration {
                        self.captureTime = self.captureTime < 60 ? self.captureTime : 60
                    }
                    else {
                        self.captureTime = 60
                    }
                default:
                    if let duration = self.audioDuration {
                        self.captureTime = self.captureTime < 15 ? self.captureTime : 15
                    }
                    else {
                        self.captureTime = 15
                    }
                }
                collectionView.reloadData()
            //}
        }
    }
}

extension CameraPreviewVC: SpeedVariationViewDelegate {
    //MARK:- SpeedVariationView Delegates...
    func selectedSpeed(speedItem: SpeedItem) {
        self.currentSpeed = speedItem
        self.collection_options.reloadItems(at: [IndexPath(item: 1, section: 0)])
    }
}

extension CameraPreviewVC: FilterSelectionViewDelegate {
    //MARK:- FilterSelectionView Delegates...
    func didSelectFilter(filter: (title: String, type: FilterItem)) {
        print("selected filter:",filter)
        switch filter.type {
        case .NONE:
            self.filter = nil
        /*case .MONOCHROME_FILTER:
            self.filter = CIFilter(name: "CIColorMonochrome")
        case .INVERT_COLORS_FILTER:
            self.filter = CIFilter(name: "CIColorInvert")
        case .FALSE_COLOR_FILTER:
            self.filter = CIFilter(name: "CIFalseColor")*/
        case .COLOR_MATRIX_FILTER:
            self.filter = CIFilter(name: filter.type.rawValue)
            self.filter?.setValue(CIVector(x: 0, y: 0, z: 0, w: 0), forKey: "inputGVector")
        case .TEMPERATURE_TINT_FILTER:
            self.filter = CIFilter(name: filter.type.rawValue)
            if filter.title == "F10" {
                self.filter?.setValue(CIVector(x: 16000, y: 1000), forKey: "inputNeutral")
                self.filter?.setValue(CIVector(x: 1000, y: 500), forKey: "inputTargetNeutral")
            }
            else if filter.title == "F11" {
                self.filter?.setValue(CIVector(x: 6500, y: 500), forKey: "inputNeutral")
                self.filter?.setValue(CIVector(x: 1000, y: 630), forKey: "inputTargetNeutral")
            }
        default:
            self.filter = CIFilter(name: filter.type.rawValue)
        }
    }
}

extension CameraPreviewVC: TimerSelectionViewDelegate {
    //MARK:- TimerSelectionView Delegates...
    func startShutter(timeSpot: Int, timer: Float) {
        self.timerDuration = Double(timer)
        self.shutterDuration = timeSpot
        self.lbl_shutterTime.text = "\(self.shutterDuration)"
        self.lbl_shutterTime.isHidden = false
        self.shutterTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.countShutter), userInfo: nil, repeats: true)
        guard let timerAudioPath = Bundle.main.url(forResource: "Timer_Sound", withExtension: "mp3") else {
            print("timer sound not found")
            return
        }
        self.audioPlayer = AVPlayer(url: timerAudioPath)
        self.audioPlayer?.play()
        
    }
    
    @objc func countShutter() {
        self.shutterDuration -= 1
        self.lbl_shutterTime.text = "\(self.shutterDuration)"
        if shutterDuration <= 0 {
            self.audioPlayer?.pause()
            self.audioPlayer = nil
            self.captureVideo()
            self.lbl_shutterTime.isHidden = true
            self.shutterTimer?.invalidate()
            self.shutterTimer = nil
        }
    }
}
