//
//  UploadVideoVC.swift
//  LmTek
//
//  Created by PTBLR-1128 on 15/09/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import AVFoundation
import SwiftyJSON
import Alamofire

class UploadVideoVC: UIViewController {
        
    //MARK: Outlets...
    //@IBOutlet weak var txt_description : UITextView!
    @IBOutlet weak var tbl_postPermissions : UITableView!
    @IBOutlet weak var btn_post : UIButton!
    @IBOutlet weak var constraint_postPermissionsTable : NSLayoutConstraint!
    @IBOutlet weak var img_thumbnail : UIImageView!
    @IBOutlet weak var view_tags : UIView!
    @IBOutlet weak var tbl_tags : UITableView!
    @IBOutlet weak var tagging: Tagging! {
        didSet {
            tagging.accessibilityIdentifier = "Tagging"
            tagging.textView.accessibilityIdentifier = "TaggingTextView"
            tagging.textView.textColor = UIColor.lightGray//UIColor(hexString: "#1D1B1B")?.withAlphaComponent(0.19)
            tagging.textView.text = descriptionPlaceholder
            tagging.textView.font = UIFont(name: "OpenSans-Regular", size: 16.0)
            tagging.backgroundColor = UIColor.black
            tagging.defaultAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: "OpenSans-Regular", size: 16.0)]
            tagging.symbolAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            tagging.taggedAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.underlineStyle: NSNumber(value: 1)]
            tagging.dataSource = self
            tagging.delegate = self

            tagging.symbol = "#"
            tagging.tagableList = []
        }
    }
    
    //MARK:- Variables...
    var video_url : URL?
    var permissionsArray = ["Who can view this video", "Allow comments", "Allow Duet", "Save to device"]
    var permissionsStatusArray = [true, true, true, true]
    var isInitial = true
    var descriptionPlaceholder = "Describe your video"
    var hashtagsArray = [String]()
    var duetVideoID : String?
    var duetUserID : String?
    var startDurationTime: CGFloat = 0
    
    private var matchedList: [String] = [] {
        didSet {
            tbl_tags.reloadSections(IndexSet(integer: 0), with: .fade)
            if self.view_tags.isHidden {
                self.view_tags.isHidden = false
            }
        }
    }
    private var taggedList: [TaggingModel] = []
    
    var imageData: NSData?
    var isDuet: String = "0"
    //MARK: View Life Cycle...
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getHastagVideosAPI()
        self.cellRegistration()
        self.getThumbnail()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if self.isInitial {
            self.isInitial = false
            self.constraint_postPermissionsTable.constant = .greatestFiniteMagnitude
            self.tbl_postPermissions.reloadData()
            self.tbl_postPermissions.layoutIfNeeded()
            self.constraint_postPermissionsTable.constant = self.tbl_postPermissions.contentSize.height
        }
    }
    

    //MARK:- IBActions...
    @IBAction func backClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func postClicked() {
        self.uploadVideosAPI()
    }
    
    @IBAction func shareClicked() {
        guard let url = self.video_url else {
            print("shareClicked empty url")
            return
        }
        let linkToShare = [url]
        let activityViewController = UIActivityViewController(activityItems: linkToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that ipad won't crash
        
        DispatchQueue.main.async {
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    @IBAction func hashtagClicked() {
        if tagging.textView.textColor == UIColor.lightGray { //UIColor(hexString: "#1D1B1B")?.withAlphaComponent(0.19)
            let mutableAttrString = NSMutableAttributedString(string: "")
            //mutableAttrString.append(self.tagging.textView.attributedText)
            let hashString = NSAttributedString(string: "#", attributes: [NSAttributedString.Key.font : UIFont(name: "OpenSans-Regular", size: 16.0)])
            mutableAttrString.append(hashString)//NSAttributedString(string: "#"))
            self.tagging.textView.attributedText = mutableAttrString
            //self.tagging.textView.text = "#"
            self.view_tags.isHidden = false
            tagging.textView.textColor = UIColor.black
            tagging.textView.becomeFirstResponder()
        }
        else {
            if self.view_tags.isHidden {
                let mutableAttrString = NSMutableAttributedString(string: "")
                mutableAttrString.append(self.tagging.textView.attributedText)
                let hashString = NSAttributedString(string: "#", attributes: [NSAttributedString.Key.font : UIFont(name: "OpenSans-Regular", size: 16.0)])
                mutableAttrString.append(hashString)//NSAttributedString(string: "#"))
                self.tagging.textView.attributedText = mutableAttrString
                //self.tagging.textView.text += "#"
                self.view_tags.isHidden = false
            }
        }
    }
    
    @IBAction func atClicked() {
        if tagging.textView.textColor == UIColor.lightGray { //UIColor(hexString: "#1D1B1B")?.withAlphaComponent(0.19)
            let mutableAttrString = NSMutableAttributedString(string: "")
            //mutableAttrString.append(self.tagging.textView.attributedText)
            let atString = NSAttributedString(string: "@", attributes: [NSAttributedString.Key.font : UIFont(name: "OpenSans-Regular", size: 16.0)])
            mutableAttrString.append(atString)//NSAttributedString(string: "@"))
            self.tagging.textView.attributedText = mutableAttrString
            //self.tagging.textView.text = "#"
            tagging.textView.textColor = UIColor.black
            tagging.textView.becomeFirstResponder()
        }
        else {
            if self.view_tags.isHidden {
                let mutableAttrString = NSMutableAttributedString(string: "")
                mutableAttrString.append(self.tagging.textView.attributedText)
                let atString = NSAttributedString(string: "@", attributes: [NSAttributedString.Key.font : UIFont(name: "OpenSans-Regular", size: 16.0)])
                mutableAttrString.append(atString)//NSAttributedString(string: "@"))
                self.tagging.textView.attributedText = mutableAttrString
            }
        }
        self.tagUser()
    }
    
    //MARK:- Functions..
    func cellRegistration() {
        self.tbl_postPermissions.register(UINib(nibName: "PostPermissionSwitchTVCell", bundle: nil), forCellReuseIdentifier: "PostPermissionSwitchTVCell")
        self.tbl_postPermissions.register(UINib(nibName: "PublicPrivateTVCell", bundle: nil), forCellReuseIdentifier: "PublicPrivateTVCell")
        
        self.tbl_postPermissions.rowHeight = UITableView.automaticDimension
        self.tbl_postPermissions.estimatedRowHeight = 40
    }
    func thumbnailForVideoAtURL(){
        if let url = self.video_url {
            let time = CGFloat(startDurationTime)
            let asset = AVURLAsset(url: url, options: nil)
            let imageGenerator = AVAssetImageGenerator(asset: asset)
            do {
                let cgImage = try imageGenerator.copyCGImage(at: CMTime(value: CMTimeValue(time), timescale: 1), actualTime: nil)
                self.img_thumbnail.image = UIImage(cgImage: cgImage)
            }
            catch {
                print("getThumbnail error:",error.localizedDescription)
            }
        }
    }

    func getThumbnail() {
        if let url = self.video_url {
            let asset = AVURLAsset(url: url, options: nil)
            let imageGenerator = AVAssetImageGenerator(asset: asset)
            do {
                let cgImage = try imageGenerator.copyCGImage(at: CMTime(value: 0, timescale: 1), actualTime: nil)
                self.img_thumbnail.image = UIImage(cgImage: cgImage)
            }
            catch {
                print("getThumbnail error:",error.localizedDescription)
            }
        }
    }
    //MARK: Select video Cover Picture
    
    @IBAction func btnSelectCoverPictureClicked(_ sender: UIButton) {
        print("cover picture")
        if let url = self.video_url {
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "VideoTrimmerVC") as! VideoTrimmerVC
            VC.videoAsset = AVAsset(url: url)
            VC.delegate1 = self
            VC.video_url = video_url
            VC.checkThumbImage = true
            self.navigationController?.pushViewController(VC, animated: false)
        }
        
    }
    /*func updateTaggedList(allText: String, tagText: String) {
        guard let range = currentTaggingRange else {return}
        print("updateTaggedList range:",range)
        
        let origin = (allText as NSString).substring(with: range)
        let tag = tagFormat(tagText)
        let replace = tag.appending(" ")
        let changed = (allText as NSString).replacingCharacters(in: range, with: replace)
        let tagRange = NSMakeRange(range.location, tag.utf16.count)
        
        taggedList.append(TaggingModel(text: tagText, range: tagRange))
        for i in 0..<taggedList.count-1 {
            var location = taggedList[i].range.location
            let length = taggedList[i].range.length
            if location > tagRange.location {
                location += replace.count - origin.count
                taggedList[i].range = NSMakeRange(location, length)
            }
        }
        
        txt_description.text = changed
        updateAttributeText(selectedLocation: range.location+replace.count)
        dataSource?.tagging(self.txt_description, didChangedTaggedList: taggedList)
    }
    
    func tagging(textView: UITextView) {
        let selectedLocation = textView.selectedRange.location
        let taggingText = (textView.text as NSString).substring(with: NSMakeRange(0, selectedLocation))
        let space: Character = " "
        let lineBrak: Character = "\n"
        var tagable: Bool = false
        var characters: [Character] = []
        
        for char in Array(taggingText).reversed() {
            if char == symbol.first {
                characters.append(char)
                tagable = true
                break
            } else if char == space || char == lineBrak {
                tagable = false
                break
            }
            characters.append(char)
        }
        
        guard tagable else {
            currentTaggingRange = nil
            currentTaggingText = nil
            return
        }
        
        let data = matchedData(taggingCharacters: characters, selectedLocation: selectedLocation, taggingText: taggingText)
        currentTaggingRange = data.0
        currentTaggingText = data.1
    }
    
    func tagFormat(_ text: String) -> String {
        return symbol.appending(text)
    }
    
    private func updateAttributeText(selectedLocation: Int) {
        var error: Error? = nil
        var regex: NSRegularExpression? = nil
        do {
            regex = try NSRegularExpression(pattern: "#(\\w+)", options: [])
        } catch {
        }
        let stringWithTags = txt_description.text//textView.text
        let matches = regex?.matches(in: stringWithTags ?? "", options: [], range: NSRange(location: 0, length: stringWithTags?.count ?? 0))
        let attString = NSMutableAttributedString(string: stringWithTags ?? "")
        let font = UIFont(name: "OpenSans-Regular", size: 16.0)
        attString.addAttribute(.font, value: font, range: NSRange(location: 0, length: stringWithTags!.count))
        attString.addAttribute(.foregroundColor, value: UIColor.black, range: NSRange(location: 0, length: stringWithTags!.count))
        let stringLength = stringWithTags?.count ?? 0
        for match in matches ?? [] {
            let wordRange = match.range(at: 1)
            let word = (stringWithTags as NSString?)?.substring(with: wordRange)
            let font = UIFont(name: "OpenSans-Regular", size: 16.0)
            attString.addAttribute(.font, value: font, range: NSRange(location: 0, length: stringLength))
            //let backgroundColor = UIColor.orange
            //attString.addAttribute(.backgroundColor, value: backgroundColor, range: wordRange)
            let foregroundColor = UIColor.blue
            attString.addAttribute(.foregroundColor, value: foregroundColor, range: wordRange)
            print("Found tag \(word ?? "")")
        }
        //textView.attributedText = attString//attributedString
        txt_description.attributedText = attString
        txt_description.selectedRange = NSMakeRange(selectedLocation, 0)
        //textView.selectedRange = NSMakeRange(selectedLocation, 0)
    }
    
    func matchedData(taggingCharacters: [Character], selectedLocation: Int, taggingText: String) -> (NSRange?, String?) {
        var matchedRange: NSRange?
        var matchedString: String?
        let tag = String(taggingCharacters.reversed())
        let textRange = NSMakeRange(selectedLocation-tag.count, tag.count)
        
        guard tag == symbol else {
            let matched = tagRegex.matches(in: taggingText, options: .reportCompletion, range: textRange)
            if matched.count > 0, let range = matched.last?.range {
                matchedRange = range
                matchedString = (taggingText as NSString).substring(with: range).replacingOccurrences(of: symbol, with: "")
            }
            return (matchedRange, matchedString)
        }
        
        matchedRange = textRange
        matchedString = symbol
        return (matchedRange, matchedString)
    }
    
    func updateTaggedList(range: NSRange, textCount: Int) {
        taggedList = taggedList.filter({ (model) -> Bool in
            if model.range.location < range.location && range.location < model.range.location+model.range.length {
                return false
            }
            if range.length > 0 {
                if range.location <= model.range.location && model.range.location < range.location+range.length {
                    return false
                }
            }
            return true
        })
        
        for i in 0..<taggedList.count {
            var location = taggedList[i].range.location
            let length = taggedList[i].range.length
            if location >= range.location {
                if range.length > 0 {
                    if textCount > 1 {
                        location += textCount - range.length
                    } else {
                        location -= range.length
                    }
                } else {
                    location += textCount
                }
                taggedList[i].range = NSMakeRange(location, length)
            }
        }
        
        currentTaggingText = nil
        dataSource?.tagging(self.txt_description, didChangedTaggedList: taggedList)
    }*/

}

extension UploadVideoVC: UITextViewDelegate {
    //MARK:- TextView Delegates...
    /*func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor(hexString: "#1D1B1B")?.withAlphaComponent(0.19) {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }*/
    
    func textViewDidEndEditing(_ textView: UITextView) {
        /*if textView.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            textView.text = self.descriptionPlaceholder
            textView.textColor = UIColor(hexString: "#1D1B1B")?.withAlphaComponent(0.19)
        }*/
        if !self.view_tags.isHidden {
            self.view_tags.isHidden = true
        }
    }
    
    /*func textViewDidChange(_ textView: UITextView) {
        /*if textView.text.last == "#" {
            self.getHashtags()
        }
        if textView.text.last == " " {
            if !self.view_tags.isHidden {
                self.view_tags.isHidden = true
            }
        }*/
        tagging(textView: textView)
        updateAttributeText(selectedLocation: textView.selectedRange.location)
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        tagging(textView: textView)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        updateTaggedList(range: range, textCount: text.utf16.count)
        return true
    }*/
}

extension UploadVideoVC: UITableViewDelegate, UITableViewDataSource {
    //MARK:- TableView Delegates...
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tbl_postPermissions {
            return 4
        }
        else {
            return self.matchedList.count//self.hashtagsArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tbl_postPermissions {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PublicPrivateTVCell") as! PublicPrivateTVCell
                
                cell.lbl_title.text = self.permissionsArray[indexPath.row]
                
                cell.btn_public.addTarget(self, action: #selector(togglePublic), for: .touchUpInside)
                cell.btn_private.addTarget(self, action: #selector(togglePublic), for: .touchUpInside)
                
                if self.permissionsStatusArray[indexPath.row] {
                    let publicLayers = cell.btn_public.layer.sublayers ?? []
                    for i in 0 ..< publicLayers.count {
                        let layer = publicLayers[i]
                        if layer.isKind(of: CAGradientLayer.self) {
                            cell.btn_public.layer.sublayers?.remove(at: i)
                            break
                        }
                    }
                    cell.btn_public.backgroundColor = colorYellow
                    cell.btn_public.setTitleColor(.black, for: .normal)
                    let privateLayer = cell.btn_private.layer.sublayers ?? []
                    for i in 0 ..< privateLayer.count {
                        let layer = privateLayer[i]
                        if layer.isKind(of: CAGradientLayer.self) {
                            cell.btn_private.layer.sublayers?.remove(at: i)
                            break
                        }
                    }
                    
                    cell.btn_public.setTitleColor(UIColor.black, for: .normal)
                    cell.btn_private.setTitleColor(.white, for: .normal)//setTitleColor(UIColor(hexString: "#1D1B1B")?.withAlphaComponent(0.45), for: .normal)
                    cell.btn_private.borderWidth = 1
                    cell.btn_public.borderWidth = 0
                }
                else {
                    let publicLayers = cell.btn_public.layer.sublayers ?? []
                    for i in 0 ..< publicLayers.count {
                        let layer = publicLayers[i]
                        if layer.isKind(of: CAGradientLayer.self) {
                            cell.btn_public.layer.sublayers?.remove(at: i)
                            break
                        }
                    }
                    
                    let privateLayer = cell.btn_private.layer.sublayers ?? []
                    for i in 0 ..< privateLayer.count {
                        let layer = privateLayer[i]
                        if layer.isKind(of: CAGradientLayer.self) {
                            cell.btn_private.layer.sublayers?.remove(at: i)
                            break
                        }
                    }
                    cell.btn_private.backgroundColor = colorYellow
                    cell.btn_private.setTitleColor(UIColor.black, for: .normal)
                    cell.btn_public.setTitleColor(.white, for: .normal)//setTitleColor(UIColor(hexString: "#1D1B1B")?.withAlphaComponent(0.45), for: .normal)
                    cell.btn_public.borderWidth = 1
                    cell.btn_private.borderWidth = 0
                }
                
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PostPermissionSwitchTVCell") as! PostPermissionSwitchTVCell
                
                cell.delegate = self
                
                cell.lbl_title.text = self.permissionsArray[indexPath.row]
                
                if indexPath.row == permissionsArray.count - 1 {
                    cell.lbl_separator.isHidden = true
                }
                else {
                    cell.lbl_separator.isHidden = false
                }
                
                //cell.switch_postPermission.isOn = self.permissionsStatusArray[indexPath.row]
                
                return cell
            }
        }
        else {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "HashtagTVCell")
            cell.selectionStyle = .none
            cell.textLabel?.text = self.matchedList[indexPath.row]//hashtagsArray[indexPath.row]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tbl_tags {
            /*self.txt_description.text += "#\(self.hashtagsArray[indexPath.row])"
            self.view_tags.isHidden = true*/
            //updateTaggedList(allText: txt_description.text, tagText: hashtagsArray[indexPath.row])
            tagging.updateTaggedList(allText: tagging.textView.text, tagText: matchedList[indexPath.row])
            self.view_tags.isHidden = true
        }
    }
    
    @objc func togglePublic(_ sender: UIButton) {
        if sender.tag == 0 {
            if !self.permissionsStatusArray[0] {
                self.permissionsStatusArray[0] = true
                self.tbl_postPermissions.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
            }
        }
        else {
            if self.permissionsStatusArray[0] {
                self.permissionsStatusArray[0] = false
                self.tbl_postPermissions.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
            }
        }
    }
    
}

extension UploadVideoVC: PostPermissionSwitchTVCellDelegate {
    //MARK:- PostPermissionSwitchTVCell Delegates...
    func changeStatus(cell: PostPermissionSwitchTVCell) {
        if let indexPath = self.tbl_postPermissions.indexPath(for: cell) {
            self.permissionsStatusArray[indexPath.row] = cell.switch_postPermission.isOn
        }
    }
}

extension UploadVideoVC: TaggingDataSource, TaggingDelegate {
    //MARK:- TaggingData Sources...
    
    func tagging(_ tagging: Tagging, didChangedTagableList tagableList: [String]) {
        print("didChangedTagableList tagableList:",tagableList)
        matchedList = tagableList
    }
    
    func tagging(_ tagging: Tagging, didChangedTaggedList taggedList: [TaggingModel]) {
        self.taggedList = taggedList
    }
    /*func tagging(_ tagging: UITextView, didChangedTagableList tagableList: [String]) {
        print("didChangedTagableList tagableList:",tagableList)
        self.matchedList = tagableList
    }
    func tagging(_ tagging: UITextView, didChangedTaggedList taggedList: [TaggingModel]) {
        
    }*/
    func taggingHideTagsList() {
        if !self.view_tags.isHidden {
            self.view_tags.isHidden = true
        }
    }
    
    func tagUser() {
        print("tagUser")
        let inviteFriendsVC = self.storyboard?.instantiateViewController(withIdentifier: "InviteFriendsVC") as! InviteFriendsVC
        inviteFriendsVC.delegate = self
        self.present(inviteFriendsVC, animated: true, completion: nil)
    }
}

extension UploadVideoVC: InviteFriendsVCDelegate {
    //MARK: InviteFriendsVC Delegates...
    func didSelectFriend(user: FollowUserItem) {
        print("didSelectFriend user:",user)
        let mutableAttrString = NSMutableAttributedString(string: "")
        mutableAttrString.append(self.tagging.textView.attributedText)
        let usernameString = NSAttributedString(string: user.username ?? "")
        mutableAttrString.append(usernameString)
        self.tagging.textView.attributedText = mutableAttrString
        self.tagging.updateAttributeText(selectedLocation: self.tagging.textView.text.count - 1)
    }
}

extension UploadVideoVC: ThumbnailImage {
    func selectThumbNailImage(_ startTime: CGFloat) {
        print(startTime)
        startDurationTime = startTime
        self.thumbnailForVideoAtURL()
    }
}

extension UploadVideoVC {
    //MARK: API's...
    
    func uploadVideosAPI() {
        
        if let vurl = self.video_url, let image = self.img_thumbnail.image {
            var caption = ""
            if self.tagging.textView.textColor != UIColor.lightGray, !self.tagging.textView.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                caption = self.tagging.textView.text.trimmingCharacters(in: .whitespacesAndNewlines)
            }
            let param: Parameters = ["caption": caption, "video_type": self.permissionsStatusArray[0] ? "0" : "1", "allow_comment": self.permissionsStatusArray[1] ? "1" : "0", "allow_duet": self.permissionsStatusArray[2] ? "1" : "0", "allow_download": self.permissionsStatusArray[3] ? "1" : "0", "duet_user_id": self.duetUserID ?? "0", "duet_video_id": self.duetVideoID ?? "0","is_duet": self.isDuet]
            imageData = image.jpegData(compressionQuality: 0.50) as NSData?
            print(self.video_url!)
            print(param)
            let url = URL(string: Constants.BASE_URL+Constants.UPLOAD_VIDEO)!
            let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
            DialougeUtils.addActivityView(view: self.view)
            
            self.uploadVideosAPICall(param, url: url, header: headers, imagedata: imageData!, videoURL: self.video_url!)
        }
    }
    
    func uploadVideosAPICall(_ param: Parameters, url: URL, header: HTTPHeaders, imagedata: NSData, videoURL: URL){
        AllUserApis.uploadVideoAPI(vc: self, url: url, imgData: imagedata, header: header, videoUrl: videoURL, params: param) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status: Bool = result!["status"] as! Bool
                        if status == true{
                            UIApplication.shared.keyWindow?.makeToast(message: result!["message"] as? String ?? "Added Successfully")
                            guard let url = self.video_url else {
                                self.navigationController?.popToRootViewController(animated: true)
                                return
                            }
                            try? FileManager.default.removeItem(at: url)
                            NotificationCenter.default.post(name: NSNotification.Name(Constants.VIDEO_ADDED_NOTIFICATION), object: nil)
                            self.navigationController?.popToRootViewController(animated: true)
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                }
            }
        }
    }

    func getHashtags() {
        TransportManager.sharedInstance.getHashtags { (data, err) in
            if let error = err {
                ToastMessage.showToast(in: self, message: error.localizedDescription, bgColor: .white)
            }
            else {
                if let json = data as? JSON, let resultObj = json.dictionaryObject {
                    print("getHashtags resultObj:",resultObj)
                    var tagArray = [String]()
                    //self.hashtagsArray.removeAll()
                    let tagsArray = resultObj["data"] as? [[String : Any]] ?? []
                    for tagDict in tagsArray {
                        if let tag = tagDict["name"] as? String, tag != "" {
                            //self.hashtagsArray.append(tag)
                            tagArray.append(tag)
                        }
                    }
                    self.tagging.tagableList = tagArray
                }
            }
        }
    }
    //VIDEO_HASHTAGS
    
    func getHastagVideosAPI(){
        let url = URL(string: Constants.BASE_URL+Constants.VIDEO_HASHTAGS)!
        let headers: HTTPHeaders = ["Authentication": User.sharedInstance.auth_key ?? ""]
        print(String(User.sharedInstance.auth_key ?? ""))
        DialougeUtils.addActivityView(view: self.view)
        self.getHastagVideosAPICall(url, header: headers)
    }
    
    func getHastagVideosAPICall(_ url: URL, header: HTTPHeaders){
        LoginSignUpAPI.getUserDataAPI(vc: self, url: url, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            print("getHashtags resultObj:",result!)
                            var tagArray = [String]()
                            //self.hashtagsArray.removeAll()
                            let tagsArray = result!["user_data"] as? [[String : Any]] ?? []
                            for tagDict in tagsArray {
                                if let tag = tagDict["name"] as? String, tag != "" {
                                    tagArray.append(tag)
                                }
                            }
                            self.tagging.tagableList = tagArray
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                }
            }
        }
    }
}

