//
//  SettingsVC.swift
//  LmTek
//
//  Created by PTBLR-1128 on 26/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {
    //MARK: Outlets...
    @IBOutlet weak var tbl_settings : UITableView!

    //MARK: Variables...
    var settingsArray = [["Edit Profile", "Privacy Settings", "Language", "Categories"], ["Clear Cache", "Feedback & Help", "About us", "Rate us", "Logout"]]
    var settingsAndPrivacyArray = [(title: "ACCOUNT", array: [(image: #imageLiteral(resourceName: "ic_manageAccount"), title: "Manage my account"), (image: #imageLiteral(resourceName: "ic_security"), title: "Privacy and safety"), (image: #imageLiteral(resourceName: "ic_contentPreference"), title: "Content preferences"), (image: #imageLiteral(resourceName: "ic_wallet"), title: "Balance"), (image: #imageLiteral(resourceName: "ic_shareOutline"), title: "Share profile"), (image: #imageLiteral(resourceName: "ic_tikCode"), title: "TikCode")]), (title: "GENERAL", array: [(image: #imageLiteral(resourceName: "ic_notifications"), title: "Push notifications"), (image: #imageLiteral(resourceName: "ic_language"), title: "Language"), (image: #imageLiteral(resourceName: "ic_digitalWellbeing"), title: "Digital Wellbeing"), (image: #imageLiteral(resourceName: "ic_familyPairing"), title: "Family Pairing"), (image: #imageLiteral(resourceName: "ic_accessibility"), title: "Accessibility"), (image: #imageLiteral(resourceName: "ic_dataSaver"), title: "DataSaver")]), (title: "SUPPORT", array: [(image: #imageLiteral(resourceName: "ic_report"), title: "Report a problem"), (image: #imageLiteral(resourceName: "ic_help"), title: "Help Center"), (image: #imageLiteral(resourceName: "ic_safety"), title: "Safety Center")]), (title: "ABOUT", array: [(image: #imageLiteral(resourceName: "ic_termsOfUse"), title: "Terms of Use"), (image: #imageLiteral(resourceName: "ic_community"), title: "Community Guidelines"), (image: #imageLiteral(resourceName: "ic_privacy"), title: "Privacy Policy"), (image: #imageLiteral(resourceName: "ic_copyright"), title: "Copyright Policy")]), (title: "", array: [(image: #imageLiteral(resourceName: "ic_freeUpSpace"), title: "Free up space"), (image: #imageLiteral(resourceName: "ic_addAccount"), title: "Add account"), (image: #imageLiteral(resourceName: "ic_logout"), title: "Log out")])]
    
    //MARK: View Life Cycle...
    override func viewDidLoad() {
        super.viewDidLoad()

        self.cellRegistration()
    }
    
    //MARK: IBActions...
    @IBAction func backClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Functions...
    func cellRegistration() {
        self.tbl_settings.register(UINib(nibName: "SettingsTVCell", bundle: nil), forCellReuseIdentifier: "SettingsTVCell")
        self.tbl_settings.register(UINib(nibName: "SettingsAndPrivacyHeaderTVCell", bundle: nil), forCellReuseIdentifier: "SettingsAndPrivacyHeaderTVCell")
        self.tbl_settings.register(UINib(nibName: "SettingsAndPrivacyFooterTVCell", bundle: nil), forCellReuseIdentifier: "SettingsAndPrivacyFooterTVCell")
        self.tbl_settings.register(UINib(nibName: "SettingsAndPrivacyTVCell", bundle: nil), forCellReuseIdentifier: "SettingsAndPrivacyTVCell")
    }

}

extension SettingsVC: UITableViewDelegate, UITableViewDataSource {
    //MARK: TableView Delegates...
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.settingsArray.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var height : CGFloat
        if section == 0 {
            height = 20
        }
        else {
            height = 10
        }
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: height))
        view.backgroundColor = UIColor(hexString: "#D0D0D0")
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 20
        }
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 1 {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
            view.backgroundColor = .clear
            return view
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.settingsArray[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTVCell") as! SettingsTVCell
        
        let settings = self.settingsArray[indexPath.section]
        
        cell.lbl_title.text = settings[indexPath.row]
        
        if indexPath.row == settings.count - 1 {
            cell.lbl_separator.isHidden = true
        }
        else {
            cell.lbl_separator.isHidden = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let editProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(editProfileVC, animated: true)
                }
            }
            else if indexPath.row == 1 {
                let privacySettingsVC = self.storyboard?.instantiateViewController(withIdentifier: "PrivacySettingsVC") as! PrivacySettingsVC
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(privacySettingsVC, animated: true)
                }
            }
            else if indexPath.row == 2 {
                let chooseLanguageVC = self.storyboard?.instantiateViewController(withIdentifier: "ChooseLanguageVC") as! ChooseLanguageVC
                chooseLanguageVC.isAfterLogin = false
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(chooseLanguageVC, animated: true)
                }
            }
            else if indexPath.row == 3 {
                let chooseCategoryVC = self.storyboard?.instantiateViewController(withIdentifier: "ChooseCategoryVC") as! ChooseCategoryVC
                chooseCategoryVC.isAfterLogin = false
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(chooseCategoryVC, animated: true)
                }
            }
        }
        else {
            if indexPath.row == 0 {
                CacheManager.shared.clearCache { (result) in
                    switch result{
                    case .success(let msg):
                        ToastMessage.showToast(in: self, message: msg, bgColor: .white)
                    case .failure(let error):
                        print("caching error:",error.description)
                    }
                }
            }
            else if indexPath.row == 1 {
                let feedbackVC = self.storyboard?.instantiateViewController(withIdentifier: "FeedbackVC") as! FeedbackVC
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(feedbackVC, animated: true)
                }
            }
            else if indexPath.row == 2 {
            }
            else if indexPath.row == 3 {
            }
            else if indexPath.row == 4 {
                if #available(iOS 13.0, *) {
                    if let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate {
                        UserDefaults.standard.set(false, forKey: Constants.KEYS.isLoggedIn)
                        sceneDelegate.showLoginScreen()
                    }
                } else {
                    if let appdelegate =  UIApplication.shared.delegate as? AppDelegate {
                        UserDefaults.standard.set(false, forKey: Constants.KEYS.isLoggedIn)
                        appdelegate.showLoginScreen()
                    }
                }
            }
        }
    }
}
