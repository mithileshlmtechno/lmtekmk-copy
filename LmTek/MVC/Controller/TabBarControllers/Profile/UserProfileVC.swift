//
//  UserProfileVC.swift
//  LmTek
//
//  Created by Vijay on 23/09/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import SwiftyJSON
import AVFoundation
import AVKit
import Alamofire

protocol UserProfileVCDelegate {
    func followUser(_ isFollowing: Bool, at index: Int?, user_id: String?)
}

class UserProfileVC: UIViewController {
    
    //MARK: IBOutlets...
    
    @IBOutlet weak var scrollView_content : UIScrollView!
    @IBOutlet weak var lbl_videoDuet : UILabel!
    @IBOutlet weak var coll_videos:UICollectionView!
    
    @IBOutlet weak var lbl_userName:UILabel!
    @IBOutlet weak var lbl_profileId:UILabel!
    @IBOutlet weak var img_profile:UIImageView!
    @IBOutlet weak var btn_follow:UIButton!
    @IBOutlet weak var btn_hearts:UIButton!
    @IBOutlet weak var btn_followers:UIButton!
    @IBOutlet weak var btn_following:UIButton!
    @IBOutlet weak var img_background:UIImageView!

    @IBOutlet weak var img_noDataCheck:UIImageView!

    @IBOutlet weak var view_popup:UIView!
    @IBOutlet weak var coll_popupVideos:UICollectionView!

    @IBOutlet weak var constraint_videosListHeight : NSLayoutConstraint!
    
    @IBOutlet weak var lbl_videos : UILabel!
    @IBOutlet weak var lbl_duets : UILabel!
    
    //MARK: Variables
    
    var user_id: String = ""
    var videos_list = [GetVideosData]()//[[String:Any]]()
    var duet_list = [GetVideosData]()

    var isFollowing = false
    var delegate : UserProfileVCDelegate?
    var videoIndex : Int?
    
    //MARK: View life cycle...

    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerCells()
        
        lbl_videoDuet.frame = CGRect.init(x: 0, y: lbl_videoDuet.frame.origin.y, width: self.view.frame.width/2, height: lbl_videoDuet.frame.height)
     
        //getOtherProfileDetails(user_id: user_id)
        self.getOtherProfileAPI(user_id)
        
        view_popup.frame = self.view.bounds
        view_popup.isHidden = true
        self.view.addSubview(view_popup)
        
        self.setFollowTitle()
    }
 
    func showUserDetails(details:[String : Any]){
        
        lbl_userName.text = details["first_name"] as? String ?? ""
        
        let total_likes = "\(details["total_likes"] as? String ?? "") Post"
        let totla_followers = "\(details["total_followers"] as? String ?? "") Followers"
        let totla_following = "\(details["total_following"] as? String ?? "") Following"
        
        btn_hearts.setTitle(total_likes, for: .normal)
        btn_followers.setTitle(totla_followers, for: .normal)
        btn_following.setTitle(totla_following, for: .normal)
        
        let profile_image = details["profile_picture"] as? String ?? ""
        img_profile.sd_setImage(with: URL(string: profile_image), completed: nil)
        
        let cover_image = details["cover_photo"] as? String ?? ""
        img_background.sd_setImage(with: URL(string: cover_image), completed: nil)
                
        let arrData: [[String : Any]] = details["video_list"] as? [[String : Any]] ?? []
        
        if videos_list.count > 0 {
            videos_list.removeAll()
        }

        for i in 0..<arrData.count {
            let dictData: [String : Any] = arrData[i]
            let objdata:GetVideosData = GetVideosData.init(VideoInfo: dictData)!
            self.videos_list.append(objdata)
        }
        
        self.lbl_videos.text = "Videos \(self.videos_list.count)"
        
        self.constraint_videosListHeight.constant = .greatestFiniteMagnitude
        coll_videos.reloadData()
        coll_videos.layoutIfNeeded()
        let height = coll_videos.contentSize.height + 55.0
        self.constraint_videosListHeight.constant = (height < 250) ? 250 : height
        coll_popupVideos.reloadData()

        img_noDataCheck.isHidden = (videos_list.count == 0) ? false : true

    }
    
    func showVideo(video_link:String){
        
        guard let video_url = URL.init(string: video_link) else{ return }
        
        let avPlayerrrr = AVPlayer(url: video_url)
        let vc = AVPlayerViewController()
        vc.player = avPlayerrrr
        vc.delegate = self
        
        self.present(vc, animated: true) {
            vc.player?.play()
        }
    }
    
    func setFollowTitle() {
        if self.isFollowing {
            print("setFollowTitle if")
            self.btn_follow.setTitle("Following", for: .normal)
        }
        else {
            print("setFollowTitle else")
            self.btn_follow.setTitle("Follow", for: .normal)
        }
    }
    
}

extension UserProfileVC:AVPlayerViewControllerDelegate{
    
}

extension UserProfileVC {
    
    //MARK: API's...
    
    func getOtherProfileAPI(_ otherUserID: String){
        let url = URL(string: Constants.BASE_URL+Constants.GET_OTHER_PROFILE)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        let param: Parameters = ["user_id": otherUserID]
        DialougeUtils.addActivityView(view: self.view)
        self.getOtherProfileAPICall(param, url: url, header: headers)
    }
    
    func getOtherProfileAPICall(_ param: Parameters, url: URL, header: HTTPHeaders){
        AllUserApis.allPostDataWithoutImageAPI(vc: self, url: url, parameter: param, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status: Bool = result!["status"] as! Bool
                        if status == true{
                            if let user_data = result!["user_data"] as? [String : Any]{
                                self.showUserDetails(details: user_data)
                            }
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                }
            }
        }
    }

    func followUser(user_id: String) {
        TransportManager.sharedInstance.followUser(follower_user_id: user_id) { (data, err) in
            if let error = err {
                ToastMessage.showToast(in: self, message: error.localizedDescription, bgColor: .white)
            }
            else {
                if let json = data as? JSON, let resultObj = json.dictionaryObject {
                    print("followUser resultObj:",resultObj)
                    if resultObj["status"] as? Bool == true {
                        let message = self.isFollowing ? "Unfollowed" : "Followed"
                        UIApplication.shared.keyWindow?.makeToast(message: resultObj["message"] as? String ?? message)
//                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.PROFILE_UPDATE_NOTIFICATION), object: nil)
                        self.isFollowing = !self.isFollowing
                        self.delegate?.followUser(self.isFollowing, at: self.videoIndex, user_id: user_id)
                        self.setFollowTitle()
                    }
                }
            }
        }
    }
    
    
    func doFollowUserAPI(_ follower_user_id: String) {
        let url = URL(string: Constants.BASE_URL+Constants.FOLLOW_USER)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        print(headers)
        let param: Parameters = ["follower_user_id": follower_user_id]
        print(param)
        DialougeUtils.addActivityView(view: self.view)
        self.doFollowUserAPICall(param, url: url, header: headers,follower_user_id: follower_user_id)
    }

    func doFollowUserAPICall(_ param: Parameters, url: URL, header: HTTPHeaders,follower_user_id: String){
        AllUserApis.allPostDataWithoutImageAPI(vc: self, url: url, parameter: param, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status: Bool = result!["status"] as! Bool
                        if status == true{
//                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.PROFILE_UPDATE_NOTIFICATION), object: nil)
                            let isfollow: String = self.btn_follow.title(for: .normal) ?? ""
                            if isfollow == "Follow" {
                                self.btn_follow.setTitle("Following", for: .normal)
                            } else {
                                self.btn_follow.setTitle("Follow", for: .normal)
                            }
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                }
            }
        }
    }
}

extension UserProfileVC{
    
    //MARK: IBActions...
    
    @IBAction func backClicked(_ sender: UIButton){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func videoDuetClicked(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.4) {
            self.lbl_videoDuet.frame.origin.x = (CGFloat(sender.tag)*(self.view.frame.width/2))
            self.view.layoutIfNeeded()
        }
        
        if sender.tag == 0{
            coll_videos.isHidden = false
            img_noDataCheck.isHidden = (videos_list.count == 0) ? false : true
        }else{
            coll_videos.isHidden = true
            img_noDataCheck.isHidden =  false
        }
        
    }
    
    @IBAction func settingsClicked() {
        let settingsVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(settingsVC, animated: true)
        }
    }
    
    @IBAction func hidePopupClicked(){
        
        view_popup.isHidden = true

    }
    
    @IBAction func followClicked() {
        //self.followUser(user_id: self.user_id)
        self.doFollowUserAPI(user_id)
    }
    
}

extension UserProfileVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
   
    func registerCells(){
        
        coll_videos.register(UINib.init(nibName: "ProfileVideosCVCell", bundle: nil), forCellWithReuseIdentifier: "ProfileVideosCVCell")
        coll_popupVideos.register(UINib.init(nibName: "ProfileVideosCVCell", bundle: nil), forCellWithReuseIdentifier: "ProfileVideosCVCell")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videos_list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileVideosCVCell", for: indexPath) as! ProfileVideosCVCell
        let cover_image = videos_list[indexPath.row].thumb_url ?? ""//["image_url"] as? String ?? ""
        cell.img_thumbnail.sd_setImage(with: URL(string: cover_image as! String), completed: nil)
        
        if collectionView == coll_videos{
            cell.img_thumbnail.contentMode = .scaleAspectFill
        }else{
            cell.img_thumbnail.contentMode = .scaleToFill
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == coll_videos{
            let cellSize = CGSize.init(width: (self.view.frame.width-20)/3, height: 140)
            return cellSize
        }else{
            let cellSize = CGSize.init(width: self.view.frame.width, height: collectionView.frame.size.height)
            return cellSize
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let playVideoVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        playVideoVC.checkSearchVideo = true
        playVideoVC.videosArray = videos_list
        playVideoVC.checkVideoTag = indexPath.row
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(playVideoVC, animated: true)
        }

    }
    
}
