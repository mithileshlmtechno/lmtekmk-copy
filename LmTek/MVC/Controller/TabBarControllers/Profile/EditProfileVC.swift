//
//  EditProfileVC.swift
//  LmTek
//
//  Created by PTBLR-1128 on 27/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire


class EditProfileVC: ViewController {
    //MARK: Outlets...
    @IBOutlet weak var tbl_editProfile : UITableView!
    
    //MARK: Variables...
    var profileDetailsArray = [(title:"First Name", value: "", type: 1), (title:"Last Name", value: "", type: 1), (title:"LmTek user ID", value: "", type: 2), (title:"Gender", value: "", type: 3), (title:"DOB", value: "", type: 4), (title:"About me", value: "", type: 1)]
    var profileImage : UIImage?
    var strProfile_Image : String = ""
    var dateFormatter = DateFormatter()

    var imgData: NSData?
    
    //MARK: View Life Cycle...
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cellRegistration()
        self.dateFormatter.dateFormat = "dd-MM-yyyy"
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getUserDetails()
    }
    //MARK: IBActions...
    @IBAction func backClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveClicked() {
        self.uodateProfileAPI()
    }
    
    //MARK: Functions...
    func cellRegistration() {
        self.tbl_editProfile.register(UINib(nibName: "ProfileImageTVCell", bundle: nil), forCellReuseIdentifier: "ProfileImageTVCell")
        self.tbl_editProfile.register(UINib(nibName: "ProfileDetailsTVCell", bundle: nil), forCellReuseIdentifier: "ProfileDetailsTVCell")
        self.tbl_editProfile.register(UINib(nibName: "EditProfileTextFieldTVCell", bundle: nil), forCellReuseIdentifier: "EditProfileTextFieldTVCell")
        self.tbl_editProfile.register(UINib(nibName: "EditProfilePickerTVCell", bundle: nil), forCellReuseIdentifier: "EditProfilePickerTVCell")
        self.tbl_editProfile.rowHeight = UITableView.automaticDimension
        self.tbl_editProfile.estimatedRowHeight = 40
    }
    
    func showProfileDetails(profile: GetProfileDataModel) {
        profileDetailsArray[0].value = String(describing: profile.first_name ?? "")
        profileDetailsArray[1].value = String(describing: profile.last_name ?? "")
        profileDetailsArray[2].value = String(describing: profile.username ?? "")
        profileDetailsArray[3].value = String(describing: profile.gender ?? "")
        profileDetailsArray[4].value = String(describing: profile.dob ?? "")
        profileDetailsArray[5].value = String(describing: profile.about ?? "")
        strProfile_Image = String(describing: profile.profile_picture ?? "")
        self.tbl_editProfile.reloadData()
    }
}

extension EditProfileVC: UITableViewDelegate, UITableViewDataSource {
    //MARK: TableView Delegates...
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.profileDetailsArray.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileImageTVCell") as! ProfileImageTVCell
        cell.btn_edit.addTarget(self, action: #selector(editClicked), for: .touchUpInside)
        if let _ = self.profileImage {
            cell.img_profile.image = self.profileImage
        }
        else {
            cell.img_profile.sd_setImage(with: URL(string: strProfile_Image), completed: nil)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileDetailsTVCell") as! ProfileDetailsTVCell
        
        let model = self.profileDetailsArray[indexPath.row]
        if model.type == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileDetailsTVCell") as! ProfileDetailsTVCell
            cell.lbl_title.text = model.title
            cell.lbl_value.text = model.value
            
            if indexPath.row == 2 {
                cell.btn_copy.isHidden = false
                cell.constraint_copyButtonWidth.constant = 50
            }
            else {
                cell.btn_copy.isHidden = true
                cell.constraint_copyButtonWidth.constant = 0
            }
            
            return cell
        }
        else if model.type == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditProfileTextFieldTVCell") as! EditProfileTextFieldTVCell
            cell.delegate = self
            cell.lbl_title.text = model.title
            cell.txt_value.text = model.value
            switch indexPath.row {
            case 0:
                cell.txt_value.placeholder = "First Name"
            case 1:
                cell.txt_value.placeholder = "Last Name"
            default:
                break
            }

            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditProfilePickerTVCell") as! EditProfilePickerTVCell
            
            cell.lbl_title.text = model.title
            cell.btn_picker.tag = indexPath.row
            cell.btn_picker.addTarget(self, action: #selector(self.pickerClicked), for: .touchUpInside)
            cell.btn_picker.setTitleColor(UIColor.black, for: .normal)
            if model.type == 3 {
                switch model.value {
                case "1":
                    cell.btn_picker.setTitle("Male", for: .normal)
                case "2":
                    cell.btn_picker.setTitle("Female", for: .normal)
                case "3":
                    cell.btn_picker.setTitle("Other", for: .normal)
                default:
                    cell.btn_picker.setTitle("Select Gender", for: .normal)
                    cell.btn_picker.setTitleColor(UIColor.lightGray, for: .normal)
                }
            }
            else {
                if model.value == "" {
                    cell.btn_picker.setTitle("Select Date", for: .normal)
                    cell.btn_picker.setTitleColor(UIColor.lightGray, for: .normal)
                }
                else {
                    cell.btn_picker.setTitle(model.value, for: .normal)
                }
            }
            return cell
        }
    }
    
    @objc func editClicked() {
        let alertController = UIAlertController(title: "Select image from", message: nil, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            DispatchQueue.main.async {
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        alertController.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            DispatchQueue.main.async {
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @objc func pickerClicked(_ sender: UIButton) {
        let model = self.profileDetailsArray[sender.tag]
        if model.type == 3 {
            let selectGenderView = Bundle.main.loadNibNamed("SelectGenderView", owner: nil, options: nil)![0] as! SelectGenderView
            selectGenderView.delegate = self
            if let row = Int(self.profileDetailsArray[3].value) {
                selectGenderView.picker_gender.selectRow(row - 1, inComponent: 0, animated: false)
            }
            DispatchQueue.main.async {
                let window = UIApplication.shared.keyWindow!
                window.addSubview(selectGenderView)
            }
        }
        else if model.type == 4 {
            let selectDOBView = Bundle.main.loadNibNamed("SelectDOBView", owner: nil, options: nil)![0] as! SelectDOBView
            selectDOBView.delegate = self
            selectDOBView.datePicker.maximumDate = Date()
            selectDOBView.datePicker.date = self.dateFormatter.date(from: self.profileDetailsArray[4].value) ?? Date()
            DispatchQueue.main.async {
                let window = UIApplication.shared.keyWindow!
                window.addSubview(selectDOBView)
            }
        }
    }
}


extension EditProfileVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    //MARK: ImagePicker Delegates...
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.editedImage] as? UIImage {
            self.profileImage = image
            self.tbl_editProfile.reloadData()
            self.uploadProfilePictureAPI(image: image)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}


extension EditProfileVC: EditProfileDetailDelegate {
    //MARK: EditProfileDetail Delegates...
    func textDidChange(cell: EditProfileTextFieldTVCell, textField: UITextField) {
        if let indexPath = self.tbl_editProfile.indexPath(for: cell) {
            self.profileDetailsArray[indexPath.row].value = textField.text ?? ""
        }
    }
}


extension EditProfileVC: SelectGenderViewDelegate, SelectedDOBViewDelegate {
    //MARK: SelectGenderView Delegates...
    func selectedGender(gender: Int) {
        self.profileDetailsArray[3].value = "\(gender + 1)"
        DispatchQueue.main.async {
            let contentOffset = self.tbl_editProfile.contentOffset
            self.tbl_editProfile.reloadRows(at: [IndexPath(row: 3, section: 0)], with: .none)
            self.tbl_editProfile.layoutIfNeeded()
            self.tbl_editProfile.setContentOffset(contentOffset, animated: false)
        }
    }
    
    //MARK: SelectedDOBView Delegates...
    func selectedDate(date: Date) {
        self.profileDetailsArray[4].value = self.dateFormatter.string(from: date)
        DispatchQueue.main.async {
            let contentOffset = self.tbl_editProfile.contentOffset
            //self.tbl_editProfile.beginUpdates()
            self.tbl_editProfile.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .none)
            //self.tbl_editProfile.beginUpdates()
            self.tbl_editProfile.layoutIfNeeded()
            self.tbl_editProfile.setContentOffset(contentOffset, animated: false)
        }
    }
}

extension EditProfileVC {
    //MARK: API's...
    
    func getUserDetails() {
        let url = URL(string: Constants.BASE_URL+Constants.GET_USER_DETAILS)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        DialougeUtils.addActivityView(view: self.view)
        self.getUserDataAPI(url, header: headers)
    }
    
    func getUserDataAPI(_ url: URL, header: HTTPHeaders){
        LoginSignUpAPI.getUserDataAPI(vc: self, url: url, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            let arrdata:[String: Any] = result!["data"] as! [String : Any]
                            let objdata: GetProfileDataModel = GetProfileDataModel.init(ProfileInfo: arrdata)!
                                self.showProfileDetails(profile: objdata)
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                }
            }
        }
    }
    
    func uodateProfileAPI() {
        var params = [String : String]()
        params["first_name"] = self.profileDetailsArray[0].value
        params["last_name"] = self.profileDetailsArray[1].value
        params["gender"] = self.profileDetailsArray[3].value
        params["dob"] = self.profileDetailsArray[4].value
        params["about"] = self.profileDetailsArray[5].value

        let url = URL(string: Constants.BASE_URL+Constants.UPDATE_USER_PROFILE)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        DialougeUtils.addActivityView(view: self.view)
        self.updateProfileAPICall(params, url: url, header: headers)
    }
    
    func updateProfileAPICall(_ param: Parameters, url: URL, header: HTTPHeaders){
        LoginSignUpAPI.SendOTPAPI(vc: self, parameter: param, url: url, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status: Bool = result!["status"] as! Bool
                        if status == true{
                            NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: Constants.PROFILE_UPDATE_NOTIFICATION), object: nil)
                            self.navigationController?.popViewController(animated: true)
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                }
            }
        }
    }
    
    func uploadProfilePictureAPI(image: UIImage) {
        let img:UIImage = image
        self.imgData = img.jpegData(compressionQuality: 0.50) as NSData?

        let url = URL(string: Constants.BASE_URL+Constants.UPDATE_PROFILE_PICTURE)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        DialougeUtils.addActivityView(view: self.view)
        self.UploadProfilePhotoAPICall(url, header: headers, imagedata: imgData!)
    }
    
    func UploadProfilePhotoAPICall(_ url: URL, header: HTTPHeaders, imagedata: NSData) {
        AllUserApis.uploadUserProfilePhotoAPI(vc: self, url: url, imgData: imagedata, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: Constants.PROFILE_UPDATE_NOTIFICATION), object: nil)
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                }
            }
        }
    }
}
