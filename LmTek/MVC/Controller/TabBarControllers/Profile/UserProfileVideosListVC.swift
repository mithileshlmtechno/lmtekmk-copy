//
//  UserProfileVideosListVC.swift
//  LmTek
//
//  Created by PTBLR-1128 on 12/10/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import SwiftyJSON
import Photos
import AVFoundation

class UserProfileVideosListVC: UIViewController {
    //MARK: Outlets...
    @IBOutlet weak var collection_videos : UICollectionView!
    @IBOutlet weak var btn_back : UIButton!
    
    //MARK: Variables...
    var videosArray = [GetVideosData]()//[[String : Any]]()
    var selectedIndex = 0
    var isInitial = true
    var downloadProgressView : DownloadProgressView?

    //MARK: View Life Cycle...
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.cellRegistration()
        self.btn_back.setImage(#imageLiteral(resourceName: "ic_back").withRenderingMode(.alwaysTemplate), for: .normal)
        self.btn_back.tintColor = UIColor.white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        DispatchQueue.main.async {
            self.collection_videos.scrollToItem(at: IndexPath(item: self.selectedIndex, section: 0), at: .centeredVertically, animated: false)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                self.pausePlayeVideos()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        ASVideoPlayerController.sharedVideoPlayer.removePlayer(collectionView: self.collection_videos)
    }
    
    func likeVideo(at index: Int, video_model: GetVideosData) {
        TransportManager.sharedInstance.likeVideo(video_id: String(describing: video_model.video_id ?? ""), user_id: video_model.user_id as! String) { (data, err) in
            if let error = err {
                print("likeVideo error:",error.localizedDescription)
                ToastMessage.showToast(in: self, message: error.localizedDescription, bgColor: .white)
            }
            else {
                if let json = data as? JSON, let resultObj = json.dictionaryObject {
                    print("likeVideo resultObj:",resultObj)
                    if resultObj["status"] as? Bool == true {
                        let data = resultObj["data"] as? [String : Any] ?? [:]
                        var model : GetVideosData
                            //self.videosArray[index].likes = data["likes"] as? Int ?? 0//String ?? "0"
                            //self.videosArray[index].isLiked = (resultObj["message"] as? String == "Liked") ? "1" : "0"//(video_model.is_liked == "1") ? "0" : "1"
                            model = self.videosArray[index]

                        let cell = self.collection_videos.cellForItem(at: IndexPath(item: index, section: 0)) as? HomeCVCell
                        cell?.loadVideoDetails(model: model)
                    }
                }
            }
        }
    }
    
    func doShareVideo(video_model: GetVideosData, at index: Int) {
        TransportManager.sharedInstance.doShare(video_id: video_model.video_id as! String) { (data, err) in
            if let error = err {
                ToastMessage.showToast(in: self, message: error.localizedDescription, bgColor: .white)
            }
            else {
                if let json = data as? JSON, let resultObj = json.dictionaryObject {
                    print("doShareVideo resultObj:",resultObj)
                    if resultObj["status"] as? Bool == true {
                        let data = resultObj["data"] as? [String : Any] ?? [:]
                        //self.videosArray[index].shares = data["share"] as? Int//String
                        
                        let homeCell = self.collection_videos.cellForItem(at: IndexPath(item: index, section: 0)) as? HomeCVCell
                        homeCell?.loadVideoDetails(model: self.videosArray[index])
                        
                    }
                }
            }
        }
    }
    
    func downloadVideo(url: URL) {
        print("downloadVideo url:",url)
        let session = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
        let downloadTask = session.downloadTask(with: url)
        downloadTask.resume()
        self.downloadProgressView = Bundle.main.loadNibNamed("DownloadProgressView", owner: nil, options: nil)![0] as! DownloadProgressView
        self.view.addSubview(self.downloadProgressView!)
    }
    
    //MARK: IBActions...
    @IBAction func backClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Functions...
    func cellRegistration() {
        self.collection_videos.register(UINib(nibName: "HomeCVCell", bundle: nil), forCellWithReuseIdentifier: "HomeCVCell")
    }
    
}

extension UserProfileVideosListVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    //MARK: CollectionView Delegates...
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.videosArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCVCell", for: indexPath) as! HomeCVCell
        
        cell.invalidateIntrinsicContentSize()
        cell.layoutIfNeeded()
        cell.delegate = self
        var model : GetVideosData
        model = self.videosArray[indexPath.item]
        cell.loadVideoDetails(model: model)
                        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let videoCell = cell as? ASAutoPlayVideoLayerContainer, let _ = videoCell.videoURL {
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let videoCell = cell as? HomeCVCell {
            videoCell.img_play.isHidden = true
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    func pausePlayeVideos() {
        print("pausePlayeVideos")
        ASVideoPlayerController.sharedVideoPlayer.pausePlayeVideosFor(collectionView: self.collection_videos)
    }
}

extension UserProfileVideosListVC: UIScrollViewDelegate {
    //MARK:- ScrollView Delegates...
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.pausePlayeVideos()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            self.pausePlayeVideos()
        }
    }
}

extension UserProfileVideosListVC : HomeCVCellDelegate {
    //MARK:- HomeCVCell Delegates...
    func commentClicked(cell: HomeCVCell) {
        print("commentClicked")
        if let indexPath = self.collection_videos.indexPath(for: cell) {
            let commentsView = Bundle.main.loadNibNamed("CommentsView", owner: nil, options: nil)![0] as! CommentsView
                commentsView.video_model = self.videosArray[indexPath.item]
            
            commentsView.tag = indexPath.item
            commentsView.delegate = self
            let window = UIApplication.shared.keyWindow!
            window.addSubview(commentsView)
        }
    }
    
    func likeClicked(cell: HomeCVCell) {
        print("likeClicked")
        if let indexPath = self.collection_videos.indexPath(for: cell) {
            self.likeVideo(at: indexPath.item, video_model: self.videosArray[indexPath.item])
        }
    }
    
    func reportClicked(cell: HomeCVCell) {
        print("reportClicked")
    }
    
    func profileImageClicked(cell: HomeCVCell) {
        print("profileImageClicked")
    }
    
    func shareClicked(cell: HomeCVCell) {
        print("shareClicked")
        guard let indexPath = self.collection_videos.indexPath(for: cell) else {
            if let tabBar = self.tabBarController?.tabBar, let items = tabBar.items {
                self.tabBarController?.tabBar(tabBar, didSelect: items[Constants.previousTab])
            }
            return
        }
        var video_model : GetVideosData
        video_model = self.videosArray[indexPath.item]
        
        print("shareClicked:",video_model)
        if let url = URL(string: String(describing: video_model.video_url ?? "")) {
            let linkToShare = [url]
            let activityViewController = UIActivityViewController(activityItems: linkToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view // so that ipad won't crash
            
            activityViewController.completionWithItemsHandler = {(activityType: UIActivity.ActivityType?, completed: Bool, returnedItems:[Any]?, error: Error?) in
                
                print("activityType:",activityType, ", completed:",completed)
                if let type = activityType {
                    self.doShareVideo(video_model: video_model, at: indexPath.item)
                }
            }
            DispatchQueue.main.async {
                self.present(activityViewController, animated: true, completion: nil)
            }
        }
    }
    
    func downloadClicked(cell: HomeCVCell) {
        print("downloadClicked")
            if let indexPath = self.collection_videos.indexPath(for: cell) {
                var video_model : GetVideosData
                    video_model = self.videosArray[indexPath.item]
                
                if video_model.allow_download as! String == "1" {
                    
                    if let video_url = URL(string: String(describing: video_model.video_url ?? "")) {
                        self.downloadVideo(url: video_url)
                    }
                }
            }
        
    }
    
    func followClicked(cell: HomeCVCell) {
        print("followClicked")
    }
    
    func duetClicked(cell: HomeCVCell) {
        print("duetClicked")
    }
    
    
}

extension UserProfileVideosListVC: URLSessionDownloadDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        print("didFinishDownloadingTo")
        DispatchQueue.main.async {
            self.downloadProgressView?.removeFromSuperview()
            self.downloadProgressView = nil
        }
        guard let nsData = NSData(contentsOf: location), let data = nsData as Data? else { return }
        let videoAsset : AVAsset = data.getAVAsset()
        self.mixVideoAsset(videoAsset: videoAsset)
        return
        guard let watermarkImage = UIImage.gifImageWithName("lmtek-watermark") else {
            print("watermark error")
            return
        }
        guard let url = Bundle.main.url(forResource: "lmtek-watermark", withExtension: "gif"), let gifData = NSData(contentsOf: url) as Data? else { return }
        let tempURL = URL(fileURLWithPath: NSTemporaryDirectory().appending("temp.mp4"))
        GIF2MP4(data: gifData)?.convertAndExport(to: tempURL, completion: {
            let gifAsset = AVAsset(url: tempURL)
        })
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        print("downloadTask didWriteData bytesWritten:",bytesWritten," totalBytesWritten:",totalBytesWritten," totalBytesExpectedToWrite:",totalBytesExpectedToWrite)
        let progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        DispatchQueue.main.async {
            self.downloadProgressView?.progress_download.progress = progress
        }
    }
    
    func mixVideoAsset(videoAsset: AVAsset) {
        var mutableVideoComposition = AVMutableVideoComposition()
        var mixComposition = AVMutableComposition()
                
        let videoTrack = videoAsset.tracks(withMediaType: .video)[0]
                
        let mutableCompositionVideoTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)!
        
        do {
            try mutableCompositionVideoTrack.insertTimeRange(CMTimeRange(start: .zero, duration: videoAsset.duration), of: videoTrack, at: .zero)
        }
        catch {
            print("savedmutabletrack error:",error.localizedDescription)
        }
        
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRange(start: .zero, duration: videoAsset.duration)
        
        let transform = videoTrack.preferredTransform
        let assetInfo = orientationFromTransform(transform)
        var videoSize : CGSize
        if assetInfo.isPortrait {
            videoSize = CGSize(width: videoTrack.naturalSize.height, height: videoTrack.naturalSize.width)
        }
        else {
            videoSize = videoTrack.naturalSize
        }
        print("assetInfo:",assetInfo)
        let videoLayerInstruction = self.compositionLayerInstruction(
        for: mutableCompositionVideoTrack,
        assetTrack: videoTrack)
        
        mainInstruction.layerInstructions = [videoLayerInstruction]
        
        mutableVideoComposition.instructions = [mainInstruction]
        mutableVideoComposition.frameDuration = CMTime(value: 1, timescale: 30)
        let width = UIScreen.main.bounds.width
        let compositionWidth = (Int(width) % 2 == 0) ? width : width - 1
        mutableVideoComposition.renderSize = videoSize
        if let audioTrackFirst = videoAsset.tracks(withMediaType: .audio).first {
            let audioTrack = mixComposition.addMutableTrack(
                withMediaType: .audio,
                preferredTrackID: kCMPersistentTrackID_Invalid)
            
            do {
                
                try audioTrack?.insertTimeRange(
                    CMTimeRangeMake(
                        start: CMTime.zero,
                        duration: videoAsset.duration),
                    of: /*savedVideoAsset.tracks(withMediaType: .audio)[0]*/audioTrackFirst,
                    at: .zero)
            } catch {
                print("Failed to load Audio track")
            }
        }
                
        self.applyVideoEffectsToComposition(composition: mutableVideoComposition, size: videoSize, duration: videoAsset.duration)//naturalSize)
        
        let savePathURL = URL(fileURLWithPath: NSHomeDirectory() + "/Documents/camRecordedVideo.mp4")
        do {
            try FileManager.default.removeItem(at: savePathURL)
        }
        catch {
            print("file remove error:",error.localizedDescription)
        }
        
        DispatchQueue.main.async {
            let window = UIApplication.shared.keyWindow!
            DialougeUtils.addActivityView(view:window)
            let finalPath = savePathURL.absoluteString
            let assetExport = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)!
            assetExport.videoComposition = mutableVideoComposition
            assetExport.outputFileType = .mp4
            
            assetExport.outputURL = savePathURL
            assetExport.shouldOptimizeForNetworkUse = true
            
            assetExport.exportAsynchronously(completionHandler: {
                print("duet merge exporter")
                DialougeUtils.removeActivityView(view: window)
                switch assetExport.status {
                case .completed:
                    print("success")
                    DispatchQueue.global(qos: .background).async {
                        if let videoURL = assetExport.outputURL, let videoData = NSData(contentsOf: videoURL) {
                            print("downloadClicked videoURL:")
                            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                            let filePath = "\(documentsPath)/\(Int64(Date().timeIntervalSince1970)).mp4"
                            DispatchQueue.main.async {
                                videoData.write(toFile: filePath, atomically: true)
                                PHPhotoLibrary.shared().performChanges({
                                    print("downloadClicked performChanges")
                                    PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: filePath))
                                }) { (completed, error) in
                                    print("downloadClicked completed:",completed," , error:",error)
                                    if completed {
                                        let url = URL(fileURLWithPath: filePath) //{
                                        //let asset = AVAsset(url: url)
                                        let linkToShare = [url]
                                        let activityViewController = UIActivityViewController(activityItems: linkToShare, applicationActivities: nil)
                                        activityViewController.popoverPresentationController?.sourceView = self.view // so that ipad won't crash
                                        
                                        DispatchQueue.main.async {
                                            self.present(activityViewController, animated: true, completion: nil)
                                        }
                                        //}
                                    }
                                }
                            }
                        }
                    }
                case .failed:
                    print("failed",assetExport.error)
                case .cancelled:
                    print("cancelled",assetExport.error)
                default:
                    print("default switch")
                }
            })
        }
    }
    
    func applyVideoEffectsToComposition(composition: AVMutableVideoComposition, size: CGSize, duration: CMTime) {
        let parentLayer = CALayer()
        let videoLayer = CALayer()
        parentLayer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        videoLayer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        parentLayer.addSublayer(videoLayer)
        
        var size1 = size
        
        size1.width = 100
        size1.height = 100
        
        /*let overLayer = CALayer()
        overLayer.frame = CGRect(x: 0, y: videoLayer.frame.size.height - 100, width: size1.width, height: size1.height)
        overLayer.backgroundColor = UIColor.clear.cgColor
        
        let overLayer2 = CALayer()
        overLayer2.frame = CGRect(x: videoLayer.frame.size.width - 100, y: 0, width: size1.width, height: size1.height)
        overLayer2.backgroundColor = UIColor.clear.cgColor*/
        
        guard let fileURL = Bundle.main.url(forResource: "lmtek-watermark", withExtension: "gif") else { return }
        
        let animation = animationGIFWithURL(url: fileURL)
        let animationDuration = animation.duration
        
        var numberOfAnimation = 1
        if duration.seconds > animationDuration {
            numberOfAnimation = Int(ceil(duration.seconds / animationDuration))
        }
        
        for i in 0 ..< numberOfAnimation {
            let overLayer = CALayer()
            overLayer.backgroundColor = UIColor.clear.cgColor
            let textLayer = CATextLayer()
            textLayer.backgroundColor = UIColor.clear.cgColor
            let attributedText = NSAttributedString(
            string: "@lmtek",
            attributes: [
              .font: UIFont(name: "OpenSans-Regular", size: 20) as Any,
              .foregroundColor: UIColor.white,
              .strokeColor: UIColor(hexString: Constants.ORANGE_COLOR)!,
              .strokeWidth: -1])
            textLayer.string = attributedText
            if i % 2 == 0 {
                overLayer.frame = CGRect(x: 0, y: videoLayer.frame.size.height - 100, width: size1.width, height: size1.height)
                textLayer.frame = CGRect(x: 0, y: overLayer.frame.origin.y - 5, width: size.width, height: 30)
                textLayer.alignmentMode = .left
            }
            else {
                overLayer.frame = CGRect(x: videoLayer.frame.size.width - 100, y: 0, width: size1.width, height: size1.height)
                textLayer.frame = CGRect(x: 0, y: 0, width: size.width, height: 30)
                textLayer.alignmentMode = .right
            }
            animation.beginTime = i == 0 ? AVCoreAnimationBeginTimeAtZero : Double(i) * animationDuration
            self.startGifAnimation(animation: animation, inLayer: overLayer)
            textLayer.beginTime = i == 0 ? AVCoreAnimationBeginTimeAtZero : Double(i) * animationDuration
            textLayer.duration = animationDuration
            textLayer.displayIfNeeded()
            parentLayer.addSublayer(overLayer)
            parentLayer.addSublayer(textLayer)
        }
        
        composition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: parentLayer)
    }
    
    func startGifAnimation(/*url: URL,*/animation: CAKeyframeAnimation, inLayer layer: CALayer) {
        layer.add(animation, forKey: "contents")
    }
    
    func animationGIFWithURL(url: URL) -> CAKeyframeAnimation {
        let animation = CAKeyframeAnimation(keyPath: "contents")
        
        var frames = [CGImage]()
        var delayTimes = [CGFloat]()
        
        var totalTime : CGFloat = 0.0
        var gifWidth : CGFloat = 0.0
        var gifHeight : CGFloat = 0.0
        let gifSource = CGImageSourceCreateWithURL(url as CFURL, nil)!
        
        let frameCount = CGImageSourceGetCount(gifSource)
        
        for i in 0 ..< frameCount {
            guard let frame = CGImageSourceCreateImageAtIndex(gifSource, i, nil) else { continue }
            frames.append(frame)
            
            let cfDictionary = CGImageSourceCopyPropertiesAtIndex(gifSource, i, nil) as? [AnyHashable : Any] ?? [:]
            
            gifWidth = cfDictionary[kCGImagePropertyPixelWidth] as? CGFloat ?? 0.0
            gifHeight = cfDictionary[kCGImagePropertyPixelHeight] as? CGFloat ?? 0.0
            
            let gifDict = cfDictionary[kCGImagePropertyGIFDictionary] as? [AnyHashable : Any] ?? [:]
            delayTimes.append(gifDict[kCGImagePropertyGIFDelayTime] as? CGFloat ?? 0.0)
            
            totalTime = totalTime + (gifDict[kCGImagePropertyGIFDelayTime] as? CGFloat ?? 0.0)
        }
        
        var times = [NSNumber](repeating: 0, count: 3)
        var currentTime : CGFloat = 0.0
        let count = delayTimes.count
        
        for i in 0 ..< count {
            times.append(NSNumber(value: Float((currentTime / totalTime))))
            currentTime += delayTimes[i]
        }
        
        var images = [Any](repeating: 0, count: 3)
        for i in 0 ..< count {
            if i < frames.count {
                images.append(frames[i])
            }
        }
        
        animation.keyTimes = times
        animation.values = images
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = CFTimeInterval(totalTime)
        animation.repeatCount = 1//.greatestFiniteMagnitude
        animation.isRemovedOnCompletion = true//false
        
        return animation
    }
        
    func orientationFromTransform(
      _ transform: CGAffineTransform
    ) -> (orientation: UIImage.Orientation, isPortrait: Bool) {
        var assetOrientation = UIImage.Orientation.up
        var isPortrait = false
        let tfA = transform.a
        let tfB = transform.b
        let tfC = transform.c
        let tfD = transform.d
        
        if tfA == 0 && tfB == 1.0 && tfC == -1.0 && tfD == 0 {
            print("orientationFromTransform 1")
            assetOrientation = .right
            isPortrait = true
        } else if tfA == 0 && tfB == -1.0 && tfC == 1.0 && tfD == 0 {
            print("orientationFromTransform 2")
            assetOrientation = .left
            isPortrait = true
        } else if tfA == 1.0 && tfB == 0 && tfC == 0 && tfD == 1.0 {
            print("orientationFromTransform 3")
            assetOrientation = .up
        } else if tfA == -1.0 && tfB == 0 && tfC == 0 && tfD == -1.0 {
            print("orientationFromTransform 4")
            assetOrientation = .down
        }
        else if tfA == 0.0 && tfB == 1.0 && tfC == 1.0 && tfD == 0.0 {
            print("orientationFromTransform 5")
            assetOrientation = .rightMirrored
            isPortrait = true
        }
        else {
            print("orientationFromTransform else:",tfA, tfB, tfC, tfD)
        }
        return (assetOrientation, isPortrait)
    }
    
    private func compositionLayerInstruction(for track: AVCompositionTrack, assetTrack: AVAssetTrack) -> AVMutableVideoCompositionLayerInstruction {
      let instruction = AVMutableVideoCompositionLayerInstruction(assetTrack: track)
      let transform = assetTrack.preferredTransform

      instruction.setTransform(transform, at: .zero)

      return instruction
    }
}

extension UserProfileVideosListVC: CommentsViewDelegate {
    //MARK: CommentsView Delegates...
    func updateComments(view: CommentsView) {
    }
}
