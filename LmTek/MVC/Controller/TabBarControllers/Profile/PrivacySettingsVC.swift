//
//  PrivacySettingsVC.swift
//  LmTek
//
//  Created by PTBLR-1128 on 27/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class PrivacySettingsVC: UIViewController {
    //MARK: Outlets...
    @IBOutlet weak var tbl_privacySettings : UITableView!
    
    //MARK:- Variables...
    var privacySettingsArray = [[(title: "Allow Others to Find Me", status: "If you disable “Allow Others to Find Me”, Other users will not receive suggestions to follow you")], [(title: "Private Account", status: "When your account is private, only people you approve Can see your videos. Your existing followers won’t be affected")], [(title: "Who Can Download My Video", status: "Friends"), (title: "Who Can Send Me Comments", status: "Everyone"), (title: "Who Can React to Me", status: "Everyone"), (title: "Who Can Duet With Me", status: "Everyone"), (title: "Who Can Send Me Messages", status: "Friends"), (title: "Block List", status: "")]]
    
    var privacySettingModel : PrivacySettingItem?
    
    enum PrivacySettingType {
        case ALLOW_OTHERS_TO_FIND_ME
        case PRIVATE_ACCOUNT
        case CAN_DOWNLOAD_VIDEO
        case CAN_SEND_COMMENTS
        case CAN_REACTE_TO_ME
        case CAN_DUET_WITH_ME
        case CAN_SEND_MESSAGE
        case BLOCK_LIST
    }

    //MARK:- View Life Cycle...
    override func viewDidLoad() {
        super.viewDidLoad()

        self.cellRegistration()
        self.getPrivacySettingAPI()
    }
    

    //MARK:- IBActions...
    @IBAction func backClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Functions...
    func cellRegistration() {
        self.tbl_privacySettings.register(UINib(nibName: "PrivacySettingsSwitchTVCell", bundle: nil), forCellReuseIdentifier: "PrivacySettingsSwitchTVCell")
        self.tbl_privacySettings.register(UINib(nibName: "PrivacySettingsTVCell", bundle: nil), forCellReuseIdentifier: "PrivacySettingsTVCell")
        
        self.tbl_privacySettings.rowHeight = UITableView.automaticDimension
        self.tbl_privacySettings.estimatedRowHeight = 40
    }

}

extension PrivacySettingsVC : UITableViewDelegate, UITableViewDataSource {
    //MARK:- TableView Delegates...
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.privacySettingsArray.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var height : CGFloat
        if section == 0 {
            height = 20
        }
        else {
            height = 10
        }
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: height))
        
        view.backgroundColor = UIColor(hexString: "#D0D0D0")
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 20
        }
        return 10
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.privacySettingsArray[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let privacySettings = self.self.privacySettingsArray[indexPath.section]
        let model = privacySettings[indexPath.row]
        if indexPath.section != 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PrivacySettingsSwitchTVCell") as! PrivacySettingsSwitchTVCell
            
            cell.lbl_title.text = model.title
            cell.lbl_description.text = model.status
            
            cell.switch_setting.tag = indexPath.section
            cell.switch_setting.addTarget(self, action: #selector(self.privacySettingSwitchClicked), for: .valueChanged)
            
            if indexPath.section == 0 {
                if self.privacySettingModel?.allow_other_to_find_me == "1" {
                    cell.switch_setting.isOn = true
                }
                else {
                    cell.switch_setting.isOn = false
                }
            }
            else {
                if self.privacySettingModel?.is_private == "1" {
                    cell.switch_setting.isOn = true
                }
                else {
                    cell.switch_setting.isOn = false
                }
            }
            
            if cell.switch_setting.isOn {
                cell.switch_setting.borderColor = UIColor(hexString: Constants.PRIVACY_SETTING_SWITCH_BORDER_ORANGE_COLOR)
                cell.switch_setting.thumbTintColor = UIColor(hexString: Constants.PRIVACY_SETTING_SWITCH_THUMB_ORANGE_COLOR)
            }
            else {
                cell.switch_setting.borderColor = UIColor(hexString: Constants.PRIVACY_SETTING_SWITCH_BORDER_BLACK_COLOR)
                cell.switch_setting.thumbTintColor = UIColor(hexString: Constants.PRIVACY_SETTING_SWITCH_THUMB_BLACK_COLOR)
            }
            
            if let _ = self.privacySettingModel {
                cell.switch_setting.isHidden = false
            }
            else {
                cell.switch_setting.isHidden = true
            }
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PrivacySettingsTVCell") as! PrivacySettingsTVCell
            
            cell.lbl_title.text = model.title
            
            cell.lbl_status.text = model.status
            
            if indexPath.row == privacySettings.count - 1 {
                cell.lbl_separator.isHidden = true
            }
            else {
                cell.lbl_separator.isHidden = false
            }
            
            if let settingModel = self.privacySettingModel {
                //cell.switch_setting.isHidden = false
                var status = ""
                print("privacy setting model not nil:")
                switch indexPath.row {
                case 0:
                    status = self.getStatus(status: settingModel.video_download_security ?? "0")
                case 1:
                    status = self.getStatus(status: settingModel.comment_security ?? "0")
                case 2:
                    status = self.getStatus(status: settingModel.react_security ?? "0")
                case 3:
                    status = self.getStatus(status: settingModel.duet_security ?? "0")
                case 4:
                    status = self.getStatus(status: settingModel.message_security ?? "0")
                default:
                    status = ""
                }
                cell.lbl_status.text = status
            }
            else {
                cell.lbl_status.text = ""
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2 {
            if indexPath.row != self.privacySettingsArray[indexPath.section].count - 1 {
                let privacySettingAlertView = Bundle.main.loadNibNamed("PrivacySettingAlertView", owner: nil, options: nil)![0] as! PrivacySettingAlertView
                privacySettingAlertView.tag = indexPath.row
                privacySettingAlertView.delegate = self
                var status = 0
                switch indexPath.row {
                case 0:
                    status = Int(self.privacySettingModel?.video_download_security ?? "") ?? 0
                case 1:
                    status = Int(self.privacySettingModel?.comment_security ?? "") ?? 0
                case 2:
                    status = Int(self.privacySettingModel?.react_security ?? "") ?? 0
                case 3:
                    status = Int(self.privacySettingModel?.duet_security ?? "") ?? 0
                case 4:
                    status = Int(self.privacySettingModel?.message_security ?? "") ?? 0
                default:
                    break
                }
                privacySettingAlertView.selectedStatus = status
                privacySettingAlertView.title = self.privacySettingsArray[indexPath.section][indexPath.row].title
                let window = UIApplication.shared.keyWindow!
                window.addSubview(privacySettingAlertView)
            }
        }
    }
    
    @objc func privacySettingSwitchClicked(_ switch_setting: UISwitch) {
        let status = switch_setting.isOn ? "1" : "0"
        if switch_setting.tag == 0 {
            self.uodatePrivacySettingAPI("allow_other_to_find_me", security_status: String(status), Types: .ALLOW_OTHERS_TO_FIND_ME)
        }
        else {
            self.uodatePrivacySettingAPI("is_private", security_status: String(status), Types: .PRIVATE_ACCOUNT)
        }
    }
    
    func getStatus(status: String) -> String {
        switch status {
        case "1":
            print("Everyone")
            return "Everyone"
        case "2":
            print("Friends")
            return "Friends"
        case "3":
            print("None")
            return "None"
        default:
            print("default")
            return ""
        }
    }
}

extension PrivacySettingsVC : PrivacySettingDelegate, PrivacySettingsAlertViewDelegate {
    //MARK: PrivacySetting Delegates...
    func switchClicked(status:Bool, cell: PrivacySettingsSwitchTVCell) {
        if let indexPath = self.tbl_privacySettings.indexPath(for: cell) {
            let security_status = status ? "1" : "0"
            if indexPath.section == 0 {
                self.uodatePrivacySettingAPI("allow_other_to_find_me", security_status: security_status, Types: .ALLOW_OTHERS_TO_FIND_ME)
            }
            else {
                self.uodatePrivacySettingAPI("is_private", security_status: security_status, Types: .PRIVATE_ACCOUNT)

            }
        }
    }
    
    //MARK: PrivacySettingsAlertView Delegates...
    func doneClicked(status: Int, view: PrivacySettingAlertView) {
        switch view.tag {
        case 0:
            self.uodatePrivacySettingAPI("video_download_security", security_status: String(status), Types: .CAN_DOWNLOAD_VIDEO)
        case 1:
            self.uodatePrivacySettingAPI("comment_security", security_status: String(status), Types: .CAN_SEND_COMMENTS)
        case 2:
            self.uodatePrivacySettingAPI("react_security", security_status: String(status), Types: .CAN_REACTE_TO_ME)
        case 3:
            self.uodatePrivacySettingAPI("duet_security", security_status: String(status), Types: .CAN_DUET_WITH_ME)
        case 4:
            self.uodatePrivacySettingAPI("message_security", security_status: String(status), Types: .CAN_SEND_MESSAGE)
        default:
            break
        }
        view.removeFromSuperview()
    }
}

extension PrivacySettingsVC {
    //MARK: API's...
    func getPrivacySettingAPI(){
        let url = URL(string: Constants.BASE_URL+Constants.GET_PRIVACY_SETTINGS)!
        let headers: HTTPHeaders = ["Authentication": User.sharedInstance.auth_key ?? ""]
        print(String(User.sharedInstance.auth_key ?? ""))
        DialougeUtils.addActivityView(view: self.view)
        self.getPrivacySettingAPICall(url, header: headers)
    }
    
    func getPrivacySettingAPICall(_ url: URL, header: HTTPHeaders){
        LoginSignUpAPI.getUserDataAPI(vc: self, url: url, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            self.privacySettingModel = PrivacySettingModel().decodingPrivacySettingItem(dict: result!["data"] as? [String : Any] ?? [:])
                            DispatchQueue.main.async {
                                self.tbl_privacySettings.reloadData()
                            }
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                }
            }
        }
    }
    
    func uodatePrivacySettingAPI(_ security_type: String, security_status: String, Types: PrivacySettingType){
        let url = URL(string: Constants.BASE_URL+Constants.UPDATE_PRIVACY_SETTINGS)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        let param : Parameters = ["security_type": security_type, "security_status" : security_status]
        print(param)
        DialougeUtils.addActivityView(view: self.view)
        self.uodatePrivacySettingAPICall(param, url: url, header: headers, security_status: security_status, type: Types)
    }

    func uodatePrivacySettingAPICall(_ param: Parameters, url: URL, header: HTTPHeaders, security_status: String, type: PrivacySettingType){
        LoginSignUpAPI.SendOTPAPI(vc: self, parameter: param, url: url, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status: Bool = result!["status"] as! Bool
                        if status == true{
                            switch type {
                            case .ALLOW_OTHERS_TO_FIND_ME:
                                self.privacySettingModel?.allow_other_to_find_me = security_status
                            case .PRIVATE_ACCOUNT:
                                self.privacySettingModel?.is_private = security_status
                            case .CAN_DOWNLOAD_VIDEO:
                                self.privacySettingModel?.video_download_security = security_status
                            case .CAN_SEND_COMMENTS:
                                self.privacySettingModel?.comment_security = security_status
                            case .CAN_REACTE_TO_ME:
                                self.privacySettingModel?.react_security = security_status
                            case .CAN_DUET_WITH_ME:
                                self.privacySettingModel?.duet_security = security_status
                            case .CAN_SEND_MESSAGE:
                                self.privacySettingModel?.message_security = security_status
                            default:
                                break
                            }
                            DispatchQueue.main.async {
                                self.tbl_privacySettings.reloadData()
                            }
                            UIApplication.shared.keyWindow?.makeToast(message: result!["message"] as? String ?? "privacy setting updated")
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                }
            }
        }
    }
}
