//
//  ProfileVC.swift
//  LmTek
//
//  Created by PTBLR-1128 on 24/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class ProfileVC: UIViewController {
    //MARK: Outlets...
    @IBOutlet weak var scrollView_content : UIScrollView!
    @IBOutlet weak var lbl_videoDuet : UILabel!
    @IBOutlet weak var btn_likes : UIButton!
    @IBOutlet weak var btn_following : UIButton!
    @IBOutlet weak var btn_followers : UIButton!
    @IBOutlet weak var lbl_profileName : UILabel!
    @IBOutlet weak var lbl_userId: UILabel!
    @IBOutlet weak var lbl_videosCount : UILabel!
    @IBOutlet weak var lbl_duetCount : UILabel!
    @IBOutlet weak var img_profile : UIImageView!
    @IBOutlet weak var img_coverPhoto : UIImageView!
    @IBOutlet weak var collection_videos : UICollectionView!
    @IBOutlet weak var constraint_videosCollectionsHeight : NSLayoutConstraint!
    @IBOutlet weak var img_noDataCheck : UIImageView!
    
    @IBOutlet weak var lbl_videos : UILabel!
    @IBOutlet weak var lbl_duets : UILabel!
    
    //MARK:- Variables...
    //var profileModel : ProfileItem?
    var videos_list = [GetVideosData]()
    var duet_list = [GetVideosData]()
    var selectedVideoList = 0
    var strTotalDeuet: String = ""
    var yRef: CGFloat = 0.0
    var imgData:NSData?
    
    //MARK:- View Life Cycle...
    override func viewDidLoad() {
        super.viewDidLoad()

        self.cellRegistration()
        NotificationCenter.default.addObserver(self, selector: #selector(self.getProfileDetails/*viewProfile*/), name: NSNotification.Name.init(rawValue: Constants.PROFILE_UPDATE_NOTIFICATION), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.getProfileDetails/*viewProfile*/), name: NSNotification.Name(rawValue: Constants.VIDEO_ADDED_NOTIFICATION), object: nil)
        
//        self.view.backgroundColor = .white
//        self.screenDesigning()
    }
    func screenDesigning() {
        
        let viewHeader: UIView = UIView()
        viewHeader.frame = CGRect(x: 0, y: yRef, width: screenWidth, height: 70)
        viewHeader.backgroundColor = .white
        self.view.addSubview(viewHeader)
        
        let imgUserIcon:UIImageView = UIImageView()
        imgUserIcon.frame = CGRect(x: 15, y: 40, width: 25, height: 25)
        imgUserIcon.backgroundColor = .clear
        imgUserIcon.image = UIImage.init(named: "Add_Account")
        self.view.addSubview(imgUserIcon)
        
        let lblUserName:UILabel = UILabel()
        lblUserName.frame = CGRect(x: (screenWidth-150)/2, y: 40, width: 150, height: 25)
        lblUserName.backgroundColor = .clear
        lblUserName.text = "Aamir"
        lblUserName.font = fontSizeSemiBoldTitle
        lblUserName.textColor = .black
        lblUserName.textAlignment = .center
        self.view.addSubview(lblUserName)

        let viewBG: UIView = UIView()
        viewBG.frame = CGRect(x: 0, y: 70, width: screenWidth, height: screenHeight)
        viewBG.backgroundColor = .white
        self.view.addSubview(viewBG)

        let imgThreeDot:UIImageView = UIImageView()
        imgThreeDot.frame = CGRect(x: screenWidth-45, y: 50, width: 30, height: 7)
        imgThreeDot.backgroundColor = .clear
        imgThreeDot.image = UIImage.init(named: "MenuThreedot")
        self.view.addSubview(imgThreeDot)
        
        let btnThreeDot:UIButton = UIButton()
        btnThreeDot.frame = CGRect(x: screenWidth-50, y: 40, width: 50, height: 30)
        btnThreeDot.backgroundColor = .clear
        btnThreeDot.addTarget(self, action: #selector(self.btnThreeDotClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnThreeDot)

        yRef = yRef+viewHeader.frame.size.height
        
        let lblheaderLine:UILabel = UILabel()
        lblheaderLine.frame = CGRect(x: 0, y: yRef, width: screenWidth, height: 1)
        lblheaderLine.backgroundColor = UIColor.lightGray.withAlphaComponent(0.7)
        self.view.addSubview(lblheaderLine)

        yRef = yRef+lblheaderLine.frame.size.height + 20
        
        let imgUserProfile: UIImageView = UIImageView()
        imgUserProfile.frame = CGRect(x: 15, y: yRef, width: 60, height: 60)
        imgUserProfile.backgroundColor = .lightGray
        imgUserProfile.image = UIImage.init(named: "")
        imgUserProfile.layer.cornerRadius = 30
        imgUserProfile.clipsToBounds = true
        self.view.addSubview(imgUserProfile)
        
        let lblTotalPost:UILabel = UILabel()
        lblTotalPost.frame = CGRect(x: (screenWidth/2)-60, y: yRef+10, width: 60, height: 25)
        lblTotalPost.backgroundColor = UIColor.clear
        lblTotalPost.text = "6"
        lblTotalPost.textAlignment = .center
        lblTotalPost.font = fontSizeSemiBoldTitle
        self.view.addSubview(lblTotalPost)
        
        let lblPost:UILabel = UILabel()
        lblPost.frame = CGRect(x: (screenWidth/2)-60, y: yRef+35, width: 60, height: 20)
        lblPost.backgroundColor = UIColor.clear
        lblPost.text = "Post"
        lblPost.textAlignment = .center
        lblPost.font = fontSizeSmall
        self.view.addSubview(lblPost)

        let lblTotalFollowers:UILabel = UILabel()
        lblTotalFollowers.frame = CGRect(x: (screenWidth/2), y: yRef+10, width: 80, height: 25)
        lblTotalFollowers.backgroundColor = UIColor.clear
        lblTotalFollowers.text = "38"
        lblTotalFollowers.textAlignment = .center
        lblTotalFollowers.font = fontSizeSemiBoldTitle
        self.view.addSubview(lblTotalFollowers)
        
        let lblFollowers:UILabel = UILabel()
        lblFollowers.frame = CGRect(x: (screenWidth/2), y: yRef+35, width: 80, height: 20)
        lblFollowers.backgroundColor = UIColor.clear
        lblFollowers.text = "Followers"
        lblFollowers.textAlignment = .center
        lblFollowers.font = fontSizeSmall
        self.view.addSubview(lblFollowers)

        let lblTotalFollowing:UILabel = UILabel()
        lblTotalFollowing.frame = CGRect(x: (screenWidth/2)+80, y: yRef+10, width: 80, height: 25)
        lblTotalFollowing.backgroundColor = UIColor.clear
        lblTotalFollowing.text = "98"
        lblTotalFollowing.textAlignment = .center
        lblTotalFollowing.font = fontSizeSemiBoldTitle
        self.view.addSubview(lblTotalFollowing)
        
        let lblFollowing:UILabel = UILabel()
        lblFollowing.frame = CGRect(x: (screenWidth/2)+80, y: yRef+35, width: 80, height: 20)
        lblFollowing.backgroundColor = UIColor.clear
        lblFollowing.text = "Followers"
        lblFollowing.textAlignment = .center
        lblFollowing.font = fontSizeSmall
        self.view.addSubview(lblFollowing)

        yRef = yRef+imgUserProfile.frame.size.height + 10
        
        let lblProfileUserName:UILabel = UILabel()
        lblProfileUserName.frame = CGRect(x: 15, y: yRef, width: 180, height: 25)
        lblProfileUserName.backgroundColor = UIColor.clear
        lblProfileUserName.text = "Amir_w"
        lblProfileUserName.textAlignment = .left
        lblProfileUserName.font = fontSizeSemiBoldTitle
        self.view.addSubview(lblProfileUserName)

        yRef = yRef+lblProfileUserName.frame.size.height+5

        let lblDescription:UILabel = UILabel()
        lblDescription.frame = CGRect(x: 15, y: yRef, width: 180, height: 20)
        lblDescription.backgroundColor = UIColor.clear
        lblDescription.text = "Lorem ipsum dolor sit amet, consectetur adipiscing "
        lblDescription.textAlignment = .left
        lblDescription.font = fontSizeSmall
        lblDescription.numberOfLines = 0
        lblDescription.lineBreakMode = .byWordWrapping
        lblDescription.sizeToFit()
        self.view.addSubview(lblDescription)

        yRef = yRef+lblProfileUserName.frame.size.height+15

        let btnTrimandUse:UIButton = UIButton()
        btnTrimandUse.frame = CGRect(x: 15, y: yRef, width: screenWidth-30, height: 40)
        btnTrimandUse.backgroundColor = colorYellow
        btnTrimandUse.setTitle("Trim and Use", for: .normal)
        btnTrimandUse.setTitleColor(.black, for: .normal)
        btnTrimandUse.titleLabel?.font = fontSizeSemiBoldTitle
        btnTrimandUse.layer.cornerRadius = 4
        btnTrimandUse.clipsToBounds = true
        btnTrimandUse.layer.shadowOffset = .zero
        btnTrimandUse.layer.shadowRadius = 4
        btnTrimandUse.layer.shadowOpacity = 3
        btnTrimandUse.layer.shadowColor = UIColor.lightGray.withAlphaComponent(0.7).cgColor
        btnTrimandUse.layer.masksToBounds = false
        self.view.addSubview(btnTrimandUse)

        yRef = yRef+btnTrimandUse.frame.size.height+15

        let lblheaderLine1:UILabel = UILabel()
        lblheaderLine1.frame = CGRect(x: 0, y: yRef, width: screenWidth, height: 1)
        lblheaderLine1.backgroundColor = UIColor.lightGray.withAlphaComponent(0.7)
        self.view.addSubview(lblheaderLine1)

        yRef = yRef+lblheaderLine1.frame.size.height+15

        let btnTabFirst:UIButton = UIButton()
        btnTabFirst.frame = CGRect(x: ((screenWidth/2)-25)/2, y: yRef, width: 25, height: 25)
        btnTabFirst.backgroundColor = .clear
        btnTabFirst.setImage(UIImage.init(named: "Tabs_Icon"), for: .normal)
        btnTabFirst.clipsToBounds = true
        self.view.addSubview(btnTabFirst)

        let btnHeartTab:UIButton = UIButton()
        btnHeartTab.frame = CGRect(x: (screenWidth/2) + ((screenWidth/4)-12.5), y: yRef, width: 25, height: 25)
        btnHeartTab.backgroundColor = .clear
        btnHeartTab.setImage(UIImage.init(named: "Heart_Stroke"), for: .normal)
        btnHeartTab.clipsToBounds = true
        self.view.addSubview(btnHeartTab)

        yRef = yRef+btnHeartTab.frame.size.height+15

        let lblheaderLine2:UILabel = UILabel()
        lblheaderLine2.frame = CGRect(x: 0, y: yRef, width: screenWidth, height: 1)
        lblheaderLine2.backgroundColor = UIColor.lightGray.withAlphaComponent(0.7)
        self.view.addSubview(lblheaderLine2)

        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserDefaults.standard.bool(forKey: Constants.KEYS.isLoggedIn) {
            self.getProfileDetails()
        }

    }
//    deinit {
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.init(rawValue: Constants.PROFILE_UPDATE_NOTIFICATION), object: nil)
//    }
    @objc func btnThreeDotClicked(_ sender: UIButton) {
        let settingsVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        //settingsVC.profileModel = self.profileModel
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(settingsVC, animated: true)
        }
    }
    //MARK: IBActions...
    @IBAction func videoDuetClicked(_ sender: UIButton) {
        if self.selectedVideoList != sender.tag {
            UIView.animate(withDuration: 0.3) {
                self.lbl_videoDuet.frame.origin.x = sender.frame.origin.x
                self.view.layoutIfNeeded()
            }
            self.selectedVideoList = sender.tag
            self.collection_videos.reloadData()
        }
        
    }
    
    @IBAction func settingsClicked() {
        let settingsVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        //settingsVC.profileModel = self.profileModel
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(settingsVC, animated: true)
        }
    }
    
    @IBAction func selectCoverPhoto() {
        let alertController = UIAlertController(title: "Select image from", message: nil, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            DispatchQueue.main.async {
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        alertController.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            DispatchQueue.main.async {
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    //MARK: Functions...
    
    func showProfileDetails(profile: GetProfileDataModel) {
        self.btn_likes.setTitle("\(profile.total_videos ?? 0) Posts", for: .normal)
        self.btn_likes.setTitleColor(.white, for: .normal)
        self.btn_followers.setTitle("\(profile.total_followers ?? 0) Followers", for: .normal)
        self.btn_followers.setTitleColor(.white, for: .normal)
        self.btn_following.setTitle("\(profile.total_following ?? 0) Following", for: .normal)
        self.btn_following.setTitleColor(.white, for: .normal)
        self.lbl_profileName.text = "\(profile.first_name ?? "") \(profile.last_name ?? "")"
        self.lbl_userId.text = "\(profile.username ?? "")"
        self.lbl_videosCount.text = "Video \(profile.total_videos ?? 0)"
        //self.lbl_duetCount.text = "Duet 0"
        self.img_profile.sd_setImage(with: URL(string: profile.profile_picture as! String), completed: nil)
        self.img_coverPhoto.sd_setImage(with: URL(string: profile.cover_photo as! String), completed: nil)
        
        let arrdata:[[String: Any]] = profile.videos_list as! [[String: Any]]
        if videos_list.count > 0 {
            videos_list.removeAll()
        }
        
        for i in 0..<arrdata.count {
            let dictdata:[String: Any] = arrdata[i]
            let objdata: GetVideosData = GetVideosData.init(VideoInfo: dictdata)!
            videos_list.append(objdata)
        }
        
        self.lbl_videos.text = "Videos \(self.videos_list.count)"
        
        self.constraint_videosCollectionsHeight.constant = .greatestFiniteMagnitude
        collection_videos.reloadData()
        collection_videos.layoutIfNeeded()
        let height = collection_videos.contentSize.height + 55.0
        self.constraint_videosCollectionsHeight.constant = (height < 250) ? 250 : height

        img_noDataCheck.isHidden = (videos_list.count == 0) ? false : true
    }
    
    func cellRegistration() {
        collection_videos.register(UINib.init(nibName: "ProfileVideosCVCell", bundle: nil), forCellWithReuseIdentifier: "ProfileVideosCVCell")
    }
    
    @objc func getProfileDetails() {
        self.getUserDetails()
        self.getDuteVideosAPI()
    }

}

extension ProfileVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    //MARK:- CollectionView Delegates...
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.selectedVideoList == 0 {
            return videos_list.count
        }
        return duet_list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileVideosCVCell", for: indexPath) as! ProfileVideosCVCell
        var video_model : GetVideosData
        
        if self.selectedVideoList == 0 {
            let objdata:GetVideosData = videos_list[indexPath.row]
            let cover_image = objdata.thumb_url ?? ""//videos_list[indexPath.row].image_url ?? ""//["image_url"] as? String ?? ""
            cell.img_thumbnail.sd_setImage(with: URL(string: cover_image as! String), completed: nil)
            cell.img_thumbnail.contentMode = .scaleAspectFill

        }else {
            let objdata:GetVideosData = duet_list[indexPath.row]
            let cover_image = objdata.thumb_url ?? ""//videos_list[indexPath.row].image_url ?? ""//["image_url"] as? String ?? ""
            cell.img_thumbnail.sd_setImage(with: URL(string: cover_image as! String), completed: nil)
            cell.img_thumbnail.contentMode = .scaleAspectFill
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellSize = CGSize.init(width: (self.view.frame.width-20)/3, height: 140)
        return cellSize
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let playVideoVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        playVideoVC.checkSearchVideo = true
        playVideoVC.videosArray =  self.selectedVideoList == 0 ? self.videos_list : self.duet_list//self.videos_list
        playVideoVC.checkVideoTag = indexPath.row
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(playVideoVC, animated: true)
        }
    }
    
    func checkDuetVideo() {
        self.lbl_duetCount.text = "Duet \(duet_list.count)"
    }
}

extension ProfileVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    //MARK: ImagePicker Delegates...
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.editedImage] as? UIImage {
            self.img_coverPhoto.image = image
            self.uploadCoverImageAPI(image: image)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func getUserDetails() {
        let url = URL(string: Constants.BASE_URL+Constants.GET_USER_DETAILS)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        DialougeUtils.addActivityView(view: self.view)
        self.getUserDataAPI(url, header: headers)
    }
    
    func getUserDataAPI(_ url: URL, header: HTTPHeaders){
        LoginSignUpAPI.getUserDataAPI(vc: self, url: url, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            let arrdata:[String: Any] = result!["data"] as! [String : Any]
                            let objdata: GetProfileDataModel = GetProfileDataModel.init(ProfileInfo: arrdata)!
                                self.showProfileDetails(profile: objdata)
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                }
            }
        }
    }
}

extension ProfileVC {
    //MARK: API's...

    func uploadCoverImageAPI(image: UIImage) {
        let img:UIImage = image
        self.imgData = img.jpegData(compressionQuality: 0.50) as NSData?

        let url = URL(string: Constants.BASE_URL+Constants.UPDATE_COVER_PICTURE)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        DialougeUtils.addActivityView(view: self.view)
        self.UploadCoverPhotoAPICall(url, header: headers, imagedata: imgData!)
    }
    
    func UploadCoverPhotoAPICall(_ url: URL, header: HTTPHeaders, imagedata: NSData){
        AllUserApis.uploadUserCoverPhotoAPI(vc: self, url: url, imgData: imagedata, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                }
            }
        }
    }
    
    func getDuteVideosAPI() {
        let url = URL(string: Constants.BASE_URL+Constants.GET_DUET_VIDEOS)!
        let headers: HTTPHeaders = ["Authentication": User.sharedInstance.auth_key ?? ""]
        print(String(User.sharedInstance.auth_key ?? ""))
        DialougeUtils.addActivityView(view: self.view)
        self.getDuteVideosAPICall(url, header: headers)
    }
    
    func getDuteVideosAPICall(_ url: URL, header: HTTPHeaders){
        LoginSignUpAPI.getUserDataAPI(vc: self, url: url, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            let alldata: [[String : Any]] = result!["user_data"] as? [[String : Any]] ?? []
                            if self.duet_list.count > 0 {
                                self.duet_list.removeAll()
                            }
                            for i in 0..<alldata.count {
                                let dictdata: [String : Any] = alldata[i]
                                let objdata: GetVideosData = GetVideosData.init(VideoInfo: dictdata)!
                                self.duet_list.append(objdata)
                            }
                            self.checkDuetVideo()
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                }
            }
        }
    }
}
