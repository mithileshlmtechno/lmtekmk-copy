//
//  ModelProfile.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 15/07/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import Foundation


class GetProfileDataModel: NSObject{
    
    var about:Any?
    var added_on:Any?
    var app_language:Any?
    var apple_id:Any?
    var autoplay_next_video:Any?
    var country_access:Any?
    var country_code:Any?
    var cover_photo:Any?
    var dob:Any?
    var email:Any?
    var facebook_id:Any?
    var first_name:Any?
    var gender:Any?
    var group_name:Any?
    var id:Any?
    var included_tags:Any?
    var insta_id:Any?
    var introduction:Any?
    var is_celebrity:Any?
    var is_email:Any?
    var language:Any?
    var last_name:Any?
    var phone:Any?
    var profile_picture:Any?
    var profile_security:Any?
    var rank:Any?
    var referral_code:Any?
    var refresh_token:Any?
    var total_dislikes:Any?
    var total_followers:Any?
    var total_following:Any?
    var total_likes:Any?
    var total_videos:Any?
    var username:Any?
    var videos_list:Any?
    

    init?(ProfileInfo: [String: Any]) {
        self.about = ProfileInfo["about"]
        self.added_on = ProfileInfo["added_on"]
        self.app_language = ProfileInfo["app_language"]
        self.apple_id = ProfileInfo["apple_id"]
        self.autoplay_next_video = ProfileInfo["autoplay_next_video"]
        self.country_access = ProfileInfo["country_access"]
        self.country_code = ProfileInfo["country_code"]
        self.cover_photo = ProfileInfo["cover_photo"]
        self.dob = ProfileInfo["dob"]
        self.email = ProfileInfo["email"]
        self.facebook_id = ProfileInfo["facebook_id"]
        self.first_name = ProfileInfo["first_name"]
        self.gender = ProfileInfo["gender"]
        self.group_name = ProfileInfo["group_name"]
        self.id = ProfileInfo["id"]
        self.included_tags = ProfileInfo["included_tags"]
        self.insta_id = ProfileInfo["insta_id"]
        self.introduction = ProfileInfo["introduction"]
        self.is_celebrity = ProfileInfo["is_celebrity"]
        self.is_email = ProfileInfo["is_email"]
        self.language = ProfileInfo["language"]
        self.last_name = ProfileInfo["last_name"]
        self.phone = ProfileInfo["phone"]
        self.profile_picture = ProfileInfo["profile_picture"]
        self.profile_security = ProfileInfo["profile_security"]
        self.rank = ProfileInfo["rank"]
        self.referral_code = ProfileInfo["referral_code"]
        self.refresh_token = ProfileInfo["refresh_token"]
        self.total_dislikes = ProfileInfo["total_dislikes"]
        self.total_followers = ProfileInfo["total_followers"]
        self.total_following = ProfileInfo["total_following"]
        self.total_likes = ProfileInfo["total_likes"]
        self.total_videos = ProfileInfo["total_videos"]
        self.username = ProfileInfo["username"]
        self.videos_list = ProfileInfo["videos_list"]
    }
}
