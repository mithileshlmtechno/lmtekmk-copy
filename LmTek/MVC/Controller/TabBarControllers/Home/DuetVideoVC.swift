//
//  DuetVideoVC.swift
//  LmTek
//
//  Created by PTBLR-1128 on 29/09/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import AVFoundation

class DuetVideoVC: UIViewController {
    //MARK:- Outlets...
    @IBOutlet weak var view_camera : UIView!
    @IBOutlet weak var view_duetVideo : UIView!
    @IBOutlet weak var collection_cameraOptions : UICollectionView!
    @IBOutlet weak var img_vidoePreview : UIImageView!
    @IBOutlet weak var view_recordFiller : UIView!
    
    @IBOutlet weak var view_activity : UIView!
    @IBOutlet weak var activityIndicator : UIActivityIndicatorView!
    @IBOutlet weak var lbl_timer : UILabel!
    
    //MARK:- Variables...
    var isInitial = true
    var cameraOptionsArray = ["Flip", "Speed", "Beauty", "Filters", "Timer", "Flash"]
    var cameraOptionsImagesArray = [#imageLiteral(resourceName: "ic_changeCamera"), #imageLiteral(resourceName: "ic_speed"), #imageLiteral(resourceName: "ic_beauty"), #imageLiteral(resourceName: "ic_filters"), #imageLiteral(resourceName: "ic_timer")]
    var isFlashOn = false
    var video_model : VideoItem?
    var video_player : AVPlayer?
    
    var captureSession = AVCaptureSession()
    var backCamera: AVCaptureDevice?
    var frontCamera: AVCaptureDevice?
    var currentCamera: AVCaptureDevice?
    var videoInput : AVCaptureInput?
    var movieFileOutput = AVCaptureMovieFileOutput()
    
    var isRecording = false
    var recordEndCorrectly = true
    var videoDuration = 0
    var recordingTime = 0
    
    //var videoTimer : Timer?
    var recordTimer : Timer?
    
    var cameraURL : URL?
    
    var recordFillLayer = CAShapeLayer()
    var circularPath : UIBezierPath!
    var totalTime = 0
    
    //MARK:- View Life Cycle...
    override func viewDidLoad() {
        super.viewDidLoad()

        self.cellRegistration()
        self.setupDevice()
        self.setupInputOutput()
        /*if let url = URL(string: self.video_model?.video_url ?? "") {
            let videoAsset = AVAsset(url: url)
            let outputURL = URL(fileURLWithPath: NSHomeDirectory() + "/Documents/videoAudio.m4a")
            videoAsset.writeAudioTrack(to: outputURL, success: {
                print("audio extract success")
                let asset = AVAsset(url: outputURL)
                let playerItem = AVPlayerItem(asset: asset)
                let audioPlayer = AVPlayer(playerItem: playerItem)
                audioPlayer.play()
            }) { (error) in
                print("failed to extract audio:",error.localizedDescription)
            }
        }*/

        NotificationCenter.default.addObserver(self, selector: #selector(videoFinished), name: .AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    /*override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if self.isInitial {
            self.isInitial = false
            if let url = URL(string: self.video_model?.video_url ?? "") {
                let playerItem = AVPlayerItem(asset: AVAsset(url: url))
                self.video_player = AVPlayer(playerItem: playerItem)
                let playerLayer = AVPlayerLayer(player: self.video_player)
                playerLayer.videoGravity = .resizeAspectFill
                playerLayer.frame = self.view_duetVideo.bounds //CGRect(x: 0, y: 0, width: self.view_duetVideo.frame.size.width, height: self.view_duetVideo.frame.size.height)
                print("playerLayer:",playerLayer.frame)
                print("view_duetVideo:",self.view_duetVideo.frame)
                self.view_duetVideo.layer.insertSublayer(playerLayer, at: 0)
                
                //self.video_player?.play()
                /*DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.video_player?.pause()
                }*/
                
                let preview = AVCaptureVideoPreviewLayer(session: self.captureSession)
                preview.frame = self.view_camera.bounds
                preview.videoGravity = .resizeAspectFill
                self.view_camera.layer.insertSublayer(preview, at: 0)
            }
        }
    }*/
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.isInitial {
            self.isInitial = false
            /*if let url = URL(string: self.video_model?.video_url ?? "") {
                self.img_vidoePreview.sd_setImage(with: URL(string: self.video_model?.image_url ?? ""), completed: nil)
                let videoAsset = AVAsset(url: url)
                self.videoDuration = Int(videoAsset.duration.seconds)
                let playerItem = AVPlayerItem(asset: AVAsset(url: url))
                self.video_player = AVPlayer(playerItem: playerItem)
                let playerLayer = AVPlayerLayer(player: self.video_player)
                playerLayer.videoGravity = .resizeAspectFill
                playerLayer.frame = self.view_duetVideo.bounds //CGRect(x: 0, y: 0, width: self.view_duetVideo.frame.size.width, height: self.view_duetVideo.frame.size.height)
                print("playerLayer:",playerLayer.frame)
                print("view_duetVideo:",self.view_duetVideo.frame)
                self.view_duetVideo.layer.insertSublayer(playerLayer, at: 0)
                
                //self.video_player?.play()
                /*DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                 self.video_player?.pause()
                 }*/
                
                let preview = AVCaptureVideoPreviewLayer(session: self.captureSession)
                preview.frame = self.view_camera.bounds
                preview.videoGravity = .resizeAspectFill
                self.view_camera.layer.insertSublayer(preview, at: 0)
                
                /*if let url = URL(string: self.video_model?.video_url ?? "") {
                    self.exportAudio(url: url)
                }*/
            }*/
            CacheManager.shared.getFileWith(stringURL: self.video_model?.videoURL ?? "") { (result) in
                switch result {
                case .success(let videoURL):
                    self.img_vidoePreview.sd_setImage(with: URL(string: self.video_model?.thumbURL ?? ""), completed: nil)
                    let videoAsset = AVAsset(url: videoURL)
                    self.videoDuration = Int(videoAsset.duration.seconds)
                    self.setupRecordFillLayer()
                    let playerItem = AVPlayerItem(asset: AVAsset(url: videoURL))
                    self.video_player = AVPlayer(playerItem: playerItem)
                    let playerLayer = AVPlayerLayer(player: self.video_player)
                    playerLayer.videoGravity = .resizeAspectFill
                    playerLayer.frame = self.view_duetVideo.bounds //CGRect(x: 0, y: 0, width: self.view_duetVideo.frame.size.width, height: self.view_duetVideo.frame.size.height)
                    print("playerLayer:",playerLayer.frame)
                    print("view_duetVideo:",self.view_duetVideo.frame)
                    self.view_duetVideo.layer.insertSublayer(playerLayer, at: 0)
                    
                    //self.video_player?.play()
                    /*DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                     self.video_player?.pause()
                     }*/
                    
                    let preview = AVCaptureVideoPreviewLayer(session: self.captureSession)
                    preview.frame = self.view_camera.bounds
                    preview.videoGravity = .resizeAspectFill
                    self.view_camera.layer.insertSublayer(preview, at: 0)
                default:
                    break
                }
            }
        }
    }

    //MARK:- IBActions...
    @IBAction func backClicked() {
        if self.captureSession.isRunning {
            self.captureSession.stopRunning()
        }
        if let _ = self.video_player {
            self.video_player?.pause()
            self.video_player = nil
        }
        if self.isRecording {
            self.recordEndCorrectly = false
            self.invalidateTimer()
            self.movieFileOutput.stopRecording()
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func recordClicked() {
        if !self.isRecording {
            print("recordClicked")
            let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            let fileURL = paths[0].appendingPathComponent("output\(Int64(Date().timeIntervalSince1970)).mp4")//mov")
            //do {
            try? FileManager.default.removeItem(at: fileURL)
            self.movieFileOutput.startRecording(to: fileURL, recordingDelegate: self)
            self.startTimer()
            //self.startrecordTimer()
            self.isRecording = true
            self.video_player?.play()
            self.img_vidoePreview.isHidden = true
        }
        else {
            self.recordEndCorrectly = true
            self.invalidateTimer()
            self.video_player?.pause()
            self.movieFileOutput.stopRecording()
        }
    }
    
    //MARK:- Functions...
    func cellRegistration() {
        self.collection_cameraOptions.register(UINib(nibName: "CameraOptionsCVCell", bundle: nil), forCellWithReuseIdentifier: "CameraOptionsCVCell")
    }
    
    func setupDevice() {
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
        let devices = deviceDiscoverySession.devices
        
        for device in devices {
            if device.position == AVCaptureDevice.Position.back {
                backCamera = device
            }
            else if device.position == AVCaptureDevice.Position.front {
                frontCamera = device
            }
        }
        
        currentCamera = frontCamera
    }
    
    func setupInputOutput() {
        do {
            guard let camera = currentCamera else {
                return
            }
            self.videoInput = try AVCaptureDeviceInput(device: camera)
            self.captureSession.sessionPreset = .photo//.high
            if let input = self.videoInput, captureSession.canAddInput(input) {
                captureSession.addInput(input)
            }
            
            guard let audioDevice = AVCaptureDevice.default(for: .audio) else { return }
            
            let audioInput = try AVCaptureDeviceInput(device: audioDevice)
            if captureSession.canAddInput(audioInput) {
                captureSession.addInput(audioInput)
            }
            
            if self.captureSession.canAddOutput(self.movieFileOutput) {
                self.captureSession.addOutput(self.movieFileOutput)
            }
            /*if self.captureSession.canAddOutput(videoOutput) {
                videoOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "com.lmtek.LmTek"))
                self.captureSession.addOutput(videoOutput)
            }*/
            
            
            self.mirrorFrontCamera()
            
            self.captureSession.startRunning()
        } catch {
            print(error)
        }
    }
    
    func mirrorFrontCamera() {
        /*for connection in self.movieFileOutput.connections {
            for port in connection.inputPorts {
                if port.mediaType == .video {
                    if connection.isVideoMirroringSupported {
                        connection.isVideoMirrored = true
                        break
                    }
                }
            }
        }*/
        print("mirrorFrontCamera")
        guard let currentCameraInput = captureSession.inputs.first as? AVCaptureDeviceInput else {
            return
        }
        if let conn = self.movieFileOutput.connection(with: .video) {
            conn.isVideoMirrored = true//currentCameraInput.device.position == .front
        }
    }
    
    /*func startTimer() {
        self.videoTimer?.invalidate()
        self.videoTimer = nil
        
        self.recordingTime = 0
        self.videoTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.countTimer), userInfo: nil, repeats: true)
    }*/
    
    @objc func videoFinished(_ notification: Notification) {
        print("videFinished:",notification.object,notification.userInfo)
        self.recordEndCorrectly = true
        self.invalidateTimer()
        self.captureSession.stopRunning()
        self.movieFileOutput.stopRecording()
    }
    
    /*func invalidateTimer() {
        self.videoTimer?.invalidate()
        self.videoTimer = nil
        self.recordingTime = 0
    }
    
    @objc func countTimer(_ timer: Timer) {
        self.recordingTime += 1
        if self.recordingTime == self.videoDuration {
            self.recordEndCorrectly = true
            self.invalidateTimer()
            self.captureSession.stopRunning()
            self.movieFileOutput.stopRecording()
        }
    }*/
    
    func mergeVideoAudio(videoURL: URL, cameraType: String) {
        print("mergeVideoAudio")
        let videoAsset = AVAsset(url: videoURL)
        let mixComposition = AVMutableComposition()

        // 2 - Create two video tracks
        guard
          let videoTrack = mixComposition.addMutableTrack(
            withMediaType: .video,
            preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
          else { return }
        print("guard videoTrack")

        do {
            print("videoAsset.tracks(withMediaType: .video):",videoAsset.tracks(withMediaType: .video))
          try videoTrack.insertTimeRange(
            CMTimeRangeMake(start: .zero, duration: videoAsset.duration),
            of: videoAsset.tracks(withMediaType: .video)[0],
            at: .zero)
        } catch {
          print("Failed to load first track")
          return
        }

        if cameraType == "back" {
            
        }
        else {
            
        }

        // 3 - Composition Instructions
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRangeMake(
          start: .zero,
          duration: videoAsset.duration)
        print("mainInstruction")

        // 4 - Set up the instructions — one for each asset
        let videoInstruction = self.videoCompositionInstruction(
          videoTrack,
          asset: videoAsset, cameraType: cameraType)
        videoInstruction.setOpacity(0.0, at: videoAsset.duration)
        print("videoInstruction")

        // 5 - Add all instructions together and create a mutable video composition
        mainInstruction.layerInstructions = [videoInstruction]
        let mainComposition = AVMutableVideoComposition()
        mainComposition.instructions = [mainInstruction]
        mainComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        /*let width = floor(UIScreen.main.bounds.width / 2)
        let compositionWidth = (Int(width) % 2 == 0) ? width : width + 1
        let height = floor(UIScreen.main.bounds.height / 2)
        let compositionHeight = (Int(height) % 2 == 0) ? height : height + 1
        print("width:",width,"compositionWidth:",compositionWidth,"height:",height,"compositionHeight:",compositionHeight)*/
        //if cameraType == "back" {
        let width = UIScreen.main.bounds.width
        let compositionWidth = (Int(width) % 2 == 0) ? width : width - 1
        mainComposition.renderSize = CGSize(
            width: /*UIScreen.main.bounds.width*/compositionWidth,
            height: UIScreen.main.bounds.height)
        /*} else {
            mainComposition.renderSize = CGSize(
            width: UIScreen.main.bounds.width,
            height: UIScreen.main.bounds.height)
        }*/
        
        print("mainComposition")
        // 6 - Audio track
        /*if let loadedAudioAsset = self.audioAsset {
          let audioTrack = mixComposition.addMutableTrack(
            withMediaType: .audio,
            preferredTrackID: 0)
          do {
            try audioTrack?.insertTimeRange(
              CMTimeRangeMake(
                start: CMTime.zero,
                duration: videoAsset.duration),
              of: loadedAudioAsset.tracks(withMediaType: .audio)[0],
              at: .zero)
          } catch {
            print("Failed to load Audio track")
          }
        }*/

        // 7 - Get path
        guard
          let documentDirectory = FileManager.default.urls(
            for: .documentDirectory,
            in: .userDomainMask).first
          else { return }
        print("guard documentDirectory")
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .short
        let date = dateFormatter.string(from: Date())
        let url = documentDirectory.appendingPathComponent("mergeVideo-\(date).mp4")

        // 8 - Create Exporter
        guard let exporter = AVAssetExportSession(
          asset: mixComposition,
          presetName: AVAssetExportPresetHighestQuality)
          else { return }
        print("guard exporter")
        exporter.outputURL = url
        exporter.outputFileType = AVFileType.mp4
        exporter.shouldOptimizeForNetworkUse = true
        exporter.videoComposition = mainComposition

        // 9 - Perform the Export
        exporter.exportAsynchronously {
            print("exporter.exportAsynchronously")
            DispatchQueue.main.async {
                self.exportDidFinish(exporter)
            }
        }
    }
    
    func orientationFromTransform(
      _ transform: CGAffineTransform
    ) -> (orientation: UIImage.Orientation, isPortrait: Bool) {
        var assetOrientation = UIImage.Orientation.up
        var isPortrait = false
        let tfA = transform.a
        let tfB = transform.b
        let tfC = transform.c
        let tfD = transform.d
        
        if tfA == 0 && tfB == 1.0 && tfC == -1.0 && tfD == 0 {
            print("orientationFromTransform 1")
            assetOrientation = .right
            isPortrait = true
        } else if tfA == 0 && tfB == -1.0 && tfC == 1.0 && tfD == 0 {
            print("orientationFromTransform 2")
            assetOrientation = .left
            isPortrait = true
        } else if tfA == 1.0 && tfB == 0 && tfC == 0 && tfD == 1.0 {
            print("orientationFromTransform 3")
            assetOrientation = .up
        } else if tfA == -1.0 && tfB == 0 && tfC == 0 && tfD == -1.0 {
            print("orientationFromTransform 4")
            assetOrientation = .down
        }
        else if tfA == 0.0 && tfB == 1.0 && tfC == 1.0 && tfD == 0.0 {
            print("orientationFromTransform 5")
            assetOrientation = .rightMirrored
            isPortrait = true
        }
        else {
            print("orientationFromTransform else:",tfA, tfB, tfC, tfD)
        }
        return (assetOrientation, isPortrait)
    }
    
    func videoCompositionInstruction(
      _ track: AVCompositionTrack,
      asset: AVAsset, cameraType: String
    ) -> AVMutableVideoCompositionLayerInstruction {
        print("videoCompositionInstruction-----")
        let instruction = AVMutableVideoCompositionLayerInstruction(assetTrack: track)
        let assetTrack = asset.tracks(withMediaType: AVMediaType.video)[0]
        
        let transform = assetTrack.preferredTransform
        let assetInfo = orientationFromTransform(transform)
                
        var scaleToFitRatio = UIScreen.main.bounds.width / assetTrack.naturalSize.width
        if assetInfo.isPortrait {
            
            print("assetInfo.isPortrait")
            
            
            if cameraType == "front" {
                print("cameraType == front")
                scaleToFitRatio = UIScreen.main.bounds.width / UIScreen.main.bounds.height
                let scaleFactor = CGAffineTransform(
                    scaleX: scaleToFitRatio,
                    y: scaleToFitRatio)
                
                
                let finalTransform = assetTrack.preferredTransform.concatenating(scaleFactor).translatedBy(x: self.view_camera.frame.size.height * 1.0, y: 0)
                instruction.setTransform(finalTransform , at: .zero)
            }
            else {
                scaleToFitRatio = UIScreen.main.bounds.width / assetTrack.naturalSize.height
                let scaleFactor = CGAffineTransform(
                    scaleX: scaleToFitRatio,
                    y: scaleToFitRatio)
                let finalTransform = assetTrack.preferredTransform.concatenating(scaleFactor)
                instruction.setTransform(finalTransform , at: .zero)
            }
            
            
            
        } else {
            print("assetInfo.isPortrait else")
            let scaleFactor = CGAffineTransform(
                scaleX: scaleToFitRatio,
                y: scaleToFitRatio)
            var concat = assetTrack.preferredTransform.concatenating(scaleFactor)
                .concatenating(CGAffineTransform(
                    translationX: 0,
                    y: UIScreen.main.bounds.width / 2))
            if assetInfo.orientation == .down {
                print("assetInfo.orientation == .down")
                let fixUpsideDown = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                let windowBounds = UIScreen.main.bounds
                let yFix = assetTrack.naturalSize.height + windowBounds.height
                let centerFix = CGAffineTransform(
                    translationX: assetTrack.naturalSize.width,
                    y: yFix)
                concat = fixUpsideDown.concatenating(centerFix).concatenating(scaleFactor)
            }
            instruction.setTransform(concat, at: .zero)
        }
        print("----videoCompositionInstruction")
        return instruction
    }
    
    func exportDidFinish(_ session: AVAssetExportSession) {
        print("exportDidFinish")
        // Cleanup assets
        //audioAsset = nil
        
        //self.lbl_audio.text = ""
        print("session status completed:",AVAssetExportSession.Status.completed.rawValue," cancelled:",AVAssetExportSession.Status.cancelled.rawValue," exporting:",AVAssetExportSession.Status.exporting.rawValue," failed:",AVAssetExportSession.Status.failed.rawValue," waiting:",AVAssetExportSession.Status.waiting.rawValue," unknown:",AVAssetExportSession.Status.unknown.rawValue)
        guard
            session.status == AVAssetExportSession.Status.completed,
            let outputURL = session.outputURL
            else {
                
                print("session output errr",session.status.rawValue)
                if let url = self.cameraURL {
                    let cameraType = (self.currentCamera == self.backCamera) ? "back" : "front"
                    self.mergeVideoAudio(videoURL: url, cameraType: cameraType)
                }
                return
                
        }
        
        /*let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
        videoPlayerVC.videoURL = outputURL
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(videoPlayerVC, animated: true)
        }*/
        if let url = URL(string: self.video_model?.videoURL ?? "") {
            /*self.merge(arrayVideos: [AVAsset(url: outputURL), AVAsset(url: url)]) { (exporter) in
                print("merge completion")
                print("merge status:",exporter.status.rawValue, AVAssetExportSession.Status.completed.rawValue, AVAssetExportSession.Status.cancelled.rawValue, AVAssetExportSession.Status.exporting.rawValue, AVAssetExportSession.Status.failed.rawValue, ", mergeURL:",exporter.outputURL)
                guard exporter.status == AVAssetExportSession.Status.completed, let mergeURL = exporter.outputURL else {
                    print("merge error")
                    return
                }
                print("merged mergeURL",mergeURL)
                let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                videoPlayerVC.videoURL = mergeURL
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(videoPlayerVC, animated: true)
                }
            }*/
            print("if let url exporter")
            self.mergeVideosFilesWithURL(savedVideoURL: url, newVideoURL: outputURL)
        }
        /*CacheManager.shared.getFileWith(stringURL: self.video_model?.video_url ?? "") { (result) in
            switch result {
            case .success(let videoURL):
                self.mergeVideosFilesWithURL(savedVideoURL: videoURL, newVideoURL: outputURL)
            default:
                print("unable to load cached video")
            }
        }*/
    }
    
    
    func mergeVideosFilesWithURL(savedVideoURL: URL, newVideoURL: URL) {
        print("mergeVideosFilesWithURL:",savedVideoURL.absoluteString, newVideoURL.absoluteString)
        let savePathURL = URL(fileURLWithPath: NSHomeDirectory() + "/Documents/camRecordedVideo.mp4")
        do {
            try FileManager.default.removeItem(at: savePathURL)
        }
        catch {
            print("file remove error:",error.localizedDescription)
        }
        
        var mutableVideoComposition = AVMutableVideoComposition()
        var mixComposition = AVMutableComposition()
        
        let newVideoAsset = AVAsset(url: newVideoURL)
        let savedVideoAsset = AVAsset(url: savedVideoURL)
        
        let finalDuration = CMTimeMinimum(newVideoAsset.duration, savedVideoAsset.duration)
        
        let newVideoTrack = newVideoAsset.tracks(withMediaType: .video)[0]
        let savedVideoTrack = savedVideoAsset.tracks(withMediaType: .video)[0]
        
        let mutableCompositionNewVideoTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)!
        do {
            try mutableCompositionNewVideoTrack.insertTimeRange(CMTimeRange(start: .zero, duration: /*newVideoAsset*/finalDuration), of: newVideoTrack, at: .zero)
        }
        catch {
            print("newmutabletrack error:",error.localizedDescription)
        }
        
        let mutableCompositionSavedVideoTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)!
        
        do {
            try mutableCompositionSavedVideoTrack.insertTimeRange(CMTimeRange(start: .zero, duration: finalDuration), of: savedVideoTrack, at: .zero)
        }
        catch {
            print("savedmutabletrack error:",error.localizedDescription)
        }
        
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRange(start: .zero, duration: finalDuration)//CMTimeMaximum(newVideoAsset.duration, savedVideoAsset.duration))
        
        let newVideoLayerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: mutableCompositionNewVideoTrack)
        var newScale : CGAffineTransform
        var newMove : CGAffineTransform
        if self.currentCamera == self.backCamera {
            newScale = CGAffineTransform(scaleX: 0.51, y: 0.665)
            newMove = CGAffineTransform(translationX: 0, y: UIScreen.main.bounds.height / 4)
        }
        else {
            newScale = CGAffineTransform(scaleX: 0.51, y: 0.59)
            let y = UIScreen.main.bounds.height / 4
            let statusBarHeight = UIApplication.shared.statusBarFrame.height
            let transformY = y - (statusBarHeight * 1.4)
            newMove = CGAffineTransform(translationX: 0, y: transformY)
        }
        
        
        //let newMove = CGAffineTransform(translationX: 0, y: UIScreen.main.bounds.height / 4)
        newVideoLayerInstruction.setTransform(newScale.concatenating(newMove), at: .zero)
        
        let savedVideoLayerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: mutableCompositionSavedVideoTrack)
        let transform = savedVideoTrack.preferredTransform
        let assetInfo = orientationFromTransform(transform)
        print("assetInfo:",assetInfo)
        let saveX = (UIScreen.main.bounds.width / 2) / savedVideoTrack.naturalSize.width
        let saveY = (UIScreen.main.bounds.height / 2) / savedVideoTrack.naturalSize.height
        let savedScale = CGAffineTransform(scaleX: saveX, y: saveY)//(scaleX: 0.4, y: 0.345)
        let savedScaleWidth = ceil(UIScreen.main.bounds.width / 2)
        let savedScaleTransformWidth = (Int(savedScaleWidth) % 2 == 0) ? savedScaleWidth : savedScaleWidth - 1
        let savedMove = CGAffineTransform(translationX: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height / 4)
        savedVideoLayerInstruction.setTransform(savedScale.concatenating(savedMove), at: .zero)
        
        mainInstruction.layerInstructions = [newVideoLayerInstruction, savedVideoLayerInstruction]
        
        mutableVideoComposition.instructions = [mainInstruction]
        mutableVideoComposition.frameDuration = CMTime(value: 1, timescale: 30)
        let width = UIScreen.main.bounds.width
        let compositionWidth = (Int(width) % 2 == 0) ? width : width - 1
        mutableVideoComposition.renderSize = CGSize(
        width: /*UIScreen.main.bounds.width*/compositionWidth,
        height: UIScreen.main.bounds.height)
        if let audioTrackFirst = savedVideoAsset.tracks(withMediaType: .audio).first {
            let audioTrack = mixComposition.addMutableTrack(
                withMediaType: .audio,
                preferredTrackID: kCMPersistentTrackID_Invalid)
            
            do {
                
                try audioTrack?.insertTimeRange(
                    CMTimeRangeMake(
                        start: CMTime.zero,
                        duration: finalDuration),
                    of: /*savedVideoAsset.tracks(withMediaType: .audio)[0]*/audioTrackFirst,
                    at: .zero)
            } catch {
                print("Failed to load Audio track")
            }
        }
        
        let finalPath = savePathURL.absoluteString
        let assetExport = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)!
        assetExport.videoComposition = mutableVideoComposition
        assetExport.outputFileType = .mp4
        
        assetExport.outputURL = savePathURL
        assetExport.shouldOptimizeForNetworkUse = true
        
        assetExport.exportAsynchronously {
            print("duet merge exporter")
            self.stopIndicator()
            switch assetExport.status {
            case .completed:
                print("success")
                let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                videoPlayerVC.videoURL = assetExport.outputURL
                videoPlayerVC.duetVideoID = self.video_model?.videoID
                videoPlayerVC.duetUserID = self.video_model?.userID.rawValue
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(videoPlayerVC, animated: true)
                }
            case .failed:
                print("failed",assetExport.error)
            case .cancelled:
                print("cancelled",assetExport.error)
            default:
                print("default switch")
            }
        }
    }
    
    func setupRecordFillLayer() {
        let radius = self.view_recordFiller.frame.size.height / 2
        self.circularPath = UIBezierPath(arcCenter: CGPoint(x: radius, y: radius), radius: radius, startAngle: 0, endAngle: .pi * 2, clockwise: true)
        
        let grayLayer = CAShapeLayer()
        grayLayer.path = circularPath.cgPath
        grayLayer.strokeColor = /*UIColor(hexString: Constants.ORANGE_COLOR)*/UIColor.clear.cgColor
        grayLayer.lineWidth = 2
        grayLayer.fillColor = UIColor.clear.cgColor
        grayLayer.position = CGPoint(x: radius, y: radius)
        self.view_recordFiller.layer.addSublayer(grayLayer)
        
        self.recordFillLayer.path = circularPath.cgPath
        
        self.recordFillLayer.strokeColor = UIColor(hexString: Constants.ORANGE_COLOR)?.cgColor//UIColor.gray.cgColor
        self.recordFillLayer.fillColor = UIColor.clear.cgColor
        self.recordFillLayer.lineWidth = 5
        
        self.recordFillLayer.lineCap = .round
        
        self.recordFillLayer.position = CGPoint(x: 50, y: 50)
        
        self.recordFillLayer.transform = CATransform3DMakeRotation(-.pi / 2.0, 0, 0, 1)
        
        self.recordFillLayer.strokeEnd = 0
        
        self.view_recordFiller.layer.addSublayer(self.recordFillLayer)
    }
    
    func startTimer() {
        self.recordTimer?.invalidate()
        self.recordTimer = nil
        
        self.recordingTime = 0
        self.lbl_timer.text = self.recordingTime.timeFormatted()
        self.lbl_timer.isHidden = false
        self.recordTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.countTimer), userInfo: nil, repeats: true)
    }
    
    @objc func countTimer(_ timer: Timer) {
        self.recordingTime += 1
        self.lbl_timer.text = self.recordingTime.timeFormatted()
    }
    
    func invalidateTimer() {
        self.recordTimer?.invalidate()
        self.recordTimer = nil
        self.lbl_timer.isHidden = true
        self.recordingTime = 0
        self.lbl_timer.text = self.recordingTime.timeFormatted()
    }
    
    func animteIndicator() {
        DispatchQueue.main.async {
            self.activityIndicator.startAnimating()
            self.view_activity.isHidden = false
        }
    }
    
    func stopIndicator() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.view_activity.isHidden = true
        }
    }
}

extension DuetVideoVC: AVCaptureFileOutputRecordingDelegate {
    //MARK:- AVCaptureFileOuwitch")tputRecording Delegates...
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        if let err = error {
            print("didFinishRecordingTo error:",err.localizedDescription)
        }
        else {
            print("didFinishRecordingTo outputFileURL:",outputFileURL)
            if self.recordEndCorrectly {
                print("recordEndCorrectly:",recordEndCorrectly)

                var cameraType = "back"
                if self.currentCamera == self.backCamera {
                    cameraType = "back"
                }
                else {
                    cameraType = "front"
                }
                self.cameraURL = outputFileURL
                self.animteIndicator()
                self.mergeVideoAudio(videoURL: outputFileURL, cameraType: cameraType)
                /*if let url = URL(string: self.video_model?.video_url ?? "") {
                    self.mergeVideosFilesWithURL(savedVideoURL: url, newVideoURL: outputFileURL)
                }*/
            }
        }
    }
}

extension DuetVideoVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    //MARK:- CollectionView Delegates...
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.cameraOptionsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CameraOptionsCVCell", for: indexPath) as! CameraOptionsCVCell
        
        cell.lbl_title.text = self.cameraOptionsArray[indexPath.item]
        
        if indexPath.item == 5 {
            if self.isFlashOn {
                cell.img_option.image = #imageLiteral(resourceName: "ic_flashOff")
            }
            else {
                cell.img_option.image = #imageLiteral(resourceName: "ic_flashOn")
            }
        }
        else {
            cell.img_option.image = self.cameraOptionsImagesArray[indexPath.item]
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 50, height: 65)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.item {
        case 0:
            guard let input = self.videoInput else {
                return
            }
            if self.currentCamera == self.backCamera {
                if self.isFlashOn {
                    guard let device = AVCaptureDevice.default(for: .video) else { return }
                    guard device.hasTorch else { return }
                    do {
                        try device.lockForConfiguration()
                        
                        device.torchMode = .off
                        self.isFlashOn = false
                        device.unlockForConfiguration()
                    }
                    catch {
                        print("lockForConfiguration error : ",error.localizedDescription)
                    }
                }
                self.captureSession.removeInput(input)
                guard let camera = self.frontCamera else {
                    return
                }
                do {
                    self.videoInput = try AVCaptureDeviceInput(device: camera)
                    self.currentCamera = camera
                    if let video = self.videoInput,  self.captureSession.canAddInput(video) {
                        self.captureSession.addInput(video)
                    }
                    self.mirrorFrontCamera()
                }
                catch {
                    print("camera error:",error.localizedDescription)
                }
            }
            else {
                self.captureSession.removeInput(input)
                guard let camera = self.backCamera else {
                    return
                }
                do {
                    self.videoInput = try AVCaptureDeviceInput(device: camera)
                    self.currentCamera = camera
                    if let video = self.videoInput,  self.captureSession.canAddInput(video) {
                        self.captureSession.addInput(video)
                    }
                }
                catch {
                    print("camera error:",error.localizedDescription)
                }
            }
            collectionView.reloadItems(at: [IndexPath(item: 5, section: 0)])
        case 5:
            if self.currentCamera == self.backCamera {
                guard let device = AVCaptureDevice.default(for: .video) else { return }
                guard device.hasTorch else { return }
                do {
                    try device.lockForConfiguration()
                    
                    if device.torchMode == .on {
                        device.torchMode = .off
                        self.isFlashOn = false
                        //sender.setImage(#imageLiteral(resourceName: "flash_off"), for: .normal)
                    }
                    else {
                        do {
                            try device.setTorchModeOn(level: 1.0)
                            self.isFlashOn = true
                            //sender.setImage(#imageLiteral(resourceName: "flash_on"), for: .normal)
                        }
                        catch {
                            print("setTorchModeOn error : ",error.localizedDescription)
                        }
                    }
                    device.unlockForConfiguration()
                    collectionView.reloadItems(at: [indexPath])
                }
                catch {
                    print("lockForConfiguration error : ",error.localizedDescription)
                }
            }
        default:
            break
        }
        
    }
}

extension AVAsset {
    // Provide a URL for where you wish to write
    // the audio file if successful
    func writeAudioTrack(to url: URL,
                         success: @escaping () -> (),
                         failure: @escaping (Error) -> ()) {
        do {
            let asset = try audioAsset()
            asset.write(to: url, success: success, failure: failure)
        } catch {
            failure(error)
        }
    }

    private func write(to url: URL,
                       success: @escaping () -> (),
                       failure: @escaping (Error) -> ()) {
        // Create an export session that will output an
        // audio track (M4A file)
        guard let exportSession = AVAssetExportSession(asset: self,
                                                       presetName: AVAssetExportPresetAppleM4A) else {
                                                        // This is just a generic error
                                                        let error = NSError(domain: "domain",
                                                                            code: 0,
                                                                            userInfo: nil)
                                                        failure(error)

                                                        return
        }

        exportSession.outputFileType = .m4a
        exportSession.outputURL = url

        exportSession.exportAsynchronously {
            switch exportSession.status {
            case .completed:
                success()
            case .unknown, .waiting, .exporting, .failed, .cancelled:
                let error = NSError(domain: "domain", code: 0, userInfo: nil)
                failure(error)
            }
        }
    }

    private func audioAsset() throws -> AVAsset {
        // Create a new container to hold the audio track
        let composition = AVMutableComposition()
        // Create an array of audio tracks in the given asset
        // Typically, there is only one
        let audioTracks = tracks(withMediaType: .audio)

        // Iterate through the audio tracks while
        // Adding them to a new AVAsset
        for track in audioTracks {
            let compositionTrack = composition.addMutableTrack(withMediaType: .audio,
                                                               preferredTrackID: kCMPersistentTrackID_Invalid)
            do {
                // Add the current audio track at the beginning of
                // the asset for the duration of the source AVAsset
                try compositionTrack?.insertTimeRange(track.timeRange,
                                                      of: track,
                                                      at: track.timeRange.start)
            } catch {
                throw error
            }
        }

        return composition
    }
}
