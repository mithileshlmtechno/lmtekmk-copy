//
//  ReportVC.swift
//  LmTek
//
//  Created by PTBLR-1128 on 21/09/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import SwiftyJSON

class ReportVC: UIViewController {
    //MARK:- Outlets...
    @IBOutlet weak var tbl_reportReasons : UITableView!
    
    //MARK:- Variables...
    var video_model : GetVideosData?
    var reportsArray = [ReportItem]()
    
    //MARK:- View Life Cycle...
    override func viewDidLoad() {
        super.viewDidLoad()

        self.cellRegistration()
        self.getReportReasons()
    }
    

    //MARK:- IBActions...
    @IBAction func backClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Functions...
    func cellRegistration() {
        self.tbl_reportReasons.register(UINib(nibName: "SettingsTVCell", bundle: nil), forCellReuseIdentifier: "SettingsTVCell")
    }
    

}

extension ReportVC: UITableViewDelegate, UITableViewDataSource {
    //MARK:- TableView Delegates...
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.reportsArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTVCell") as! SettingsTVCell
        
        if indexPath.row == 0 {
            cell.img_arrow.isHidden = true
            cell.lbl_title.text = "Please Select a Reason"
            cell.backgroundColor = UIColor(hexString: "#F0F0F0")
            cell.lbl_title.textColor = UIColor.lightGray
        }
        else {
            cell.img_arrow.isHidden = false
            cell.backgroundColor = UIColor.white
            cell.lbl_title.textColor = UIColor.black
            
            let model = self.reportsArray[indexPath.row - 1]
            
            cell.lbl_title.text = model.reason
        }
        cell.lbl_separator.isHidden = false
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != 0 {
            let model = self.reportsArray[indexPath.row - 1]
            self.reportVideo(report: model)
        }
    }
}

extension ReportVC {
    //MARK:- API's...
    func getReportReasons() {
        TransportManager.sharedInstance.getReportReasons { (data, err) in
            if let error = err {
                ToastMessage.showToast(in: self, message: error.localizedDescription, bgColor: .white)
            }
            else {
                if let json = data as? JSON, let resultObj = json.dictionaryObject {
                    print("getReportReasons resultObj:",resultObj)
                    self.reportsArray = ReportModel().decodingReportItems(array: resultObj["data"] as? [[String : Any]] ?? [])
                    DispatchQueue.main.async {
                        self.tbl_reportReasons.reloadData()
                    }
                }
            }
        }
    }
    
    func reportVideo(report: ReportItem) {
        TransportManager.sharedInstance.reportVideo(video_id: String(describing: self.video_model?.video_id ?? ""), reason_id: report.id ?? "") { (data, err) in
            if let error = err {
                ToastMessage.showToast(in: self, message: error.localizedDescription, bgColor: .white)
            }
            else {
                if let json = data as? JSON, let resultObj = json.dictionaryObject {
                    print("reportVideo resultObj:",resultObj)
                    if resultObj["status"] as? Bool == true {
                        UIApplication.shared.keyWindow?.makeToast(message: resultObj["message"] as? String ?? "Reported Successfully")
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
    }
}
