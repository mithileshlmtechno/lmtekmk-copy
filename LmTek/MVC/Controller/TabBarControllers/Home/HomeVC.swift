//
//  HomeVC.swift
//  LmTek
//
//  Created by Sai Sankar on 14/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import SwiftyJSON
import Photos
import AVFoundation
import Alamofire

@available(iOS 13.0, *)
class HomeVC: BaseViewController {
    //MARK: Outlets...
    @IBOutlet weak var collection_home : UICollectionView!
    @IBOutlet weak var view_gradient : UIView!
    @IBOutlet var btns_videosList : [UIButton]!
    //MARK: Variables...
    var videosArray = [GetVideosData]()
    var followingVideosArray = [GetVideosData]()
    
    var selectedList = 0
    var downloadProgressView : DownloadProgressView?
    var skipVideos = 0
    let arrAllData:NSMutableArray = NSMutableArray()
    var checkSearchVideo: Bool = false
    var checkVideoTag: Int = 0
    var isLiked: Bool = false
    var totalLiked: String = ""
    var totalShared: String = ""
    var totalDownloads: String = ""
    //MARK: View Life Cycle...
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view_gradient.setGradient(colors: [UIColor.black.withAlphaComponent(0.7).cgColor, UIColor.black.withAlphaComponent(0).cgColor], startPoint: CGPoint.init(x:0, y:0), endPoint: CGPoint.init(x:0, y:1))
        if checkSearchVideo == true {
            let btnBack = UIButton()
            btnBack.frame = CGRect(x: 10, y: 44, width: 30, height: 30)
            btnBack.backgroundColor = UIColor.clear
            btnBack.setImage(UIImage.init(named: "back_btn"), for: .normal)
            btnBack.addTarget(self, action: #selector(self.btnBackClicked(_:)), for: .touchUpInside)
            self.view.addSubview(btnBack)

        } else {
            self.getHomePageAPI()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.getVideos), name: NSNotification.Name(rawValue: Constants.VIDEO_ADDED_NOTIFICATION), object: nil)
        self.view.backgroundColor = .black
        
    }
    func reloadItems(_ indexNumber: Int, sectionNumber: Int) {
        UIView.performWithoutAnimation() {
            self.collection_home.scrollToItem(at:IndexPath(item: indexNumber, section: sectionNumber), at: .right, animated: false)
        }

    }
    //MARK: Action back
    @objc func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        ASVideoPlayerController.sharedVideoPlayer.removePlayer(collectionView: self.collection_home)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if checkSearchVideo == false {
            if !self.videosArray.isEmpty {
                self.pausePlayeVideos()
                let cells = self.collection_home.visibleCells
                for cell in cells {
                    guard let homeCell = cell as? HomeCVCell else {
                        return
                    }
                    homeCell.img_play.isHidden = true
                }
            }
        } else {
            print(videosArray.count)
            view_gradient.isHidden = true
            self.cellRegistration()
            DispatchQueue.main.async {
                self.collection_home.reloadData()
                self.reloadItems(self.checkVideoTag, sectionNumber: 0)
                if !self.videosArray.isEmpty {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                        self.pausePlayeVideos()
                    }
                }
            }
        }
    }
    
    //MARK:- IBActions...
    @available(iOS 13.0, *)
    @IBAction func videoTypeClicked(_ sender: UIButton) {
        print("videoTypeClicked")
        for btn in self.btns_videosList {
            if btn.tag == sender.tag {
                btn.setTitleColor(colorYellow, for: .normal)
                //let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchAllSoundVC") as! SearchAllSoundVC
//                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AlbumVC") as! AlbumVC
//                self.navigationController?.pushViewController(vc, animated: true)
            }
            else {
                btn.setTitleColor(UIColor.lightGray, for: .normal)
            }
        }
        if sender.tag != self.selectedList {
            if sender.tag == 1 {
                self.getFollowingUserVideosAPI()
            }
            else {
                self.getHomePageAPI()
            }
        }
    }
    
    //MARK: Functions...
    
//    func likeAnimation() {
//        UIView.animate(withDuration: 0.3, delay: 0, options: .allowUserInteraction, animations: {() -> Void in
//            heartPopup.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
//            heartPopup.alpha = 1.0
//        }, completion: {(_ finished: Bool) -> Void in
//            UIView.animate(withDuration: 0.1, delay: 0, options: .allowUserInteraction, animations: {() -> Void in
//                heartPopup.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
//            }, completion: {(_ finished: Bool) -> Void in
//                UIView.animate(withDuration: 0.3, delay: 0, options: .allowUserInteraction, animations: {() -> Void in
//                    heartPopup.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
//                    heartPopup.alpha = 0.0
//                }, completion: {(_ finished: Bool) -> Void in
//                    heartPopup.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
//                })
//            })
//        })
//    }

    func cellRegistration() {
        self.collection_home.delegate = self
        self.collection_home.dataSource = self
        self.collection_home.register(UINib(nibName: "HomeCVCell", bundle: nil), forCellWithReuseIdentifier: "HomeCVCell")
    }
        
    func showLoginActivity() {
        let alertController = UIAlertController(title: nil, message: "You need to login first to access this section", preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Login", style: .default, handler: { (action) in
            if #available(iOS 13.0, *) {
                if let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate {
                    sceneDelegate.showLoginScreen()
                }
            } else {
                if let appdelegate =  UIApplication.shared.delegate as? AppDelegate {
                    appdelegate.showLoginScreen()
                }
            }
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @objc func getVideos() {
        if self.selectedList == 0 {
            //self.getGeneralVideos()
            self.getHomePageAPI()
        }
    }
    
    func addWaterMark(inputURL: URL, outputURL: URL, handler: @escaping (_ exportSession: AVAssetExportSession?) -> Void) {
        let mixCompostion = AVMutableComposition()
        let asset = AVAsset(url: inputURL)
        let videoTrack = asset.tracks(withMediaType: .video)[0]
        let timeRange = CMTimeRange(start: .zero, end: asset.duration)
        
        let compositionVideoTrack : AVMutableCompositionTrack = mixCompostion.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)!
        
        do {
            try compositionVideoTrack.insertTimeRange(timeRange, of: videoTrack, at: .zero)
            compositionVideoTrack.preferredTransform = videoTrack.preferredTransform
        }
        catch {
            print("error:",error.localizedDescription)
        }
        
        let waterMarkFilter = CIFilter(name: "CISourceOverCompositing")
        let waterMarkImage = CIImage(image: #imageLiteral(resourceName: "lmtek_selected"))
        let videoComposition = AVVideoComposition(asset: asset) { (filteringRequest) in
            let source = filteringRequest.sourceImage.clampedToExtent()
            waterMarkFilter?.setValue(source, forKey: "inputBackgroundImage")
            let transform = CGAffineTransform(translationX: filteringRequest.sourceImage.extent.width - (waterMarkImage?.extent.width)! - 2, y: 0)
            waterMarkFilter?.setValue(waterMarkImage?.transformed(by: transform), forKey: "inputImage")
            filteringRequest.finish(with: (waterMarkFilter?.outputImage)!, context: nil)
        }
        
        guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPreset640x480) else {
            handler(nil)
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mp4
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.videoComposition = videoComposition
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }

    func checkLike(_ tag: Int) {
        if self.selectedList == 0 {
            if isLiked == true {
                let cell = self.collection_home.cellForItem(at: IndexPath(item: tag, section: 0)) as? HomeCVCell
                cell?.btn_heart.setImage(UIImage.init(named: "likeHeart"), for: .normal)
                cell?.lbl_likes.text = totalLiked
            } else {
                let cell = self.collection_home.cellForItem(at: IndexPath(item: tag, section: 0)) as? HomeCVCell
                cell?.btn_heart.setImage(UIImage.init(named: "ic_heart"), for: .normal)
                cell?.lbl_likes.text = totalLiked
            }
        } else {
            if isLiked == true {
                let cell = self.collection_home.cellForItem(at: IndexPath(item: tag, section: 0)) as? HomeCVCell
                cell?.btn_heart.setImage(UIImage.init(named: "likeHeart"), for: .normal)
                cell?.lbl_likes.text = totalLiked
            } else {
                let cell = self.collection_home.cellForItem(at: IndexPath(item: tag, section: 0)) as? HomeCVCell
                cell?.btn_heart.setImage(UIImage.init(named: "ic_heart"), for: .normal)
                cell?.lbl_likes.text = totalLiked
            }
        }
    }
    
    func checkShare(_ tag: Int) {
        if self.selectedList == 0 {
            if isLiked == true {
                let cell = self.collection_home.cellForItem(at: IndexPath(item: tag, section: 0)) as? HomeCVCell
                cell?.lbl_shares.text = totalShared
            } else {
                let cell = self.collection_home.cellForItem(at: IndexPath(item: tag, section: 0)) as? HomeCVCell
                cell?.lbl_shares.text = totalShared
            }
        } else {
            if isLiked == true {
                let cell = self.collection_home.cellForItem(at: IndexPath(item: tag, section: 0)) as? HomeCVCell
                cell?.lbl_shares.text = totalShared
            } else {
                let cell = self.collection_home.cellForItem(at: IndexPath(item: tag, section: 0)) as? HomeCVCell
                cell?.lbl_shares.text = totalShared
            }
        }
    }
    
    func checkDownloads(_ tag: Int) {
        if self.selectedList == 0 {
            if isLiked == true {
                let cell = self.collection_home.cellForItem(at: IndexPath(item: tag, section: 0)) as? HomeCVCell
                cell?.lblDownloads.text = totalDownloads
            } else {
                let cell = self.collection_home.cellForItem(at: IndexPath(item: tag, section: 0)) as? HomeCVCell
                cell?.lblDownloads.text = totalDownloads
            }
        } else {
            if isLiked == true {
                let cell = self.collection_home.cellForItem(at: IndexPath(item: tag, section: 0)) as? HomeCVCell
                cell?.lblDownloads.text = totalDownloads
            } else {
                let cell = self.collection_home.cellForItem(at: IndexPath(item: tag, section: 0)) as? HomeCVCell
                cell?.lblDownloads.text = totalDownloads
            }
        }
    }
}

@available(iOS 13.0, *)
extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    //MARK: CollectionView Delegates...paw
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if selectedList == 0 {
            return self.videosArray.count
        }
        return self.followingVideosArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCVCell", for: indexPath) as! HomeCVCell
                
        cell.invalidateIntrinsicContentSize()
        cell.layoutIfNeeded()
        
        cell.delegate = self
        
        var model : GetVideosData
        
        if self.selectedList == 0 {
            model = self.videosArray[indexPath.item]
        }
        else {
            model = self.followingVideosArray[indexPath.item]
        }
        if checkSearchVideo == true {
        } else {
            cell.constraint_reportButtonBottom.constant = (self.tabBarController?.tabBar.frame.size.height)! + 10
        }
        cell.loadVideoDetails(model: model)
        self.markVideoWatchedAPI(String(describing: model.video_id) )
        
        if self.selectedList == 0 {
            if indexPath.item == self.videosArray.count - 1 {
                print("last cell:",self.skipVideos)
                if self.skipVideos % 20 == 0 {
                    print("get videos from api:",self.skipVideos)
                    self.getHomePageAPI()
                }
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let videoCell = cell as? ASAutoPlayVideoLayerContainer, let _ = videoCell.videoURL {
            ASVideoPlayerController.sharedVideoPlayer.removeLayerFor(cell: videoCell)
            if !ASVideoPlayerController.sharedVideoPlayer.shouldPlay {
                ASVideoPlayerController.sharedVideoPlayer.shouldPlay.toggle()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let videoCell = cell as? HomeCVCell {
            videoCell.img_play.isHidden = true//ASVideoPlayerController.sharedVideoPlayer.shouldPlay
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            if let cell = collectionView.cellForItem(at: indexPath) as? HomeCVCell {
                ASVideoPlayerController.sharedVideoPlayer.shouldPlay.toggle()
                cell.img_play.isHidden = ASVideoPlayerController.sharedVideoPlayer.shouldPlay
            }
        }
    }
    
    func pausePlayeVideos() {
        print("pausePlayeVideos")
        ASVideoPlayerController.sharedVideoPlayer.pausePlayeVideosFor(collectionView: self.collection_home)
    }
}

@available(iOS 13.0, *)
extension HomeVC: UIScrollViewDelegate {
    //MARK: ScrollView Delegates...
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEndDecelerating")
        self.pausePlayeVideos()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        print("scrollViewDidEndDecelerating")
        if scrollView.contentOffset == .zero {
            print("zero")
           if videosArray.count > 0 {
               videosArray.removeAll()
               self.getHomePageAPI()
            }
        }
        if !decelerate {
            self.pausePlayeVideos()
        }
    }
}

@available(iOS 13.0, *)
extension HomeVC: HomeCVCellDelegate {
    
    //MARK: HomeCVCell Delegates...
    func commentClicked(cell: HomeCVCell) {
        if UserDefaults.standard.bool(forKey: Constants.KEYS.isLoggedIn) {
            if let indexPath = self.collection_home.indexPath(for: cell) {
                if self.selectedList == 0 {
                    let checkComment = self.videosArray[indexPath.item].allow_comment
                    if String(describing: checkComment) == "0" {
                        ToastMessage.showToast(in: self, message: "comment is not allowed for this video", bgColor: .white)
                    }else{
                        let commentsView = Bundle.main.loadNibNamed("CommentsView", owner: nil, options: nil)![0] as! CommentsView
                        commentsView.video_model = self.videosArray[indexPath.item]
                        commentsView.tag = indexPath.item
                        commentsView.delegate = self
                        let window = UIApplication.shared.keyWindow!
                        window.addSubview(commentsView)
                    }
                }
                else {
                    let checkComment = self.followingVideosArray[indexPath.item].allow_comment
                    if String(describing: checkComment) == "0" {
                        ToastMessage.showToast(in: self, message: "comment is not allowed for this video", bgColor: .white)
                    }else{
                    let commentsView = Bundle.main.loadNibNamed("CommentsView", owner: nil, options: nil)![0] as! CommentsView
                    commentsView.video_model = self.followingVideosArray[indexPath.item]
                    commentsView.tag = indexPath.item
                    commentsView.delegate = self
                    let window = UIApplication.shared.keyWindow!
                    window.addSubview(commentsView)
                }
               }
            }
        }
        else {
            self.showLoginActivity()
        }
    }
    
    func likeClicked(cell: HomeCVCell) {
        if UserDefaults.standard.bool(forKey: Constants.KEYS.isLoggedIn) {
            if let indexPath = self.collection_home.indexPath(for: cell) {
                if self.selectedList == 0 {
                    self.likeVideoAPI(indexPath.item, video_model: self.videosArray[indexPath.item])
                }
                else {
                    self.likeVideoAPI(indexPath.item, video_model: self.followingVideosArray[indexPath.item])
                }
            }
        }
        else {
            self.showLoginActivity()
        }
    }
    
    func reportClicked(cell: HomeCVCell) {
        if let indexPath = self.collection_home.indexPath(for: cell) {
            let reportVC = self.storyboard?.instantiateViewController(withIdentifier: "ReportVC") as! ReportVC
            if self.selectedList == 0 {
                reportVC.video_model = self.videosArray[indexPath.item]
            }
            else {
                reportVC.video_model = self.followingVideosArray[indexPath.item]
            }
            self.navigationController?.pushViewController(reportVC, animated: true)
        }
    }
    func profileImageClicked(cell: HomeCVCell) {
        guard let indexPath = self.collection_home.indexPath(for: cell) else { return }
        
        var video_model : GetVideosData
        if self.selectedList == 0 {
            video_model = self.videosArray[indexPath.item]
        }
        else {
            video_model = self.followingVideosArray[indexPath.item]
        }
        if let loginDict = UserDefaults.standard.value(forKey: Constants.KEYS.LoginResponse) as? [String : Any], (loginDict["id"] as? String)! == String(describing: video_model.user_id as? String ) {
            self.tabBarController?.selectedIndex = 4
            if let tabBar = self.tabBarController?.tabBar, let items = tabBar.items {
                self.tabBarController?.tabBar(tabBar, didSelect: items[4])
            }
            return
        }
        
        let user_id = video_model.user_id
        
        let userProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "UserProfileVC") as! UserProfileVC
        userProfileVC.delegate = self
        userProfileVC.isFollowing = (video_model.is_following as? String == "1") ? true : false
        userProfileVC.videoIndex = indexPath.item
        userProfileVC.user_id = user_id as! String
        self.navigationController?.pushViewController(userProfileVC, animated: true)
        
    }
    
    func shareClicked(cell: HomeCVCell) {
        if UserDefaults.standard.bool(forKey: Constants.KEYS.isLoggedIn) {
            guard let indexPath = self.collection_home.indexPath(for: cell) else {
                if let tabBar = self.tabBarController?.tabBar, let items = tabBar.items {
                    self.tabBarController?.tabBar(tabBar, didSelect: items[Constants.previousTab])
                }
                return
            }
            var video_model : GetVideosData
            if self.selectedList == 0 {
                video_model = self.videosArray[indexPath.item]
            }
            else {
                video_model = self.followingVideosArray[indexPath.item]
            }
            //let video_model = self.videosArray[indexPath.item]
            print("shareClicked:",video_model)
            //self.doShareVideo(video_model: video_model, at: indexPath.item)
            if let url = URL(string: String(describing: video_model.video_url ?? "")) {
                let linkToShare = [url]
                let activityViewController = UIActivityViewController(activityItems: linkToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view // so that ipad won't crash
                
                activityViewController.completionWithItemsHandler = {(activityType: UIActivity.ActivityType?, completed: Bool, returnedItems:[Any]?, error: Error?) in
                    
                    print("activityType:",activityType, ", completed:",completed)
                    if let type = activityType {
                        self.doShareVideoAPI(indexPath.row, video_model: video_model)
                    }
                }
                DispatchQueue.main.async {
                    self.present(activityViewController, animated: true, completion: nil)
                }
            }
        }
        else {
            self.showLoginActivity()
        }
    }
    
    func downloadClicked(cell: HomeCVCell) {
        print("downloadClicked")
        if UserDefaults.standard.bool(forKey: Constants.KEYS.isLoggedIn) {
            if let indexPath = self.collection_home.indexPath(for: cell) {
                var video_model : GetVideosData
                if self.selectedList == 0 {
                    video_model = self.videosArray[indexPath.item]
                }
                else {
                    video_model = self.followingVideosArray[indexPath.item]
                }
                if video_model.allow_download as! String == "1" {
                    
                    if let video_url = URL(string: String(describing: video_model.video_url ?? "")) {
                        if String(describing: video_model.allow_download) == "0"{
                            ToastMessage.showToast(in: self, message: "download is not allowed for this video", bgColor: .white)
                        }else {
                            self.downloadVideo(url: video_url)
                            self.doDownloadVideoAPI(indexPath.row, video_id: String(describing: video_model.video_id ?? ""))
                        }
                    }
                }
            }
        }
        else {
            self.showLoginActivity()
        }
    }
    
    func followClicked(cell: HomeCVCell) {
        if UserDefaults.standard.bool(forKey: Constants.KEYS.isLoggedIn) {
            if let indexPath = self.collection_home.indexPath(for: cell) {
                var video_model : GetVideosData
                if self.selectedList == 0 {
                    video_model = self.videosArray[indexPath.item]
                }
                else {
                    video_model = self.followingVideosArray[indexPath.item]
                }
                //self.doFollowUser(video_model, at: indexPath.item)
                self.doFollowUserAPI(String(describing: video_model.user_id ?? "") , is_following: String(describing: video_model.is_following))
            }
        }
        else {
            self.showLoginActivity()
        }
    }
    
    func duetClicked(cell: HomeCVCell) {
        if UserDefaults.standard.bool(forKey: Constants.KEYS.isLoggedIn) {
            if let indexPath = self.collection_home.indexPath(for: cell) {
                var video_model : GetVideosData
                if self.selectedList == 0 {
                    video_model = self.videosArray[indexPath.item]
                }
                else {
                    video_model = self.followingVideosArray[indexPath.item]
                }
                if video_model.allow_duet as! String == "1" {
                    DispatchQueue.main.async {
                        let window = UIApplication.shared.keyWindow!
                        DialougeUtils.addActivityView(view:window)
                        if let url = URL(string: video_model.video_url as! String) {
                            let urlSession = URLSession(configuration: .default)
                            urlSession.downloadTask(with: url) { (videoURL, response, err) in
                                if let error = err {
                                    print("download error for duet:",error.localizedDescription)
                                }
                                if let vidURL = videoURL, let urlData = NSData(contentsOf: vidURL), let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                                    DispatchQueue.main.async {
                                        let outputPath = documentsURL.appendingPathComponent("videoForDuet.mp4")
                                        urlData.write(to: outputPath, atomically: true)
                                        let duetVideoRecordingVC = self.storyboard?.instantiateViewController(withIdentifier: "DuetVideoRecordingVC") as! DuetVideoRecordingVC
                                        duetVideoRecordingVC.video_model = video_model
                                        duetVideoRecordingVC.isDuet = "1"
                                        duetVideoRecordingVC.videoDuetURL = outputPath
                                        DispatchQueue.main.async {
                                            DialougeUtils.removeActivityView(view: window)
                                            self.navigationController?.pushViewController(duetVideoRecordingVC, animated: true)
                                        }
                                    }
                                }
                                else {
                                    print("duet download else")
                                    DispatchQueue.main.async {
                                        DialougeUtils.removeActivityView(view: window)
                                    }
                                }
                            }.resume()
                        }
                    }
                }
                else {
                    ToastMessage.showToast(in: self, message: "Duet is not allowed for this video", bgColor: .white)
                }
            }
        }
        else {
            self.showLoginActivity()
        }
    }
    
    func downloadVideo(url: URL) {
        print("downloadVideo url:",url)
        let session = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
        let downloadTask = session.downloadTask(with: url)
        downloadTask.resume()
        self.downloadProgressView = Bundle.main.loadNibNamed("DownloadProgressView", owner: nil, options: nil)![0] as? DownloadProgressView
        self.view.addSubview(self.downloadProgressView!)
    }
    
}

@available(iOS 13.0, *)
extension HomeVC: URLSessionDownloadDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        print("didFinishDownloadingTo")
        DispatchQueue.main.async {
            self.downloadProgressView?.removeFromSuperview()
            self.downloadProgressView = nil
        }
        guard let nsData = NSData(contentsOf: location), let data = nsData as Data? else { return }
        let videoAsset : AVAsset = data.getAVAsset() //AVURLAsset = AVURLAsset(url: location, options: nil)
        
        self.mixVideoAsset(videoAsset: videoAsset)
        return
        guard let watermarkImage = UIImage.gifImageWithName("lmtek-watermark") else {
            print("watermark error")
            return
        }
        guard let url = Bundle.main.url(forResource: "lmtek-watermark", withExtension: "gif"), let gifData = NSData(contentsOf: url) as Data? else { return }
        let tempURL = URL(fileURLWithPath: NSTemporaryDirectory().appending("temp.mp4"))
        GIF2MP4(data: gifData)?.convertAndExport(to: tempURL, completion: {
            let gifAsset = AVAsset(url: tempURL)
            //self.mergeVideoAssets(savedVideoAsset: videoAsset, gifAsset: gifAsset)
            print("download videos")
        })
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        print("downloadTask didWriteData bytesWritten:",bytesWritten," totalBytesWritten:",totalBytesWritten," totalBytesExpectedToWrite:",totalBytesExpectedToWrite)
        let progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        DispatchQueue.main.async {
            self.downloadProgressView?.progress_download.progress = progress
        }
    }
    
    func mixVideoAsset(videoAsset: AVAsset) {
        
        var mutableVideoComposition = AVMutableVideoComposition()
        var mixComposition = AVMutableComposition()
                
        let videoTrack = videoAsset.tracks(withMediaType: .video)[0]
                
        let mutableCompositionVideoTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)!
        
        do {
            try mutableCompositionVideoTrack.insertTimeRange(CMTimeRange(start: .zero, duration: videoAsset.duration), of: videoTrack, at: .zero)
        }
        catch {
            print("savedmutabletrack error:",error.localizedDescription)
        }
        
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRange(start: .zero, duration: videoAsset.duration)
        
        //let videoLayerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: mutableCompositionVideoTrack)
        let transform = videoTrack.preferredTransform
        let assetInfo = orientationFromTransform(transform)
        var videoSize : CGSize
        if assetInfo.isPortrait {
            videoSize = CGSize(width: videoTrack.naturalSize.height, height: videoTrack.naturalSize.width)
        }
        else {
            videoSize = videoTrack.naturalSize
        }
        print("assetInfo:",assetInfo)
        let videoLayerInstruction = self.compositionLayerInstruction(
        for: mutableCompositionVideoTrack,
        assetTrack: videoTrack)
        
        mainInstruction.layerInstructions = [videoLayerInstruction]
        
        mutableVideoComposition.instructions = [mainInstruction]
        mutableVideoComposition.frameDuration = CMTime(value: 1, timescale: 30)
        let width = UIScreen.main.bounds.width
        let compositionWidth = (Int(width) % 2 == 0) ? width : width - 1
        mutableVideoComposition.renderSize = videoSize/*CGSize(
        width: /*UIScreen.main.bounds.width*/compositionWidth,
        height: UIScreen.main.bounds.height)*/
        if let audioTrackFirst = videoAsset.tracks(withMediaType: .audio).first {
            let audioTrack = mixComposition.addMutableTrack(
                withMediaType: .audio,
                preferredTrackID: kCMPersistentTrackID_Invalid)
            
            do {
                
                try audioTrack?.insertTimeRange(
                    CMTimeRangeMake(
                        start: CMTime.zero,
                        duration: videoAsset.duration),
                    of: /*savedVideoAsset.tracks(withMediaType: .audio)[0]*/audioTrackFirst,
                    at: .zero)
            } catch {
                print("Failed to load Audio track")
            }
        }
        self.applyVideoEffectsToComposition(composition: mutableVideoComposition, size: videoSize, duration: videoAsset.duration)//naturalSize)
        
        let savePathURL = URL(fileURLWithPath: NSHomeDirectory() + "/Documents/camRecordedVideo.mp4")
        do {
            try FileManager.default.removeItem(at: savePathURL)
        }
        catch {
            print("file remove error:",error.localizedDescription)
        }
        
        DispatchQueue.main.async {
            let window = UIApplication.shared.keyWindow!
            DialougeUtils.addActivityView(view:window)
            let finalPath = savePathURL.absoluteString
            let assetExport = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)!
            assetExport.videoComposition = mutableVideoComposition
            assetExport.outputFileType = .mp4
            
            assetExport.outputURL = savePathURL
            assetExport.shouldOptimizeForNetworkUse = true
            
            assetExport.exportAsynchronously(completionHandler: {
                print("duet merge exporter")
                DialougeUtils.removeActivityView(view: window)
                switch assetExport.status {
                case .completed:
                    print("success")
                    DispatchQueue.global(qos: .background).async {
                        if let videoURL = assetExport.outputURL, let videoData = NSData(contentsOf: videoURL) {
                            print("downloadClicked videoURL:")
                            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                            let filePath = "\(documentsPath)/\(Int64(Date().timeIntervalSince1970)).mp4"
                            DispatchQueue.main.async {
                                videoData.write(toFile: filePath, atomically: true)
                                PHPhotoLibrary.shared().performChanges({
                                    print("downloadClicked performChanges")
                                    PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: filePath))
                                }) { (completed, error) in
                                    print("downloadClicked completed:",completed," , error:",error ?? "")
                                    if completed {
                                        let url = URL(fileURLWithPath: filePath)
                                        _ = [url]
                                        DispatchQueue.main.async {
                                            ToastMessage.showToast(in: self, message: "Saved Video", bgColor: .white)
                                        }
                                    }
                                }
                            }
                        }
                    }
                case .failed:
                    print("failed",assetExport.error ?? "")
                case .cancelled:
                    print("cancelled",assetExport.error ?? "")
                default:
                    print("default switch")
                }
            })
        }
    }
    
    func applyVideoEffectsToComposition(composition: AVMutableVideoComposition, size: CGSize, duration: CMTime) {
        let parentLayer = CALayer()
        let videoLayer = CALayer()
        parentLayer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        videoLayer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        parentLayer.addSublayer(videoLayer)
        
        var size1 = size
        
        size1.width = 100
        size1.height = 100
                
        guard let fileURL = Bundle.main.url(forResource: "lmtek-watermark", withExtension: "gif") else { return }
        
        let animation = animationGIFWithURL(url: fileURL)
        let animationDuration = animation.duration
        
        var numberOfAnimation = 1
        if duration.seconds > animationDuration {
            numberOfAnimation = Int(ceil(duration.seconds / animationDuration))
        }
        
        for i in 0 ..< numberOfAnimation {
            let overLayer = CALayer()
            overLayer.backgroundColor = UIColor.clear.cgColor
            let textLayer = CATextLayer()
            textLayer.backgroundColor = UIColor.clear.cgColor
            let attributedText = NSAttributedString(
            string: "@lmtek",
            attributes: [
              .font: UIFont(name: "OpenSans-Regular", size: 20) as Any,
              .foregroundColor: UIColor.white,
              .strokeColor: UIColor(hexString: Constants.ORANGE_COLOR)!,
              .strokeWidth: -1])
            textLayer.string = attributedText
            if i % 2 == 0 {
                overLayer.frame = CGRect(x: 0, y: videoLayer.frame.size.height - 100, width: size1.width, height: size1.height)
                textLayer.frame = CGRect(x: 0, y: overLayer.frame.origin.y - 5, width: size.width, height: 30)
                textLayer.alignmentMode = .left
            }
            else {
                overLayer.frame = CGRect(x: videoLayer.frame.size.width - 100, y: 0, width: size1.width, height: size1.height)
                textLayer.frame = CGRect(x: 0, y: 0, width: size.width, height: 30)
                textLayer.alignmentMode = .right
            }
            animation.beginTime = i == 0 ? AVCoreAnimationBeginTimeAtZero : Double(i) * animationDuration
            self.startGifAnimation(animation: animation, inLayer: overLayer)
            textLayer.beginTime = i == 0 ? AVCoreAnimationBeginTimeAtZero : Double(i) * animationDuration
            textLayer.duration = animationDuration
            textLayer.displayIfNeeded()
            //overLayer.addSublayer(textLayer)
            parentLayer.addSublayer(overLayer)
            parentLayer.addSublayer(textLayer)
        }
        
        composition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: parentLayer)
    }
    
    func startGifAnimation(/*url: URL,*/animation: CAKeyframeAnimation, inLayer layer: CALayer) {
        //let animation = self.animationGIFWithURL(url: url)
        
        layer.add(animation, forKey: "contents")
    }
    
    func animationGIFWithURL(url: URL) -> CAKeyframeAnimation {
        let animation = CAKeyframeAnimation(keyPath: "contents")
        
        var frames = [CGImage]()
        var delayTimes = [CGFloat]()
        
        var totalTime : CGFloat = 0.0
        var gifWidth : CGFloat = 0.0
        var gifHeight : CGFloat = 0.0
        let gifSource = CGImageSourceCreateWithURL(url as CFURL, nil)!
        
        let frameCount = CGImageSourceGetCount(gifSource)
        
        for i in 0 ..< frameCount {
            guard let frame = CGImageSourceCreateImageAtIndex(gifSource, i, nil) else { continue }
            frames.append(frame)
            
            let cfDictionary = CGImageSourceCopyPropertiesAtIndex(gifSource, i, nil) as? [AnyHashable : Any] ?? [:]
            
            gifWidth = cfDictionary[kCGImagePropertyPixelWidth] as? CGFloat ?? 0.0
            gifHeight = cfDictionary[kCGImagePropertyPixelHeight] as? CGFloat ?? 0.0
            
            let gifDict = cfDictionary[kCGImagePropertyGIFDictionary] as? [AnyHashable : Any] ?? [:]
            delayTimes.append(gifDict[kCGImagePropertyGIFDelayTime] as? CGFloat ?? 0.0)
            
            totalTime = totalTime + (gifDict[kCGImagePropertyGIFDelayTime] as? CGFloat ?? 0.0)
        }
        
        var times = [NSNumber](repeating: 0, count: 3)
        var currentTime : CGFloat = 0.0
        let count = delayTimes.count
        
        for i in 0 ..< count {
            times.append(NSNumber(value: Float((currentTime / totalTime))))
            currentTime += delayTimes[i]
        }
        
        var images = [Any](repeating: 0, count: 3)
        for i in 0 ..< count {
            if i < frames.count {
                images.append(frames[i])
            }
        }
        
        animation.keyTimes = times
        animation.values = images
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = CFTimeInterval(totalTime)
        animation.repeatCount = 1//.greatestFiniteMagnitude
        
        //animation.beginTime = AVCoreAnimationBeginTimeAtZero
        animation.isRemovedOnCompletion = true//false
        
        return animation
    }
        
    func orientationFromTransform(
      _ transform: CGAffineTransform
    ) -> (orientation: UIImage.Orientation, isPortrait: Bool) {
        var assetOrientation = UIImage.Orientation.up
        var isPortrait = false
        let tfA = transform.a
        let tfB = transform.b
        let tfC = transform.c
        let tfD = transform.d
        
        if tfA == 0 && tfB == 1.0 && tfC == -1.0 && tfD == 0 {
            print("orientationFromTransform 1")
            assetOrientation = .right
            isPortrait = true
        } else if tfA == 0 && tfB == -1.0 && tfC == 1.0 && tfD == 0 {
            print("orientationFromTransform 2")
            assetOrientation = .left
            isPortrait = true
        } else if tfA == 1.0 && tfB == 0 && tfC == 0 && tfD == 1.0 {
            print("orientationFromTransform 3")
            assetOrientation = .up
        } else if tfA == -1.0 && tfB == 0 && tfC == 0 && tfD == -1.0 {
            print("orientationFromTransform 4")
            assetOrientation = .down
        }
        else if tfA == 0.0 && tfB == 1.0 && tfC == 1.0 && tfD == 0.0 {
            print("orientationFromTransform 5")
            assetOrientation = .rightMirrored
            isPortrait = true
        }
        else {
            print("orientationFromTransform else:",tfA, tfB, tfC, tfD)
        }
        return (assetOrientation, isPortrait)
    }
    
    private func compositionLayerInstruction(for track: AVCompositionTrack, assetTrack: AVAssetTrack) -> AVMutableVideoCompositionLayerInstruction {
      let instruction = AVMutableVideoCompositionLayerInstruction(assetTrack: track)
      let transform = assetTrack.preferredTransform

      instruction.setTransform(transform, at: .zero)

      return instruction
    }
}

@available(iOS 13.0, *)
extension HomeVC: CommentsViewDelegate {
    //MARK:  CommentsView Delegates...
    func updateComments(view: CommentsView) {
        if self.selectedList == 0 {
            self.videosArray[view.tag].comments = view.video_model?.comments
        }
        else {
            self.followingVideosArray[view.tag].comments = view.video_model?.comments
        }
        if let cell = self.collection_home.cellForItem(at: IndexPath(item: view.tag, section: 0)) as? HomeCVCell {
            if self.selectedList == 0 {
                cell.loadVideoDetails(model: self.videosArray[view.tag])
                cell.lbl_comments.text = view.commentsCount
            }
            else {
                cell.loadVideoDetails(model: self.followingVideosArray[view.tag])
                cell.lbl_comments.text = view.commentsCount
            }
        }
    }
}

@available(iOS 13.0, *)
extension HomeVC : UserProfileVCDelegate {
    //MARK:- UserProfileVC Delegates...
    func followUser(_ isFollowing: Bool, at index: Int?,user_id: String?) {
        if let ind = index {
            //var model : GetVideosData
            if self.selectedList == 0 {
                for i in 0 ..< self.videosArray.count {
                    let videoModel = self.videosArray[i]
                    if String(describing: videoModel.user_id ?? "") == user_id {
                        //videoModel.isFollowing = isFollowing ? "1" : "0"
                        self.videosArray[i] = videoModel
                    }
                }
                self.collection_home.reloadData()
            }
            else {
                self.getFollowingUserVideosAPI()
            }
            
        }
    }
}

@available(iOS 13.0, *)
extension HomeVC {
    //MARK: API's...
    
    func getHomePageAPI() {
        let url = URL(string: Constants.BASE_URL+Constants.GET_VIDEOS_LIST_PAGINATION)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        DialougeUtils.addActivityView(view: self.view)
        self.getvideosHomePageAPI(url, header: headers)
    }
    
    func getvideosHomePageAPI(_ url: URL, header: HTTPHeaders) {
        LoginSignUpAPI.getUserDataAPI(vc: self, url: url, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            
                            let userData = result!["data"] as? [[String : Any]] ?? []
                            for i in 0..<userData.count {
                                let dictdata: [String: Any] = userData[i]
                                let objdata: GetVideosData = GetVideosData.init(VideoInfo: dictdata)!
                                self.videosArray.append(objdata)
                            }
                            self.skipVideos += self.videosArray.count
                            self.selectedList = 0
                            print("getGeneralVideos videosArray:",self.videosArray)
                            self.cellRegistration()
                            DispatchQueue.main.async {
                                self.collection_home.reloadData()
                                if !self.videosArray.isEmpty {
                                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                                        self.pausePlayeVideos()
                                    }
                                }
                            }
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                    self.showAlertMessage("server error")
                }
            }
        }
    }
    
    func likeVideoAPI(_ index: Int, video_model: GetVideosData) {
        
        let video_id: String = video_model.video_id as! String
        
        let url = URL(string: Constants.BASE_URL+Constants.LIKE_VIDEO)!
        let parameters: Parameters = ["video_id": video_id]
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        print(parameters)
        DialougeUtils.addActivityView(view: self.view)
        self.likeVideoAPICall(parameters, url: url, index: index, header: headers)

    }
    func likeVideoAPICall(_ param: Parameters, url: URL, index: Int, header: HTTPHeaders){
        AllUserApis.allPostDataWithoutImageAPI(vc: self, url: url, parameter: param, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status: Bool = result!["status"] as! Bool
                        if status == true{
                            let data: NSDictionary = result!["user_data"] as! NSDictionary
                            var model : GetVideosData
                            self.totalLiked = data["total_likes"] as? String ?? ""
                            if self.selectedList == 0 {
                                let likeAny: Any? = data["is_liked"]
                                let like:String = String(describing: likeAny ?? "")
                                if like == "0" {
                                    self.isLiked = false
                                } else {
                                    self.isLiked = true
                                }
                                self.videosArray[index].is_liked = data["total_likes"] as? String
                                self.videosArray[index].is_liked = (result!["message"] as? String == "Liked") ? "1" : "0"
                                model = self.videosArray[index]
                            }
                            else {
                                let like:Any? = data["is_liked"]
                                if String(describing: like) == "0" {
                                    self.isLiked = false
                                } else {
                                    self.isLiked = true
                                }
                                self.followingVideosArray[index].is_liked = data["total_likes"] as? String
                                self.followingVideosArray[index].is_liked = (result!["message"] as? String == "Liked") ? "1" : "0"//(video_model.is_liked == "1") ? "0" : "1"
                                model = self.followingVideosArray[index]
                            }
                            let cell = self.collection_home.cellForItem(at: IndexPath(item: index, section: 0)) as? HomeCVCell
                            self.checkLike(index)
                            
                            //cell?.loadVideoDetails(model: model)
                        }else{
                            let message:String = result!["message"] as! String
                            ToastMessage.showToast(in: self, message: message, bgColor: .white)
                        }
                    }
                } else {
                    self.showAlertMessage("server error")
                }
            }
        }
    }

    func getVideo(video_model: GetVideosData, at index: Int) {
        TransportManager.sharedInstance.getVideo(video_id: String(describing: video_model.video_id ?? "")) { (data, err) in
            if let error = err {
                print("getVideo error:",error.localizedDescription)
                ToastMessage.showToast(in: self, message: error.localizedDescription, bgColor: .white)
            }
            else {
                if let json = data as? JSON, let resultObj = json.dictionaryObject {
                    print("getVideo resultObj:",resultObj)
                    if let model = resultObj["data"] {
                        if self.selectedList == 0 {
                            self.videosArray[index] = model as! GetVideosData
                        }
                        else {
                            self.followingVideosArray[index] = model as! GetVideosData
                        }
                        let cell = self.collection_home.cellForItem(at: IndexPath(item: index, section: 0)) as? HomeCVCell
                        cell?.loadVideoDetails(model: model as! GetVideosData)
                    }
                }
            }
        }
    }
    
//    func doShareVideo(video_model: GetVideosData, at index: Int) {
//        TransportManager.sharedInstance.doShare(video_id: String(describing: video_model.video_id ?? "")) { (data, err) in
//            if let error = err {
//                self.view.makeToast(message: error.localizedDescription)
//            }
//            else {
//                if let json = data as? JSON, let resultObj = json.dictionaryObject {
//                    print("doShareVideo resultObj:",resultObj)
//                    if resultObj["status"] as? Bool == true {
//                        let data = resultObj["data"] as? [String : Any] ?? [:]
//                        if self.selectedList == 0 {
//                            self.videosArray[index].shares = data["share"] as! String//String
//                        }
//                        else {
//                            self.followingVideosArray[index].shares =  data["share"] as! String //data["share"] as? Int//String
//                        }
//                        let homeCell = self.collection_home.cellForItem(at: IndexPath(item: index, section: 0)) as? HomeCVCell
//                        if self.selectedList == 0 {
//                            homeCell?.loadVideoDetails(model: self.videosArray[index])
//                        }
//                        else {
//                            homeCell?.loadVideoDetails(model: self.followingVideosArray[index])
//                        }
//                    }
//                }
//            }
//        }
//    }
    func doShareVideoAPI(_ index: Int, video_model: GetVideosData) {
        let video_id: String = video_model.video_id as! String
        
        let url = URL(string: Constants.BASE_URL+Constants.DO_SHARE)!
        let parameters: Parameters = ["video_id": video_id]
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        print(parameters)
        DialougeUtils.addActivityView(view: self.view)
        self.doShareVideoAPICall(parameters, url: url, index: index, header: headers)

    }
    
    func doShareVideoAPICall(_ param: Parameters, url: URL, index: Int, header: HTTPHeaders){
        AllUserApis.allPostDataWithoutImageAPI(vc: self, url: url, parameter: param, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status: Bool = result!["status"] as! Bool
                        if status == true{
                            let data = result!["data"] as? [String : Any] ?? [:]
                            if self.selectedList == 0 {
                                self.totalShared = result!["user_data"] as? String ?? ""
                            }
                            else {
                                self.totalShared = result!["user_data"] as? String ?? ""
                            }
                            let homeCell = self.collection_home.cellForItem(at: IndexPath(item: index, section: 0)) as? HomeCVCell
                            if self.selectedList == 0 {
                                homeCell?.loadVideoDetails(model: self.videosArray[index])
                            }
                            else {
                                homeCell?.loadVideoDetails(model: self.followingVideosArray[index])
                            }
                            self.checkShare(index)
                        }else{
                            let message:String = result!["message"] as! String
                            ToastMessage.showToast(in: self, message: message, bgColor: .white)
                        }
                    }
                } else {
                    self.showAlertMessage("server error")
                }
            }
        }
    }
    
//    func doDownloadVideo(video_model: GetVideosData, at index: Int) {
//        TransportManager.sharedInstance.doDownload(video_id: String(describing: video_model.video_id ?? "")) { (data, err) in
//            if let error = err {
//                self.view.makeToast(message: error.localizedDescription)
//            }
//            else {
//                if let json = data as? JSON, let resultObj = json.dictionaryObject {
//                    print("doDownloadVideo resultObj:",resultObj)
//                }
//            }
//        }
//    }
    
    func doDownloadVideoAPI(_ index: Int, video_id: String) {
        let video_id: String = video_id
        
        let url = URL(string: Constants.BASE_URL+Constants.DO_DOWNLOAD)!
        let parameters: Parameters = ["video_id": video_id]
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        print(parameters)
        DialougeUtils.addActivityView(view: self.view)
        self.doDownloadVideoAPICall(parameters, url: url, index: index, header: headers)
    }
    
    func doDownloadVideoAPICall(_ param: Parameters, url: URL, index: Int, header: HTTPHeaders){
        AllUserApis.allPostDataWithoutImageAPI(vc: self, url: url, parameter: param, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status: Bool = result!["status"] as! Bool
                        if status == true{
                            print("doDownloadVideo resultObj:",result!)
                            self.totalDownloads = result!["user_data"] as? String ?? ""
                        }else{
                            let message:String = result!["message"] as! String
                            ToastMessage.showToast(in: self, message: message, bgColor: .white)
                        }
                        self.checkDownloads(index)
                    }
                } else {
                    self.showAlertMessage("server error")
                }
            }
        }
    }
    
    func getFollowingUserVideosAPI() {
        let url = URL(string: Constants.BASE_URL+Constants.FOLLOWING_VIDEOS)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        DialougeUtils.addActivityView(view: self.view)
        self.getFollowingUserVideosAPICall(url, header: headers)
    }
    
    func getFollowingUserVideosAPICall(_ url: URL, header: HTTPHeaders){
        LoginSignUpAPI.getUserDataAPI(vc: self, url: url, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            let dictdata: [[String : Any]] = result!["user_data"] as! [[String : Any]]
                            
                            for i in 0..<dictdata.count {
                                let objdata: [String: Any] = dictdata[i]
                                let dataobj: GetVideosData = GetVideosData.init(VideoInfo: objdata)!
                                self.followingVideosArray.append(dataobj)
                            }
                            
                            self.selectedList = 1
                            print("getFollowingVideosList videosArray:",self.videosArray)
                            self.cellRegistration()
                            DispatchQueue.main.async {
                                self.collection_home.reloadData()
                                if !self.followingVideosArray.isEmpty {
                                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                                        self.pausePlayeVideos()
                                    }
                                }
                            }
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                    self.showAlertMessage("server error")
                }
            }
        }
    }
    
    //FOLLOW_USER
    
    func doFollowUserAPI(_ follower_user_id: String,is_following: String ){
        let url = URL(string: Constants.BASE_URL+Constants.FOLLOW_USER)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        print(headers)
        let param: Parameters = ["follower_user_id": follower_user_id]
        print(param)
        DialougeUtils.addActivityView(view: self.view)
        self.doFollowUserAPICall(param, url: url, header: headers,follower_user_id: follower_user_id, is_following: is_following)
    }
    
    func doFollowUserAPICall(_ param: Parameters, url: URL, header: HTTPHeaders,follower_user_id: String, is_following: String){
        AllUserApis.allPostDataWithoutImageAPI(vc: self, url: url, parameter: param, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status: Bool = result!["status"] as! Bool
                        if status == true{
//                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.PROFILE_UPDATE_NOTIFICATION), object: nil)
                            if self.selectedList == 0 {
                                for i in 0 ..< self.videosArray.count {
                                    let videoModel = self.videosArray[i]
                                    if String(describing: videoModel.user_id ?? "") == follower_user_id {
                                        videoModel.is_following = (is_following == "1") ? "0" : "1"
                                        self.videosArray[i] = videoModel
                                    }
                                }
                            }
                            else {
                                self.getFollowingUserVideosAPI()
                            }
                            self.collection_home.reloadData()
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                }
            }
        }
    }
    
    func markVideoWatchedAPI(_ video_id: String){
        let param: Parameters = ["video_id": video_id]
        let url = URL(string: Constants.BASE_URL+Constants.MARK_VIDEO_AS_WATCHED)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        DialougeUtils.addActivityView(view: self.view)
        self.markVideoWatchedAPICall(param, url: url, header: headers)

    }
    
    func markVideoWatchedAPICall(_ param: Parameters, url: URL, header: HTTPHeaders){
        AllUserApis.allPostDataWithoutImageAPI(vc: self, url: url, parameter: param, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status: Bool = result!["status"] as! Bool
                        if status == true{
                            print("markVideoAsWatched resultObj:",result!)
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                }
            }
        }
    }

}

extension Data {
    func getAVAsset() -> AVAsset {
        let directory = NSTemporaryDirectory()
        let fileName = "\(NSUUID().uuidString).mp4"
        let fullURL = NSURL.fileURL(withPathComponents: [directory, fileName])
        try! self.write(to: fullURL!)
        let asset = AVAsset(url: fullURL!)
        return asset
    }
}
