//
//  AlbumVC.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 22/06/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class AlbumVC: BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    var yRef: CGFloat = 0.0
    var allvideosCollectionview:UICollectionView!
    var cellId = "Cell"

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .white
        self.screenDesigning()
    }
    
    func screenDesigning() {
        
        let viewHeader: UIView = UIView()
        viewHeader.frame = CGRect(x: 0, y: yRef, width: screenWidth, height: 70)
        viewHeader.backgroundColor = .white
        self.view.addSubview(viewHeader)
        
        let btnBack:UIButton = UIButton()
        btnBack.frame = CGRect(x: 10, y: 40, width: 15, height: 26)
        btnBack.backgroundColor = .clear
        btnBack.setImage(UIImage.init(named: "LeftArrowIcon")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btnBack.tintColor = .black
        btnBack.clipsToBounds = true
        btnBack.addTarget(self, action: #selector(self.btnBackClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnBack)
        
        let imgThreeDot:UIImageView = UIImageView()
        imgThreeDot.frame = CGRect(x: screenWidth-45, y: 40, width: 25, height: 25)
        imgThreeDot.backgroundColor = .clear
        imgThreeDot.image = UIImage.init(named: "ShareIcon")
        self.view.addSubview(imgThreeDot)
        
        let btnThreeDot:UIButton = UIButton()
        btnThreeDot.frame = CGRect(x: screenWidth-50, y: 40, width: 50, height: 30)
        btnThreeDot.backgroundColor = .clear
        btnThreeDot.addTarget(self, action: #selector(self.btnThreeDotClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnThreeDot)

        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -2)
        layout.itemSize = CGSize(width: (screenWidth/3)-2, height: (screenWidth/3)-2)

        allvideosCollectionview = UICollectionView(frame: CGRect(x: 0, y: 80, width: screenWidth, height: screenHeight-(yRef+40)), collectionViewLayout: layout)
        allvideosCollectionview.dataSource = self
        allvideosCollectionview.delegate = self
        allvideosCollectionview.register(AlbumCollectionCell.self, forCellWithReuseIdentifier: cellId)
        allvideosCollectionview.showsVerticalScrollIndicator = false
        allvideosCollectionview.backgroundColor = UIColor.clear
        self.view.addSubview(allvideosCollectionview)
        
        let btnUseSound:UIButton = UIButton()
        btnUseSound.frame = CGRect(x: (screenWidth-170)/2, y: screenHeight-80, width: 170, height: 35)
        btnUseSound.backgroundColor = colorYellow
        btnUseSound.setTitle("   Use this sound", for: .normal)
        btnUseSound.setImage(UIImage.init(named: "Videocamera"), for: .normal)
        btnUseSound.setTitleColor(.black, for: .normal)
        btnUseSound.titleLabel?.font = fontSizeSemiBoldSmall
        btnUseSound.layer.cornerRadius = 35/2
        btnUseSound.clipsToBounds = true
        btnUseSound.layer.shadowOffset = .zero
        btnUseSound.layer.shadowRadius = 4
        btnUseSound.layer.shadowOpacity = 3
        btnUseSound.layer.shadowColor = UIColor.lightGray.withAlphaComponent(0.7).cgColor
        btnUseSound.layer.masksToBounds = false
        btnUseSound.addTarget(self, action: #selector(self.btnUseSoundClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnUseSound)

    }
    
    //MARK: Action
    
    @objc func btnUseSoundClicked(_ sender: UIButton) {
        
    }
    
    @objc func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func btnThreeDotClicked(_ sender: UIButton) {
        
    }
    
    //MARK: CollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        let viewHeader = UIView()
        viewHeader.frame = CGRect(x: 10, y: 0, width: screenWidth-20, height: 140)
        viewHeader.backgroundColor = UIColor.clear
        viewHeader.clipsToBounds = true
        collectionView.addSubview(viewHeader)

        let imgUserProfile: UIImageView = UIImageView()
        imgUserProfile.frame = CGRect(x: 0, y: yRef, width: 120, height: 120)
        imgUserProfile.backgroundColor = .lightGray
        imgUserProfile.image = UIImage.init(named: "")
        imgUserProfile.layer.cornerRadius = 0
        imgUserProfile.clipsToBounds = true
        viewHeader.addSubview(imgUserProfile)

        let lblProfileUserName:UILabel = UILabel()
        lblProfileUserName.frame = CGRect(x: 140, y: yRef, width: 180, height: 25)
        lblProfileUserName.backgroundColor = UIColor.clear
        lblProfileUserName.text = "The Round"
        lblProfileUserName.textAlignment = .left
        lblProfileUserName.font = fontSizeSemiBoldTitle
        viewHeader.addSubview(lblProfileUserName)

        let lblSongName:UILabel = UILabel()
        lblSongName.frame = CGRect(x: 140, y: yRef+25, width: 180, height: 20)
        lblSongName.backgroundColor = UIColor.clear
        lblSongName.text = "Roddy Roundicch"
        lblSongName.textColor = .lightGray
        lblSongName.textAlignment = .left
        lblSongName.font = fontSizeSmall
        viewHeader.addSubview(lblSongName)

        let lblTotalVideo:UILabel = UILabel()
        lblTotalVideo.frame = CGRect(x: 140, y: yRef+47, width: 180, height: 20)
        lblTotalVideo.backgroundColor = UIColor.clear
        lblTotalVideo.text = "1.7M videos"
        lblTotalVideo.textColor = .lightGray
        lblTotalVideo.textAlignment = .left
        lblTotalVideo.font = fontSizeSmall
        viewHeader.addSubview(lblTotalVideo)

        let btnAddtoFavorite:UIButton = UIButton()
        btnAddtoFavorite.frame = CGRect(x: 140, y: yRef+95, width: 150, height: 25)
        btnAddtoFavorite.backgroundColor = UIColor.white
        btnAddtoFavorite.setTitle("   Add to Favorites", for: .normal)
        btnAddtoFavorite.setTitleColor(.black, for: .normal)
        btnAddtoFavorite.titleLabel?.font = fontSizeSemiBoldSmall
        btnAddtoFavorite.layer.cornerRadius = 2
        btnAddtoFavorite.layer.borderWidth = 1
        btnAddtoFavorite.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        btnAddtoFavorite.setImage(UIImage.init(named: "Bookmark"), for: .normal)
        btnAddtoFavorite.clipsToBounds = true
        btnAddtoFavorite.layer.shadowOffset = .zero
        viewHeader.addSubview(btnAddtoFavorite)

        return CGSize(width: screenWidth-20, height: 140)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 17
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)  as! AlbumCollectionCell
        return Cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchFirstVC") as! SearchFirstVC
        self.navigationController?.pushViewController(vc, animated: true)

    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemSize = ((collectionView.frame.width/3.0)-6)
            return CGSize(width: itemSize, height: itemSize)
    }

}
