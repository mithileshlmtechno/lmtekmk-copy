//
//  LocalVC.swift
//  LmTek
//
//  Created by PTBLR-1128 on 24/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class LocalVC: UIViewController {
    //MARK:- Outlets...
    @IBOutlet weak var view_background : UIView!
    @IBOutlet var btns_topBar : [UIButton]!
    @IBOutlet weak var lbl_dot : UILabel!
    @IBOutlet weak var tbl_notifications : UITableView!
    @IBOutlet weak var lbl_emptyList : UILabel!
    
    //MARK: Variables...
    var selectedTab = 0
    var follwersArray = [(name: "Official Account", status: "Hello There I’m LMTEX, Our Office", isOfficial: true, isFollowing: false), (name: "Spider ledy", status: "Hi,all", isOfficial: false, isFollowing: true), (name: "Rosy Meri", status: "Hi,all", isOfficial: false, isFollowing: false)]
    var followingArray = [(name: "Official Account", status: "Hello There I’m LMTEX, Our Office", isOfficial: true, isFollowing: true), (name: "Spider ledy", status: "Hi,all", isOfficial: false, isFollowing: true), (name: "Rosy Meri", status: "Hi,all", isOfficial: false, isFollowing: true)]
    var isInitial = true
    var followingUsersArray = [FollowUserItem]()
    var followerUsersArray = [FollowUserItem]()
    var notificationsArray = [NotificationItem]()
    
    //MARK: View Life Cycle...
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.cellRegistration()
        if selectedTab == 0{
            self.getNotificationAPI()
        }else if self.selectedTab == 1 {
            self.getFollowersAPI()
        }
        else if self.selectedTab == 2 {
            self.getFollowingsAPI()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if self.isInitial {
            self.isInitial = false
        }
    }
    
    //MARK: IBActions...
    @IBAction func topTabClicked(_ sender: UIButton) {
        if sender.tag != self.selectedTab {
            self.selectedTab = sender.tag
            self.lbl_emptyList.text = ""
            self.lbl_emptyList.isHidden = false
            for btn in self.btns_topBar {
                if btn.tag == sender.tag {
                    btn.setTitleColor(colorYellow, for: .normal)
                    UIView.animate(withDuration: 0.3) {
                        self.lbl_dot.center.x = btn.center.x
                        self.view.layoutIfNeeded()
                    }
                }
                else {
                    btn.setTitleColor(UIColor.lightGray.withAlphaComponent(0.37), for: .normal)
                }
            }
            self.loadDetails()
        }
    }
    
    //MARK: Functions...
    func cellRegistration() {
        self.tbl_notifications.rowHeight = UITableView.automaticDimension
        self.tbl_notifications.estimatedRowHeight = 40
        self.tbl_notifications.register(UINib(nibName: "NotificationTVCell", bundle: nil), forCellReuseIdentifier: "NotificationTVCell")
        self.tbl_notifications.register(UINib(nibName: "FollowerTVCell", bundle: nil), forCellReuseIdentifier: "FollowerTVCell")
    }
    
    func loadDetails() {
        if self.selectedTab == 0 {
            self.getNotificationAPI()
            DispatchQueue.main.async {
                self.tbl_notifications.reloadData()
            }
        }
        else if self.selectedTab == 1 {
            self.getFollowersAPI()
        }
        else if self.selectedTab == 2 {
            self.getFollowingsAPI()
        }
    }
}

extension LocalVC: UITableViewDelegate, UITableViewDataSource {
    //MARK: TableView Delegates...
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.selectedTab {
        case 0:
            return self.notificationsArray.count
        case 1:
            return self.followerUsersArray.count
        case 2:
            return self.followingUsersArray.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.selectedTab == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTVCell") as! NotificationTVCell
            
            let notification = self.notificationsArray[indexPath.row]
            cell.lbl_notification.text = notification.title ?? ""
            
            let added_on = notification.added_on ?? ""
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateFormatter2 = DateFormatter()
            dateFormatter2.dateFormat = "yyyy-MM-dd HH:mm"
            let todayDate = Date().currentUTCTimeZoneDate
            if let date = dateFormatter.date(from: added_on) {
                cell.lbl_date.text = dateFormatter2.string(from: date)
            }
            else {
                cell.lbl_date.text = ""
            }
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FollowerTVCell") as! FollowerTVCell
            var followUser : FollowUserItem
            if self.selectedTab == 1 {
                followUser = self.followerUsersArray[indexPath.row]
            }
            else {
                followUser = self.followingUsersArray[indexPath.row]
            }
            
            cell.lbl_status.text = ""
            cell.lbl_accountName.text = "\(followUser.first_name ?? "") \(followUser.last_name ?? "")"
            cell.img_account.sd_setImage(with: URL(string: followUser.profile_picture ?? ""))
            
            if followUser.is_following == "1" {
                cell.lbl_follow.backgroundColor = colorYellow
                cell.lbl_follow.text = "Following"
            }
            else {
                cell.lbl_follow.backgroundColor = colorYellow
                cell.lbl_follow.text = "Follow"
            }
            
            cell.img_account.sd_setImage(with: URL(string: followUser.profile_picture ?? ""), placeholderImage: #imageLiteral(resourceName: "profile_unselected"))
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.selectedTab == 1 {
            let followUser = self.followerUsersArray[indexPath.row]
            let userProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "UserProfileVC") as! UserProfileVC
            userProfileVC.isFollowing = (followUser.is_following == "1") ? true : false
            userProfileVC.user_id = followUser.id ?? ""
            self.navigationController?.pushViewController(userProfileVC, animated: true)
        }
        
        if self.selectedTab == 2 {
            let followUser = self.followingUsersArray[indexPath.row]
            let userProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "UserProfileVC") as! UserProfileVC
            userProfileVC.isFollowing = (followUser.is_following == "1") ? true : false
            userProfileVC.user_id = followUser.id ?? ""
            self.navigationController?.pushViewController(userProfileVC, animated: true)
        }
    }
}

extension LocalVC {
    //MARK: API's...
    
    func getFollowingsAPI(){
        let url = URL(string: Constants.BASE_URL+Constants.GET_FOLLOWING_LIST)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        DialougeUtils.addActivityView(view: self.view)
        self.getFollowingsAPICall(url, header: headers)
    }
    
    func getFollowingsAPICall(_ url: URL, header: HTTPHeaders){
        LoginSignUpAPI.getAllUseDataAPI(vc: self, url: url, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            self.followingUsersArray.removeAll()
                            self.followerUsersArray.removeAll()
                            self.followingUsersArray = FollowUserModel().decodingFollowUserItems(array: result!["user_data"] as? [[String : Any]] ?? [])
                            self.lbl_emptyList.isHidden = self.followingArray.count != 0
                            DispatchQueue.main.async {
                                self.tbl_notifications.reloadData()
                            }
                        }
                    }else{
                        self.lbl_emptyList.text = "No Following Users"
                        self.lbl_emptyList.isHidden = false
                    }
                } else {
                    self.followingUsersArray.removeAll()
                    self.followerUsersArray.removeAll()
                    self.tbl_notifications.reloadData()
                    self.lbl_emptyList.text = "No Following Users"
                    self.lbl_emptyList.isHidden = false
                }
            }
        }
    }
    
    func getFollowersAPI(){
        let url = URL(string: Constants.BASE_URL+Constants.GET_FOLLOWERS_LIST)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        print(String(User.sharedInstance.auth_key ?? ""))
        DialougeUtils.addActivityView(view: self.view)
        self.getFollowerAPICall(url, header: headers)
    }
    
    func getFollowerAPICall(_ url: URL, header: HTTPHeaders){
        LoginSignUpAPI.getAllUseDataAPI(vc: self, url: url, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            self.followingUsersArray.removeAll()
                            self.followerUsersArray.removeAll()
                            
                            self.followerUsersArray = FollowUserModel().decodingFollowUserItems(array: result!["data"] as? [[String : Any]] ?? [])
                            self.lbl_emptyList.isHidden = self.followingArray.count != 0
                            DispatchQueue.main.async {
                                self.tbl_notifications.reloadData()
                            }
                        }else{
                            self.followingUsersArray.removeAll()
                            self.followerUsersArray.removeAll()
                            self.tbl_notifications.reloadData()
                            self.lbl_emptyList.text = "No Followers"
                            self.lbl_emptyList.isHidden = false
                        }
                    }
                } else {
                    self.lbl_emptyList.text = "No Followers"
                    self.lbl_emptyList.isHidden = false
                    
                }
            }
        }
    }
    
    func getNotificationAPI(){
        let url = URL(string: Constants.BASE_URL+Constants.GET_NOTIFICATIONS_LIST)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        DialougeUtils.addActivityView(view: self.view)
        self.getNotificationAPICall(url, header: headers)
    }
    
    func getNotificationAPICall(_ url: URL, header: HTTPHeaders){
        LoginSignUpAPI.getAllUseDataAPI(vc: self, url: url, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            self.notificationsArray = NotificationModel().decodingNotificationItems(array: result!["data"] as? [[String : Any]] ?? [])
                            self.tbl_notifications.reloadData()
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                }
            }
        }
    }
    
}
