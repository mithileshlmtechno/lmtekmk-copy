//
//  DiscoveryVC.swift
//  LmTek
//
//  Created by PTBLR-1128 on 12/03/21.
//  Copyright © 2021 LmTek. All rights reserved.
//

import UIKit
import SwiftyJSON
import SearchTextField
import Alamofire

@available(iOS 13.0, *)
class DiscoveryVC: BaseViewController {
    //MARK:- Outlets...
    //MARK:- Properties...
    var topDiscoverItem : TopDiscoverItem?
    var hashtagVideosArray = [VideoItem]()

    
    var yRef: CGFloat = 42.0
    var searchUsers = UISearchBar()
    var searchActive:Bool = false

    var ProductCollectionview:UICollectionView!
    var cellId = "Cell"
    var viewScroll: UIScrollView = UIScrollView()
    var arrAllSearchData: NSMutableArray = NSMutableArray()
    var arrAllHasData: NSMutableArray = NSMutableArray()

    var videosArray = [GetVideosData]()
    override func viewDidLoad() {
        super.viewDidLoad()

        //self.cellRegistration()
        //self.setSearchFieldSize()
        self.screenDesigning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getAllVideosAPI()
    }
    override func viewDidAppear(_ animated: Bool) {
        if topDiscoverItem == nil {
            //self.getDiscuverDataAPI()
        }
    }
    
    func screenDesigning() {
        
        searchUsers = UISearchBar()
        searchUsers.frame = CGRect(x: 10, y: yRef, width: screenWidth-20, height: 40)
        searchUsers.backgroundColor = .clear
        searchUsers.placeholder = " Search Users, Hastags"
        searchUsers.barTintColor = .clear
        searchUsers.searchTextField.font = fontSizeRegulor
        searchUsers.isUserInteractionEnabled = true
        searchUsers.layer.cornerRadius = 4
        searchUsers.showsCancelButton = false
        searchUsers.clipsToBounds = true
        self.view.addSubview(searchUsers)
                
        let btnSearch = UIButton()
        btnSearch.frame = CGRect(x: 10, y: yRef, width: screenWidth-100, height: 40)
        btnSearch.backgroundColor = UIColor.clear
        btnSearch.addTarget(self, action: #selector(self.btnSearchClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnSearch)

        yRef = yRef+searchUsers.frame.size.height + 15
        
        viewScroll = UIScrollView()
        viewScroll.frame = CGRect(x: 0, y: yRef, width: screenWidth, height: screenHeight-yRef)
        viewScroll.backgroundColor = UIColor.clear
        viewScroll.isPagingEnabled = true
        viewScroll.clipsToBounds = true
        self.view.addSubview(viewScroll)
                
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: (screenWidth/3), height: (screenWidth/3))

        ProductCollectionview = UICollectionView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight-(yRef+40)), collectionViewLayout: layout)
        ProductCollectionview.dataSource = self
        ProductCollectionview.delegate = self
        ProductCollectionview.register(SearchAllCollectionCell.self, forCellWithReuseIdentifier: cellId)
        ProductCollectionview.showsVerticalScrollIndicator = false
        ProductCollectionview.backgroundColor = UIColor.clear
        viewScroll.addSubview(ProductCollectionview)
    
    }
    @objc func btnCancelClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    //MARK: IBActions...
    @IBAction func textDidChange(_ textField: UITextField) {
        
    }
  @objc func btnSearchClicked(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchAllTabsVC") as! SearchAllTabsVC
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    @IBAction func viewAllClicked() {
        if let search_key = self.topDiscoverItem?.name {
            self.getAllHashtagVideos(search_key: search_key)
        }
    }
    
    //MARK:- Functions...
//    func cellRegistration() {
//        self.collection_topDiscover.register(UINib(nibName: "ProfileVideosCVCell", bundle: nil), forCellWithReuseIdentifier: "ProfileVideosCVCell")
//        self.tbl_allHashtagVideos.register(UINib(nibName: "DiscoverHashtagListTVCell", bundle: nil), forCellReuseIdentifier: "DiscoverHashtagListTVCell")
//    }
//
//    func setSearchFieldSize() {
//        // search text field setup
//        self.txt_search.delegate = self
//
//        // Modify current theme properties
//        txt_search.theme.font = UIFont(name: "OpenSans-Regular", size: 15) ?? UIFont.systemFont(ofSize: 15)
//        txt_search.theme.cellHeight = 50
//
//        txt_search.theme.bgColor = UIColor.white
//        txt_search.theme.borderColor = UIColor.lightGray.withAlphaComponent(0.5)
//        txt_search.theme.separatorColor = UIColor.lightGray.withAlphaComponent(0.5)
//        txt_search.theme.cellHeight = 50
//        txt_search.theme.placeholderColor = UIColor.lightGray
//
//        // Set specific comparision options - Default: .caseInsensitive
//        txt_search.comparisonOptions = [.caseInsensitive,.caseInsensitive]
//
//        // Set the max number of results. By default it's not limited
//        txt_search.maxNumberOfResults = 5
//
//        // You can also limit the max height of the results list
//        txt_search.maxResultsListHeight = 200
//    }

}
@available(iOS 13.0, *)
extension DiscoveryVC: UITextFieldDelegate {
    //MARK: TextField Delegates...
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        self.txt_search.resignFirstResponder()
//        return true
//    }
//
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        self.txt_search.userStoppedTypingHandler = {
//            if let criteria = self.txt_search.text {
//                if criteria.count > 0 {
//
//                    // Show loading indicator
//                    self.txt_search.showLoadingIndicator()
//
//                    self.searchDiscovery(search_key: self.txt_search.text!)
//                    self.txt_search.stopLoadingIndicator()
//                }
//            }
//            } as (() -> Void)
//
//        return true
//    }
}
@available(iOS 13.0, *)
extension DiscoveryVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    //MARK:- CollectionView Delegates...
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videosArray.count

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let objdata: GetVideosData = videosArray[indexPath.row]

        let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)  as! SearchAllCollectionCell
        
        let strurl:String = objdata.thumb_url as! String
        let url = URL(string: strurl);
        if (url != nil)
        {
            Cell.imgDraw.clipsToBounds = true
            Cell.imgDraw.sd_setImage(with: url, placeholderImage: UIImage.init(named: ""))
        }

      return Cell

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemSize = ((collectionView.frame.width/3.0))
            return CGSize(width: itemSize, height: itemSize)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        print("didSelectItemAt indexPath:",indexPath)
//        guard let videosArray = self.topDiscoverItem?.videos else { return }
//        let videoItem = videosArray[indexPath.item]
//        
//        let playVideoVC = self.storyboard?.instantiateViewController(withIdentifier: "PlayVideoVC") as! PlayVideoVC
//        playVideoVC.videoURL = URL(string: videoItem.video_url ?? "")
//        DispatchQueue.main.async {
//            self.navigationController?.pushViewController(playVideoVC, animated: true)
//        }

//        print("didSelectItemAt indexPath:",indexPath)
//        let objdata: GetVideosData = videosArray[indexPath.row]
//
//        let videoItem = String(describing: objdata.video_url ?? "")
//
//        let playVideoVC = self.storyboard?.instantiateViewController(withIdentifier: "PlayVideoVC") as! PlayVideoVC
//        playVideoVC.videoURL = URL(string: videoItem)
//        DispatchQueue.main.async {
//            self.navigationController?.pushViewController(playVideoVC, animated: true)
//        }
        
        let playVideoVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        playVideoVC.checkSearchVideo = true
        playVideoVC.videosArray = videosArray
        playVideoVC.checkVideoTag = indexPath.row
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(playVideoVC, animated: true)
        }
    }
}

@available(iOS 13.0, *)
extension DiscoveryVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.hashtagVideosArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiscoverHashtagListTVCell") as! DiscoverHashtagListTVCell
        
        let videoItem = self.hashtagVideosArray[indexPath.row]
        cell.img_discoveryThumbnail.sd_setImage(with: URL(string: videoItem.thumbURL ?? ""), completed: nil)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelectRowAt indexPath:",indexPath)
        let videoItem = self.hashtagVideosArray[indexPath.row]
        
        
        let playVideoVC = self.storyboard?.instantiateViewController(withIdentifier: "PlayVideoVC") as! PlayVideoVC
        playVideoVC.videoURL = URL(string: videoItem.videoURL ?? "")
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(playVideoVC, animated: true)
        }
    }
}
@available(iOS 13.0, *)
extension DiscoveryVC {
    //MARK:- API's...
//    func getTopDiscoveryData() {
//        TransportManager.sharedInstance.getTopDiscoveryData { (data, err) in
//            if let error = err {
//                self.view.makeToast(message: error.localizedDescription)
//            }
//            else {
//                if let json = data as? JSON, let resultObj = json.dictionaryObject {
//                    print("getTopDiscoveryData resultObj:",resultObj)
//                    let user_data = resultObj["data"] as? [String : Any] ?? [:]
//                    self.topDiscoverItem = TopDiscoverModel().decodingTopDiscoverItem(dict: user_data)
//                    self.lbl_topDiscoverHashtag.text = self.topDiscoverItem?.name ?? ""
//                    self.btn_viewAll.isHidden = (self.topDiscoverItem?.videos?.count == 0)
//                    self.collection_topDiscover.reloadData()
//                }
//            }
//        }
//    }
    func getDiscuverDataAPI() {
        let url = URL(string: Constants.BASE_URL+Constants.GET_TOP_DISCOVERY_DATA)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        DialougeUtils.addActivityView(view: self.view)
        self.getDiscuverDataAPICall(url, header: headers)

    }

    func getDiscuverDataAPICall(_ url: URL, header: HTTPHeaders){
        LoginSignUpAPI.getUserDataAPI(vc: self, url: url, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            let user_data = result!["user_data"] as? [String : Any] ?? [:]
//                            self.topDiscoverItem = TopDiscoverModel().decodingTopDiscoverItem(dict: user_data)
//                            self.lbl_topDiscoverHashtag.text = self.topDiscoverItem?.name ?? ""
//                            self.btn_viewAll.isHidden = (self.topDiscoverItem?.videos?.count == 0)
//                            self.collection_topDiscover.reloadData()
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                    self.showAlertMessage("server error")
                }
            }
        }
    }
    func searchDiscovery(search_key: String) {
        TransportManager.sharedInstance.searchDiscovery(search_key: search_key) { (data, err) in
            if let error = err {
                ToastMessage.showToast(in: self, message: error.localizedDescription, bgColor: .white)
            }
            else {
                if let json = data as? JSON, let resultObj = json.dictionaryObject {
                    print("searchDiscovery resultObj:",resultObj)
                    let searchItemsArray = TopDiscoverModel().decodingSearchDiscoveryItems(array: resultObj["data"] as? [[String : Any]] ?? [])
                    var searchResultArray = [SearchTextFieldItem]()
                    for item in searchItemsArray {
                        let resultItem = SearchTextFieldItem(title: item.name ?? "", subtitle: item.type)
                        searchResultArray.append(resultItem)
                    }
//                    self.txt_search.filterItems(searchResultArray)
//                    self.txt_search.itemSelectionHandler = { filteredResults, itemPosition in
//
//                        let selectedItem = filteredResults[itemPosition]
//                        self.getAllHashtagVideos(search_key: selectedItem.title)
//                        self.txt_search.resignFirstResponder()
//                    }
                }
            }
        }
    }
    
    func getAllHashtagVideos(search_key: String) {
        TransportManager.sharedInstance.getAllHashtagVideos(search_key: search_key) { (data, err) in
            if let error = err {
                ToastMessage.showToast(in: self, message: error.localizedDescription, bgColor: .white)
            }
            else {
                if let json = data as? JSON, let resultObj = json.dictionaryObject {
                    print("getAllHashtagVideos resultObj:",resultObj)
                    let user_data = resultObj["data"] as? [[String : Any]] ?? []
                    self.hashtagVideosArray = VideoModel().decodingVideoItems(array: user_data)
                    //self.tbl_allHashtagVideos.reloadData()
                }
            }
        }
    }
    
    
    func getAllVideosAPI() {
        let url = URL(string: Constants.BASE_URL+Constants.GET_VIDEOS_LIST_PAGINATION)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        DialougeUtils.addActivityView(view: self.view)
        self.getvideosHomePageAPI(url, header: headers)
    }
    
    func getvideosHomePageAPI(_ url: URL, header: HTTPHeaders){
        LoginSignUpAPI.getUserDataAPI(vc: self, url: url, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            
                            let userData = result!["data"] as? [[String : Any]] ?? []
                            if self.videosArray.count > 0 {
                                self.videosArray.removeAll()
                            }
                            for i in 0..<userData.count {
                                let dictdata: [String: Any] = userData[i]
                                let objdata: GetVideosData = GetVideosData.init(VideoInfo: dictdata)!
                                self.videosArray.append(objdata)
                            }
                            DispatchQueue.main.async {
                                self.ProductCollectionview.reloadData()
                            }
                        }else{
                            if self.videosArray.count > 0 {
                                self.videosArray.removeAll()
                            }
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                    self.showAlertMessage("server error")
                }
            }
        }
    }
}
