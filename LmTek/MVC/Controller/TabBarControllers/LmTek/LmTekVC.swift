//
//  LmTekVC.swift
//  LmTek
//
//  Created by PTBLR-1128 on 20/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import ARKit

class LmTekVC: UIViewController {
    //MARK: Outlets...
    
    @IBOutlet weak var collection_specializations : UICollectionView!
    @IBOutlet weak var view_search : UIView!
    @IBOutlet weak var view_badge : UIView!
    @IBOutlet weak var collection_liveUsers : UICollectionView!
    @IBOutlet weak var constraint_liveUsersCollectionHeight : NSLayoutConstraint!
    @IBOutlet weak var view_curve : UIView!
    @IBOutlet weak var tbl_users : UITableView!
    @IBOutlet weak var constraint_usersTableHeight : NSLayoutConstraint!
    
    //MARK: Variables...
    var specializationsArray = ["Celebrations", "Acting", "SpecialTalent", "Kids"]
    var selectedSpecialization = 0
    var userCellHeight : CGFloat = 0
    
    var isInitial = true

    //MARK:- View Life Cycle...
    override func viewDidLoad() {
        super.viewDidLoad() 

        self.cellRegistration()
        self.view_search.dropShadowWithRadius(25, inDirection: .Right)
        self.view_badge.dropShadowWithRadius(25, inDirection: .Left)
        
        self.constraint_liveUsersCollectionHeight.constant = ((self.view.frame.size.width - 25) / 4) + 10
        self.userCellHeight = ((self.view.frame.size.width - 20.0) / 3.0) * 1.28
        self.view_curve.dropShadowWithRadius(20, inDirection: .Bottom)
    }
        
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if self.isInitial {
            self.isInitial = false
            self.constraint_usersTableHeight.constant = .greatestFiniteMagnitude
            self.tbl_users.reloadData()
            self.tbl_users.layoutIfNeeded()
            print("self.tbl_users.contentSize.height",self.tbl_users.contentSize.height)
            self.constraint_usersTableHeight.constant = self.tbl_users.contentSize.height
        }
    }
    
    //MARK: Functions...
    func cellRegistration() {
        self.collection_specializations.register(UINib(nibName: "SpecializationsCVCell", bundle: nil), forCellWithReuseIdentifier: "SpecializationsCVCell")
        self.collection_liveUsers.register(UINib(nibName: "LiveUsersCVCell", bundle: nil), forCellWithReuseIdentifier: "LiveUsersCVCell")
        
        self.tbl_users.register(UINib(nibName: "UserHeaderTVCell", bundle: nil), forCellReuseIdentifier: "UserHeaderTVCell")
        self.tbl_users.register(UINib(nibName: "UserCollectionTVCell", bundle: nil), forCellReuseIdentifier: "UserCollectionTVCell")
    }
}

extension LmTekVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    //MARK: CollectionView Delegates...
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collection_specializations {
            return specializationsArray.count
        }
        if collectionView == self.collection_liveUsers {
            return 4
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.collection_specializations {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SpecializationsCVCell", for: indexPath) as! SpecializationsCVCell
            
            cell.lbl_title.text = self.specializationsArray[indexPath.item]
            
            var color = UIColor()
            if self.selectedSpecialization == indexPath.item {
                color = UIColor(hexString: "#E26B49") ?? UIColor.orange
            }
            else {
                color = UIColor.black
            }
            
            cell.lbl_title.textColor = color
            cell.lbl_title.borderColor = color
            
            return cell
        }
        if collectionView == self.collection_liveUsers {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LiveUsersCVCell", for: indexPath) as! LiveUsersCVCell
            cell.img_liveUser.cornerRadius = (self.constraint_liveUsersCollectionHeight.constant - 20) / 2
            
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.collection_specializations {
            let text =  self.specializationsArray[indexPath.item]
            let width = UILabel.textWidth(font: UIFont(name:"OpenSans-Regular",size:15)!, text: text)
            
            return CGSize(width: width + 30, height: 50)
        }
        
        if collectionView == self.collection_liveUsers {
            return CGSize(width: self.constraint_liveUsersCollectionHeight.constant - 10, height: self.constraint_liveUsersCollectionHeight.constant - 10)
        }
        
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == self.collection_specializations {
            return 5
        }
        if collectionView == self.collection_liveUsers {
            return 5
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == self.collection_specializations {
            return 0
        }
        if collectionView == self.collection_liveUsers {
            return 0
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == self.collection_specializations {
            return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        }
        if collectionView == self.collection_liveUsers {
            return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        }
        
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.collection_specializations {
            self.selectedSpecialization = indexPath.item
            collectionView.reloadData()
        }
    }
}

extension LmTekVC: UITableViewDelegate, UITableViewDataSource {
    //MARK:- TableView Delegates...
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserHeaderTVCell") as! UserHeaderTVCell
        
        switch section {
        case 0:
            cell.lbl_title.text = "#Celebrations"
        default:
            cell.lbl_title.text = "#Acting Challenge"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (self.userCellHeight * 2) + 15
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCollectionTVCell") as! UserCollectionTVCell
        
        return cell
    }
}

