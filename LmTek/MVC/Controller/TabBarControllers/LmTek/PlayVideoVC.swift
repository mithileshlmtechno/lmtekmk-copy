//
//  PlayVideoVC.swift
//  LmTek
//
//  Created by PTBLR-1128 on 15/03/21.
//  Copyright © 2021 LmTek. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire

class PlayVideoVC: UIViewController {
    //MARK: Outlets...
    @IBOutlet weak var view_video: UIView!
    @IBOutlet weak var btn_back: UIButton!
    
    //MARK: Properties...
    var videoURL : URL?
    var isInitial = true
    var videoPlayer: AVPlayer?
    var videosArray = [GetVideosData]()
    
    //MARK: View Life Cycle...
    override func viewDidLoad() {
        super.viewDidLoad()

        self.btn_back.setImage(#imageLiteral(resourceName: "ic_backArrow").withRenderingMode(.alwaysTemplate), for: .normal)
        self.btn_back.tintColor = UIColor.white
        self.screenDesigning()
    }
    
    func screenDesigning() {
        
        var yPosition: CGFloat = 5.0
        
        let viewRightSide: UIView = UIView()
        viewRightSide.frame = CGRect(x: screenWidth-80, y: screenHeight-410, width: 80, height: 360)
        viewRightSide.backgroundColor = UIColor.clear
        self.view.addSubview(viewRightSide)
        
        let btnLike: UIButton = UIButton()
        btnLike.frame = CGRect(x: 25, y: yPosition, width: 30, height: 30)
        btnLike.backgroundColor = UIColor.clear
        btnLike.setImage(UIImage.init(named: "ic_heart"), for: .normal)
        viewRightSide.addSubview(btnLike)
        
        yPosition = yPosition+btnLike.frame.size.height+5
        
        let lblLike: UILabel = UILabel()
        lblLike.frame = CGRect(x: 20, y: yPosition, width: 40, height: 20)
        lblLike.backgroundColor = UIColor.clear
        lblLike.text = "0"
        lblLike.textColor = .white
        lblLike.textAlignment = .center
        lblLike.font = fontSizeSemiBoldSmall
        viewRightSide.addSubview(lblLike)

        yPosition = yPosition+lblLike.frame.size.height+10

        let btnComment: UIButton = UIButton()
        btnComment.frame = CGRect(x: 25, y: yPosition, width: 30, height: 30)
        btnComment.backgroundColor = UIColor.clear
        btnComment.setImage(UIImage.init(named: "ic_comment"), for: .normal)
        viewRightSide.addSubview(btnComment)
        
        yPosition = yPosition+btnComment.frame.size.height+5
        
        let lblComment: UILabel = UILabel()
        lblComment.frame = CGRect(x: 20, y: yPosition, width: 40, height: 20)
        lblComment.backgroundColor = UIColor.clear
        lblComment.text = "0"
        lblComment.textColor = .white
        lblComment.textAlignment = .center
        lblComment.font = fontSizeSemiBoldSmall
        viewRightSide.addSubview(lblComment)

        yPosition = yPosition+lblComment.frame.size.height+10

        let btnShare: UIButton = UIButton()
        btnShare.frame = CGRect(x: 25, y: yPosition, width: 30, height: 30)
        btnShare.backgroundColor = UIColor.clear
        btnShare.setImage(UIImage.init(named: "ic_share"), for: .normal)
        viewRightSide.addSubview(btnShare)
        
        yPosition = yPosition+btnShare.frame.size.height+5
        
        let lblShare: UILabel = UILabel()
        lblShare.frame = CGRect(x: 20, y: yPosition, width: 40, height: 20)
        lblShare.backgroundColor = UIColor.clear
        lblShare.text = "0"
        lblShare.textColor = .white
        lblShare.textAlignment = .center
        lblShare.font = fontSizeSemiBoldSmall
        viewRightSide.addSubview(lblShare)

        yPosition = yPosition+lblShare.frame.size.height+10

        let btnDownload: UIButton = UIButton()
        btnDownload.frame = CGRect(x: 25, y: yPosition, width: 30, height: 30)
        btnDownload.backgroundColor = UIColor.clear
        btnDownload.setImage(UIImage.init(named: "ic_download"), for: .normal)
        btnDownload.layer.cornerRadius = 15
        btnDownload.clipsToBounds = true
        viewRightSide.addSubview(btnDownload)
        
        yPosition = yPosition+btnDownload.frame.size.height+5
        
        let lblDownload: UILabel = UILabel()
        lblDownload.frame = CGRect(x: 20, y: yPosition, width: 40, height: 20)
        lblDownload.backgroundColor = UIColor.clear
        lblDownload.text = "0"
        lblDownload.textColor = UIColor.white
        lblDownload.textAlignment = .center
        lblDownload.font = fontSizeSemiBoldSmall
        viewRightSide.addSubview(lblDownload)

        yPosition = yPosition+lblDownload.frame.size.height+25

        let layer1: CAShapeLayer = CAShapeLayer()
        
        let btnDuetVideo: UIButton = UIButton()
        btnDuetVideo.frame = CGRect(x: 10, y: yPosition, width: 70, height: 50)
        btnDuetVideo.backgroundColor = colorYellow
        btnDuetVideo.ShaplayerLeft(layer1, corner: 25)
        btnDuetVideo.clipsToBounds = true
        viewRightSide.addSubview(btnDuetVideo)

        let imgDuet: UIImageView = UIImageView()
        imgDuet.frame = CGRect(x: 20, y: 5, width: 20, height: 20)
        imgDuet.backgroundColor = UIColor.clear
        imgDuet.image = UIImage.init(named: "ic_duet")
        btnDuetVideo.addSubview(imgDuet)

        let lblDuet: UILabel = UILabel()
        lblDuet.frame = CGRect(x: 10, y: 25, width: 40, height: 20)
        lblDuet.textColor = .lightGray
        lblDuet.text = "Duet"
        lblDuet.font = fontSizeSemiBoldSmall
        lblDuet.textAlignment = .center
        btnDuetVideo.addSubview(lblDuet)
        
        let userImage: UIImageView = UIImageView()
        userImage.frame = CGRect(x: 20, y: screenHeight-150, width: 40, height: 40)
        userImage.backgroundColor = .white
        userImage.layer.cornerRadius = 20
        userImage.clipsToBounds = true
        self.view.addSubview(userImage)
        
        let lblUserName: UILabel = UILabel()
        lblUserName.frame = CGRect(x: 20, y: screenHeight-105, width: 120, height: 20)
        lblUserName.textColor = .white
        lblUserName.text = "Joishnu skjcbskjk"
        lblUserName.font = fontSizeSemiBoldSmall
        lblUserName.textAlignment = .left
        self.view.addSubview(lblUserName)
        
        let btnFollow: UIButton = UIButton()
        btnFollow.frame = CGRect(x: 145, y: screenHeight-108, width: 70, height: 30)
        btnFollow.backgroundColor = .lightGray.withAlphaComponent(0.5)
        btnFollow.layer.cornerRadius = 4
        btnFollow.setTitle("Follow", for: .normal)
        btnFollow.setTitleColor(.white, for: .normal)
        btnFollow.titleLabel?.font = fontSizeSemiBoldSmall
        btnFollow.clipsToBounds = true
        self.view.addSubview(btnFollow)

        let lblUserComments: UILabel = UILabel()
        lblUserComments.frame = CGRect(x: 20, y: screenHeight-75, width: 250, height: 40)
        lblUserComments.textColor = .white
        lblUserComments.text = "jhjahjd# hjbscs j j adaddkjnkw jk vhgvvj ygygugu gi"
        lblUserComments.font = fontSizeSemiBoldSmall
        lblUserComments.textAlignment = .left
        lblUserComments.lineBreakMode = .byWordWrapping
        lblUserComments.numberOfLines = 2
        self.view.addSubview(lblUserComments)

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if self.isInitial {
            self.isInitial = false
            if let url = videoURL {
                self.playVideo(url: url)
            }
        }
    }
    

    //MARK: IBActions...
    @IBAction func backClicked() {
        self.videoPlayer?.pause()
        self.navigationController?.popViewController(animated: true)
    }
    func shareVideo() {
        if UserDefaults.standard.bool(forKey: Constants.KEYS.isLoggedIn) {
            var video_model : GetVideosData
            video_model = self.videosArray[0]
            
            print("shareClicked:",video_model)
            if let url = URL(string: String(describing: video_model.video_url ?? "")) {
                let linkToShare = [url]
                let activityViewController = UIActivityViewController(activityItems: linkToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view // so that ipad won't crash
                
                activityViewController.completionWithItemsHandler = {(activityType: UIActivity.ActivityType?, completed: Bool, returnedItems:[Any]?, error: Error?) in
                    
                    print("activityType:",activityType, ", completed:",completed)
                    if let type = activityType {
                        //self.doShareVideo(video_model: video_model, at: indexPath.item)
                        self.doShareVideoAPI(video_model)
                    }
                }
                DispatchQueue.main.async {
                    self.present(activityViewController, animated: true, completion: nil)
                }
            }
        }
        else {
            self.showLoginActivity()
        }
    }
    
func showLoginActivity() {
    let alertController = UIAlertController(title: nil, message: "You need to login first to access this section", preferredStyle: .actionSheet)
    alertController.addAction(UIAlertAction(title: "Login", style: .default, handler: { (action) in
        if #available(iOS 13.0, *) {
            if let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate {
                sceneDelegate.showLoginScreen()
            }
        } else {
            if let appdelegate =  UIApplication.shared.delegate as? AppDelegate {
                appdelegate.showLoginScreen()
            }
        }
    }))
    alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
    DispatchQueue.main.async {
        self.present(alertController, animated: true, completion: nil)
    }
}
    //MARK: Functions...
    func playVideo(url: URL) {
        print("VideoPlayerVC playVideo:",url)
        let asset = AVAsset(url: url)
        let item = AVPlayerItem(asset: asset)
        
        self.videoPlayer = AVPlayer(playerItem: item)
        
        let videoLayer = AVPlayerLayer(player: self.videoPlayer)
        
        DispatchQueue.main.async {
            videoLayer.frame = self.view_video.bounds
            videoLayer.videoGravity = .resizeAspectFill
            self.view_video.layer.insertSublayer(videoLayer, at: 0)
            self.videoPlayer?.actionAtItemEnd = .none
            self.videoPlayer?.play()
            NotificationCenter.default.removeObserver(self)
            NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(notification:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: item)
        }
    }
    
    @objc func playerDidFinishPlaying(notification: Notification) {
        if let playerItem = notification.object as? AVPlayerItem {
            playerItem.seek(to: CMTime.zero, completionHandler: nil)
        }
    }
    //MARK: APIS
    
    func doShareVideoAPI(_ video_model: GetVideosData) {
        let video_id: String = video_model.video_id as! String
        
        let url = URL(string: Constants.BASE_URL+Constants.DO_SHARE)!
        let parameters: Parameters = ["video_id": video_id]
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        print(parameters)
        DialougeUtils.addActivityView(view: self.view)
        self.doShareVideoAPICall(parameters, url: url, header: headers)

    }
    
    func doShareVideoAPICall(_ param: Parameters, url: URL, header: HTTPHeaders){
        AllUserApis.allPostDataWithoutImageAPI(vc: self, url: url, parameter: param, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status: Bool = result!["status"] as! Bool
//                        if status == true{
//                            let data = result!["data"] as? [String : Any] ?? [:]
//                            if self.selectedList == 0 {
//                                //self.videosArray[index].shares = data["share"] as! String//String
//                            }
//                            else {
//                                //self.followingVideosArray[index].shares =  data["share"] as! String //data["share"] as? Int//String
//                            }
//                            let homeCell = self.collection_home.cellForItem(at: IndexPath(item: index, section: 0)) as? HomeCVCell
//                            if self.selectedList == 0 {
//                                homeCell?.loadVideoDetails(model: self.videosArray[index])
//                            }
//                            else {
//                                homeCell?.loadVideoDetails(model: self.followingVideosArray[index])
//                            }
//                        }else{
//                            let message:String = result!["message"] as! String
//                            self.showAlertMessageDispatchQueue(message)
//                        }
                    }
                } else {
                    //self.showAlertMessage("server error")
                }
            }
        }
    }

}
