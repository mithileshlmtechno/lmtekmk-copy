//
//  VideoRecordExampleVC.swift
//  LmTek
//
//  Created by PTBLR-1128 on 02/12/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import AVFoundation
import AssetsLibrary
import CoreMedia
import Photos

class VideoRecordExampleVC: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {

    var captureSession: AVCaptureSession!

    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var recordButtton: UIButton!
    @IBOutlet weak var imageView: UIImageView!

    var assetWriter: AVAssetWriter?
    var assetWriterPixelBufferInput: AVAssetWriterInputPixelBufferAdaptor?
    var isWriting = false
    var currentSampleTime: CMTime?
    var currentVideoDimensions: CMVideoDimensions?
    var filter: CIFilter = CIFilter(name: "CIColorMonochrome")!

    override func viewDidLoad() {
        super.viewDidLoad()
        //FilterVendor.register()
        setupCaptureSession()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func setupCaptureSession() {
        let captureSession = AVCaptureSession()
        captureSession.sessionPreset = AVCaptureSession.Preset.photo

        /*guard let captureDevice = AVCaptureDevice.default(for: .video)/*defaultDevice(withMediaType: AVMediaTypeVideo)*/, let input = try? AVCaptureDeviceInput(device: captureDevice) else {
            print("Can't access the camera")
            return
        }*/
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
        let devices = deviceDiscoverySession.devices
        
        var device : AVCaptureDevice?
        
        for devi in devices {
            if devi.position == .front {
                device = devi
                break
            }
        }
        guard let captureDevice = device, let input = try? AVCaptureDeviceInput(device: captureDevice) else {
            print("Can't access the camera")
            return
        }
        

        if captureSession.canAddInput(input) {
            captureSession.addInput(input)
        }
        
        guard let audioDevice = AVCaptureDevice.default(for: .audio), let audioInput = try? AVCaptureDeviceInput(device: audioDevice) else {
            print("can't access the microphone")
            return
        }
        
        if captureSession.canAddInput(audioInput) {
            captureSession.addInput(audioInput)
        }

        let videoOutput = AVCaptureVideoDataOutput()

        videoOutput.setSampleBufferDelegate(self, queue: DispatchQueue.main)
        if captureSession.canAddOutput(videoOutput) {
            captureSession.addOutput(videoOutput)
        }

        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.videoGravity = .resizeAspectFill
        //if((previewLayer) != nil) {
            //view.layer.addSublayer(previewLayer)
        //}
        previewView.layer.addSublayer(previewLayer)
        
        guard let currentCameraInput = captureSession.inputs.first as? AVCaptureDeviceInput else {
            return
        }
        if let conn = videoOutput.connection(with: .video) {
            conn.isVideoMirrored = true
        }
        
        captureSession.startRunning()
    }

    @IBAction func record(_ sender: Any) {
        /*if isWriting {
            print("stop record")
            self.isWriting = false
            assetWriterPixelBufferInput = nil
            assetWriter?.finishWriting(completionHandler: {[unowned self] () -> Void in
                //self.saveMovieToCameraRoll()
                /*let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                videoPlayerVC.videoURL = self.assetWriter?.outputURL
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(videoPlayerVC, animated: true)
                }*/
                guard let url = self.assetWriter?.outputURL else { print("output url is nil"); return }
                let asset = AVAsset(url: url)
                let filter = CIFilter(name: "CIColorMonochrome")!
                let composition = AVVideoComposition(asset: asset, applyingCIFiltersWithHandler: { request in

                    // Clamp to avoid blurring transparent pixels at the image edges
                    let source = request.sourceImage.clampedToExtent()
                    filter.setValue(source, forKey: kCIInputImageKey)

                    // Vary filter parameters based on video timing
                    let seconds = CMTimeGetSeconds(request.compositionTime)
                    filter.setValue(seconds * 10.0, forKey: kCIInputRadiusKey)

                    // Crop the blurred output to the bounds of the original image
                    let output = filter.outputImage!.cropped(to: request.sourceImage.extent)

                    // Provide the filter output to the composition
                    request.finish(with: output, context: nil)
                })
                
                let export = AVAssetExportSession(asset: asset, presetName: AVAssetExportPreset1920x1080)
                export?.outputFileType = AVFileType.mov
                export?.outputURL = url
                export?.videoComposition = composition
                
                DispatchQueue.main.async {
                    export?.exportAsynchronously(completionHandler: {
                        let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                        videoPlayerVC.videoURL = url//self.assetWriter?.outputURL
                        DispatchQueue.main.async {
                            self.navigationController?.pushViewController(videoPlayerVC, animated: true)
                        }
                    })
                }
            })
        }*/
        if isWriting {
            print("stop record")
            self.isWriting = false
            assetWriterPixelBufferInput = nil
            assetWriter?.finishWriting(completionHandler: {[unowned self] () -> Void in
                //self.saveMovieToCameraRoll()
                /*let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                 videoPlayerVC.videoURL = self.assetWriter?.outputURL
                 DispatchQueue.main.async {
                 self.navigationController?.pushViewController(videoPlayerVC, animated: true)
                 }*/

                guard let url = self.assetWriter?.outputURL else { print("output url is nil"); return }
                let asset = AVAsset(url: url)
                let errors = Error.self
                
                let item = AVPlayerItem(asset: asset)
                //item.videoComposition = AVVideoComposition(asset: asset,  applyingCIFiltersWithHandler: { [weak self] request in
                let videoComposition = AVVideoComposition(asset: asset,  applyingCIFiltersWithHandler: { [weak self] request in
                    guard let self = self else {
                        //request.finish(with: errors.self)
                        return
                    }
                    let source = request.sourceImage.clampedToExtent()
                    self.filter.setValue(source, forKey: kCIInputImageKey)
                    
                    let output = self.filter.outputImage?.cropped(to: request.sourceImage.extent)
                    
                    request.finish(with: output!, context: nil)
                })
                
                item.videoComposition = videoComposition
                if let audioTrack = asset.tracks(withMediaType: .audio).first {
                    
                }
                
                // self.player.replaceCurrentItem(with: item)
                
                let exporter = AVAssetExportSession(asset: item.asset, presetName: AVAssetExportPresetHighestQuality)
                exporter?.videoComposition = videoComposition
                //exporter?.timeRange = CMTimeRange(start: CMTime(seconds: 1, preferredTimescale: 1), duration: asset.duration)
                
                exporter?.outputFileType = .mp4
                let filename = "filename.mp4"
                
                let documentsDirectory = FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).last!
                let outputURL = documentsDirectory.appendingPathComponent(filename)
                try? FileManager.default.removeItem(at: outputURL)
                exporter?.outputURL = outputURL
                exporter?.exportAsynchronously(completionHandler: {
                    guard exporter?.status == .completed else {
                        print("export failed: \(exporter?.error)")
                        return
                    }
                    print("done: ",outputURL)
                    
                    DispatchQueue.main.async {
                        let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                        videoPlayerVC.videoURL = outputURL//self.assetWriter?.outputURL
                        
                        self.navigationController?.pushViewController(videoPlayerVC, animated: true)
                    }
                })
                
            })
        }
        else {
            print("start record")
            DispatchQueue.main.async {
                self.createWriter()
                self.assetWriter?.startWriting()
                self.assetWriter?.startSession(atSourceTime: self.currentSampleTime!)
                self.isWriting = true
            }
        }
        
    }

    func saveMovieToCameraRoll() {
        PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: self.movieURL() as URL)
        }) { saved, error in
            if saved {
                print("saved")
            }
        }
    }

    func movieURL() -> NSURL {
        let tempDir = NSTemporaryDirectory()
        let url = NSURL(fileURLWithPath: tempDir).appendingPathComponent("tmpMov.mov")
        return url! as NSURL
    }

    func checkForAndDeleteFile() {
        let fm = FileManager.default
        let url = movieURL()
        let exist = fm.fileExists(atPath: url.path!)

        if exist {
            do {
                try fm.removeItem(at: url as URL)
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
    }

    func createWriter() {
        self.checkForAndDeleteFile()

        do {
            assetWriter = try AVAssetWriter(outputURL: movieURL() as URL, fileType: AVFileType.mov)
        } catch let error as NSError {
            print(error.localizedDescription)
            return
        }

        let outputSettings = [
            AVVideoCodecKey : AVVideoCodecH264,
            AVVideoWidthKey : Int(currentVideoDimensions!.width),
            AVVideoHeightKey : Int(currentVideoDimensions!.height)
        ] as [String : Any]

        let assetWriterVideoInput = AVAssetWriterInput(mediaType: AVMediaType.video, outputSettings: outputSettings as? [String : AnyObject])
        assetWriterVideoInput.expectsMediaDataInRealTime = true
        assetWriterVideoInput.transform = CGAffineTransform(rotationAngle: CGFloat(0.0))//M_PI / 2.0))

        let sourcePixelBufferAttributesDictionary = [
            String(kCVPixelBufferPixelFormatTypeKey) : Int(kCVPixelFormatType_32BGRA),
            String(kCVPixelBufferWidthKey) : Int(currentVideoDimensions!.width),
            String(kCVPixelBufferHeightKey) : Int(currentVideoDimensions!.height),
            String(kCVPixelFormatOpenGLESCompatibility) : kCFBooleanTrue
        ] as [String : Any]

        assetWriterPixelBufferInput = AVAssetWriterInputPixelBufferAdaptor(assetWriterInput: assetWriterVideoInput,
                                                                           sourcePixelBufferAttributes: sourcePixelBufferAttributesDictionary)

        if assetWriter!.canAdd(assetWriterVideoInput) {
            assetWriter!.add(assetWriterVideoInput)
        } else {
            print("no way\(assetWriterVideoInput)")
        }
        
        let audioOutputSetings = [AVFormatIDKey : kAudioFormatLinearPCM, AVSampleRateKey : 48000, AVLinearPCMBitDepthKey : 16, AVLinearPCMIsNonInterleaved : false, AVLinearPCMIsFloatKey : false, AVLinearPCMIsBigEndianKey : false, AVNumberOfChannelsKey : 1] as? [String : Any]
        let assetWriterAudioInput = AVAssetWriterInput(mediaType: .audio, outputSettings: audioOutputSetings)
        assetWriterAudioInput.expectsMediaDataInRealTime = true
        if assetWriter?.canAdd(assetWriterAudioInput) == true {
            assetWriter?.add(assetWriterAudioInput)
        }
        else {
            print("unable to add audio input")
        }
    }
    
    

    /*func captureOutput(_ captureOutput: AVCaptureOutput, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, from connection: AVCaptureConnection) {
        print("didOutputSampleBuffer")
        autoreleasepool {

            connection.videoOrientation = AVCaptureVideoOrientation.landscapeLeft;

            guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }
            let cameraImage = CIImage(cvPixelBuffer: pixelBuffer)

            let filter = CIFilter(name: "Fİlter")!
            filter.setValue(cameraImage, forKey: kCIInputImageKey)


            let formatDescription = CMSampleBufferGetFormatDescription(sampleBuffer)!
            self.currentVideoDimensions = CMVideoFormatDescriptionGetDimensions(formatDescription)
            self.currentSampleTime = CMSampleBufferGetOutputPresentationTimeStamp(sampleBuffer)

            if self.isWriting {
                if self.assetWriterPixelBufferInput?.assetWriterInput.isReadyForMoreMediaData == true {
                    var newPixelBuffer: CVPixelBuffer? = nil

                    CVPixelBufferPoolCreatePixelBuffer(nil, self.assetWriterPixelBufferInput!.pixelBufferPool!, &newPixelBuffer)

                    let success = self.assetWriterPixelBufferInput?.append(newPixelBuffer!, withPresentationTime: self.currentSampleTime!)

                    if success == false {
                        print("Pixel Buffer failed")
                    }
                }
            }

            DispatchQueue.main.async {

                if let outputValue = filter.value(forKey: kCIOutputImageKey) as? CIImage {
                    let filteredImage = UIImage(ciImage: outputValue)
                    self.imageView.image = filteredImage
                }
            }
        }
    }*/

    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        autoreleasepool {

            connection.videoOrientation = .portrait//AVCaptureVideoOrientation.landscapeLeft;

            // COMMENT: This line makes sense - this is your pixelbuffer from the camera.
            guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }

            // COMMENT: OK, so you turn pixelBuffer into a CIImage...
            let cameraImage = CIImage(cvPixelBuffer: pixelBuffer)
            // COMMENT: And now you've create a CIImage with a Filter instruction...
            let filter = CIFilter(name: "CIColorMonochrome")!
            filter.setValue(cameraImage, forKey: kCIInputImageKey)


            let formatDescription = CMSampleBufferGetFormatDescription(sampleBuffer)!
            self.currentVideoDimensions = CMVideoFormatDescriptionGetDimensions(formatDescription)
            self.currentSampleTime = CMSampleBufferGetOutputPresentationTimeStamp(sampleBuffer)

            if self.isWriting {
                if self.assetWriterPixelBufferInput?.assetWriterInput.isReadyForMoreMediaData == true {
                    // COMMENT: Here's where it gets weird. You've declared a new, empty pixelBuffer... but you already have one (pixelBuffer) that contains the image you want to write...
                    var newPixelBuffer: CVPixelBuffer? = nil

                    // COMMENT: And you grabbed memory from the pool.
                    CVPixelBufferPoolCreatePixelBuffer(nil, self.assetWriterPixelBufferInput!.pixelBufferPool!, &newPixelBuffer)

                    // COMMENT: And now you wrote an empty pixelBuffer back <-- this is what's causing the black frame.
                    /*guard let buffer = newPixelBuffer else {
                        print("newPixelBuffer error")
                        return }*/
                    let context = CIContext()
                    context.render(cameraImage, to: pixelBuffer)
                    let success = self.assetWriterPixelBufferInput?.append(/*newPixelBuffer!*/pixelBuffer, withPresentationTime: self.currentSampleTime!)

                    if success == false {
                        print("Pixel Buffer failed")
                    }
                }
            }

            // COMMENT: And now you're sending the filtered image back to the screen.
            DispatchQueue.main.async {

                if let outputValue = filter.value(forKey: kCIOutputImageKey) as? CIImage {
                    let filteredImage = UIImage(ciImage: outputValue)
                    self.imageView.image = filteredImage
                }
            }
        }
    }
    
    func pixelBuffer(from image: CGImage?, andSize size: CGSize) -> CVPixelBuffer? {
        let options = [kCVPixelBufferCGImageCompatibilityKey : NSNumber(value: true), kCVPixelBufferCGBitmapContextCompatibilityKey : NSNumber(value: true)]
        
        var pxBuffer : CVPixelBuffer?
        
        let status = CVPixelBufferCreate(kCFAllocatorDefault, Int(size.width), Int(size.height), kCVPixelFormatType_32ARGB, options as? CFDictionary, &pxBuffer)
        assert(status == kCVReturnSuccess && pxBuffer != nil, "Invalid parameter not satisfying: status == kCVReturnSuccess && pxbuffer != nil")
        
        guard let buffer = pxBuffer else {
            print("buffer error")
            return pxBuffer
        }
        
        CVPixelBufferLockBaseAddress(buffer, [])
        var pxData = CVPixelBufferGetBaseAddress(buffer)
        assert(pxData != nil, "Invalid parameter not satisfying: pxdata != nil")
        
        let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
        let context = CGContext(data: &pxData, width: Int(size.width), height: Int(size.width), bitsPerComponent: 8, bytesPerRow: 4 * (Int(size.width)), space: rgbColorSpace, bitmapInfo: CGImageAlphaInfo.noneSkipFirst.rawValue)
        assert(context != nil, "Invalid parameter not satisfying: context != nil")
        
        context?.concatenate(CGAffineTransform(rotationAngle: 0))
        context?.draw(image!, in: CGRect(x: 0, y: 0, width: image!.width, height: image!.height))
        
        CVPixelBufferUnlockBaseAddress(buffer, [])
        
        return pxBuffer
    }
}
