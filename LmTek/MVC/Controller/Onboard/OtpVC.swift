//
//  OtpVC.swift
//  LmTek
//
//  Created by Sai Sankar on 12/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import SwiftyJSON

class OtpVC: UIViewController {
    //MARK:- Outlets...
    @IBOutlet weak var txt_otp1 : OtpTextField!
    @IBOutlet weak var txt_otp2 : OtpTextField!
    @IBOutlet weak var txt_otp3 : OtpTextField!
    @IBOutlet weak var txt_otp4 : OtpTextField!
    
    @IBOutlet weak var view_otp1 : UIView!
    @IBOutlet weak var view_otp2 : UIView!
    @IBOutlet weak var view_otp3 : UIView!
    @IBOutlet weak var view_otp4 : UIView!
    
    @IBOutlet weak var lbl_timer : UILabel!
    @IBOutlet weak var lbl_mobileNumber : UILabel!
    @IBOutlet weak var view_progress : UIView!
    
    //MARK:- Variables...
    var borderBlueColor = UIColor(red: 33.0/255.0, green: 24.0/255.0, blue: 197.0/255.0, alpha: 1)
    var timer: Timer?
    var totalTime = 30.0
    var mobileNumber = ""
    
    var timeLayer = CAShapeLayer()
    var circularPath : UIBezierPath!
    
    var loginDataDict = [String : Any]()
    var loginDict = [String : String]()
        
    //MARK:- View Life Cycle...
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lbl_mobileNumber.text = "Enter the 4 - digit code, we sent to \(self.mobileNumber)"
        self.otpFieldsSetup()
        self.setUpLayer()
        self.startOtpTimer()
        if let otp = self.loginDataDict["otp"] as? Int {
            print("otp:",otp)
            ToastMessage.showToast(in: self, message: "Please verify OTP for login : \(otp)", bgColor: .white)
        }
        //self.animateLayer()
    }
    

    //MARK:- IBActions...
    @IBAction func backClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendItAgainClicked() {
        self.login()
        /*self.timeLayer.removeAllAnimations()
        if let timer = self.timer {
            timer.invalidate()
        }
        //self.lbl_timer.isHidden = false
        self.view_progress.isHidden = false
        self.lbl_timer.text = "00:30"
        self.timeLayer.strokeEnd = 0
        self.startOtpTimer()*/
        //self.animateLayer()
    }
    
    //MARK:- Functions...
    func otpFieldsSetup() {
        self.txt_otp1.delegate = self
        self.txt_otp2.delegate = self
        self.txt_otp3.delegate = self
        self.txt_otp4.delegate = self
        
        self.txt_otp1.nextTextField = self.txt_otp2
        
        self.txt_otp2.previousTextField = self.txt_otp1
        self.txt_otp2.nextTextField = self.txt_otp3
        
        self.txt_otp3.previousTextField = self.txt_otp2
        self.txt_otp3.nextTextField = self.txt_otp4
        
        self.txt_otp4.previousTextField = self.txt_otp3
        
        self.txt_otp1.addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)
        self.txt_otp2.addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)
        self.txt_otp3.addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)
        self.txt_otp4.addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)
    }
    
    private func startOtpTimer() {
        self.totalTime = 30
        self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        self.lbl_timer.text = self.timeFormatted(Int(ceil(self.totalTime))) // will show timer
        DispatchQueue.main.async {
            self.timeLayer.strokeEnd = CGFloat((30.0 - Double(self.totalTime)) / 30.0)
        }
        if totalTime >= 0 {
            totalTime -= 0.1  // decrease counter timer
        } else {
            if let timer = self.timer {
                //self.lbl_timer.isHidden = true
                self.view_progress.isHidden = true
                timer.invalidate()
                self.timer = nil
                self.lbl_timer.text = "00:30"
            }
        }
    }
    
    func setUpLayer() {
        self.circularPath = UIBezierPath(arcCenter: .zero, radius: 50, startAngle: 0, endAngle: .pi * 2, clockwise: true)
        
        let grayLayer = CAShapeLayer()
        grayLayer.path = circularPath.cgPath
        grayLayer.strokeColor = self.borderBlueColor.cgColor
        grayLayer.lineWidth = 2
        grayLayer.fillColor = UIColor.clear.cgColor
        grayLayer.position = CGPoint(x: 50, y: 50)
        self.view_progress.layer.addSublayer(grayLayer)
        
        self.timeLayer.path = circularPath.cgPath
        
        self.timeLayer.strokeColor = UIColor.gray.cgColor
        self.timeLayer.fillColor = UIColor.clear.cgColor
        self.timeLayer.lineWidth = 2
        
        self.timeLayer.lineCap = .round
        
        self.timeLayer.position = CGPoint(x: 50, y: 50)
        
        self.timeLayer.transform = CATransform3DMakeRotation(-.pi / 2.0, 0, 0, 1)
        
        self.timeLayer.strokeEnd = 0
        
        self.view_progress.layer.addSublayer(self.timeLayer)
    }
    
    func animateLayer() {
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        
        basicAnimation.toValue = 1
        
        basicAnimation.duration = 30
        
        basicAnimation.fillMode = .forwards
        
        self.timeLayer.add(basicAnimation, forKey: "circularProgress")
    }
    
    func changeRootController() {
        //let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        //let navController =  storyBoard.instantiateViewController(withIdentifier: "HomeNavigation")
        if #available(iOS 13.0, *) {
            if let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate {
                //sceneDelegate.window?.rootViewController = navController
                //sceneDelegate.window?.makeKeyAndVisible()
                sceneDelegate.showHomeScreen()
            }
        } else {
            if let appDelegate =  UIApplication.shared.delegate as? AppDelegate {
                //appdelegate.window?.rootViewController = navController
                //appdelegate.window?.makeKeyAndVisible()
                appDelegate.showHomeScreen()
            }
        }
    }
    
    func resendOtpTimer() {
        self.timeLayer.removeAllAnimations()
        if let timer = self.timer {
            timer.invalidate()
        }
        self.view_progress.isHidden = false
        self.lbl_timer.text = "00:30"
        self.timeLayer.strokeEnd = 0
        self.startOtpTimer()
    }

}

extension OtpVC : UITextFieldDelegate {
    //MARK:- TextField Delegates
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.text = ""
        if textField == self.txt_otp1 {
            self.view_otp1.borderColor = self.borderBlueColor
        }
        else if textField == self.txt_otp2 {
            self.view_otp2.borderColor = self.borderBlueColor
        }
        else if textField == self.txt_otp3 {
            self.view_otp3.borderColor = self.borderBlueColor
        }
        else if textField == self.txt_otp4 {
            self.view_otp4.borderColor = self.borderBlueColor
        }
        return true
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        let text = textField.text
        if  text?.count == 1 {
            switch textField{
            case txt_otp1:
                txt_otp2.keyboardType = txt_otp1.keyboardType
                txt_otp2.becomeFirstResponder()
            case txt_otp2:
                txt_otp3.keyboardType = txt_otp2.keyboardType
                txt_otp3.becomeFirstResponder()
            case txt_otp3:
                txt_otp4.keyboardType = txt_otp3.keyboardType
                txt_otp4.becomeFirstResponder()
            case txt_otp4:
                txt_otp4.resignFirstResponder()
                var txt_otp = self.txt_otp1
                var otp = ""
                while let txt = txt_otp {
                    if txt.text != "" {
                        otp += txt.text!
                        txt_otp = txt_otp?.nextTextField
                    }
                    else {
                        return
                    }
                }
                self.validateOtp(otp)
            default:
                break
            }
        }
        if  text?.count == 0 {
            switch textField{
            case txt_otp1:
                txt_otp1.becomeFirstResponder()
            case txt_otp2:
                txt_otp1.becomeFirstResponder()
                view_otp2.borderColor = UIColor.lightGray
            case txt_otp3:
                txt_otp2.becomeFirstResponder()
                view_otp3.borderColor = UIColor.lightGray
            case txt_otp4:
                txt_otp3.becomeFirstResponder()
                view_otp4.borderColor = UIColor.lightGray
            default:
                break
            }
        }
        else{
            
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let char = string.cString(using: String.Encoding.utf8) {
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                if textField.text?.count == 0 {
                    switch textField{
                    case txt_otp1:
                        txt_otp1.becomeFirstResponder()
                    case txt_otp2:
                        txt_otp1.becomeFirstResponder()
                        view_otp2.borderColor = UIColor.lightGray
                    case txt_otp3:
                        txt_otp2.becomeFirstResponder()
                        view_otp3.borderColor = UIColor.lightGray
                    case txt_otp4:
                        txt_otp3.becomeFirstResponder()
                        view_otp4.borderColor = UIColor.lightGray
                    default:
                        break
                    }
                }
            }
            else {
                let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                let numberOfCharacters = newText.count
                return numberOfCharacters <= 1
            }
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text?.trimmingCharacters(in: .whitespaces).isEmpty == true {
            if textField == self.txt_otp1 {
                self.view_otp1.borderColor = UIColor.lightGray
            }
            else if textField == self.txt_otp2 {
                self.view_otp2.borderColor = UIColor.lightGray
            }
            else if textField == self.txt_otp3 {
                self.view_otp3.borderColor = UIColor.lightGray
            }
            else if textField == self.txt_otp4 {
                self.view_otp4.borderColor = UIColor.lightGray
            }
        }
    }
}

extension OtpVC {
    //MARK:- API's...
    func validateOtp(_ otpString: String) {
        let user_id = self.loginDataDict["user_id"] as? String ?? ""
        let otp = Int(otpString) ?? 0
        TransportManager.sharedInstance.verifyOtp(otp: otp, user_id: user_id) { (data, err) in
            if let error = err {
                ToastMessage.showToast(in: self, message: error.localizedDescription, bgColor: .white)
            }
            else {
                if let json = data as? JSON, let resultObj = json.dictionaryObject {
                    print("validateOtp resultObj:",resultObj)
                    let user_data = resultObj["data"] as? [String : Any] ?? [:]
                    UserDefaults.standard.set(user_data, forKey: Constants.KEYS.LoginResponse)
                    UserDefaults.standard.set(true, forKey: Constants.KEYS.isLoggedIn)
                     UserDefaults.standard.synchronize()
                    
                     if (UserDefaults.standard.value(forKey: Constants.KEYS.LoginResponse) != nil)
                     {
                         let dict = UserDefaults.standard.value(forKey: Constants.KEYS.LoginResponse) as! [String : AnyObject]
                         print(dict)
                         APIManager.shared.headers["token"] = (dict["token"] as! String)
                         
                         print("headers\(APIManager.shared.headers)")
                         
                         /*if let app = UIApplication.shared.delegate as? AppDelegate {
                             app.showHomeDashBoard()
                         }*/
                       // self.changeRootController()
                        let chooseLanguageVC = self.storyboard?.instantiateViewController(withIdentifier: "ChooseLanguageVC") as! ChooseLanguageVC
                        chooseLanguageVC.isAfterLogin = true
                        DispatchQueue.main.async {
                            self.navigationController?.pushViewController(chooseLanguageVC, animated: true)
                        }
                     }
                }
            }
        }
    }
    
    func login() {
        TransportManager.sharedInstance.login(loginDict: loginDict) { (data, err) in
            if let error = err {
                print("login error:",error.localizedDescription)
            }
            else {
                if let json = data as? JSON, let resultObj = json.dictionaryObject {
                    print("login resultObj:",resultObj)
                    if resultObj["status"] as? Bool == true {
                        let loginDataDict = resultObj["data"] as? [String : Any] ?? [:]
                        //self.startOtpTimer()
                        if let otp = loginDataDict["otp"] as? Int {
                            print("otp:",otp)
                            ToastMessage.showToast(in: self, message: "Please verify OTP for login : \(otp)", bgColor: .white)
                        }
                        self.resendOtpTimer()
                    }
                }
            }
        }
    }
}
