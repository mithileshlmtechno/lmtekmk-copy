//
//  LoginVC.swift
//  LmTek
//
//  Created by Sai Sankar on 12/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation
import FBSDKCoreKit
import FBSDKLoginKit
//import GoogleSignIn

class LoginVC: UIViewController {
    //MARK:- Outlets...
    @IBOutlet weak var img_background : UIImageView!
    
    //MARK:- Variables...
    var locationManager : CLLocationManager?
    //var location : CLLocation?

    //MARK:- View Life Cycle...
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*let path = Bundle.main.path(forResource: "splash", ofType: "gif") ?? ""
        if let data = NSData(contentsOfFile: path) {
            let gifImage = UIImage.sd_image(withGIFData: data as Data)
            self.img_background.image = gifImage
        }*/
        self.getSplashScreen()
        if Constants.COUNTRY_CODES.isEmpty {
            self.getCountryCodes()
        }
        
//        GIDSignIn.sharedInstance()?.presentingViewController = self
//        GIDSignIn.sharedInstance()?.delegate = self
        
        //self.fetchLocation()
    }
    
    //MARK:- IBActions...
    @IBAction func signUpClicked() {
        let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        //signUpVC.location = self.location
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    
    @IBAction func skipLogiinClicked() {
        print("skipLoginClicked")
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let navController =  storyBoard.instantiateViewController(withIdentifier: "HomeNavigation")
        if #available(iOS 13.0, *) {
            if let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate {
                sceneDelegate.window?.rootViewController = navController
                sceneDelegate.window?.makeKeyAndVisible()
            }
        } else {
            if let appdelegate =  UIApplication.shared.delegate as? AppDelegate {
                print("appdelegate:",appdelegate)
                appdelegate.window?.rootViewController = navController
                appdelegate.window?.makeKeyAndVisible()
            }
        }
        
    }
    
    @IBAction func facebookLoginClicked() {
        let login = LoginManager()
        login.logIn(permissions: ["public_profile"], viewController: self) { (result) in
            switch result {
            case .cancelled:
                ToastMessage.showToast(in: self, message: "login cancelled", bgColor: .white)
            case .success(granted: let granted, declined: let declined, token: let token):
                print("granted:",granted," , declined:",declined," , token",token)
                for permission in granted {
                    if permission == .publicProfile {
                        self.getFBUserData()
                        break
                    }
                }
                break
            case .failed(let error):
                ToastMessage.showToast(in: self, message: error.localizedDescription, bgColor: .white)
            }
        }
    }
    
    @IBAction func googleLoginClicked() {
//        GIDSignIn.sharedInstance()?.signIn()
    }
    
    @IBAction func termsOfUseClicked() {
        let termsOfUseVC = self.storyboard?.instantiateViewController(withIdentifier: "TermsOfUseVC") as! TermsOfUseVC
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(termsOfUseVC, animated: true)
        }
    }
    
    @IBAction func privacyPolicyClicked() {
        let privacyPolicyVC = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyVC") as! PrivacyPolicyVC
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(privacyPolicyVC, animated: true)
        }
    }
    
    //MARK:- Functions...
    func fetchLocation() {
        self.locationManager = CLLocationManager()
        self.locationManager?.delegate = self
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            locationManager?.requestWhenInUseAuthorization()
        case .authorizedAlways, .authorizedWhenInUse:
            self.locationManager?.startUpdatingLocation()
        default:
            break
        }
    }
    
    func getFBUserData() {
        if let token = AccessToken.current {
            let parameters = ["fields": "id, name, picture.type(large), first_name, last_name"]
            GraphRequest(graphPath: "me", parameters: parameters).start { (connection, result, err) in
                if let error = err {
                    print("getFBUserData error:",error.localizedDescription)
                    ToastMessage.showToast(in: self, message: error.localizedDescription, bgColor: .white)
                }
                else {
                    print("getFBUserData result:",result)
                    if let resultObj = result as? [String : Any] {
                        var params = [String : String]()
                        params["facebook_id"] = resultObj["id"] as? String ?? ""
                        params["first_name"] = resultObj["first_name"] as? String ?? ""
                        params["last_name"] = resultObj["last_name"] as? String ?? ""
                        params["type"] = "1"
                        params["device_id"] = UserDefaults.standard.string(forKey: Constants.KEYS.FCMID) ?? ""
                        params["imei_number"] = ""
                        params["lat"] = ""
                        params["lng"] = ""
                        params["country_name"] = ""
                        params["ip_address"] = ""
                        print("login params:",params)
                        self.loginWithFacebook(params)
                    }
                }
            }
        }
    }
    
    func changeRootController() {
        if #available(iOS 13.0, *) {
            if let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate {
                sceneDelegate.showHomeScreen()
            }
        } else {
            if let appDelegate =  UIApplication.shared.delegate as? AppDelegate {
                appDelegate.showHomeScreen()
            }
        }
    }

}

//extension LoginVC: GIDSignInDelegate {
//    //MARK:- GIDSignIn Delegates...
//
//    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
//        if let err = error {
//            self.view.makeToast(message: err.localizedDescription)
//        }
//        else {
//            var params = [String : String]()
//            params["google_id"] = user.userID
//            params["first_name"] = user.profile.name
//            params["last_name"] = user.profile.familyName
//            params["type"] = "1"
//            params["device_id"] = UserDefaults.standard.string(forKey: Constants.KEYS.FCMID) ?? ""
//            params["imei_number"] = ""
//            params["lat"] = ""
//            params["lng"] = ""
//            params["country_name"] = ""
//            params["ip_address"] = ""
//            print("login params:",params)
//            self.loginWithGoogle(params)
//        }
//    }
//
//}

extension LoginVC: CLLocationManagerDelegate {
    //MARK:- CLLocationManager Delegates...
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        /*guard let location = locations.first else {
            return
        }
        
        
        self.location = location
        manager.stopUpdatingLocation()*/
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        /*switch status {
        /*case .denied, .restricted:
            let pickLocation_vc = self.storyboard?.instantiateViewController(withIdentifier: "PickLocationVC") as! PickLocationVC
            pickLocation_vc.delegate = self
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(pickLocation_vc, animated: true)
            }*/
        case .authorizedAlways, .authorizedWhenInUse:
            manager.startUpdatingLocation()
        default:
            break
        }*/
    }
}

extension LoginVC {
    //MARK:- API's...
    func getCountryCodes() {
        TransportManager.sharedInstance.getCountryCodes { (data, err) in
            if let error = err {
                print("getCountryCodes error:",error.localizedDescription)
            }
            else {
                if let json = data as? JSON, let resultObj = json.dictionaryObject {
                    //print("getCountryCodes resultObj:",resultObj)
                    let countryCodesArray = resultObj["data"] as? [[String : Any]] ?? []
                    Constants.COUNTRY_CODES = CountryCodeModel().decodingCountryCodes(countryCodesArray)
                }
            }
        }
    }
    
    func loginWithFacebook(_ params: [String : String]) {
        TransportManager.sharedInstance.loginFacebook(params: params) { (data, err) in
            if let error = err {
                ToastMessage.showToast(in: self, message: error.localizedDescription, bgColor: .white)
            }
            else {
                if let json = data as? JSON, let resultObj = json.dictionaryObject {
                    print("loginWithFacebook resultObj:",resultObj)
                    
                    var user_data = resultObj["data"] as? [String : Any] ?? [:]
                    user_data["country_code"] = nil
                    UserDefaults.standard.set(user_data, forKey: Constants.KEYS.LoginResponse)
                    UserDefaults.standard.set(true, forKey: Constants.KEYS.isLoggedIn)
                    UserDefaults.standard.synchronize()
                    
                    if (UserDefaults.standard.value(forKey: Constants.KEYS.LoginResponse) != nil)
                    {
                        let dict = UserDefaults.standard.value(forKey: Constants.KEYS.LoginResponse) as! [String : AnyObject]
                        print(dict)
                        APIManager.shared.headers["token"] = (dict["token"] as! String)
                        
                        print("headers\(APIManager.shared.headers)")
                        
                        //self.changeRootController()
                        let chooseLanguageVC = self.storyboard?.instantiateViewController(withIdentifier: "ChooseLanguageVC") as! ChooseLanguageVC
                        chooseLanguageVC.isAfterLogin = true
                        DispatchQueue.main.async {
                            self.navigationController?.pushViewController(chooseLanguageVC, animated: true)
                        }
                    }
                }
            }
        }
    }
    
    func loginWithGoogle(_ params: [String : String]) {
        TransportManager.sharedInstance.loginGoogle(params: params) { (data, err) in
            if let error = err {
                ToastMessage.showToast(in: self, message: error.localizedDescription, bgColor: .white)
            }
            else {
                if let json = data as? JSON, let resultObj = json.dictionaryObject {
                    print("loginWithGoogle resultObj:",resultObj)
                    
                    var user_data = resultObj["data"] as? [String : Any] ?? [:]
                    user_data["country_code"] = nil
                    UserDefaults.standard.set(user_data, forKey: Constants.KEYS.LoginResponse)
                    UserDefaults.standard.set(true, forKey: Constants.KEYS.isLoggedIn)
                    UserDefaults.standard.synchronize()
                    
                    if (UserDefaults.standard.value(forKey: Constants.KEYS.LoginResponse) != nil)
                    {
                        let dict = UserDefaults.standard.value(forKey: Constants.KEYS.LoginResponse) as! [String : AnyObject]
                        print(dict)
                        APIManager.shared.headers["token"] = (dict["token"] as! String)
                        
                        print("headers\(APIManager.shared.headers)")
                        
                        //self.changeRootController()
                        let chooseLanguageVC = self.storyboard?.instantiateViewController(withIdentifier: "ChooseLanguageVC") as! ChooseLanguageVC
                        chooseLanguageVC.isAfterLogin = true
                        DispatchQueue.main.async {
                            self.navigationController?.pushViewController(chooseLanguageVC, animated: true)
                        }
                    }
                }
            }
        }
    }
    
    func getSplashScreen() {
        TransportManager.sharedInstance.getSplashScreen { (data, err) in
            if let error = err {
                ToastMessage.showToast(in: self, message: error.localizedDescription, bgColor: .white)
            }
            else {
                if let json = data as? JSON, let resultObj = json.dictionaryObject {
                    print("getSplashyScreen resultObj:",resultObj)
                    let userData = resultObj["data"] as? [String : Any] ?? [:]
                    self.img_background.sd_setImage(with: URL(string: userData["image"] as? String ?? ""), completed: nil)
                }
            }
        }
    }
}
