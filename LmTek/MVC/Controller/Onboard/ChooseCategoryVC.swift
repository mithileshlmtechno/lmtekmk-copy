//
//  ChooseCategoryVC.swift
//  LmTek
//
//  Created by PTBLR-1128 on 08/10/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class ChooseCategoryVC: UIViewController {
    //MARK:- Outlets...
    @IBOutlet weak var tbl_categories : UITableView!
    @IBOutlet weak var btn_continue : UIButton!
    @IBOutlet weak var btn_backArrow : UIButton!
    @IBOutlet weak var lbl_selectedInterests : UILabel!

    //MARK:- Variables...
    var categoriesArray = [CategoryItem]()
    var selectedCategoriesArray = [CategoryItem]()
    var isAfterLogin = false

    //MARK:- View Life Cycle...
    override func viewDidLoad() {
        super.viewDidLoad()
        tbl_categories.delegate = self
        tbl_categories.dataSource = self
        self.basicSetup()
        self.cellRegistration()
        //self.getCategories()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getCategoriesAPI()
    }

    //MARK:- IBActions...
    @IBAction func backClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func continueClicked() {
        if !self.selectedCategoriesArray.isEmpty {
            var ids = ""
            for language in self.selectedCategoriesArray {
                if let id = language.id {
                    if ids.isEmpty {
                        ids = id
                    }
                    else {
                        ids += ",\(id)"
                    }
                }
            }
            //self.updateCategories(categoryIDs: ids)
            self.updateCatregoryAPI(categoryIDs: ids)
            
        }
    }
    
    //MARK:- Functions...
    func cellRegistration() {
        self.tbl_categories.register(UINib(nibName: "ChooseCategoryTVCell", bundle: nil), forCellReuseIdentifier: "ChooseCategoryTVCell")
        self.tbl_categories.estimatedRowHeight = 0
        self.tbl_categories.estimatedSectionHeaderHeight = 0
        self.tbl_categories.estimatedSectionFooterHeight = 0
    }
    
    func basicSetup() {
        self.btn_backArrow.setImage(#imageLiteral(resourceName: "ic_back").withRenderingMode(.alwaysTemplate), for: .normal)
        self.btn_backArrow.tintColor = UIColor.white
    }

}

extension ChooseCategoryVC: UITableViewDelegate, UITableViewDataSource {
    //MARK:- TableView Delegates...
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categoriesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChooseCategoryTVCell") as! ChooseCategoryTVCell
        
        let categoryModel = self.categoriesArray[indexPath.row]
        
        cell.img_category.sd_setImage(with: URL(string: categoryModel.image ?? ""), completed: nil)
        cell.lbl_title.text = categoryModel.name
        
        if let _ = self.selectedCategoriesArray.firstIndex(where: { $0.id == categoryModel.id }) {
            cell.view_selectedCategory.isHidden = false
        }
        else {
            cell.view_selectedCategory.isHidden = true
        }
        
        if indexPath.row % 2 == 0 {
            cell.constraint_backgroundViewLeading.constant = 20
        }
        else {
            cell.constraint_backgroundViewLeading.constant = self.view.frame.size.width - (self.view.frame.size.width * 0.7 + 20)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let categoryModel = self.categoriesArray[indexPath.row]
                
        if let index = self.selectedCategoriesArray.firstIndex(where: { $0.id == categoryModel.id }) {
            self.selectedCategoriesArray.remove(at: index)
        }
        else {
            self.selectedCategoriesArray.append(categoryModel)
        }
        
        DispatchQueue.main.async {
            let contentOffset = tableView.contentOffset
            tableView.reloadRows(at: [indexPath], with: .none)
            tableView.layoutIfNeeded()
            tableView.setContentOffset(contentOffset, animated: false)
            self.lbl_selectedInterests.text = "Interest Selected \(self.selectedCategoriesArray.count)"
        }
    }
   @objc func gotoBackViewContyroller() {
       self.navigationController?.popViewController(animated: true)
    }
}

extension ChooseCategoryVC {
    //MARK: API's...
    
    func getCategoriesAPI(){
        let url = URL(string: Constants.BASE_URL+Constants.GET_CATEGORIES)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        DialougeUtils.addActivityView(view: self.view)
        self.getCategoriesAPICall(url, header: headers)
    }
    
    func getCategoriesAPICall(_ url: URL, header: HTTPHeaders){
        LoginSignUpAPI.getUserDataAPI(vc: self, url: url, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            self.categoriesArray = CategoryModel().decodingCategoryItems(result!["user_data"] as? [[String : Any]] ?? [])
                            self.tbl_categories.reloadData()
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                }
            }
        }
    }
//UPDATE_CATEGORY
//    func updateCategories(categoryIDs: String) {
//        TransportManager.sharedInstance.updateCategory(category_id: categoryIDs) { (data, err) in
//            if let error = err {
//                self.view.makeToast(message: error.localizedDescription)
//            }
//            else {
//                if let json = data as? JSON, let resultObj = json.dictionaryObject {
//                    print("updateCategories resultObj:",resultObj)
//                    if resultObj["status"] as? Bool == true {
//                        UIApplication.shared.keyWindow?.makeToast(message: resultObj["message"] as? String ?? "Categories updated")
//
//                        if self.isAfterLogin {
//                            self.changeRootController()
//                        }else{
//                            self.navigationController?.popViewController(animated: true)
//                        }
//                    }
//                    else {
//                        self.view.makeToast(message: resultObj["message"] as? String ?? "Unable to update category")
//                    }
//                }
//            }
//        }
//    }
    func updateCatregoryAPI(categoryIDs: String){
        let param: Parameters = ["category_id": categoryIDs]
        let url = URL(string: Constants.BASE_URL+Constants.UPDATE_CATEGORY)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        DialougeUtils.addActivityView(view: self.view)
        self.updateCatregoryAPICall(param, url: url, header: headers)
    }
    
    func updateCatregoryAPICall(_ param: Parameters, url: URL, header: HTTPHeaders){
        LoginSignUpAPI.SendOTPAPI(vc: self, parameter: param, url: url, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status: Bool = result!["status"] as! Bool
                        if status == true{
                           // UIApplication.shared.keyWindow?.makeToast(message: result!["message"] as? String ?? "Categories updated")
                            
                            if self.isAfterLogin {
                                self.changeRootController()
                            }else{
                                
                                ToastMessage.showToast(in: self, message: result!["message"] as! String, bgColor: .white)
                                Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.gotoBackViewContyroller), userInfo: nil, repeats: false)
                            }
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                }
            }
        }
    }

    func changeRootController() {
        //let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        //let navController =  storyBoard.instantiateViewController(withIdentifier: "HomeNavigation")
        if #available(iOS 13.0, *) {
            if let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate {
                //sceneDelegate.window?.rootViewController = navController
                //sceneDelegate.window?.makeKeyAndVisible()
                sceneDelegate.showHomeScreen()
            }
        } else {
            if let appDelegate =  UIApplication.shared.delegate as? AppDelegate {
                //appdelegate.window?.rootViewController = navController
                //appdelegate.window?.makeKeyAndVisible()
                appDelegate.showHomeScreen()
            }
        }
    }
}
