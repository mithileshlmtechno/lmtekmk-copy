//
//  CountryCodesVC.swift
//  LmTek
//
//  Created by PTBLR-1128 on 10/09/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

protocol CountryCodesVCDelegate {
    func selectedCountryCode(_ countryCode: CountryCodeItem)
}

class CountryCodesVC: UIViewController {
    //MARK:- Outlets...
    @IBOutlet weak var tbl_countryCodes : UITableView!
    @IBOutlet weak var txt_searchCountryCode : UITextField!

    //MARK:- Variables...
    var filteredCountryCodesArray = [CountryCodeItem]()
    var isSearching = false
    var delegate: CountryCodesVCDelegate? = nil
    
    //MARK:- View Life Cycle...
    override func viewDidLoad() {
        super.viewDidLoad()

        self.cellRegistration()
    }
    

    //MARK:- IBActions...
    @IBAction func backClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func textDidChange(_ textField: UITextField) {
        if textField.text?.isEmpty == true {
            self.isSearching = false
        }
        else {
            self.isSearching = true
            let countryCode = (textField.text ?? "").lowercased()
            self.filteredCountryCodesArray = Constants.COUNTRY_CODES.filter({ (codeItem) -> Bool in
                let name = (codeItem.name ?? "").lowercased()
                let code = (codeItem.phonecode ?? "").lowercased()
                return name.contains(countryCode) || code.contains(countryCode)
            })
            self.tbl_countryCodes.reloadData()
        }
    }
    
    //MARK:- Functions...
    func cellRegistration() {
        self.tbl_countryCodes.register(UINib(nibName: "CountryCodeTVCell", bundle: nil), forCellReuseIdentifier: "CountryCodeTVCell")
        self.tbl_countryCodes.rowHeight = UITableView.automaticDimension
        self.tbl_countryCodes.estimatedRowHeight = 40
    }


}

extension CountryCodesVC: UITableViewDelegate, UITableViewDataSource {
    //MARK:- TableView Delegates...
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isSearching {
            return self.filteredCountryCodesArray.count
        }
        return Constants.COUNTRY_CODES.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCodeTVCell") as! CountryCodeTVCell
        
        var model : CountryCodeItem
        
        if self.isSearching {
            model = self.filteredCountryCodesArray[indexPath.row]
        }
        else {
            model = Constants.COUNTRY_CODES[indexPath.row]
        }
        
        cell.loadCountryCode(model: model)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var model : CountryCodeItem
        
        if self.isSearching {
            model = self.filteredCountryCodesArray[indexPath.row]
        }
        else {
            model = Constants.COUNTRY_CODES[indexPath.row]
        }
        self.delegate?.selectedCountryCode(model)
        self.navigationController?.popViewController(animated: true)
    }
}
