//
//  ChooseLanguageVC.swift
//  LmTek
//
//  Created by PTBLR-1128 on 05/10/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class ChooseLanguageVC: UIViewController {
    //MARK:- Outlets...
    @IBOutlet weak var collection_languages : UICollectionView!
    @IBOutlet weak var btn_continue : UIButton!
    
    @IBOutlet var btnBack: UIButton!
    //MARK:- Variables...
    var languagesArray = [LanguageItem]()
    var selectedLanguagesArray = [LanguageItem]()
    var isAfterLogin = false
    
    //MARK: View Life Cycle...
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btn_continue.backgroundColor = colorYellow
        self.cellRegistration()
        if isAfterLogin == true {
            btnBack.isHidden = true
        } else {
            btnBack.isHidden = false
        }
        self.getLanguages()
    }
    

    //MARK: IBActions...
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func continueClicked() {
        if !self.selectedLanguagesArray.isEmpty {
            var ids = ""
            for language in self.selectedLanguagesArray {
                if let id = language.id {
                    if ids.isEmpty {
                        ids = id
                    }
                    else {
                        ids += ",\(id)"
                    }
                }
            }
            self.uploadLanguageAPI(ids)
        }
        //self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Functions...
    func cellRegistration() {
        self.collection_languages.register(UINib(nibName: "ChooseLanguageCVCell", bundle: nil), forCellWithReuseIdentifier: "ChooseLanguageCVCell")
    }

}

extension ChooseLanguageVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    //MARK:- CollectionView Delegates...
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.languagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChooseLanguageCVCell", for: indexPath) as! ChooseLanguageCVCell
        
        let languageModel = self.languagesArray[indexPath.item]
        cell.lbl_language.text = "\(languageModel.name ?? "")\n\(languageModel.translation ?? "")"
        
        if let _ = self.selectedLanguagesArray.firstIndex(where: { $0.id == languageModel.id }) {
            cell.lbl_language.textColor = .black//UIColor.white
            cell.view_background.backgroundColor = colorYellow
            cell.img_language.sd_setImage(with: URL(string: languageModel.image ?? "")) { (image, err, cacheType, url) in
                cell.img_language.image = image?.withRenderingMode(.alwaysTemplate)
                cell.img_language.tintColor = UIColor.black
            }
        }
        else {
            cell.lbl_language.textColor = UIColor.black
            cell.view_background.backgroundColor = UIColor.white
            cell.img_language.sd_setImage(with: URL(string: languageModel.image ?? "")) { (image, err, cacheType, url) in
                cell.img_language.image = image?.withRenderingMode(.alwaysTemplate)
                cell.img_language.tintColor = UIColor.black
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.view.frame.size.width - 50) / 2
        return CGSize(width: width, height: width / 2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let languageModel = self.languagesArray[indexPath.item]
        if let index = self.selectedLanguagesArray.firstIndex(where: { $0.id == languageModel.id }) {
            self.selectedLanguagesArray.remove(at: index)
        }
        else {
            self.selectedLanguagesArray.append(languageModel)
        }
        DispatchQueue.main.async {
            collectionView.reloadItems(at: [indexPath])
        }
    }
    //MARK: Goto Navigation
    
    @objc func gotoViewContyroller() {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func gotoCategoryViewContyroller() {
        let chooseCategoryVC = self.storyboard?.instantiateViewController(withIdentifier: "ChooseCategoryVC") as! ChooseCategoryVC
        chooseCategoryVC.isAfterLogin = true
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(chooseCategoryVC, animated: true)
        }
    }

}

extension ChooseLanguageVC {
    //MARK: Api's...
    func getLanguages(){
        let url = URL(string: Constants.BASE_URL+Constants.GET_LANGUAGES)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        DialougeUtils.addActivityView(view: self.view)
        self.getLanguagesAPICAll(url, header: headers)
    }
    
    func getLanguagesAPICAll(_ url: URL, header: HTTPHeaders){
        LoginSignUpAPI.getUserDataAPI(vc: self, url: url, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            print("getLanguages resultObj:",result!)
                            self.languagesArray = LanguageModel().decodingLanguageItems(result!["user_data"] as? [[String : Any]] ?? [])
                            DispatchQueue.main.async {
                                self.collection_languages.reloadData()
                            }
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                }
            }
        }
    }
//
//    func updateLanguages(languageIDs: String) {
//        TransportManager.sharedInstance.updateLanguage(language_id: languageIDs) { (data, err) in
//            if let error = err {
//                ToastMessage.showToast(in: self, message: error.localizedDescription, bgColor: .white)
//            }
//            else {
//                if let json = data as? JSON, let resultObj = json.dictionaryObject {
//                    print("updateLanguages resultObj:",resultObj)
//                    if resultObj["status"] as? Bool == true {
//                        UIApplication.shared.keyWindow?.makeToast(message: resultObj["message"] as? String ?? "Languages updated")
//                        if self.isAfterLogin {
//                            let chooseCategoryVC = self.storyboard?.instantiateViewController(withIdentifier: "ChooseCategoryVC") as! ChooseCategoryVC
//                            chooseCategoryVC.isAfterLogin = true
//                            DispatchQueue.main.async {
//                                self.navigationController?.pushViewController(chooseCategoryVC, animated: true)
//                            }
//                        }else{
//                            self.navigationController?.popViewController(animated: true)
//                        }
//                    }
//                    else {
//                        ToastMessage.showToast(in: self, message: resultObj["message"] as? String ?? "Unable to update language", bgColor: .white)
//                    }
//                }
//            }
//        }
//    }
    
    func uploadLanguageAPI(_ languageID: String) {
        let url = URL(string: Constants.BASE_URL+Constants.UPDATE_LANGUAGE)!
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        let param: Parameters = ["language_id": languageID]
        DialougeUtils.addActivityView(view: self.view)
        self.uploadLanguageAPICall(url, header: headers, param: param)
    }
    
    func uploadLanguageAPICall(_ url: URL, header: HTTPHeaders, param: Parameters){
        AllUserApis.allPostDataWithoutImageAPI(vc: self, url: url, parameter: param, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self.view)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status:Bool = result!["status"] as! Bool
                        if status == true{
                            if self.isAfterLogin {
                                ToastMessage.showToast(in: self, message: result!["message"] as! String, bgColor: .white)
                                Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.gotoCategoryViewContyroller), userInfo: nil, repeats: false)
                            }else{
                                ToastMessage.showToast(in: self, message: result!["message"] as! String, bgColor: .white)
                                Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.gotoViewContyroller), userInfo: nil, repeats: false)
                            }
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                }
            }
        }
    }
}
