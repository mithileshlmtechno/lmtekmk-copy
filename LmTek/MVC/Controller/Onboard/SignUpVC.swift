//
//  SignUpVC.swift
//  LmTek
//
//  Created by Sai Sankar on 12/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation

class SignUpVC: UIViewController {
    //MARK:- Outlets...
    @IBOutlet weak var txt_mobileNumber : UITextField!
    @IBOutlet weak var txt_countryCode : UITextField!
    @IBOutlet weak var view_next : UIView!
    @IBOutlet weak var btn_next : UIButton!
    
    //MARK:- Variables...
    //var location : CLLocation?

    //MARK:- View Life Cycle...
    override func viewDidLoad() {
        super.viewDidLoad()

        self.txt_mobileNumber.delegate = self
        //self.setGradientBackGround(self.view_next, colors: [UIColor.lightGray.withAlphaComponent(1).cgColor, UIColor.lightGray.withAlphaComponent(0.5).cgColor, UIColor.lightGray.withAlphaComponent(0).cgColor])
        self.view_next.setGradient(colors: [UIColor.lightGray.withAlphaComponent(1).cgColor, UIColor.lightGray.withAlphaComponent(0.5).cgColor, UIColor.lightGray.withAlphaComponent(0).cgColor], startPoint: CGPoint(x: 0, y: 1), endPoint: CGPoint(x: 0, y: 0))
        if Constants.COUNTRY_CODES.isEmpty {
            self.getCountryCodes()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        DispatchQueue.main.async {
            self.txt_mobileNumber.becomeFirstResponder()
        }
    }
    

    //MARK:- IBActions...
    @IBAction func backClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextClicked() {
        if self.txt_mobileNumber.text?.trimmingCharacters(in: .whitespaces).count == 10 {
            self.login()
        }
        /*let otpVC = self.storyboard?.instantiateViewController(withIdentifier: "OtpVC") as! OtpVC
        otpVC.mobileNumber = (self.txt_countryCode.text ?? "") + (self.txt_mobileNumber.text ?? "")
        self.navigationController?.pushViewController(otpVC, animated: true)*/
    }
    
    @IBAction func countryCodeClicked() {
        let countryCodesVC = self.storyboard?.instantiateViewController(withIdentifier: "CountryCodesVC") as! CountryCodesVC
        countryCodesVC.delegate = self
        self.navigationController?.pushViewController(countryCodesVC, animated: true)
    }
    
    //MARK:- Functions...
    func setGradientBackGround(_ view: UIView, colors: [CGColor]) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height)
        gradientLayer.colors = colors//[UIColor(hexString: "#45b448")!.cgColor,UIColor(hexString: "#fba919")!.cgColor,UIColor(hexString: "#ef3425")!.cgColor]
        gradientLayer.startPoint = CGPoint.init(x:0, y:1)
        gradientLayer.endPoint = CGPoint.init(x:0, y:0)
        
        //view.layer.insertSublayer(tgl, at: 0)
        gradientLayer.cornerRadius = 20
        view.layer.addSublayer(gradientLayer)
    }
    
    func changeNextViewBackground(enable: Bool) {
        let layers = self.view_next.layer.sublayers ?? []
        for i in 0 ..< layers.count {
            let layer = layers[i]
            if layer.isKind(of: CAGradientLayer.self) {
                self.view_next.layer.sublayers?.remove(at: i)
            }
        }
        if enable {
            self.view_next.setGradient(colors: [UIColor(hexString: Constants.ORANGE_COLOR)?.cgColor ?? UIColor.orange.cgColor, UIColor(hexString:  Constants.RED_COLOR)?.cgColor ?? UIColor.red.cgColor], startPoint: CGPoint(x: 0, y: 0), endPoint: CGPoint(x: 1, y: 1))
            self.btn_next.setTitleColor(UIColor.white, for: .normal)
        }
        else {
            self.view_next.setGradient(colors: [UIColor.lightGray.withAlphaComponent(1).cgColor, UIColor.lightGray.withAlphaComponent(0.5).cgColor, UIColor.lightGray.withAlphaComponent(0).cgColor], startPoint: CGPoint(x: 0, y: 1), endPoint: CGPoint(x: 0, y: 0))
            self.btn_next.setTitleColor(UIColor.black, for: .normal)
        }
    }

}

extension SignUpVC : CountryCodesVCDelegate {
    //MARK:- CountryCodesVC Delegates...
    func selectedCountryCode(_ countryCode: CountryCodeItem) {
        self.txt_countryCode.text = "+\(countryCode.phonecode ?? "")"
    }
}

extension SignUpVC: UITextFieldDelegate {
    //MARK:- UITextField Delegates...
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let _ = textField.text {
            if string.isEmpty {
                self.changeNextViewBackground(enable: false)
                return true
            }
        }
        if textField == txt_mobileNumber {
            if (txt_mobileNumber.text?.count)! >= 9 {
                self.changeNextViewBackground(enable: true)
            }
            else {
                self.changeNextViewBackground(enable: false)
            }
            if (textField.text?.count)! >= 10 {
                //self.changeNextViewBackground(enable: true)
                return false
            }
        }
        return true
    }
    
}

extension SignUpVC {
    //MARK:- API's...
    func getCountryCodes() {
        TransportManager.sharedInstance.getCountryCodes { (data, err) in
            if let error = err {
                print("error getting country codes:",error.localizedDescription)
            }
            else {
                if let json = data as? JSON, let resultObj = json.dictionaryObject {
                    //print("getCountryCodes resultObj:",resultObj)
                    let countryCodesArray = resultObj["data"] as? [[String : Any]] ?? []
                    Constants.COUNTRY_CODES = CountryCodeModel().decodingCountryCodes(countryCodesArray)
                }
            }
        }
    }
    
    func login() {
        var loginDict = [String : String]()
        loginDict["phone"] = self.txt_mobileNumber.text ?? ""
        loginDict["code"] = self.txt_countryCode.text ?? ""
        loginDict["type"] = "1"
        loginDict["device_id"] = UserDefaults.standard.string(forKey: Constants.KEYS.FCMID) ?? ""
        loginDict["imei_number"] = ""
        loginDict["lat"] = ""
        loginDict["lng"] = ""
        loginDict["country_name"] = ""
        loginDict["ip_address"] = ""
        TransportManager.sharedInstance.login(loginDict: loginDict) { (data, err) in
            if let error = err {
                print("login error:",error.localizedDescription)
            }
            else {
                if let json = data as? JSON, let resultObj = json.dictionaryObject {
                    print("login resultObj:",resultObj)
                    if resultObj["status"] as? Bool == true {
                        let loginDataDict = resultObj["data"] as? [String : Any] ?? [:]
                        let otpVC = self.storyboard?.instantiateViewController(withIdentifier: "OtpVC") as! OtpVC
                        otpVC.loginDataDict = loginDataDict
                        otpVC.loginDict = loginDict
                        DispatchQueue.main.async {
                            self.navigationController?.pushViewController(otpVC, animated: true)
                        }
                        
                    }
                }
            }
        }
    }
}
