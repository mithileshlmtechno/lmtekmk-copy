//
//  ResponseModel.swift
//  DocKare Doctor
//
//  Created by PTBLR-1128 on 20/11/19.
//  Copyright © 2019 Provab Technosoft Pvt Ltd. All rights reserved.
//

import Foundation

// Responder body for all apis response.
struct ResponseBody: Codable {
    
    // variables
    var error: Int = 0
    var status: Bool = false
    var message: String?
    
    // alternative
    enum CodingKeys: String, CodingKey {
        case error
        case status
        case message
    }
}

// Main - responder for all APIs.
struct ResponseModel {
    
    // convert date to model...
    static func responseResultModel(result_data: Data) -> ResponseBody? {
        
        do {
            // try block
            let model = try JSONDecoder().decode(ResponseBody.self, from: result_data)
            return model
        } catch {
            
            // error block
            print("Responder error(decoder): \(error.localizedDescription)")
            return nil
        }
    }
    
    // convert object to data
    static func convertObject(toData: Any) -> Data {
        
        do {
            // try block
            let json_data = try JSONSerialization.data(withJSONObject: toData, options: [])
            return json_data
        } catch {
            
            // error block
            print("Responder data error(decoder): \(error.localizedDescription)")
            return Data()
        }
    }
    
    // get response dictionary from result
    static func getResponseDictionary(object: Any?) -> [String: Any]? {
        
        // response object is dictionary...
        if let result_dict = object as? [String: Any] {
            if result_dict["status"] as? Bool == true {
                if let user_dict = result_dict["user_data"] as? [String: Any] {
                    return user_dict
                }
            }
        }
        return nil
    }
    
    
    // get response array from result
    static func getResponseArray(object: Any?) -> [[String: Any]]? {
        
        // response object is dictionary...
        if let result_dict = object as? [String: Any] {
            if result_dict["status"] as? Bool == true {
                if let user_dict = result_dict["user_data"] as? [[String: Any]] {
                    return user_dict
                }
            }
        }
        return nil
    }
    
    // get response string from result
    static func getResponseString(object: Any?) -> String? {
        
        // response object is dictionary...
        if let result_dict = object as? [String: Any] {
            if result_dict["status"] as? Bool == true {
                if let user_info = result_dict["user_data"] as? String {
                    return user_info
                }
            }
        }
        return nil
    }
}
