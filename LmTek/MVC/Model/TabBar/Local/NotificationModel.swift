//
//  NotificationModel.swift
//  LmTek
//
//  Created by PTBLR-1128 on 16/12/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import Foundation

struct NotificationModel {
    func decodingNotificationItems(array: [[String : Any]]) -> [NotificationItem] {
        let data = ResponseModel.convertObject(toData: array)
        do {
            let array_notifications = try JSONDecoder().decode([NotificationItem].self, from: data)
            return array_notifications
        }
        catch {
            print("decodingNotificationItems error:",error.localizedDescription)
            return []
        }
    }
}

struct NotificationItem: Codable {
    var user_id : String?
    var title : String?
    var message : String?
    var type : String?
    var added_on : String?
    var id : String?
    
    enum CodingKeys: String, CodingKey {
        case user_id
        case title
        case message
        case type
        case added_on
        case id
    }
}
