//
//  FollowUserModel.swift
//  LmTek
//
//  Created by PTBLR-1128 on 30/10/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import Foundation

struct FollowUserModel {
    func decodingFollowUserItems(array: [[String : Any]]) -> [FollowUserItem] {
        let data = ResponseModel.convertObject(toData: array)
        do {
            let array_followUsers = try JSONDecoder().decode([FollowUserItem].self, from: data)
            return array_followUsers
        }
        catch {
            print("decodingFollowUserItems error:",error.localizedDescription)
            return []
        }
    }
}

struct FollowUserItem : Codable {
    var is_following : String?
    var id : String?
    var follow_id : String?
    var profile_picture : String?
    var first_name : String?
    var last_name : String?
    var username : String?
    
    enum CodingKeys: String, CodingKey {
        case is_following
        case id
        case follow_id
        case profile_picture
        case first_name
        case last_name
        case username
    }
}
