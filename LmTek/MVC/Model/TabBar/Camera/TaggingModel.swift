//
//  TaggingModel.swift
//  LmTek
//
//  Created by PTBLR-1128 on 14/12/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import Foundation

public struct TaggingModel {
    public var text: String
    public var range: NSRange
}
