//
//  RecordedVideoModel.swift
//  LmTek
//
//  Created by PTBLR-1128 on 28/12/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import Foundation
import AVFoundation

struct RecordedVideoModel {
    var video_url: URL
    var isBackCameraActive: Bool
    var progress: Float
    var recordedTime: Double
    //var mergingURL: URL? = nil
    var audioProgress: Float? = nil
    var audioTime: CMTime? = nil
    var duetVideoTime: CMTime? = nil
    var videoSpeed: SpeedItem
}
