//
//  AudioModel.swift
//  LmTek
//
//  Created by PTBLR-1128 on 10/09/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import Foundation

struct AudioModel {
    func decodingAudios(_ array: [[String : Any]]) -> [AudioItem] {
        let data = ResponseModel.convertObject(toData: array)
        do {
            let array_consultations = try JSONDecoder().decode([AudioItem].self, from: data)
            return array_consultations
        }
        catch {
            print("decodingAudios error:",error.localizedDescription)
            return []
        }
    }
    
    func decodingFavouriteAudioItems(_ array: [[String : Any]]) -> [FavouriteAudioItem] {
        let data = ResponseModel.convertObject(toData: array)
        do {
            let array_favouriteAudios = try JSONDecoder().decode([FavouriteAudioItem].self, from: data)
            return array_favouriteAudios
        }
        catch {
            print("decodingFavouriteAudioItems error:",error.localizedDescription)
            return []
        }
    }
}

struct AudioItem : Codable {
    var id : String?
    var name : String?
    var url : String?
    var caption : String?
    //var status : String?
    //var added_on : String?
    var is_favourite : String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case url
        case caption
        //case status
        //case added_on
        case is_favourite
    }
}

struct FavouriteAudioItem: Codable {
    var favourite_sound_id : String?
    var id : String?
    var name : String?
    var url : String?
    var caption : String?
    var created_at : String?
    
    enum CodingKeys : String, CodingKey {
        case favourite_sound_id
        case id
        case name
        case url
        case caption
        case created_at
    }
}
