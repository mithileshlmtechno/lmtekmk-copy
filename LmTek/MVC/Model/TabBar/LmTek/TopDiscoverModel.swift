//
//  TopDiscoverModel.swift
//  LmTek
//
//  Created by PTBLR-1128 on 12/03/21.
//  Copyright © 2021 LmTek. All rights reserved.
//

import Foundation

struct TopDiscoverModel {
    func decodingTopDiscoverItem(dict: [String : Any]) -> TopDiscoverItem? {
        let data = ResponseModel.convertObject(toData: dict)
        do {
            let topDiscoverModel = try JSONDecoder().decode(TopDiscoverItem.self, from: data)
            return topDiscoverModel
        }
        catch {
            print("decodingTopDiscoverItem error:",error.localizedDescription)
            return nil
        }
    }
    
    func decodingSearchDiscoveryItems(array: [[String : Any]]) -> [SearchDiscoveryItem] {
        let data = ResponseModel.convertObject(toData: array)
        do {
            let array_searchDiscoveryItems = try JSONDecoder().decode([SearchDiscoveryItem].self, from: data)
            return array_searchDiscoveryItems
        }
        catch {
            print("decodingSearchDiscoveryItems error:",error.localizedDescription)
            return []
        }
    }
}

struct TopDiscoverItem: Codable {
    var name : String?
    var type : String?
    var id : String?
    var username : String?
    var count : Int?
    var videos : [TopDiscoverVideoItem]?
    
    enum CodingKeys: String, CodingKey {
        case name
        case type
        case id
        case username
        case count
        case videos
    }
}

struct TopDiscoverVideoItem: Codable {
    var video_url : String?
    var thumb_url : String?
    var video_id : String?
    
    enum CodingKeys: String, CodingKey {
        case video_url
        case thumb_url
        case video_id
    }
}

struct SearchDiscoveryItem: Codable {
    var name : String?
    var type : String?
    var id : String?
    var username : String?
    var count : Int?
    
    enum CodingKeys: String, CodingKey {
        case name
        case type
        case id
        case username
        case count
    }
}
