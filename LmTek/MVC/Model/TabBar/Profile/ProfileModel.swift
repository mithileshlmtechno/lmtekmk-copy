//
//  ProfileModel.swift
//  LmTek
//
//  Created by PTBLR-1128 on 13/10/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import Foundation

struct ProfileModel {
    func decodingProfileItem(dict: [String : Any]) -> ProfileItem? {
        let data = ResponseModel.convertObject(toData: dict)
        do {
            let profileModel = try JSONDecoder().decode(ProfileItem.self, from: data)
            return profileModel
        }
        catch {
            print("decodingProfileItem error:",error.localizedDescription)
            return nil
        }
    }
}

struct ProfileItem : Codable {
    var id : String?
    var username : String?
    var first_name : String?
    var last_name : String?
    var country_access : String?
    var country_code : String?
    var phone : String?
    var email : String?
    var image : String?
    var profile_picture : String?
    var cover_photo : String?
    var referral_code : String?
    var dob : String?
    var gender : String?
    var about : String?
    var is_celebrity : String?
    var insta_id : String?
    var apple_id : String?
    var facebook_id : String?
    var is_email : String?
    var added_on : String?
    var introduction : String?
    var autoplay_next_video : String?
    var language : String?
    var app_language : String?
    var total_likes : Int?//String?
    var total_dislike : Int?//String?
    var total_followers : Int?//String?
    var total_following : Int?//String?
    var included_tags : String?
    var rank : String?
    var total_videos : Int?//String?
    var group_name : String?
    var profile_security : String?
    var videos_list : [VideoItem]?
    
    enum CodingKeys : String, CodingKey {
        case id
        case username
        case first_name
        case last_name
        case country_access
        case country_code
        case phone
        case email
        case image
        case profile_picture
        case cover_photo
        case referral_code
        case dob
        case gender
        case about
        case is_celebrity
        case insta_id
        case apple_id
        case facebook_id
        case is_email
        case added_on
        case introduction
        case autoplay_next_video
        case language
        case app_language
        case total_likes
        case total_dislike
        case total_followers
        case total_following
        case included_tags
        case rank
        case total_videos
        case group_name
        case profile_security
        case videos_list
    }
}
