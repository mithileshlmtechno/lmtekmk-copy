//
//  PrivacySettingModel.swift
//  LmTek
//
//  Created by PTBLR-1128 on 02/11/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import Foundation

struct PrivacySettingModel {
    func decodingPrivacySettingItem(dict: [String : Any]) -> PrivacySettingItem? {
        let data = ResponseModel.convertObject(toData: dict)
        do {
            let privacySettingModel = try JSONDecoder().decode(PrivacySettingItem.self, from: data)
            return privacySettingModel
        }
        catch {
            print("decodingPrivacySettingItem error:",error.localizedDescription)
            return nil
        }
    }
}

struct PrivacySettingItem : Codable {
    var allow_other_to_find_me : String?
    var duet_security : String?
    var comment_security : String?
    var message_security : String?
    var react_security : String?
    var is_private : String?
    var video_download_security : String?
    
    enum CodingKeys: String, CodingKey {
        case allow_other_to_find_me
        case duet_security
        case comment_security
        case message_security
        case react_security
        case is_private
        case video_download_security
    }
}
