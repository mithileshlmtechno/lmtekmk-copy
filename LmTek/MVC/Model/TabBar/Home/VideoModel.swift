//
//  VideoModel.swift
//  LmTek
//
//  Created by PTBLR-1128 on 26/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import Foundation

struct VideoModel {
    func decodingVideoItems(array: [[String : Any]]) -> [VideoItem] {
        let data = ResponseModel.convertObject(toData: array)
        do {
            let array_consultations = try JSONDecoder().decode([VideoItem].self, from: data)
            return array_consultations
        }
        catch {
            print("decodingVideoItems error:",error.localizedDescription)
            return []
        }
    }
    
    func decodingVideoItem(dict: [String : Any]) -> VideoItem? {
        let data = ResponseModel.convertObject(toData: dict)
        do {
            let videoModel = try JSONDecoder().decode(VideoItem.self, from: data)
            return videoModel
        }
        catch {
            print("decodingVideoItem error:",error.localizedDescription)
            return nil
        }
    }
}

//struct VideoItem : Codable {
//    var id : String?
//    var video_id : String?
//    var user_id : String?
//    var video_url : String?
//    var caption : String?
//    var status : String?
//    var created_at : String?
//    var updated_at : String?
//    var likes : Int?//String?
//    var comments : Int?//String?
//    var shares : Int?//String?
//    var image_url : String?
//    var first_name : String?
//    var last_name : String?
//    var profile_image : String?
//    var downloads : Int?//String?
//    var is_following : String?
//    var is_liked : String?
//    var is_commented : String?
//    var audio_id : String?
//    var audio_name : String?
//    var audio_caption : String?
//    var audio_url : String?
//    var allow_comment : String?
//    var allow_download : String?
//    var allow_duet : String?
//    var username : String?
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case video_id
//        case user_id
//        case video_url
//        case caption
//        case status
//        case created_at
//        case updated_at
//        case likes
//        case comments
//        case shares
//        case image_url
//        case first_name
//        case last_name
//        case profile_image
//        case downloads
//        case is_following
//        case is_liked
//        case is_commented
//        case audio_id
//        case audio_name
//        case audio_caption
//        case audio_url
//        case allow_comment
//        case allow_download
//        case allow_duet
//        case username
//    }
//
//}

struct VideoItem : Codable {
    let id: String
    var userID : UserID
    let videoURL: String
    let thumbURL: String
    let caption: String
    let audioID, audioName: String?
    var status, likes, comments, shares: String
    let downloads, allowDownload, allowDuet, allowComment: String
    let firstName: String
    let lastName: String
    let username: String
    let profilePicture, totalFollowing, createdAt, updatedAt: String
    let videoID: String
    let isUserVerified: Bool
    var audioURL, audioCaption, isLiked, isCommented: String
    var isFollowing, isPlaying, playDuration: String

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case videoURL = "video_url"
        case thumbURL = "thumb_url"
        case caption
        case audioID = "audio_id"
        case audioName = "audio_name"
        case status, likes, comments, shares, downloads
        case allowDownload = "allow_download"
        case allowDuet = "allow_duet"
        case allowComment = "allow_comment"
        case firstName = "first_name"
        case lastName = "last_name"
        case username
        case profilePicture = "profile_picture"
        case totalFollowing = "total_following"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case videoID = "video_id"
        case isUserVerified = "is_user_verified"
        case audioURL = "audio_url"
        case audioCaption = "audio_caption"
        case isLiked = "is_liked"
        case isCommented = "is_commented"
        case isFollowing = "is_following"
        case isPlaying, playDuration
    }
}
enum FirstName: String, Codable {
    case joishnu = "Joishnu"
}

enum LastName: String, Codable {
    case roychowdhury = "Roychowdhury"
}

enum UserID: String, Codable {
    case the62B224B65E78904C3D1A24E8 = "62b224b65e78904c3d1a24e8"
}

enum Username: String, Codable {
    case joi7757 = "joi7757"
}
