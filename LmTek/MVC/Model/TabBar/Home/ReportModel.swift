//
//  ReportModel.swift
//  LmTek
//
//  Created by PTBLR-1128 on 21/09/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import Foundation

struct ReportModel {
    func decodingReportItems(array: [[String : Any]]) -> [ReportItem] {
        let data = ResponseModel.convertObject(toData: array)
        do {
            let array_reports = try JSONDecoder().decode([ReportItem].self, from: data)
            return array_reports
        }
        catch {
            print("decodingReportItems error:",error.localizedDescription)
            return []
        }
    }
}

struct ReportItem : Codable {
    var status : String?
    var id : String?
    var reason : String?
    
    enum CodingKeys: String, CodingKey {
        case status
        case id
        case reason
    }
}
