//
//  CacheManager.swift
//  LmTek
//
//  Created by PTBLR-1128 on 04/09/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import Foundation

public enum Result<T> {
    case success(T)
    case failure(NSError)
}

class CacheManager {
    
    static let shared = CacheManager()
    private let fileManager = FileManager.default
    private lazy var mainDirectoryURL : URL = {
        
        let documentURL = self.fileManager.urls(for: .cachesDirectory, in: .userDomainMask).first!
        return documentURL
    }()
    
    func getFileWith(stringURL: String, completionHandler: @escaping (Result<URL>) -> Void ) {
        let file = self.directoryFor(stringURL: stringURL)
        
        guard !fileManager.fileExists(atPath: file.path) else {
            completionHandler(Result.success(file))
            return
        }
        
        DispatchQueue.global().async {
            if let videoData = NSData(contentsOf: URL(string: stringURL)!) {
                videoData.write(to: file, atomically: true)
                DispatchQueue.main.async {
                    completionHandler(Result.success(file))
                }
            }
            else {
                DispatchQueue.main.async {
                    let error = NSError(domain: "SomeErrorDomain", code: -2001, userInfo: ["description" : "Can't download video"])
                    
                    completionHandler(Result.failure(error))
                }
            }
        }
    }
    
    private func directoryFor(stringURL: String) -> URL {
        let fileURL = URL(string: stringURL)!.lastPathComponent
        let file = self.mainDirectoryURL.appendingPathComponent(fileURL)
        return file
    }
    
    func clearCache(completionHandler: @escaping (Result<String>) -> Void){
        do {
            // Get the directory contents urls (including subfolders urls)
            let directoryContents = try fileManager.contentsOfDirectory( at: mainDirectoryURL, includingPropertiesForKeys: nil, options: [])
            for file in directoryContents {
                do {
                    try fileManager.removeItem(at: file)
                }
                catch let error as NSError {
                    debugPrint("Ooops! Something went wrong: \(error)")
                }

            }
            completionHandler(Result.success("Cache Cleared"))
        } catch let error as NSError {
            print(error.localizedDescription)
            completionHandler(Result.failure(error))

        }
    }
}
