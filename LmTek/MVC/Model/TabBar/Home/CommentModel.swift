//
//  CommentModel.swift
//  LmTek
//
//  Created by PTBLR-1128 on 18/09/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import Foundation

struct CommentModel {
    func decodingCommentItems(array: [[String : Any]]) -> [CommentItem] {
        let data = ResponseModel.convertObject(toData: array)
        do {
            let array_consultations = try JSONDecoder().decode([CommentItem].self, from: data)
            return array_consultations
        }
        catch {
            print("decodingCommentItems error:",error.localizedDescription)
            return []
        }
    }
}

struct CommentItem : Codable {
    var added_on : String?
    var profile_picture : String?
    var comment : String?
    var video_id : String?
    var first_name : String?
    var user_id : String?
    var last_name : String?
    var likes : Int?//String?
    var status : String?
    var id : String?
    
    enum CodingKeys : String, CodingKey {
        case added_on
        case profile_picture
        case comment
        case video_id
        case first_name
        case user_id
        case last_name
        case likes
        case status
        case id
    }
}
