//
//  CategoryModel.swift
//  LmTek
//
//  Created by PTBLR-1128 on 08/10/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import Foundation

struct CategoryModel {
    func decodingCategoryItems(_ array: [[String : Any]]) -> [CategoryItem] {
        let data = ResponseModel.convertObject(toData: array)
        do {
            let array_categories = try JSONDecoder().decode([CategoryItem].self, from: data)
            return array_categories
        }
        catch {
            print("decodingCategoryItems error:",error.localizedDescription)
            return []
        }
    }
}

struct CategoryItem : Codable {
    var id : String?
    var name : String?
    var image : String?
    var status : String?
    var created_at : String?
    
    enum CodingKeys : String, CodingKey {
        case id
        case name
        case image
        case status
        case created_at
    }
}
