//
//  CountryCodeModel.swift
//  LmTek
//
//  Created by PTBLR-1128 on 10/09/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import Foundation

struct CountryCodeModel {
    func decodingCountryCodes(_ array: [[String : Any]]) -> [CountryCodeItem] {
        let data = ResponseModel.convertObject(toData: array)
        do {
            let array_consultations = try JSONDecoder().decode([CountryCodeItem].self, from: data)
            return array_consultations
        }
        catch {
            print("decodingCountryCodes error:",error.localizedDescription)
            return []
        }
    }
}

struct CountryCodeItem : Codable {
    var id : String?
    var name : String?
    var phonecode : String?
    var iso3 : String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case phonecode
        case iso3
    }
}
