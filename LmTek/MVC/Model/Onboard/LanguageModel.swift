//
//  LanguageModel.swift
//  LmTek
//
//  Created by PTBLR-1128 on 07/10/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import Foundation

struct LanguageModel {
    func decodingLanguageItems(_ array: [[String : Any]]) -> [LanguageItem] {
        let data = ResponseModel.convertObject(toData: array)
        do {
            let array_languages = try JSONDecoder().decode([LanguageItem].self, from: data)
            return array_languages
        }
        catch {
            print("decodingLanguageItems error:",error.localizedDescription)
            return []
        }
    }
}

struct LanguageItem : Codable {
    var id : String?
    var name : String?
    var translation : String?
    var image : String?
    var status : String?
    var created_at : String?
    
    enum CodingKeys : String, CodingKey {
        case id
        case name
        case translation
        case image
        case status
        case created_at
    }
}
