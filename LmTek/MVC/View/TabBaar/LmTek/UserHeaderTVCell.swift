//
//  UserHeaderTVCell.swift
//  LmTek
//
//  Created by PTBLR-1128 on 21/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

class UserHeaderTVCell: UITableViewCell {
    //MARK:- Outlets...
    @IBOutlet weak var lbl_title : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
