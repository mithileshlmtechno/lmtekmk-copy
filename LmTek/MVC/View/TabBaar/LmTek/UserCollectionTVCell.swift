//
//  UserCollectionTVCell.swift
//  LmTek
//
//  Created by PTBLR-1128 on 21/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

class UserCollectionTVCell: UITableViewCell {
    //MARK:- Outlets...
    @IBOutlet weak var collection_users : UICollectionView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
        self.cellRegistration()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Functions...
    func cellRegistration() {
        self.collection_users.delegate = self
        self.collection_users.dataSource = self
        self.collection_users.register(UINib(nibName: "UserCVCell", bundle: nil), forCellWithReuseIdentifier: "UserCVCell")
    }
    
}

extension UserCollectionTVCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    //MARK:- CollectionView Delegates...
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserCVCell", for: indexPath) as! UserCVCell
        
        cell.img_user.image = UIImage(named: "india.png")
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (UIScreen.main.bounds.width - 20) / 3
        return CGSize(width: width, height: width * 1.28)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
}
