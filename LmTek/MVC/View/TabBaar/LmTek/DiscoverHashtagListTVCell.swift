//
//  DiscoverHashtagListTVCell.swift
//  LmTek
//
//  Created by PTBLR-1128 on 15/03/21.
//  Copyright © 2021 LmTek. All rights reserved.
//

import UIKit

class DiscoverHashtagListTVCell: UITableViewCell {
    //MARK:- Outlets...
    @IBOutlet weak var img_discoveryThumbnail: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
