//
//  SettingsAndPrivacyHeaderTVCell.swift
//  LmTek
//
//  Created by PTBLR-1128 on 17/11/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

class SettingsAndPrivacyHeaderTVCell: UITableViewCell {
    //MARK:- Outlets...
    @IBOutlet weak var lbl_title : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
