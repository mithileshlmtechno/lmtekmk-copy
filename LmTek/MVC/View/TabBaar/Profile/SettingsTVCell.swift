//
//  SettingsTVCell.swift
//  LmTek
//
//  Created by PTBLR-1128 on 26/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

class SettingsTVCell: UITableViewCell {
    //MARK:- Outlets...
    @IBOutlet weak var lbl_title : UILabel!
    @IBOutlet weak var img_arrow : UIImageView!
    @IBOutlet weak var lbl_separator : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
        self.img_arrow.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
