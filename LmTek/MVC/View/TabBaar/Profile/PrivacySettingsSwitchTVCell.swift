//
//  PrivacySettingsSwitchTVCell.swift
//  LmTek
//
//  Created by PTBLR-1128 on 27/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

protocol PrivacySettingDelegate {
    func switchClicked(status:Bool, cell: PrivacySettingsSwitchTVCell)
}

class PrivacySettingsSwitchTVCell: UITableViewCell {
    //MARK:- Outlets...
    @IBOutlet weak var switch_setting : UISwitch!
    @IBOutlet weak var lbl_title : UILabel!
    @IBOutlet weak var lbl_description : UILabel!
    
    //MARK:- Variables...
    var delegate : PrivacySettingDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- IBActions...
    @IBAction func switchAction(_ sender: UISwitch) {
        print("switchAction")
        if sender.isOn {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3) {
                    sender.borderColor = UIColor(hexString: Constants.PRIVACY_SETTING_SWITCH_BORDER_ORANGE_COLOR)
                    sender.thumbTintColor = UIColor(hexString: Constants.PRIVACY_SETTING_SWITCH_THUMB_ORANGE_COLOR)
                }
            }
        }
        else {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3) {
                    sender.borderColor = UIColor(hexString: Constants.PRIVACY_SETTING_SWITCH_BORDER_BLACK_COLOR)
                    sender.thumbTintColor = UIColor(hexString: Constants.PRIVACY_SETTING_SWITCH_THUMB_BLACK_COLOR)
                }
            }
        }
        self.delegate?.switchClicked(status:sender.isOn, cell: self)
    }
    
}
