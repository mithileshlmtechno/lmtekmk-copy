//
//  PrivacySettingAlertView.swift
//  LmTek
//
//  Created by PTBLR-1128 on 06/11/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

protocol PrivacySettingsAlertViewDelegate {
    func doneClicked(status: Int, view: PrivacySettingAlertView)
}


class PrivacySettingAlertView: UIView {

    //MARK:- Outlets...
    @IBOutlet weak var lbl_title : UILabel!
    @IBOutlet var img_radio_array : [UIImageView]!
    
    //MARK:- Variables...
    var delegate : PrivacySettingsAlertViewDelegate? = nil
    var selectedStatus = 0
    var title = ""
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        self.lbl_title.text = title
        for img in self.img_radio_array {
            if img.tag == selectedStatus {
                img.image = #imageLiteral(resourceName: "ic_radio_on")
            }
            else {
                img.image = #imageLiteral(resourceName: "ic_radio_off")
            }
        }
    }
    
    @IBAction func doneClicked() {
        self.delegate?.doneClicked(status: self.selectedStatus, view: self)
    }
    
    @IBAction func cancelClicked() {
        self.removeFromSuperview()
    }
    
    @IBAction func statusClicked(_ sender: UIButton) {
        if self.selectedStatus != sender.tag {
            self.selectedStatus = sender.tag
            for img in self.img_radio_array {
                if img.tag == sender.tag {
                    img.image = #imageLiteral(resourceName: "ic_radio_on")
                }
                else {
                    img.image = #imageLiteral(resourceName: "ic_radio_off")
                }
            }
        }
    }

}
