//
//  SettingsAndPrivacyTVCell.swift
//  LmTek
//
//  Created by PTBLR-1128 on 17/11/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

class SettingsAndPrivacyTVCell: UITableViewCell {
    //MARK:- Outlets...
    @IBOutlet weak var img_setting : UIImageView!
    @IBOutlet weak var lbl_setting : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
