//
//  EditProfileTextFieldTVCell.swift
//  LmTek
//
//  Created by PTBLR-1128 on 13/10/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

protocol EditProfileDetailDelegate {
    func textDidChange(cell: EditProfileTextFieldTVCell, textField: UITextField)
}

class EditProfileTextFieldTVCell: UITableViewCell {
    //MARK:- Outlets...
    @IBOutlet weak var lbl_title : UILabel!
    @IBOutlet weak var txt_value : UITextField!
    
    //MARK:- Variables...
    var delegate : EditProfileDetailDelegate? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- IBActions...
    @IBAction func textDidChange(textField: UITextField) {
        self.delegate?.textDidChange(cell: self, textField: textField)
    }
    
}
