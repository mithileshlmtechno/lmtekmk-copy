//
//  SelectDOBView.swift
//  LmTek
//
//  Created by PTBLR-1128 on 13/10/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

protocol SelectedDOBViewDelegate {
    func selectedDate(date: Date)
}

class SelectDOBView: UIView {
    //MARK:- Outlets...
    @IBOutlet weak var datePicker: UIDatePicker!

    //MARK:- Variables...
    var delegate : SelectedDOBViewDelegate? = nil
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
    }
    
    //MARK:- IBActions...
    @IBAction func cancelClicked() {
        self.removeFromSuperview()
    }
    
    @IBAction func selectGenderClicked() {
        self.delegate?.selectedDate(date: self.datePicker.date)
        self.removeFromSuperview()
    }

}
