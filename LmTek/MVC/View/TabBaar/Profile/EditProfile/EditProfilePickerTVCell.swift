//
//  EditProfilePickerTVCell.swift
//  LmTek
//
//  Created by PTBLR-1128 on 13/10/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

class EditProfilePickerTVCell: UITableViewCell {
    //MARK:- Outlets...
    @IBOutlet weak var lbl_title : UILabel!
    @IBOutlet weak var btn_picker : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
