//
//  ProfileDetailsTVCell.swift
//  LmTek
//
//  Created by PTBLR-1128 on 27/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

class ProfileDetailsTVCell: UITableViewCell {
    //MARK:- Outlets...
    @IBOutlet weak var lbl_title : UILabel!
    @IBOutlet weak var lbl_value : UILabel!
    @IBOutlet weak var btn_copy : UIButton!
    @IBOutlet weak var constraint_copyButtonWidth : NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
