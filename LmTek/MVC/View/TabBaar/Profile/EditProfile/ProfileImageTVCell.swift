//
//  ProfileImageTVCell.swift
//  LmTek
//
//  Created by PTBLR-1128 on 27/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

class ProfileImageTVCell: UITableViewCell {
    //MARK:- Outlets...
    @IBOutlet weak var img_profile : UIImageView!
    @IBOutlet weak var btn_edit : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
