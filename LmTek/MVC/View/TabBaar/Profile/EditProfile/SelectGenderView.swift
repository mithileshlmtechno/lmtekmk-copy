//
//  SelectGenderView.swift
//  LmTek
//
//  Created by PTBLR-1128 on 13/10/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
protocol SelectGenderViewDelegate {
    func selectedGender(gender: Int)
}

class SelectGenderView: UIView {
    //MARK:- Outlets...
    @IBOutlet weak var picker_gender : UIPickerView!
    
    //MARK:- Variables...
    var delegate: SelectGenderViewDelegate? = nil
    var gendersArray = ["Male", "Female", "Other"]
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.picker_gender.delegate = self
        self.picker_gender.dataSource = self
    }
    
    //MARK:- IBActions...
    @IBAction func cancelClicked() {
        self.removeFromSuperview()
    }
    
    @IBAction func selectClicked() {
        self.delegate?.selectedGender(gender: self.picker_gender.selectedRow(inComponent: 0))
        self.removeFromSuperview()
    }

}

extension SelectGenderView : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.gendersArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.gendersArray[row]
    }
}
