//
//  NotificationTVCell.swift
//  LmTek
//
//  Created by PTBLR-1128 on 25/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

class NotificationTVCell: UITableViewCell {
    //MARK:- Outlets...
    @IBOutlet weak var lbl_notification : UILabel!
    @IBOutlet weak var img_notification : UIImageView!
    @IBOutlet weak var lbl_separator : UILabel!
    @IBOutlet weak var lbl_date : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
