//
//  DownloadProgressView.swift
//  LmTek
//
//  Created by PTBLR-1128 on 12/11/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

class DownloadProgressView: UIView {
    //MARK:- Outlets...
    @IBOutlet weak var progress_download : UIProgressView!
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
    }

}
