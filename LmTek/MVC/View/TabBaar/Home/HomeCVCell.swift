//
//  HomeCVCell.swift
//  LmTek
//
//  Created by Sai Sankar on 14/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import AVFoundation

protocol HomeCVCellDelegate {
    func commentClicked(cell: HomeCVCell)
    func likeClicked(cell: HomeCVCell)
    func reportClicked(cell: HomeCVCell)
    func profileImageClicked(cell: HomeCVCell)
    func shareClicked(cell: HomeCVCell)
    func downloadClicked(cell: HomeCVCell)
    func followClicked(cell: HomeCVCell)
    func duetClicked(cell: HomeCVCell)
}

class HomeCVCell: UICollectionViewCell, ASAutoPlayVideoLayerContainer {
    
    //MARK:- Outlets...
    @IBOutlet weak var view_video : UIView!
    @IBOutlet weak var img_thumbnail : UIImageView!
    @IBOutlet weak var img_play : UIImageView!
    
    @IBOutlet weak var view_duet : UIView!
    @IBOutlet weak var view_audio : UIView!
    @IBOutlet weak var view_gradientBottom : UIView!
    
    @IBOutlet weak var lbl_likes : UILabel!
    @IBOutlet weak var lbl_comments : UILabel!
    @IBOutlet weak var lbl_shares : UILabel!
    @IBOutlet weak var lblDownloads: UILabel!
    
    @IBOutlet weak var constraint_reportButtonBottom : NSLayoutConstraint!
    @IBOutlet weak var btn_heart : UIButton!
    
    @IBOutlet weak var img_profileImage : UIImageView!
    @IBOutlet weak var lbl_name : UILabel!
    @IBOutlet weak var btn_follow : UIButton!
    
    @IBOutlet weak var img_duet : UIImageView!
    
    @IBOutlet weak var lbl_caption : UILabel!

    //MARK: Variables...
    var avPlayer: AVPlayer?
    var delegate : HomeCVCellDelegate? = nil
    
    var videoURL: String? {
        didSet {
            if let videoURL = videoURL {
                ASVideoPlayerController.sharedVideoPlayer.setupVideoFor(url: videoURL)
            }
            videoLayer.isHidden = videoURL == nil
        }
    }
    
    var videoLayer: AVPlayerLayer = AVPlayerLayer()
    var playerController: ASVideoPlayerController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.view_gradientBottom.setGradient(colors: [UIColor.black.withAlphaComponent(0).cgColor, UIColor.black.withAlphaComponent(0.7).cgColor], startPoint: CGPoint.init(x:0, y:0), endPoint: CGPoint.init(x:0, y:1))
        
        self.view_duet.cornerRadiusInDirection(radius: 27, direction: [.layerMinXMinYCorner, .layerMinXMaxYCorner])
        self.view_duet.setGradient(colors: [UIColor(hexString: Constants.ORANGE_COLOR)?.cgColor ?? UIColor.orange.cgColor, UIColor(hexString:  Constants.RED_COLOR)?.cgColor ?? UIColor.red.cgColor], startPoint: CGPoint.init(x:0, y:0), endPoint: CGPoint.init(x:0, y:1))
        
        self.view_audio.dropShadowWithRadius(x: 5)
        
        videoLayer.backgroundColor = UIColor.clear.cgColor
        videoLayer.videoGravity = .resizeAspectFill//AVLayerVideoGravity.resize
        self.img_thumbnail.layer.addSublayer(videoLayer)
        
        self.img_profileImage.rotate()
        
        let doubleTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleDoubleTap))
        doubleTap.numberOfTapsRequired = 2
        view_video.addGestureRecognizer(doubleTap)

        //view_video
    }
    
    override func prepareForReuse() {
        self.img_thumbnail.image = nil
        super.prepareForReuse()
        
        /*var layers = self.view_video.layer.sublayers ?? []
        if layers.count > 0 {
            layers.remove(at: 0)
        }
        self.view_video.layer.sublayers = layers
        self.avPlayer = nil*/
    }
    //MARK: Action
    
    @objc func handleDoubleTap() {
        print("Double Tap!")
        self.delegate?.likeClicked(cell: self)
    }

    //MARK: IBActions...
    @IBAction func commentClicked() {
        self.delegate?.commentClicked(cell: self)
    }
    
    @IBAction func likeClicked() {
        self.delegate?.likeClicked(cell: self)
    }
    
    @IBAction func reportClicked() {
        self.delegate?.reportClicked(cell: self)
    }
    
    @IBAction func profileImageClicked() {
        self.delegate?.profileImageClicked(cell: self)
    }
    
    @IBAction func shareClicked() {
        self.delegate?.shareClicked(cell: self)
    }
    
    @IBAction func downloadClicked() {
        self.delegate?.downloadClicked(cell: self)
    }
    
    @IBAction func followClicked() {
        self.delegate?.followClicked(cell: self)
    }
    
    @IBAction func duetClicked() {
        self.delegate?.duetClicked(cell: self)
    }
    
    //MARK:- Functions...
    func loadVideo(url: String) {
        print("loadVideo")
        /*guard let filePath = Bundle.main.path(forResource: url, ofType: "mp4") else { return }
        print("filepath:",filePath)*/
        
        CacheManager.shared.getFileWith(stringURL: url) { (result) in
            switch result {
            case .success(let videoURL):
                self.avPlayer = AVPlayer(url: videoURL)
                
                let videoLayer = AVPlayerLayer(player: self.avPlayer)
                videoLayer.frame = self.view_video.bounds
                videoLayer.videoGravity = .resizeAspectFill
                self.view_video.layer.addSublayer(videoLayer)
                
                self.img_thumbnail.isHidden = true
                self.img_play.isHidden = true
                
                self.avPlayer?.play()
                break
            case .failure(let error):
                print("caching error:",error.description)
            }
        }
        
        /*guard let fileURL = URL(string: url) else {
            
            print("unable to convert string to url")
            return
            
        }//URL(fileURLWithPath: filePath)
        */
        
    }
    
    func loadVideoDetail(model: VideoItem) {
        self.lbl_likes.text = "\(model.likes ?? "0")"
        self.lbl_shares.text = "\(model.shares ?? "0")"
        self.lbl_comments.text = "\(model.comments ?? "0")"
        if model.isLiked as! String == "1" {
            self.btn_heart.setImage(#imageLiteral(resourceName: "ic_heart_liked"), for: .normal)
        }
        else {
            self.btn_heart.setImage(#imageLiteral(resourceName: "ic_heart"), for: .normal)
        }
        self.lbl_name.text = model.firstName as! String
        if let loginDict = UserDefaults.standard.value(forKey: Constants.KEYS.LoginResponse) as? [String : Any], loginDict["id"] as? String == model.userID as! String {
            self.btn_follow.isHidden = true
        }
        else {
            if String(describing: model.isFollowing) == "1" {
                self.btn_follow.isHidden = true
            }
            else {
                self.btn_follow.isHidden = false
            }
        }
        if String(describing: model.audioID ?? "") != "0" {
            self.view_audio.isHidden = true
        }
        else {
            self.view_audio.isHidden = false
        }
        if String(describing: model.allowDuet) == "1" {
            self.img_duet.image = #imageLiteral(resourceName: "ic_duet").withRenderingMode(.alwaysOriginal)
        }
        else {
            self.img_duet.image = #imageLiteral(resourceName: "ic_duet").withRenderingMode(.alwaysTemplate)
            self.img_duet.tintColor = UIColor.lightGray
        }
        self.lbl_caption.text = String(describing: model.caption)
        self.videoURL = String(describing: model.videoURL)
        let imgUrl: String = String(describing: model.thumbURL)
        self.img_thumbnail.sd_setImage(with: URL(string: imgUrl), completed: nil)

        self.img_profileImage.sd_setImage(with: URL(string: String(describing: model.profilePicture)), completed: nil)
    }
//
    func loadVideoDetails(model: GetVideosData) {
        self.lbl_likes.text = "\(model.likes ?? "0")"
        self.lbl_shares.text = "\(model.shares ?? "0")"
        self.lbl_comments.text = "\(model.comments ?? "0")"
        self.lblDownloads.text = "\(model.downloads ?? "0")"
        
        if model.is_liked as! String == "1" {
            self.btn_heart.setImage(UIImage.init(named: "likeHeart"), for: .normal)
        }
        else {
            self.btn_heart.setImage(UIImage.init(named: "ic_heart"), for: .normal)
        }
        self.lbl_name.text = model.first_name as? String
        if let loginDict = UserDefaults.standard.value(forKey: Constants.KEYS.LoginResponse) as? [String : Any], loginDict["id"] as? String == model.user_id as? String {
            self.btn_follow.isHidden = true
        }
        else {
            if model.is_following as! String == "1" {
                self.btn_follow.isHidden = true
            }
            else {
                self.btn_follow.isHidden = false
            }
        }
        if String(describing: model.audio_id ?? "") != "0" {
            self.view_audio.isHidden = true
        }
        else {
            self.view_audio.isHidden = false
        }
        if model.allow_duet as! String == "1" {
            self.img_duet.image = #imageLiteral(resourceName: "ic_duet").withRenderingMode(.alwaysOriginal)
        }
        else {
            self.img_duet.image = #imageLiteral(resourceName: "ic_duet").withRenderingMode(.alwaysTemplate)
            self.img_duet.tintColor = UIColor.lightGray
        }
        self.lbl_caption.text = model.caption as? String
        self.videoURL = model.video_url as? String

        self.img_thumbnail.sd_setImage(with: URL(string: model.thumb_url as! String), completed: nil)
        self.img_profileImage.sd_setImage(with: URL(string: model.profile_picture as? String ?? ""), completed: nil)

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let horizontalMargin: CGFloat = 20
        let width: CGFloat = bounds.size.width - horizontalMargin * 2
        let height: CGFloat = (width * 0.9).rounded(.up)
        videoLayer.frame = CGRect(x: 0, y: 0, width: self.img_thumbnail.frame.size.width, height: self.img_thumbnail.frame.size.height)
        print("layoutSubviews videoLayer:",videoLayer.frame)
    }
    
    func visibleVideoHeight() -> CGFloat {
        let videoFrameInParentSuperView: CGRect? = self.superview?.superview?.convert(self.img_thumbnail.frame, from: self.img_thumbnail)
        guard let videoFrame = videoFrameInParentSuperView,
            let superViewFrame = superview?.frame else {
             return 0
        }
        let visibleVideoFrame = videoFrame.intersection(superViewFrame)
        return visibleVideoFrame.size.height
    }

}
