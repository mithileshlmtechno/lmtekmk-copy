//
//  CommentTVCell.swift
//  LmTek
//
//  Created by PTBLR-1128 on 18/09/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

class CommentTVCell: UITableViewCell {
    //MARK:- Outlets...
    @IBOutlet weak var lbl_name : UILabel!
    @IBOutlet weak var lbl_comment : UILabel!
    @IBOutlet weak var lbl_time : UILabel!
    @IBOutlet weak var img_profile : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
