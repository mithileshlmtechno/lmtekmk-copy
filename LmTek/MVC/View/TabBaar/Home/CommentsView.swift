//
//  CommentsView.swift
//  LmTek
//
//  Created by PTBLR-1128 on 17/09/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import SwiftyJSON
import IQKeyboardManagerSwift
import Alamofire

protocol CommentsViewDelegate {
    func updateComments(view: CommentsView)
}

class CommentsView: UIView {
    //MARK:- IBOutlets...
    @IBOutlet weak var view_commentsContent : IQPreviousNextView!
    @IBOutlet weak var lbl_comments : UILabel!
    @IBOutlet weak var txt_comment : UITextField!
    @IBOutlet weak var btn_send : UIButton!
    @IBOutlet weak var tbl_comments : UITableView!
    @IBOutlet weak var btn_cross : UIButton!
    @IBOutlet weak var constraint_commentsTextFieldBottom : NSLayoutConstraint!

    //MARK:- Variables...
    var video_model : GetVideosData?
    var commentsArray = [CommentItem]()
    var delegate : CommentsViewDelegate? = nil
    var commentsCount: String = ""
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        self.btn_send.setGradient(colors: [UIColor(hexString: Constants.ORANGE_COLOR)?.cgColor ?? UIColor.orange.cgColor, UIColor(hexString:  Constants.RED_COLOR)?.cgColor ?? UIColor.red.cgColor], startPoint: CGPoint(x: 0, y: 0), endPoint: CGPoint(x: 1, y: 0))
        self.btn_cross.setImage(#imageLiteral(resourceName: "ic_cross_white").withRenderingMode(.alwaysTemplate), for: .normal)
        self.btn_cross.tintColor = UIColor.black
//        self.lbl_comments.text = "\(video_model?.comments ?? 0) Comments"
        
        //self.getCommentsList()
        self.commentsListAPI()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.view_commentsContent.cornerRadiusInDirection(radius: 20, direction: [.layerMinXMinYCorner, .layerMaxXMinYCorner])
        self.cellRegistration()
        self.tbl_comments.delegate = self
        self.tbl_comments.dataSource = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(showKeyboard), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideKeyboard), name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    //MARK: IBActions...
    @IBAction func dismissView() {
        self.removeFromSuperview()
    }
    
    @IBAction func sendClicked() {
        if self.txt_comment.text?.trimmingCharacters(in: .whitespaces).isEmpty == false {
            self.txt_comment.resignFirstResponder()
            //self.addComment(comment: self.txt_comment.text?.trimmingCharacters(in: .whitespaces) ?? "")
            self.AddCommentstAPI(comment: self.txt_comment.text?.trimmingCharacters(in: .whitespaces) ?? "")
        }
    }
    
    //MARK: Functions...
    func cellRegistration() {
        self.tbl_comments.register(UINib(nibName: "CommentTVCell", bundle: nil), forCellReuseIdentifier: "CommentTVCell")
        self.tbl_comments.rowHeight = UITableView.automaticDimension
        self.tbl_comments.estimatedRowHeight = 40
    }
    
    @objc func showKeyboard(notification: Notification) {
        if let userInfo = notification.userInfo, let keyboardRect = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            print("showKeyboard",keyboardRect)
            DispatchQueue.main.async {
                let height = UIApplication.shared.statusBarFrame.height
                print("statusbar height:",height)
                if height != 20 {
                    self.constraint_commentsTextFieldBottom.constant = keyboardRect.size.height - 20
                    return
                }
                self.constraint_commentsTextFieldBottom.constant = keyboardRect.size.height + 10
                //self.view.frame.origin.y = 0
            }
        }
    }
    
    @objc func hideKeyboard(notification: Notification) {
        print("hideKeyboard")
        DispatchQueue.main.async {
            self.constraint_commentsTextFieldBottom.constant = 10
        }
    }

    func getDateTimeFormate() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "IST")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        return dateFormatter
    }
    func getDateTimeFormate2() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "IST")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.s"
        return dateFormatter
    }

    func getDateMonth(){
        //let todayDate = Date().currentUTCTimeZoneDate
    }
}

extension CommentsView : UITableViewDelegate, UITableViewDataSource {
    //MARK:- TableView Delegates...
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.commentsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTVCell") as! CommentTVCell
        
        let comment_model = self.commentsArray[indexPath.row]
        cell.lbl_name.text = "\(comment_model.first_name ?? "") \(comment_model.last_name ?? "")"
        cell.lbl_comment.text = comment_model.comment ?? ""
        cell.img_profile.sd_setImage(with: URL(string: comment_model.profile_picture ?? ""), placeholderImage: #imageLiteral(resourceName: "profile_unselected"))
        
        let added_on = comment_model.added_on ?? ""
        let todayDate = Date().currentUTCTimeZoneDate
        if let date = self.getDateTimeFormate().date(from: added_on), let today = self.getDateTimeFormate2().date(from: todayDate) {
            let calendar = Calendar(identifier: .gregorian)
            let components = calendar.dateComponents([.year, .month, .weekOfYear, .day, .hour, .minute], from: date, to: today)
            if let year = components.year, let month = components.month, let weeks = components.weekOfYear, let days = components.day, var hours = components.hour, var minutes = components.minute {
                var timeString = ""
                
                if year > 0 {
                    let years = (year > 1) ? "years" : "year"
                    timeString = "\(year) \(years) ago"
                }
                else if month > 0 {
                    let months = (month > 1) ? "months" : "month"
                    timeString = "\(month) \(months) ago"
                }
                else if weeks > 0 {
                    let week = (weeks > 1) ? "weeks" : "week"
                    timeString = "\(weeks) \(week) ago"
                }
                else if days > 0 {
                    let day = (days > 1) ? "days" : "day"
                    timeString = "\(days) \(day) ago"
                }
                else if hours < 0 {
                    if hours < 0 {
                        let absolute = abs(hours)
                        hours = absolute
                    }
                    let hour = (hours > 1) ? "hours" : "hour"
                    timeString = "\(hours) \(hour) ago"
                }
                else if minutes < 0 {
                    if minutes < 0 {
                        let absolute = abs(minutes)
                        minutes = absolute
                    }
                    let minute = (minutes > 1) ? "minutes" : "minute"
                    timeString = "\(minutes) \(minute) ago"
                }
                else {
                    timeString = "Just now"
                }
                cell.lbl_time.text = timeString
            }
            else {
                cell.lbl_time.text = ""
            }
        }
        else {
            cell.lbl_time.text = ""
        }
        
        return cell
    }
}

extension CommentsView {
    //MARK: API's...
//    func getCommentsList() {
//        let video_id = self.video_model?.video_id ?? ""
//        TransportManager.sharedInstance.getComments(video_id: video_id as! String) { (data, err) in
//            if let error = err {
//                self.makeToast(message: error.localizedDescription)
//            }
//            else {
//                if let json = data as? JSON, let resultObj = json.dictionaryObject {
//                    print("getcommentsList resultObj:",resultObj)
//                    let data = resultObj["data"] as? [[String : Any]] ?? []
//                    self.commentsArray = CommentModel().decodingCommentItems(array: data)
//                    //self.video_model?.comments = self.commentsArray.count//"\(self.commentsArray.count)"
//                    self.lbl_comments.text = "\(self.commentsArray.count) Comments"
//                    self.delegate?.updateComments(view: self)
//                    self.tbl_comments.reloadData()
//                }
//            }
//        }
//    }
    //GET_COMMETNS
    func commentsListAPI() {
        let video_id = self.video_model?.video_id ?? ""
        let url = URL(string: Constants.BASE_URL+Constants.GET_COMMETNS)!
        let parameters: Parameters = ["video_id": video_id]
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        print(parameters)
        DialougeUtils.addActivityView(view: self)
        self.commentsAPICall(parameters, url: url, header: headers)

    }
    func commentsAPICall(_ param: Parameters, url: URL, header: HTTPHeaders){
        AllUserApis.allPostDatalViewWithoutImageAPI(vc: self, url: url, parameter: param, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status: Bool = result!["status"] as! Bool
                        if status == true{
                            let data = result!["user_data"] as? [[String : Any]] ?? []
                            self.commentsArray = CommentModel().decodingCommentItems(array: data)
                            //self.video_model?.comments = self.commentsArray.count//"\(self.commentsArray.count)"
                            self.lbl_comments.text = "\(self.commentsArray.count) Comments"
                            self.delegate?.updateComments(view: self)
                            self.tbl_comments.reloadData()
                        }else{
                            let message:String = result!["message"] as! String
//                            self.showAlertMessageDispatchQueue(message)
                        }
                    }
                } else {
                }
            }
        }
    }

    func addComment(comment: String) {
        let video_id = self.video_model?.video_id ?? ""
        TransportManager.sharedInstance.addComment(video_id: video_id as! String, comment: comment) { (data, err) in
            if let error = err {
                self.makeToast(message: error.localizedDescription)
            }
            else {
                self.txt_comment.text = ""
                self.commentsListAPI()
            }
        }
    }
    
    func AddCommentstAPI(comment: String) {
        let video_id = self.video_model?.video_id ?? ""
        let url = URL(string: Constants.BASE_URL+Constants.ADD_COMMENT)!
        let parameters: Parameters = ["video_id": video_id, "comment": comment]
        let headers: HTTPHeaders = ["Authentication": String(User.sharedInstance.auth_key ?? "")]
        print(parameters)
        DialougeUtils.addActivityView(view: self)
        self.AddCommentstAPICall(parameters, url: url, header: headers)
    }
    
    func AddCommentstAPICall(_ param: Parameters, url: URL, header: HTTPHeaders){
        AllUserApis.allPostDatalViewWithoutImageAPI(vc: self, url: url, parameter: param, header: header) { result, error in
            DispatchQueue.main.async {
                DialougeUtils.removeActivityView(view: self)
                if error == nil{
                    if result != nil{
                        print(result!)
                        let status: Bool = result!["status"] as! Bool
                        if status == true{
                            self.txt_comment.text = ""
                            let dictdata: NSDictionary = result!["user_data"] as! NSDictionary
                            self.commentsCount = String(describing: dictdata["total_comment"] ?? "")
                            self.commentsListAPI()
                        }else{
                            let message:String = result!["message"] as! String
                            print(message)
                        }
                    }
                } else {
                }
            }
        }
    }

}
