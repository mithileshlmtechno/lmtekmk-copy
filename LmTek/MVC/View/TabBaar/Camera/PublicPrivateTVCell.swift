//
//  PublicPrivateTVCell.swift
//  LmTek
//
//  Created by PTBLR-1128 on 16/09/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

class PublicPrivateTVCell: UITableViewCell {
    //MARK:- Outlets...
    @IBOutlet weak var btn_public : UIButton!
    @IBOutlet weak var btn_private : UIButton!
    @IBOutlet weak var lbl_title : UILabel!
    @IBOutlet weak var lbl_separator : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
        //self.btn_public.setGradient(colors: [UIColor(hexString: Constants.ORANGE_COLOR)?.cgColor ?? UIColor.orange.cgColor, UIColor(hexString:  Constants.RED_COLOR)?.cgColor ?? UIColor.red.cgColor], startPoint: CGPoint(x: 0, y: 0), endPoint: CGPoint(x: 0, y: 1))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
