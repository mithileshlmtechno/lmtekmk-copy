//
//  CameraOptionsCVCell.swift
//  LmTek
//
//  Created by PTBLR-1128 on 24/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

class CameraOptionsCVCell: UICollectionViewCell {
    //MARK:- Outlets...
    @IBOutlet weak var lbl_title : UILabel!
    @IBOutlet weak var img_option : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
