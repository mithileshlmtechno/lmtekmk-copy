//
//  AudioTVCell.swift
//  LmTek
//
//  Created by PTBLR-1128 on 10/09/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

class AudioTVCell: UITableViewCell {
    //MARK:- Outlets...
    
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var lbl_title : UILabel!
    @IBOutlet weak var lbl_subTitle : UILabel!
    @IBOutlet weak var btn_useAudio : UIButton!
    @IBOutlet weak var view_useAudio : UIView!
    @IBOutlet weak var constraint_useAudioView : NSLayoutConstraint!
    @IBOutlet weak var img_playPause : UIImageView!
    @IBOutlet weak var btn_favourite : UIButton!
    @IBOutlet weak var lblCenterText: UILabel!
    var lblBG: UILabel = UILabel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblCenterText.text = ""
        self.selectionStyle = .none
//        self.view_useAudio.setGradient(colors: [UIColor(hexString: Constants.ORANGE_COLOR)?.cgColor ?? UIColor.orange.cgColor, UIColor(hexString: Constants.RED_COLOR)?.cgColor ?? UIColor.red.cgColor], startPoint: CGPoint(x: 0, y: 0), endPoint: CGPoint(x: 1, y: 0))
        self.view_useAudio.backgroundColor = colorYellow
        
        lblBG = UILabel()
        lblBG.frame = CGRect(x: 0, y: 0, width: 0, height: 110)
        lblBG.backgroundColor = colorYellow.withAlphaComponent(0.2)
        lblBG.clipsToBounds = true
        self.addSubview(lblBG)

        lblBG.isHidden = true

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
