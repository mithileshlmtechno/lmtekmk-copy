//
//  EfectsView.swift
//  LmTek
//
//  Created by Mithilesh kumar satnami on 07/07/22.
//  Copyright © 2022 LmTek. All rights reserved.
//

import UIKit

protocol EfectsViewDelegate {
    func didSelectEffect(voiceEffect: (name: String, type: VideoEffect))
}

class EfectsView: UIView {
    
    //MARK:- Outlets...
    var collectionEffects : UICollectionView!
    
    //MARK:- Variables...
    var voiceEffectsArray : [(name: String, type: VideoEffect)] = [(name: "None", type: .None), (name: "Chipmunk", type: .Chipmunk), (name: "Baritone", type: .Baritone)]
    var selectedvideoEffect : (name: String, type: VideoEffect) = (name: "None", type: .None)
    var delegate : EfectsViewDelegate? = nil

    override func draw(_ rect: CGRect) {
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.backgroundColor = UIColor.black.withAlphaComponent(0.1)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cellRegistration()
    }
    
    //MARK:- IBActions...
   @objc func closeViewClicked() {
        self.removeFromSuperview()
    }
    
    //MARK:- Functions...
    func cellRegistration() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)

        collectionEffects = UICollectionView(frame: CGRect(x: 0, y: screenHeight-150, width: screenWidth, height: 150), collectionViewLayout: layout)
        collectionEffects.backgroundColor = UIColor.black
        collectionEffects.dataSource = self
        collectionEffects.delegate = self
        collectionEffects.isUserInteractionEnabled = true
        collectionEffects.showsVerticalScrollIndicator = false

        self.collectionEffects.delegate = self
        self.collectionEffects.dataSource = self
        self.collectionEffects.register(UINib(nibName: "FilterCVCell", bundle: nil), forCellWithReuseIdentifier: "FilterCVCell")
        self.addSubview(collectionEffects)

    }
}

extension EfectsView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.voiceEffectsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCVCell", for: indexPath) as! FilterCVCell
        
        let voiceEffect = self.voiceEffectsArray[indexPath.item]
        cell.lbl_filterTitle.text = voiceEffect.name
        
        if self.selectedvideoEffect == voiceEffect {
            cell.img_filter.borderWidth = 1.0
            cell.lbl_filterTitle.textColor = UIColor(hexString: Constants.ORANGE_COLOR)
        }
        else {
            cell.img_filter.borderWidth = 0.0
            cell.lbl_filterTitle.textColor = UIColor.white
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let voiceEffect = self.voiceEffectsArray[indexPath.item]
        if self.selectedvideoEffect != voiceEffect {
            self.selectedvideoEffect = voiceEffect
            collectionView.reloadData()
            self.delegate?.didSelectEffect(voiceEffect: voiceEffect)
        }
    }
}

//MARK: VoiceEffectItem


public enum VideoEffect {
    case None
    case Chipmunk
    case Baritone
}
