//
//  VolumeFilterView.swift
//  LmTek
//
//  Created by PTBLR-1128 on 25/03/21.
//  Copyright © 2021 LmTek. All rights reserved.
//

import UIKit

protocol VolumeFilterViewDelegate {
    func adjustVolumeOfVideo(volume: Float)
    func sliderAdjusted(volume: Float)
}

class VolumeFilterView: UIView {
    //MARK:- Outlets...
    @IBOutlet weak var slider_volume: UISlider!
    

    //MARK:- Variables...
    var delegate : VolumeFilterViewDelegate? = nil
    var previousValue: Float = 1.0
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.slider_volume.value = previousValue
    }
    
    //MARK:- IBActions...
    @IBAction func closeViewClicked() {
        self.delegate?.adjustVolumeOfVideo(volume: self.previousValue)
        self.removeFromSuperview()
    }

    @IBAction func doneClicked() {
        self.delegate?.adjustVolumeOfVideo(volume: self.slider_volume.value)
        self.removeFromSuperview()
    }
    
    @IBAction func sliderAdjusted() {
        self.delegate?.sliderAdjusted(volume: self.slider_volume.value)
    }

}
