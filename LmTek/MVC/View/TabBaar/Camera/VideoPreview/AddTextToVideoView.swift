//
//  AddTextToVideoView.swift
//  LmTek
//
//  Created by PTBLR-1128 on 22/12/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

protocol AddTextToVideoViewDelegate {
    func addLabel(text: String, font: (title: String, font: String), alignment: NSTextAlignment, textStyle: Int, color: UIColor, selectedFont: Int)
    func editLabel(text: String, font: (title: String, font: String), alignment: NSTextAlignment, textStyle: Int, color: UIColor, tag: Int, selectedFont: Int)
}

class AddTextToVideoView: UIView {
    //MARK:- Outlets...
    @IBOutlet weak var txt_description: UITextView!
    @IBOutlet weak var collection_colors : UICollectionView!
    @IBOutlet weak var constraint_colorsCollectionBottom : NSLayoutConstraint!
    @IBOutlet weak var btn_alignment : UIButton!
    @IBOutlet weak var lbl_textStyle : UILabel!
    @IBOutlet weak var constraint_descriptionTextViewHeight : NSLayoutConstraint!
    @IBOutlet weak var collection_fonts : UICollectionView!
    
    //MARK:- Variables...
    var selectedAlignment : NSTextAlignment = .center
    var colorsArray : [UIColor] = [UIColor.white, UIColor.black, UIColor.red, UIColor.magenta, UIColor.blue, UIColor.green, UIColor.yellow, UIColor.orange, UIColor.systemPink, UIColor.systemTeal, UIColor.lightGray, UIColor.darkGray]
    var selectedColor : UIColor = .white//0
    var selectedTextStyle = 0
    var selectedFont = 0
    var fontsArray : [(title: String, font: String)] = [(title: "Helvetica", font: "Helvetica"), (title: "Bradley Hand", font: "BradleyHandITCTT-Bold"), (title: "Copperplate", font: "Copperplate"), (title: "Noteworthy", font: "Noteworthy-Bold"), (title: "Snell Roundhand", font: "SnellRoundhand"), (title: "Times New Roman", font: "TimesNewRomanPSMT")]
    var delegate : AddTextToVideoViewDelegate? = nil
    
    enum AddTextType {
        case ADD
        case EDIT
    }
    
    var addTextType: AddTextType = .ADD
    var labelTag: Int?
    var labelText: String?
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        DispatchQueue.main.async {
            self.txt_description.becomeFirstResponder()
        }
        self.setAlignmentButtonImage()
        if self.addTextType == .EDIT, let text = labelText {
            self.txt_description.text = text
        }
        self.setLabelStyle()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cellRegistration()
        self.txt_description.delegate = self
        
        //self.txt_description.becomeFirstResponder()
        self.txt_description.textAlignment = self.selectedAlignment
        
        //IQKeyboardManager.shared.enable = false
        NotificationCenter.default.addObserver(self, selector: #selector(showKeyboard), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideKeyboard), name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidHideNotification, object: nil)
        //IQKeyboardManager.shared.enable = true
    }
    
    //MARK:- IBActions...
    @IBAction func doneClicked() {
        if self.addTextType == .ADD {
            self.delegate?.addLabel(text:  self.txt_description.text, font: self.fontsArray[self.selectedFont], alignment: self.selectedAlignment, textStyle: self.selectedTextStyle, color: /*self.colorsArray[*/self.selectedColor, selectedFont: self.selectedFont)//])
        }
        else {
            self.delegate?.editLabel(text:  self.txt_description.text, font: self.fontsArray[self.selectedFont], alignment: self.selectedAlignment, textStyle: self.selectedTextStyle, color: self.selectedColor, tag: self.labelTag!, selectedFont: self.selectedFont)
        }
        self.removeFromSuperview()
    }
    
    @IBAction func backgroundButtonClicked() {
        self.removeFromSuperview()
    }
    
    @IBAction func changeTextAlignment() {
        switch self.selectedAlignment {
        case .center:
            self.selectedAlignment = .left
        case .left:
            self.selectedAlignment = .right
        case .right:
            self.selectedAlignment = .center
        default:
            break
        }
        self.txt_description.textAlignment = self.selectedAlignment
        self.setAlignmentButtonImage()
    }
    
    @IBAction func changeTextStyle() {
        switch self.selectedTextStyle {
        case 0:
            self.selectedTextStyle = 1
        case 1:
            self.selectedTextStyle = 2
        case 2:
            self.selectedTextStyle = 0
        default:
            break
        }
        self.setLabelStyle()
    }
    
    //MARK:- Functions...
    func cellRegistration() {
        self.collection_colors.delegate = self
        self.collection_colors.dataSource = self
        self.collection_colors.register(UINib(nibName: "TextColorCVCell", bundle: nil), forCellWithReuseIdentifier: "TextColorCVCell")
        
        self.collection_fonts.delegate = self
        self.collection_fonts.dataSource = self
        self.collection_fonts.register(UINib(nibName: "AddTextFontCVCell", bundle: nil), forCellWithReuseIdentifier: "AddTextFontCVCell")
    }
    
    @objc func showKeyboard(notification: Notification) {
        if let userInfo = notification.userInfo, let keyboardRect = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            print("showKeyboard",keyboardRect)
            DispatchQueue.main.async {
                //self.constraint_messageViewBottom.constant = keyboardRect.size.height
                let height = UIApplication.shared.statusBarFrame.height
                print("statusbar height:",height)
                if height != 20 {
                    DispatchQueue.main.async {
                        self.constraint_colorsCollectionBottom.constant = keyboardRect.size.height - 20
                    }
                    return
                }
                DispatchQueue.main.async {
                    self.constraint_colorsCollectionBottom.constant = keyboardRect.size.height+10
                }
            }
        }
    }
    
    @objc func hideKeyboard(notification: Notification) {
        print("hideKeyboard")
        DispatchQueue.main.async {
            self.constraint_colorsCollectionBottom.constant = 0
        }
    }
    
    func setLabelStyle() {
        switch self.selectedTextStyle {
        case 0:
            //let color = self.colorsArray[self.selectedColor]
            self.lbl_textStyle.backgroundColor = .clear
            self.lbl_textStyle.textColor = self.selectedColor//color
        case 1:
            //let color = self.colorsArray[self.selectedColor]
            self.lbl_textStyle.backgroundColor = self.selectedColor//color
            if /*color*/self.selectedColor == .white {
                self.lbl_textStyle.textColor = .black
            }
            else {
                self.lbl_textStyle.textColor = .white
            }
        case 2:
            //let color = self.colorsArray[self.selectedColor]
            self.lbl_textStyle.backgroundColor = self.selectedColor.withAlphaComponent(0.5)//color.withAlphaComponent(0.5)
            self.lbl_textStyle.textColor = .white
        default:
            break
        }
        self.changeText()
    }
    
    func setAlignmentButtonImage() {
        switch self.selectedAlignment {
        case .center:
            self.btn_alignment.setImage(#imageLiteral(resourceName: "ic_alignCenter"), for: .normal)
        case .left:
            self.btn_alignment.setImage(#imageLiteral(resourceName: "ic_alignLeft"), for: .normal)
        case .right:
            self.btn_alignment.setImage(#imageLiteral(resourceName: "ic_alignRight"), for: .normal)
        default:
            break
        }
    }
    
    func changeText() {
        var backgroundColor : UIColor
        var foregroundColor : UIColor
        //let color = self.colorsArray[self.selectedColor]
        switch self.selectedTextStyle {
        case 0:
            backgroundColor = .clear
            foregroundColor = self.selectedColor//color
        case 1:
            backgroundColor = self.selectedColor//color
            if /*color*/self.selectedColor == .white {
                foregroundColor = .black
            }
            else {
                foregroundColor = .white
            }
        case 2:
            backgroundColor = self.selectedColor.withAlphaComponent(0.5)//color.withAlphaComponent(0.5)
            foregroundColor = .white
        default:
            backgroundColor = .clear
            foregroundColor = self.selectedColor//color
        }
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = self.selectedAlignment
        let fontModel = self.fontsArray[self.selectedFont]
        let font = UIFont(name: fontModel.font, size: 25)!
        let attributedString = NSAttributedString(string: self.txt_description.text, attributes: [NSAttributedString.Key.foregroundColor : foregroundColor, NSAttributedString.Key.backgroundColor : backgroundColor, NSAttributedString.Key.font : font/*self.txt_description.font ?? UIFont.boldSystemFont(ofSize: 25)*/, NSAttributedString.Key.paragraphStyle : paragraphStyle])
        self.txt_description.attributedText = attributedString
        self.layoutSubviews()
        self.constraint_descriptionTextViewHeight.constant = self.txt_description.contentSize.height
    }

}

extension AddTextToVideoView : UITextViewDelegate {
    //MARK:- TextView Delegates...
    func textViewDidChange(_ textView: UITextView) {
        self.changeText()
    }
}

extension AddTextToVideoView : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    //MARK:- CollectionView Delegates...
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collection_colors {
            return self.colorsArray.count
        }
        if collectionView == self.collection_fonts {
            return self.fontsArray.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.collection_colors {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TextColorCVCell", for: indexPath) as! TextColorCVCell
            
            let color = self.colorsArray[indexPath.item]
            if self.selectedColor == color/*indexPath.item*/ {
                cell.constraint_colorViewWidth.constant = 30
                cell.view_color.cornerRadius = 15
            }
            else {
                cell.constraint_colorViewWidth.constant = 25
                cell.view_color.cornerRadius = 12.5
            }
            
            cell.view_color.backgroundColor = self.colorsArray[indexPath.item]
            
            return cell
        }
        if collectionView == self.collection_fonts {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddTextFontCVCell", for: indexPath) as! AddTextFontCVCell
            
            let font = self.fontsArray[indexPath.item]
            cell.lbl_font.font = UIFont(name: font.font,size:16)!
            cell.lbl_font.text = font.title
            
            if indexPath.item == self.selectedFont {
                cell.lbl_font.borderColor = UIColor.white
                cell.lbl_font.borderWidth = 2
            }
            else {
                cell.lbl_font.borderColor = UIColor.white.withAlphaComponent(0.3)
                cell.lbl_font.borderWidth = 1
            }
            
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.collection_colors {
            return CGSize(width: 40, height: 40)
        }
        if collectionView == self.collection_fonts {
            let font =  self.fontsArray[indexPath.item]
            print("sizeForItemAt font:",font.title)
            let fonts = UIFont.fontNames(forFamilyName: font.font)
            print("fonts:",fonts)
            let width = UILabel.textWidth(font: UIFont(name: font.font,size:16)!, text: font.title)
            
            return CGSize(width: width + 40, height: 50)
        }
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == self.collection_colors {
            return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        }
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.collection_colors {
            //self.txt_description.textColor = self.colorsArray[indexPath.item]
            self.selectedColor = self.colorsArray[indexPath.item]//indexPath.item
            collectionView.reloadData()
            self.changeText()
        }
        else if collectionView == self.collection_fonts {
            self.selectedFont = indexPath.item
            collectionView.reloadData()
            self.changeText()
        }
    }
}
