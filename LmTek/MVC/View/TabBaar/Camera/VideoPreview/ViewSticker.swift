//
//  StickerView.swift
//  LmTek
//
//  Created by PTBLR-1128 on 15/03/21.
//  Copyright © 2021 LmTek. All rights reserved.
//

import UIKit

protocol StickerDelegate {
    func didSelect(sticker: UIImage)
}

class ViewSticker: UIView {
    //MARK:- Outlets...
    @IBOutlet weak var collection_stickers: UICollectionView!
    
    //MARK:- Properties...
    var stickersArray : [UIImage] = [#imageLiteral(resourceName: "sticker1"), #imageLiteral(resourceName: "sticker2"), #imageLiteral(resourceName: "sticker3"), #imageLiteral(resourceName: "sticker4"), #imageLiteral(resourceName: "sticker5"), #imageLiteral(resourceName: "sticker6"), #imageLiteral(resourceName: "sticker7"), #imageLiteral(resourceName: "sticker8"), #imageLiteral(resourceName: "sticker9"), #imageLiteral(resourceName: "sticker10"), #imageLiteral(resourceName: "sticker11"), #imageLiteral(resourceName: "sticker12"), #imageLiteral(resourceName: "sticker13"), #imageLiteral(resourceName: "sticker14"), #imageLiteral(resourceName: "sticker15"), #imageLiteral(resourceName: "sticker16"), #imageLiteral(resourceName: "sticker17"), #imageLiteral(resourceName: "sticker18"), #imageLiteral(resourceName: "sticker19"), #imageLiteral(resourceName: "sticker20"), #imageLiteral(resourceName: "sticker21"), #imageLiteral(resourceName: "sticker22"), #imageLiteral(resourceName: "sticker23"), #imageLiteral(resourceName: "sticker24"), #imageLiteral(resourceName: "sticker25"), #imageLiteral(resourceName: "sticker26"), #imageLiteral(resourceName: "sticker27"), #imageLiteral(resourceName: "sticker28")]
    var delegate: StickerDelegate? = nil

    override func draw(_ rect: CGRect) {
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cellRegistration()
    }
    
    //MARK:- IBActions...
    @IBAction func closeStickerView() {
        self.removeFromSuperview()
    }
    
    //MARK:- Functions...
    func cellRegistration() {
        self.collection_stickers.delegate = self
        self.collection_stickers.dataSource = self
        self.collection_stickers.register(UINib(nibName: "StickerListCVCell", bundle: nil), forCellWithReuseIdentifier: "StickerListCVCell")
    }

}

extension ViewSticker: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    //MARK:- CollectionView Delegates...
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.stickersArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StickerListCVCell", for: indexPath) as! StickerListCVCell
        
        cell.img_sticker.image = self.stickersArray[indexPath.item]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 50, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.didSelect(sticker: self.stickersArray[indexPath.item])
        self.closeStickerView()
    }
}
