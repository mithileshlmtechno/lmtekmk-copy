//
//  VideoTextLabel.swift
//  LmTek
//
//  Created by PTBLR-1128 on 23/12/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

protocol VideoTextLabelDelegate {
    func editLabel(label: VideoTextLabel)
}

class VideoTextLabel: UILabel {

    //MARK:- Properties...
    var textStyle : Int = 0
    var textFont : (title: String, font: String)?
    var alignment : NSTextAlignment?
    var attText : NSMutableAttributedString?
    var color : UIColor?
    var pointSize : CGFloat = 0
    var gestureScale : Int = 0
    var selectedFont: Int = 0
    var rotationAngle : CGFloat = 0
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }*/
    var delegate: VideoTextLabelDelegate? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //self.setText()
    }

    //MARK:- Functions...
    func setText() {
        print("setText")
        //self.attributedText = self.attText
        var backgroundColor : UIColor
        var foregroundColor : UIColor
        switch self.textStyle {
        case 0:
            backgroundColor = .clear
            foregroundColor = color!
        case 1:
            backgroundColor = color!
            if color == .white {
                foregroundColor = .black
            }
            else {
                foregroundColor = .white
            }
        case 2:
            backgroundColor = color!.withAlphaComponent(0.5)
            foregroundColor = .white
        default:
            backgroundColor = .clear
            foregroundColor = color!
        }
        print("changeText backgroundColor:",backgroundColor,"foregroundColor:",foregroundColor)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = self.alignment!
        let font = UIFont(name: textFont!.font, size: 25)!
        let attributedString = NSAttributedString(string: self.attText?.string ?? "", attributes: [NSAttributedString.Key.foregroundColor : foregroundColor, NSAttributedString.Key.backgroundColor : backgroundColor, NSAttributedString.Key.font : font/*self.txt_description.font ?? UIFont.boldSystemFont(ofSize: 25)*/, NSAttributedString.Key.paragraphStyle : paragraphStyle])
        self.attributedText = attributedString
        self.layoutSubviews()
    }
    
    @objc func doubleTapped(_ gesture: UITapGestureRecognizer) {
        print("doubleTapped")
        self.delegate?.editLabel(label: self)
    }
}
