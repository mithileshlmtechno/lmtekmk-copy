//
//  VoiceEffectSelectionView.swift
//  LmTek
//
//  Created by PTBLR-1128 on 18/02/21.
//  Copyright © 2021 LmTek. All rights reserved.
//

import UIKit

protocol VoiceEffectSelectionViewDelegate {
    func didSelectVoiceEffect(voiceEffect: (name: String, type: VoiceEffectItem))
}

class VoiceEffectSelectionView: UIView {

    //MARK:- Outlets...
    @IBOutlet weak var collection_voiceEffects : UICollectionView!
    
    //MARK:- Variables...
    var voiceEffectsArray : [(name: String, type: VoiceEffectItem)] = [(name: "None", type: .None), (name: "Chipmunk", type: .Chipmunk), (name: "Baritone", type: .Baritone)]
    var selectedVoiceEffect : (name: String, type: VoiceEffectItem) = (name: "None", type: .None)
    var delegate : VoiceEffectSelectionViewDelegate? = nil

    override func draw(_ rect: CGRect) {
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cellRegistration()
    }
    
    //MARK:- IBActions...
    @IBAction func closeViewClicked() {
        self.removeFromSuperview()
    }
    
    //MARK:- Functions...
    func cellRegistration() {
        self.collection_voiceEffects.delegate = self
        self.collection_voiceEffects.dataSource = self
        self.collection_voiceEffects.register(UINib(nibName: "FilterCVCell", bundle: nil), forCellWithReuseIdentifier: "FilterCVCell")
    }
}

extension VoiceEffectSelectionView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.voiceEffectsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCVCell", for: indexPath) as! FilterCVCell
        
        let voiceEffect = self.voiceEffectsArray[indexPath.item]
        cell.lbl_filterTitle.text = voiceEffect.name
        
        if self.selectedVoiceEffect == voiceEffect {
            cell.img_filter.borderWidth = 1.0
            cell.lbl_filterTitle.textColor = UIColor(hexString: Constants.ORANGE_COLOR)
        }
        else {
            cell.img_filter.borderWidth = 0.0
            cell.lbl_filterTitle.textColor = UIColor.white
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let voiceEffect = self.voiceEffectsArray[indexPath.item]
        if self.selectedVoiceEffect != voiceEffect {
            self.selectedVoiceEffect = voiceEffect
            collectionView.reloadData()
            self.delegate?.didSelectVoiceEffect(voiceEffect: voiceEffect)
        }
    }
}

//MARK:- VoiceEffectItem
public enum VoiceEffectItem {
    case None
    case Chipmunk
    case Baritone
}
