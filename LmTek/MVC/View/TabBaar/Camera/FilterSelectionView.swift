//
//  FilterSelectionView.swift
//  LmTek
//
//  Created by PTBLR-1128 on 03/12/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

protocol FilterSelectionViewDelegate {
    func didSelectFilter(filter: (title: String, type: FilterItem))
}

class FilterSelectionView: UIView {
    //MARK:- Outlets...
    @IBOutlet weak var collection_filters : UICollectionView!
    
    //MARK:- Variables...
    var filtersArray : [(title: String, type: FilterItem)] = [(title: "None", type: .NONE), (title: "F1", type: .MONOCHROME_FILTER), (title: "F2", type: .INVERT_COLORS_FILTER), (title: "F3", type: .FALSE_COLOR_FILTER), (title: "F4", type: .SEPIA_COLOR_FILTER), (title: "F5", type: .MONO_EFFECT_FILTER), (title: "F6", type: .PROCESS_EFFECT_FILTER), (title: "F7", type: .TRANSFER_EFFECT_FILTER), (title: "F8", type: .POSTERIZE_FILTER), (title: "F9", type: .COLOR_MATRIX_FILTER), (title: "F10", type: .TEMPERATURE_TINT_FILTER), (title: "F11", type: .TEMPERATURE_TINT_FILTER)]
    var delegate : FilterSelectionViewDelegate? = nil
    
    var selectedFilter : (title: String, type: FilterItem) = (title: "None", type: .NONE)
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cellRegistration()
    }
    
    //MARK:- IBActions...
    @IBAction func closeViewClicked() {
        self.removeFromSuperview()
    }
    
    //MARK:- Functions...
    func cellRegistration() {
        self.collection_filters.delegate = self
        self.collection_filters.dataSource = self
        self.collection_filters.register(UINib(nibName: "FilterCVCell", bundle: nil), forCellWithReuseIdentifier: "FilterCVCell")
    }

}

extension FilterSelectionView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.filtersArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCVCell", for: indexPath) as! FilterCVCell
        
        let filter = self.filtersArray[indexPath.item]
        cell.lbl_filterTitle.text = filter.title//self.filtersArray[indexPath.item].title
        
        if self.selectedFilter == filter {
            cell.img_filter.borderWidth = 1.0
            cell.lbl_filterTitle.textColor = UIColor(hexString: Constants.ORANGE_COLOR)
        }
        else {
            cell.img_filter.borderWidth = 0.0
            cell.lbl_filterTitle.textColor = UIColor.white
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let filter = self.filtersArray[indexPath.item]
        if self.selectedFilter != filter {
            self.selectedFilter = filter
            collectionView.reloadData()
            self.delegate?.didSelectFilter(filter: filter)
        }
    }
}

//MARK:- FilterItem enum
public enum FilterItem : String {
    case NONE = ""
    case INVERT_COLORS_FILTER = "CIColorInvert"
    case MONOCHROME_FILTER = "CIColorMonochrome"
    case FALSE_COLOR_FILTER = "CIFalseColor"
    case SEPIA_COLOR_FILTER = "CISepiaTone"
    case MONO_EFFECT_FILTER = "CIPhotoEffectMono"
    case PROCESS_EFFECT_FILTER = "CIPhotoEffectProcess"
    case TRANSFER_EFFECT_FILTER = "CIPhotoEffectTransfer"
    case POSTERIZE_FILTER = "CIColorPosterize"
    
    case COLOR_MATRIX_FILTER = "CIColorMatrix"
    case TEMPERATURE_TINT_FILTER = "CITemperatureAndTint"
}
