//
//  SpeedVariationView.swift
//  LmTek
//
//  Created by PTBLR-1128 on 13/11/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

protocol SpeedVariationViewDelegate {
    func selectedSpeed(speedItem: SpeedItem)
}

class SpeedVariationView: UIView {
    //MARK:- Outlets...
    @IBOutlet var speedButtonsArray : [UIButton]!
    
    //MARK:- Variables...
    var selectedSpeed : SpeedItem = .NORMAL
    var delegate : SpeedVariationViewDelegate? = nil
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.setButtonsColors()
        for btn in self.speedButtonsArray {
            switch btn.tag {
            case 0:
                btn.cornerRadiusInDirection(radius: 5, direction: [.layerMinXMinYCorner, .layerMinXMaxYCorner])
            case 4:
                btn.cornerRadiusInDirection(radius: 5, direction: [.layerMaxXMinYCorner, .layerMaxXMaxYCorner])
            default:
                continue
            }
        }
    }
    
    //MARK:- IBActions...
    @IBAction func cancelClicked() {
        self.removeFromSuperview()
    }
    
    @IBAction func speedButtonClicked(_ sender: UIButton) {
        if selectedSpeed.rawValue != sender.tag {
            self.selectedSpeed = SpeedItem(rawValue: sender.tag)!
            self.setButtonsColors()
            self.delegate?.selectedSpeed(speedItem: self.selectedSpeed)
        }
    }
    
    //MARK:- Fuctions...
    func setButtonsColors() {
        for btn in speedButtonsArray {
            if btn.tag == selectedSpeed.rawValue {
                //btn.borderColor = UIColor(hexString: Constants.ORANGE_COLOR)
                btn.backgroundColor = UIColor.lightGray//UIColor.init(hexString: Constants.ORANGE_COLOR)
                btn.setTitleColor(UIColor.black, for: .normal)
            }
            else {
                //btn.borderColor = UIColor.lightGray
                btn.backgroundColor = UIColor.black.withAlphaComponent(0.7)//UIColor.clear
                btn.setTitleColor(UIColor.white, for: .normal)
            }
        }
    }

}
