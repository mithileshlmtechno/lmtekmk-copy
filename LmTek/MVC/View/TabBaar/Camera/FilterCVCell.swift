//
//  FilterCVCell.swift
//  LmTek
//
//  Created by PTBLR-1128 on 03/12/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

class FilterCVCell: UICollectionViewCell {
    //MARK:- Outlets...
    @IBOutlet weak var img_filter : UIImageView!
    @IBOutlet weak var lbl_filterTitle : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        img_filter.borderColor = UIColor(hexString: Constants.ORANGE_COLOR)
    }

}
