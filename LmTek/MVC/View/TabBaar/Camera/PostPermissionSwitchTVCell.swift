//
//  PostPermissionSwitchTVCell.swift
//  LmTek
//
//  Created by PTBLR-1128 on 16/09/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

protocol PostPermissionSwitchTVCellDelegate {
    func changeStatus(cell: PostPermissionSwitchTVCell)
}

class PostPermissionSwitchTVCell: UITableViewCell {
    //MARK:- Outlets...
    @IBOutlet weak var lbl_title : UILabel!
    @IBOutlet weak var lbl_separator : UILabel!
    @IBOutlet weak var switch_postPermission : UISwitch!
    
    //MARK:- Variables...
    var delegate : PostPermissionSwitchTVCellDelegate? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- IBActions...
    @IBAction func switchAction(_ sender: UISwitch) {
        if sender.isOn {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3) {
                    sender.borderColor = UIColor(hexString: Constants.PRIVACY_SETTING_SWITCH_BORDER_ORANGE_COLOR)
                    sender.thumbTintColor = colorYellow//UIColor(hexString: Constants.PRIVACY_SETTING_SWITCH_THUMB_ORANGE_COLOR)
                }
            }
        }
        else {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3) {
                    sender.borderColor = UIColor(hexString: Constants.PRIVACY_SETTING_SWITCH_BORDER_BLACK_COLOR)
                    sender.thumbTintColor = UIColor.gray//UIColor(hexString: Constants.PRIVACY_SETTING_SWITCH_THUMB_BLACK_COLOR)
                }
            }
        }
        self.delegate?.changeStatus(cell: self)
    }
    
}
