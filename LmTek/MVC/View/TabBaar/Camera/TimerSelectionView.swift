//
//  TimerSelectionView.swift
//  LmTek
//
//  Created by PTBLR-1128 on 07/12/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

protocol TimerSelectionViewDelegate {
    func startShutter(timeSpot: Int, timer: Float)
}

class TimerSelectionView: UIView {

    //MARK:- Outlets...
    @IBOutlet weak var collection_spots : UICollectionView!
    @IBOutlet weak var lbl_timer : UILabel!
    @IBOutlet weak var slider_timer : UISlider!
    
    //MARK:- Variables...
    var timerSlotsArray = [3, 10, 15, 20, 30]//, 40, 50, 60]
    var delegate : TimerSelectionViewDelegate? = nil
    var selectedSlot = 0
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cellRegistration()
    }
    
    //MARK:- IBActions...
    @IBAction func backButtonClicked() {
        self.removeFromSuperview()
    }
    
    @IBAction func startShootingClicked() {
        self.delegate?.startShutter(timeSpot: self.timerSlotsArray[self.selectedSlot], timer: self.slider_timer.value)
        self.removeFromSuperview()
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        self.lbl_timer.text = "\(Int(floor(sender.value)))s"
    }
    
    //MARK:- Functions...
    func cellRegistration() {
        self.collection_spots.delegate = self
        self.collection_spots.dataSource = self
        self.collection_spots.register(UINib(nibName: "TimerSpotCVCell", bundle: nil), forCellWithReuseIdentifier: "TimerSpotCVCell")
    }
}

extension TimerSelectionView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    //MARK:- CollectionView Delegates...
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.timerSlotsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TimerSpotCVCell", for: indexPath) as! TimerSpotCVCell
        
        let slot = self.timerSlotsArray[indexPath.item]
        cell.lbl_timer.text = "\(slot)s"
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let text =  "\(self.timerSlotsArray[indexPath.item])s"
        let width = UILabel.textWidth(font: UIFont(name:"OpenSans-Regular",size:12)!, text: text)
        
        return CGSize(width: width + 10, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let window = UIApplication.shared.keyWindow!
        let slot = self.timerSlotsArray[indexPath.item]
        self.selectedSlot = indexPath.item
        window.makeToast(message: "Switched to \(slot)s timer")
    }
    
}
