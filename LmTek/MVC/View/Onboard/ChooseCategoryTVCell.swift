//
//  ChooseCategoryTVCell.swift
//  LmTek
//
//  Created by PTBLR-1128 on 08/10/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

class ChooseCategoryTVCell: UITableViewCell {
    //MARK:- IBOutlets...
    @IBOutlet weak var lbl_title : UILabel!
    @IBOutlet weak var img_category : UIImageView!
    @IBOutlet weak var view_selectedCategory : UIView!
    @IBOutlet weak var view_background : UIView!
    @IBOutlet weak var constraint_backgroundViewLeading : NSLayoutConstraint!
    @IBOutlet weak var img_tick : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
        self.view_background.cornerRadiusInDirection(radius: 10, direction: [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner])
        self.img_tick.image = #imageLiteral(resourceName: "ic_save").withRenderingMode(.alwaysTemplate)
        self.img_tick.tintColor = UIColor(red: 0, green: 202/255, blue: 44/255, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
