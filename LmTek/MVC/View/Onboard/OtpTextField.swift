//
//  OtpTextField.swift
//  LmTek
//
//  Created by Sai Sankar on 12/08/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

class OtpTextField: UITextField {
    
    weak var previousTextField : OtpTextField?
    weak var nextTextField : OtpTextField?
    
    override func deleteBackward() {
        if text == "" {
            previousTextField?.keyboardType = self.keyboardType
            previousTextField?.becomeFirstResponder()
        }
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
