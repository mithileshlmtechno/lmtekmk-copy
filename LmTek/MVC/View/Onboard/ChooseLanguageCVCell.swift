//
//  ChooseLanguageCVCell.swift
//  LmTek
//
//  Created by PTBLR-1128 on 05/10/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

class ChooseLanguageCVCell: UICollectionViewCell {
    //MARK:- Outlets...
    @IBOutlet weak var view_background : UIView!
    @IBOutlet weak var lbl_language : UILabel!
    @IBOutlet weak var img_language : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.view_background.dropShadowWithRadius(x: 5)
    }

}
