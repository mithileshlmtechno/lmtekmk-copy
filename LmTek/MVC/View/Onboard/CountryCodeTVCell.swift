//
//  CountryCodeTVCell.swift
//  LmTek
//
//  Created by PTBLR-1128 on 10/09/20.
//  Copyright © 2020 LmTek. All rights reserved.
//

import UIKit

class CountryCodeTVCell: UITableViewCell {
    //MARK:- Outlets...
    @IBOutlet weak var lbl_countryCode : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Functions...
    func loadCountryCode(model: CountryCodeItem) {
        let countryName = model.name ?? ""
        let countryCode = model.phonecode ?? ""
        let attributedText = NSMutableAttributedString()
        attributedText.append(NSAttributedString(string: countryName, attributes: [NSAttributedString.Key.font : UIFont(name:"OpenSans-Semibold",size:15) ?? UIFont.boldSystemFont(ofSize: 15)]))
        attributedText.append(NSAttributedString(string: " (\(countryCode))"))
        self.lbl_countryCode.attributedText = attributedText
    }
}
